/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if !defined _FR_H_
#define _FR_H_

/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY.h>         /* get Fr_1_ERAY API */

/******************************************************************************
 Global Macros
******************************************************************************/
#define Fr_Init(initptr) Fr_1_ERAY_Init(initptr)    /* rename variant specific API function */

/******************************************************************************
 Global Data Types
******************************************************************************/

/******************************************************************************
 Global Data
******************************************************************************/

/******************************************************************************
 Global Function Declarations
******************************************************************************/

#endif /* _FR_H_ */

/*
 * = eof ======================================================================
 */

