/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

#if !defined _FR_1_ERAY_EXTENSION_H_
#define _FR_1_ERAY_EXTENSION_H_

/******************************************************************************
  Include File Checking Section
******************************************************************************/

#ifndef _FR_1_ERAY_API_H_
#error this file is not self-contained but depends on Fr_1_ERAY_Api.h which was not included
#endif

#ifndef FR_1_ERAY_VERSION_H
#error this file is not self-contained but depends on Fr_1_ERAY_Version.h which was not included
#endif

#ifndef _FR_1_ERAY_CFG_H_
#error this file is not self-contained but depends on Fr_1_ERAY_Cfg.h which was not included
#endif

/******************************************************************************
 Macro check section
******************************************************************************/


/* check for defined STD_ON macro value */
#if !defined STD_ON
#error STD_ON not defined
#endif  /* STD_ON */

/* check for defined STD_OFF macro value */
#if !defined STD_OFF
#error STD_OFF not defined
#endif  /* STD_OFF */


/* test for valid macro definition of FR_1_ERAY_RELOCATABLE_CFG_ENABLE */
#if !defined FR_1_ERAY_RELOCATABLE_CFG_ENABLE
#error FR_1_ERAY_RELOCATABLE_CFG_ENABLE not defined
#endif  /* FR_1_ERAY_RELOCATABLE_CFG_ENABLE */

/* test for valid value of FR_1_ERAY_RELOCATABLE_CFG_ENABLE */
#if (FR_1_ERAY_RELOCATABLE_CFG_ENABLE != STD_ON) && \
    (FR_1_ERAY_RELOCATABLE_CFG_ENABLE != STD_OFF)
#error macro FR_1_ERAY_RELOCATABLE_CFG_ENABLE has an invalid value
#endif /* FR_1_ERAY_RELOCATABLE_CFG_ENABLE */


/* test for valid macro definition of FR_1_ERAY_SETEXTSYNC_API_ENABLE */
#if !defined FR_1_ERAY_SETEXTSYNC_API_ENABLE
#error macro FR_1_ERAY_SETEXTSYNC_API_ENABLE not defined
#endif /* FR_1_ERAY_SETEXTSYNC_API_ENABLE */

/* test for valid value of FR_1_ERAY_SETEXTSYNC_API_ENABLE */
#if (FR_1_ERAY_SETEXTSYNC_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_SETEXTSYNC_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_SETEXTSYNC_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_SETEXTSYNC_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_MTS_API_ENABLE */
#if !defined FR_1_ERAY_MTS_API_ENABLE
#error macro FR_1_ERAY_MTS_API_ENABLE not defined
#endif /* FR_1_ERAY_MTS_API_ENABLE */

/* test for valid value of FR_1_ERAY_MTS_API_ENABLE */
#if (FR_1_ERAY_MTS_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_MTS_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_MTS_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_MTS_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_WAKEUP_API_ENABLE */
#if !defined FR_1_ERAY_WAKEUP_API_ENABLE
#error macro FR_1_ERAY_WAKEUP_API_ENABLE not defined
#endif /* FR_1_ERAY_WAKEUP_API_ENABLE */

/* test for valid value of FR_1_ERAY_WAKEUP_API_ENABLE */
#if (FR_1_ERAY_WAKEUP_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_WAKEUP_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_WAKEUP_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_WAKEUP_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_PREPARELPDU_API_ENABLE */
#if !defined FR_1_ERAY_PREPARELPDU_API_ENABLE
#error macro FR_1_ERAY_PREPARELPDU_API_ENABLE not defined
#endif /* FR_1_ERAY_PREPARELPDU_API_ENABLE */

/* test for valid value of FR_1_ERAY_PREPARELPDU_API_ENABLE */
#if (FR_1_ERAY_PREPARELPDU_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_PREPARELPDU_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_PREPARELPDU_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_PREPARELPDU_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_GETIRQSTATUS_API_ENABLE */
#if !defined FR_1_ERAY_GETIRQSTATUS_API_ENABLE
#error macro FR_1_ERAY_GETIRQSTATUS_API_ENABLE not defined
#endif /* FR_1_ERAY_GETIRQSTATUS_API_ENABLE */

/* test for valid value of FR_1_ERAY_GETIRQSTATUS_API_ENABLE */
#if (FR_1_ERAY_GETIRQSTATUS_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_GETIRQSTATUS_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_GETIRQSTATUS_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_GETIRQSTATUS_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_DISABLEIRQ_API_ENABLE */
#if !defined FR_1_ERAY_DISABLEIRQ_API_ENABLE
#error macro FR_1_ERAY_DISABLEIRQ_API_ENABLE not defined
#endif /* FR_1_ERAY_DISABLEIRQ_API_ENABLE */

/* test for valid value of FR_1_ERAY_DISABLEIRQ_API_ENABLE */
#if (FR_1_ERAY_DISABLEIRQ_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_DISABLEIRQ_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_DISABLEIRQ_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_DISABLEIRQ_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE */
#if !defined FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE
#error macro FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE not defined
#endif /* FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE */

/* test for valid value of FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE */
#if (FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_ALLSLOTS_API_ENABLE */
#if !defined FR_1_ERAY_ALLSLOTS_API_ENABLE
#error macro FR_1_ERAY_ALLSLOTS_API_ENABLE not defined
#endif /* FR_1_ERAY_ALLSLOTS_API_ENABLE */

/* test for valid value of FR_1_ERAY_ALLSLOTS_API_ENABLE */
#if (FR_1_ERAY_ALLSLOTS_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_ALLSLOTS_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_ALLSLOTS_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_ALLSLOTS_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_RECONFIGLPDU_API_ENABLE */
#if !defined FR_1_ERAY_RECONFIGLPDU_API_ENABLE
#error macro FR_1_ERAY_RECONFIGLPDU_API_ENABLE not defined
#endif /* FR_1_ERAY_RECONFIGLPDU_API_ENABLE */

/* test for valid value of FR_1_ERAY_RECONFIGLPDU_API_ENABLE */
#if (FR_1_ERAY_RECONFIGLPDU_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_RECONFIGLPDU_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_RECONFIGLPDU_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_RECONFIGLPDU_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_DISABLELPDU_API_ENABLE */
#if !defined FR_1_ERAY_DISABLELPDU_API_ENABLE
#error macro FR_1_ERAY_DISABLELPDU_API_ENABLE not defined
#endif /* FR_1_ERAY_DISABLELPDU_API_ENABLE */

/* test for valid value of FR_1_ERAY_DISABLELPDU_API_ENABLE */
#if (FR_1_ERAY_DISABLELPDU_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_DISABLELPDU_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_DISABLELPDU_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_DISABLELPDU_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE */
#if !defined FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE
#error macro FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE not defined
#endif /* FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE */

/* test for valid value of FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE */
#if (FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE */
#if !defined FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE
#error macro FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE not defined
#endif /* FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE */

/* test for valid value of FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE */
#if (FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_NMVECTOR_ENABLE */
#if !defined FR_1_ERAY_NMVECTOR_ENABLE
#error macro FR_1_ERAY_NMVECTOR_ENABLE not defined
#endif /* FR_1_ERAY_NMVECTOR_ENABLE */

/* test for valid value of FR_1_ERAY_NMVECTOR_ENABLE */
#if (FR_1_ERAY_NMVECTOR_ENABLE != STD_ON) && \
    (FR_1_ERAY_NMVECTOR_ENABLE != STD_OFF)
#error macro FR_1_ERAY_NMVECTOR_ENABLE has an invalid value
#endif /* FR_1_ERAY_NMVECTOR_ENABLE */


/* test for valid macro definition of FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE */
#if !defined FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE
#error macro FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE not defined
#endif /* FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE */

/* test for valid value of FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE */
#if (FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE */
#if !defined FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE
#error macro FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE not defined
#endif /* FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE */

/* test for valid value of FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE */
#if (FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE */


/* test for valid macro definition of FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE */
#if !defined FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE
#error macro FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE not defined
#endif /* FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE */

/* test for valid value of FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE */
#if (FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE */

/* check for correctly defined switch FR_1_ERAY_RELATIVE_TIMER_ENABLE */
#if !defined FR_1_ERAY_RELATIVE_TIMER_ENABLE
#error macro FR_1_ERAY_RELATIVE_TIMER_ENABLE not defined
#endif /* FR_1_ERAY_RELATIVE_TIMER_ENABLE */

/* test for valid value of FR_1_ERAY_RELATIVE_TIMER_ENABLE */
#if (FR_1_ERAY_RELATIVE_TIMER_ENABLE != STD_ON) && \
    (FR_1_ERAY_RELATIVE_TIMER_ENABLE != STD_OFF)
#error macro FR_1_ERAY_RELATIVE_TIMER_ENABLE has an invalid value
#endif /* FR_1_ERAY_RELATIVE_TIMER_ENABLE */

/* test for valid macro definition of FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */
#if !defined FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE
#error macro FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE not defined
#endif /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

/* test for valid value of FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */
#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE != STD_ON) && \
    (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE != STD_OFF)
#error macro FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE has an invalid value
#endif /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */


/* test for valid macro definition of FR_1_ERAY_REPORT_TO_DET_ENABLE */
#if !defined FR_1_ERAY_REPORT_TO_DET_ENABLE
#error macro FR_1_ERAY_REPORT_TO_DET_ENABLE not defined
#endif /* FR_1_ERAY_REPORT_TO_DET_ENABLE */

/* test for valid value of FR_1_ERAY_REPORT_TO_DET_ENABLE */
#if (FR_1_ERAY_REPORT_TO_DET_ENABLE != STD_ON) && \
    (FR_1_ERAY_REPORT_TO_DET_ENABLE != STD_OFF)
#error macro FR_1_ERAY_REPORT_TO_DET_ENABLE has an invalid value
#endif /* FR_1_ERAY_REPORT_TO_DET_ENABLE */


/* test for valid macro definition of FR_1_ERAY_FRIF_INTEGRATION_ENABLE */
#if !defined FR_1_ERAY_FRIF_INTEGRATION_ENABLE
#error macro FR_1_ERAY_FRIF_INTEGRATION_ENABLE not defined
#endif /* FR_1_ERAY_FRIF_INTEGRATION_ENABLE */

/* test for valid value of FR_1_ERAY_FRIF_INTEGRATION_ENABLE */
#if (FR_1_ERAY_FRIF_INTEGRATION_ENABLE != STD_ON) && \
    (FR_1_ERAY_FRIF_INTEGRATION_ENABLE != STD_OFF)
#error macro FR_1_ERAY_FRIF_INTEGRATION_ENABLE has an invalid value
#endif /* FR_1_ERAY_FRIF_INTEGRATION_ENABLE */


/* test for valid macro definition of FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE */
#if !defined FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE
#error macro FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE not defined
#endif /* FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE */

/* test for valid value of FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE */
#if (FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE != STD_ON) && \
    (FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE != STD_OFF)
#error macro FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE has an invalid value
#endif /* FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE */


/* Checking pre-compile time value "FR_1_ERAY_PAYLOAD_PADDING_ENABLE" */
#if !defined FR_1_ERAY_PAYLOAD_PADDING_ENABLE
#error FR_1_ERAY_PAYLOAD_PADDING_ENABLE not defined
#endif  /* FR_1_ERAY_PAYLOAD_PADDING_ENABLE */

/* test for valid value of FR_1_ERAY_PAYLOAD_PADDING_ENABLE */
#if (FR_1_ERAY_PAYLOAD_PADDING_ENABLE != STD_ON) && \
    (FR_1_ERAY_PAYLOAD_PADDING_ENABLE != STD_OFF)
#error macro FR_1_ERAY_PAYLOAD_PADDING_ENABLE has an invalid value
#endif /* FR_1_ERAY_PAYLOAD_PADDING_ENABLE */


/* Checking pre-compile time value "FR_1_ERAY_PAYLOAD_PADDING_U8_VALUE" */
#if !defined FR_1_ERAY_PAYLOAD_PADDING_U8_VALUE
#error FR_1_ERAY_PAYLOAD_PADDING_U8_VALUE not defined
#endif  /* FR_1_ERAY_PAYLOAD_PADDING_U8_VALUE */


/* FR_1_ERAY_DEV_ERROR_DETECT must be enabled if
 * FR_1_ERAY_REPORT_TO_DET_ENABLE is enabled
 */
#if (FR_1_ERAY_REPORT_TO_DET_ENABLE == STD_ON) && \
    (FR_1_ERAY_DEV_ERROR_DETECT == STD_OFF)
#error Invalid FR_1_ERAY_REPORT_TO_DET_ENABLE configuration
#endif


/******************************************************************************
 Global Macros
******************************************************************************/

#define FR_1_ERAY_CONTROLLER_CHIERROR    (0x01U)
/**< CHI Error macro for status bits of API service Fr_1_ERAY_GetControllerErrorStatus() */

#define FR_1_ERAY_CONTROLLER_POCERROR    (0x02U)
/**< POC Error macro for status bits of API service Fr_1_ERAY_GetControllerErrorStatus() */

#define FR_1_ERAY_CONTROLLER_PARITYERROR (0x04U)
/**< Parity Error macro for status bits of API service Fr_1_ERAY_GetControllerErrorStatus() */

#define FR_1_ERAY_CONTROLLER_BUFFERERROR (0x08U)
/**< Message Buffer Error macro for status bits of API service Fr_1_ERAY_GetControllerErrorStatus() */

/* for integration we have a special function - see above */
#if defined FR_1_ERAY_FRIF_INTEGRATION_ENABLE
#if (FR_1_ERAY_FRIF_INTEGRATION_ENABLE == STD_ON)

/*
 * redefine Fr_1_ERAY functions into FrIf functions if they have the
 * same signature and return code.
 */
#define Fr_1_ERAY_AbortCommunication FrIf_AbortCommunication
#define Fr_1_ERAY_AckAbsoluteTimerIRQ FrIf_AckAbsoluteTimerIRQ
#define Fr_1_ERAY_AckRelativeTimerIRQ FrIf_AckRelativeTimerIRQ
#define Fr_1_ERAY_AllowColdstart FrIf_AllowColdstart
#define Fr_1_ERAY_AllSlots FrIf_AllSlots
#define Fr_1_ERAY_CancelAbsoluteTimer FrIf_CancelAbsoluteTimer
#define Fr_1_ERAY_CancelRelativeTimer FrIf_CancelRelativeTimer
#define Fr_1_ERAY_CheckMTS FrIf_CheckMTS
#define Fr_1_ERAY_DisableAbsoluteTimerIRQ FrIf_DisableAbsoluteTimerIRQ
#define Fr_1_ERAY_DisableLPdu FrIf_DisableLPdu
#define Fr_1_ERAY_DisableRelativeTimerIRQ FrIf_DisableRelativeTimerIRQ
#define Fr_1_ERAY_EnableAbsoluteTimerIRQ FrIf_EnableAbsoluteTimerIRQ
#define Fr_1_ERAY_EnableRelativeTimerIRQ FrIf_EnableRelativeTimerIRQ
#define Fr_1_ERAY_GetAbsoluteTimerIRQStatus FrIf_GetAbsoluteTimerIRQStatus
#define Fr_1_ERAY_GetChannelStatus FrIf_GetChannelStatus
#define Fr_1_ERAY_GetWakeupRxStatus FrIf_GetWakeupRxStatus
#define Fr_1_ERAY_GetControllerErrorStatus FrIf_GetControllerErrorStatus
#define Fr_1_ERAY_GetGlobalTime FrIf_GetGlobalTime
#define Fr_1_ERAY_GetPOCStatus FrIf_GetPOCStatus
#define Fr_1_ERAY_GetRelativeTimerIRQStatus FrIf_GetRelativeTimerIRQStatus
#define Fr_1_ERAY_GetSyncState FrIf_GetSyncState
#define Fr_1_ERAY_HaltCommunication FrIf_HaltCommunication
#define Fr_1_ERAY_ReconfigLPdu FrIf_ReconfigLPdu
#define Fr_1_ERAY_SendMTS FrIf_SendMTS
#define Fr_1_ERAY_SendWUP FrIf_SendWUP
#define Fr_1_ERAY_SetAbsoluteTimer FrIf_SetAbsoluteTimer
#define Fr_1_ERAY_SetExtSync FrIf_SetExtSync
#define Fr_1_ERAY_SetRelativeTimer FrIf_SetRelativeTimer
#define Fr_1_ERAY_SetWakeupChannel FrIf_SetWakeupChannel
#define Fr_1_ERAY_StartCommunication FrIf_StartCommunication
#define Fr_1_ERAY_StopMTS FrIf_StopMTS
#define Fr_1_ERAY_DisableLPdu FrIf_DisableLPdu
#define Fr_1_ERAY_AckIRQ FrIf_AckIRQ
#define Fr_1_ERAY_GetIRQStatus FrIf_GetIRQStatus
#define Fr_1_ERAY_EnableIRQ FrIf_EnableIRQ
#define Fr_1_ERAY_DisableIRQ FrIf_DisableIRQ
#define Fr_1_ERAY_GetClockCorrection FrIf_GetClockCorrection
#define Fr_1_ERAY_GetSyncFrameList FrIf_GetSyncFrameList

#endif /* FR_1_ERAY_FRIF_INTEGRATION_ENABLE */
#endif /* FR_1_ERAY_FRIF_INTEGRATION_ENABLE */

/** \endcond end block Ignore in documentation */

/**
 * \brief DET error code
 *
 * Invalid IRQ index passed to service.
 */
#define FR_1_ERAY_E_INV_IRQ_IDX       (0x80U)



/**
 * \brief IRQ source
 *
 * Startup completed interrupt source index.
 */
#define FR_1_ERAY_IRQ_STARTUP_COMPLETED     (0x00U)

/**
 * \brief IRQ source
 *
 * Cycle start interrupt source index.
 */
#define FR_1_ERAY_IRQ_CYCLE_START           (0x01U)

/**
 * \brief IRQ source
 *
 * Dynamic segment start interrupt source index.
 */
#define FR_1_ERAY_IRQ_DYNAMICSEGMENT_START  (0x02U)

/**
 * \brief IRQ source
 *
 * Change of NM-Vector interrupt source index.
 */
#define FR_1_ERAY_IRQ_NMVECTOR_CHANGED      (0x03U)

/**
 * \brief IRQ source
 *
 *  Number of interrupt sources supported
 */
#define FR_1_ERAY_IRQ_NUM      (0x04U)


/******************************************************************************
 Global Data Types
******************************************************************************/

/******************************************************************************
 Global Data
******************************************************************************/

/******************************************************************************
 Global Function Declarations
******************************************************************************/

/* start code section declaration */
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/**
 * \brief   Returns a FlexRay controller error status.
 *
 * This service reads certain FlexRay controller specific error status information and writes it
 * bitcoded int *Fr_ControllerErrorStatusPtr. Afterwards the status information is reset.
 * The bitcoding has the following meaning (Bit 0 - LSB, Bit 15 - MSB):
 * - Bit 1: POCError (some error detecetd while driving the POC Statemachine).
 * - Bit 2: ParityError (internal consistency check failed, e.g. memory parity).
 * - Bit 3: BufferError (Error caused by reading/writing message buffers).
 * - Bit 4-15: always 0 (not used)
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ControllerErrorStatusPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx                 FlexRay controller index.
 * \param[out] Fr_ControllerErrorStatusPtr       Address to write the error status to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x81}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetControllerErrorStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ControllerErrorStatusPtr
    );

/**
 * \brief   Dynamically reconfigures a LPdu.
 *
 * This service reconfigures the LPdu to FlexRay FrameTriggering assignment at execution time.
 * This service doesn't restrict the configuration within the general valid limits. However it does
 * not ensure that missconfigurations are avoided (e.g. assignment of multiple LPdus to a
 * single FrameTriggering). The behvaviour might be implementation dependent in such cases.
 * The flags SUP, SYNC, PPI are always configured to 0.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LPduIdx contains an invalid value
 * FR_1_ERAY_E_INV_LPDU_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_FrameId contains an invalid value
 * FR_1_ERAY_E_INV_CONFIG is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChnlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CHNL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CycleRepetition contains an invalid value (different from 1,2,4,8,16,32,64)
 * FR_1_ERAY_E_INV_CYCLE is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CycleOffset contains an invalid value ( >= Fr_CycleRepetition))
 * FR_1_ERAY_E_INV_CYCLE is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_PayloadLength contains an invalid value (larger than buffer can manage)
 * FR_1_ERAY_E_INV_LENGTH is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_HeaderCRC contains an invalid value (out of bounds)
 * FR_1_ERAY_E_INV_CONFIG is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx         FlexRay controller index.
 * \param[in] Fr_LPduIdx         LPdu index.
 * \param[in] Fr_FrameId         FlexRay frame ID.
 * \param[in] Fr_ChnlIdx         FlexRay Channel.
 * \param[in] Fr_CycleRepetition Cycle Repetition pattern.
 * \param[in] Fr_CycleOffset       Base Cycle pattern
 * \param[in] Fr_PayloadLength   Payload length in units of bytes.
 * \param[in] Fr_HeaderCRC       FlexRay Header CRC.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x25}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ReconfigLPdu
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        VAR(uint16,AUTOMATIC) Fr_FrameId,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx,
        VAR(uint8,AUTOMATIC) Fr_CycleRepetition,
        VAR(uint8,AUTOMATIC) Fr_CycleOffset,
        VAR(uint8,AUTOMATIC) Fr_PayloadLength,
        VAR(uint16,AUTOMATIC) Fr_HeaderCRC
    );

/**
 * \brief   Enables an IRQ.
 *
 * This service enables an IRQ source.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_IRQIdx contains an invalid value
 * FR_1_ERAY_E_INV_IRQ_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx         FlexRay controller index.
 * \param[in] Fr_IRQIdx          Interrupt source index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x86}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_EnableIRQ
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_IRQIdx
    );

/**
 * \brief   Disables an IRQ.
 *
 * This service disables an IRQ source.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_IRQIdx contains an invalid value
 * FR_1_ERAY_E_INV_IRQ_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx         FlexRay controller index.
 * \param[in] Fr_IRQIdx          Interrupt source index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x87}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_DisableIRQ
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_IRQIdx
    );

/**
 * \brief   Acknowledges an IRQ.
 *
 * This service acknowledges an IRQ source.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_IRQIdx contains an invalid value
 * FR_1_ERAY_E_INV_IRQ_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx         FlexRay controller index.
 * \param[in] Fr_IRQIdx          Interrupt source index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x88}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_AckIRQ
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_IRQIdx
    );

/**
 * \brief   Returns an IRQ status.
 *
 * This service reads and returns the IRQ sources pending status.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_IRQIdx contains an invalid value
 * FR_1_ERAY_E_INV_IRQ_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in]  Fr_CtrlIdx         FlexRay controller index.
 * \param[in]  Fr_IRQIdx          Interrupt source index.
 * \param[out] Fr_IRQStatusPtr    Address to write the IRQ pending status.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x89}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetIRQStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_IRQIdx,
        P2VAR(boolean,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_IRQStatusPtr
    );

#if (FR_1_ERAY_RELATIVE_TIMER_ENABLE == STD_ON)
/* relative timer API is available */

/**
 * \brief   Sets a relative timer.
 *
 * This service programs a relative timer of the FlexRay controller.
 * Only a single relative timer is supported.
 * If the trimer was not set successfully, E_NOT_OK is retuned.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_RelTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_Offset contains an invalid value
 * FR_1_ERAY_E_INV_OFFSET is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx           FlexRay controller index.
 * \param[in] Fr_RelTimerIdx       Relative timer index.
 * \param[in] Fr_Offset            Macrotick offset the alarm should elapse.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x90}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_SetRelativeTimer
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_RelTimerIdx,
        VAR(uint16,AUTOMATIC) Fr_Offset
    );


/**
 * \brief   Cancels the relative timer.
 *
 * This service cancels an already active relative timer.
 * Only a single relative timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_RelTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx           FlexRay controller index.
 * \param[in] Fr_RelTimerIdx       Relative timer index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x91}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CancelRelativeTimer
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_RelTimerIdx
    );


/**
 * \brief   Enables the relative timer IRQ.
 *
 * This service enables the interrupt of a relative timer.
 * Only a single relative timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_RelTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx           FlexRay controller index.
 * \param[in] Fr_RelTimerIdx       Relative timer index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x92}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_EnableRelativeTimerIRQ
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_RelTimerIdx
    );


/**
 * \brief   Acknowledges the relative timer IRQ.
 *
 * This service acknowledges the pending interrupt of a relative timer.
 * Only a single relative timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_RelTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx           FlexRay controller index.
 * \param[in] Fr_RelTimerIdx       Relative timer index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x93}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_AckRelativeTimerIRQ
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_RelTimerIdx
    );


/**
 * \brief   Disables the relative timer IRQ.
 *
 * This service disables the interrupt of a relative timer.
 * Only a single relative timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_RelTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx           FlexRay controller index.
 * \param[in] Fr_RelTimerIdx       Relative timer index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x94}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_DisableRelativeTimerIRQ
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_RelTimerIdx
    );

/**
 * \brief   Reads the relative timer IRQ status.
 *
 * This service disables the interrupt of a relative timer.
 * Only a single relative timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_IRQStatusPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_RelTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_RelTimerIdx       Relative timer index.
 * \param[out] Fr_IRQStatusPtr    Address the timer IRQ status is stored to
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x95}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetRelativeTimerIRQStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_RelTimerIdx,
        P2VAR(boolean,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_IRQStatusPtr
    );
#endif /* FR_1_ERAY_RELATIVE_TIMER_ENABLE */

/**
 * \brief   Sends an MTS symbol in the symbol window.
 *
 * This service initiates a transmission of a single MTS symbol in the next symbol window.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChnlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CHNL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and the FlexRay controller is not synchronized
 * FR_1_ERAY_E_INV_POCSTATE is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx       FlexRay controller index.
 * \param[in] Fr_ChnlIdx       Channel the MTS should be transmitted on.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x96}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Asynchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_SendMTS
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx
    );


/**
 * \brief   Stops transmission of an MTS symbol.
 *
 * This service stops the transmission of an activated MTS symbol transmission.
 * Since this FlexRay controller has a single-shot MTS-control semantics, no explicit MTS stopping
 * is required. Therefore this is a dummy service that doesn't perform any action on the FlexRay
 * controller.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChnlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CHNL_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_ChnlIdx        Channel the MTS should be transmitted on.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x97}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_StopMTS
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx
    );


/**
 * \brief   Reports the MTS receive status.
 *
 * This service reads the status of the symbol window of the last communication cycle.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChnlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CHNL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_MTSStatusPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_ChnlIdx        Channel the MTS should be received on.
 * \param[out] Fr_MTSStatusPtr  Address the MTS status is written to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x98}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CheckMTS
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx,
        P2VAR(Fr_MTSStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_MTSStatusPtr
    );

/**
 * \brief   Initiates external clock synchronization.
 *
 * This service triggers the appliance of an external clock correction (rate, offset,or both) term.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and the FlexRay controller is not synchronized
 * FR_1_ERAY_E_INV_POCSTATE is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx            FlexRay controller index.
 * \param[in] Fr_Offset             Offset correction application mode.
 * \param[in] Fr_Rate               Rate correction application mode.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x99}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Asynchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_SetExtSync
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(Fr_OffsetCorrectionType,AUTOMATIC) Fr_Offset,
        VAR(Fr_RateCorrectionType,AUTOMATIC) Fr_Rate
    );


/**
 * \brief   Reads the FlexRay CC synchronization state.
 *
 * This service evaluates whether the FlexRay controller is synchronized to global time
 * (POC-state 'normal-active' or 'normal-passive') or not.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_MTSStatusPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx            FlexRay controller index.
 * \param[out] Fr_SyncStatePtr      Address the synchronization state is written to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x9A}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetSyncState
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(Fr_SyncStateType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_SyncStatePtr
    );

/* stop code section declaration */
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */


#endif /* _FR_1_ERAY_EXTENSION_H_ */

/*
 * = eof ======================================================================
 */

