/* !LINKSTO Fr.ASR40.FR074,1 */

/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined FR_1_ERAY_VERSION_H)
#define FR_1_ERAY_VERSION_H

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*------------------[AUTOSAR vendor identification]-------------------------*/

#if (defined FR_1_ERAY_VENDOR_ID)
#error FR_1_ERAY_VENDOR_ID is already defined
#endif
/** \brief AUTOSAR vendor identification: Elektrobit Automotive GmbH */
#define FR_1_ERAY_VENDOR_ID         1U

/*------------------[AUTOSAR module identification]-------------------------*/

#if (defined FR_1_ERAY_MODULE_ID)
#error FR_1_ERAY_MODULE_ID already defined
#endif
/** \brief AUTOSAR module identification */
#define FR_1_ERAY_MODULE_ID         81U

/*------------------[AUTOSAR release version identification]----------------*/

#if (defined FR_1_ERAY_AR_RELEASE_MAJOR_VERSION)
#error FR_1_ERAY_AR_RELEASE_MAJOR_VERSION already defined
#endif
/** \brief AUTOSAR release major version */
#define FR_1_ERAY_AR_RELEASE_MAJOR_VERSION     4U

#if (defined FR_1_ERAY_AR_RELEASE_MINOR_VERSION)
#error FR_1_ERAY_AR_RELEASE_MINOR_VERSION already defined
#endif
/** \brief AUTOSAR release minor version */
#define FR_1_ERAY_AR_RELEASE_MINOR_VERSION     0U

#if (defined FR_1_ERAY_AR_RELEASE_REVISION_VERSION)
#error FR_1_ERAY_AR_RELEASE_REVISION_VERSION already defined
#endif
/** \brief AUTOSAR release revision version */
#define FR_1_ERAY_AR_RELEASE_REVISION_VERSION  3U

/*------------------[AUTOSAR module version identification]------------------*/

#if (defined FR_1_ERAY_SW_MAJOR_VERSION)
#error FR_1_ERAY_SW_MAJOR_VERSION already defined
#endif
/** \brief AUTOSAR module major version */
#define FR_1_ERAY_SW_MAJOR_VERSION             5U

#if (defined FR_1_ERAY_SW_MINOR_VERSION)
#error FR_1_ERAY_SW_MINOR_VERSION already defined
#endif
/** \brief AUTOSAR module minor version */
#define FR_1_ERAY_SW_MINOR_VERSION             2U

#if (defined FR_1_ERAY_SW_PATCH_VERSION)
#error FR_1_ERAY_SW_PATCH_VERSION already defined
#endif
/** \brief AUTOSAR module patch version */
#define FR_1_ERAY_SW_PATCH_VERSION             5U

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( FR_1_ERAY_VERSION_H ) */
/*==================[end of file]============================================*/
