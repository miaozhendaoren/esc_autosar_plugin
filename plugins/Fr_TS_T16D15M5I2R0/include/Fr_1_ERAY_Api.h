/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

#if !defined _FR_1_ERAY_API_H_
#define _FR_1_ERAY_API_H_

/******************************************************************************
 Include Section
******************************************************************************/

#include <ComStack_Types.h>        /* get AUTOSAR standard type definitions */
/* !LINKSTO Fr.ASR40.FR460,1 */
#include <Fr_GeneralTypes.h>       /* get AUTOSAR standard type definitions */
#include <Fr_1_ERAY_Cfg.h>         /* get Fr_1_ERAY pre-compile-time configuration   */
#include <Fr_1_ERAY_Version.h>     /* version check macros */
#include <Fr_1_ERAY_Types.h>       /* get Fr_1_ERAY type definitions */
#include <Fr_1_ERAY_Extension.h>   /* get Fr_1_ERAY vendor specific extensions */
#if (FR_1_ERAY_PBCFGM_SUPPORT_ENABLED == STD_ON)
#include <PbcfgM_Api.h>         /* Post build configuration manager */
#endif /* FR_1_ERAY_PBCFGM_SUPPORT_ENABLED */
/******************************************************************************
 Global Macros
******************************************************************************/

#define FR_1_ERAY_WUP_CHANNEL_A (0x01U)
/**< Channel A wakeup status of API service Fr_1_ERAY_GetWakeupRxStatus() */

#define FR_1_ERAY_WUP_CHANNEL_B (0x02U)
/**< Channel B wakeup status of API service Fr_1_ERAY_GetWakeupRxStatus() */

/**
 * \brief DET error code
 *
 * Invalid timer index offset passed to service.
 */
#define FR_1_ERAY_E_INV_TIMER_IDX      (0x01U)

/**
 * \brief DET error code
 *
 * Invalid pointer (NULL_PTR) passed to service.
 */
#define FR_1_ERAY_E_INV_POINTER        (0x02U)

/**
 * \brief DET error code
 *
 * Invalid macrotick offset passed to service.
 */
#define FR_1_ERAY_E_INV_OFFSET         (0x03U)

/**
 * \brief DET error code
 *
 * Invalid controller index passed to service.
 */
#define FR_1_ERAY_E_INV_CTRL_IDX       (0x04U)

/**
 * \brief DET error code
 *
 * Invalid channel index passed to service.
 */
#define FR_1_ERAY_E_INV_CHNL_IDX       (0x05U)

/**
 * \brief DET error code
 *
 * Invalid cycle value passed to service.
 */
#define FR_1_ERAY_E_INV_CYCLE          (0x06U)

/**
 * \brief DET error code
 *
 * Invalid configuration detected.
 */
#define FR_1_ERAY_E_INV_CONFIG         (0x07U)

/**
 * \brief DET error code
 *
 * Service was called although module was not initialized.
 */
#define FR_1_ERAY_E_NOT_INITIALIZED    (0x08U)

/**
 * \brief DET error code
 *
 * Invalid controller POC state detected.
 */
#define FR_1_ERAY_E_INV_POCSTATE       (0x09U)

/**
 * \brief DET error code
 *
 * Invalid payload length passed to service.
 */
#define FR_1_ERAY_E_INV_LENGTH         (0x0AU)

/**
 * \brief DET error code
 *
 * Invalid LPDu index passed to service.
 */
#define FR_1_ERAY_E_INV_LPDU_IDX       (0x0BU)

/**
 * \brief DET error code
 *
 * Invalid header CRC passed to service.
 */
#define FR_1_ERAY_E_INV_HEADERCRC      (0x0CU)

/**
 * \brief Service IDs
 *
 * API function service IDs.
 */
/**
 * Service identifiers for Fr_1_ERAY_CheckTxLPduStatus() API function
 */
#define FR_1_ERAY_CHECKTXLPDUSTATUS_SERVICE_ID 0x0D
/**
 * Service identifiers for Fr_1_ERAY_CheckMTS() API function
 */
#define FR_1_ERAY_CHECKMTS_SERVICE_ID  0x98
/**
 * Service identifiers for Fr_1_ERAY_CancelRelativeTimer() API function
 */
#define FR_1_ERAY_CANCELRELATIVETIMER_SERVICE_ID 0x91
/**
 * Service identifiers for Fr_1_ERAY_CancelAbsoluteTimer() API function
 */
#define FR_1_ERAY_CANCELABSOLUTETIMER_SERVICE_ID 0x13
/**
 * Service identifiers for Fr_1_ERAY_AllSlots() API function
 */
#define FR_1_ERAY_ALLSLOTS_SERVICE_ID 0x24
/**
 * Service identifiers for Fr_1_ERAY_AllowColdstart() API function
 */
#define FR_1_ERAY_ALLOWCOLDSTART_SERVICE_ID 0x23
/**
 * Service identifiers for Fr_1_ERAY_AckRelativeTimerIRQ() API function
 */
#define FR_1_ERAY_ACKRELATIVETIMERIRQ_SERVICE_ID 0x93
/**
 * Service identifiers for Fr_1_ERAY_AckIRQ() API function
 */
#define FR_1_ERAY_ACKIRQ_SERVICE_ID 0x88
/**
 * Service identifiers for Fr_1_ERAY_AckAbsoluteTimerIRQ() API function
 */
#define FR_1_ERAY_ACKABSOLUTETIMERIRQ_SERVICE_ID 0x17
/**
 * Service identifiers for Fr_1_ERAY_AbortCommunication() API function
 */
#define FR_1_ERAY_ABORTCOMMUNICATION_SERVICE_ID 0x05
/**
 * Service identifiers for Fr_1_ERAY_TransmitTxLPdu() API function
 */
#define FR_1_ERAY_TRANSMITTXLPDU_SERVICE_ID 0x0B
/**
 * Service identifiers for Fr_1_ERAY_StartCommunication() API function
 */
#define FR_1_ERAY_STARTCOMMUNICATION_SERVICE_ID 0x03
/**
 * Service identifiers for Fr_1_ERAY_SetWakeupChannel() API function
 */
#define FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID 0x07
/**
 * Service identifiers for Fr_1_ERAY_SetRelativeTimer() API function
 */
#define FR_1_ERAY_SETRELATIVETIMER_SERVICE_ID 0x90
/**
 * Service identifiers for Fr_1_ERAY_SetExtSync() API function
 */
#define FR_1_ERAY_SETEXTSYNC_SERVICE_ID 0x99
/**
 * Service identifiers for Fr_1_ERAY_SetAbsoluteTimer() API function
 */
#define FR_1_ERAY_SETABSOLUTETIMER_SERVICE_ID 0x11
/**
 * Service identifiers for Fr_1_ERAY_SendWUP() API function
 */
#define FR_1_ERAY_SENDWUP_SERVICE_ID 0x06
/**
 * Service identifiers for Fr_1_ERAY_SendMTS() API function
 */
#define FR_1_ERAY_SENDMTS_SERVICE_ID 0x96
/**
 * Service identifiers for Fr_1_ERAY_StopMTS() API function
 */
#define FR_1_ERAY_STOPMTS_SERVICE_ID 0x97
/**
 * Service identifiers for Fr_1_ERAY_ReconfigLPdu() API function
 */
#define FR_1_ERAY_RECONFIGLPDU_SERVICE_ID 0x25
/**
 * Service identifiers for Fr_1_ERAY_ReceiveRxLPdu() API function
 */
#define FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID 0x0C
/**
 * Service identifiers for Fr_1_ERAY_PrepareLPdu() API function
 */
#define FR_1_ERAY_PREPARELPDU_SERVICE_ID 0x1F
/**
 * Service identifiers for Fr_1_ERAY_Init() API function
 */
#define FR_1_ERAY_INIT_SERVICE_ID 0x1C
/**
 * Service identifiers for Fr_1_ERAY_HaltCommunication() API function
 */
#define FR_1_ERAY_HALTCOMMUNICATION_SERVICE_ID 0x04
/**
 * Service identifiers for Fr_1_ERAY_GetWakeupRxStatus() API function
 */
#define FR_1_ERAY_GETWAKEUPRXSTATUS_SERVICE_ID 0x2B
/**
 * Service identifiers for Fr_1_ERAY_GetVersionInfo() API function
 */
#define FR_1_ERAY_GETVERSIONINFO_SERVICE_ID 0x1B
/**
 * Service identifiers for Fr_1_ERAY_GetSyncState() API function
 */
#define FR_1_ERAY_GETSYNCSTATE_SERVICE_ID 0x9A
/**
 * Service identifiers for Fr_1_ERAY_GetSyncFrameList() API function
 */
#define FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID 0x2A
/**
 * Service identifiers for Fr_1_ERAY_GetRelativeTimerIRQStatus() API function
 */
#define FR_1_ERAY_GETRELATIVETIMERIRQSTATUS_SERVICE_ID 0x95
/**
 * Service identifiers for Fr_1_ERAY_GetPOCStatus() API function
 */
#define FR_1_ERAY_GETPOCSTATUS_SERVICE_ID 0x0A
/**
 * Service identifiers for Fr_1_ERAY_GetPOCStatus() API function
 */
#define FR_1_ERAY_GETNMVECTOR_SERVICE_ID 0x22
/**
 * Service identifiers for Fr_1_ERAY_GetIRQStatus() API function
 */
#define FR_1_ERAY_GETIRQSTATUS_SERVICE_ID 0x89
/**
 * Service identifiers for Fr_1_ERAY_GetGlobalTime() API function
 */
#define FR_1_ERAY_GETGLOBALTIME_SERVICE_ID 0x10
/**
 * Service identifiers for Fr_1_ERAY_GetControllerErrorStatus() API function
 */
#define FR_1_ERAY_GETCONTROLLERERRORSTATUS_SERVICE_ID 0x81
/**
 * Service identifiers for Fr_1_ERAY_GetClockCorrection() API function
 */
#define FR_1_ERAY_GETCLOCKCORRECTION_SERVICE_ID 0x29
/**
 * Service identifiers for Fr_1_ERAY_GetChannelStatus() API function
 */
#define FR_1_ERAY_GETCHANNELSTATUS_SERVICE_ID 0x28
/**
 * Service identifiers for Fr_1_ERAY_GetAbsoluteTimerIRQStatus() API function
 */
#define FR_1_ERAY_GETABSOLUTETIMERIRQSTATUS_SERVICE_ID 0x20
/**
 * Service identifiers for Fr_1_ERAY_EnableRelativeTimerIRQ() API function
 */
#define FR_1_ERAY_ENABLERELATIVETIMERIRQ_SERVICE_ID 0x92
/**
 * Service identifiers for Fr_1_ERAY_EnableIRQ() API function
 */
#define FR_1_ERAY_ENABLEIRQ_SERVICE_ID 0x86
/**
 * Service identifiers for Fr_1_ERAY_EnableAbsoluteTimerIRQ() API function
 */
#define FR_1_ERAY_ENABLEABSOLUTETIMERIRQ_SERVICE_ID 0x15
/**
 * Service identifiers for Fr_1_ERAY_DisableRelativeTimerIRQ() API function
 */
#define FR_1_ERAY_DISABLERELATIVETIMERIRQ_SERVICE_ID 0x94
/**
 * Service identifiers for Fr_1_ERAY_DisableLPdu() API function
 */
#define FR_1_ERAY_DISABLELPDU_SERVICE_ID 0x26
/**
 * Service identifiers for Fr_1_ERAY_DisableIRQ() API function
 */
#define FR_1_ERAY_DISABLEIRQ_SERVICE_ID 0x87
/**
 * Service identifiers for Fr_1_ERAY_DisableAbsoluteTimerIRQ() API function
 */
#define FR_1_ERAY_DISABLEABSOLUTETIMERIRQ_SERVICE_ID 0x19
/**
 * Service identifiers for Fr_1_ERAY_ControllerInit() API function
 */
#define FR_1_ERAY_CONTROLLERINIT_SERVICE_ID 0x0


/******************************************************************************
 Macro checks
******************************************************************************/

/* check for defined STD_ON macro value */
#if !defined STD_ON
#error STD_ON not defined
#endif  /* STD_ON */

/* check for defined STD_OFF macro value */
#if !defined STD_OFF
#error STD_OFF not defined
#endif  /* STD_OFF */

/* check for correctly defined switch FR_1_ERAY_VERSION_INFO_API */
#if !defined FR_1_ERAY_VERSION_INFO_API
#error macro FR_1_ERAY_VERSION_INFO_API not defined
#endif /* FR_1_ERAY_VERSION_INFO_API */

/* test for valid value of FR_1_ERAY_VERSION_INFO_API */
#if (FR_1_ERAY_VERSION_INFO_API != STD_ON) && \
    (FR_1_ERAY_VERSION_INFO_API != STD_OFF)
#error macro FR_1_ERAY_VERSION_INFO_API has an invalid value
#endif /* FR_1_ERAY_VERSION_INFO_API */

/* test for valid macro definition of FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE */
#if !defined FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE
#error macro FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE not defined
#endif /* FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE */

/* test for valid value of FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE */
#if (FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE != STD_ON) && \
    (FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE != STD_OFF)
#error macro FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE has an invalid value
#endif /* FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE */

/* check for correctly defined switch FR_1_ERAY_DEV_ERROR_DETECT */
#if !defined FR_1_ERAY_DEV_ERROR_DETECT
#error macro FR_1_ERAY_DEV_ERROR_DETECT not defined
#endif /* FR_1_ERAY_DEV_ERROR_DETECT */

/* test for valid value of FR_1_ERAY_DEV_ERROR_DETECT */
#if (FR_1_ERAY_DEV_ERROR_DETECT != STD_ON) && \
    (FR_1_ERAY_DEV_ERROR_DETECT != STD_OFF)
#error macro FR_1_ERAY_DEV_ERROR_DETECT has an invalid value
#endif /* FR_1_ERAY_DEV_ERROR_DETECT */

/******************************************************************************
 Global Data Types
******************************************************************************/

/******************************************************************************
 Global Data
******************************************************************************/

/******************************************************************************
 Global Function Declarations
******************************************************************************/


/* start code section declaration */
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/** \brief Validate configuration
 **
 ** Checks if the post build configuration fits to the link time configuration part.
 **
 ** \param[in] ConfigPtr Pointer where the post-build-time configuration is stored
 ** \return E_OK if the given module configurations is valid otherwise E_NOT_OK.
 **
 ** \ServiceID{0x60}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 **/
extern FUNC(Std_ReturnType, FR_1_ERAY_CODE) Fr_1_ERAY_IsValidConfig(
    P2CONST(void, AUTOMATIC, FR_1_ERAY_APPL_CONST) voidConfigPtr);


/** \brief Initializes the module.
 *
 * This service initializes the module and registers the post-build-time configuration passed as
 * argument for usage by other service of this module.
 * This function leaves all FlexRay CC's in 'halt' POC-state.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM.
 *
 * If Fr_ConfigPtr is NULL_PTR, FR_1_ERAY_E_INV_POINTER is reported to DET.
 *
 * \param[in] Fr_ConfigPtr Pointer where the post-build-time configuration is stored.
 *
 * \ServiceID{0x1C}
 * \Reentrancy{Non Reentrant}
 * \Synchronicity{Synchronous}
 */
extern FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_Init
    (
        P2CONST(Fr_1_ERAY_ConfigType,AUTOMATIC,FR_1_ERAY_APPL_CONST) Fr_ConfigPtr
    );


/**
 * \brief Initializes a FlexRay controller
 *
 * This services brings the FlexRay controller from any POC-state into POC-state config using the
 * CHI command 'FREEZE'. It initializes the FlexRay CC low level parameters and all buffers
 * according to the configuration passed at Fr_1_ERAY_Init(). Afterwards the POC-state 'CONFIG' is
 * left to POC-state 'READY'
 * This service ensures that
 * - no transmission requests are pending.
 * - no reception indications are pending.
 * - no interrupts are pending.
 * - all timers are disabled.
 * - all interrupts are disabled.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx            Index of FlexRay controller to initialize.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x00}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ControllerInit
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );

/**
 * \brief   Invokes CHI command 'RUN'.
 *
 * This service executes the CHI command 'RUN' which activates the FlexRay controllers startup
 * procedure.
 * For Autosar 2.1 only this service executes first the CHI command 'ALLOW_COLDSTART'
 * if configration parameter 'FrController/FrIsLeadingColdstarter' is set to 'true'.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and the FlexRay controller is not in POC-state 'ready'
 * FR_1_ERAY_E_INV_POCSTATE is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x03}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Asynchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_StartCommunication
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );


/**
 * \brief   Invokes CHI command 'ALLOW_COLDTSTART'.
 *
 * This service executes the CHI command 'ALLOW_COLDSTART', which enables a controller to be a
 * leading coldstarter.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and the FlexRay controller is in one of the POC-states 'defaultConfig',
 * 'config' or 'halt' FR_1_ERAY_E_INV_POCSTATE is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x23}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Asynchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_AllowColdstart
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );

/**
 * \brief   Invokes CHI command 'ALL_SLOTS'.
 *
 * This service executes the CHI command 'ALL_SLOTS', which activates the all-slot-mode with the
 * next communication cycle.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and the FlexRay controller is not synchronized
 * FR_1_ERAY_E_INV_POCSTATE is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x24}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_AllSlots
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );

/**
 * \brief   Invokes CHI command 'HALT'.
 *
 * This service executes the CHI command 'HALT', which halts the controllers FlexRay participation
 * at the end of the current communication cycle and leaves it then it POC-state 'halt'.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and the FlexRay controller is not synchronized
 * FR_1_ERAY_E_INV_POCSTATE is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x04}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Asynchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_HaltCommunication
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );

/**
 * \brief   Invokes CHI command 'FREEZE'.
 *
 * This service executes the CHI command 'FREEZE', which immediately puts the FlexRay controller
 * into POC-state 'halt' from in whatever POC-state it is currently in.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x05}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_AbortCommunication
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );


/**
 * \brief   Invokes CHI command 'WAKEUP'.
 *
 * This service executes the CHI command 'WAKEUP', which initiates the transmission of a wakeup
 * symbol on the predefined channel.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and the FlexRay controller is not in POC-state 'ready'
 * FR_1_ERAY_E_INV_POCSTATE is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x06}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Asynchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_SendWUP
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );


/**
 * \brief   Selects a channel for wakeup pattern transmission.
 *
 * This service bring the FlexRay CC into POC-state config, reconfigures the wakeup channel and
 * and finally moves the POC-state back to 'ready' again.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChnlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CHNL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and the FlexRay controller is not in POC-state 'ready'
 * FR_1_ERAY_E_INV_POCSTATE is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_ChnlIdx        Channel the wakeup pattern should be transmitted on.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x07}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_SetWakeupChannel
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx
    );

/**
 * \brief   Reads FlexRay CC POC-status.
 *
 * This service reads and returns the POCStatus as specified in the FlexRay Protocol
 * specification 2.1.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_POCStatusPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx             FlexRay controller index.
 * \param[out] Fr_POCStatusPtr       Address the POC-status is written to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x0A}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetPOCStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    );


/**
 * \brief   Transmits a LPdu.
 *
 * This service writes the payload of a LPdu into a FlexRay CC transmit buffer and commits it for
 * transmission. The service ensures that the byte order on the network is equal to the byte order
 * in memory (lower address - first, higher address - later).
 *
 * Note that this service doesn't check the transmit message buffer for being correctly configured.
 * Rational: The MCG ensures that buffer optimization is used only if Fr_PrepareLPdu() and
 * Fr_TransmitTxLPdu() are executed within a single FrIf Job. Therefore an invalid mesage buffer
 * configuration caused by reconfiguration is avoided.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LPduIdx contains an invalid value
 * FR_1_ERAY_E_INV_LPDU_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LSduLength contains an invalid value
 * FR_1_ERAY_E_INV_LENGTH is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LSduPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx       FlexRay controller index.
 * \param[in] Fr_LPduIdx       LPdu index of PDU used for LSdu transmission.
 * \param[in] Fr_LSduPtr       Payload data pointer.
 * \param[in] Fr_LSduLength    Payload data length (in units of bytes).
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x0B}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_TransmitTxLPdu
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        P2CONST(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduPtr,
        VAR(uint8,AUTOMATIC) Fr_LSduLength
    );

/**
 * \brief   Receives a LPdu.
 *
 * This service reads the payload of a LPdu out of a FlexRay CC receive buffer.
 * The service ensures that the byte order on the network is equal to the byte order
 * in memory (lower address - first, higher address - later).
 * Only valid, non-null frames are received.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LPduIdx contains an invalid value
 * FR_1_ERAY_E_INV_LPDU_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LSduPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LPduStatusPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LSduLengthPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx                FlexRay controller index.
 * \param[in] Fr_LPduIdx                LPdu index of PDU used for LSdu reception.
 * \param[out] Fr_LSduPtr               Address payload data should be written to.
 * \param[out] Fr_LPduStatusPtr         Address the receive status should be written to.
 * \param[out] Fr_LSduLengthPtr         Address the actually received payload data length
 *                                      (in units of bytes) should be written to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x0C}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ReceiveRxLPdu
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduPtr,
        P2VAR(Fr_RxLPduStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LPduStatusPtr,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduLengthPtr
    );


/**
 * \brief   Checks the transmission status of a previously transmitted LPdu.
 *
 * This service checks whether a previously commited transmit message buffer is still pending
 * for transmission or not.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LPduIdx contains an invalid value
 * FR_1_ERAY_E_INV_LPDU_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LPduInfoPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in]  Fr_CtrlIdx            FlexRay controller index.
 * \param[in]  Fr_LPduIdx            Virtual Buffer used for LSdu transmission.
 * \param[out] Fr_TxLPduStatusPtr    Address LPdu transmit status should be written to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x0D}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CheckTxLPduStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        P2VAR(Fr_TxLPduStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_TxLPduStatusPtr
    );


/**
 * \brief   Prepares a LPdu for transmission/reception
 *
 * This service reconfigures a message buffer according to frame-triggering properties (slot, channel, cycle)
 * bound to the current LPdu. This service can be used if multiple LPDus share a single FlexRay controller's
 * message buffer.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LPduIdx contains an invalid value
 * FR_1_ERAY_E_INV_LPDU_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx       FlexRay controller index.
 * \param[in] Fr_LPduIdx       LPdu index of PDU to prepare.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x1F}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_PrepareLPdu
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx
    );

/**
 * \brief   Dynamically disables a LPdu.
 *
 * This service disables a LPdu in a way that transmission as well as reception are stopped.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_LPduIdx contains an invalid value
 * FR_1_ERAY_E_INV_LPDU_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx         FlexRay controller index.
 * \param[in] Fr_LPduIdx         LPdu index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x26}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_DisableLPdu
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx
    );

/**
 * \brief   Reads the FlexRay clusters global time.
 *
 * This service reads the global time of the FlexRay cluister and returns its value.
 * The service ensures that both, cycle and macrotick offset are consistent.
 * The service returns E_NOT_OK if the controller is not synchronized.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CyclePtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_MacroTickPtr is NULL_PTR,
 *            FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx            FlexRay controller index.
 * \param[out] Fr_CyclePtr          Address to write the current cycle counter value to.
 * \param[out] Fr_MacroTickPtr      Address to write the current macrotick counter value to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x10}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 *
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetGlobalTime
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_CyclePtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_MacroTickPtr
    );

/**
 * \brief   Reads the FlexRay NM-Vector of the last FlexRay cycle.
 *
 * This service reads the Nm-Vector of the laste communication cycle.
 * The service ensures that the byte order on the network is equal to the byte order
 * in memory (lower address - first, higher address - later).
 * The service returns E_NOT_OK if the controller is not synchronized.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_NmVectorPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx            FlexRay controller index.
 * \param[out] Fr_NmVectorPtr       Address to write the Nm-Vector to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x22}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 *
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetNmVector
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_NmVectorPtr
    );

/**
 * \brief   Returns the FlexRay channel status.
 *
 * This service reads the aggregated channel status of channel A and channel B and writes it
 * bitcoded int *Fr_ChannelStatusPtr. Afterwards the status information is reset.
 * The bitcoding has the following meaning (Bit 0 - LSB, Bit 15 - MSB):
 * - Bit 0: Channel A/B aggregated channel status vSS!ValidFrame
 * - Bit 1: Channel A/B aggregated channel status vSS!SyntaxError
 * - Bit 2: Channel A/B aggregated channel status vSS!ContentError
 * - Bit 3: Channel A/B aggregated channel status additional communication
 * - Bit 4: Channel A/B aggregated channel status vSS!Bviolation
 * - Bit 5: Not used (0)
 * - Bit 6: Not used (0)
 * - Bit 7: Not used (0)
 * - Bit 8: Channel A/B symbol window status data vSS!ValidMTS
 * - Bit 9: Channel A/B symbol window status data vSS!SyntaxError
 * - Bit 10: Channel A/B symbol window status data vSS!Bviolation
 * - Bit 11: Channel A/B symbol window status data vSS!TxConflict
 * - Bit 12: Channel A/B NIT status data vSS!SyntaxError
 * - Bit 13: Channel A/B NIT status data vSS!Bviolation
 * - Bit 14: Not used (0)
 * - Bit 15: Not used (0)
 *
 * Not used channels return 0 for all status bits.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChannelStatusPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx                 FlexRay controller index.
 * \param[out] Fr_ChannelAStatusPtr      Address to write channel A status to.
 * \param[out] Fr_ChannelBStatusPtr      Address to write channel B status to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x28}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetChannelStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAStatusPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBStatusPtr
    );

/**
 * \brief   Returns the FlexRay clock correction terms.
 *
 * This service reads the FlexRay clock correction terms and writes them
 * into *Fr_RateCorrectionPtr and *Fr_RateCorrectionPtr.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_RateCorrectionPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_OffsetCorrectionPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx                 FlexRay controller index.
 * \param[out] Fr_RateCorrectionPtr      Address to write the rate correction value to.
 * \param[out] Fr_OffsetCorrectionPtr    Address to write the offset correction value to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x29}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetClockCorrection
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(sint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_RateCorrectionPtr,
        P2VAR(sint32,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_OffsetCorrectionPtr
    );

/**
 * \brief   Returns a list of sync frames.
 *
 * This service writes a list of sync frames observed in the last even/odd communication cycle couple
 * into *Fr_ChannelAEvenListPtr, *Fr_ChannelBEvenListPtr, *Fr_ChannelAOddListPtr and
 * *Fr_ChannelBOddListPtr. The maximum number of sync frames written is determined by Fr_ListSize.
 * Unused array elements are written with value 0, determining that all seen syncframes are reported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChannelAEvenListPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChannelBEvenListPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChannelAOddListPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ChannelBOddListPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_ListSize > 15 then,
 * FR_1_ERAY_E_INV_CONFIG is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx                 FlexRay controller index.
 * \param[in] Fr_ListSize                Size of list passed to the output pointers.
 * \param[out] Fr_ChannelAEvenListPtr    Address to write the list of even sync frames of channel A.
 * \param[out] Fr_ChannelBEvenListPtr    Address to write the list of even sync frames of channel B.
 * \param[out] Fr_ChannelAOddListPtr     Address to write the list of odd sync frames of channel A.
 * \param[out] Fr_ChannelBOddListPtr     Address to write the list of odd sync frames of channel B.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x2A}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetSyncFrameList
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_ListSize,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAEvenListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBEvenListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAOddListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBOddListPtr
    );
    
/**
 * \brief   Reads the wakeup rx status of the FlexRay controller.
 *
 * This service reads the status whether a wakeup pattern has been received on channel A and channel B
 * and writes this information bitcoded to *FrWakeupRxStatusPtr with the following meaning:
 * - Bit 0: 1 - wakeup detected on Channel A, 0 - no wakeup detected on channel A
 * - Bit 1: 1 - wakeup detected on Channel B, 0 - no wakeup detected on channel B
 *
 * Afterwards the status information is reset.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_WakeupRxStatusPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx            FlexRay controller index.
 * \param[out] Fr_WakeupRxStatusPtr       Address to write the wakeup rx status to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x2B}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 *
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetWakeupRxStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_WakeupRxStatusPtr
    );
    
/**
 * \brief   Sets an absolute timer.
 *
 * This service programs an absolute timer of the FlexRay controller.
 * Only a single absolute timer is supported.
 * If the trimer was not set successfully, E_NOT_OK is retuned.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_AbsTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_Cycle contains an invalid value
 * FR_1_ERAY_E_INV_CYCLE is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_Offset contains an invalid value
 * FR_1_ERAY_E_INV_OFFSET is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_AbsTimerIdx    Absolute timer index.
 * \param[in] Fr_Cycle          Communication Cycle the alarm should elapse.
 * \param[in] Fr_Offset         Macrotick offset the alarm should elapse.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x11}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 *
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_SetAbsoluteTimer
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_AbsTimerIdx,
        VAR(uint8,AUTOMATIC) Fr_Cycle,
        VAR(uint16,AUTOMATIC) Fr_Offset
    );


/**
 * \brief   Cancels the absolute timer.
 *
 * This service cancels an already active absolute timer.
 * Only a single absolute timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_AbsTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_AbsTimerIdx       Absolute timer index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x13}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CancelAbsoluteTimer
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_AbsTimerIdx
    );


/**
 * \brief   Enables the absolute timer IRQ.
 *
 * This service enables the interrupt of an absolute timer.
 * Only a single absolute timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_AbsTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_AbsTimerIdx       Absolute timer index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x15}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 *
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_EnableAbsoluteTimerIRQ
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_AbsTimerIdx
    );


/**
 * \brief   Acknowledges the absolute timer IRQ.
 *
 * This service acknowledges the pending interrupt of an absolute timer.
 * Only a single absolute timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_AbsTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_AbsTimerIdx       Absolute timer index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x17}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 *
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_AckAbsoluteTimerIRQ
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_AbsTimerIdx
    );


/**
 * \brief   Disables the absolute timer IRQ.
 *
 * This service disables the interrupt of an absolute timer.
 * Only a single absolute timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_AbsTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_AbsTimerIdx       Absolute timer index.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x19}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_DisableAbsoluteTimerIRQ
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_AbsTimerIdx
    );


/**
 * \brief   Reads the absolute timer IRQ status.
 *
 * This service reads the interrupt pending status of an absolute timer.
 * Only a single absolute timer is supported.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * If DET is enabled and Fr_1_ERAY_Init() was not called before this service
 * FR_1_ERAY_E_NOT_INITIALIZED is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_CtrlIdx contains an invalid value
 * FR_1_ERAY_E_INV_CTRL_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_AbsTimerIdx contains an invalid value
 * FR_1_ERAY_E_INV_TIMER_IDX is reported to DET and E_NOT_OK returned.
 *
 * If DET is enabled and Fr_IRQStatusPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET and E_NOT_OK returned.
 *
 * \param[in] Fr_CtrlIdx        FlexRay controller index.
 * \param[in] Fr_AbsTimerIdx       Absolute timer index.
 * \param[out] Fr_IRQStatusPtr    Address the timer IRQ status is stored to
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 * \ServiceID{0x20}
 * \Reentrancy{Non Reentrant for same FlexRay controller}
 * \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetAbsoluteTimerIRQStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_AbsTimerIdx,
        P2VAR(boolean,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_IRQStatusPtr
    );

#if (FR_1_ERAY_VERSION_INFO_API == STD_ON)
/* Fr_1_ERAY_GetVersionInfo() API function is enabled */

/** \brief Get version information of the FlexRay Driver.
 *
 * This service returns the version information of this module. The version
 * information includes:
 * - Module Id
 * - Vendor Id
 * - Vendor specific version numbers
 *
 * If DET is enabled and VersionInfoPtr is NULL_PTR,
 * FR_1_ERAY_E_INV_POINTER is reported to DET.
 *
 * \param[out] VersioninfoPtr Pointer where to store the version
 *                                 information of this module.
 *
 * \ServiceID{0x1B}
 * \Reentrancy{Reentrant}
 * \Synchronicity{Synchronous}
 */
extern FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetVersionInfo
    (
        P2VAR(Std_VersionInfoType,AUTOMATIC,FR_1_ERAY_APPL_DATA) VersioninfoPtr
    );
#endif /* FR_1_ERAY_VERSION_INFO_API */

/* stop code section declaration */
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */


#endif /* _FR_1_ERAY_API_H_ */

/*
 * = eof ======================================================================
 */

