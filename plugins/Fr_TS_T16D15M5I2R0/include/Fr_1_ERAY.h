/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/*   
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 19.6 (required)
 *   #undef shall not be used.
 *
 *   Reason:
 *   Macro FR_NO_PBCFG_REQUIRED may be defined in
 *   more than one instance which will cause compile
 *   warning.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

#if !defined _FR_1_ERAY_H_
#define _FR_1_ERAY_H_

/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Api.h>         /* get Fr_1_ERAY API */  /* !LINKSTO Fr.ASR40.FR462_1,1 */

#if (!defined FR_1_ERAY_NO_PBCFG_REQUIRED) && (!defined FR_1_ERAY_NO_CFG_REQUIRED) 
#if (FR_1_ERAY_PBCFGM_SUPPORT_ENABLED == STD_OFF)
#include <Fr_1_ERAY_PBcfg.h>      /* get Fr_1_ERAY post-build configuration */
#endif /* FR_1_ERAY_PBCFGM_SUPPORT_ENABLED == STD_OFF */
#endif /* FR_1_ERAY_NO_PBCFG_REQUIRED, FR_1_ERAY_NO_CFG_REQUIRED*/

/* Deviation MISRA-1 */
#undef FR_1_ERAY_NO_PBCFG_REQUIRED
/* Deviation MISRA-1 */
#undef FR_1_ERAY_NO_CFG_REQUIRED

/******************************************************************************
 Global Macros
******************************************************************************/

/******************************************************************************
 Global Data Types
******************************************************************************/

/******************************************************************************
 Global Data
******************************************************************************/

/******************************************************************************
 Global Function Declarations
******************************************************************************/

#endif /* _FR_1_ERAY_H_ */

/*
 * = eof ======================================================================
 */

