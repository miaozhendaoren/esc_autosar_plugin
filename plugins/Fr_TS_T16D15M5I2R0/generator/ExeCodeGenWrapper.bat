@echo off
set externalGenerator=%1\generator\Fr_1_ERAY_MCG.exe
set outputDir=%2

set moduleConfigs=
:filter_modules
if "%3"=="" (goto call_generator)
for %%i in (Fr,FrIf,Dem) do (
echo.%3 > %outputDir%\Fr_moduleConfigFilter_helper.txt
findstr /c:"%%i_" %outputDir%\Fr_moduleConfigFilter_helper.txt >nul && set moduleConfigs=%moduleConfigs% -c %outputDir%\%%i.epc
del %outputDir%\Fr_moduleConfigFilter_helper.txt
)
shift /3
goto filter_modules


:call_generator
mkdir %outputDir%\dummy_dir_exec_gen_out
mkdir %outputDir%\src
mkdir %outputDir%\include
mkdir %outputDir%\make
mkdir %outputDir%\xgen
%externalGenerator% %moduleConfigs% -o %outputDir%\dummy_dir_exec_gen_out
move %outputDir%\dummy_dir_exec_gen_out\*.c %outputDir%\src
move %outputDir%\dummy_dir_exec_gen_out\*.h %outputDir%\include
move %outputDir%\dummy_dir_exec_gen_out\*.xgen %outputDir%\xgen
move %outputDir%\xgen\Fr_1_ERAY.xgen %outputDir%\xgen\Fr.xgen
rmdir %outputDir%\dummy_dir_exec_gen_out

