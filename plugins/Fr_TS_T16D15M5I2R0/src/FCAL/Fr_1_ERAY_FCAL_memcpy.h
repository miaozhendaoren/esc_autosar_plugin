/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 19.12 (required)
 * "There shall be at most one occurrence of the # or ## preprocessor operators in a single
 * macro definition."
 *
 * Reason:
 * Usage of multiple ## per macro definition improves readability in a high degree.
 * Order of ## examination (even if it was random) has no impact on result.
 *
 * MISRA-2) Deviated Rule: 19.13 (advisory)
 * "The # and ## preprocessor operators should not be used."
 *
 * Reason:
 * String concatenation is used to create efficient readable code.
 *
 */

#if !defined _FR_1_ERAY_FCAL_MEMCPY_H_
#define _FR_1_ERAY_FCAL_MEMCPY_H_

/* Deviation MISRA-1, MISRA-2 <START> */ 

/*
 * copy a range of bytes from the bosch e-ray module to memory as 1:1 copy
 * assume the memory address to be 8-bit aligned, the controller address
 * to be 32-bit aligned
 * avoid byte accesses to the controller
 */
#define FCAL_ERAY_CTRL_TO_MEM_ALIGN8(prefix,h,off,size,addr,ptclass) \
           do \
           { P2VAR(uint8,AUTOMATIC,prefix##_##ptclass) d; \
             uint16_least sz; \
             uint16_least i; \
             d = (P2VAR(uint8,AUTOMATIC,prefix##_##ptclass))(addr); \
             sz = (uint16_least)(size); \
             for(i = (uint16_least)0U; i < (sz &~ (uint16_least)3U); i += (uint16_least)4U) \
             { \
                 MAL_WRITE_U32_STREAM(prefix,d,ptclass,FCAL_ERAY_READ_U32(h,((off)+i))); \
                 d = &d[4]; \
             } \
             if((sz&(uint16_least)3U) != 0U) \
             { \
                 const uint8 *pRemainder; \
                 uint32 Remainder = FCAL_ERAY_READ_U32(h,((off)+i)); \
                 pRemainder = (P2VAR(uint8,AUTOMATIC,AUTOMATIC))(P2VAR(void,AUTOMATIC,AUTOMATIC))&Remainder; \
                 for(i = (uint16_least)0U; i < (sz & (uint16_least)3); i++) \
                 { \
                     *d = *pRemainder; \
                     d = &d[1]; \
                     pRemainder = &pRemainder[1]; \
                 } \
             } \
           } while(0)


/*
 * copy a range of bytes from the bosch e-ray module to memory as partly swapped
 * assume the memory address to be 8-bit aligned, the controller address
 * to be 32-bit aligned
 */
#define FCAL_ERAY_CTRL_TO_MEM_SWAP32_PARTLY_ALIGN8(prefix,h,off,size,addr,ptclass) \
           do \
           { P2VAR(uint8,AUTOMATIC,prefix##_##ptclass) d; \
             uint16_least sz; \
             uint16_least i; \
             d = (P2VAR(uint8,AUTOMATIC,prefix##_##ptclass))(addr); \
             sz = (uint16_least)(size); \
             for(i = (uint16_least)0U;i < (sz &~ (uint16_least)3U); i += (uint16_least)4U) \
             { \
                 MAL_WRITE_U32_STREAM(prefix,d,ptclass,MLIB_SWAP32_PARTLY(FCAL_ERAY_READ_U32(h,(off)+i))); \
                 d = &d[4]; \
             } \
             if((sz & (uint16_least)3U) != 0U) \
             { \
                 const uint8 *pRemainder; \
                 uint32 Remainder = MLIB_SWAP32_PARTLY(FCAL_ERAY_READ_U32(h,(off)+i)); \
                 pRemainder = (P2VAR(uint8,AUTOMATIC,AUTOMATIC))(P2VAR(void,AUTOMATIC,AUTOMATIC))&Remainder; \
                 for(i = (uint16_least)0U; i < (sz & (uint16_least)3U);i++) \
                 { \
                     *d = *pRemainder; \
                     d = &d[1]; \
                     pRemainder = &pRemainder[1]; \
                 } \
             } \
           } while(0)


/*
 * copy a range of bytes from the bosch e-ray module to memory as halfword swapped
 * assume the memory address to be 8-bit aligned, the controller address
 * to be 32-bit aligned
 */
#define FCAL_ERAY_CTRL_TO_MEM_SWAP32_HALFWORD_ALIGN8(prefix,h,off,size,addr,ptclass) \
           do \
           { P2VAR(uint8,AUTOMATIC,prefix##_##ptclass) d; \
             uint16_least sz; \
             uint16_least i; \
             d = (P2VAR(uint8,AUTOMATIC,prefix##_##ptclass))(addr); \
             sz = (uint16_least)(size); \
             for(i = (uint16_least)0U; i < (sz &~ (uint16_least)3U); i += (uint16_least)4U) \
             { \
                 MAL_WRITE_U32_STREAM(prefix,d,ptclass,MLIB_SWAP32_HALFWORD(FCAL_ERAY_READ_U32(h,((off)+i)))); \
                 d = &d[4]; \
             } \
             if((sz & (uint16_least)3U) != 0U) \
             { \
                 const uint8 *pRemainder; \
                 uint32 Remainder = MLIB_SWAP32_HALFWORD(FCAL_ERAY_READ_U32(h,((off)+i))); \
                 pRemainder = (P2VAR(uint8,AUTOMATIC,AUTOMATIC))(P2VAR(void,AUTOMATIC,AUTOMATIC))&Remainder; \
                 for(i = (uint16_least)0U;i < (sz & (uint16_least)3U);i++) \
                 { \
                     *d = *pRemainder; \
                     d = &d[1]; \
                     pRemainder = &pRemainder[1]; \
                 } \
             } \
           } while(0)

/*
 * copy a range of bytes from the bosch e-ray module to memory as swapped32
 * assume the memory address to be 8-bit aligned, the controller address
 * to be 32-bit aligned
 */
#define FCAL_ERAY_CTRL_TO_MEM_SWAP32_ALIGN8(prefix,h,off,size,addr,ptclass) \
           do \
           { P2VAR(uint8,AUTOMATIC,prefix##_##ptclass) d; \
             uint16_least sz; \
             uint16_least i; \
             d = (P2VAR(uint8,AUTOMATIC,prefix##_##ptclass))(addr); \
             sz = (uint16_least)(size); \
             for(i= (uint16_least)0U;i < (sz &~ (uint16_least)3U);i += (uint16_least)4U) \
             { \
                 MAL_WRITE_U32_STREAM(prefix,d,ptclass,MLIB_SWAP32(FCAL_ERAY_READ_U32(h,(off)+i))); \
                 d = &d[4]; \
             } \
             if((sz & (uint16_least)3U) != 0U) \
             { \
                 const uint8 *pRemainder; \
                 uint32 Remainder = MLIB_SWAP32(FCAL_ERAY_READ_U32(h,(off)+i)); \
                 pRemainder = (P2VAR(uint8,AUTOMATIC,AUTOMATIC))(P2VAR(void,AUTOMATIC,AUTOMATIC))&Remainder; \
                 for(i = (uint16_least)0U; i < (sz & (uint16_least)3U);i++) \
                 { \
                     *d = *pRemainder; \
                     d = &d[1]; \
                     pRemainder = &pRemainder[1]; \
                 } \
             } \
           } while(0)


#define FCAL_ERAY_MEM_TO_CTRL_PADDING(h,reg_off,payload_off,buffer_size) \
           do                                                                           \
           {                                                                            \
             const uint16 off = (((uint16)(payload_off) + 3U)&((uint16)(~(uint16)3U)));          \
             const uint16 sz = ((((uint16)(buffer_size) + 3U)&((uint16)(~(uint16)3U)))-(off)); \
             uint16_least i;                                                            \
             for(i = 0U; i < sz; i += 4U)                                               \
             {                                                                          \
                 FCAL_ERAY_WRITE_U32(h,(reg_off) + (off) + i,Fr_1_ERAY_PaddingPattern); \
             }                                                                          \
           } while(0)


/*
 * copy a range of bytes from memory to the bosch e-ray module as 1:1 copy
 * assume the memory address to be 8-bit aligned, the controller address
 * to be 32-bit aligned
 */
#define FCAL_ERAY_MEM_TO_CTRL_ALIGN8(prefix,h,off,size,addr,ptclass) \
           do \
           { P2CONST(uint8,AUTOMATIC,prefix##_##ptclass) s;            \
             uint16_least sz;                                           \
             uint16_least i;                                            \
             uint32 Remainder = Fr_1_ERAY_PaddingPattern;                               \
             P2VAR(uint8,AUTOMATIC,AUTOMATIC) pRemainder;                                         \
             pRemainder = (P2VAR(uint8,AUTOMATIC,AUTOMATIC))(P2VAR(void,AUTOMATIC,AUTOMATIC))&Remainder;                          \
             s = (P2CONST(uint8,AUTOMATIC,prefix##_##ptclass))(addr);  \
             sz = (uint16_least)(size);                                 \
             for(i = (uint16_least)0U; i < (sz &~ (uint16_least)3U); i += (uint16_least)4U)                                 \
             {                                                          \
                 FCAL_ERAY_WRITE_U32(h,(off) + i,MAL_READ_U32_STREAM(prefix,s,ptclass)); \
                 s = &s[4];                                             \
             }                                                          \
             switch((sz & (uint16_least)3U))                            \
             {                                                          \
                 case 3U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    pRemainder = &pRemainder[1];                        \
                    s = &s[1];                                          \
                 }                                                      \
                 /* no break - fall through */                          \
                 case 2U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    pRemainder = &pRemainder[1];                        \
                    s = &s[1];                                          \
                 }                                                      \
                 /* no break - fall through */                          \
                 case 1U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    FCAL_ERAY_WRITE_U32(h,(off)+i,Remainder);             \
                    break;                                              \
                 }                                                      \
                 default:                                               \
                    break;                                              \
             }                                                          \
           } while(0)

/*
 * copy a range of bytes from memory to the bosch e-ray module as partly swapped
 * assume the memory address to be 8-bit aligned, the controller address
 * to be 32-bit aligned
 */
#define FCAL_ERAY_MEM_TO_CTRL_SWAP32_PARTLY_ALIGN8(prefix,h,off,size,addr,ptclass)     \
           do                                                           \
           { P2CONST(uint8,AUTOMATIC,prefix##_##ptclass) s;            \
             uint16_least sz;                                           \
             uint16_least i;                                            \
             uint32 Remainder = Fr_1_ERAY_PaddingPattern;                               \
             P2VAR(uint8,AUTOMATIC,AUTOMATIC) pRemainder;                                         \
             pRemainder = (P2VAR(uint8,AUTOMATIC,AUTOMATIC))(P2VAR(void,AUTOMATIC,AUTOMATIC))&Remainder;                          \
             s = (P2CONST(uint8,AUTOMATIC,prefix##_##ptclass))(addr);  \
             sz = (uint16_least)(size);                                 \
             for(i = (uint16_least)0U; i < (sz &~ (uint16_least)3U);i += (uint16_least)4U) \
             {                                                          \
                 FCAL_ERAY_WRITE_U32(h,(off)+i,MLIB_SWAP32_PARTLY(MAL_READ_U32_STREAM(prefix,s,ptclass))); \
                 s = &s[4];                                             \
             }                                                          \
             switch((sz & (uint16_least)3U))                            \
             {                                                          \
                 case 3U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    pRemainder = &pRemainder[1];                        \
                    s = &s[1];                                          \
                 }                                                      \
                 /* no break - fall through */                          \
                 case 2U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    pRemainder = &pRemainder[1];                        \
                    s = &s[1];                                          \
                 }                                                      \
                 /* no break - fall through */                          \
                 case 1U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    FCAL_ERAY_WRITE_U32(h,(off)+i,MLIB_SWAP32_PARTLY(Remainder));               \
                    break;                                              \
                 }                                                      \
                 default:                                               \
                    break;                                              \
             }                                                          \
           } while(0)

/*
 * copy a range of bytes from memory to the bosch e-ray module as halfword swapped
 * assume the memory address to be 8-bit aligned, the controller address
 * to be 32-bit aligned
 */
#define FCAL_ERAY_MEM_TO_CTRL_SWAP32_HALFWORD_ALIGN8(prefix,h,off,size,addr,ptclass)   \
           do                                                           \
           { P2CONST(uint8,AUTOMATIC,prefix##_##ptclass) s;            \
             uint16_least sz;                                           \
             uint16_least i;                                            \
             uint32 Remainder = Fr_1_ERAY_PaddingPattern;                               \
             P2VAR(uint8,AUTOMATIC,AUTOMATIC) pRemainder;                                         \
             pRemainder = (P2VAR(uint8,AUTOMATIC,AUTOMATIC))(P2VAR(void,AUTOMATIC,AUTOMATIC))&Remainder;                          \
             s = (P2CONST(uint8,AUTOMATIC,prefix##_##ptclass))(addr);  \
             sz = (uint16_least)(size);                                 \
             for(i = (uint16_least)0U; i < (sz &~ (uint16_least)3U);i += (uint16_least)4U) \
             {                                                          \
                 FCAL_ERAY_WRITE_U32(h,(off)+i,MLIB_SWAP32_HALFWORD(MAL_READ_U32_STREAM(prefix,s,ptclass))); \
                 s = &s[4];                                             \
             }                                                          \
             switch((sz & (uint16_least)3U))                            \
             {                                                          \
                 case 3U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    pRemainder = &pRemainder[1];                        \
                    s = &s[1];                                          \
                 }                                                      \
                 /* no break - fall through */                          \
                 case 2U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    pRemainder = &pRemainder[1];                        \
                    s = &s[1];                                          \
                 }                                                      \
                 /* no break - fall through */                          \
                 case 1U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    FCAL_ERAY_WRITE_U32(h,(off)+i,MLIB_SWAP32_HALFWORD(Remainder));               \
                    break;                                              \
                 }                                                      \
                 default:                                               \
                    break;                                              \
             }                                                          \
           } while(0)

/*
 * copy a range of bytes from memory to the bosch e-ray module as swapped32
 * assume the memory address to be 8-bit aligned, the controller address
 * to be 32-bit aligned
 */
#define FCAL_ERAY_MEM_TO_CTRL_SWAP32_ALIGN8(prefix,h,off,size,addr,ptclass)            \
           do                                                           \
           { P2CONST(uint8,AUTOMATIC,prefix##_##ptclass) s;            \
             uint16_least sz;                                           \
             uint16_least i;                                            \
             uint32 Remainder = Fr_1_ERAY_PaddingPattern;                               \
             P2VAR(uint8,AUTOMATIC,AUTOMATIC) pRemainder;                                         \
             pRemainder = (P2VAR(uint8,AUTOMATIC,AUTOMATIC))(P2VAR(void,AUTOMATIC,AUTOMATIC))&Remainder;                          \
             s = (P2CONST(uint8,AUTOMATIC,prefix##_##ptclass))(addr);  \
             sz = (uint16_least)(size);                                 \
             for(i = (uint16_least)0U; i < (sz &~ (uint16_least)3U);i += (uint16_least)4U) \
             {                                                          \
                 FCAL_ERAY_WRITE_U32(h,(off)+i,MLIB_SWAP32(MAL_READ_U32_STREAM(prefix,s,ptclass))); \
                 s = &s[4];                                             \
             }                                                          \
             switch((sz & (uint16_least)3U))                            \
             {                                                          \
                 case 3U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    pRemainder = &pRemainder[1];                        \
                    s = &s[1];                                          \
                 }                                                      \
                 /* no break - fall through */                          \
                 case 2U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    pRemainder = &pRemainder[1];                        \
                    s = &s[1];                                          \
                 }                                                      \
                 /* no break - fall through */                          \
                 case 1U:                                               \
                 {                                                      \
                    *pRemainder = *s;                                   \
                    FCAL_ERAY_WRITE_U32(h,(off)+i,MLIB_SWAP32(Remainder));\
                    break;                                              \
                 }                                                      \
                 default:                                               \
                    break;                                              \
             }                                                          \
           } while(0)

/* Deviation MISRA-1, MISRA-2 <STOP> */ 

#endif  /* _FR_1_ERAY_FCAL_MEMCPY_H_ */

