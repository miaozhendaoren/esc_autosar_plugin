/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

/* check whether the API is enabled or not */
#if (FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */


/**
 * \brief   Reads and resets the FlexRay controllers erorr status.
 *
 * \param Fr_CtrlIdx (in)                FlexRay controller index.
 * \param Fr_ChannelStatusPtr (out)      Address to write the error
 *                                       status to.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetControllerErrorStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ControllerErrorStatusPtr
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_GETCONTROLLERERRORSTATUS_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETCONTROLLERERRORSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETCONTROLLERERRORSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check pointer for being valid */
    else if(Fr_ControllerErrorStatusPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETCONTROLLERERRORSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_GETCONTROLLERERRORSTATUS_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        /* implemented functionality */
        {
            uint16 tmpStatus = 0x00U;

            /* read ERAY status vector register */
            CONST(FCAL_ERAY_RegSegHType,AUTOMATIC) RegEIR_H0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    EIR,
                    H0
                    );

            /* no CHIError detected by ERAY (Bit 0)*/

            /* generate POCError */
            if((RegEIR_H0&((uint16)FCAL_ERAY_EIR_CNA_H0_MASK|
                           (uint16)FCAL_ERAY_EIR_CCL_H0_MASK)) != 0x0U)
            {
                tmpStatus |= FR_1_ERAY_CONTROLLER_POCERROR;
            }

            /* generate ParityError */
            if((RegEIR_H0&(uint16)FCAL_ERAY_EIR_PERR_H0_MASK) != 0x0U)
            {
                tmpStatus |= FR_1_ERAY_CONTROLLER_PARITYERROR;

                /* reset error status (message handler status) */
                FCAL_ERAY_SET(
                        FCAL_ERAY_GetCCHandle(),
                        MHDS,
                        B0,
                        FCAL_ERAY_ALIGN_VALUE(0x1FU,B0));

            }

            /* generate BufferError */
            if((RegEIR_H0&((uint16)FCAL_ERAY_EIR_IIBA_H0_MASK|
                           (uint16)FCAL_ERAY_EIR_IOBA_H0_MASK|
                           (uint16)FCAL_ERAY_EIR_MHF_H0_MASK)) != 0x0U)
            {
                tmpStatus |= FR_1_ERAY_CONTROLLER_BUFFERERROR;

                /* reset error status (message handler constraints flags) */
                FCAL_ERAY_SET(
                        FCAL_ERAY_GetCCHandle(),
                        MHDF,
                        H0,
                        FCAL_ERAY_ALIGN_VALUE(0x13FU,H0));
            }

            if(tmpStatus != 0x0U)
            {
                /* reset error status */
                FCAL_ERAY_SET(
                        FCAL_ERAY_GetCCHandle(),
                        EIR,
                        H0,
                        (((uint16)FCAL_ERAY_EIR_CNA_H0_MASK|
                        (uint16)FCAL_ERAY_EIR_CCL_H0_MASK|
                        (uint16)FCAL_ERAY_EIR_PERR_H0_MASK|
                        (uint16)FCAL_ERAY_EIR_IIBA_H0_MASK|
                        (uint16)FCAL_ERAY_EIR_IOBA_H0_MASK|
                        (uint16)FCAL_ERAY_EIR_MHF_H0_MASK)&RegEIR_H0));
            }

            *Fr_ControllerErrorStatusPtr = tmpStatus;
        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif  /* (FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE == STD_ON) */

