/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* set macro that enables merged compilation macros */
#define TS_MERGED_COMPILE STD_ON

/* !LINKSTO Fr.ASR40.FR074,1 */

/* first of all include all header files, the C-files depend on */
#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/* list of files that include only memory abstraction CODE segments */
/* start code section declaration */
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/* !LINKSTO Fr.ASR40.FR115,1 */
#include "Fr_1_ERAY.c"
#include "Fr_1_ERAY_AbortCommunication.c"
#include "Fr_1_ERAY_AckAbsoluteTimerIRQ.c"
#include "Fr_1_ERAY_AckIRQ.c"
#include "Fr_1_ERAY_AckRelativeTimerIRQ.c"
#include "Fr_1_ERAY_AllowColdstart.c"
#include "Fr_1_ERAY_AllSlots.c"
#include "Fr_1_ERAY_CancelAbsoluteTimer.c"
#include "Fr_1_ERAY_CancelRelativeTimer.c"
#include "Fr_1_ERAY_CheckTxLPduStatus.c"
#include "Fr_1_ERAY_ControllerInit.c"
#include "Fr_1_ERAY_DisableAbsoluteTimerIRQ.c"
#include "Fr_1_ERAY_DisableIRQ.c"
#include "Fr_1_ERAY_DisableLPdu.c"
#include "Fr_1_ERAY_DisableRelativeTimerIRQ.c"
#include "Fr_1_ERAY_EnableAbsoluteTimerIRQ.c"
#include "Fr_1_ERAY_EnableRelativeTimerIRQ.c"
#include "Fr_1_ERAY_GetAbsoluteTimerIRQStatus.c"
#include "Fr_1_ERAY_GetChannelStatus.c"
#include "Fr_1_ERAY_GetClockCorrection.c"
#include "Fr_1_ERAY_GetControllerErrorStatus.c"
#include "Fr_1_ERAY_GetGlobalTime.c"
#include "Fr_1_ERAY_GetIRQStatus.c"
#include "Fr_1_ERAY_GetNmVector.c"
#include "Fr_1_ERAY_GetRelativeTimerIRQStatus.c"
#include "Fr_1_ERAY_GetSyncFrameList.c"
#include "Fr_1_ERAY_GetSyncState.c"
#include "Fr_1_ERAY_GetVersionInfo.c"
#include "Fr_1_ERAY_GetWakeupRxStatus.c"
#include "Fr_1_ERAY_HaltCommunication.c"
#include "Fr_1_ERAY_PrepareLPdu.c"
#include "Fr_1_ERAY_ReceiveRxLPdu.c"
#include "Fr_1_ERAY_ReconfigLPdu.c"
#include "Fr_1_ERAY_SendMTS.c"
#include "Fr_1_ERAY_SendWUP.c"
#include "Fr_1_ERAY_SetAbsoluteTimer.c"
#include "Fr_1_ERAY_SetExtSync.c"
#include "Fr_1_ERAY_SetRelativeTimer.c"
#include "Fr_1_ERAY_SetWakeupChannel.c"
#include "Fr_1_ERAY_StartCommunication.c"

/* stop code section declaration */
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

#include "Fr_1_ERAY_Init.c"
#include "Fr_1_ERAY_EnableIRQ.c"
#include "Fr_1_ERAY_CheckMTS.c"
#include "Fr_1_ERAY_GetPOCStatus.c"
#include "Fr_1_ERAY_TransmitTxLPdu.c"

