/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/

#define Fr_1_ERAY_GetFreeze(CCSV_W0,POCStatusPtr) \
    ((POCStatusPtr)->Freeze = FCAL_ERAY_TEST_BIT((CCSV_W0),CCSV,FSI,W0) ? TRUE : FALSE)

#define Fr_1_ERAY_GetCHIHaltRequest(CCSV_W0,POCStatusPtr) \
    ((POCStatusPtr)->CHIHaltRequest = FCAL_ERAY_TEST_BIT((RegCCSV_W0),CCSV,HRQ,W0) ? TRUE : FALSE)

#define Fr_1_ERAY_GetColdstartNoise(CCSV_W0,POCStatusPtr) \
    ((POCStatusPtr)->ColdstartNoise = FCAL_ERAY_TEST_BIT((RegCCSV_W0),CCSV,CSNI,W0) ? TRUE : FALSE)

#define Fr_1_ERAY_GetCHIReadyRequest(POCStatusPtr) \
    ((POCStatusPtr)->CHIReadyRequest = FALSE)

/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/

/* start constant section declaration */
#define FR_1_ERAY_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

#define FR_1_ERAY_WAKEUPSTATUSMAPPINGTABLE_SIZE 8U
#define FR_1_ERAY_STARTUPSTATEMAPPINGTABLE_SIZE 16U
#define FR_1_ERAY_POCMAPPINGTABLE_SIZE 8U

/* Mapping table that maps the Subtate to a POC state in case
 * Main-State is 0
 */
STATIC CONST(uint8,FR_1_ERAY_CONST) POCMappingTable[FR_1_ERAY_POCMAPPINGTABLE_SIZE] =
    {
        (VAR(uint8,AUTOMATIC))FR_POCSTATE_DEFAULT_CONFIG,
        (VAR(uint8,AUTOMATIC))FR_POCSTATE_READY,
        (VAR(uint8,AUTOMATIC))FR_POCSTATE_NORMAL_ACTIVE,
        (VAR(uint8,AUTOMATIC))FR_POCSTATE_NORMAL_PASSIVE,
        (VAR(uint8,AUTOMATIC))FR_POCSTATE_HALT,
        (VAR(uint8,AUTOMATIC))FR_POCSTATE_NORMAL_PASSIVE,
        (VAR(uint8,AUTOMATIC))FR_POCSTATE_DEFAULT_CONFIG,
        (VAR(uint8,AUTOMATIC))FR_POCSTATE_DEFAULT_CONFIG,
    };

/* Mapping table that maps the Subtate to a Startup State in case
 * Main-State is 'Startup'
 */
STATIC CONST(uint8,FR_1_ERAY_CONST) StartupStateMappingTable[FR_1_ERAY_STARTUPSTATEMAPPINGTABLE_SIZE] =
    {
        (VAR(uint8,AUTOMATIC))FR_STARTUP_UNDEFINED, /* STARTUP_PREPARE */
        (VAR(uint8,AUTOMATIC))FR_STARTUP_COLDSTART_LISTEN,
        (VAR(uint8,AUTOMATIC))FR_STARTUP_COLDSTART_COLLISION_RESOLUTION,
        (VAR(uint8,AUTOMATIC))FR_STARTUP_COLDSTART_CONSISTENCY_CHECK,
        (VAR(uint8,AUTOMATIC))FR_STARTUP_COLDSTART_GAP,
        (VAR(uint8,AUTOMATIC))FR_STARTUP_COLDSTART_JOIN,
        (VAR(uint8,AUTOMATIC))FR_STARTUP_INTEGRATION_COLDSTART_CHECK,
        (VAR(uint8,AUTOMATIC))FR_STARTUP_INTEGRATION_LISTEN,
        (VAR(uint8,AUTOMATIC))FR_STARTUP_INTEGRATION_CONSISTENCY_CHECK,
        (VAR(uint8,AUTOMATIC))FR_STARTUP_INITIALIZE_SCHEDULE,
        (VAR(uint8,AUTOMATIC))FR_STARTUP_UNDEFINED, /* ABORT_STARTUP */
        (VAR(uint8,AUTOMATIC))FR_STARTUP_UNDEFINED, /* STARTUP_SUCCESS */
        (VAR(uint8,AUTOMATIC))FR_STARTUP_UNDEFINED, /* Invalid value */
        (VAR(uint8,AUTOMATIC))FR_STARTUP_UNDEFINED, /* Invalid value */
        (VAR(uint8,AUTOMATIC))FR_STARTUP_UNDEFINED, /* Invalid value */
        (VAR(uint8,AUTOMATIC))FR_STARTUP_UNDEFINED, /* Invalid value */
    };

/* Mapping table for the Wakeup Status bit field (CCSV->WSV) */
STATIC CONST(uint8,FR_1_ERAY_CONST) WakeupStatusMappingTable[FR_1_ERAY_WAKEUPSTATUSMAPPINGTABLE_SIZE] =
    {
        (VAR(uint8,AUTOMATIC))FR_WAKEUP_UNDEFINED,
        (VAR(uint8,AUTOMATIC))FR_WAKEUP_RECEIVED_HEADER,
        (VAR(uint8,AUTOMATIC))FR_WAKEUP_RECEIVED_WUP,
        (VAR(uint8,AUTOMATIC))FR_WAKEUP_COLLISION_HEADER,
        (VAR(uint8,AUTOMATIC))FR_WAKEUP_COLLISION_WUP,
        (VAR(uint8,AUTOMATIC))FR_WAKEUP_COLLISION_UNKNOWN,
        (VAR(uint8,AUTOMATIC))FR_WAKEUP_TRANSMITTED,
        (VAR(uint8,AUTOMATIC))FR_WAKEUP_UNDEFINED
    };

/* stop constant section declaration */
#define FR_1_ERAY_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/******************************************************************************
 Local Functions
******************************************************************************/

/* start code section declaration */
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/**
 * \brief   Sets POC error mode to output variable Fr_POCStatusPtr
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param CCEV_B0 (in)               Current value of register CCEV.
 * \param Fr_POCStatusPtr (out)      Address to store the status to.
 *
 */
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetErrorMode
    (
        uint8 Fr_CtrlIdx,
        FCAL_ERAY_RegSegWType CCEV_B0,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    );
    
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetErrorMode
    (
        uint8 Fr_CtrlIdx,
        FCAL_ERAY_RegSegWType CCEV_B0,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    )
{
    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* get error mode and MAP to enumerator */
    switch(FCAL_ERAY_GET_BIT(CCEV_B0,CCEV,ERRM,B0))
    {
        case FCAL_ERAY_CCEV_ERRM_PASSIVE:
        {
            Fr_POCStatusPtr->ErrorMode = FR_ERRORMODE_PASSIVE;
            break;
        }
        case FCAL_ERAY_CCEV_ERRM_COMM_HALT:
        {
            Fr_POCStatusPtr->ErrorMode = FR_ERRORMODE_COMM_HALT;
            break;
        }
        /* case FCAL_ERAY_CCEV_ERRM_ACTIVE: */
        default:
        {
            Fr_POCStatusPtr->ErrorMode = FR_ERRORMODE_ACTIVE;
            break;
        }
    }
}

/**
 * \brief   Sets POC slot mode to output variable Fr_POCStatusPtr
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param CCSV_W0 (in)               Current value of register CCSV.
 * \param Fr_POCStatusPtr (out)      Address to store the status to.
 *
 */
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetSlotMode
    (
        uint8 Fr_CtrlIdx,
        FCAL_ERAY_RegSegWType CCSV_W0,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    );
    
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetSlotMode
    (
        uint8 Fr_CtrlIdx,
        FCAL_ERAY_RegSegWType CCSV_W0,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    )
{
    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* write slot-mode information (map register to enumerator value)*/
    switch(FCAL_ERAY_GET_BIT(CCSV_W0,CCSV,SLM,W0))
    {
        case FCAL_ERAY_CCSV_SLM_ALL_PENDING:
        {
            Fr_POCStatusPtr->SlotMode = FR_SLOTMODE_ALL_PENDING;
            break;
        }
        case FCAL_ERAY_CCSV_SLM_SINGLE:
        {
            Fr_POCStatusPtr->SlotMode = FR_SLOTMODE_SINGLE;
            break;
        }
        /* case FCAL_ERAY_CCSV_SLM_ALL: */
        default:
        {
            Fr_POCStatusPtr->SlotMode = FR_SLOTMODE_ALL;
            break;
        }
    }
}

/**
 * \brief   Sets POC wakeup status to output variable Fr_POCStatusPtr
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param CCSV_W0 (in)               Current value of register CCSV.
 * \param Fr_POCStatusPtr (out)      Address to store the status to.
 *
 */
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetWakeupStatus
    (
        uint8 Fr_CtrlIdx,
        FCAL_ERAY_RegSegWType CCSV_W0,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    );
    
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetWakeupStatus
    (
        uint8 Fr_CtrlIdx,
        FCAL_ERAY_RegSegWType CCSV_W0,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    )
{
    CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) WSV =
        FCAL_ERAY_GET_BIT(CCSV_W0,CCSV,WSV,W0);
            
    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* map Wakeup Status from ERAY representation to API representation */
    Fr_POCStatusPtr->WakeupStatus =
        (Fr_WakeupStatusType) WakeupStatusMappingTable[WSV];
}


/**
 * \brief   Sets POC and startup state to output variable Fr_POCStatusPtr
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param POCS (in)                  Current value of register POCS.
 * \param Fr_POCStatusPtr (out)      Address to store the status to.
 *
 */
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetPOCAndStartupState
    (
        uint8 Fr_CtrlIdx,
        FCAL_ERAY_RegSegWType POCS,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    );
    
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetPOCAndStartupState
    (
        uint8 Fr_CtrlIdx,
        FCAL_ERAY_RegSegWType POCS,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    )
{
    /* extract Main-State out of POC-State variable */
    CONST(uint8,AUTOMATIC) POCState = (VAR(uint8,AUTOMATIC))((POCS>>4U)&(0x03U));

    /* extract Sub-State out of POC-State variable */
    CONST(uint8,AUTOMATIC) SubState = (VAR(uint8,AUTOMATIC))(POCS&0x0FU);
    
    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* evaluate Main-State */
    switch(POCState)
    {
        /* FlexRay CC is in Wakeup */
        case 1:
        {
            /* set POC-State Wakeup */
            Fr_POCStatusPtr->State = FR_POCSTATE_WAKEUP;

            /* set Startup Sub-State to Undefined */
            Fr_POCStatusPtr->StartupState = FR_STARTUP_UNDEFINED;
            break;
        }
        /* FlexRay CC is in Startup */
        case 2:
        {
            /* set POC-State Startup */
            Fr_POCStatusPtr->State = FR_POCSTATE_STARTUP;

            /* set Startup Sub-State */
            Fr_POCStatusPtr->StartupState =
                (Fr_StartupStateType)StartupStateMappingTable[SubState];
            break;
        }
        /* case 0: */
        default:
        {
            /* Set startup sub-state to undefined if POC-state is not 'Startup' */
            Fr_POCStatusPtr->StartupState = FR_STARTUP_UNDEFINED;

            /* Test for config state */
            if(SubState == 0x0F)
            {
                Fr_POCStatusPtr->State = FR_POCSTATE_CONFIG;
            }
            /* if state fits into translation table - then perform mapping */
            else
            {
                Fr_POCStatusPtr->State = (Fr_POCStateType)POCMappingTable[(SubState&0x7U)];
            }
            break;

        }
    }
}

/******************************************************************************
 Global Functions
******************************************************************************/



/**
 * \brief   Reads FlexRay CC POC-status.
 *
 * \param Fr_CtrlIdx (in)            FlexRay controller index.
 * \param Fr_POCStatusPtr (out)      Address the POC-status is written to.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetPOCStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(Fr_POCStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_POCStatusPtr
    )
{
    Std_ReturnType retval = E_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_GETPOCSTATUS_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETPOCSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETPOCSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check output pointer for beeing valid */
    else if(Fr_POCStatusPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETPOCSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else

#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */
    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_GETPOCSTATUS_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        /* implemented functionality */
        {

            /* read ERAY->CCSV */
            CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) RegCCSV_W0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    CCSV,
                    W0
                    );

            /* read ERAY->CCEV */
            CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegCCEV_B0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    CCEV,
                    B0
                    );
                    
            /*
             * a local variable is used to store the POC Status temporarily
             * to avoid writing to the output parameter in case of errors
             */
            VAR(Fr_POCStatusType,AUTOMATIC) TmpPOCStatus;
            
            /* extract the Wakeup Status out of the CCSV register value */
                    /* temporary to store POC state to */
            VAR(FCAL_ERAY_RegSegWType,AUTOMATIC) POCS;

            
            Fr_1_ERAY_GetFreeze(RegCCSV_W0, &TmpPOCStatus);
            Fr_1_ERAY_GetCHIHaltRequest(RegCCSV_W0,&TmpPOCStatus);
            Fr_1_ERAY_GetColdstartNoise(RegCCSV_W0,&TmpPOCStatus);
            Fr_1_ERAY_GetCHIReadyRequest(&TmpPOCStatus);
            
            if(TmpPOCStatus.Freeze)
            {
                /* read protocol operation control status log */
                POCS = FCAL_ERAY_GET_BIT(RegCCSV_W0,CCSV,PSL,W0);

            }
            else
            {
                /* read protocol operation control status */
                POCS = FCAL_ERAY_GET_BIT(RegCCSV_W0,CCSV,POCS,W0);
            }
                
            Fr_1_ERAY_GetPOCAndStartupState(FR_1_ERAY_CTRLIDX, POCS, &TmpPOCStatus);
            Fr_1_ERAY_GetWakeupStatus(FR_1_ERAY_CTRLIDX, RegCCSV_W0,&TmpPOCStatus);
            Fr_1_ERAY_GetSlotMode(FR_1_ERAY_CTRLIDX, RegCCSV_W0,&TmpPOCStatus);
            Fr_1_ERAY_GetErrorMode(FR_1_ERAY_CTRLIDX, RegCCEV_B0,&TmpPOCStatus);
            /* write output parameter */
            *Fr_POCStatusPtr = TmpPOCStatus;
        }
    }

    /* Return function status OK */
    return retval;
}

/* stop code section declaration */
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

