/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */


/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */
/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Data
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

/* check whether this API service is enabled or not */
#if (FR_1_ERAY_PREPARELPDU_API_ENABLE == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

/**
 * \brief   Prepares a LPdu for transmission/reception
 *          (e.g. configures a buffer for a certain LPdu).
 *
 * \param Fr_CtrlIdx (in)      FlexRay controller index.
 * \param Fr_LPduIdx (in)      LPdu index of PDU to prepare.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_PrepareLPdu
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_PREPARELPDU_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_PREPARELPDU_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_PREPARELPDU_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check whether configuration has an entry for this FlexRay CC Idx */
    else if(FR_1_ERAY_CTRLIDX >= gFr_1_ERAY_ConfigPtr->nNumCtrl)
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_PREPARELPDU_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;

    }

    /* check that the virtual buffer index is within bounds */
    else if(Fr_LPduIdx >= ((&FR_1_ERAY_GET_CONFIG_ADDR(Fr_1_ERAY_CtrlCfgType,
                                                  gFr_1_ERAY_ConfigPtr->pCtrlCfg)
                                                  [FR_1_ERAY_CTRLIDX])->nNumLPdus))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_LPDU_IDX, FR_1_ERAY_PREPARELPDU_SERVICE_ID);

        /* Return negative status code */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);


        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_PREPARELPDU_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        /* implementation of funcionality */
        {
            /* get pointer to controller configuration */
            CONSTP2CONST(Fr_1_ERAY_CtrlCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCtrlCfg =
                 &FR_1_ERAY_GET_CONFIG_ADDR
                (
                    Fr_1_ERAY_CtrlCfgType,
                    gFr_1_ERAY_ConfigPtr->pCtrlCfg
                )[FR_1_ERAY_CTRLIDX];

            /* get buffer configuration data structure */
            CONSTP2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu =
                &FR_1_ERAY_GET_CONFIG_ADDR
                (
                    Fr_1_ERAY_BufferCfgType,
                    pCtrlCfg->pLPduCfg
                )[Fr_LPduIdx];

            /* get config parameter nFIDExt2 into local variable (since used several times) */
            CONST(uint16_least,AUTOMATIC) nFIDExt2 = pLPdu->nFID_Ext2;

            /* yes it isn't - load init values from config */
            CONST(uint16_least,AUTOMATIC) nCRCExt1 = pLPdu->nCRC_Ext1;

            /* variables holding the buffer configuration register values */
            /* ERAY WRHS1 register variable */
            CONST(uint32,AUTOMATIC) RegWRHS1_W0 =
                FR_1_ERAY_GET_WRHS1(nCRCExt1,nFIDExt2,pLPdu->nCycle);

            /* ERAY WRHS2 register variable */
            CONST(uint32,AUTOMATIC) RegWRHS2_W0 =
                FR_1_ERAY_GET_WRHS2(nCRCExt1,(((uint32)pLPdu->nPayloadLength + 1U) / 2U));

            /* ERAY WRHS3 register variable */
            CONST(uint32,AUTOMATIC) RegWRHS3_W0 =
                FR_1_ERAY_GET_WRHS3(pLPdu->nDataPointer);

            /* if the MCG didn't decide to reconfigure this message buffer */
            if(FR_1_ERAY_GET_LPDUMODE(nFIDExt2) != FR_1_ERAY_LPDUMODE_STATIC_NORMAL)
            {

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

                /* is this not a statically reconfigurable buffer */
                if(FR_1_ERAY_GET_LPDUMODE(nFIDExt2) != FR_1_ERAY_LPDUMODE_STATIC_RECONFIG)
                {
                    /* Report to DET */
                    FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_LPDU_IDX, FR_1_ERAY_PREPARELPDU_SERVICE_ID);

                    /* Return negative status code */
                    retval = E_NOT_OK;
                }
                else
#endif /* FR_1_ERAY_DEV_ERROR_DETECT */
                {
                    /* finally, initialize the message buffer */
                    retval = Fr_1_ERAY_ConfigBuffer(FR_1_ERAY_CTRLIDX,
                                                    pLPdu->nBufferCfgIdx,
                                                    RegWRHS1_W0,
                                                    RegWRHS2_W0,
                                                    RegWRHS3_W0,
                                                    FR_1_ERAY_PREPARELPDU_SERVICE_ID);
                }
            }
        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif /* FR_1_ERAY_PREPARELPDU_API_ENABLE */

