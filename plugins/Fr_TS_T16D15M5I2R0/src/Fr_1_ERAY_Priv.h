/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

#if !defined _FR_1_ERAY_PRIV_H_
#define _FR_1_ERAY_PRIV_H_

/******************************************************************************
 Include Section
******************************************************************************/

#include <TSAutosar.h> /* TS_PARAM_UNUSED() */
 /* !LINKSTO Fr.ASR40.FR462_1,1 */
#include <Fr_1_ERAY_Api.h>     /* get Fr types and function prototypes          */
#define TS_RELOCATABLE_CFG_ENABLE FR_1_ERAY_RELOCATABLE_CFG_ENABLE
#include <TSPBConfig_Access.h>


#include <Fr_1_ERAY_LocalSrcCfg.h>  /* get source pre-compile time configuration data */

/******************************************************************************
 Version Check
******************************************************************************/

/******************************************************************************
 Check private Pre-compile time switches
******************************************************************************/

/* Checking pre-compile time value FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT  */
#if !defined FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT
#error FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT not defined
#endif /* FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT */


/* Checking pre-compile time value "FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT" */
#if !defined FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT
#error FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT not defined
#endif  /* FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT */


/* Checking pre-compile time value "FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT" */
#if !defined FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT
#error FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT not defined
#endif  /* FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT */


/* Checking pre-compile time value "FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE" */
#if !defined FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE
#error FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE not defined
#endif  /* FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE */

/* test for valid value of FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE */
#if (FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE != STD_ON) && \
    (FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE != STD_OFF)
#error macro FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE has an invalid value
#endif /* FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE */


/* Checking pre-compile time value "FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE" */
#if !defined FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE
#error FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE not defined
#endif  /* FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE */

/* test for valid value of FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE */
#if (FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE != STD_ON) && \
    (FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE != STD_OFF)
#error macro FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE has an invalid value
#endif /* FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE */


/* Checking pre-compile time value FR_1_ERAY_REG_EILS_VALUE */
#if !defined FR_1_ERAY_REG_EILS_VALUE
#error FR_1_ERAY_REG_EILS_VALUE not defined
#endif /* FR_1_ERAY_REG_EILS_VALUE */


/* Checking pre-compile time value FR_1_ERAY_REG_SILS_VALUE */
#if !defined FR_1_ERAY_REG_SILS_VALUE
#error FR_1_ERAY_REG_SILS_VALUE not defined
#endif /* FR_1_ERAY_REG_SILS_VALUE */


/* Checking pre-compile time value FR_1_ERAY_REG_ILE_VALUE */
#if !defined FR_1_ERAY_REG_ILE_VALUE
#error FR_1_ERAY_REG_ILE_VALUE not defined
#endif /* FR_1_ERAY_REG_ILE_VALUE */


/* Checking pre-compile time value FR_1_ERAY_GLOBAL_TIME_SKIP_MT0_TIMEOUT */
#if !defined FR_1_ERAY_GLOBAL_TIME_SKIP_MT0_TIMEOUT
#error FR_1_ERAY_GLOBAL_TIME_SKIP_MT0_TIMEOUT not defined
#endif /* FR_1_ERAY_GLOBAL_TIME_SKIP_MT0_TIMEOUT */


/* Checking pre-compile time value "FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE" */
#if !defined FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE
#error FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE not defined
#endif  /* FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE */

/* test for valid value of FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE */
#if (FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE != STD_ON) && \
    (FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE != STD_OFF)
#error macro FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE has an invalid value
#endif /* FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE */


/******************************************************************************
 Global Macros
******************************************************************************/

#if (defined FR_1_ERAY_GET_CONFIG_ADDR) /* to prevent double declaration */
#error FR_1_ERAY_GET_CONFIG_ADDR already defined
#endif /* FR_1_ERAY_GET_CONFIG_ADDR */

/** \brief Define FR_1_ERAY_GET_CONFIG_ADDR macro for getting a reference to a config element, */
#define FR_1_ERAY_GET_CONFIG_ADDR(type, offset) \
TS_UNCHECKEDGETCFG(gFr_1_ERAY_ConfigPtr, type, FR_1_ERAY, offset)


#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)
#include <Det.h> /* Development error tracer interface */

/*
 * Macro for simplified Det usage - map to Det-API function call
 */
#define FR_1_ERAY_DET_REPORTERROR(nErrorID, nFuncID) \
    FR_1_ERAY_DET_REPORTERROR_HLP(nErrorID, nFuncID)
#define FR_1_ERAY_DET_REPORTERROR_HLP(nErrorID, nFuncID)               \
    (void)Det_ReportError(                                             \
        ((VAR(uint16,AUTOMATIC)) FR_1_ERAY_MODULE_ID),                 \
        ((VAR(uint8,AUTOMATIC)) 0U),                                   \
        ((VAR(uint8,AUTOMATIC)) (nFuncID)),                            \
        ((VAR(uint8,AUTOMATIC)) (nErrorID))                            \
        )
#elif (FR_1_ERAY_DEV_ERROR_DETECT == STD_OFF)
/**
 * macro for simplified DET usage - map to empty statement
 */
#define FR_1_ERAY_DET_REPORTERROR(nErrorID, nFuncID)
#endif /* (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON) */



/* define DEM handling macros if at least a single event DEM reporting is enabled */
#if (FR_1_ERAY_PROD_ERR_HANDLING_CTRL_TEST == TS_PROD_ERR_REP_TO_DEM)

#include <Dem.h>             /* include DEM interface */

/**
 * function-like macro for simplified DEM usage - map to Dem - API
 * function call passing compile-time error id
 */
#define FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(ApiId) \
    do { \
        CONSTP2CONST(Fr_1_ERAY_CtrlCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCtrlCfgLocal = \
             &FR_1_ERAY_GET_CONFIG_ADDR \
            ( \
                Fr_1_ERAY_CtrlCfgType, \
                gFr_1_ERAY_ConfigPtr->pCtrlCfg \
            )[FR_1_ERAY_CTRLIDX]; \
        if(pCtrlCfgLocal->DemCtrlTestResultId != 0U) \
        { \
            (void) Dem_ReportErrorStatus((Dem_EventIdType) pCtrlCfgLocal->DemCtrlTestResultId, DEM_EVENT_STATUS_FAILED); \
        } \
    }while(0)

#elif (FR_1_ERAY_PROD_ERR_HANDLING_CTRL_TEST == TS_PROD_ERR_REP_TO_DET) && (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

#define FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(ApiId) \
    FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_DEMTODET_CTRL_TEST,(ApiId))

#else /* (FR_1_ERAY_PROD_ERR_HANDLING_CTRL_TEST == TS_PROD_ERR_DISABLE) */

#define FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(ApiId) \
    do{ }while(0)
        
#endif  /* (FR_1_ERAY_PROD_ERR_HANDLING_CTRL_TEST == TS_PROD_ERR_REP_TO_DEM) */

/********************** POC-State handling macros ************************/


/* busy wait loop including reset timeout check */
/* this lopp is just present if timeout > 0 */
#if (FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT > 0U)

#define Fr_1_ERAY_WaitWhileCmdBusyReset(CtrlIdx,ApiId) \
        Fr_1_ERAY_WaitWhileCmdBusy(CtrlIdx,FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT,ApiId)

#else /* (FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT > 0) */

#define Fr_1_ERAY_WaitWhileCmdBusyReset(CtrlIdx,ApiId)

#endif /* (FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT > 0) */


#if (FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT > 0U)

#define Fr_1_ERAY_WaitWhileCmdBusyStandard(CtrlIdx,ApiId) \
        Fr_1_ERAY_WaitWhileCmdBusy(CtrlIdx,FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT,ApiId)

#else /* (FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT > 0) */

#define Fr_1_ERAY_WaitWhileCmdBusyStandard(CtrlIdx,ApiId)

#endif /* (FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT > 0) */


/************************** Single controller optimization macros *************************/
/* only the macros here must be used for addressing the drivers hardware ressources */

/* single controller optimization is enabled */
#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

/* replace all indexing macros by a constant index - indexing parameters are not used */
#define FR_1_ERAY_CTRLIDX (0)

#elif (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_OFF)

/* replace all indexing macros by function parameter names */
#define FR_1_ERAY_CTRLIDX (Fr_CtrlIdx)

#endif /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

/* buffer configuration access macros */
/* extract WRHS1 information out of buffer configuration */
#define FR_1_ERAY_GET_WRHS1(nCRC_Ext1,nFID_Ext2,nCycle)      \
            ((uint32)(((uint32)(nFID_Ext2))&(uint32)0x000007FFUL)|           \
            ((uint32)((uint32)(nCycle))<<16U)|                       \
            ((uint32)(((uint32)(nCRC_Ext1))<<12U)&(uint32)0x0F000000UL)|     \
            ((uint32)(((uint32)(nFID_Ext2))<<16U)&(uint32)0x30000000UL))     \

/* extract WRHS2 information out of buffer configuration */
#define FR_1_ERAY_GET_WRHS2(nCRC_Ext1,nPayloadLength)        \
            ((((uint32)(nCRC_Ext1)&(uint32)0x000007FFUL))|           \
            ((uint32)(((uint32)(nPayloadLength))<<16U)))               \

/* extract WRHS3 information out of buffer configuration */
#define FR_1_ERAY_GET_WRHS3(nDataPointer)                        \
            (((uint32)(nDataPointer)))


#define FR_1_ERAY_GET_LPDUMODE(nFID_Ext2) \
            (((nFID_Ext2)>>14U)&0x3U)
/**< Macro returns configured LPdu Mode
 */

#define FR_1_ERAY_GET_NOINIT(nFID_Ext2) \
            (((nFID_Ext2)>>11U)&0x1U)
/**< Macro returns configured NoInit-Bit
 */


#define FR_1_ERAY_INVALID_MSGBUFFER_INDEX        (0xFFU)
/**< invalid (dummy) message buffer index */

#define FR_1_ERAY_LPDUMODE_STATIC_NORMAL         (0U)
/**< LPdu to buffer assignment is 1:1 */

#define FR_1_ERAY_LPDUMODE_STATIC_RECONFIG       (1U)
/**< LPdu to buffer assignment is n:1 */

#define FR_1_ERAY_LPDUMODE_DYNAMIC_PAYLOADLENGTH (2U)
/**< LPdu to buffer assignment is 1:1, payload is dynamically reconfigured */

#define FR_1_ERAY_LPDUMODE_DYNAMIC_RECONFIG      (3U)
/**< LPdu to buffer assignment is 1:1, buffer is dynamically reconfigured */


#define FR_1_ERAY_CFG_OPTION_NMVECTOR_LENGTH(optionfield) \
            ((optionfield)&((VAR(uint8,AUTOMATIC))0x0FU))
/**< Macro returns configured NM-Vector length when passing nConfigOptions
 */


/******************************************************************************
 Global Data Types
******************************************************************************/

#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

/**
 * This type is used to define the initialization status of Fr_1_ERAY
 */
typedef enum
{
    FR_1_ERAY_UNINIT = 0, /**< Fr_1_ERAY is not initialized or not usable. */
    FR_1_ERAY_INIT /**< Fr_1_ERAY is initialized and usable. */
} Fr_1_ERAY_InitStatusType;

#endif /* FR_1_ERAY_DEV_ERROR_DETECT */

/******************************************************************************
 Global Data
******************************************************************************/

#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

#define FR_1_ERAY_START_SEC_VAR_INIT_8
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/**
 * global variable for the actual initialization status of the Fr_1_ERAY
 */
extern VAR(Fr_1_ERAY_InitStatusType,  FR_1_ERAY_VAR_FAST) Fr_1_ERAY_InitStatus;

#define FR_1_ERAY_STOP_SEC_VAR_INIT_8
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

#endif /* FR_1_ERAY_DEV_ERROR_DETECT */

#define FR_1_ERAY_START_SEC_VAR_FAST_INIT_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/**
 * global variable for the pointer to teh config of Fr_1_ERAY
 */
extern P2CONST(Fr_1_ERAY_ConfigType, FR_1_ERAY_VAR_FAST, FR_1_ERAY_APPL_CONST) gFr_1_ERAY_ConfigPtr;

#define FR_1_ERAY_STOP_SEC_VAR_FAST_INIT_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */


#define FR_1_ERAY_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

extern CONST(uint32,AUTOMATIC) Fr_1_ERAY_IRQIndexToBitMapping[FR_1_ERAY_IRQ_NUM];

#define FR_1_ERAY_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/******************************************************************************
 Global Function Declarations
******************************************************************************/

/* start code section declaration */
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */



/**
 * \brief   Configures a single ERAY Buffer
 *
 * \param Fr_CtrlIdx (in)            FlexRay controller index.
 * \param nBufIdx (in)               Message buffer Index.
 * \param nWRHS1 (in)                Content for register WRHS1.
 * \param nWRHS2 (in)                Content for register WRHS2.
 * \param nWRHS3 (in)                Content for register WRHS3.
 * \param Fr_ApiId (in)              API Id which is source of the call.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 * \note    This function can be called in POC-state 'ready' only.
 * \note    This function leaves the FlexRay CC in POC-state "ready".
 */
TS_MOD_PRIV_DECL FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ConfigBuffer
    (
        uint8  Fr_CtrlIdx,
        uint8  nBufIdx,
        uint32 nWRHS1,
        uint32 nWRHS2,
        uint32 nWRHS3,
        uint8 ApiId
    );
    

/**
 * \brief   Sets a FlexRay Controller CHI command, while preserving all oth bits in the register.
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller to initialize.
 * \param Fr_CHICmd  (in)            CHI command to perform.
 * \param Fr_ServiceId (in)          Service ID of the function this function was called from.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */
TS_MOD_PRIV_DECL FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CHICommandPreserveOtherBits
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_CHICmd,
        VAR(uint8,AUTOMATIC) Fr_ServiceId
    );


/**
 * \brief   Apply the CHI command passed in Fr_CHICmd to controller Fr_CtrlIdx
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller to initialize.
 * \param Fr_CHICmd (in)             ERAY CHI command to apply.
 * \param Fr_ServiceId (in)          Service ID of the function this function was called from.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */
TS_MOD_PRIV_DECL FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CHICommand
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_CHICmd,
        VAR(uint8,AUTOMATIC) Fr_ServiceId
    );


/* busy wait loop including timeout check */
/* this lopp is just present if timeout > 0 */
#if (FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT > 0U) || (FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT > 0U)

/**
 * \brief   Waits while the FlexRay CC is busy executing a command
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param Fr_ApiId (in)              API Id which is source of the call.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed (timeout while waiting).
 */
TS_MOD_PRIV_DECL FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_WaitWhileCmdBusy
    (
        uint8 Fr_CtrlIdx,
        uint16 Fr_Timeout,
        uint8 Fr_ApiId
    );

#endif /* (FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT > 0U) || (FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT > 0U) */


#if (FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT > 0U)

/**
 * \brief   Waits while the FlexRay CC Input Buffer is busy
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param Fr_ApiId (in)              API Id which is source of the call.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed (timeout while waiting).
 */
TS_MOD_PRIV_DECL FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_WaitWhileIBFBusy
    (
        uint8 Fr_CtrlIdx,
        uint8 Fr_ApiId
    );
      
/**
 * \brief   Waits while the FlexRay CC Output Buffer is busy
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param Fr_ApiId (in)              API Id which is source of the call.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed (timeout while waiting).
 */
TS_MOD_PRIV_DECL FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_WaitWhileOBFBusy
    (
        uint8 Fr_CtrlIdx,
        uint8 Fr_ApiId
    );

#else /* (FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT > 0U) */

#define Fr_1_ERAY_WaitWhileIBFBusy(CtrlIdx, ApiId)
#define Fr_1_ERAY_WaitWhileOBFBusy(CtrlIdx, ApiId)

#endif /* (FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT > 0U) */

/* start code section declaration */
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */



#endif  /* _FR_1_ERAY_PRIV_H_ */

/*
 * = eof ======================================================================
 */

