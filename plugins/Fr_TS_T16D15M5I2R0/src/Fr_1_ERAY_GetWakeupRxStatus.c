/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

/* check whether this API service is enabled or not */
#if (FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */


/**
 * \brief   Test for received wakeup pattern.
 *
 * \param Fr_CtrlIdx (in)       FlexRay controller index.
 * \param Fr_LPduIdx (in)       Pointer to write the received status to.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetWakeupRxStatus
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_WakeupRxStatusPtr
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_GETWAKEUPRXSTATUS_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETWAKEUPRXSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETWAKEUPRXSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check pointer for being valid */
    /* Deviation MISRA-3 */
    else if(Fr_WakeupRxStatusPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETWAKEUPRXSTATUS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_GETWAKEUPRXSTATUS_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        {
            /* read ERAY status interrupt register */
            CONST(FCAL_ERAY_RegSegHType,AUTOMATIC) RegSIR_H1 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    SIR,
                    H1);

            /* variable for constructing status value */
            VAR(uint8,AUTOMATIC) tmpWakeupRxStatus = 0x0U;

            /* check for WUP on channel A */
            if(FCAL_ERAY_GET_BIT(RegSIR_H1,SIR,WUPA,H1) != 0U)
            {
                tmpWakeupRxStatus |= FR_1_ERAY_WUP_CHANNEL_A;
            }

            /* check for WUP on channel B */
            if(FCAL_ERAY_GET_BIT(RegSIR_H1,SIR,WUPB,H1) != 0U)
            {
                tmpWakeupRxStatus |= FR_1_ERAY_WUP_CHANNEL_B;
            }

            if(tmpWakeupRxStatus != 0x0U)
            {
                /* reset WUP status */
                /* reset only if WUP was detected! */
                FCAL_ERAY_SET(
                        FCAL_ERAY_GetCCHandle(),
                        SIR,
                        H1,
                        (RegSIR_H1&(FCAL_ERAY_ALIGN_VALUE(
                            (FCAL_ERAY_SIR_WUPA_W0_MASK|
                             FCAL_ERAY_SIR_WUPB_W0_MASK),H1))));
            }

            /* return result to caller */
            (*Fr_WakeupRxStatusPtr) = tmpWakeupRxStatus;

        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif  /* (FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE == STD_ON) */

