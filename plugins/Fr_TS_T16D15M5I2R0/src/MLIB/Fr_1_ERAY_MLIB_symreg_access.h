/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */



/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 19.12 (required)
 * "There shall be at most one occurrence of the # or ## preprocessor operators in a single
 * macro definition."
 *
 * Reason:
 * Usage of multiple ## per macro definition improves readability in a high degree.
 * Order of ## examination (even if it was random) has no impact on result.
 *
 * MISRA-2) Deviated Rule: 19.13 (advisory)
 * "The # and ## preprocessor operators should not be used."
 *
 * Reason:
 * String concatenation is used to create efficient readable code.
 *
 */

#if !defined _FR_1_ERAY_MLIB_SYMREG_ACCESS_H_
#define _FR_1_ERAY_MLIB_SYMREG_ACCESS_H_


/*
 * Registers are splitted into so called register segments which are named according to their
 * access width:
 * B (Byte): 8-bit register segments
 * H (Half word): 16-bit register segments
 * W (Word): 32-bit register segments
 *
 * Each register is build up by segments which are started by numbering from 0.
 *
 *     MSB                 LSB
 *      ---------------------
 *      | B3 | B2 | B1 | B0 |   byte segemnts
 *      ---------------------
 *      |    H1   |    H0   |   half-word segments
 *      ---------------------
 *      |        W0         |   word-segment
 *      ---------------------
 */


/* register access witdth symbolic names */
#define MLIB_SYMREG_ACCESS8     0       /* use for 8-bit access configuration */
#define MLIB_SYMREG_ACCESS16    1       /* use for 16-bit access configuration */
#define MLIB_SYMREG_ACCESS32    2       /* use for 32-bit access configuration */


/* Checking pre-compile time value MLIB_SYMREG_REGISTER_ACCESS_MIN */
#if !defined MLIB_SYMREG_REGISTER_ACCESS_MIN
#error MLIB_SYMREG_REGISTER_ACCESS_MIN not defined
#endif /* MLIB_SYMREG_REGISTER_ACCESS_MIN */

/* test for valid value of MLIB_SYMREG_REGISTER_ACCESS_MIN */
#if (MLIB_SYMREG_REGISTER_ACCESS_MIN != MLIB_SYMREG_ACCESS8) && \
    (MLIB_SYMREG_REGISTER_ACCESS_MIN != MLIB_SYMREG_ACCESS16) && \
    (MLIB_SYMREG_REGISTER_ACCESS_MIN != MLIB_SYMREG_ACCESS32)
#error macro MLIB_SYMREG_REGISTER_ACCESS_MIN has an invalid value
#endif /* MLIB_SYMREG_REGISTER_ACCESS_MIN */

/* Checking pre-compile time value MLIB_SYMREG_REGISTER_ACCESS_MAX */
#if !defined MLIB_SYMREG_REGISTER_ACCESS_MAX
#error MLIB_SYMREG_REGISTER_ACCESS_MAX not defined
#endif /* MLIB_SYMREG_REGISTER_ACCESS_MAX */

/* test for valid value of MLIB_SYMREG_REGISTER_ACCESS_MAX */
#if (MLIB_SYMREG_REGISTER_ACCESS_MAX != MLIB_SYMREG_ACCESS8) && \
    (MLIB_SYMREG_REGISTER_ACCESS_MAX != MLIB_SYMREG_ACCESS16) && \
    (MLIB_SYMREG_REGISTER_ACCESS_MAX != MLIB_SYMREG_ACCESS32)
#error macro MLIB_SYMREG_REGISTER_ACCESS_MAX has an invalid value
#endif /* MLIB_SYMREG_REGISTER_ACCESS_MAX */

/* Checking pre-compile time value MLIB_SYMREG_OPCODE_ACCESS_MIN */
#if !defined MLIB_SYMREG_OPCODE_ACCESS_MIN
#error MLIB_SYMREG_OPCODE_ACCESS_MIN not defined
#endif /* MLIB_SYMREG_OPCODE_ACCESS_MIN */

/* test for valid value of MLIB_SYMREG_OPCODE_ACCESS_MIN */
#if (MLIB_SYMREG_OPCODE_ACCESS_MIN != MLIB_SYMREG_ACCESS8) && \
    (MLIB_SYMREG_OPCODE_ACCESS_MIN != MLIB_SYMREG_ACCESS16) && \
    (MLIB_SYMREG_OPCODE_ACCESS_MIN != MLIB_SYMREG_ACCESS32)
#error macro MLIB_SYMREG_OPCODE_ACCESS_MIN has an invalid value
#endif /* MLIB_SYMREG_OPCODE_ACCESS_MIN */

/* Checking pre-compile time value MLIB_SYMREG_OPCODE_ACCESS_MAX */
#if !defined MLIB_SYMREG_OPCODE_ACCESS_MAX
#error MLIB_SYMREG_OPCODE_ACCESS_MAX not defined
#endif /* MLIB_SYMREG_OPCODE_ACCESS_MAX */

/* test for valid value of MLIB_SYMREG_OPCODE_ACCESS_MAX */
#if (MLIB_SYMREG_OPCODE_ACCESS_MAX != MLIB_SYMREG_ACCESS8) && \
    (MLIB_SYMREG_OPCODE_ACCESS_MAX != MLIB_SYMREG_ACCESS16) && \
    (MLIB_SYMREG_OPCODE_ACCESS_MAX != MLIB_SYMREG_ACCESS32)
#error macro MLIB_SYMREG_OPCODE_ACCESS_MAX has an invalid value
#endif /* MLIB_SYMREG_OPCODE_ACCESS_MAX */

/* translate small accesses to bigger ones if small accesses are not supported */
#if MLIB_SYMREG_OPCODE_ACCESS_MIN == MLIB_SYMREG_ACCESS8

/* all right - no up-translation required */
typedef uint8  MLIB_SYMREG_RegSegBType; /* register byte segments are represented by uint8 */
typedef uint16 MLIB_SYMREG_RegSegHType; /* register halfword segments are represented by uint16 */
typedef uint32 MLIB_SYMREG_RegSegWType; /* register word segments are represented by uint32 */

#elif MLIB_SYMREG_OPCODE_ACCESS_MIN == MLIB_SYMREG_ACCESS16

/* 8-bit accesses are not possible - translate them into 16-bit accesses */
#define _B0 _H0     /* B0 now maps to H0 */
#define _B1 _H0     /* B1 now maps to H0 */
#define _B2 _H1     /* B2 now maps to H1 */
#define _B3 _H1     /* B3 now maps to H1 */

typedef uint16 MLIB_SYMREG_RegSegBType; /* register byte segments are represented by uint16 */
typedef uint16 MLIB_SYMREG_RegSegHType; /* register halfword segments are represented by uint16 */
typedef uint32 MLIB_SYMREG_RegSegWType; /* register word segments are represented by uint32 */

/* consider 32-bit access lower limit */
#else /* MLIB_SYMREG_OPCODE_ACCESS_MIN == MLIB_SYMREG_ACCESS32 */

#define _B0 _W0     /* B0 now maps to W0 */
#define _B1 _W0     /* B1 now maps to W0 */
#define _B2 _W0     /* B2 now maps to W0 */
#define _B3 _W0     /* B3 now maps to W0 */

#define _H0 _W0     /* H0 now maps to W0 */
#define _H1 _W0     /* H1 now maps to W0 */

typedef uint32 MLIB_SYMREG_RegSegBType;     /* register byte segments are represented by uint32 */
typedef uint32 MLIB_SYMREG_RegSegHType;     /* register halfword segments are represented by uint32 */
typedef uint32 MLIB_SYMREG_RegSegWType;     /* register word segments are represented by uint32 */

#endif /* MLIB_SYMREG_OPCODE_ACCESS_MIN */


/* macros used for scaling a value to the actually translated register segment access width */
#define MLIB_SYMREG_ALIGN_VALUE_B0_OFFSET 0U
#define MLIB_SYMREG_ALIGN_VALUE_B0_MASK ((U32)0x000000FFUL)
#define MLIB_SYMREG_ALIGN_VALUE_B0_TYPE uint8

#define MLIB_SYMREG_ALIGN_VALUE_B1_OFFSET 8U
#define MLIB_SYMREG_ALIGN_VALUE_B1_MASK ((U32)0x0000FF00UL)
#define MLIB_SYMREG_ALIGN_VALUE_B1_TYPE uint8

#define MLIB_SYMREG_ALIGN_VALUE_B2_OFFSET 16U
#define MLIB_SYMREG_ALIGN_VALUE_B2_MASK ((U32)0x00FF0000UL)
#define MLIB_SYMREG_ALIGN_VALUE_B2_TYPE uint8

#define MLIB_SYMREG_ALIGN_VALUE_B3_OFFSET 24U
#define MLIB_SYMREG_ALIGN_VALUE_B3_MASK ((U32)0xFF000000UL)
#define MLIB_SYMREG_ALIGN_VALUE_B3_TYPE uint8

#define MLIB_SYMREG_ALIGN_VALUE_H0_OFFSET 0U
#define MLIB_SYMREG_ALIGN_VALUE_H0_MASK ((U32)0x0000FFFFUL)
#define MLIB_SYMREG_ALIGN_VALUE_H0_TYPE uint16

#define MLIB_SYMREG_ALIGN_VALUE_H1_OFFSET 16U
#define MLIB_SYMREG_ALIGN_VALUE_H1_MASK ((U32)0xFFFF0000UL)
#define MLIB_SYMREG_ALIGN_VALUE_H1_TYPE uint16

#define MLIB_SYMREG_ALIGN_VALUE_W0_OFFSET 0U
#define MLIB_SYMREG_ALIGN_VALUE_W0_MASK ((U32)0xFFFFFFFFUL)
#define MLIB_SYMREG_ALIGN_VALUE_W0_TYPE uint32

/* this macro aligns a register value to the actual register segment used
 * value: Value to be written into the complete register
 * rseg: register segment of interesst
 */
/* Deviation MISRA-2 */
#define MLIB_SYMREG_ALIGN_VALUE(value,rseg)    \
                MLIB_SYMREG_HLP_ALIGN_VALUE(value,_##rseg)

#define MLIB_SYMREG_HLP_ALIGN_VALUE(value,rseg)    \
                MLIB_SYMREG_HLP0_ALIGN_VALUE(value,rseg)

/* Deviation MISRA-1, MISRA-2 <+6> */
#define MLIB_SYMREG_HLP0_ALIGN_VALUE(value,rseg)                \
                MLIB_SYMREG_HLP1_ALIGN_VALUE(value,             \
                MLIB_SYMREG_ALIGN_VALUE ## rseg ## _OFFSET,     \
                MLIB_SYMREG_ALIGN_VALUE ## rseg ## _MASK,       \
                MLIB_SYMREG_ALIGN_VALUE ## rseg ## _TYPE        \
                )

#define MLIB_SYMREG_HLP1_ALIGN_VALUE(value,offset,mask,type)    \
                ((type)((((uint32)(value))&((uint32)(mask)))>>(offset)))


/* Macros for reading a register by symbolic name using a symbol-address mapping */
#define MLIB_SYMREG_GET(hwid,h,reg,rseg)    \
                MLIB_SYMREG_HLP0_GET(hwid,h,reg,rseg)


/* Deviation MISRA-1, MISRA-2 <+5> */
#define MLIB_SYMREG_HLP0_GET(hwid,h,reg,rseg)                               \
                MLIB_SYMREG_HLP1_GET(hwid,h,                                \
                               hwid ## _ ## reg ## _ ## rseg ## _OFFSET,    \
                               hwid ## _ ## reg ## _ ## rseg ## _TYPE      \
                               )

#define MLIB_SYMREG_HLP1_GET(hwid,h,off,type) \
                MLIB_SYMREG_HLP2_GET(hwid,h,off,type)

/* Deviation MISRA-1, MISRA-2 */
#define MLIB_SYMREG_HLP2_GET(hwid,h,off,type) \
                hwid ## _GET_ ## type(h,off)

/* Macros for reading a register by symbolic name and index schema using a symbol-address mapping */
#define MLIB_SYMREG_GET_IDX(hwid,h,reg,rseg,idx)    \
                MLIB_SYMREG_HLP0_GET_IDX(hwid,h,reg,rseg,idx)

/* Deviation MISRA-1, MISRA-2 <+6> */
#define MLIB_SYMREG_HLP0_GET_IDX(hwid,h,reg,rseg,idx)                           \
                MLIB_SYMREG_HLP1_GET_IDX(hwid,h,idx,                            \
                               hwid ## _ ## reg ## _ ## rseg ## _OFFSET,    \
                               hwid ## _ ## reg ## _ ## rseg ## _TYPE,      \
                               hwid ## _ ## reg ## _STEP      \
                               )

#define MLIB_SYMREG_HLP1_GET_IDX(hwid,h,idx,off,type,step) \
                MLIB_SYMREG_HLP2_GET_IDX(hwid,h,idx,off,type,step)

/* Deviation MISRA-1, MISRA-2 */
#define MLIB_SYMREG_HLP2_GET_IDX(hwid,h,idx,off,type,step) \
                hwid ## _GET_ ## type(h,(((uint16)(off))+(((uint16)(step))*((uint16)(idx)))))



/* Macros for writing a register by symbolic name using a symbol-address mapping */
/* Deviation MISRA-2 */
#define MLIB_SYMREG_SET(hwid,h,reg,rseg,source_rseg,val)    \
                MLIB_SYMREG_HLP_SET(hwid,h,reg,_##rseg,source_rseg,val)

#define MLIB_SYMREG_HLP_SET(hwid,h,reg,rseg,source_rseg,val)    \
                MLIB_SYMREG_HLP0_SET(hwid,h,reg,rseg,source_rseg,val)

/* Deviation MISRA-1, MISRA-2 <+7> */
#define MLIB_SYMREG_HLP0_SET(hwid,h,reg,rseg,source_rseg,val)                       \
                MLIB_SYMREG_HLP1_SET(hwid,h,                                        \
                               hwid ## _ ## reg ## rseg ## _OFFSET,   \
                               hwid ## _ ## reg ## rseg ## _TYPE,     \
                               source_rseg,                                 \
                               val                                          \
                               )

#define MLIB_SYMREG_HLP1_SET(hwid,h,off,type,source_rseg,val) \
                MLIB_SYMREG_HLP2_SET(hwid,h,off,type,source_rseg,val)

/* Deviation MISRA-1, MISRA-2 */
#define MLIB_SYMREG_HLP2_SET(hwid,h,off,type,source_rseg,val) \
                hwid ## _SET_ ## type ## _ ## source_rseg(h,off,val)

/* Macros for writing a register by symbolic name and indexing schema using a symbol-address mapping */
/* Deviation MISRA-2 */
#define MLIB_SYMREG_SET_IDX(hwid,h,reg,rseg,source_rseg,idx,val)    \
                MLIB_SYMREG_HLP_SET_IDX(hwid,h,reg,_##rseg,source_rseg,idx,val)

#define MLIB_SYMREG_HLP_SET_IDX(hwid,h,reg,rseg,source_rseg,idx,val)    \
                MLIB_SYMREG_HLP0_SET_IDX(hwid,h,reg,rseg,source_rseg,idx,val)

/* Deviation MISRA-1, MISRA-2 <+8> */
#define MLIB_SYMREG_HLP0_SET_IDX(hwid,h,reg,rseg,source_rseg,idx,val)                       \
                MLIB_SYMREG_HLP1_SET_IDX(hwid,h,idx,                                        \
                               hwid ## _ ## reg ## rseg ## _OFFSET,   \
                               hwid ## _ ## reg ## rseg ## _TYPE,     \
                               hwid ## _ ## reg ## _STEP,     \
                               source_rseg,                                 \
                               val                                          \
                               )

#define MLIB_SYMREG_HLP1_SET_IDX(hwid,h,idx,off,type,step,source_rseg,val) \
                MLIB_SYMREG_HLP2_SET_IDX(hwid,h,idx,off,type,step,source_rseg,val)

/* Deviation MISRA-1, MISRA-2 */
#define MLIB_SYMREG_HLP2_SET_IDX(hwid,h,idx,off,type,step,source_rseg,val) \
                hwid ## _SET_ ## type ## _ ## source_rseg(h,(((uint16)(off))+(((uint16)(step))*((uint16)(idx)))),val)


/* Macros for writing a register by symbolic name while don't caring about other
 * register segments using a smbol-address mapping
 * "Don't care" means that other register segments might be written with 0 while
 * executing this command
 */
/* Deviation MISRA-2 */
#define MLIB_SYMREG_SETDC(hwid,h,reg,rseg,source_rseg,val)   \
                MLIB_SYMREG_HLP_SETDC(hwid,h,reg,_##rseg,val)

#define MLIB_SYMREG_HLP_SETDC(hwid,h,reg,rseg,val)   \
                MLIB_SYMREG_HLP0_SETDC(hwid,h,reg,rseg,val)

/* Deviation MISRA-1, MISRA-2 <+6> */
#define MLIB_SYMREG_HLP0_SETDC(hwid,h,reg,rseg,val)                         \
                MLIB_SYMREG_HLP1_SETDC(hwid,h,                              \
                               hwid ## _ ## reg ## rseg ## _OFFSET,        \
                               rseg,                                        \
                               val                                          \
                               )

#define MLIB_SYMREG_HLP1_SETDC(hwid,h,off,rseg,val) \
                MLIB_SYMREG_HLP2_SETDC(hwid,h,off,rseg,val)

/* Deviation MISRA-1, MISRA-2 */
#define MLIB_SYMREG_HLP2_SETDC(hwid,h,off,rseg,val) \
                hwid ## _SETDC ## rseg(h,off,val)


/*
 * Macros for writing a register by symbolic name and indexing schema
 * while don't caring about other register segments using a
 * smbol-address mapping.
 * "Don't care" means that other register segments might be written with 0
 * while executing this command.
 */
/* Deviation MISRA-2 */
#define MLIB_SYMREG_SETDC_IDX(hwid,h,reg,rseg,source_rseg,idx,val) \
                MLIB_SYMREG_HLP_SETDC_IDX(hwid,h,reg,_##rseg,idx,val)

#define MLIB_SYMREG_HLP_SETDC_IDX(hwid,h,reg,rseg,idx,val) \
                MLIB_SYMREG_HLP0_SETDC_IDX(hwid,h,reg,rseg,idx,val)

/* Deviation MISRA-1, MISRA-2 <+7> */
#define MLIB_SYMREG_HLP0_SETDC_IDX(hwid,h,reg,rseg,idx,val) \
                MLIB_SYMREG_HLP1_SETDC_IDX(hwid,h,idx, \
                               hwid ## _ ## reg ## rseg ## _OFFSET, \
                               hwid ## _ ## reg ## _STEP,     \
                               rseg, \
                               val \
                               )

#define MLIB_SYMREG_HLP1_SETDC_IDX(hwid,h,idx,off,step,rseg,val) \
                MLIB_SYMREG_HLP2_SETDC_IDX(hwid,h,idx,off,step,rseg,val)

/* Deviation MISRA-1, MISRA-2 */
#define MLIB_SYMREG_HLP2_SETDC_IDX(hwid,h,idx,off,step,rseg,val) \
                hwid ## _SETDC ## rseg(h,(((uint16)(off))+(((uint16)(step))*((uint16)(idx)))),val)



/* Macros for testing a bit by symbolic name using a symbol-address mapping */
/* Deviation MISRA-2 */
#define MLIB_SYMREG_TEST_BIT(hwid,variable,reg,bit,rseg)    \
            MLIB_SYMREG_HLP_TEST_BIT(hwid,variable,reg,bit,_##rseg)

#define MLIB_SYMREG_HLP_TEST_BIT(hwid,variable,reg,bit,rseg)    \
            MLIB_SYMREG_HLP0_TEST_BIT(hwid,variable,reg,bit,rseg)

/* Deviation MISRA-1, MISRA-2 <+6> */
#define MLIB_SYMREG_HLP0_TEST_BIT(hwid,variable,reg,bit,rseg)                           \
            MLIB_SYMREG_HLP1_TEST_BIT(variable,                                         \
                           hwid ## _ ## reg ## rseg ## _TYPE,             \
                           hwid ## _ ## reg ## _ ## bit ## rseg ## _SHIFT, \
                           hwid ## _ ## reg ## _ ## bit ## rseg ## _MASK   \
                               )

#define MLIB_SYMREG_HLP1_TEST_BIT(variable,type,shift,mask) \
            MLIB_SYMREG_HLP2_TEST_BIT(variable,type,shift,mask)

#define MLIB_SYMREG_HLP2_TEST_BIT(variable,type,shift,mask) \
                (((type)(((type)(variable))&((type)(mask)))) != ((type)(0U)))


/* Macros for requesting a bitfield by symbolic name using a symbol-address mapping */
/* Deviation MISRA-2 */
#define MLIB_SYMREG_GET_BIT(hwid,variable,reg,bit,rseg) \
            MLIB_SYMREG_HLP_GET_BIT(hwid,variable,reg,bit,_##rseg)

#define MLIB_SYMREG_HLP_GET_BIT(hwid,variable,reg,bit,rseg) \
            MLIB_SYMREG_HLP0_GET_BIT(hwid,variable,reg,bit,rseg)

/* Deviation MISRA-1, MISRA-2 <+6> */
#define MLIB_SYMREG_HLP0_GET_BIT(hwid,variable,reg,bit,rseg)                            \
            MLIB_SYMREG_HLP1_GET_BIT(variable,                                          \
                           hwid ## _ ## reg ## rseg ## _TYPE,             \
                           hwid ## _ ## reg ## _ ## bit ## rseg ## _SHIFT, \
                           hwid ## _ ## reg ## _ ## bit ## rseg ## _MASK   \
                               )

#define MLIB_SYMREG_HLP1_GET_BIT(variable,type,shift,mask) \
            MLIB_SYMREG_HLP2_GET_BIT(variable,type,shift,mask)

#define MLIB_SYMREG_HLP2_GET_BIT(variable,type,shift,mask) \
                ((type)(((type)((variable) >> (shift)))&((type)((type)(mask) >> (shift)))))



/* Macros for getting a bitfield being set by symbolic name using a symbol-address mapping */
/* Deviation MISRA-2 */
#define MLIB_SYMREG_SET_BIT(hwid,variable,reg,bit,rseg,value)   \
            MLIB_SYMREG_HLP_SET_BIT(hwid,variable,reg,bit,_##rseg,value)

#define MLIB_SYMREG_HLP_SET_BIT(hwid,variable,reg,bit,rseg,value)   \
            MLIB_SYMREG_HLP0_SET_BIT(hwid,variable,reg,bit,rseg,value)

/* Deviation MISRA-1, MISRA-2 <+6> */
#define MLIB_SYMREG_HLP0_SET_BIT(hwid,variable,reg,bit,rseg,value)                      \
            MLIB_SYMREG_HLP1_SET_BIT(variable,                                          \
                           hwid ## _ ## reg ## rseg ## _TYPE,             \
                           hwid ## _ ## reg ## _ ## bit ## rseg ## _SHIFT, \
                           hwid ## _ ## reg ## _ ## bit ## rseg ## _MASK,  \
                           value)

#define MLIB_SYMREG_HLP1_SET_BIT(variable,type,shift,mask,value) \
            MLIB_SYMREG_HLP2_SET_BIT(variable,type,shift,mask,value)

#define MLIB_SYMREG_HLP2_SET_BIT(variable,type,shift,mask,value) \
            ((variable) = (type)(((type)((variable) & ((type)(~((type)(mask)))))) | ((type)(((type)(value)) << (shift)))))



#endif  /* _FR_1_ERAY_MLIB_SYMREG_ACCESS_H_ */

