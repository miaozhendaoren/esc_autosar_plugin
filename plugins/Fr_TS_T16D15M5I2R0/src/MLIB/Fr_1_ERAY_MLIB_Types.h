/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if !defined _FR_1_ERAY_MLIB_TYPES_H_
#define _FR_1_ERAY_MLIB_TYPES_H_

#define FR_1_ERAY__AUTOMATIC_ AUTOMATIC

typedef uint8 U8;    /**< Represents an unsigned 8 bit entity. */
typedef uint16 U16;  /**< Represents an unsigned 16 bit entity.*/
typedef uint32 U32;  /**< Represents an unsigned 32 bit entity. */

#endif /* _FR_1_ERAY_MLIB_TYPES_H_ */

