/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 19.12 (required)
 * "There shall be at most one occurrence of the # or ## preprocessor operators in a single
 * macro definition."
 *
 * Reason:
 * Usage of multiple ## per macro definition improves readability in a high degree.
 * Order of ## examination (even if it was random) has no impact on result.
 *
 * MISRA-2) Deviated Rule: 19.13 (advisory)
 * "The # and ## preprocessor operators should not be used."
 *
 * Reason:
 * String concatenation is used to create efficient readable code.
 *
 */

#if !defined _FR_1_ERAY_MAL_RW_FORBID_UNALIGNED_LE_INLINE_H_
#define _FR_1_ERAY_MAL_RW_FORBID_UNALIGNED_LE_INLINE_H_

/* Deviation MISRA-1, MISRA-2 <START> */ 

/*
 * the appendix 'ALIGN8' means 8-bit aligned addresses (=unaligned)
 * the appendix 'ALIGN16' means 16-bit aligned addresses
 */
/*
 * read unsigned values from non-volatile addresses
 */
#define MAL_READ_U16_STREAM(prefix,addr,ptclass) \
           ((U16)((U16)(((U16)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,0U),ptclass))<< 0)  \
           |(U16)(((U16)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,1U),ptclass))<< 8)))

#define MAL_READ_U32_STREAM(prefix,addr,ptclass) \
           ((U32)((U32)(((U32)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,0U),ptclass))<< 0)  \
           |(U32)(((U32)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,1U),ptclass))<< 8)        \
           |(U32)(((U32)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,2U),ptclass))<<16)        \
           |(U32)(((U32)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,3U),ptclass))<<24)))

#define MAL_READ_U16_ALIGN8(addr,ptclass) \
           ((U16)((U16)(((U16)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,0U),ptclass))<< 0) \
           |(U16)(((U16)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,1U),ptclass))<< 8)))
#define MAL_READ_U32_ALIGN8(addr,ptclass) \
           ((U32)((U32)(((U32)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,0U),ptclass))<< 0) \
           |(U32)(((U32)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,1U),ptclass))<< 8) \
           |(U32)(((U32)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,2U),ptclass))<<16) \
           |(U32)(((U32)MAL_READ_U8(prefix,MLIB_CALC_CONST_ADDR_U8(prefix,addr,ptclass,3U),ptclass))<<24)))
#define MAL_READ_U32_ALIGN16(addr,ptclass) \
           ((U32)((U32)(((U32)MAL_READ_U16(prefix,MLIB_CALC_CONST_ADDR_U16(prefix,addr,ptclass,0U),ptclass))<< 0) \
           |(U32)(((U32)MAL_READ_U16(prefix,MLIB_CALC_CONST_ADDR_U16(prefix,addr,ptclass,1U),ptclass))<<16)))

/*
 * write unsigned values to non-volatile addresses
 */
#define MAL_WRITE_U16_STREAM(prefix,addr,ptclass,val)                          \
    do                                                                  \
    {                                                                   \
        CONSTP2VAR(uint8,AUTOMATIC,prefix##_##ptclass) _addr_ =                    \
            (P2VAR(uint8,AUTOMATIC,prefix##_##ptclass))(addr);                \
        const U16 _value_ = (val);                                      \
        _addr_[0] = (uint8)(_value_>>0);                                \
        _addr_[1] = (uint8)(_value_>>8);                                \
    }while(0)

#define MAL_WRITE_U32_STREAM(prefix,addr,ptclass,val)                          \
    do                                                                  \
    {                                                                   \
        CONSTP2VAR(uint8,AUTOMATIC,prefix##_##ptclass) _addr_ =                    \
            (P2VAR(uint8,AUTOMATIC,prefix##_##ptclass))(addr);                \
        const U32 _value_ = (val);                                      \
        _addr_[0] = (uint8)(_value_>>0);                                \
        _addr_[1] = (uint8)(_value_>>8);                                \
        _addr_[2] = (uint8)(_value_>>16);                               \
        _addr_[3] = (uint8)(_value_>>24);                               \
    }while(0)

/*
 * read unsigned values from volatile addresses
 */
#define MAL_READ_VOLATILE_U16_ALIGN8(prefix,addr,ptclass) \
           ((U16)((U16)(((U16)MAL_READ_VOLATILE_U8(prefix,MLIB_CALC_CONST_ADDR_VOLATILE_U8(prefix,addr,ptclass,0U),ptclass))<< 0) \
           |(U16)(((U16)MAL_READ_VOLATILE_U8(prefix,MLIB_CALC_CONST_ADDR_VOLATILE_U8(prefix,addr,ptclass,1U),ptclass))<< 8)))
#define MAL_READ_VOLATILE_U32_ALIGN8(prefix,addr,ptclass) \
           ((U32)((U32)(((U32)MAL_READ_VOLATILE_U8(prefix,MLIB_CALC_CONST_ADDR_VOLATILE_U8(prefix,addr,ptclass,0U),ptclass))<< 0) \
           |(U32)(((U32)MAL_READ_VOLATILE_U8(prefix,MLIB_CALC_CONST_ADDR_VOLATILE_U8(prefix,addr,ptclass,1U),ptclass))<< 8) \
           |(U32)(((U32)MAL_READ_VOLATILE_U8(prefix,MLIB_CALC_CONST_ADDR_VOLATILE_U8(prefix,addr,ptclass,2U),ptclass))<<16) \
           |(U32)(((U32)MAL_READ_VOLATILE_U8(prefix,MLIB_CALC_CONST_ADDR_VOLATILE_U8(prefix,addr,ptclass,3U),ptclass))<<24)))
#define MAL_READ_VOLATILE_U32_ALIGN16(prefix,addr,ptclass) \
           ((U32)((U32)(((U32)MAL_READ_VOLATILE_U16(prefix,MLIB_CALC_CONST_ADDR_VOLATILE_U16(prefix,addr,ptclass,0U),ptclass))<< 0) \
           |(U32)(((U32)MAL_READ_VOLATILE_U16(prefix,MLIB_CALC_CONST_ADDR_VOLATILE_U16(prefix,addr,ptclass,1U),ptclass))<<16)))


/*
 * write unsigned values to volatile addresses
 */
#define MAL_WRITE_VOLATILE_U16_ALIGN8(prefix,addr,ptclass,val)             \
    do                                                              \
    {                                                               \
        P2CONST(volatile,U8,AUTOMATIC,prefix##_##ptclass) _addr_ =             \
            (P2CONST(volatile,U8,AUTOMATIC,prefix##_##ptclass))(addr);         \
        const U16 _value_ = (val);                                  \
        MAL_WRITE_VOLATILE_U8(prefix,&_addr_[0],ptclass,_value_>>0);       \
        MAL_WRITE_VOLATILE_U8(prefix,&_addr_[1],ptclass,_value_>>8);       \
    }while(0)

#define MAL_WRITE_VOLATILE_U32_ALIGN8(prefix,addr,ptclass,val)             \
    do                                                              \
    {                                                               \
        P2CONST(volatile,U8,AUTOMATIC,prefix##_##ptclass) _addr_ =             \
            (P2CONST(volatile,U8,AUTOMATIC,prefix##_##ptclass))(addr);         \
        const U32 _value_ = (val);                                  \
        MAL_WRITE_VOLATILE_U8(prefix,&_addr_[0],ptclass,_value_>> 0);      \
        MAL_WRITE_VOLATILE_U8(prefix,&_addr_[1],ptclass,_value_>> 8);      \
        MAL_WRITE_VOLATILE_U8(prefix,&_addr_[2],ptclass,_value_>>16);      \
        MAL_WRITE_VOLATILE_U8(prefix,&_addr_[3],ptclass,_value_>>24);      \
    }while(0)

#define MAL_WRITE_VOLATILE_U32_ALIGN16(prefix,addr,ptclass,val)            \
    do                                                              \
    {                                                               \
        P2CONST(volatile,U16,AUTOMATIC,prefix##_##ptclass) _addr_ =            \
            (P2CONST(volatile,U16,AUTOMATIC,prefix##_##ptclass))(addr);        \
        const U32 _value_ = (val);                                  \
        MAL_WRITE_VOLATILE_U16(prefix,&_addr_[0],ptclass,_value_>> 0);     \
        MAL_WRITE_VOLATILE_U16(prefix,&_addr_[1],ptclass,_value_>>16);     \
    }while(0)

/* Deviation MISRA-1, MISRA-2 <STOP> */ 

#endif /* _FR_1_ERAY_MAL_RW_FORBID_UNALIGNED_LE_INLINE_H_ */

