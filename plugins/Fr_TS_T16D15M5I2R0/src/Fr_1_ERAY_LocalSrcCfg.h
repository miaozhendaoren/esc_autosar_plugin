/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if !defined _FR_1_ERAY_LOCALCFG_H_
#define _FR_1_ERAY_LOCALCFG_H_

#include <Std_Types.h>  /* get AUTOSAR standard data types                */

/******************************************************************************
 General Configuration Parameters
******************************************************************************/

/* buffer access retry cycles before a timeout error occurs */
#define FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT FR_1_ERAY_TIMEOUT_LOOP_LIMIT

#define FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE STD_OFF
#define FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE STD_ON
#define FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT FR_1_ERAY_TIMEOUT_LOOP_LIMIT
#define FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT FR_1_ERAY_TIMEOUT_LOOP_LIMIT

#define FR_1_ERAY_GLOBAL_TIME_SKIP_MT0_TIMEOUT FR_1_ERAY_TIMEOUT_LOOP_LIMIT

#define FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE STD_ON

#define FR_1_ERAY_REG_EILS_VALUE (0x00000000UL)
#define FR_1_ERAY_REG_SILS_VALUE (0x00000000UL)
#define FR_1_ERAY_REG_ILE_VALUE  (0x00000001UL)

/******************************************************************************
 MLIB configuration section
******************************************************************************/

/* define real register access width (min/max) */
#define MLIB_SYMREG_REGISTER_ACCESS_MIN MLIB_SYMREG_ACCESS8
#define MLIB_SYMREG_REGISTER_ACCESS_MAX MLIB_SYMREG_ACCESS32

/* define opcode access width (min/max) */
#define MLIB_SYMREG_OPCODE_ACCESS_MIN MLIB_SYMREG_ACCESS8
#define MLIB_SYMREG_OPCODE_ACCESS_MAX MLIB_SYMREG_ACCESS32

#include <Fr_1_ERAY_MLIB_Types.h>
#include <Fr_1_ERAY_MLIB_calc_addr.h>


/******************************************************************************
 MAL configuration section
******************************************************************************/

#include <Fr_1_ERAY_MAL_rw.h>
#include <Fr_1_ERAY_MAL_rw_forbid_unaligned_le.h>

/******************************************************************************
 FCAL configuration section
******************************************************************************/


/* Implementation of raw hardware access macros for accessing the FlexRay CC and System Memory */

/* reads a raw uint32 value from 'h' at offset 'off' */
#define FCAL_ERAY_READ_U32(h,off) \
    MAL_READ_VOLATILE_U32(FR_1_ERAY,MLIB_CALC_CONST_ADDR_VOLATILE_U32(FR_1_ERAY,h,_AUTOMATIC_,off),_AUTOMATIC_)

/* reads a raw uint16 value from 'h' at offset 'off' */
#define FCAL_ERAY_READ_U16(h,off) \
    MAL_READ_VOLATILE_U16(FR_1_ERAY,MLIB_CALC_CONST_ADDR_VOLATILE_U16(FR_1_ERAY,h,_AUTOMATIC_,off),_AUTOMATIC_)

/* reads a raw uint8 value from 'h' at offset 'off' */
#define FCAL_ERAY_READ_U8(h,off) \
    MAL_READ_VOLATILE_U8(FR_1_ERAY,MLIB_CALC_CONST_ADDR_VOLATILE_U8(FR_1_ERAY,h,_AUTOMATIC_,off),_AUTOMATIC_)

/* writes a raw uint32 value 'val' to 'h' at offset 'off' */
#define FCAL_ERAY_WRITE_U32(h,off,val) \
    MAL_WRITE_VOLATILE_U32(FR_1_ERAY,MLIB_CALC_ADDR_VOLATILE_U32(FR_1_ERAY,h,_AUTOMATIC_,off),_AUTOMATIC_,val)

/* writes a raw uint16 value 'val' to 'h' at offset 'off' */
#define FCAL_ERAY_WRITE_U16(h,off,val) \
    MAL_WRITE_VOLATILE_U16(FR_1_ERAY,MLIB_CALC_ADDR_VOLATILE_U16(FR_1_ERAY,h,_AUTOMATIC_,off),_AUTOMATIC_,val)

/* writes a raw uint8 value 'val' to 'h' at offset 'off' */
#define FCAL_ERAY_WRITE_U8(h,off,val) \
    MAL_WRITE_VOLATILE_U8(FR_1_ERAY,MLIB_CALC_ADDR_VOLATILE_U8(FR_1_ERAY,h,_AUTOMATIC_,off),_AUTOMATIC_,val)


/******************************************************************************
 * Implementation of register get/set macros based on raw access macros
 * defined before:
 * FCAL_ERAY_<READ|WRITE>_<U8|U16|U32>()
 * Those macros must return the register value as specified and if required
 * perform swapping operations.
 *****************************************************************************/

/* read uint32 register out of the FlexRay CC 'h' from offset 'off' */
#define FCAL_ERAY_GET_U32(h,off) \
    FCAL_ERAY_READ_U32(h,off)

/* read uint16 register out of the FlexRay CC 'h' from offset 'off' */
#define FCAL_ERAY_GET_U16(h,off) \
    FCAL_ERAY_READ_U16(h,off)

/* read uint8 register out of the FlexRay CC 'h' from offset 'off' */
#define FCAL_ERAY_GET_U8(h,off) \
    FCAL_ERAY_READ_U8(h,off)

/* write uint32 into the FlexRay CC 'h' register at offset 'off' */
#define FCAL_ERAY_SET_U32(h,off,val) \
    FCAL_ERAY_WRITE_U32(h,off,val)

/* write uint16 into the FlexRay CC 'h' register at offset 'off' */
#define FCAL_ERAY_SET_U16(h,off,val) \
    FCAL_ERAY_WRITE_U16(h,off,val)

/* write uint8 into the FlexRay CC 'h' register at offset 'off' */
#define FCAL_ERAY_SET_U8(h,off,val) \
    FCAL_ERAY_WRITE_U8(h,off,val)

/******************************************************************************
 * Implementation of payload copy macors. There are library macro-copy
 * routines defined in FCAL_ERAY_memcopy.h which should be used.
 * Those are based on FCAL_ERAY_<READ|WRITE>_U32().
 * Those macros must copy the payload considering the network byte order.
 * First byte on the network must result on lowest address in memory
 * and vice versa.
 *****************************************************************************/

/*
 * copy a range of bytes from the bosch e-ray module to memory in network
 * byte ordering (that is, byte addresses do not change)
 * make only 32-bit reads from the controller
 * Assume the memory address to be 8-bit aligned.
 */
#define FCAL_ERAY_CTRL_TO_MEM_NBO_ALIGN8(h,off,size,addr,ptclass) \
            FCAL_ERAY_CTRL_TO_MEM_ALIGN8(FR_1_ERAY,h,off,size,addr,ptclass)
/*
 * copy a range of bytes from memory to the bosch e-ray module in network
 * byte ordering (that is, byte addresses do not change)
 * make only 32-bit writes to the controller
 * Assume the memory address to be 8-bit aligned.
 */
#define FCAL_ERAY_MEM_TO_CTRL_NBO_ALIGN8(h,off,size,addr,ptclass) \
            FCAL_ERAY_MEM_TO_CTRL_ALIGN8(FR_1_ERAY,h,off,size,addr,ptclass)

/**
 * Access hook for interface access driver init
 *
 * - Clear the endinit bit to unsecure the clock register
 * - Set the ERAY PLL configuration and wait for PLL lock
 * - Set the endinit bit to secure the clock register again
 */
#include <Ifx_reg.h>                  /* Tricore TC277 register definitions */
#include <Mcal.h>                     /* EndInit function declaration */
#include <Fr_1_ERAY_IntegrationCfg.h>   /* defines for NDIV, K1DIV, K2DIV */

#define FR_1_ERAY_ENABLE_CLOCK                  0x00000108U

#define FCAL_ERAY_AccessDriverInit()                                            \
    do                                                                          \
    {                                                                           \
        Mcal_ResetENDINIT();                                                    \
        ERAY0_CLC.U = FR_1_ERAY_ENABLE_CLOCK;                                   \
        ERAY0_CUST1.U =  FR_1_ERAY_RIS_VALUE;                                   \
        Mcal_SetENDINIT();                                                      \
    } while (0)

/* define handle type */
#define FCAL_ERAY_DefineCCHandle() \

/* define handle validity check */
#define FCAL_ERAY_IsCCHandleValid(CtrlIdx) \
    (TRUE)

/* define handle initialization from controller index */
#define FCAL_ERAY_InitCCHandle(CtrlIdx) \

/* define handle extraction routine */
#define FCAL_ERAY_GetCCHandle() \
    (0xF001C000UL)

/* define CC access check macro */
#define FCAL_ERAY_IsCCAccessValid(CtrlIdx) \
    (TRUE)

/* define number of CC access macro */
#define FCAL_ERAY_GetNumCC() \
    ((uint8)1U)

#include <Fr_1_ERAY_FCAL_memcpy.h>
#include <Fr_1_ERAY_FCAL_sym.h>
#include <Fr_1_ERAY_FCAL_regdef_le.h>

#endif /* _FR_1_ERAY_LOCALCFG_H_ */

