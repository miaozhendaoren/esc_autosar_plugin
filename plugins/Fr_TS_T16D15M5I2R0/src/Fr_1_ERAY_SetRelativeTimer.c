/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

/* check whether relative timer is enabled or not */
#if (FR_1_ERAY_RELATIVE_TIMER_ENABLE == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */


/**
 * \brief   Sets a relative time alarm.
 *
 * \param Fr_CtrlIdx (in)          FlexRay controller index.
 * \param Fr_RelTimerIdx (in)      Relative timer index.
 * \param Fr_Offset (in)           Macrotick offset the alarm should elapse.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_SetRelativeTimer
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_RelTimerIdx,
        VAR(uint16,AUTOMATIC) Fr_Offset
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_SETRELATIVETIMER_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_SETRELATIVETIMER_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_SETRELATIVETIMER_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check if timer index is within supported bounds */
    else if(Fr_RelTimerIdx != ((VAR(uint8,AUTOMATIC))0U))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_TIMER_IDX, FR_1_ERAY_SETRELATIVETIMER_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    else if(Fr_Offset > (uint16)(FCAL_ERAY_T1C_T1MC_W0_MASK>>FCAL_ERAY_T1C_T1MC_W0_SHIFT))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_OFFSET, FR_1_ERAY_SETRELATIVETIMER_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);
    TS_PARAM_UNUSED(Fr_RelTimerIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_SETRELATIVETIMER_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        /* perform an FlexRay CC sync-check */
        /* since ERAY updates the POC-state immediately, we don't need to evaluate the freeze-bit */
        {
            /* read ERAY status vector register */
            CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegCCSV_B0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    CCSV,
                    B0
                    );

            /* evaluate the POC-state */
            CONST(uint32,AUTOMATIC) tmpPOCState =
                FCAL_ERAY_GET_BIT(RegCCSV_B0,CCSV,POCS,B0);

            /* check if POC state is "normal-active" or "normal-passive" */
            /* if not abort function execution */
            if((tmpPOCState != ((VAR(uint32,AUTOMATIC))FCAL_ERAY_CCSV_POCS_NORMAL_ACTIVE)) &&
               (tmpPOCState != ((VAR(uint32,AUTOMATIC))FCAL_ERAY_CCSV_POCS_NORMAL_PASSIVE)) )
            {
                /* CC is not synchronous, report to Det (if enabled) */
                FR_1_ERAY_DET_REPORTERROR(
                    FR_1_ERAY_E_INV_POCSTATE,
                    FR_1_ERAY_SETRELATIVETIMER_SERVICE_ID
                    );

                /* not synchronized before timer programming - report error condition */
                retval = E_NOT_OK;
            }
        }

        if(retval == E_OK)
        {
            /* halt timer and set to single-shot mode */
            FCAL_ERAY_SETDC(
                FCAL_ERAY_GetCCHandle(),
                T1C,
                B0,
                0x0U
                );

            /* if the timer is set to offset 0 - which is invalid for ERAY - set it to offset 1 */
            /* this increase in jitter is better than producing errors the upper layer must handle with */
            if(Fr_Offset == 0x0U)
            {
                Fr_Offset = 0x01U;
            }

#if (MLIB_SYMREG_REGISTER_ACCESS_MAX == MLIB_SYMREG_ACCESS32)
            {
                /* temporary timer control register */
                /* set to disabled, single-shot timer */
                VAR(FCAL_ERAY_RegSegWType,AUTOMATIC) RegT1C_W0 = FCAL_ERAY_T1C_T1RC_W0_MASK;

                /* set macrotick offset to elapse at */
                FCAL_ERAY_SET_BIT(
                    RegT1C_W0,
                    T1C,
                    T1MC,
                    W0,
                    ((VAR(uint32,AUTOMATIC))Fr_Offset)
                    );

                /* write to timer control register */
                FCAL_ERAY_SET(
                    FCAL_ERAY_GetCCHandle(),
                    T1C,
                    W0,
                    RegT1C_W0
                    );
            }

#elif (MLIB_SYMREG_REGISTER_ACCESS_MAX == MLIB_SYMREG_ACCESS16)
            {
                /* temporary timer control registers */
                /* set to disabled, single-shot timer */
                CONST(FCAL_ERAY_RegSegHType,AUTOMATIC) RegT1C_H0 = FCAL_ERAY_T1C_T1RC_H0_MASK;
                VAR(FCAL_ERAY_RegSegHType,AUTOMATIC) RegT1C_H1 = 0x0U;

                /* set macrotick offset to elapse at */
                FCAL_ERAY_SET_BIT(
                    RegT1C_H1,
                    T1C,
                    T1MC,
                    H1,
                    Fr_Offset
                    );

                /* write to timer control register */
                /* high half-word */
                FCAL_ERAY_SET(
                    FCAL_ERAY_GetCCHandle(),
                    T1C,
                    H1,
                    RegT1C_H1
                    );

                /* write to timer control register */
                /* low half-word */
                FCAL_ERAY_SET(
                    FCAL_ERAY_GetCCHandle(),
                    T1C,
                    H0,
                    RegT1C_H0
                    );

            }
#else /* (MLIB_SYMREG_REGISTER_ACCESS_MAX == MLIB_SYMREG_ACCESS8) */
#error no support for maximum 8-bit ERAY access
#endif  /* MLIB_SYMREG_REGISTER_ACCESS_MAX */
        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif  /* (FR_1_ERAY_RELATIVE_TIMER_ENABLE == STD_ON) */

