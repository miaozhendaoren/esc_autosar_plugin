/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

/* check whether this API service is enabled or not */
#if (FR_1_ERAY_WAKEUP_API_ENABLE == STD_ON)


/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */


/**
 * \brief   Selects a channel for wakeup pattern transmission.
 *
 * \param Fr_CtrlIdx (in)       FlexRay controller index.
 * \param Fr_ChnlIdx (in)       Channel the wakeup pattern should be transmitted on.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 * \note    The function must be called in CC POC-state "ready".
 *
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_SetWakeupChannel
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check for correct channel parameter */
    else if((Fr_ChnlIdx != FR_CHANNEL_A) && (Fr_ChnlIdx != FR_CHANNEL_B))
    {
        /* report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CHNL_IDX, FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else

#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)
        else
        /* perform an FlexRay CC POC-state 'ready' check*/
        {
            /* read ERAY status vector register */
            CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegCCSV_B0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    CCSV,
                    B0
                    );

            /* evaluate the POC-state */
            CONST(uint32,AUTOMATIC) tmpPOCState =
                FCAL_ERAY_GET_BIT(RegCCSV_B0,CCSV,POCS,B0);

            /* check if POC state is "ready" */
            /* if not abort function execution */
            if(tmpPOCState != ((VAR(uint32,AUTOMATIC))FCAL_ERAY_CCSV_POCS_READY))
            {

                /* Report to DET */
                FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POCSTATE, FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID);

                /* Return function status NOT OK */
                retval = E_NOT_OK;

            }
        }

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

        if(retval == E_OK)
        /* implement functionality */
        {
            /* variable holding SUCC1 value */
            VAR(FCAL_ERAY_RegSegBType,AUTOMATIC) RegSUCC1_B2;

/* check for busy bit only if optimization is disabled */
#if (FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE == STD_OFF)

            /* check for busy bit and wait if CMD is busy (with timeout) */

            retval = Fr_1_ERAY_WaitWhileCmdBusyStandard(FR_1_ERAY_CTRLIDX, FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID);

            if(retval == E_OK)

#endif /* FR_1_ERAY_BUSY_NONIMMEDIATE_OPTIMIZATION */


            /* write CONFIG command to SUCC1->CMD */
            {
                const Std_ReturnType RetCode = Fr_1_ERAY_CHICommand(FR_1_ERAY_CTRLIDX, FCAL_ERAY_SUCC1_CMD_CONFIG, FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID);

                if(RetCode != E_OK)
                {
                    retval = E_NOT_OK;
                }
            }

            /* now the ERAY is in config state */


            if(retval == E_OK)
            {
                /* get actual value of ERAY->SUCC1:B2 */
                RegSUCC1_B2 = FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    SUCC1,
                    B2
                    );


                if(Fr_ChnlIdx == FR_CHANNEL_A)
                {

                    /* clear WUCS bit */
                    FCAL_ERAY_SET_BIT(
                        RegSUCC1_B2,
                        SUCC1,
                        WUCS,
                        B2,
                        ((VAR(uint8,AUTOMATIC))0x0U)
                        );
                }
                else /* channel B */
                {
                    /* set WUCS bit */
                    FCAL_ERAY_SET_BIT(
                        RegSUCC1_B2,
                        SUCC1,
                        WUCS,
                        B2,
                        ((VAR(uint8,AUTOMATIC))0x1U)
                        );
                }

                /* write SUCC1 configuration */
                FCAL_ERAY_SET(
                    FCAL_ERAY_GetCCHandle(),
                    SUCC1,
                    B2,
                    RegSUCC1_B2
                    );

                /* unlock CC to change to ready state */
                /* unlock step 1 */
                FCAL_ERAY_SETDC(
                    FCAL_ERAY_GetCCHandle(),
                    LCK,
                    B0,
                    FCAL_ERAY_ALIGN_VALUE(0xCEU,B0)
                    );

                /* unlock step 2 */
                FCAL_ERAY_SETDC(
                    FCAL_ERAY_GetCCHandle(),
                    LCK,
                    B0,
                    FCAL_ERAY_ALIGN_VALUE(0x31U,B0)
                    );

/* check for busy bit only if optimization is disabled */
#if (FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE == STD_OFF)

                /* check for busy bit and wait if CMD is busy (with timeout) */
                if(Fr_1_ERAY_WaitWhileCmdBusyStandard(FR_1_ERAY_CTRLIDX, FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID) != E_OK)
                {
                    retval = E_NOT_OK;
                }
                else

#endif /* FR_1_ERAY_BUSY_NONIMMEDIATE_OPTIMIZATION */

                {
                    /* write READY command to SUCC1->CMD bit while conserving all other bits */
                    retval = Fr_1_ERAY_CHICommandPreserveOtherBits(FR_1_ERAY_CTRLIDX, FCAL_ERAY_SUCC1_CMD_READY, FR_1_ERAY_SETWAKEUPCHANNEL_SERVICE_ID);
                }
            }
        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif /* FR_1_ERAY_WAKEUP_API_ENABLE */

