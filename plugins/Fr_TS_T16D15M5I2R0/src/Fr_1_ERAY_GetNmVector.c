/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

/* check whether the API is enabled or not */
#if (FR_1_ERAY_NMVECTOR_ENABLE == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */


/**
 * \brief   Reads the FlexRay NM-Vector of the last FlexRay cycle.
 *
 * \param Fr_CtrlIdx (in)           FlexRay controller index.
 * \param Fr_NmVectorPtr (out)      Address to write the Nm-Vector to.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetNmVector
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_NmVectorPtr
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_GETNMVECTOR_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETNMVECTOR_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETNMVECTOR_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check whether configuration has an entry for this FlexRay CC Idx */
    else if(FR_1_ERAY_CTRLIDX >= gFr_1_ERAY_ConfigPtr->nNumCtrl)
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETNMVECTOR_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;

    }

    /* check pointer for being valid */
    else if(Fr_NmVectorPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETNMVECTOR_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_GETNMVECTOR_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        /* perform an FlexRay CC sync-check */
        {
            /* read ERAY status vector register */
            CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegCCSV_B0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    CCSV,
                    B0
                    );

            /* evaluate the POC-state */
            CONST(uint32,AUTOMATIC) tmpPOCState =
                FCAL_ERAY_GET_BIT(RegCCSV_B0,CCSV,POCS,B0);

            /* check if POC state is "normal-active" or "normal-passive" */
            /* if not abort function execution */
            if((tmpPOCState != ((VAR(uint32,AUTOMATIC))FCAL_ERAY_CCSV_POCS_NORMAL_ACTIVE)) &&
               (tmpPOCState != ((VAR(uint32,AUTOMATIC))FCAL_ERAY_CCSV_POCS_NORMAL_PASSIVE)) )
            {
                retval = E_NOT_OK;
            }
        }

        if(retval == E_OK)
        /* implemented functionality */
        {
            /* get pointer to controller configuration */
            CONSTP2CONST(Fr_1_ERAY_CtrlCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCtrlCfg =
                 &FR_1_ERAY_GET_CONFIG_ADDR
                (
                    Fr_1_ERAY_CtrlCfgType,
                    gFr_1_ERAY_ConfigPtr->pCtrlCfg
                )[FR_1_ERAY_CTRLIDX];

            CONST(uint8,AUTOMATIC) NmVectorLength =
                FR_1_ERAY_CFG_OPTION_NMVECTOR_LENGTH(
                    pCtrlCfg->nCfgOptions);

            /* copy payload data */
            /* using do { }while(0); within macro for keeping it a single statement */
            FCAL_ERAY_CTRL_TO_MEM_NBO_ALIGN8(
                FCAL_ERAY_GetCCHandle(),
                FCAL_ERAY_NMVn_W0_OFFSET,
                NmVectorLength,
                Fr_NmVectorPtr,
                APPL_DATA
                );
        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif  /* (FR_1_ERAY_NMVECTOR_ENABLE == STD_ON) */

