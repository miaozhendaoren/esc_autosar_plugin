/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if !defined _FR_1_ERAY_INTEGRATIONCFG_H_
#define _FR_1_ERAY_INTEGRATIONCFG_H_

/******************************************************************************
 Include Section
******************************************************************************/

/******************************************************************************
 Global Macros
******************************************************************************/
#define FR_1_ERAY_RXDA0 0U
#define FR_1_ERAY_RXDA1 1U
#define FR_1_ERAY_RXDA2 2U
#define FR_1_ERAY_RXDA3 3U

#define FR_1_ERAY_RXDB0 0U
#define FR_1_ERAY_RXDB1 1U
#define FR_1_ERAY_RXDB2 2U
#define FR_1_ERAY_RXDB3 3U

#define FR_1_ERAY_RISA FR_1_ERAY_[!"FrGeneral/VendorSpecific/HardwareSettings/ErayRISA"!]
#define FR_1_ERAY_RISB FR_1_ERAY_[!"FrGeneral/VendorSpecific/HardwareSettings/ErayRISB"!]


#define FR_1_ERAY_RIS_VALUE ((uint32)(((uint32)FR_1_ERAY_RISA)<<10U)|(((uint32)FR_1_ERAY_RISB)<<12U))
/******************************************************************************
 Global Data
******************************************************************************/
/******************************************************************************
 Global Function Declarations
******************************************************************************/

#endif /* _FR_1_ERAY_INTEGRATIONCFG_H_ */

/*
 * = eof ======================================================================
 */

