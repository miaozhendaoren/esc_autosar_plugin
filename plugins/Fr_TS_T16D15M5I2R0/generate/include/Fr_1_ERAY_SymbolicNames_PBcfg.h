/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

[!CODE!]
#ifndef FR_1_ERAY_SYMBOLICNAMES_PBCFG_H
#define FR_1_ERAY_SYMBOLICNAMES_PBCFG_H

/*==================[macros]=================================================*/

/*------------------[symbolic name definitions]------------------------------*/

/* FrController */[!/*
*/!][!LOOP "as:modconf('Fr')[1]/FrMultipleConfiguration/*[1]/FrController/*"!]
#define Fr_1_ERAYConf_[!"./@name"!] [!"./FrCtrlIdx"!]U

#if (!defined FR_1_ERAY_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"@name"!] [!"./FrCtrlIdx"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define Fr_1_ERAY_[!"@name"!] [!"./FrCtrlIdx"!]U
#endif /* FR_1_ERAY_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]

#endif /* FR_1_ERAY_SYMBOLICNAMES_PBCFG_H */
[!ENDCODE!][!//
