/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

/*==================[inclusions]============================================*/

#include <TSAutosar.h> /* EB specific standard types */
#include <Std_Types.h> /* AUTOSAR standard types */
#include <Fr_1_ERAY_Lcfg.h> /* Generated configuration */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/* start data section declaration */
#define FR_1_ERAY_START_SEC_CONST_32
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/* value used to validate post build configuration against link time configuration */
CONST(uint32, FR_1_ERAY_CONST) Fr_1_ERAY_LcfgSignature = [!"asc:getConfigSignature(as:modconf('Fr')[1]//*[not(child::*) and (node:configclass() = 'Link') ])"!]U;

/* stop data section declaration */
#define FR_1_ERAY_STOP_SEC_CONST_32
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */


/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external constants]====================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
