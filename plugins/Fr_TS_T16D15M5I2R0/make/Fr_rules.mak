# \file
#
# \brief AUTOSAR Fr
#
# This file contains the implementation of the AUTOSAR
# module Fr.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS


#################################################################
# REGISTRY

ifndef TS_MERGED_COMPILE
TS_MERGED_COMPILE := TRUE
endif

ifndef TS_FR_MERGED_COMPILE
TS_FR_MERGED_COMPILE := $(TS_MERGED_COMPILE)
endif

ifeq ($(TS_FR_MERGED_COMPILE),TRUE)

Fr_src_FILES +=                              \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_Merged.c \

else

Fr_src_FILES +=                                                \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY.c                            \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_AbortCommunication.c         \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_AckAbsoluteTimerIRQ.c        \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_AckIRQ.c                     \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_AckRelativeTimerIRQ.c        \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_AllowColdstart.c             \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_AllSlots.c                   \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_CancelAbsoluteTimer.c        \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_CancelRelativeTimer.c        \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_CheckMTS.c                   \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_CheckTxLPduStatus.c          \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_ControllerInit.c             \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_DisableAbsoluteTimerIRQ.c    \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_DisableIRQ.c                 \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_DisableLPdu.c                \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_DisableRelativeTimerIRQ.c    \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_EnableAbsoluteTimerIRQ.c     \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_EnableIRQ.c                  \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_EnableRelativeTimerIRQ.c     \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetAbsoluteTimerIRQStatus.c  \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetChannelStatus.c           \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetClockCorrection.c         \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetControllerErrorStatus.c   \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetGlobalTime.c              \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetIRQStatus.c               \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetNmVector.c                \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetPOCStatus.c               \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetRelativeTimerIRQStatus.c  \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetSyncFrameList.c           \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetSyncState.c               \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetVersionInfo.c             \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_GetWakeupRxStatus.c          \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_HaltCommunication.c          \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_Init.c                       \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_PrepareLPdu.c                \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_ReceiveRxLPdu.c              \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_ReconfigLPdu.c               \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_SendMTS.c                    \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_SendWUP.c                    \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_SetAbsoluteTimer.c           \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_SetExtSync.c                 \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_SetRelativeTimer.c           \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_SetWakeupChannel.c           \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_StartCommunication.c         \
    $(Fr_CORE_PATH)\src\Fr_1_ERAY_TransmitTxLPdu.c             \

endif

Fr_src_FILES += $(wildcard $(Fr_CORE_PATH)\src\Fr_1_ERAY_LocalSrcCfg.c)  \
                $(Fr_OUTPUT_PATH)\src\Fr_1_ERAY_Lcfg.c

LIBRARIES_TO_BUILD   += Fr_src
								
# Fill the list with post build configuration files needed to build the post build binary.
Fr_pbconfig_FILES := $(wildcard $(Fr_OUTPUT_PATH)/src/Fr_1_ERAY_PBcfg.c)

ifneq ($(strip $(Fr_pbconfig_FILES)),)
LIBRARIES_PBCFG_TO_BUILD += Fr_pbconfig
LIBRARIES_TO_BUILD += Fr_pbconfig
endif

define defineFrLibOutputPATH
$(1)_OBJ_OUTPUT_PATH    := $(Fr_lib_LIB_OUTPUT_PATH)
endef
$(foreach SRC,$(basename $(notdir $(subst \,/,$(Fr_lib_FILES)))),$(eval $(call defineFrLibOutputPATH,$(SRC))))

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES




