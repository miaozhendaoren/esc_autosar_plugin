# \file
#
# \brief AUTOSAR Gpt
#
# This file contains the implementation of the AUTOSAR
# module Gpt.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Gpt_src

Gpt_src_FILES       += $(Gpt_CORE_PATH)\src\Gpt.c
Gpt_src_FILES       += $(Gpt_CORE_PATH)\src\Gpt_Ver.c
Gpt_src_FILES       += $(Gpt_OUTPUT_PATH)\src\Gpt_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Gpt_src_FILES :=
endif
