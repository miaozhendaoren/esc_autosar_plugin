/**
 * \file
 *
 * \brief AUTOSAR Ea
 *
 * This file contains the implementation of the AUTOSAR
 * module Ea.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined EA_TRACE_H)
#define EA_TRACE_H
/*==================[inclusions]============================================*/

[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>
[!ENDIF!]

/*==================[macros]================================================*/

#ifndef DBG_EA_JOBERRORNOTIFICATION_ENTRY
/** \brief Entry point of function Ea_JobErrorNotification() */
#define DBG_EA_JOBERRORNOTIFICATION_ENTRY()
#endif

#ifndef DBG_EA_INIT_ENTRY
/** \brief Entry point of function Ea_Init() */
#define DBG_EA_INIT_ENTRY()
#endif
/* !LINKSTO EA155,1 */
#ifndef DBG_EA_MAINSTATE
/** \brief Change of Ea_MainState */
#define DBG_EA_MAINSTATE(a,b)
#endif

#ifndef DBG_EA_JOBRESULT
/** \brief Change of Ea_JobResult */
#define DBG_EA_JOBRESULT(a,b)
#endif

#ifndef DBG_EA_STATUS
/** \brief Change of Ea_Status */
#define DBG_EA_STATUS(a,b)
#endif

#ifndef DBG_EA_INIT_EXIT
/** \brief Exit point of function Ea_Init() */
#define DBG_EA_INIT_EXIT()
#endif

#ifndef DBG_EA_SETMODE_ENTRY
/** \brief Entry point of function Ea_SetMode() */
#define DBG_EA_SETMODE_ENTRY(a)
#endif

#ifndef DBG_EA_SETMODE_EXIT
/** \brief Exit point of function Ea_SetMode() */
#define DBG_EA_SETMODE_EXIT(a)
#endif

#ifndef DBG_EA_READ_ENTRY
/** \brief Entry point of function Ea_Read() */
#define DBG_EA_READ_ENTRY(a,b,c,d)
#endif

#ifndef DBG_EA_READ_EXIT
/** \brief Exit point of function Ea_Read() */
#define DBG_EA_READ_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_EA_WRITE_ENTRY
/** \brief Entry point of function Ea_Write() */
#define DBG_EA_WRITE_ENTRY(a,b)
#endif

#ifndef DBG_EA_WRITE_EXIT
/** \brief Exit point of function Ea_Write() */
#define DBG_EA_WRITE_EXIT(a,b,c)
#endif

#ifndef DBG_EA_CANCEL_ENTRY
/** \brief Entry point of function Ea_Cancel() */
#define DBG_EA_CANCEL_ENTRY()
#endif

#ifndef DBG_EA_CANCEL_EXIT
/** \brief Exit point of function Ea_Cancel() */
#define DBG_EA_CANCEL_EXIT()
#endif

#ifndef DBG_EA_GETSTATUS_ENTRY
/** \brief Entry point of function Ea_GetStatus() */
#define DBG_EA_GETSTATUS_ENTRY()
#endif

#ifndef DBG_EA_GETSTATUS_EXIT
/** \brief Exit point of function Ea_GetStatus() */
#define DBG_EA_GETSTATUS_EXIT(a)
#endif

#ifndef DBG_EA_GETJOBRESULT_ENTRY
/** \brief Entry point of function Ea_GetJobResult() */
#define DBG_EA_GETJOBRESULT_ENTRY()
#endif

#ifndef DBG_EA_GETJOBRESULT_EXIT
/** \brief Exit point of function Ea_GetJobResult() */
#define DBG_EA_GETJOBRESULT_EXIT(a)
#endif

#ifndef DBG_EA_INVALIDATEBLOCK_ENTRY
/** \brief Entry point of function Ea_InvalidateBlock() */
#define DBG_EA_INVALIDATEBLOCK_ENTRY(a)
#endif

#ifndef DBG_EA_INVALIDATEBLOCK_EXIT
/** \brief Exit point of function Ea_InvalidateBlock() */
#define DBG_EA_INVALIDATEBLOCK_EXIT(a,b)
#endif

#ifndef DBG_EA_GETVERSIONINFO_ENTRY
/** \brief Entry point of function Ea_GetVersionInfo() */
#define DBG_EA_GETVERSIONINFO_ENTRY(a)
#endif

#ifndef DBG_EA_GETVERSIONINFO_EXIT
/** \brief Exit point of function Ea_GetVersionInfo() */
#define DBG_EA_GETVERSIONINFO_EXIT(a)
#endif

#ifndef DBG_EA_ERASEIMMEDIATEBLOCK_ENTRY
/** \brief Entry point of function Ea_EraseImmediateBlock() */
#define DBG_EA_ERASEIMMEDIATEBLOCK_ENTRY(a)
#endif

#ifndef DBG_EA_ERASEIMMEDIATEBLOCK_EXIT
/** \brief Exit point of function Ea_EraseImmediateBlock() */
#define DBG_EA_ERASEIMMEDIATEBLOCK_EXIT(a,b)
#endif

#ifndef DBG_EA_JOBENDNOTIFICATION_ENTRY
/** \brief Entry point of function Ea_JobEndNotification() */
#define DBG_EA_JOBENDNOTIFICATION_ENTRY()
#endif

#ifndef DBG_EA_JOBENDNOTIFICATION_EXIT
/** \brief Exit point of function Ea_JobEndNotification() */
#define DBG_EA_JOBENDNOTIFICATION_EXIT()
#endif

#ifndef DBG_EA_JOBERRORNOTIFICATION_EXIT
/** \brief Exit point of function Ea_JobErrorNotification() */
#define DBG_EA_JOBERRORNOTIFICATION_EXIT()
#endif

#ifndef DBG_EA_MAINFUNCTION_ENTRY
/** \brief Entry point of function Ea_MainFunction() */
#define DBG_EA_MAINFUNCTION_ENTRY()
#endif

#ifndef DBG_EA_MAINFUNCTION_EXIT
/** \brief Exit point of function Ea_MainFunction() */
#define DBG_EA_MAINFUNCTION_EXIT()
#endif

#ifndef DBG_EA_SFWRITESTARTMARKER_ENTRY
/** \brief Entry point of function Ea_SfWriteStartMarker() */
#define DBG_EA_SFWRITESTARTMARKER_ENTRY()
#endif

#ifndef DBG_EA_SFWRITESTARTMARKER_EXIT
/** \brief Exit point of function Ea_SfWriteStartMarker() */
#define DBG_EA_SFWRITESTARTMARKER_EXIT()
#endif

#ifndef DBG_EA_SFWRITEDATA_ENTRY
/** \brief Entry point of function Ea_SfWriteData() */
#define DBG_EA_SFWRITEDATA_ENTRY()
#endif

#ifndef DBG_EA_SFWRITEDATA_EXIT
/** \brief Exit point of function Ea_SfWriteData() */
#define DBG_EA_SFWRITEDATA_EXIT()
#endif

#ifndef DBG_EA_SFWRITEENDMARKER_ENTRY
/** \brief Entry point of function Ea_SfWriteEndMarker() */
#define DBG_EA_SFWRITEENDMARKER_ENTRY()
#endif

#ifndef DBG_EA_SFWRITEENDMARKER_EXIT
/** \brief Exit point of function Ea_SfWriteEndMarker() */
#define DBG_EA_SFWRITEENDMARKER_EXIT()
#endif

#ifndef DBG_EA_SFWRITEFINAL_ENTRY
/** \brief Entry point of function Ea_SfWriteFinal() */
#define DBG_EA_SFWRITEFINAL_ENTRY()
#endif

#ifndef DBG_EA_SFWRITEFINAL_EXIT
/** \brief Exit point of function Ea_SfWriteFinal() */
#define DBG_EA_SFWRITEFINAL_EXIT()
#endif

#ifndef DBG_EA_SFREADSTARTMARKER_ENTRY
/** \brief Entry point of function Ea_SfReadStartMarker() */
#define DBG_EA_SFREADSTARTMARKER_ENTRY()
#endif

#ifndef DBG_EA_SFREADSTARTMARKER_EXIT
/** \brief Exit point of function Ea_SfReadStartMarker() */
#define DBG_EA_SFREADSTARTMARKER_EXIT()
#endif

#ifndef DBG_EA_SFREADENDMARKER_ENTRY
/** \brief Entry point of function Ea_SfReadEndMarker() */
#define DBG_EA_SFREADENDMARKER_ENTRY()
#endif

#ifndef DBG_EA_SFREADENDMARKER_EXIT
/** \brief Exit point of function Ea_SfReadEndMarker() */
#define DBG_EA_SFREADENDMARKER_EXIT()
#endif

#ifndef DBG_EA_SFREADDATA_ENTRY
/** \brief Entry point of function Ea_SfReadData() */
#define DBG_EA_SFREADDATA_ENTRY()
#endif

#ifndef DBG_EA_SFREADDATA_EXIT
/** \brief Exit point of function Ea_SfReadData() */
#define DBG_EA_SFREADDATA_EXIT()
#endif

#ifndef DBG_EA_SFREADCHECKMARKER_ENTRY
/** \brief Entry point of function Ea_SfReadCheckMarker() */
#define DBG_EA_SFREADCHECKMARKER_ENTRY()
#endif

#ifndef DBG_EA_SFREADCHECKMARKER_EXIT
/** \brief Exit point of function Ea_SfReadCheckMarker() */
#define DBG_EA_SFREADCHECKMARKER_EXIT()
#endif

#ifndef DBG_EA_SFERASEIMMEDIATE_ENTRY
/** \brief Entry point of function Ea_SfEraseImmediate() */
#define DBG_EA_SFERASEIMMEDIATE_ENTRY()
#endif

#ifndef DBG_EA_SFERASEIMMEDIATE_EXIT
/** \brief Exit point of function Ea_SfEraseImmediate() */
#define DBG_EA_SFERASEIMMEDIATE_EXIT()
#endif

#ifndef DBG_EA_SFINVALIDATEERASE_ENTRY
/** \brief Entry point of function Ea_SfInvalidateErase() */
#define DBG_EA_SFINVALIDATEERASE_ENTRY()
#endif

#ifndef DBG_EA_SFINVALIDATEERASE_EXIT
/** \brief Exit point of function Ea_SfInvalidateErase() */
#define DBG_EA_SFINVALIDATEERASE_EXIT()
#endif

#ifndef DBG_EA_SFINVALIDATE_ENTRY
/** \brief Entry point of function Ea_SfInvalidate() */
#define DBG_EA_SFINVALIDATE_ENTRY()
#endif

#ifndef DBG_EA_SFINVALIDATE_EXIT
/** \brief Exit point of function Ea_SfInvalidate() */
#define DBG_EA_SFINVALIDATE_EXIT()
#endif

#ifndef DBG_EA_SFFINAL_ENTRY
/** \brief Entry point of function Ea_SfFinal() */
#define DBG_EA_SFFINAL_ENTRY()
#endif

#ifndef DBG_EA_SFFINAL_EXIT
/** \brief Exit point of function Ea_SfFinal() */
#define DBG_EA_SFFINAL_EXIT()
#endif

#ifndef DBG_EA_SFVLESTARTMARKER_ENTRY
/** \brief Entry point of function Ea_SfVleStartMarker() */
#define DBG_EA_SFVLESTARTMARKER_ENTRY()
#endif

#ifndef DBG_EA_SFVLESTARTMARKER_EXIT
/** \brief Exit point of function Ea_SfVleStartMarker() */
#define DBG_EA_SFVLESTARTMARKER_EXIT()
#endif

#ifndef DBG_EA_SFVLEENDMARKER_ENTRY
/** \brief Entry point of function Ea_SfVleEndMarker() */
#define DBG_EA_SFVLEENDMARKER_ENTRY()
#endif

#ifndef DBG_EA_SFVLEENDMARKER_EXIT
/** \brief Exit point of function Ea_SfVleEndMarker() */
#define DBG_EA_SFVLEENDMARKER_EXIT()
#endif

#ifndef DBG_EA_SFVLEFINAL_ENTRY
/** \brief Entry point of function Ea_SfVleFinal() */
#define DBG_EA_SFVLEFINAL_ENTRY()
#endif

#ifndef DBG_EA_SFVLEFINAL_EXIT
/** \brief Exit point of function Ea_SfVleFinal() */
#define DBG_EA_SFVLEFINAL_EXIT()
#endif

#ifndef DBG_EA_FINDINTERNALBLOCKNUMBER_ENTRY
/** \brief Entry point of function Ea_FindInternalBlockNumber() */
#define DBG_EA_FINDINTERNALBLOCKNUMBER_ENTRY(a,b)
#endif

#ifndef DBG_EA_FINDINTERNALBLOCKNUMBER_EXIT
/** \brief Exit point of function Ea_FindInternalBlockNumber() */
#define DBG_EA_FINDINTERNALBLOCKNUMBER_EXIT(a,b,c)
#endif

#ifndef DBG_EA_SEARCHAVAILABLECOPY_ENTRY
/** \brief Entry point of function Ea_SearchAvailableCopy() */
#define DBG_EA_SEARCHAVAILABLECOPY_ENTRY()
#endif

#ifndef DBG_EA_SEARCHAVAILABLECOPY_EXIT
/** \brief Exit point of function Ea_SearchAvailableCopy() */
#define DBG_EA_SEARCHAVAILABLECOPY_EXIT()
#endif

#ifndef DBG_EA_GETNEXTCOPY_ENTRY
/** \brief Entry point of function Ea_GetNextCopy() */
#define DBG_EA_GETNEXTCOPY_ENTRY()
#endif

#ifndef DBG_EA_GETNEXTCOPY_EXIT
/** \brief Exit point of function Ea_GetNextCopy() */
#define DBG_EA_GETNEXTCOPY_EXIT()
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined EA_TRACE_H) */
/*==================[end of file]===========================================*/
