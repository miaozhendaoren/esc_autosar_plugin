#if (!defined EA_CFG_H)
#define EA_CFG_H
/**
 * \file
 *
 * \brief AUTOSAR Ea
 *
 * This file contains the implementation of the AUTOSAR
 * module Ea.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[includes]==============================================*/
/* !LINKSTO EA083,1, EA113,1  */
#include <MemIf_Types.h>         /* Memory Abstraction Interface Types */
#include <Eep.h>                 /* EEPROM Header */
[!IF "((node:exists(EaGeneral/EaNvmJobErrorNotification)) and
(EaGeneral/EaNvmJobErrorNotification != '') and (EaGeneral/EaNvmJobErrorNotification != 'NULL_PTR'))
or ((node:exists(EaGeneral/EaNvmJobEndNotification)) and 
(EaGeneral/EaNvmJobEndNotification != '') and (EaGeneral/EaNvmJobEndNotification != 'NULL_PTR'))"!]
#include <NvM_Cbk.h>            /* NVRAM Manager callback API */
[!ENDIF!]
/*==================[macros]================================================*/

/** \brief Flag to en/disable development error detection for the Ea */
#define EA_DEV_ERROR_DETECT      [!//
[!IF "EaGeneral/EaDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Provide API function Ea_GetVersionInfo() */
#define EA_VERSION_INFO_API      [!//
[!IF "EaGeneral/EaVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Indicates if polling is enabled or not. */
#define EA_POLLING_MODE          [!//
[!IF "EaGeneral/EaPollingMode = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief size of virtual page in bytes. */
#define EA_VIRTUAL_PAGE_SIZE     [!"EaGeneral/EaVirtualPageSize"!]U

/** \brief Indicates if Ea_SetMode() is enabled or not. */
#define EA_SET_MODE_SUPPORTED    [!//
[!IF "EaGeneral/EaSetModeSupported = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!NOCODE!][!//
--- Get general configuration parameters
[!VAR "thisVirtualPageSize" = "EaGeneral/EaVirtualPageSize"!]
---  Generate internal buffer size
[!IF "node:exists(EaGeneral/EaBuffer) and (num:i(EaGeneral/EaBuffer) >= num:i(2*$thisVirtualPageSize))"!]
  [!VAR "thisTempBufferLength" = "num:i(EaGeneral/EaBuffer)"!]
[!ELSE!]
  [!VAR "thisTempBufferLength" = "num:i(2*$thisVirtualPageSize)"!]
[!ENDIF!]
--- Minimum buffer size is: 2 * 1 Byte (VLEC) + 1 Byte (IF) = 3 Byte
[!IF "$thisTempBufferLength < 3"!]
  [!VAR "thisTempBufferLength" = "num:i(3)"!]
[!ENDIF!]
[!ENDNOCODE!][!//
/** \brief Temporary buffer length */
#define EA_TEMPBUFFER_LENGTH     [!"$thisTempBufferLength"!]U

/** \brief Number of blocks configured. */
#define EA_NUMBEROFBLOCKS        [!"num:i(count(EaBlockConfiguration/*))"!]U

/** \brief Erase pattern as published by the Eep driver */
#define EA_EEP_ERASE_VALUE       [!//
[!"as:modconf('Eep')[1]/EepPublishedInformation/EepEraseValue"!]U

/* -----------[Callback function names to upper layer module]-------------- */

/** \brief Callback function in upper layer to indicate successful job
 * completion. */
#define EA_JOB_END_NFY_CBK()     [!//
[!IF "node:exists(EaGeneral/EaNvmJobEndNotification) and
(EaGeneral/EaNvmJobEndNotification != '') and (EaGeneral/EaNvmJobEndNotification != 'NULL_PTR')"!][!//
[!"EaGeneral/EaNvmJobEndNotification"!]()[!ELSE!]do {} while (0)[!ENDIF!]

/** \brief Callback function in upper layer to indicate unsuccessful job
 * completion. */
#define EA_JOB_ERROR_NTY_CBK()   [!//
[!IF "node:exists(EaGeneral/EaNvmJobErrorNotification) and
(EaGeneral/EaNvmJobErrorNotification != '') and (EaGeneral/EaNvmJobErrorNotification != 'NULL_PTR')"!][!//
[!"EaGeneral/EaNvmJobErrorNotification"!]()[!ELSE!]do {} while (0)[!ENDIF!]

/* -----------[Symbolic names for the Ea blocks]--------------------------- */

[!LOOP "EaBlockConfiguration/*"!][!//
/** \brief Symbolic name for the Ea Block Number [!"EaBlockNumber"!] */
#define EaConf_EaBlockConfiguration_[!"name(.)"!]   [!"EaBlockNumber"!]U

#if (!defined EA_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]   [!"EaBlockNumber"!]U
/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define Ea_[!"@name"!] [!"EaBlockNumber"!]U
#endif /* !defined EA_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( EA_CFG_H ) */
/*==================[end of file]===========================================*/
