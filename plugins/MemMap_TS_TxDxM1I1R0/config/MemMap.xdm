<?xml version='1.0'?>
<datamodel version="3.0" 
           xmlns="http://www.tresos.de/_projects/DataModel2/08/root.xsd" 
           xmlns:a="http://www.tresos.de/_projects/DataModel2/08/attribute.xsd" 
           xmlns:v="http://www.tresos.de/_projects/DataModel2/06/schema.xsd" 
           xmlns:d="http://www.tresos.de/_projects/DataModel2/06/data.xsd">

  <d:ctr type="AUTOSAR" factory="autosar" 
         xmlns:ad="http://www.tresos.de/_projects/DataModel2/08/admindata.xsd" 
         xmlns:icc="http://www.tresos.de/_projects/DataModel2/08/implconfigclass.xsd" 
         xmlns:mt="http://www.tresos.de/_projects/DataModel2/11/multitest.xsd" >
    <d:lst type="TOP-LEVEL-PACKAGES">
      <d:ctr name="TS_TxDxM1I1R0" type="AR-PACKAGE">
        <d:lst type="ELEMENTS">
          <d:chc name="MemMap" type="AR-ELEMENT" value="MODULE-DEF">
            <v:ctr type="MODULE-DEF">
              <a:a name="ADMIN-DATA" type="ADMIN-DATA">
                <ad:ADMIN-DATA>
                  <ad:DOC-REVISIONS>
                    <ad:DOC-REVISION>
                      <ad:REVISION-LABEL>4.2.0</ad:REVISION-LABEL>
                      <ad:ISSUED-BY>AUTOSAR</ad:ISSUED-BY>
                      <ad:DATE>2011-11-09</ad:DATE>
                    </ad:DOC-REVISION>
                  </ad:DOC-REVISIONS>
                </ad:ADMIN-DATA>
              </a:a>
              <a:a name="DESC" value="EN: Configuration of the MemMap module."/>
              <a:a name="LOWER-MULTIPLICITY" value="1"/>
              <a:a name="RELEASE" value="asc:4.0"/>
              <a:a name="UPPER-MULTIPLICITY" value="1"/>
              <a:a name="UUID" value="50c67902-cf60-4ca0-826e-1db669058f3c"/>
              <v:ctr name="CommonPublishedInformation" type="IDENTIFIABLE">
                <a:a name="DESC">
                  <a:v>EN:
                    &lt;html&gt;
                      Common container, aggregated by all modules. It contains published information about vendor and versions.
                  &lt;/html&gt;</a:v>
                </a:a>
                <a:a name="LABEL" value="Common Published Information"/>
                <a:a name="UUID" 
                     value="ECUC:a00be3e0-8783-9123-2d52-1eb616737ca6"/>
                <v:var name="ArMajorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Major version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="AUTOSAR Major Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:948f3f2e-f129-4bc5-b4e1-7a7bdb8599e1"/>
                  <a:da name="DEFAULT" value="1"/>
                </v:var>
                <v:var name="ArMinorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Minor version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="AUTOSAR Minor Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:2893b920-59d5-4ac2-b2c1-e23742e66d70"/>
                  <a:da name="DEFAULT" value="4"/>
                </v:var>
                <v:var name="ArPatchVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Patch level version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="AUTOSAR Patch Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:6428eb9b-8790-488a-b9a3-0fba52d0f59e"/>
                  <a:da name="DEFAULT" value="0"/>
                </v:var>
                <v:var name="SwMajorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Major version number of the vendor specific implementation of the module.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Software Major Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:605b18ae-3f9a-41d4-9225-67c9c5f6fc34"/>
                  <a:da name="DEFAULT" value="1"/>
                </v:var>
                <v:var name="SwMinorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Minor version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Software Minor Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" value="a7fe44dc-ea25-4b8d-8fad-9fbcae86d56f"/>
                  <a:da name="DEFAULT" value="1"/>
                </v:var>
                <v:var name="SwPatchVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Patch level version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Software Patch Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" value="a318d2d9-0e75-49da-ac43-e7e4e682e2f9"/>
                  <a:da name="DEFAULT" value="3"/>
                </v:var>
                <v:var name="ModuleId" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Module ID of this module from Module List
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Numeric Module ID"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:78bc8362-080f-4253-b3da-804ab69a7154"/>
                  <a:da name="DEFAULT" value="195"/>
                </v:var>
                <v:var name="VendorId" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Vendor ID of the dedicated implementation of this module according to the AUTOSAR vendor list
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Vendor ID"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:01b0f467-6943-4558-b4f2-3fa1fad28449"/>
                  <a:da name="DEFAULT" value="1"/>
                </v:var>
                <v:var name="VendorApiInfix" type="STRING_LABEL">
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Vendor API Infix"/>
                  <a:a name="OPTIONAL" value="true"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:1c68a547-f24e-4a4e-9540-69fbd466ec89"/>
                  <a:a name="VISIBLE" value="false"/>
                  <a:da name="DEFAULT" value=""/>
                  <a:da name="ENABLE" value="false"/>
                </v:var>
                <v:var name="Release" type="STRING_LABEL">
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Release Information"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:1c68a547-f24e-4a4e-9540-69fbd533ec89"/>
                  <a:da name="DEFAULT" value=""/>
                </v:var>
              </v:ctr>
              <v:var name="IMPLEMENTATION_CONFIG_VARIANT" type="ENUMERATION">
                <a:a name="LABEL" value="Config Variant"/>
                <a:a name="UUID" value="9e76bfa9-a39b-4f6f-be31-a41f1be00276"/>
                <a:da name="DEFAULT" value="VariantPreCompile"/>
                <a:da name="RANGE" value="VariantPreCompile"/>
              </v:var>
              <v:lst name="MemMapAddressingModeSet" type="MAP">
                <v:ctr name="MemMapAddressingModeSet" type="IDENTIFIABLE">
                  <a:a name="DESC" 
                       value="EN: Defines a set of addressing modes which might apply to a SwAddrMethod."/>
                  <a:a name="UUID" value="999e0867-2d12-4af6-9ea3-ba96538fc0fa"/>
                  <v:lst name="MemMapSupportedAddressingMethodOption">
                    <v:var name="MemMapSupportedAddressingMethodOption" 
                           type="STRING">
                      <a:a name="DESC">
                        <a:v>&lt;html&gt;
                           This constrains the usage of this addressing mode set for Generic Mappings to swAddrMethods. The attribute option of a swAddrMethod mapped via MemMapGenericMapping to this MemMapAddressingModeSet shall be equal to one of the configured MemMapSupportedAddressMethodOption&apos;s.
                          &lt;/html&gt;</a:v>
                      </a:a>
                      <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                           type="IMPLEMENTATIONCONFIGCLASS">
                        <icc:v class="PreCompile">VariantPreCompile</icc:v>
                      </a:a>
                      <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                      <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                      <a:a name="UUID" 
                           value="e6f6a2d8-289a-402f-aaff-f26468e096ad"/>
                      <a:da name="INVALID" type="Multi">
                        <mt:regex 
                                  false="The value must match the regular expression &quot;[a-zA-Z]([a-zA-Z0-9]|_[a-zA-Z0-9])*_?&quot;" 
                                  expr="[a-zA-Z]([a-zA-Z0-9]|_[a-zA-Z0-9])*_?"/>
                      </a:da>
                    </v:var>
                  </v:lst>
                  <v:lst name="MemMapSupportedMemoryAllocationKeywordPolicy">
                    <v:var name="MemMapSupportedMemoryAllocationKeywordPolicy" 
                           type="ENUMERATION">
                      <a:a name="DESC">
                        <a:v>&lt;html&gt;
                           This constrains the usage of this addressing mode set for Generic Mappings to swAddrMethods. The attribute MemoryAllocationKeywordPolicy of a swAddrMethod mapped via MemMapGenericMapping to this MemMapAddressingModeSet shall be equal to one of the configured MemMapSupportedMemoryAllocationKeywordPolicy&apos;s.
                           &lt;ul&gt;
                            &lt;li&gt;MEMMAP_ALLOCATION_KEYWORD_POLICY_ADDR_METHOD_SHORT_NAME: The Memory Allocation Keyword is build with the short name of the SwAddrMethod. This is the default value if the atttribute does not exist in the SwAddrMethod.&lt;/li&gt;
                            &lt;li&gt;MEMMAP_ALLOCATION_KEYWORD_POLICY_ADDR_METHOD_SHORT_NAME_AND_ALIGNMENT: The Memory Allocation Keyword is build with the the short name of the SwAddrMethod and the alignment attribute of the MemorySection. This requests a separation of objects in memory dependent from the alignment and is not applicable for RunnableEntitys and BswSchedulableEntitys.&lt;/li&gt;
                           &lt;/ul&gt;
                          &lt;/html&gt;</a:v>
                      </a:a>
                      <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                           type="IMPLEMENTATIONCONFIGCLASS">
                        <icc:v class="PreCompile">VariantPreCompile</icc:v>
                      </a:a>
                      <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                      <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                      <a:a name="UUID" 
                           value="c8098583-95fb-4f00-8410-ad9061669e1e"/>
                      <a:da name="RANGE">
                        <a:v>MEMMAP_ALLOCATION_KEYWORD_POLICY_ADDR_METHOD_SHORT_NAME</a:v>
                        <a:v>MEMMAP_ALLOCATION_KEYWORD_POLICY_ADDR_METHOD_SHORT_NAME_AND_ALIGNMENT</a:v>
                      </a:da>
                    </v:var>
                  </v:lst>
                  <v:lst name="MemMapSupportedSectionInitializationPolicy">
                    <v:var name="MemMapSupportedSectionInitializationPolicy" 
                           type="STRING">
                      <a:a name="DESC">
                        <a:v>&lt;html&gt;
                           This constrains the usage of this addressing mode set for Generic Mappings to swAddrMethods. The sectionIntializationPolicy attribute value of a swAddrMethod mapped via MemMapGenericMapping to this MemMapAddressingModeSet shall be equal to one of the configured MemMapSupportedSectionIntializationPolicy&apos;s Please note that SectionInitializationPolicyType describes the intended initialization of MemorySections. The following values are standardized in AUTOSAR Methodology:
                           &lt;ul&gt;
                             &lt;li&gt;NO-INIT: No initialization and no clearing is performed. Such data elements must not be read before one has written a value into it. * &apos;&apos;&apos;INIT&apos;&apos;&apos;: To be used for data that are initialized by every reset to the specified value (initValue).&lt;/li&gt;
                             &lt;li&gt;POWER-ON-INIT: To be used for data that are initialized by &apos;Power On&apos; to the specified value (initValue). Note: there might be several resets between power on resets.&lt;/li&gt;
                             &lt;li&gt;CLEARED: To be used for data that are initialized by every reset to zero.&lt;/li&gt;
                             &lt;li&gt;POWER-ON-CLEARED: To be used for data that are initialized by &apos;Power On&apos; to zero. Note: there might be several resets between power on resets. Please note that the values are defined similar to the representation of enumeration types in the XML schema to ensure backward compatibility.&lt;/li&gt;
                           &lt;/ul&gt;
                          &lt;/html&gt;</a:v>
                      </a:a>
                      <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                           type="IMPLEMENTATIONCONFIGCLASS">
                        <icc:v class="PreCompile">VariantPreCompile</icc:v>
                      </a:a>
                      <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                      <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                      <a:a name="UUID" 
                           value="ae3f1991-e801-4f7b-8c81-a0dee12d96ce"/>
                    </v:var>
                  </v:lst>
                  <v:lst name="MemMapSupportedSectionType">
                    <v:var name="MemMapSupportedSectionType" type="ENUMERATION">
                      <a:a name="DESC">
                        <a:v>&lt;html&gt;
                           This constrains the usage of this addressing mode set for Generic Mappings to swAddrMethods. The attribute sectionType of a swAddrMethod mapped via MemMapGenericMapping or MemMapSectionSpecificMapping to this MemMapAddressingModeSet shall be equal to one of the configured MemMapSupportedSectionType&apos;s.
                           &lt;ul&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_CALIBRATION_OFFLINE: Program data which can only be used for offline calibration. Note: This value is deprecated and shall be substituted by calPrm.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_CALIBRATION_ONLINE: Program data which can be used for online calibration. Note: This value is deprecated and shall be substituted by calPrm.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_CAL_PRM: To be used for calibratable constants of ECU-functions.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_CODE: To be used for mapping code to application block, boot block, external flash etc.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_CONFIG_DATA: Constants with attributes that show that they reside in one segment for module configuration.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_CONST: To be used for global or static constants.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_EXCLUDE_FROM_FLASH: Values existing in the ECU but not dropped down in the binary file. No upload should be needed to obtain access to the ECU data. The ECU will never be touched by the instrumentation tool, with the exception of upload. These are memory areas which are not overwritten by downloading the executable.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_USER_DEFINED: No specific categorization of sectionType possible. Note: This value is deprecated and shall be substituted by var, code, const, calPrm, configData, excludeFromFlash and the appropriate values of the orthogonal attributes sectionInitializationPolicy, memoryAllocationKeywordPolicy and option.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_VAR: To be used for global or static variables. The expected initialization is specified with the attribute sectionInitializationPolicy.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_VAR_FAST: To be used for all global or static variables that have at least one of the following properties: - accessed bit-wise - frequently used - high number of accesses in source code Some platforms allow the use of bit instructions for variables located in this specific RAM area as well as shorter addressing instructions. This saves code and runtime. Note: This value is deprecated and shall be substituted by var and the appropriate values of the orthogonal attributes sectionInitializationPolicy, memoryAllocationKeywordPolicy and option.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_VAR_NO_INIT: To be used for all global or static variables that are never initialized. Note: This value is deprecated and shall be substituted by var and the appropriate values of the orthogonal attributes sectionInitializationPolicy, memoryAllocationKeywordPolicy and option.&lt;/li&gt;
                              &lt;li&gt;MEMMAP_SECTION_TYPE_VAR_POWER_ON_INIT: To be used for all global or static variables that are initialized only after power on reset. Note: This value is deprecated and shall be substituted by var and the appropriate values of the orthogonal attributes sectionInitializationPolicy, memoryAllocationKeywordPolicy and option.&lt;/li&gt;
                           &lt;/ul&gt;
                          &lt;/html&gt;</a:v>
                      </a:a>
                      <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                           type="IMPLEMENTATIONCONFIGCLASS">
                        <icc:v class="PreCompile">VariantPreCompile</icc:v>
                      </a:a>
                      <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                      <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                      <a:a name="UUID" 
                           value="e7ec5746-9a04-4687-a058-2808b5b45cc9"/>
                      <a:da name="RANGE">
                        <a:v>MEMMAP_SECTION_TYPE_CALIBRATION_OFFLINE</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_CALIBRATION_ONLINE</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_CAL_PRM</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_CODE</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_CONFIG_DATA</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_CONST</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_EXCLUDE_FROM_FLASH</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_USER_DEFINED</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_VAR</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_VAR_FAST</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_VAR_NO_INIT</a:v>
                        <a:v>MEMMAP_SECTION_TYPE_VAR_POWER_ON_INIT</a:v>
                      </a:da>
                    </v:var>
                  </v:lst>
                  <v:lst name="MemMapAddressingMode" type="MAP">
                    <a:da name="MIN" value="1"/>
                    <v:ctr name="MemMapAddressingMode" type="IDENTIFIABLE">
                      <a:a name="DESC" 
                           value="EN: Defines a addressing mode with a set of #pragma statements implementing the start and the stop of a section."/>
                      <a:a name="UUID" 
                           value="e349d2d1-b97b-4a8c-98c6-9daf03dfc060"/>
                      <v:var name="MemMapAddressingModeStart" 
                             type="MULTILINE-STRING">
                        <a:a name="DESC" 
                             value="EN: Defines a set of #pragma statements implementing the start of a section."/>
                        <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                             type="IMPLEMENTATIONCONFIGCLASS">
                          <icc:v class="PreCompile">VariantPreCompile</icc:v>
                        </a:a>
                        <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                        <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                        <a:a name="UUID" 
                             value="c16653b7-a115-4bfc-868d-fb67792f398d"/>
                      </v:var>
                      <v:var name="MemMapAddressingModeStop" 
                             type="MULTILINE-STRING">
                        <a:a name="DESC" 
                             value="EN: Defines a set of #pragma statements implementing the end of a section."/>
                        <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                             type="IMPLEMENTATIONCONFIGCLASS">
                          <icc:v class="PreCompile">VariantPreCompile</icc:v>
                        </a:a>
                        <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                        <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                        <a:a name="UUID" 
                             value="1246b49d-2169-4019-860c-0366c28691c7"/>
                      </v:var>
                      <v:lst name="MemMapAlignmentSelector">
                        <a:da name="MIN" value="1"/>
                        <v:var name="MemMapAlignmentSelector" type="STRING">
                          <a:a name="DESC">
                            <a:v>&lt;html&gt;
                                EN: Defines a the alignments for which the MemMapAddressingMode applies. Allowed values are:
                                 &lt;ul&gt;
                                  &lt;li&gt;Numerical values (decimal, hexadecimal, octal, dual)&lt;/li&gt;
                                  &lt;li&gt;&lt;tt&gt;BOOLEAN&lt;/tt&gt;&lt;/li&gt;
                                  &lt;li&gt;&lt;tt&gt;UNSPECIFIED&lt;/tt&gt;&lt;/li&gt;
                                  &lt;li&gt;&lt;tt&gt;UNKNOWN&lt;/tt&gt;&lt;/li&gt;
                                 &lt;/ul&gt;
                              &lt;/html&gt;</a:v>
                          </a:a>
                          <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                               type="IMPLEMENTATIONCONFIGCLASS">
                            <icc:v class="PreCompile">VariantPreCompile</icc:v>
                          </a:a>
                          <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                          <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                          <a:a name="UUID" 
                               value="c3164fc9-db99-418c-ac42-6715ca2ad4b5"/>
                          <a:da name="INVALID" type="Multi">
                            <mt:regex 
                                      false="The value must match the regular expression &quot;[1-9][0-9]*|0x[0-9a-f]*|0[0-7]*|0b[0-1]*|UNSPECIFIED|UNKNOWN|BOOLEAN|&quot;" 
                                      expr="[1-9][0-9]*|0x[0-9a-f]*|0[0-7]*|0b[0-1]*|UNSPECIFIED|UNKNOWN|BOOLEAN|"/>
                          </a:da>
                        </v:var>
                      </v:lst>
                    </v:ctr>
                  </v:lst>
                </v:ctr>
              </v:lst>
              <v:lst name="MemMapAllocation" type="MAP">
                <v:ctr name="MemMapAllocation" type="IDENTIFIABLE">
                  <a:a name="DESC" 
                       value="EN: Defines the mapping of MemMapAddressingModeSets to SwAddrMethods."/>
                  <a:a name="UUID" value="a4861c5c-2c1c-4493-95d6-e1512cc335bc"/>
                  <v:lst name="MemMapGenericMapping" type="MAP">
                    <v:ctr name="MemMapGenericMapping" type="IDENTIFIABLE">
                      <a:a name="DESC" 
                           value="EN: Defines which SwAddrMethod is implemented with which MemMapAddressingModeSet."/>
                      <a:a name="UUID" 
                           value="e77dd9b4-29c7-4d9e-9685-ba311c319fae"/>
                      <v:ref name="MemMapSwAddressMethodRef" 
                             type="FOREIGN-REFERENCE">
                        <a:a name="DESC" 
                             value="EN: Reference to the SwAddrMethod which applies to the MemMapGenericMapping."/>
                        <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                             type="IMPLEMENTATIONCONFIGCLASS">
                          <icc:v class="PreCompile">VariantPreCompile</icc:v>
                        </a:a>
                        <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                        <a:a name="UUID" 
                             value="39f314ca-afc2-4a22-a0e5-0d5e2340487b"/>
                        <a:da name="REF" value="ASTyped:SwAddrMethod"/>
                      </v:ref>
                      <v:ref name="MemMapAddressingModeSetRef" type="REFERENCE">
                        <a:a name="DESC" 
                             value="EN: Reference to the MemMapAddressingModeSet which applies to the MemMapGenericMapping."/>
                        <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                             type="IMPLEMENTATIONCONFIGCLASS">
                          <icc:v class="PreCompile">VariantPreCompile</icc:v>
                        </a:a>
                        <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                        <a:a name="UUID" 
                             value="935487ee-32f2-4d50-9c82-705fd2cd8be4"/>
                        <a:da name="REF" 
                              value="ASPathDataOfSchema:/AUTOSAR/EcucDefs/MemMap/MemMapAddressingModeSet"/>
                      </v:ref>
                    </v:ctr>
                  </v:lst>
                  <v:lst name="MemMapSectionSpecificMapping" type="MAP">
                    <v:ctr name="MemMapSectionSpecificMapping" 
                           type="IDENTIFIABLE">
                      <a:a name="DESC" 
                           value="EN: Defines which MemorySection of a BSW Module or a Software Component is implemented with which MemMapAddressingModeSet."/>
                      <a:a name="UUID" 
                           value="8638655d-8fd6-464c-b42e-29f20083ce2d"/>
                      <v:ref name="MemMapMemorySectionRef" 
                             type="FOREIGN-REFERENCE">
                        <a:a name="DESC" 
                             value="EN: Reference to the MemorySection which applies to the MemMapSectionSpecificMapping."/>
                        <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                             type="IMPLEMENTATIONCONFIGCLASS">
                          <icc:v class="PreCompile">VariantPreCompile</icc:v>
                        </a:a>
                        <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                        <a:a name="UUID" 
                             value="0bdc0040-328b-4e21-a62a-50abec0d89a9"/>
                        <a:da name="REF" value="ASTyped:MemorySection"/>
                      </v:ref>
                      <v:ref name="MemMapAddressingModeSetRef" type="REFERENCE">
                        <a:a name="DESC" 
                             value="EN: Reference to the MemMapAddressingModeSet which applies to the MemMapModuleSectionSpecificMapping."/>
                        <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                             type="IMPLEMENTATIONCONFIGCLASS">
                          <icc:v class="PreCompile">VariantPreCompile</icc:v>
                        </a:a>
                        <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                        <a:a name="UUID" 
                             value="aacf4e55-8a55-49e3-b329-ee158ead2be6"/>
                        <a:da name="REF" 
                              value="ASPathDataOfSchema:/AUTOSAR/EcucDefs/MemMap/MemMapAddressingModeSet"/>
                      </v:ref>
                    </v:ctr>
                  </v:lst>
                </v:ctr>
              </v:lst>
              <v:ctr name="MemMapAS40Compatibility" type="IDENTIFIABLE">
                <v:var name="MemMapAS40Compatibility" type="BOOLEAN">
                  <a:a name="DESC">
                    <a:v>&lt;html&gt;
                         &lt;ul&gt;
                          &lt;li&gt;If enabled, MemMap macros for configuration data are defined as MODULENAME_START_CONFIG_DATA_ALIGNMENT&lt;/li&gt;
                          &lt;li&gt;If disabled, MemMap macros for configuration data are defined as MODULENAME_START_SEC_CONFIG_DATA_ALIGNMENT&lt;/li&gt;
                         &lt;/ul&gt;
                      &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       type="IMPLEMENTATIONCONFIGCLASS">
                    <icc:v class="PreCompile">VariantPreCompile</icc:v>
                  </a:a>
                  <a:a name="ORIGIN" value="Elektrobit Automotive Software"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:52d00b4f-174c-453b-8884-271b69712345"/>
                  <a:da name="DEFAULT" value="false"/>
                </v:var>
              </v:ctr>
              <v:lst name="MemMapHeaderFiles">
                <v:var name="MemMapHeaderFiles" type="STRING">
                  <a:a name="DESC" 
                       value="EN: A list of additional header files included by the generated MemMap.h. The files are included in the configured order."/>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       type="IMPLEMENTATIONCONFIGCLASS">
                    <icc:v class="PreCompile">VariantPreCompile</icc:v>
                  </a:a>
                  <a:a name="ORIGIN" value="Elektrobit Automotive Software"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" value="e6f6a2d8-289a-402f-aaff-f26468e12345"/>
                </v:var>
              </v:lst>
              <d:ref type="REFINED_MODULE_DEF" value="ASPath:/AUTOSAR/MemMap"/>
            </v:ctr>
          </d:chc>
          <d:chc name="myEcuParameterDefinition" type="AR-ELEMENT" 
                 value="ECU_PARAMETER_DEFINITION">
            <d:ctr type="AR-ELEMENT">
              <a:a name="DEF" 
                   value="ASPath:/AR_PACKAGE_SCHEMA/ECU_PARAMETER_DEFINITION"/>
              <d:lst name="MODULE_REF">
                <d:ref type="MODULE_REF" value="ASPath:/TS_TxDxM1I1R0/MemMap"/>
              </d:lst>
            </d:ctr>
          </d:chc>
        </d:lst>
      </d:ctr>
    </d:lst>
  </d:ctr>

</datamodel>
