/* Mk_TRICORE_writeallmpuregisters_tc161.s
 *
 * This file contains the function MK_WriteAllMpuRegisters for TC1.6E/P MPUs
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_writeallmpuregisters_tc161.s 18467 2015-02-20 14:48:52Z nibo2437 $
*/
#include <private/Mk_asm.h>		/* Must be first! */
#include <private/TRICORE/Mk_TRICORE_mpu_tc161.h>

	MK_file(Mk_TRICORE_writeallmpuregisters_tc161.s)

	MK_global	MK_WriteAllMpuRegisters

	MK_ASM_SDECL_TEXT
	MK_ASM_SECTION_TEXT
	MK_ASM_ALIGN_TEXT

/* MK_WriteAllMpuRegisters(const mk_mpubounds_t *bounds, const mk_mpurwxpermission_t * prsValues)
 *
 * This routine copies 24 pairs of lower and upper boundary values from the array referenced
 * by bounds (a4) to DPR[0-15]_[L,U] and CPR[0-7]_[L,U] registers and then writes the
 * parameters referenced by prsValues (a5) to the protection register set permission
 * registers [DPRE, DPWE, CPXE]_[0-3].
 *
 * All the values are loaded into data registers to start with. Then a synchronisation primitive
 * is executed to ensure that the transfers have taken place. After that, the values are copied
 * to the CSFRs. Finally another synchronisation primitive is executed to ensure that the register
 * bank has been completely programmed before returning. This is done in blocks of four pairs each.
*/
MK_WriteAllMpuRegisters:
	ld.w	d15, [a4]0						/* bounds[0].lower */
	ld.w	d14, [a4]4						/* bounds[0].upper */
	ld.w	d13, [a4]8						/* bounds[1].lower */
	ld.w	d12, [a4]12						/* bounds[1].upper */
	ld.w	d11, [a4]16						/* bounds[2].lower */
	ld.w	d10, [a4]20						/* bounds[2].upper */
	ld.w	d9,  [a4]24						/* bounds[3].lower */
	ld.w	d8,  [a4]28						/* bounds[3].upper */

	dsync									/* Ensure all register loads have taken place */

	mtcr	MK_imm(#, MK_DPR0_L), d15		/* DPR0_L = bounds[0].lower */
	mtcr	MK_imm(#, MK_DPR0_U), d14		/* DPR0_U = bounds[0].upper */
	mtcr	MK_imm(#, MK_DPR1_L), d13		/* DPR1_L = bounds[1].lower */
	mtcr	MK_imm(#, MK_DPR1_U), d12		/* DPR1_U = bounds[1].upper */
	mtcr	MK_imm(#, MK_DPR2_L), d11		/* DPR2_L = bounds[2].lower */
	mtcr	MK_imm(#, MK_DPR2_U), d10		/* DPR2_U = bounds[2].upper */
	mtcr	MK_imm(#, MK_DPR3_L), d9		/* DPR3_L = bounds[3].lower */
	mtcr	MK_imm(#, MK_DPR3_U), d8		/* DPR3_U = bounds[3].upper */

	isync									/* Ensure all mtcr have taken place */

	ld.w	d15, [a4]32						/* bounds[4].lower */
	ld.w	d14, [a4]36						/* bounds[4].upper */
	ld.w	d13, [a4]40						/* bounds[5].lower */
	ld.w	d12, [a4]44						/* bounds[5].upper */
	ld.w	d11, [a4]48						/* bounds[6].lower */
	ld.w	d10, [a4]52						/* bounds[6].upper */
	ld.w	d9,  [a4]56						/* bounds[7].lower */
	ld.w	d8,  [a4]60						/* bounds[7].upper */

	dsync									/* Ensure all register loads have taken place */

	mtcr	MK_imm(#, MK_DPR4_L), d15		/* DPR4_L = bounds[4].lower */
	mtcr	MK_imm(#, MK_DPR4_U), d14		/* DPR4_U = bounds[4].upper */
	mtcr	MK_imm(#, MK_DPR5_L), d13		/* DPR5_L = bounds[5].lower */
	mtcr	MK_imm(#, MK_DPR5_U), d12		/* DPR5_U = bounds[5].upper */
	mtcr	MK_imm(#, MK_DPR6_L), d11		/* DPR6_L = bounds[6].lower */
	mtcr	MK_imm(#, MK_DPR6_U), d10		/* DPR6_U = bounds[6].upper */
	mtcr	MK_imm(#, MK_DPR7_L), d9		/* DPR7_L = bounds[7].lower */
	mtcr	MK_imm(#, MK_DPR7_U), d8		/* DPR7_U = bounds[7].upper */

	isync									/* Ensure all mtcr have taken place */

	ld.w	d15, [a4]64						/* bounds[8].lower */
	ld.w	d14, [a4]68						/* bounds[8].upper */
	ld.w	d13, [a4]72						/* bounds[9].lower */
	ld.w	d12, [a4]76						/* bounds[9].upper */
	ld.w	d11, [a4]80						/* bounds[10].lower */
	ld.w	d10, [a4]84						/* bounds[10].upper */
	ld.w	d9,  [a4]88						/* bounds[11].lower */
	ld.w	d8,  [a4]92						/* bounds[11].upper */

	dsync									/* Ensure all register loads have taken place */

	mtcr	MK_imm(#, MK_DPR8_L), d15		/* DPR8_L = bounds[8].lower */
	mtcr	MK_imm(#, MK_DPR8_U), d14		/* DPR8_U = bounds[8].upper */
	mtcr	MK_imm(#, MK_DPR9_L), d13		/* DPR9_L = bounds[9].lower */
	mtcr	MK_imm(#, MK_DPR9_U), d12		/* DPR9_U = bounds[9].upper */
	mtcr	MK_imm(#, MK_DPR10_L), d11		/* DPR10_L = bounds[10].lower */
	mtcr	MK_imm(#, MK_DPR10_U), d10		/* DPR10_U = bounds[10].upper */
	mtcr	MK_imm(#, MK_DPR11_L), d9		/* DPR11_L = bounds[11].lower */
	mtcr	MK_imm(#, MK_DPR11_U), d8		/* DPR11_U = bounds[11].upper */

	isync									/* Ensure all mtcr have taken place */

	ld.w	d15, [a4]96						/* bounds[12].lower */
	ld.w	d14, [a4]100					/* bounds[12].upper */
	ld.w	d13, [a4]104					/* bounds[13].lower */
	ld.w	d12, [a4]108					/* bounds[13].upper */
	ld.w	d11, [a4]112					/* bounds[14].lower */
	ld.w	d10, [a4]116					/* bounds[14].upper */
	ld.w	d9,  [a4]120					/* bounds[15].lower */
	ld.w	d8,  [a4]124					/* bounds[15].upper */

	dsync									/* Ensure all register loads have taken place */

	mtcr	MK_imm(#, MK_DPR12_L), d15		/* DPR12_L = bounds[12].lower */
	mtcr	MK_imm(#, MK_DPR12_U), d14		/* DPR12_U = bounds[12].upper */
	mtcr	MK_imm(#, MK_DPR13_L), d13		/* DPR13_L = bounds[13].lower */
	mtcr	MK_imm(#, MK_DPR13_U), d12		/* DPR13_U = bounds[13].upper */
	mtcr	MK_imm(#, MK_DPR14_L), d11		/* DPR14_L = bounds[14].lower */
	mtcr	MK_imm(#, MK_DPR14_U), d10		/* DPR14_U = bounds[14].upper */
	mtcr	MK_imm(#, MK_DPR15_L), d9		/* DPR15_L = bounds[15].lower */
	mtcr	MK_imm(#, MK_DPR15_U), d8		/* DPR15_U = bounds[15].upper */

	isync									/* Ensure all mtcr have taken place */

	ld.w	d15, [a4]128					/* bounds[16].lower */
	ld.w	d14, [a4]132					/* bounds[16].upper */
	ld.w	d13, [a4]136					/* bounds[17].lower */
	ld.w	d12, [a4]140					/* bounds[17].upper */
	ld.w	d11, [a4]144					/* bounds[18].lower */
	ld.w	d10, [a4]148					/* bounds[18].upper */
	ld.w	d9,  [a4]152					/* bounds[19].lower */
	ld.w	d8,  [a4]156					/* bounds[19].upper */

	dsync									/* Ensure all register loads have taken place */

	mtcr	MK_imm(#, MK_CPR0_L), d15		/* CPR0_L = bounds[16].lower */
	mtcr	MK_imm(#, MK_CPR0_U), d14		/* CPR0_U = bounds[16].upper */
	mtcr	MK_imm(#, MK_CPR1_L), d13		/* CPR1_L = bounds[17].lower */
	mtcr	MK_imm(#, MK_CPR1_U), d12		/* CPR1_U = bounds[17].upper */
	mtcr	MK_imm(#, MK_CPR2_L), d11		/* CPR2_L = bounds[18].lower */
	mtcr	MK_imm(#, MK_CPR2_U), d10		/* CPR2_U = bounds[18].upper */
	mtcr	MK_imm(#, MK_CPR3_L), d9		/* CPR3_L = bounds[19].lower */
	mtcr	MK_imm(#, MK_CPR3_U), d8		/* CPR3_U = bounds[19].upper */

	isync									/* Ensure all mtcr have taken place */

	ld.w	d15, [a4]160					/* bounds[20].lower */
	ld.w	d14, [a4]164					/* bounds[20].upper */
	ld.w	d13, [a4]168					/* bounds[21].lower */
	ld.w	d12, [a4]172					/* bounds[21].upper */
	ld.w	d11, [a4]176					/* bounds[22].lower */
	ld.w	d10, [a4]180					/* bounds[22].upper */
	ld.w	d9,  [a4]184					/* bounds[23].lower */
	ld.w	d8,  [a4]188					/* bounds[23].upper */

	dsync									/* Ensure all register loads have taken place */

	mtcr	MK_imm(#, MK_CPR4_L), d15		/* CPR4_L = bounds[20].lower */
	mtcr	MK_imm(#, MK_CPR4_U), d14		/* CPR4_U = bounds[20].upper */
	mtcr	MK_imm(#, MK_CPR5_L), d13		/* CPR5_L = bounds[21].lower */
	mtcr	MK_imm(#, MK_CPR5_U), d12		/* CPR5_U = bounds[21].upper */
	mtcr	MK_imm(#, MK_CPR6_L), d11		/* CPR6_L = bounds[22].lower */
	mtcr	MK_imm(#, MK_CPR6_U), d10		/* CPR6_U = bounds[22].upper */
	mtcr	MK_imm(#, MK_CPR7_L), d9		/* CPR7_L = bounds[23].lower */
	mtcr	MK_imm(#, MK_CPR7_U), d8		/* CPR7_U = bounds[23].upper */

	isync									/* Ensure all mtcr have taken place */

	ld.w	d15, [a5]0						/* prsValues[0].dpre */
	ld.w	d14, [a5]4						/* prsValues[0].dpwe */
	ld.w	d13, [a5]8						/* prsValues[0].cpxe */
	ld.w	d12, [a5]12						/* prsValues[1].dpre */
	ld.w	d11, [a5]16						/* prsValues[1].dpwe */
	ld.w	d10, [a5]20						/* prsValues[1].cpxe */
	ld.w	d9,  [a5]24						/* prsValues[2].dpre */
	ld.w	d8,  [a5]28						/* prsValues[2].dpwe */
	ld.w	d7,  [a5]32						/* prsValues[2].cpxe */
	ld.w	d6,  [a5]36						/* prsValues[3].dpre */
	ld.w	d5,  [a5]40						/* prsValues[3].dpwe */
	ld.w	d4,  [a5]44						/* prsValues[3].cpxe */

	dsync									/* Ensure all register loads have taken place */

	mtcr	MK_imm(#, MK_DPRE_0), d15		/* DPRE_0 = prsValues[0].dpre */
	mtcr	MK_imm(#, MK_DPWE_0), d14		/* DPWE_0 = prsValues[0].dpwe */
	mtcr	MK_imm(#, MK_CPXE_0), d13		/* CPXE_0 = prsValues[0].cpxe */
	mtcr	MK_imm(#, MK_DPRE_1), d12		/* DPRE_1 = prsValues[1].dpre */
	mtcr	MK_imm(#, MK_DPWE_1), d11		/* DPWE_1 = prsValues[1].dpwe */
	mtcr	MK_imm(#, MK_CPXE_1), d10		/* CPXE_1 = prsValues[1].cpxe */
	mtcr	MK_imm(#, MK_DPRE_2), d9		/* DPRE_2 = prsValues[2].dpre */
	mtcr	MK_imm(#, MK_DPWE_2), d8		/* DPWE_2 = prsValues[2].dpwe */
	mtcr	MK_imm(#, MK_CPXE_2), d7		/* CPXE_2 = prsValues[2].cpxe */
	mtcr	MK_imm(#, MK_DPRE_3), d6		/* DPRE_3 = prsValues[3].dpre */
	mtcr	MK_imm(#, MK_DPWE_3), d5		/* DPWE_3 = prsValues[3].dpwe */
	mtcr	MK_imm(#, MK_CPXE_3), d4		/* CPXE_3 = prsValues[3].cpxe */

	isync									/* Ensure all mtcr have taken place */

	ret

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
