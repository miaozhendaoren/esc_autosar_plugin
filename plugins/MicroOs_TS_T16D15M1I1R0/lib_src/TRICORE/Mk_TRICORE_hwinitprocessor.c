/* Mk_TRICORE_hwinitprocessor.c
 *
 * This file contains the TRICORE processor initialisation function MK_HwInitProcessor()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_hwinitprocessor.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <private/Mk_startup.h>
#include <private/TRICORE/Mk_TRICORE_core.h>
#include <private/TRICORE/Mk_TRICORE_interruptcontroller.h>
#include <private/TRICORE/Mk_TRICORE_startup.h>
#include <private/TRICORE/Mk_TRICORE_compiler.h>
#include <private/Mk_panic.h>

/* MK_HwInitProcessor()
 *
 * This function is called from MK_StartKernel() before the first thread is started.
 * It performs processor specific initialization.
 *
 * For Tricore processors, no intialization is necessary. However, there are a few checks that
 * we can make ...
 *
 *	- both the exception vector and startup exception vector tables must be correctly aligned.
 *	- the interrupt vector table must be correctly aligned with an offset.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_HwInitProcessor, 1
 * !doctype src
*/
void MK_HwInitProcessor(void)
{
	mk_address_t addr;

	addr = (mk_address_t)&MK_ExceptionTable;

	if ( (addr & ~MK_EXCVEC_MASK) != 0u )
	{
		/* !LINKSTO Microkernel.TRICORE.Panic.MK_panic_MisalignedVectorTable, 1
		 * !doctype src
		*/
		MK_StartupPanic(MK_panic_MisalignedVectorTable);
	}

	addr = (mk_address_t)&MK_StartupExceptionTable;

	if ( (addr & ~MK_EXCVEC_MASK) != 0u )
	{
		/* !LINKSTO Microkernel.TRICORE.Panic.MK_panic_MisalignedVectorTable, 1
		 * !doctype src
		*/
		MK_StartupPanic(MK_panic_MisalignedVectorTable);
	}

	addr = (mk_address_t)&MK_InterruptTable;

	if ( (addr & ~MK_INTVEC_MASK) != MK_INTVEC_OFFSET )
	{
		/* !LINKSTO Microkernel.TRICORE.Panic.MK_panic_MisalignedVectorTable, 1
		 * !doctype src
		*/
		MK_StartupPanic(MK_panic_MisalignedVectorTable);
	}

	/* Check that we are running on the correct core */
	MK_CHECK_CORE_ID();
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
