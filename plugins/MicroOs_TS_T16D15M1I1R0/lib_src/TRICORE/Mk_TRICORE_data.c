/* Mk_TRICORE_data.c
 *
 * This file contains data definitioms for variables used specifically by Tricore.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_data.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <private/TRICORE/Mk_TRICORE_interruptcontroller.h>

/* MK_intVectorCode - the interrupt vector code
 *
 * The function MK_TricoreDispatchInterruptSoft() accepts the vector code (range 0-254) from
 * the entry code at the interrupt vector as a parameter, and stores it here.
 *
 * The hardware-independent MK_DispatchInterruptSoft() retrieves the code from here by means of the
 * MK_HwGetVectorCode() macro.
 *
 * This procedure serves to convert Tricore's hardware vectoring into the software vectoring
 * method preferred by the microkernel.
*/
mk_hwvectorcode_t MK_intVectorCode;

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
