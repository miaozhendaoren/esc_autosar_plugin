/* Mk_TRICORE_usrgettaskid.s
 *
 * This file contains the function MK_UsrGetTaskId()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_usrgettaskid.s 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <private/Mk_asm.h>		/* Must be first! */
#include <private/Mk_syscall.h>

	MK_file(Mk_TRICORE_usrgettaskid.s)

	MK_ASM_SDECL_TEXT
	MK_ASM_SECTION_TEXT
	MK_ASM_ALIGN_TEXT

	MK_global MK_UsrGetTaskId

MK_UsrGetTaskId:
	syscall	MK_imm(#, MK_SC_GetTaskId)
	ret
