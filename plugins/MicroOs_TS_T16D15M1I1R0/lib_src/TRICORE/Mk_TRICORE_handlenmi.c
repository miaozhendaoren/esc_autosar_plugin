/* Mk_TRICORE_handlenmi.c
 *
 * This file contains the function MK_HandleNmi()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_handlenmi.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <public/Mk_basic_types.h>
#include <private/TRICORE/Mk_TRICORE_exceptionhandling.h>
#include <public/TRICORE/Mk_TRICORE_exceptioninfo.h>
#include <public/Mk_error.h>
#include <private/Mk_panic.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_exceptionhandling.h>
#include <private/Mk_thread.h>

#include <private/TRICORE/Mk_TRICORE_exceptionfunctions.h>

/* MK_HandleNmi()
 *
 * This function is entered via a jump instruction from the non-maskable interrupt exception vector.
 * The vector provides the thread's PCXI value and the TIN as parameters. The TIN is probably undefined.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_HandleNmi, 1
 * !doctype src
*/
void MK_HandleNmi(mk_uint32_t pcxiValue, mk_uint32_t d15Value)
{
	MK_ExceptionHandlerCommon(pcxiValue, d15Value, MK_FALSE); /* Doesn't return if exception is not "sane" */
	MK_FillExceptionInfo(MK_exc_NMI, d15Value);

	/* !LINKSTO Microkernel.TRICORE.Panic.MK_panic_UndocumentedException, 1
	 * !doctype src
	*/
	MK_Panic(MK_panic_UndocumentedException);

	MK_Dispatch();
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
