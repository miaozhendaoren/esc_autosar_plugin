/* Mk_TRICORE_dispatchinterruptsoft.c
 *
 * This file contains the function MK_TricoreDispatchInterruptSoft()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_dispatchinterruptsoft.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <public/Mk_basic_types.h>
#include <private/TRICORE/Mk_TRICORE_exceptionhandling.h>
#include <private/TRICORE/Mk_TRICORE_interruptcontroller.h>
#include <private/Mk_softvector.h>

#include <private/TRICORE/Mk_TRICORE_exceptionfunctions.h>

/* MK_TricoreDispatchInterruptSoft()
 *
 * This function is entered via a jump instruction from every interrupt vector.
 * The vector provides the thread's PCXI value and the interrupt vector number as parameters.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_TricoreDispatchInterruptSoft, 1
 * !doctype src
*/
void MK_TricoreDispatchInterruptSoft(mk_uint32_t pcxiValue, mk_hwvectorcode_t vectorCode)
{
	MK_ExceptionHandlerCommon(pcxiValue, vectorCode, MK_FALSE); /* Doesn't return if exception is not "sane" */

	MK_intVectorCode = vectorCode;

	MK_DispatchInterruptSoft();
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
