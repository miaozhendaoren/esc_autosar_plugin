/* Mk_TRICORE_writeendinit_tc2xx.c
 *
 * This file contains the functions MK_WriteEndinit() and MK_WriteSafetyEndinit() for
 * the endinit mechanism as used by the current TC2xx "Aurix" processors.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_writeendinit_tc2xx.c 16698 2014-07-10 14:18:25Z nibo2437 $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_LAYOUT_020]
 *  Source files shall contain at most one routine having external linkage.
 *
 * Reason:
 *  This file contains two external functions that are merely small interfaces to the
 *  common helper function they both use.
*/

/* Deviation DCG-1 <*> */

#include <public/Mk_basic_types.h>
#include <private/Mk_panic.h>
#include <private/TRICORE/Mk_TRICORE_wdt.h>


static void MK_DoWriteEndinit(mk_wdtcon_t *, mk_uint32_t);

/* MK_DoWriteEndinit
 *
 * This function sets the ENDINIT-flag in the references wdtcon-register set to the value given in v.
 *
 * Currently only plain ENDINIT-handling (without LFSR-password) is implemented
*/
static void MK_DoWriteEndinit(mk_wdtcon_t *wdtcon, mk_uint32_t v)
{
	mk_uint32_t ei = 0;
	mk_uint32_t wdtcon0;
	mk_uint32_t password;
	mk_uint32_t reload;
	mk_uint32_t modify;
	mk_uint32_t wdtStatus;

	if ( v != 0 )
	{
		ei = MK_ENDINIT;
	}

	/* To write the ENDINIT-bit, we need to perform a "password"-protected
	 * modify access to the wdtXcon0-register.
    */

	/* Read the status and wdtcon0 registers
	*/
	wdtStatus = wdtcon->wdtxssr;
	wdtcon0 = wdtcon->wdtxcon0;

	/* Read password and watchdog timer reload value.
	*/
	password = (wdtcon0 & MK_WDTPW1_MSK) | ((~wdtcon0) & MK_WDTPW0_MSK);
	reload = wdtcon0 & MK_WDTREL_MSK;

	/* Calculate value for "modify access" (LCK set, ENDINIT as requested)
	*/
	modify = reload | password | MK_WDTLCK | ei;

	/* Calculate password for "password access", i.e. perform password sequencing and time guessing
	 * if the hardware expects so.
	*/
	if ( (wdtStatus & MK_WDT_PAS) != 0 )
	{
		mk_uint32_t lsb;

		/* Calculate XOR of bits 15, 14, 13 and 3 into bit 2. The other bits contain junk after all the
		 * shifting and XORing, so discard them.
		*/
		lsb = ((password >> 13) ^ (password >> 12) ^ (password >> 11) ^ (password >> 1)) & MK_WDTPW_LSB;

		/* Shift the password one to left and add newly calculated LSB.
		*/
		password = (password << 1) & MK_WDTPW_MSK;
		password = password | lsb;

		/* password now contains the next password of the auto-generated sequence.
		*/
	}

	if ( (wdtStatus & MK_WDT_TCS) != 0 )
	{
		/* Calculate the time estimate for the password. This uses the fact that the TIM field of
		 * WDTxSR is in the same place (upper 16 bits) as the REL field of WDTxCON0.
		 * The TIMEGUESS must be shifted up there too.
		*/
		reload =  (~(wdtcon->wdtxssr + MK_WDT_TIMEGUESS)) & MK_WDTREL_MSK;
	}

	/* Unlock wdtcon0 (LCK not set, ENDINIT set).
	*/
	wdtcon->wdtxcon0 = reload | password | MK_ENDINIT;

	/* Write requested ENDINIT-value, i.e. perform "modify access".
	*/
	wdtcon->wdtxcon0 = modify;

	/* Read back value; this is done to give the hardware enough time to process it.
	*/
	if ( (wdtcon->wdtxcon0 & MK_ENDINIT) != ei )
	{
		MK_StartupPanic(MK_panic_UnexpectedHardwareResponse);
	}
}

/* MK_WriteEndinit
 *
 * Sets the ENDINIT flag on the core the microkernel is running on.
*/
/* !LINKSTO Microkernel.TRICORE.Function.MK_WriteEndInit, 1
 * !doctype src
*/
void MK_WriteEndinit(mk_uint32_t v)
{
	MK_DoWriteEndinit(MK_scu_wdtxcon, v);
}

/* MK_WriteSafetyEndinit
 *
 * Sets the system-wide Safety-ENDINIT flag.
*/
void MK_WriteSafetyEndinit(mk_uint32_t v)
{
	MK_DoWriteEndinit(MK_scu_wdtscon, v);
}


/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
