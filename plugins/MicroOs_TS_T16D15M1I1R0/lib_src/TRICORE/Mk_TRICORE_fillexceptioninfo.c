/* Mk_TRICORE_fillexceptioninfo.c
 *
 * This file contains the function Mk_FillExceptionInfo()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_fillexceptioninfo.c 15947 2014-04-28 10:16:48Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 10.1 (required)
 *  The value of an expression of integer type shall not be implicitly converted to a
 *  different underlying type if [...] it is not a conversion to a wider integer type
 *  of the same signedness, [...]
 *
 * Reason:
 *   This expression uses the representation of its input values (an enum and a uint32) to create
 *   value for another enum representing a combination of the two. Considering the valid input
 *   values for this expression, the calculation will always be correct on 32-bit TRICORE processors.
 *   Furthermore, the resulting value is never used by the microkernel: it is solely a debugging
 *   aid for the user, so that exceptions can be identified by name in the debugger.
 *
*/
#include <public/Mk_public_types.h>
#include <private/Mk_exceptionhandling.h>
#include <private/Mk_thread.h>

/* Mk_FillExceptionInfo()
 *
 * This function is called from the exception handlers. It fills the exception info structure
 * with the contents of registers that might be useful for diagnostics. In particular, registers
 * that are not accessible by threads running in user mode are copied.
 *
 * The function takes two parameters:
 * - the exception class (the vector number)
 * - the trap identification number TIN (from d15)
 *
 * The constant pointer to the exception info structure might be NULL, in which case
 * no action is taken.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_FillExceptionInfo, 1
 * !doctype src
*/
void MK_FillExceptionInfo(mk_exceptionclass_t xClass, mk_exceptiontin_t xTin)
{
	if ( MK_exceptionInfo != MK_NULL )
	{
		MK_exceptionInfo->excClass = xClass;
		MK_exceptionInfo->excTin = xTin;
		/* Deviation MISRA-1 */
		MK_exceptionInfo->type = MK_ExceptionClassTinToType(xClass, xTin);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
