/* Mk_TRICORE_hwsetstaticmemorypartition_tc161.c
 *
 * This file contains the function MK_HwSetStaticMemoryPartition() for Tricore.
 * The function programs the static memory partitions into the hardware.
 * This file is only used for Tricore Processors with a TC1.6E/P core architecture.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_hwsetstaticmemorypartition_tc161.c 18690 2015-03-05 12:13:57Z nibo2437 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 9.2 (required)
 *  Braces shall be used to indicate and match the structure in the non-zero initialisation
 *  of arrays and structures.
 *
 * Reason:
 *   This rule is reported as violated by some checkers. It does, however, not apply here,
 *   since the complete array is (intentionally) initialized to zero.
 *
*/
#include <public/Mk_basic_types.h>
#include <private/Mk_memoryprotection.h>
#include <private/Mk_panic.h>
#include <private/TRICORE/Mk_TRICORE_mpu.h>

#define MK_CPRS_OFFSET   (MK_TRICORE_NDPR)

static mk_int_t MK_alreadyProgrammedDprs[MK_TRICORE_NDPR];

static mk_uint32_t MK_GetRegionMask(mk_int_t);
static mk_int_t MK_AddPartToDpr(
	const mk_memorypartition_t *,
	mk_mpubounds_t[MK_TRICORE_NDPR],
	mk_mpurwxpermission_t *,
	mk_int_t 
);
static mk_int_t MK_AddPartToCpr(
	const mk_memorypartition_t *,
	mk_mpubounds_t[MK_TRICORE_NCPR],
	mk_mpurwxpermission_t *,
	mk_int_t
);

static mk_uint32_t MK_GetRegionMask(mk_int_t regionIdx)
{
	mk_int_t i = 0;
	mk_uint32_t mask = 0;

	while ( (i<MK_TRICORE_NDPR) && (MK_alreadyProgrammedDprs[i] != -1) )
	{
		if ( MK_alreadyProgrammedDprs[i] == regionIdx)
		{
			mask = 1u << i;
			break;
		}

		i++;
	}

	return mask;
}

static mk_int_t MK_AddPartToDpr(
	const mk_memorypartition_t *partition,
	mk_mpubounds_t dprx[MK_TRICORE_NDPR],
	mk_mpurwxpermission_t *permissions,
	mk_int_t startIdx
)
{
	mk_int_t i;
	mk_int_t regionIdx;
	mk_int_t nextIdx = startIdx;
	mk_uint32_t pMask = 1u << startIdx;
	mk_uint32_t fastMask;
	const mk_memoryregion_t *region;

	for ( i = 0;  i < partition->nRegions; i++ )
	{
		region = partition->regionMap[i].region;
		regionIdx = region - MK_memoryRegions;

		/* check if region is a data region */
		if ( ((region->mr_permissions & MK_TRICORE_PERM_RWMASK) != 0u ) )
		{
			fastMask = MK_GetRegionMask(regionIdx);
			if ( fastMask == 0 )
			{
				if ( nextIdx >= MK_TRICORE_NDPR )
				{
					/* !LINKSTO Microkernel.Panic.MK_panic_MemoryPartitionIsTooLarge, 1
					 * !doctype src
					 */
					MK_StartupPanic(MK_panic_MemoryPartitionIsTooLarge);
				}

				/* mark next region as used */
				MK_alreadyProgrammedDprs[nextIdx] = regionIdx;

				dprx[nextIdx].lower = (mk_uint32_t)region->mr_startaddr;
				dprx[nextIdx].upper = (mk_uint32_t)region->mr_limitaddr;

				if ( (region->mr_permissions & MK_TRICORE_PERM_READ) != 0u )
				{
					permissions->dpre |= pMask;
				}
				if ( (region->mr_permissions & MK_TRICORE_PERM_WRITE) != 0u )
				{
					permissions->dpwe |= pMask;
				}

				nextIdx++;
				pMask <<= 1;
			}
			else
			{
				if ( (region->mr_permissions & MK_TRICORE_PERM_READ) != 0u )
				{
					permissions->dpre |= fastMask;
				}
				if ( (region->mr_permissions & MK_TRICORE_PERM_WRITE) != 0u )
				{
					permissions->dpwe |= fastMask;
				}

			}
		}
	}

	return nextIdx;
}

static mk_int_t MK_AddPartToCpr(
	const mk_memorypartition_t *partition,
	mk_mpubounds_t cprx[MK_TRICORE_NCPR],
	mk_mpurwxpermission_t *permissions,
	mk_int_t startIdx
)
{
	mk_int_t i;
	mk_int_t nextIdx = startIdx;
	mk_uint32_t pMask = 1u << startIdx;
	const mk_memoryregion_t *region;

	for ( i = 0;  i < partition->nRegions; i++ )
	{
		region = partition->regionMap[i].region;

		/* check if region is a code region */
		if ( (region->mr_permissions & MK_TRICORE_PERM_EXECUTE) != 0u )
		{
			if ( nextIdx >= MK_TRICORE_NCPR )
			{
				/* !LINKSTO Microkernel.Panic.MK_panic_MemoryPartitionIsTooLarge, 1
				 * !doctype src
				 */
				MK_StartupPanic(MK_panic_MemoryPartitionIsTooLarge);
			}

			cprx[nextIdx].lower = (mk_uint32_t)region->mr_startaddr;
			cprx[nextIdx].upper = (mk_uint32_t)region->mr_limitaddr;

			permissions->cpxe |= pMask;

			nextIdx++;
			pMask <<= 1;
		}
	}

	return nextIdx;
}


/* MK_HwSetStaticMemoryPartition() - programs the static memory partitions into the hardware.
 *
 * The parameter indicates the global data partition. Another partition is assumed for the kernel's
 * memory regions.
 *
 * !LINKSTO Microkernel.TRICORE.MPU.GlobalPartition, 1,
 * !        Microkernel.TRICORE.MPU.KernelPartition, 1,
 * !        Microkernel.TRICORE.MPU.GlobalCodeProtection, 1,
 * !        Microkernel.TRICORE.Function.MK_HwSetStaticMemoryPartition, 1
 * !doctype src
*/
void MK_HwSetStaticMemoryPartition(mk_objectid_t partId)
{
	mk_mpurwxpermission_t permissions[4] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}}; /* PRS0..PRS3 */
	/* Deviation MISRA-1 */
	mk_mpubounds_t boundaryPairs[MK_TRICORE_NALLPR] = {{0,0},}; /* DPRx, CPRx */
	mk_int_t nextDprIdx = 0;
	mk_int_t nextCprIdx = 0;
	mk_int_t i;
	const mk_memorypartition_t *partition;

	/* mark all regions as not yet programmed */
	for ( i = 0; i<MK_TRICORE_NDPR; i++)
	{
		MK_alreadyProgrammedDprs[i] = -1;
	}

	/* Parse global Partition and add respective regions to CPR/DPR.
	*/
	for ( i = partId; i < MK_nStaticPartitions; i++ )
	{
		partition = &MK_memoryPartitions[i];
		nextDprIdx = MK_AddPartToDpr(partition, &boundaryPairs[0], &permissions[i], nextDprIdx);
		nextCprIdx = MK_AddPartToCpr(partition, &boundaryPairs[MK_TRICORE_NDPR], &permissions[i], nextCprIdx);

		/* add global partition to fast partition */
		if ( i > MK_MEMPART_KERNEL )
		{
			permissions[i].dpre |= permissions[0].dpre;
			permissions[i].dpwe |= permissions[0].dpwe;
			permissions[i].cpxe |= permissions[0].cpxe;
		}
	}

	/* Verify configured PRS1 "empty" permissions (i.e., global only).
	*/
	if ( (permissions[0].dpre != MK_MemoryPartitionPermissions[partId].dpre) ||
	     (permissions[0].dpwe != MK_MemoryPartitionPermissions[partId].dpwe) )
	{
		/* !LINKSTO Microkernel.TRICORE.Panic.MK_panic_MpuNotInitializedCorrectly, 1
		 * !doctype src
		*/
		MK_StartupPanic(MK_panic_MpuNotInitializedCorrectly);
	}

	/* PRS0 needs to contain both global (currently permissions[0]) and kernel (currently permissions[1]) regions.
	*/
	permissions[0].dpre |= permissions[1].dpre;
	permissions[0].dpwe |= permissions[1].dpwe;
	permissions[0].cpxe |= permissions[1].cpxe;

	/* permissions[1] now contains the kernel's regions, but PRS1 should only contain the global permissions, so the
	 * global permissions need to be set and the kernel permissions need to be cleared. This is conveniently achieved
	 * using XOR with permissions[0] which already contains permissions for both partitions.
	*/
	permissions[1].dpre ^= permissions[0].dpre;
	permissions[1].dpwe ^= permissions[0].dpwe;
	permissions[1].cpxe ^= permissions[0].cpxe;

	/* Prepare MPU cache.
	*/
	MK_FillMpuCache();

	/* Call assembler function to actually write the MPU registers.
	*/
	MK_WriteAllMpuRegisters(boundaryPairs, permissions);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
