/* Mk_u_libgetcountervalue.c
 *
 * This file contains the GetCounterValue() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_libgetcountervalue.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <public/Mk_autosar.h>

/* GetCounterValue() - Implements the AUTOSAR service.
 *
 * This function calls the GetCounterValue API of the QM-OS via the microkernel's delegation interface.
 * If the returned status code is E_OK, the requested value is placed in the referenced TickType variable.
 * The function returns the status code.
 *
 * !LINKSTO      Microkernel.Function.GetCounterValue, 1
 * !doctype      src
*/
StatusType GetCounterValue(CounterType counterId, TickRefType tickRef)
{
	mk_statusandvalue_t syscallReturn = MK_UsrStartForeignThread2V(MK_SC_GetCounterValue,
															(mk_parametertype_t)counterId, (mk_parametertype_t)tickRef);

	if ( syscallReturn.statusCode == E_OK )
	{
		*tickRef = (TickType)syscallReturn.requestedValue;
	}

	return (StatusType)syscallReturn.statusCode;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
