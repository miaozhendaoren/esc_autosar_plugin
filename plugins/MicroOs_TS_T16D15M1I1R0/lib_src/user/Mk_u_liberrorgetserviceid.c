/* Mk_u_liberrorgetserviceid.c
 *
 * This file contains the MK_ErrorGetServiceId() function.
 *
 * This function returns the service ID from the error-information structure.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_liberrorgetserviceid.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <public/Mk_error.h>

/* MK_ErrorGetServiceId() returns the service ID from the global error-information structure.
 *
 * CAVEAT: the service ID only has meaning if an error has occurred.
 *
 * !LINKSTO Microkernel.Function.MK_ErrorGetServiceId, 1
 * !doctype src
*/
mk_serviceid_t MK_ErrorGetServiceId(void)
{
	mk_serviceid_t sid;

	if ( MK_errorInfo == MK_NULL )
	{
		sid = MK_sid_UnknownService;
	}
	else
	{
		sid = MK_errorInfo->serviceId;
	}

	return sid;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
