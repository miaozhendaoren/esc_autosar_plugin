/* Mk_u_libfinishselftest.c
 *
 * This file contains the MK_LibFinishSelftest() function.
 *
 * This function is meant to be called in the protection hook
 * to return with MK_PRO_TERMINATE if and only if the hook was invoked
 * due to the respective self test(s).
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_libfinishselftest.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <public/Mk_public_api.h>

/* !LINKSTO      Microkernel.Function.MK_LibFinishSelftest, 1
 * !doctype      src
*/
void MK_LibFinishSelftest(mk_uint32_t mask)
{
	if ( ( MK_selftestState & mask ) == mask )
	{
		MK_UsrTerminateSelfWithResult(MK_PRO_TERMINATE);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
