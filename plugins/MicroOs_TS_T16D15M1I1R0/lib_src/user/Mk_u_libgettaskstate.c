/* Mk_u_libgettaskstate.c
 *
 * This file contains the GetTaskState() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_libgettaskstate.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <public/Mk_autosar.h>

/* GetTaskState() - Implements the AUTOSAR service.
 *
 * This function calls the MK_UsrGetTaskState API of the microkernel.
 * If the returned status code is E_OK, the requested value is placed in the referenced TaskStateType variable
 * The function returns the status code.
 *
 * !LINKSTO      Microkernel.Function.GetTaskState, 1
 * !doctype      src
*/
StatusType GetTaskState(TaskType taskId, TaskStateRefType taskStateRef)
{
	mk_statusandvalue_t syscallReturn = MK_UsrGetTaskState(taskId, (mk_parametertype_t)taskStateRef);

	if ( syscallReturn.statusCode == E_OK )
	{
		*taskStateRef = (TaskStateType)syscallReturn.requestedValue;
	}

	return (StatusType)syscallReturn.statusCode;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
