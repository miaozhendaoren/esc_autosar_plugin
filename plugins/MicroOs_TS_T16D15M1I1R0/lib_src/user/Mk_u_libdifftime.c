/* Mk_u_libdifftime.c
 *
 * This file contains the MK_DiffTime() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_libdifftime.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>

/* MK_DiffTime() - calculates the difference between two absolute times.
 *
 * void MK_DiffTime(mk_time_t *diffTime, const mk_time_t *newTime, const mk_time_t *oldTime)
 * calculates the difference between the old time and the new time (*oldTime - *newTime) and
 * places the result in the referenced result variable (*diffTime).
 *
 * The calculation is arranged so that the output is written after the calculation has been
 * performed, so that the caller can save variable space by using the same location for diff
 * and old, or for diff and new (or even for all three, though that doesn't make much sense).
 *
 * !LINKSTO      Microkernel.Function.MK_DiffTime, 1
 * !doctype      src
*/
void MK_DiffTime(mk_time_t *diffTime, const mk_time_t *newTime, const mk_time_t *oldTime)
{
	/* Preliminary calculation of high-order word.
	*/
	diffTime->timeHi = newTime->timeHi - oldTime->timeHi;

	/* If the subtraction of the low-order words is going to underflow, the
	 * high-order word needs to be adjusted.
	*/
	if ( oldTime->timeLo > newTime->timeLo )
	{
		diffTime->timeHi -= 1;
	}

	/* This might underflow. If it does, we don't care because we have already adjusted for it.
	*/
	diffTime->timeLo = newTime->timeLo - oldTime->timeLo;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
