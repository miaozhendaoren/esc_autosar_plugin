/* MK_TRICORE_qmsetupclocks_tc2xx.c
 *
 * This file contains the function MK_QmSetupClocks() to initialize the TRICORE PLL and clock dividers.
 *
 * The PLL is normally initialized during startup -- probably from the board callout
 * function before the data sections are initialized.
 *
 * Warning: This file has not been developed in accordance with a safety standard (no ASIL)!
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_qmsetupclocks_tc2xx.c 16202 2014-05-21 14:13:28Z nibo2437 $
*/
#include <public/Mk_public_types.h>
#include <private/TRICORE/Mk_TRICORE_wdt.h>
#include <QM/TRICORE/Mk_TRICORE_pll.h>

/* Set initial K2-divier to 30 (--> value to be written is 29).
*/
#define MK_PLLCON1_K2_INITIAL	29

/* MK_QmSetupClocks
 *
 * This function takes the desired values for the SCU registers PLLCON0, PLLCON1, CCUCON0 and CCUCON1 as
 * parameters.
 *
 * It then configures the PLL according to the values from the PLLCON* parameters and finally programs the
 * CCU with the values from the respective parameters. The CCU parameters need to select the PLL clock as
 * input clock. Temporary settings for these registers are derived from the input parameters where necessary.
 *
 * Note: The implementation of this function follows the examples and recommendations provided in the TC27x
 *       User's Manual, Version 1.3.1, Sections 8.1.2.1 and 8.1.8.1
 *
*/
void MK_QmSetupClocks(mk_uint32_t pllcon0, mk_uint32_t pllcon1, mk_uint32_t ccucon0, mk_uint32_t ccucon1)
{
	mk_uint32_t pllcon1_temp = (pllcon1 & (~MK_PLLCON1_K2_MASK)) | MK_PLLCON1_K2_INITIAL;

	/* Writing to PLL registers requires ENDINIT-protection to be switched off.
	*/
//	MK_WriteEndinit(0);
    MK_WriteSafetyEndinit(0);    
#if 0
	/* Step 1 - Wait until OSC is stable.
	*/

	/* Note: The OSC bits never indicate a stable status on our Aurix-boards, but PLL _does_ get
	 * stable. Disable this check for now.
	*/

	while ( (MK_SCU_OSCCON & MK_OSCSTABLE) != MK_OSCSTABLE )
	{
	}
#endif

	/* Step 2 - Program the PLL
	*/

	/* Select PLL input clock.
	*/
	while ((MK_SCU_CCUCON1 & MK_CCUCON_LCK_LOCKED) != 0)
	{
		/* Wait until CCUCON1 is unlocked. */
	}
	MK_SCU_CCUCON1 = (MK_SCU_CCUCON1 & (~MK_CCUCON1_INSEL_MASK)) | (ccucon1 & MK_CCUCON1_INSEL_MASK);
	MK_SCU_CCUCON1 |= MK_CCUCON_UP;
	while ((MK_SCU_CCUCON1 & MK_CCUCON_LCK_LOCKED) != 0)
	{
		/* Wait until CCUCON1 is unlocked. */
	}

	/* Set K1 and switch to prescalar mode before changing P and N values.
	*/
	MK_SCU_PLLCON1 = (MK_SCU_PLLCON1 & (~MK_PLLCON1_K1_MASK)) | (pllcon1 & MK_PLLCON1_K1_MASK);
	while ((MK_SCU_PLLSTAT & MK_PLLSTAT_K1RDY) == 0)
	{
		/* Wait until K1 is ready. */
	}
	MK_SCU_PLLCON0 |= MK_PLLCON0_VCOBYP;
	while ((MK_SCU_PLLSTAT & MK_PLLSTAT_VCOBYST) == 0)
	{
		/* Wait until prescaler mode has been entered (VCOBYST==1). */
	}

	/* When configuring the PLL for the first time after a system reset it is recommended
	 * to disconnect the input clock f OSC before configuring P and / or N and to connect
	 * f OSC before checking for the lock status.
	 */
	MK_SCU_PLLCON0 |= MK_PLLCON0_SETFINDIS;	/* Disconnect VCO input. */
	MK_SCU_PLLCON0 = (MK_PLLCON0_SETFINDIS | pllcon0); /* Set user values; leave VCO disconnected */
	MK_SCU_PLLCON1 = pllcon1_temp;
	MK_SCU_PLLCON0 |= MK_PLLCON0_CLRFINDIS;	/* Re-connect VCO input. */

	while ((MK_SCU_PLLSTAT & MK_PLLSTAT_K2RDY) == 0)
	{
		/* Wait until K2 is ready. */
	}

	/* Step 3
	*/

	/* Reset the VCO Lock detection.
	*/
	MK_SCU_PLLCON0 |= MK_PLLCON0_RESLD;

	while ((MK_SCU_PLLSTAT & MK_PLLSTAT_VCOLOCK) == 0)
	{
		/* Wait for PLL lock. */
	}

	/* Switch to normal mode (i.e., disable bypass mode).
	*/
	MK_SCU_PLLCON0 &= (~MK_PLLCON0_VCOBYP);
	while ((MK_SCU_PLLSTAT & MK_PLLSTAT_VCOBYST) == MK_PLLSTAT_VCOBYST)
	{
		/* Wait until normal mode has been entered (VCOBYST==0). */
	}

	/* Now increase the frequency step-wise by reducing the K2 divider to desired value.
	 *
	 * Note: the algorithm here only works as long as K2 is in the least significant bits of PLLCON1
	*/
#if (MK_PLLCON1_K2_MASK != 0x0000007f)
#error "MK_QmSetupClocks() is not compatible with MK_PLLCON1_K2_MASK"
#endif
	while (pllcon1_temp > pllcon1)
	{
		volatile mk_uint32_t i;

		pllcon1_temp--; /* Effectively decrements K2. */
		MK_SCU_PLLCON1 = pllcon1_temp;

		while ((MK_SCU_PLLSTAT & MK_PLLSTAT_K2RDY) == 0)
		{
			/* Wait until K2 is ready. */
		}

		/* Between the update of two K2-Divider values 6 cycles of fPLL should be waited.
		*/
		for (i=0; i<10; i++)
		{
		}
	}

	/* Step 4: Configure CCU as requested but do not switch to PLL yet.
	*/

	while ( ((MK_SCU_CCUCON0 & MK_CCUCON_LCK_LOCKED) != 0) ||
	        ((MK_SCU_CCUCON1 & MK_CCUCON_LCK_LOCKED) != 0) )
	{
		/* Wait until CCUCON0 & CCUCON1 are unlocked. */
	}
	MK_SCU_CCUCON0 = (ccucon0 & (~MK_CCUCON0_CLKSEL_MASK));
	MK_SCU_CCUCON1 = ccucon1;
	while ( ((MK_SCU_CCUCON0 & MK_CCUCON_LCK_LOCKED) != 0) ||
	        ((MK_SCU_CCUCON1 & MK_CCUCON_LCK_LOCKED) != 0) )
	{
		/* Wait until CCUCON0 & CCUCON1 are unlocked. */
	}

	MK_SCU_CCUCON0 |= MK_CCUCON_UP;	/* Commit changes to CCUCON0 and CCUCON1. */

	/* Step 5: Switch CCU input clock.
	*/
	while ((MK_SCU_CCUCON0 & MK_CCUCON_LCK_LOCKED) != 0)
	{
		/* Wait until CCUCON0 is unlocked. */
	}
	MK_SCU_CCUCON0 = ccucon0;
	MK_SCU_CCUCON0 |= MK_CCUCON_UP;	/* Commit changes */

	/* CPU should now be running at full speed: set ENDINIT again.
	*/
//	MK_WriteEndinit(1);
    MK_WriteSafetyEndinit(1);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
