/* Mk_k_ppaquarantineobject.c
 *
 * This file contains the MK_PpaQuarantineObject() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppaquarantineobject.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_shutdown.h>

/* MK_PpaQuarantineObject() shall terminate all instances of the current object (as MK_PRO_TERMINATEALL does) and mark
 * the object as quarantined so that it cannot be restarted. This function does not quarantine the whole OS-Application.
 *
 * At the moment, this functionality is not yet implemented, therefore we just shut down.
 *
 * !LINKSTO Microkernel.Function.MK_PpaQuarantineObject, 1
 * !doctype src
*/
void MK_PpaQuarantineObject(void)
{
	MK_PpaShutdown();
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
