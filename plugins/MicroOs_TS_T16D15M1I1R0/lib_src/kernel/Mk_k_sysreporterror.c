/* Mk_k_sysreporterror.c
 *
 * This file contains the MK_SysReportError() function.
 *
 * This function is called by the system call function whenever the ReportError system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysreporterror.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <public/Mk_error.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_shutdown.h>

/*
 * MK_SysReportError() reports an error
 *
 * This function is present in the system call table and is called whenever a thread makes
 * a "ReportError" system call.
 *
 * The parameters to the system call are:
 *	- service ID
 *	- internal error code
 *	- parameter block
 *
 * If the caller is an OS thread, the thread is terminated and the culprit becomes the thread's
 * parent. Otherwise the culprit is the calling thread.
 *
 * Error checking:
 *	The error is reported "as is".
 *
 * !LINKSTO Microkernel.Function.MK_SysReportError, 1
 * !doctype src
*/
void MK_SysReportError(void)
{
	mk_serviceid_t sid = (mk_serviceid_t)MK_HwGetParameter1(MK_threadCurrent);
	mk_errorid_t eid = (mk_errorid_t)MK_HwGetParameter2(MK_threadCurrent);
	mk_thread_t *culprit;
	mk_thread_t *thr;

	if ( MK_threadCurrent->objectType == MK_OBJTYPE_OS )
	{
		/* Determine the culprit, i.e. the thread that called the QM-OS API.
		*/
		culprit = MK_threadCurrent->parent;

		/* Dequeue the OS thread from the head of the thread queue.
		 * This is a precondition for MK_TerminateThread().
		*/
		thr = MK_threadQueueHead;
		MK_threadQueueHead = MK_threadQueueHead->next;
		thr->next = MK_NULL;

		/* Terminate the thread  */
		MK_HwFreeThreadRegisters(thr->regs);
		MK_TerminateThread(thr);

		/* This is done *after* termination because it might enqueue an error hook thread!
		*/
		(void)MK_ReportError(sid, eid, culprit);

		/* A thread has been terminated. If there are no more threads the problem needs to be handled.
		*/
		if ( MK_threadQueueHead == MK_NULL )
		{
			MK_ThreadQueueEmpty();
		}
	}
	else
	{
		/* Some other thread reported an error. In this case the first 2 parameters to the
		 * service are taken from parameters 3 and 4 to the system call. Other parameters
		 * are not available.
		*/
		culprit = MK_threadCurrent;
		if ( MK_ReportError(sid, eid, culprit) )
		{
			/* MK_ReportError stored the error in the error info structure so we rearrange the values.
			*/
			MK_errorInfo->parameter[0] = MK_errorInfo->parameter[2];
			MK_errorInfo->parameter[1] = MK_errorInfo->parameter[3];
			MK_errorInfo->parameter[2] = (mk_parametertype_t)(-1);
			MK_errorInfo->parameter[3] = (mk_parametertype_t)(-1);
		}
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
