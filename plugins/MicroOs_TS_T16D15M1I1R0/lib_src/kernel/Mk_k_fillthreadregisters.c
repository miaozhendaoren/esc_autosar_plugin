/* Mk_k_fillthreadregisters.c
 *
 * This file contains the MK_FillThreadRegisters() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_fillthreadregisters.c 17616 2014-11-03 06:51:02Z stpo8218 $
*/
/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_C_COMPL_010:vocf]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  Most of the macros used in this function set elements in a structure
 *  resulting in the high VOCF metric. The goal of the VOCF metric is to
 *  ensure maintainability. With only five macro calls the implementation
 *  of this function has this property.
*/

/* Deviation DCG-1 <*> */

#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>

/* MK_FillThreadRegisters() fills the thread's registers with the initial values
 *
 * Parameters:
 *	threadCfg	- address of a thread config block for this thread
 *
 * Precondition:
 *	the registers are not in use (the thread is idle OR the registers are not shared)
 *
 * !LINKSTO Microkernel.Function.MK_FillThreadRegisters, 1
 * !doctype src
*/
void MK_FillThreadRegisters(const mk_threadcfg_t *threadCfg)
{
	/* Poke the starting address, PSW and other initial register values into the register structure.
	*/
	MK_HwSetMain(threadCfg, threadCfg->entry);
	MK_HwSetPs(threadCfg, threadCfg->ps);
	MK_HwSetStackPointer(threadCfg, threadCfg->initialSp);
	MK_HwSetReturnAddress(threadCfg, &MK_HwThreadReturn);
	MK_HwSetConstantRegisters(threadCfg);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
