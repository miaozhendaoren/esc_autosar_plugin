/* Mk_k_unknownsyscall.c
 *
 * This file contains the function MK_UnknownSyscall()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_unknownsyscall.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_errorhandling.h>

/* MK_UnknownSyscall() - handle an unimplemented or unconfigured system call
 *
 * This function is used to fill unused entries in the system call table - for example, to minimize the
 * "dead code" caused by unused features.
 * It is also called when the system call index is out of range.
 *
 * !LINKSTO Microkernel.Function.MK_UnknownSyscall, 1
 * !doctype src
*/
void MK_UnknownSyscall(void)
{
	(void)MK_ReportError(MK_sid_UnknownService, MK_eid_OsUnknownSystemCall, MK_threadCurrent);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
