/* Mk_k_sysreleaseresource.c
 *
 * This file contains the MK_SysReleaseResource() function.
 *
 * This function is called by the system call function whenever the ReleaseResource system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysreleaseresource.c 15908 2014-04-24 07:14:25Z masa8317 $
*/

#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_resource.h>
#include <private/Mk_thread.h>
#include <private/Mk_errorhandling.h>
#include <public/Mk_public_api.h>

static void MK_ReleaseResource(mk_resource_t*);

/* MK_ReleaseResource() performs the final release of a resource
 *
 * Preconditions guaranteed by MK_SysReleaseResource():
 *	1. resource is occupied by current thread
 *	2. resource is occupied exactly once
 *	3. resource is the thread's most-recently-taken resource
 *
 * This function is present to reduce the size and complexity of the MK_SysReleaseResource().
 * With any luck the compiler will inline it anyway.
 *
 * !LINKSTO Microkernel.Function.MK_ReleaseResource, 1
 * !doctype src
*/
static void MK_ReleaseResource(mk_resource_t *resource)
{
	resource->count = 0;

	/* Restore current thread's resource status from the newly-released resource
	*/
	MK_threadCurrent->currentPriority = resource->savedPrio;
	MK_HwSetIntLevel(MK_threadCurrent, resource->savedLevel);
	MK_threadCurrent->lastResourceTaken = resource->previousTaken;

	/* Set the newly-freed resource's owner to "none". This helps with debugging.
	*/
	resource->owner = MK_NULL;

	/* The priority of the current thread might have been reduced to a level below
	 * that of the next thread in the ready-queue. If so, the current thread must be
	 * dequeued and requeued further along the queue.
	 *
	 * Note: the "next-is-NULL" case only happens if the idle thread is using resources
	 * (including the interrupt disable services), or if the idle thread has terminated.
	*/
	if ( (MK_threadCurrent->next == MK_NULL) ||
		 (MK_threadCurrent->currentPriority >= MK_threadCurrent->next->currentPriority) )
	{
		/* Requeue is not necessary; the thread remains at the head of the queue
		*/
	}
	else
	{
		/* Dequeue current thread.
		*/
		MK_threadQueueHead =  MK_threadCurrent->next;

		/* Requeue the current thread. Current thread remains in RUNNING
		 * state until dispatcher is called.
		 * The thread must be inserted AHEAD of other threads with the same priority,
		 * so MK_RequeueThread() is the correct function to use.
		*/
		MK_RequeueThread(MK_threadCurrent);
	}
}

/* MK_SysReleaseResource() releases the resource identified by the parameter.
 *
 * This function is present in the system call table and is called whenever a thread makes
 * a "ReleaseResource" system call.
 *
 * !LINKSTO Microkernel.Function.MK_SysReleaseResource, 1
 * !doctype src
*/
void MK_SysReleaseResource(void)
{
	mk_objectid_t resourceId = (mk_objectid_t)MK_HwGetParameter1(MK_threadCurrent);
	mk_resource_t *resource;
	mk_errorid_t errorCode = MK_eid_Unknown;

	if ( (resourceId >= 0) && (resourceId < MK_nResources) )
	{
		errorCode = MK_eid_NoError;

		resource = &MK_resource[resourceId];

		if ( resource->count <= 0 )
		{
			errorCode = MK_eid_ResourceNotOccupied;
		}
		else if ( resource->owner == MK_threadCurrent )
		{
			if ( resource->count > 1 )
			{
				/* Resource has been taken more than once by current thread.
				 * Release a single acquisition.
				 * Note: 2nd and subsequent acquisitions of same resource can be
				 * taken and released in any order. The final release must be
				 * in LIFO order.
				*/
				resource->count--;
			}
			else if ( MK_threadCurrent->lastResourceTaken == resource )
			{
				/* Resource becomes free.
				*/
				MK_ReleaseResource(resource);
			}
			else
			{
				errorCode = MK_eid_ResourceReleaseSequenceError;
			}
		}
		else
		{
			/* This branch could occur legally if an interrupt-lock resource is
			 * acquired from a thread that has a higher priority than the resource's
			 * ceiling priority. For example:
			 *	- ErrorHook : both locking resources
			 *	- ProtectionHook : both locking resources
			 *	- Cat1 ISR : the Cat2-locking resource
			*/
			if ( ( (resourceId == MK_resLockCat1) || (resourceId == MK_resLockCat2) ) &&
				 ( MK_threadCurrent->queueingPriority > resource->ceilingPriority ) )
			{
				/* In these cases (and only these cases!) no error is reported. The thread's
				 * priority was previously higher than the resource's ceiling priority, so interrupts
				 * were already locked. See also corresponding branch in Mk_k_sysgetresource.c
				*/
			}
			else
			{
				errorCode = MK_eid_ResourceNotOccupiedByCaller;
			}
		}
	}
	else
	{
		errorCode = MK_eid_InvalidResourceId;
	}

	if ( errorCode == MK_eid_NoError )
	{
		MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
	}
	else
	{
		(void)MK_ReportError(MK_sid_ReleaseResource, errorCode, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
