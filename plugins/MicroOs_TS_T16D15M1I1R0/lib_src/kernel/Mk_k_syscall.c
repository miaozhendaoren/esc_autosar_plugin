/* Mk_k_syscall.c
 *
 * This file contains the MK_Syscall() function from the microkernel
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_syscall.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_interrupt.h>
#include <private/Mk_panic.h>

/* MK_Syscall() - generic system call handler
 *
 * Preconditions:
 *	- caller's context has been saved - including system call index and parameters
 *	- processor mode is suitable for microkernel
 *	- memory protection is set up for microkernel
 *	- current stack is kernel stack
 *	- interrupts are locked (hard)
 *
 * !LINKSTO Microkernel.Function.MK_Syscall, 1
 * !doctype src
*/
void MK_Syscall(void)
{
	mk_uint_fast16_t syscallIndex;

	if ( MK_threadCurrent == MK_NULL )
	{
		/* Paranoia!
		 * If there's no thread we cannot get the system call index and parameters!
		 * Not to mention that we just crashed by writing the registers to the null thread.
		 * !LINKSTO Microkernel.Panic.MK_panic_UnexpectedSystemCall, 1
		 * !doctype src
		*/
		MK_Panic(MK_panic_UnexpectedSystemCall);
	}
	else
	if ( MK_threadCurrent == MK_threadQueueHead )
	{
		/* Retrieve the system call index from the thread's context
		*/
		syscallIndex = (mk_uint_fast16_t)MK_HwGetSyscallIndex(MK_threadCurrent);

		/* Range check
		*/
		if ( syscallIndex < (mk_uint_fast16_t)MK_NSYSCALL_MK )
		{
			/* Call the appropriate microkernel function.
			 * This may result in a change to the most eligible thread.
			*/
			(*MK_syscallTable[syscallIndex])();
		}
		else
		{
			/* Call the unknown system call handler for out-of-range index.
			 * This may result in a change to the most eligible thread.
			*/
			MK_UnknownSyscall();
		}

		/* Dispatch the highest-priority thread; the dispatcher never returns
		*/
		MK_Dispatch();
	}
	else
	{
		/* Paranoia!
		 * If the current thread is not at the head of the queue something has been tinkering
		 * without dispatching.
		 * !LINKSTO Microkernel.Panic.MK_panic_CurrentThreadNotAtHeadOfQueue, 1
		 * !doctype src
		*/
		MK_Panic(MK_panic_CurrentThreadNotAtHeadOfQueue);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
