/* Mk_k_ppaterminateobject.c
 *
 * This file contains the MK_PpaTerminateObject() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppaterminateobject.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>

/* MK_PpaTerminateObject() shall terminate all queued instances (search through job queue)
 * of this OS object.
 * At the moment, this function is not implemented, therefore we just shut down.
 *
 * !LINKSTO Microkernel.Function.MK_PpaTerminateObject, 1
 * !doctype src
*/
void MK_PpaTerminateObject(void)
{
	MK_PpaShutdown();
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
