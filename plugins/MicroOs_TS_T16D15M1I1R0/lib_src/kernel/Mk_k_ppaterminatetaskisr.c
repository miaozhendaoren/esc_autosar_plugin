/* Mk_k_ppaterminatetaskisr.c
 *
 * This file contains the MK_PpaTerminateTaskIsr() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppaterminatetaskisr.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>

/* MK_PpaTerminateTaskIsr() terminates the current thread if it is a task or ISR.
 *
 * Preconditions:
 * - The current thread's parent points to the culprit
 *
 * !LINKSTO Microkernel.Function.MK_PpaTerminateTaskIsr, 1
 * !doctype src
*/
void MK_PpaTerminateTaskIsr(void)
{
	/* The culprit is the protection hook's parent
	*/
	mk_thread_t *culprit = MK_threadCurrent->parent;

	const mk_objecttype_t objType = culprit->objectType;

	if ( (objType == MK_OBJTYPE_TASK) || (objType == MK_OBJTYPE_ISR) )
	{
		MK_PpaTerminateThread();
	}
	else
	{
		/* If there is no task or ISR, AUTOSAR requires to terminate the OS-Application
		*/
		MK_PpaTerminateApplication();
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
