/* Mk_k_sysgettaskid.c
 *
 * This file contains the function MK_SysGetTaskId()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysgettaskid.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>

/* MK_SysGetTaskId() - finds the ID of the current task
 *
 * It works by walking down the thread queue looking for the first task. Task in state NEW are ignored
 * because they've just been activated and cannot be "current".
 *
 * !LINKSTO Microkernel.Function.MK_SysGetTaskId, 1
 * !doctype src
*/
void MK_SysGetTaskId(void)
{
	/* Find highest-priority task thread. Threads in state NEW are ignored because they have never been dispatched
	 * and can therefore not be current even if they are of higher priority than the task that is considered
	 * to be "current".
	*/
	mk_thread_t *thread = MK_FindFirstThread(MK_OBJTYPE_TASK, MK_THS_NEW);

	if ( thread == MK_NULL )
	{
		MK_HwSetReturnValue2(MK_threadCurrent, MK_INVALID_TASK);
	}
	else
	{
		MK_HwSetReturnValue2(MK_threadCurrent, thread->currentObject);
	}

	MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
