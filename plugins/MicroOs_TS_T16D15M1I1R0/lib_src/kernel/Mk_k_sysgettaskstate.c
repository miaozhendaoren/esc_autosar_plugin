/* Mk_k_sysgettaskstate.c
 *
 * This file contains the function MK_SysGetTaskState()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysgettaskstate.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_event.h>
#include <private/Mk_errorhandling.h>

/* MK_SysGetTaskState() determines a task's state
 *
 * The state is determined by looking at:
 * - the activation counter
 * - whether the task is currently "on" its thread
 * - the pending events (for EXTENDED tasks)
 *
 * Note: this implementation is preferred over maintaining a task state for each task because
 * the cost of updating the state during normal running is borne by the system as a whole, whereas
 * with this method the cost is borne by the caller (and occurs much less frequently).
 *
 * !LINKSTO Microkernel.Function.MK_SysGetTaskState, 1
 * !doctype src
*/
void MK_SysGetTaskState(void)
{
	mk_errorid_t errorCode = MK_eid_Unknown;
	mk_objectid_t taskId = (mk_objectid_t)MK_HwGetParameter1(MK_threadCurrent);
	const mk_taskcfg_t *taskCfg;
	mk_task_t *task;
	mk_thread_t *thread;
	mk_taskstate_t state = MK_TS_INVALID;

	if ( (taskId >= 0) && (taskId < MK_nTasks) )
	{
		taskCfg = &MK_taskCfg[taskId];
		task = taskCfg->dynamic;
		thread = taskCfg->thread;

		if ( task->activationCount <= 0 )
		{
			/* Task is not active
			*/
			state = SUSPENDED;
		}
		else
		if ( (taskCfg->eventStatus != MK_NULL) && (taskCfg->eventStatus->awaitedEvents != 0u) )
		{
			/* Task is an extended task that is waiting for one or more events
			*/
			state = WAITING;
		}
		else
		if ( thread->currentObject == taskId )
		{
			/* Task is currently occupying its thread, so is either RUNNING or READY.
			 * However, the task's state is not the same as the thread's state because the service
			 * might be called from an ISR or hook function, which also run in threads and will therefore
			 * have preempted the task's thread. So we need to determine if the task's thread is the
			 * highest-priority task thread in the system. Tasks in state NEW have never been dispatched
			 * so must not be reported as RUNNING.
			*/
			if ( thread == MK_FindFirstThread(MK_OBJTYPE_TASK, MK_THS_NEW) )
			{
				state = RUNNING;
			}
			else
			{
				state = READY;
			}
		}
		else
		{
			/* Task is not occupying its thread. It is therefore assumed to be somewhere in the
			 * thread's job queue.
			 * CAVEAT: if an extended task has gone into the waiting state with an empty event set
			 * (awaitedEvents == 0) it became a zombie. This function will report it as "READY".
			 * WaitEvent should prevent that from happening so we don't try to differentiate here.
			*/
			state = READY;
		}

		errorCode = MK_eid_NoError;
	}
	else
	{
		errorCode = MK_eid_InvalidTaskId;
	}

	/* This ensures that the requested information is always defined (for errors, it's MK_TS_INVALID)
	*/
	MK_HwSetReturnValue2(MK_threadCurrent, state);

	if ( errorCode == MK_eid_NoError )
	{
		MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
	}
	else
	{
		(void)MK_ReportError(MK_sid_GetTaskState, errorCode, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
