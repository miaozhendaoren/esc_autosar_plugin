/* Mk_k_initinterrupts.c
 *
 * This file contains the function MK_InitInterrupts()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_initinterrupts.c 15843 2014-04-17 05:51:45Z masa8317 $
*/

#include <public/Mk_public_types.h>
#include <private/Mk_interrupt.h>
#include <private/Mk_softvector.h>
#include <private/Mk_panic.h>

/* MK_InitInterrupts() initializes the interrupt controller and all configured interrupt sources.
 *
 * Hardware-specific functions or macros are used.
 *
 * !LINKSTO Microkernel.Function.MK_InitInterrupts, 1
 * !doctype src
*/
void MK_InitInterrupts(void)
{
	mk_objectid_t i;

	/* Ensure that the software vector table has the correct number of elements.
	 * The end marker contains an unknown interrupt with a parameter of MK_VECTOR_LASTMARK.
	 * If this is not the case, the vector table's initializer has does not have the correct
	 * number of entries (probably too few).
	*/
	if ( (MK_softwareVectorTable[MK_HWN_INTERRUPTVECTORS].func != &MK_UnknownInterrupt) ||
		 (MK_softwareVectorTable[MK_HWN_INTERRUPTVECTORS].param != MK_VECTOR_LASTMARK) )
	{
		/* The last element of the array is not the end marker.
		 *
		 * !LINKSTO Microkernel.Panic.MK_panic_VectorTableIncorrectlyInitialized, 1
		 * !doctype src
		*/
		MK_StartupPanic(MK_panic_VectorTableIncorrectlyInitialized);
	}

	/* Initialize the interrupt controller. This disables all interrupt sources.
	*/
	MK_HwInitInterruptController();

	/* Initialize the configured interrupt sources.
	 *
	 * Note: MK_irqCfg could be NULL here, but if it is, MK_nIrqs is 0,
	 * so the loop body is not executed and the null pointer is not dereferenced.
	*/
	for ( i = 0; i < MK_nIrqs; i++ )
	{
		MK_HwInitIrq(&MK_irqCfg[i]);

		if ( (MK_irqCfg[i].flags & MK_IRQ_ENABLE) != 0u )
		{
			MK_HwEnableIrq(&MK_irqCfg[i]);
		}
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
