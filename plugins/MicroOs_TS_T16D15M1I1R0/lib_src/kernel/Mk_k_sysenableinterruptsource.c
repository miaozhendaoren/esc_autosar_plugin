/* Mk_k_sysenableinterruptsource.c
 *
 * This file contains the function MK_SysEnableInterruptSource()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysenableinterruptsource.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_isr.h>
#include <private/Mk_errorhandling.h>

/* MK_SysEnableInterruptSource() enables an interrupt source belonging to an ISR.
 * First, a range check on the ISR id is performed.
 * Afterwards, it is checked that the OS-Application to which this ISR belongs is not quarantined and the
 * interrupt is enabled.
 *
 * !LINKSTO Microkernel.Function.MK_SysEnableInterruptSource, 1
 * !doctype src
*/
void MK_SysEnableInterruptSource(void)
{
	const mk_objectid_t isrId = (mk_objectid_t) MK_HwGetParameter1(MK_threadCurrent);
	mk_objectid_t appId;

	/* Range-check on the ISR id
	*/
	if ( (isrId >= 0) && (isrId < MK_nIsrs) )
	{
		appId = MK_isrCfg[isrId].threadCfg.applicationId;

		/* Interrupt cannot be enabled when the owning OS-Application is terminated
		*/
		if ( (appId != MK_APPL_NONE) && (MK_appStates[appId] == APPLICATION_TERMINATED) )
		{
			(void)MK_ReportError(MK_sid_EnableInterruptSource, MK_eid_Quarantined, MK_threadCurrent);
		}
		else
		{
			MK_HwEnableIrq(MK_isrCfg[isrId].irq);
			MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
		}
	}
	else
	{
		(void)MK_ReportError(MK_sid_EnableInterruptSource, MK_eid_InvalidIsrId, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
