/* Mk_k_jqappend.c
 *
 * This file contains the MK_JqAppend() function from the microkernel
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_jqappend.c 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 *  Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 *  Pointers are used here for efficiency. The boundary values are pre-calculated constants.
*/
#include <public/Mk_public_types.h>
#include <private/Mk_jobqueue.h>
#include <private/Mk_panic.h>

/* MK_JqAppend() - append a job to the end of the job queue
 *
 * Preconditions:
 *	- interrupts are disabled
 *	- the queue is not full (count < length)
 *
 * Postconditions:
 *	- free is one more than on entry (if > limit, base)
 *	- count is one more than on entry
 *
 * !LINKSTO Microkernel.Function.MK_JqAppend, 1
 * !doctype src
*/
void MK_JqAppend(mk_jobqueue_t *jq, mk_jobid_t job)
{
	/* These two error conditions are tested here because the caller does not guarantee that
	 * they are not present. An error indicates a configuration error (queue size too small).
	*/
	if ( jq == MK_NULL )
	{
		/* !LINKSTO Microkernel.Panic.MK_panic_JqAppend_ThreadHasNoQueue, 1
		 * !doctype src
		*/
		MK_Panic(MK_panic_JqAppend_ThreadHasNoQueue);
	}
	else
	if ( jq->count >= jq->length )
	{
		/* !LINKSTO Microkernel.Panic.MK_panic_JqAppend_QueueIsFull, 1
		 * !doctype src
		*/
		MK_Panic(MK_panic_JqAppend_QueueIsFull);
	}
	else
	{
		*jq->free = job;

		/* Deviation MISRA-1 */
		jq->free++;
		if ( jq->free >= jq->limit )
		{
			jq->free = jq->base;
		}
		/* Deviation MISRA-1 */
		jq->count++;
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
