/* Mk_k_sysschedule.c
 *
 * This file contains the MK_SysSchedule() function.
 *
 * This function is called by the system call function whenever the Schedule system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysschedule.c 16009 2014-05-06 09:35:05Z dh $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_resource.h>
#include <private/Mk_errorhandling.h>

/*
 * MK_SysSchedule() yields non-preemptive status and internal resources to permit other threads to run.
 *
 * This function is present in the system call table and is called whenever a thread makes
 * a "Schedule" system call.
 *
 * !LINKSTO Microkernel.Function.MK_SysSchedule, 1
 * !doctype src
*/
void MK_SysSchedule(void)
{
	mk_resource_t *resource;
	mk_threadprio_t lowPrio;

	if ( MK_threadCurrent->next == MK_NULL )
	{
		/* This can only happen if Schedule() gets called from the lowest-priority thread!
		 * Normally that's the idle thread, so shouldn't happen. But we must cater for the
		 * unusual cases too.
		*/
	}
	else
	{
		/* Calculate the priority that the thread has to drop to.
		 * As basis, use the queueing priority.
		*/
		lowPrio = MK_threadCurrent->queueingPriority;

		/* However, it should not drop lower than the highest resource taken, so
		 * walk the list of resources and adjust the priority to drop to.
		 *
		 * NOTE: AUTOSAR treats this as an error, but this kernel permits it.
		 * If no resource is occupied the runtime of the following code is minimal.
		 *
		 * The advantage of this is that the kernel behaves "as you would expect" even if
		 * the error-checking is disabled (i.e. threads that are configured to use the
		 * resource *still* cannot run --> no unexpected violations of critical sections.
		*/
		resource = MK_threadCurrent->lastResourceTaken;
		while ( resource != MK_NULL )
		{
			if ( resource->ceilingPriority > lowPrio )
			{
				lowPrio = resource->ceilingPriority;
			}
			resource = resource->previousTaken;
		}

		if ( MK_threadCurrent->next->currentPriority > lowPrio )
		{
			/* Dequeue current thread from head of queue.
			*/
			MK_threadQueueHead = MK_threadCurrent->next;

			/* Requeue current thread at the lower priority.
			 * The thread must be inserted AHEAD of other threads with the same priority,
			 * so MK_RequeueThread() is the correct function to use.
			*/
			MK_threadCurrent->currentPriority = lowPrio;
			MK_RequeueThread(MK_threadCurrent);
		}
		/* else ... (omitted)
		 * No higher-priority threads waiting.
		*/
	}

	MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
