/* Mk_k_ppapanicstop.c
 *
 * This file contains the MK_PpaPanicStop() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppapanicstop.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_panic.h>
#include <private/Mk_errorhandling.h>

/* MK_PpaPanicStop()
 *
 * !LINKSTO Microkernel.Function.MK_PpaPanicStop, 1
 * !doctype src
*/
void MK_PpaPanicStop(void)
{
	MK_PanicStop(MK_panic_PanicFromProtectionHook);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
