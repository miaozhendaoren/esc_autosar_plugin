/* Mk_k_jqinit.c
 *
 * This file contains the MK_JqInit() function from the microkernel
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_jqinit.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <private/Mk_jobqueue.h>
#include <private/Mk_thread.h>

/* MK_JqInit() - associates the configured job queues with their respective threads
 *
 * The MK_JqInit() function is called by MK_StartKernel() at system startup. It loops over
 * all configured job queues and assigns each job queue to the associated thread.
 *
 * !LINKSTO Microkernel.Function.MK_JqInit, 1
 * !doctype src
 */
void MK_JqInit(void)
{
	mk_objectid_t jq;

	for ( jq = 0; jq < MK_nJobQueues; jq++ )
	{
		/* Associate the thread with the job queue.
		 * Threads with no job queue can only run a single object at a time.
		*/
		MK_jobQueueCfg[jq].thread->jobQueue = &MK_jobQueue[jq];

		/* Initialize the job queue RAM structure from the config structure
		*/
		MK_jobQueue[jq].base = MK_jobQueueCfg[jq].base;
		MK_jobQueue[jq].limit = &(MK_jobQueueCfg[jq].base)[MK_jobQueueCfg[jq].length];
		MK_jobQueue[jq].next = MK_jobQueueCfg[jq].base;
		MK_jobQueue[jq].free = MK_jobQueueCfg[jq].base;
		MK_jobQueue[jq].count = 0u;
		MK_jobQueue[jq].length = MK_jobQueueCfg[jq].length;
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
