/* Mk_k_sysstartforeignthread.c
 *
 * This file contains the MK_SysStartForeignThread() function.
 *
 * This system call provides the possiblity to run functions
 * in their own thread.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysstartforeignthread.c 15821 2014-04-16 04:55:15Z masa8317 $
*/

#include <public/Mk_public_types.h>
#include <private/Mk_foreign.h>
#include <private/Mk_syscall.h>


/* MK_SysStartForeignThread() starts a thread to run the given function with up to three parameters.
 * The function to run can either be a QM-OS function or a trusted function.
 * A list of QM-OS functions is hardcoded in the microkernel.
 * A list of trusted functions is part of the configuration.
 *
 * !LINKSTO Microkernel.Function.MK_SysStartForeignThread, 1
 * !doctype src
*/
void MK_SysStartForeignThread(void)
{
	mk_objectid_t functionIndex;
	mk_parametertype_t param1;
	mk_parametertype_t param2;
	mk_parametertype_t param3;

	/* Get the parameters for this system call. Only three parameters (for the foreign function) are supported */
	functionIndex  = (mk_objectid_t)      MK_HwGetParameter1(MK_threadCurrent);
	param1         = (mk_parametertype_t) MK_HwGetParameter2(MK_threadCurrent);
	param2         = (mk_parametertype_t) MK_HwGetParameter3(MK_threadCurrent);
	param3         = (mk_parametertype_t) MK_HwGetParameter4(MK_threadCurrent);

	/* Prepare the thread and start it
	*/
	MK_StartForeignThread(functionIndex, param1, param2, param3, MK_FALSE);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
