/* Mk_k_selftestexecbudget.c
 *
 * This file contains the MK_SelftestExecBudget() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_selftestexecbudget.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_thread.h>
#include <private/Mk_accounting.h>

/* MK_SelftestExecBudget()
 *
 * -- starts the self test thread config using MK_osThreadHigh.
 * If the given execution budget is above zero and below the max tick value,
 * it sets the new thread's remaining time budget to this value. Else, the
 * default value from the thread config is used.
 *
 * !LINKSTO Microkernel.Function.MK_SelftestExecBudget, 1
 * !doctype src
*/
mk_errorid_t MK_SelftestExecBudget(mk_tick_t execBudget)
{
	mk_errorid_t result = MK_eid_Unknown;

	if ( MK_osThreadHigh.state == MK_THS_IDLE )
	{
		MK_StartThread(&MK_osThreadHigh, &MK_selftestThreadConfig);

		if ( execBudget > 0 )
		{
			MK_osThreadHigh.accounting.timeRemaining = execBudget;
		}

		MK_selftestState |= MK_SELFTEST_STATE_EXECBUDGET;

		result = MK_eid_NoError;
	}
	else
	{
		result = MK_eid_ThreadIsNotIdle;
	}

	return result;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
