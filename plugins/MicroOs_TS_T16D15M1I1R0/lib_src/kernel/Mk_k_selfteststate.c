/* Mk_k_selfteststate.c
 *
 * This file contains the MK_GetSelftestState() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_selfteststate.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_accounting.h>
#include <private/Mk_panic.h>

/* MK_GetSelftestState()
 *
 * -- returns the current self test state masked with the given parameter value
 * and clears the masked bit(s) from the state.
 *
 * !LINKSTO Microkernel.Function.MK_GetSelftestState, 1
 * !doctype src
*/
mk_uint32_t MK_GetSelftestState(mk_uint32_t mask)
{
	mk_uint32_t state;

	state = MK_selftestState & mask;
	MK_selftestState &= ~mask;

	return state;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
