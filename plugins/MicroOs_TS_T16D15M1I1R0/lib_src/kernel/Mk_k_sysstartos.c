/* Mk_k_sysstartos.c
 *
 * This file contains the MK_SysStartOs() function from the microkernel
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysstartos.c 18539 2015-02-25 09:29:25Z nibo2437 $
*/

#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_oscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_systemcontrol.h>
#include <private/Mk_foreign.h>
#include <private/Mk_errorhandling.h>

/* MK_SysStartOs() - starts the OS
 *
 * This function handles the "StartOS" system call. It is coded as a microkernel function
 * rather than a simple OS activation because it has to terminate the calling thread.
 *
 * !LINKSTO Microkernel.Function.MK_SysStartOs, 1
 * !doctype src
 */
void MK_SysStartOs(void)
{
	/* Store the application mode of StartOS system call
	*/
	mk_parametertype_t applicationMode;

	if ( MK_threadCurrent->objectType == MK_OBJTYPE_TASK )
	{
		/* If the caller is a task, decrement the activation counter
		*/
		MK_taskCfg[MK_threadCurrent->currentObject].dynamic->activationCount--;
	}

	/* StartOS() has an application mode parameter
	*/
	applicationMode = MK_HwGetParameter1(MK_threadCurrent);

	/* Terminate the caller, even if there is an error!
	 * This has to be done first so that StartOs can run in the same thread.
	*/
	MK_threadCurrent->state = MK_THS_IDLE;
	MK_threadQueueHead = MK_threadQueueHead->next;

	/* Set the priority to zero so that it does not impact the worker thread selection in MK_StartForeignThread.
	 * Note: this is done before calling MK_TerminateThread because MK_TerminateThread might start another
	 * object in the thread.
	*/
	MK_threadCurrent->currentPriority = 0;

	/* Terminate the thread
	*/
	MK_HwFreeThreadRegisters(MK_threadCurrent->regs);
	MK_TerminateThread(MK_threadCurrent);

	/* Start the startup-hook thread if there is one (and is not occupied)
	*/
	if (MK_startupHookThreadConfig == MK_NULL)
	{
		/* No startup-hook is configured, so we just run StartOs()
		*/
		MK_StartForeignThread(MK_SC_OS_StartOs, applicationMode, 0u, 0u, MK_FALSE);
	}
	else
	if ( (MK_osThreadLow.state == MK_THS_IDLE) && (MK_osThreadHigh.state == MK_THS_IDLE) )
	{
		/* Start the OS high thread with the QM-OS StartOs() function */
		MK_StartForeignThread(MK_SC_OS_StartOs, applicationMode, 0u, 0u, MK_TRUE);

		/* Run the startup-hook. It has a lower priority than osThreadHigh.
		*/
		MK_StartThread(&MK_osThreadLow, MK_startupHookThreadConfig);
	}
	else
	{
		/* A startup-hook is configured, but too few QM-OS threads are available.
		 * Therefore, we just terminate the caller and report an error.
		*/
		(void) MK_ReportError(MK_sid_StartForeignThread, MK_eid_ThreadIsNotIdle, MK_threadCurrent);
	}

	/* A thread has been terminated. If there are no more threads the problem needs to be handled.
	*/
	if ( MK_threadQueueHead == MK_NULL )
	{
		MK_ThreadQueueEmpty();
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
