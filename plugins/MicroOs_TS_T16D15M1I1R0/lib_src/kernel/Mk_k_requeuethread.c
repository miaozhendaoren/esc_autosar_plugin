/* Mk_k_requeuethread.c
 *
 * This file contains the MK_RequeueThread() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_requeuethread.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>


/* MK_RequeueThread
 *
 * Insert an existing thread at the correct place in the thread queue.
 *
 * It assumes that the requeue is necessary, i.e. that the function is only called if the
 * next thread in the queue is of higher priority.
 *
 * Preconditions:
 *	1. the thread to be requeued (parameter) is a valid thread that is not already in the queue.
 *	2. the thread queue is not empty
 *	3. the first thread in the queue has a higher priority than the thread to be requeued
 *
 * !LINKSTO Microkernel.Function.MK_RequeueThread, 1
 * !doctype src
*/
void MK_RequeueThread(mk_thread_t *thread)
{
	mk_thread_t *pred = MK_threadQueueHead;		/* (potential) predecessor in queue */
	mk_thread_t *succ = pred->next;				/* (potential) successor in queue */

	/* Find the first thread in the queue whose priority is lower than or equal to the priority
	 * of the thread-to-insert.
	 * Thus a newly-inserted thread gets placed *after* all higher-priority threads, but
	 * *before* all equal- or lower-priority threads.
	*/
	while ( (succ != MK_NULL) && (succ->currentPriority > thread->currentPriority) )
	{
		pred = succ;
		succ = succ->next;
	}

	/* Now we insert the thread between the predecessor and the successor.
	*/
	pred->next = thread;
	thread->next = succ;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
