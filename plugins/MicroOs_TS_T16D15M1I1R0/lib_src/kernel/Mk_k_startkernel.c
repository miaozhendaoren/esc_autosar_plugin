/* Mk_k_startkernel.c
 *
 * This file contains the MK_StartKernel() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_startkernel.c 16024 2014-05-07 10:16:55Z dh $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 16.10 (required)
 *  If a function returns error information, then that error information should be tested.
 *
 * Reason:
 *  MK_UsrTerminateSelf() never returns. A non-void return type is mandated by Autosar because the
 *  function implements the TerminateTask() service.
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_C_COMPL_010:calls]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  Starting the kernel consists mainly of a sequence of calls
 *  to several initialization functions for different subsystems.
 *  This leads to a high CALLS value.
*/
/* Deviation DCG-1 <*> */

#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <private/Mk_thread.h>
#include <private/Mk_jobqueue.h>
#include <private/Mk_resource.h>
#include <private/Mk_startup.h>
#include <private/Mk_interrupt.h>
#include <private/Mk_memoryprotection.h>
#include <private/Mk_panic.h>
#include <private/Mk_exceptionhandling.h>

/* MK_StartKernel
 *
 * This function is called by the startup code in order to start the microkernel.
 *
 * This function should be running on the microkernel stack.
 *
 * A dummy thread is set up so that it appears that this code is running in the dummy thread.
 * One of the threads reserved for system calls to the QM-OS is used for this.
 *
 * Then two more threads are created:
 *	- the idle thread (at priority zero)
 *	- the startup thread (at a priority higher than all tasks and ISRs)
 * Finally, the running thread terminates itself, permitting the startup thread to run.
 *
 * Preconditions:
 * - stack is the kernel stack (not really necessary, but it's the only stack that's guaranteed big enough).
 * - interrupts are disabled.
 *
 * !LINKSTO	Microkernel.Function.MK_StartKernel, 1
 * !doctype src
*/
void MK_StartKernel(mk_uint32_t q1, mk_uint32_t q2, mk_uint32_t q3, mk_uint32_t q4)
{
	/* Go no further if the .data and .bss sections haven't been initialized.
	*/
	MK_DataInitCheck(q1, q2, q3, q4);

	/* Processor-specific initialization
	*/
	MK_HwInitProcessor();

	/* Initialize memory protection
	*/
	MK_InitMemoryProtection();

	/* Initialize execution budget monitoring
	*/
	MK_InitExecutionBudgetMonitor();

	/* Initialize the job queues
	*/
	MK_JqInit();

	/* Initialize resource table
	*/
	MK_InitResources();

	/* Initialize interrupt sources and handling.
	*/
	MK_InitInterrupts();

	/* Initialize OS-Application handling.
	*/
	MK_InitApplications();

	/* Initialize the thread queue so that it is empty
	*/
	MK_threadQueueHead = MK_NULL;

	/* Create the idle thread. No dispatch is performed so it sits in the queue.
	 *
	 * !LINKSTO Microkernel.Start.MK_StartKernel.Idle, 1
	 * !doctype src
	*/
	MK_StartThread(&MK_idleThread, &MK_idleThreadConfig);

	/* Create the initialization thread that runs main().
	 * No dispatch is performed so it sits in the queue.
	 * Both parameters are 0 because of the ".bss" initialization of the register store.
	*/
	MK_StartThread(&MK_osThreadLow, &MK_initThreadConfig);

	/* Create a high-priority thread that will be set as "current", thus converting this
	 * executing function into a thread that can be terminated.
	*/
	MK_StartThread(&MK_osThreadHigh, &MK_bootThreadConfig);

	/* Set the newly created thread as the current thread.
	*/
	MK_osThreadHigh.state = MK_THS_RUNNING;
	MK_threadCurrent = &MK_osThreadHigh;

	/* Before we can terminate the newly-created thread in which we're running, the exception
	 * vectors need to be set up. This must not be done earlier because until there is a valid
	 * MK_threadCurrent the standard microkernel vectors do not work correctly.
	*/
	MK_HwSetupExceptions(q3, q4, q1, q2);

	if ( ( (q1 ^ MK_ENDKEY_1misra) |
		   (q2 ^ MK_ENDKEY_2misra) |
		   (q3 ^ MK_ENDKEY_3misra) |
		   (q4 ^ MK_ENDKEY_4misra) ) == 0u )
	{
		/* Deviation MISRA-1 */
		(void)MK_UsrTerminateSelf();
	}

	/* The "TerminateThread" system call above should never return. If it does we panic. This panic
	 * also covers the case where the key is not correct.
	 * !LINKSTO Microkernel.Panic.MK_panic_IncorrectStartupKey, 1
	 * !doctype src
	*/
	MK_StartupPanic(MK_panic_IncorrectStartupKey);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
