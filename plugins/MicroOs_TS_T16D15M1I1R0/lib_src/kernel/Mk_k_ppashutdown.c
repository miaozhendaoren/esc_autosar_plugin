/* Mk_k_ppashutdown.c
 *
 * This file contains the MK_PpaShutdown() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppashutdown.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_shutdown.h>

/* MK_PpaShutdown() shuts the microkernel down with the error code given by the MK_protectionInfo structure.
 * If unavailable, MK_E_ERROR is used.
 *
 * !LINKSTO Microkernel.Function.MK_PpaShutdown, 1
 * !doctype src
*/
void MK_PpaShutdown(void)
{
	mk_osekerror_t osekError;

	if ( MK_protectionInfo == MK_NULL )
	{
		/* MK_protectionInfo doesn't exist, so the error code is not available.
		*/
		osekError = MK_E_ERROR;
	}
	else
	{
		osekError = MK_protectionInfo->osekError;
	}

	MK_Shutdown(osekError);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
