/* Mk_k_ppaterminateapplication.c
 *
 * This file contains the MK_PpaTerminateApplication() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppaterminateapplication.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_shutdown.h>
#include <private/Mk_thread.h>

/* MK_PpaTerminateApplication() determines the currently running OS-Application and terminates it.
 * It also marks the application as terminated, which means that it cannot be restarted.
 *
 * Preconditions:
 * - The current thread's parent points to the culprit
 *
 * !LINKSTO Microkernel.Function.MK_PpaTerminateApplication, 1
 * !doctype src
*/
void MK_PpaTerminateApplication(void)
{
	/* The culprit is the protection hook's parent
	*/
	mk_thread_t *culprit = MK_threadCurrent->parent;

	if (culprit->applicationId == MK_APPL_NONE)
	{
		/* The culprit doesn't belong to an OS-Application, so we cannot terminate it. Instead, shut down.
		*/
		MK_PpaShutdown();
	}
	else
	{
		/* Now terminate everything that is already running and belonging to the OS-Application.
		*/
		MK_TerminateApplication(culprit->applicationId, MK_FALSE);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
