/* Mk_k_ppaterminateapplicationrestart.c
 *
 * This file contains the MK_PpaTerminateApplicationRestart() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppaterminateapplicationrestart.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_shutdown.h>

/* MK_PpaTerminateApplicationRestart() determines the currently running OS-Application and restarts it.
 *
 * Preconditions:
 * - The current thread's parent points to the culprit
 *
 * !LINKSTO Microkernel.Function.MK_PpaTerminateApplicationRestart, 1
 * !doctype src
*/
void MK_PpaTerminateApplicationRestart(void)
{
	/* The culprit is the protection hook's parent
	*/
	mk_thread_t *culprit = MK_threadCurrent->parent;

	if (culprit->applicationId == MK_APPL_NONE)
	{
		/* The culprit has doesn't belong to an OS-Application, so we cannot terminate it. Instead, shut down.
		*/
		MK_PpaShutdown();
	}
	else
	{
		MK_RestartApplication(culprit->applicationId);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
