/* Mk_k_datainitcheck.c
 *
 * This file contains the function MK_DataInitCheck().
 * The function performs a simple test that the .data and .bss sections of the microkernel
 * have been initialized.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_datainitcheck.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_startup.h>
#include <private/Mk_panic.h>

/* MK_DataInitCheck() performs a simple test that the .data and .bss sections of the microkernel
 * have been initialized. To ensure that it has been called correctly it also checks the
 * 128-bit startup key against the value expected at the expected call.
 *
 * !LINKSTO Microkernel.Function.MK_DataInitCheck, 1
 * !doctype src
*/
void MK_DataInitCheck(mk_uint32_t p1, mk_uint32_t p2, mk_uint32_t p3, mk_uint32_t p4)
{
	/* This is effectively a comparison for equality ("==") of each of the key parameters
	 * to its respective expected MK_ENDKEY.
	*/
	if ( ( (p1 ^ MK_ENDKEY_1misra) |
		   (p2 ^ MK_ENDKEY_2misra) |
		   (p3 ^ MK_ENDKEY_3misra) |
		   (p4 ^ MK_ENDKEY_4misra) ) == 0u )
	{
		if ( (MK_initTestData == MK_INITTEST_VALUE) && (MK_initTestBss == 0u) )
		{
			/* The two variables are OK. Now trash their values so that even a soft reset won't get a false result.
			*/
			MK_initTestData = MK_INITTEST_TRASH;
			MK_initTestBss = MK_INITTEST_TRASH;
		}
		else
		{
			/* !LINKSTO Microkernel.Panic.MK_panic_DataSectionsNotInitialized, 1
			 * !doctype src
			*/
			MK_StartupPanic(MK_panic_DataSectionsNotInitialized);
		}
	}
	else
	{
		/* !LINKSTO Microkernel.Panic.MK_panic_IncorrectStartupKey, 1
		 * !doctype src
		*/
		MK_StartupPanic(MK_panic_IncorrectStartupKey);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
