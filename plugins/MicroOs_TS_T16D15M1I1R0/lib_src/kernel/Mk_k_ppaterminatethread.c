/* Mk_k_ppaterminatethread.c
 *
 * This file contains the MK_PpaTerminateThread() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppaterminatethread.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_panic.h>
#include <private/Mk_task.h>

/* MK_PpaTerminateThread() dequeues and terminates the thread that caused the protection
 * violation (called the "culprit").
 *
 * Preconditions:
 *  - The current thread is the protection hook thread
 *  - The protection hook thread's parent points to the culprit
 *
 * !LINKSTO Microkernel.Function.MK_PpaTerminateThread, 1
 * !doctype src
*/
void MK_PpaTerminateThread(void)
{
	/* Start with the current thread, which is the protection hook thread.
	*/
	mk_thread_t *pred = MK_threadCurrent;

	/* The culprit is the protection hook's parent.
	*/
	mk_thread_t *culprit = pred->parent;

	/* Walk along the thread queue (starting at the protection hook) looking for the
	 * predecessor of culprit (i.e. the thread whose "next" points to the culprit).
	 * The loop terminates when the predecessor is found (pred->next == culprit)
	 * or when the end of the thread queue is reached (pred == 0).
	*/
	while ( (pred != MK_NULL) && (pred->next != culprit) )
	{
		pred = pred->next;
	}

	/* If the search reached the end of the thread queue without finding the culprit, there's
	 * something seriously wrong with the microkernel's queues. The only course of action
	 * is panic.
	*/
	if (pred == MK_NULL)
	{
		/* The predecessor was not found
		 * !LINKSTO Microkernel.Panic.MK_panic_ThreadNotFoundInQueue, 1
		 * !doctype src
		*/
		MK_Panic(MK_panic_ThreadNotFoundInQueue);
	}
	else
	{
		if ( culprit->parent == MK_NULL )
		{
			/* Thread has no parent, so it could be a task. If it isn't a task there's nothing a special
			 * to do here.
			*/
			if ( culprit->objectType == MK_OBJTYPE_TASK )
			{
				/* If the caller is a task, decrement the activation counter
				*/
				MK_taskCfg[culprit->currentObject].dynamic->activationCount--;
			}
		}
		else
		{
			/* The culprit has a parent. This means that the thread is either the error hook,
			 * a QMOS thread or a trusted function thread. The only other type of thread with a
			 * parent is the protection hook, and our culprit cannot be that.
			 *
			 * If the culprit is a QMOS thread or a trusted function, the first return value register
			 * of the parent (the "status code") must be set to "KILLED" to indicated that the
			 * requested service didn't complete successfully.
			*/
			if ( (culprit->objectType == MK_OBJTYPE_ERRORHOOK) )
			{
				/* No special action required
				*/
			}
			else
			if ( (culprit->objectType == MK_OBJTYPE_OS) ||
				 (culprit->objectType == MK_OBJTYPE_TRUSTEDFUNCTION))
			{
				/* Set parent's return value (status code) to MK_E_KILLED.
				*/
				MK_HwSetReturnValue1(culprit->parent, MK_E_KILLED);
			}
			else
			{
				/* What's this?
				 * !LINKSTO Microkernel.Panic.MK_panic_UnexpectedThreadWithParent, 1
				 * !doctype src
				*/
				MK_Panic(MK_panic_UnexpectedThreadWithParent);
			}
		}

		/* Dequeue the culprit by setting the predecessor's "next" to the culprit's "next".
		*/
		pred->next = culprit->next;
		culprit->next = MK_NULL;

		/* The culprit can now be terminated
		*/
		MK_HwFreeThreadRegisters(culprit->regs);
		MK_TerminateThread(culprit);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
