/* Mk_k_ppacontinue.c
 *
 * This file contains the MK_PpaContinue() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppacontinue.c 17603 2014-10-30 10:09:05Z stpo8218 $
*/
/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_C_COMPL_010:stmt]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  This function exists for reasons of consistency and easy maintainability of
 *  a call table mechanism.
 */
/* Deviation DCG-1 <*> */
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>

/* MK_PpaContinue()
 *
 * !LINKSTO Microkernel.Function.MK_PpaContinue, 1
 * !doctype src
*/
void MK_PpaContinue(void)
{
	/* This function is intentionally empty.
	*/
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
