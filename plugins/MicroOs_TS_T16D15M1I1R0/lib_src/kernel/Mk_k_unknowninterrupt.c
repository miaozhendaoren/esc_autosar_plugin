/* Mk_k_unknowninterrupt.c
 *
 * This file contains the function MK_UnknownInterrupt()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_unknowninterrupt.c 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 14.2 (required)
 *  All non-null statements shall either a) have at least one side effect however executed,
 *  or b) cause control flow to change.
 *
 * Reason:
 *  MK_UnknownInterrupt is called by the kernel if it cannot continue because an unknown
 *  interrupt occurred. The function does not use its parameters itself, they are only
 *  provided for debugging purposes. To keep compilers from reporting an unused parameter
 *  warning, the toolchain-dependent macro MK_PARAM_UNUSED() is used which usually evaluates
 *  to a null-statement.
*/

#include <public/Mk_public_types.h>
#include <private/Mk_panic.h>
#include <private/Mk_interrupt.h>
#include <private/Mk_tool.h>

/* MK_UnknownInterrupt() - called if an unconfigured interrupt occurs.
 *
 * This function is used to fill all unused entries in the vector table.
 *
 * !LINKSTO Microkernel.Function.MK_UnknownInterrupt, 1
 * !doctype src
*/
void MK_UnknownInterrupt(mk_objectid_t intnum, mk_hwvectorcode_t vecnum)
{
	/* Deviation MISRA-1 <+2> */
	MK_PARAM_UNUSED(intnum);
	MK_PARAM_UNUSED(vecnum);

	/* !LINKSTO Microkernel.Panic.MK_panic_UnexpectedInterrupt, 1
	 * !doctype src
	*/
	MK_Panic(MK_panic_UnexpectedInterrupt);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
