/* Mk_k_startthread.c
 *
 * This file contains the MK_StartThread() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_startthread.c 16025 2014-05-07 10:23:06Z dh $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>

/* MK_StartThread() starts a thread to run the given function
 *
 * Parameters:
 *	thread		- the address of the thread structure
 *	threadCfg	- address of a thread config block for this thread
 *
 * Precondition:
 *	the thread is idle
 *
 * !LINKSTO Microkernel.Function.MK_StartThread, 1
 * !doctype src
*/
void MK_StartThread
(	mk_thread_t *thread,
	const mk_threadcfg_t *threadCfg
)
{
	/* "Restart" the thread (initialize thread fields and enqueue)
	 * Must be done first because this initializes thread->registers, which is needed
	 * for the next step...
	*/
	MK_RestartThread(thread, threadCfg);

	/* Set the parent to NULL. Most, if not all, places where MK_StartThread is used
	 * do not need a parent. In any case it can be overwritten.
	*/
	thread->parent = MK_NULL;

	/* Some processors need to allocate some space for register windows.
	*/
	MK_HwAllocateThreadRegisters(thread->regs);

	/* Load the initial values into the registers.
	*/
	MK_FillThreadRegisters(threadCfg);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
