/* Mk_k_initresources.c
 *
 * This file contains the function MK_InitResources()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_initresources.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_resource.h>

/* MK_InitResources() initializes the kernel's resource table
 *
 * !LINKSTO Microkernel.Function.MK_InitResources, 1
 * !doctype src
*/
void MK_InitResources(void)
{
	mk_objectid_t i;

	for ( i = 0; i < MK_nResources; i++ )
	{
		MK_resource[i].ceilingPriority = MK_resourceCfg[i].ceilingPriority;
		MK_resource[i].lockLevel = MK_resourceCfg[i].lockLevel;
		MK_resource[i].maxCount = MK_resourceCfg[i].maxCount;
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
