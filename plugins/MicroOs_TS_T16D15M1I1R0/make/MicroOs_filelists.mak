# MicroOs_filelists.make - lists of files that go into the microkernel build
#
# This file just contains lists of files that go into various parts of the
# build.
# The file is also included into the unsupported make so it must be compatible.
# So ...
# Anyone who puts DOS/Windows-specific constructs here (including, but not
# limited to, backslash separators, cygwin dependencies, dependencies on
# asc_Make) will be taken out and summarily executed.
# YOU HAVE BEEN WARNED
#
# (c) Elektrobit Automotive GmbH
#
# $Id: MicroOs_filelists.mak 18067 2015-01-22 08:45:52Z stpo8218 $

#===============================================================================
# System call list
#===============================================================================
# MK_SYSTEMCALL_BASELIST is a list of base filenames for the system calls.
# Note: waitgetclearevent is not in this list because there's no sys function
MK_SYSTEMCALL_BASELIST += activatetask
MK_SYSTEMCALL_BASELIST += chaintask
MK_SYSTEMCALL_BASELIST += getresource
MK_SYSTEMCALL_BASELIST += releaseresource
MK_SYSTEMCALL_BASELIST += startos
MK_SYSTEMCALL_BASELIST += schedule
MK_SYSTEMCALL_BASELIST += shutdown
MK_SYSTEMCALL_BASELIST += terminateself
MK_SYSTEMCALL_BASELIST += waitevent
MK_SYSTEMCALL_BASELIST += setevent
MK_SYSTEMCALL_BASELIST += clearevent
MK_SYSTEMCALL_BASELIST += gettaskid
MK_SYSTEMCALL_BASELIST += getisrid
MK_SYSTEMCALL_BASELIST += gettaskstate
MK_SYSTEMCALL_BASELIST += reporterror
MK_SYSTEMCALL_BASELIST += startforeignthread
MK_SYSTEMCALL_BASELIST += getapplicationid
MK_SYSTEMCALL_BASELIST += terminateapplication
MK_SYSTEMCALL_BASELIST += selftest
MK_SYSTEMCALL_BASELIST += enableinterruptsource
MK_SYSTEMCALL_BASELIST += disableinterruptsource

#===============================================================================
# Lists of base filenames for the kernel library
#===============================================================================
# MK_LIBSRCKERN_KLIB_BASELIST is the list of all files in the plugin/lib_src/kernel directory that
# go into the kernel library.
# The files are listed without prefix or extension.
# DON'T PUT SYSTEM-CALL KERNEL-SIDE FUNCTIONS HERE! - Put them in MK_SYSTEMCALL_BASELIST!!!
MK_LIBSRCKERN_KLIB_BASELIST +=	activatetask
MK_LIBSRCKERN_KLIB_BASELIST +=	clearresourcelist
MK_LIBSRCKERN_KLIB_BASELIST +=	data
MK_LIBSRCKERN_KLIB_BASELIST +=	datainitcheck
MK_LIBSRCKERN_KLIB_BASELIST	+=	dispatch
MK_LIBSRCKERN_KLIB_BASELIST	+=	dispatchinterruptsoft
MK_LIBSRCKERN_KLIB_BASELIST	+=	enqueuethread
MK_LIBSRCKERN_KLIB_BASELIST	+=	errorinternaltoosek
MK_LIBSRCKERN_KLIB_BASELIST +=	fillthreadregisters
MK_LIBSRCKERN_KLIB_BASELIST +=	findfirstthread
MK_LIBSRCKERN_KLIB_BASELIST +=	getapplicationid
MK_LIBSRCKERN_KLIB_BASELIST +=	idle
MK_LIBSRCKERN_KLIB_BASELIST +=	initapplications
MK_LIBSRCKERN_KLIB_BASELIST +=	initdatasections
MK_LIBSRCKERN_KLIB_BASELIST +=	initexecutionbudgetmonitor
MK_LIBSRCKERN_KLIB_BASELIST +=	initinterrupts
MK_LIBSRCKERN_KLIB_BASELIST +=	initmemoryprotection
MK_LIBSRCKERN_KLIB_BASELIST +=	initmemoryregion
MK_LIBSRCKERN_KLIB_BASELIST +=	initresources
MK_LIBSRCKERN_KLIB_BASELIST +=  isexecutionbudgetconfigured
MK_LIBSRCKERN_KLIB_BASELIST	+=	jqappend
MK_LIBSRCKERN_KLIB_BASELIST	+=	jqinit
MK_LIBSRCKERN_KLIB_BASELIST	+=	jqremove
MK_LIBSRCKERN_KLIB_BASELIST	+=	jqremovealltasksofapp
MK_LIBSRCKERN_KLIB_BASELIST	+=	threadqueueempty
MK_LIBSRCKERN_KLIB_BASELIST	+=	panic
MK_LIBSRCKERN_KLIB_BASELIST	+=	panicstop
MK_LIBSRCKERN_KLIB_BASELIST +=	ppacontinue
MK_LIBSRCKERN_KLIB_BASELIST +=	ppapanic
MK_LIBSRCKERN_KLIB_BASELIST +=	ppapanicstop
MK_LIBSRCKERN_KLIB_BASELIST +=	ppaquarantineobject
MK_LIBSRCKERN_KLIB_BASELIST +=	ppashutdown
MK_LIBSRCKERN_KLIB_BASELIST +=	ppaterminateobject
MK_LIBSRCKERN_KLIB_BASELIST +=	ppaterminatetaskisr
MK_LIBSRCKERN_KLIB_BASELIST +=	ppaterminateapplication
MK_LIBSRCKERN_KLIB_BASELIST +=	ppaterminateapplicationrestart
MK_LIBSRCKERN_KLIB_BASELIST +=	ppaterminatethread
MK_LIBSRCKERN_KLIB_BASELIST +=	reporterror
MK_LIBSRCKERN_KLIB_BASELIST +=	reportprotectionfault
MK_LIBSRCKERN_KLIB_BASELIST	+=	requeuethread
MK_LIBSRCKERN_KLIB_BASELIST +=	restartthread
MK_LIBSRCKERN_KLIB_BASELIST +=	restartapplication
MK_LIBSRCKERN_KLIB_BASELIST +=	selftestexecbudget
MK_LIBSRCKERN_KLIB_BASELIST +=	selfteststate
MK_LIBSRCKERN_KLIB_BASELIST +=	shutdown
MK_LIBSRCKERN_KLIB_BASELIST +=	startforeignthread
MK_LIBSRCKERN_KLIB_BASELIST +=	startkernel
MK_LIBSRCKERN_KLIB_BASELIST +=	startthread
MK_LIBSRCKERN_KLIB_BASELIST +=	startthreadforisr
MK_LIBSRCKERN_KLIB_BASELIST +=	startup
MK_LIBSRCKERN_KLIB_BASELIST +=	startuppanic
MK_LIBSRCKERN_KLIB_BASELIST +=	syscall
MK_LIBSRCKERN_KLIB_BASELIST +=	terminatethread
MK_LIBSRCKERN_KLIB_BASELIST +=	terminateapplication
MK_LIBSRCKERN_KLIB_BASELIST +=	unknowninterrupt
MK_LIBSRCKERN_KLIB_BASELIST +=	unknownsyscall
MK_LIBSRCKERN_KLIB_BASELIST +=	$(addprefix sys, $(MK_SYSTEMCALL_BASELIST))

#===============================================================================
# Lists of base filenames for the user library
#===============================================================================
# MK_LIBSRCUSER_ULIB_BASELIST is the list of all files in the plugin/lib_src/user directory
# that go into the User library.
# The files are listed without prefix or extension.
MK_LIBSRCUSER_ULIB_BASELIST += libgetalarm
MK_LIBSRCUSER_ULIB_BASELIST += libgetcountervalue
MK_LIBSRCUSER_ULIB_BASELIST += libgetevent
MK_LIBSRCUSER_ULIB_BASELIST += libgetntasks
MK_LIBSRCUSER_ULIB_BASELIST += libgettaskid
MK_LIBSRCUSER_ULIB_BASELIST += libgettaskstate
MK_LIBSRCUSER_ULIB_BASELIST += liberrorgetserviceid
MK_LIBSRCUSER_ULIB_BASELIST += liberrorgetparameter
MK_LIBSRCUSER_ULIB_BASELIST += libconditionalgetresource
MK_LIBSRCUSER_ULIB_BASELIST += libdifftime
MK_LIBSRCUSER_ULIB_BASELIST += libdifftime32
MK_LIBSRCUSER_ULIB_BASELIST += libelapsedtime
MK_LIBSRCUSER_ULIB_BASELIST += libelapsedtime32
MK_LIBSRCUSER_ULIB_BASELIST += libelapsedmicroseconds
MK_LIBSRCUSER_ULIB_BASELIST += libfinishselftest
MK_LIBSRCUSER_ULIB_BASELIST += libisschedulenecessary
MK_LIBSRCUSER_ULIB_BASELIST += libmuldiv
MK_LIBSRCUSER_ULIB_BASELIST += libwaitgetclearevent

#===============================================================================
# Lists of base filenames for QM files in the user library
#===============================================================================
# MK_LIBSRCQM_ULIB_BASELIST is the list of all files in the plugin/lib_src/QM directory that
# go into the user library.
# The files are listed without prefix or extension.
MK_LIBSRCQM_ULIB_BASELIST	+=	qmdonothing

#===============================================================================
# Lists of architecture base filenames for the user library
#===============================================================================
# MK_LIBSRCCPUFAMILY_ULIB_BASELIST_x is the list of all files in the plugin/lib_src/$CPUFAMILY directory
# that go into the User library.
# The files are listed without prefix or extension. These files go into the user library.
#
# The basenames of these files are the same for all architectures because they are the stub
# functions that make the system calls.
# It is not known here whether these are C or assembly-language files, so the
# architecture file list must make these assignments with _x replaced by _S or _C as appropriate
#MK_LIBSRCCPUFAMILY_ULIB_BASELIST_x	+=	$(addprefix usr, $(MK_SYSTEMCALL_BASELIST))
#MK_LIBSRCCPUFAMILY_ULIB_BASELIST_x	+=	$(addprefix usr, $(MK_OSCALL_BASELIST))

#===============================================================================
# Lists of base filenames for the error library
#===============================================================================

#===============================================================================
# Lists of base filenames for the binary build
#===============================================================================
# MK_SRC_CFG_BASELIST is the list of all configuration-dependent files in the plugin/src directory,
# without prefix or extension. These files go straight into the system build
MK_SRC_CFG_BASELIST +=	configuration
MK_SRC_CFG_BASELIST +=	configuration_application
MK_SRC_CFG_BASELIST +=	configuration_foreign
MK_SRC_CFG_BASELIST +=	configuration_interrupt
MK_SRC_CFG_BASELIST +=	configuration_job
MK_SRC_CFG_BASELIST +=	configuration_memoryprotection
MK_SRC_CFG_BASELIST +=	configuration_protectionaction
MK_SRC_CFG_BASELIST +=	configuration_resource
MK_SRC_CFG_BASELIST +=	configuration_task
MK_SRC_CFG_BASELIST +=	configuration_syscall
#MK_SRC_CFG_BASELIST +=	board
MK_SRC_CFG_BASELIST +=	softwarevectortable

# MK_GEN_CFG_BASELIST is the list of all generated files
# without prefix or extension. These files go straight into the system build
MK_GEN_CFG_BASELIST +=	gen_global

# OS_GEN_CFG_BASELIST is the list of all generated files (OS files)
# without prefix or extension. These files go straight into the system build
OS_GEN_CFG_BASELIST +=	gen

# Any addon can drop a named MicroOs_addon_*_filelists.mak in this
# directory. Include it as well.
-include $(MicroOs_MAKE_PATH)/MicroOs_addon_*_filelists.mak
