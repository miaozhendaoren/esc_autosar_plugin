#################################################################
##
## AUTOSAR MicroOs Operating System Micro Kernel
##
## This makefile includes definitions used by other modules.
##
## $Id: MicroOs_defs.mak 15983 2014-05-05 05:37:20Z masa8317 $
## Release: NIGHTLYBUILD_OS_MK_TRUNK_TRICORE_TC277
##
## (c) Elektrobit Automotive GmbH
##
#################################################################

#################################################################
# DEFINITIONS

MicroOs_CORE_PATH       := $(SSC_ROOT)\MicroOs_$(MicroOs_VARIANT)

include $(MicroOs_CORE_PATH)\make\Mk_make_translation_layer.mak

MicroOs_INC_PATH        := $(MicroOs_CORE_PATH)\include

MicroOs_SRC_PATH        := $(MicroOs_CORE_PATH)\src
MicroOs_CPUFAMILYSRC_PATH    := $(MicroOs_SRC_PATH)\$(MK_CPUFAMILY)

MicroOs_LIBSRC_PATH     := $(MicroOs_CORE_PATH)\lib_src
MicroOs_LIBSRCCPUFAMILY_PATH := $(MicroOs_LIBSRC_PATH)\$(MK_CPUFAMILY)
MicroOs_LIBSRCKERN_PATH := $(MicroOs_LIBSRC_PATH)\kernel
MicroOs_LIBSRCUSER_PATH := $(MicroOs_LIBSRC_PATH)\user
MicroOs_LIBSRCQM_PATH   := $(MicroOs_LIBSRC_PATH)\QM

MicroOs_MAKE_PATH       := $(MicroOs_CORE_PATH)\make
MicroOs_MAKECPUFAMILY_PATH   := $(MicroOs_MAKE_PATH)\$(MK_CPUFAMILY)
MicroOs_MAKECPU_PATH    := $(MicroOs_MAKECPUFAMILY_PATH)\$(MK_CPU)

MicroOs_LIB_BASE        := $(MicroOs_CORE_PATH)\lib
MicroOs_LIBCPUFAMILY_PATH    := $(MicroOs_LIB_BASE)\$(MK_CPUFAMILY)
MicroOs_LIBCPU_PATH     := $(MicroOs_LIBCPUFAMILY_PATH)\$(MK_CPU)
MicroOs_LIB_PATH        := $(MicroOs_LIBCPU_PATH)\$(MK_TOOL)

MicroOs_OUTPUT_PATH     := $(AUTOSAR_BASE_OUTPUT_PATH)

MicroOs_OUTPUT_INC      := $(MicroOs_OUTPUT_PATH)\include
MicroOs_OUTPUT_SRC      := $(MicroOs_OUTPUT_PATH)\src
MicroOs_OUTPUT_MAKE     := $(MicroOs_OUTPUT_PATH)\make

MicroOs_GEN_FILES       := \
		$(MicroOs_OUTPUT_SRC)\$(MicroOs_SrcFile_prefix)gen_global.c \
		$(MicroOs_OUTPUT_INC)\$(MicroOs_SrcFile_prefix)gen_config.h \
		$(MicroOs_OUTPUT_INC)\$(MicroOs_SrcFile_prefix)gen_user.h \
		$(MicroOs_OUTPUT_MAKE)\$(MicroOs_SrcFile_prefix)gen_objects.make \

MicroOs_SrcFile_prefix  = Mk_
MicroOs_File_prefix     = Mk_
MicroOs_Arch_prefix     = $(MicroOs_File_prefix)$(MK_CPUFAMILY)_

TRESOS_GEN_FILES        += $(MicroOs_GEN_FILES)

#################################################################
# REGISTRY
SSC_PLUGINS               += MicroOs

# Include the file lists. Derivative list is only included if present.
-include $(MicroOs_MAKECPU_PATH)\MicroOs_filelists_$(MK_CPU).mak
include $(MicroOs_MAKECPUFAMILY_PATH)\MicroOs_filelists_$(MK_CPUFAMILY).mak
include $(MicroOs_MAKE_PATH)\MicroOs_filelists.mak

# Now suck in the architecture- and derivative-specific make definitions
# needed at this early position for possible library build flags
include $(MicroOs_MAKECPUFAMILY_PATH)\Mk_defs_$(MK_CPUFAMILY).mak
-include $(MicroOs_MAKECPU_PATH)\Mk_defs_$(MK_CPUFAMILY)_$(MK_CPU).mak

ifneq ($(MK_CPUFAMILY_LIB_FLAGS),)
MK_CPUFAMILY_LIB_FLAG_SUFFIX=_$(MK_CPUFAMILY_LIB_FLAGS)
endif

# Names of the libraries
MK_KERNEL_LIB_NAME = MicroOs_libMkKern_n$(MK_CPUFAMILY_LIB_FLAG_SUFFIX)
MK_USER_LIB_NAME   = MicroOs_libMkUser_n$(MK_CPUFAMILY_LIB_FLAG_SUFFIX)

# Now we can construct all the lists of files that asc_Make needs.
$(MK_KERNEL_LIB_NAME)_FILES += \
	$(addprefix $(MicroOs_LIBSRCKERN_PATH)\$(MicroOs_File_prefix)k_, \
		$(addsuffix .$(CC_FILE_SUFFIX), \
			$(MK_LIBSRCKERN_KLIB_BASELIST)))

$(MK_KERNEL_LIB_NAME)_FILES += \
	$(addprefix $(MicroOs_LIBSRCCPUFAMILY_PATH)\$(MicroOs_Arch_prefix), \
		$(addsuffix .$(CC_FILE_SUFFIX), \
			$(MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C)))

$(MK_KERNEL_LIB_NAME)_FILES += \
	$(addprefix $(MicroOs_LIBSRCCPUFAMILY_PATH)\$(MicroOs_Arch_prefix), \
		$(addsuffix .$(ASM_FILE_SUFFIX), \
			$(MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S)))

$(MK_USER_LIB_NAME)_FILES += \
	$(addprefix $(MicroOs_LIBSRCUSER_PATH)\$(MicroOs_File_prefix)u_, \
		$(addsuffix .$(CC_FILE_SUFFIX), \
			$(MK_LIBSRCUSER_ULIB_BASELIST)))

$(MK_USER_LIB_NAME)_FILES += \
	$(addprefix $(MicroOs_LIBSRCQM_PATH)\$(MicroOs_File_prefix), \
		$(addsuffix .$(CC_FILE_SUFFIX), \
			$(MK_LIBSRCQM_ULIB_BASELIST)))

$(MK_USER_LIB_NAME)_FILES += \
	$(addprefix $(MicroOs_LIBSRCCPUFAMILY_PATH)\$(MicroOs_Arch_prefix), \
		$(addsuffix .$(CC_FILE_SUFFIX), \
			$(MK_LIBSRCCPUFAMILY_ULIB_BASELIST_C)))

$(MK_USER_LIB_NAME)_FILES += \
	$(addprefix $(MicroOs_LIBSRCCPUFAMILY_PATH)\$(MicroOs_Arch_prefix), \
		$(addsuffix .$(ASM_FILE_SUFFIX), \
			$(MK_LIBSRCCPUFAMILY_ULIB_BASELIST_S)))

# list for non lib C and S files
MK_SRC_C_NAME     = MicroOs_srcMkC
MK_SRC_S_NAME     = MicroOs_srcMkS
# possibility to put all together in one lib
MK_SRC_LIB_NAME   = MicroOs_libMkSrc

$(MK_SRC_C_NAME)_FILES += \
	$(addprefix $(MicroOs_SRC_PATH)\$(MicroOs_SrcFile_prefix), \
		$(addsuffix .$(CC_FILE_SUFFIX), \
			$(MK_SRC_CFG_BASELIST)))

$(MK_SRC_C_NAME)_FILES += \
	$(addprefix $(MicroOs_OUTPUT_SRC)\$(MicroOs_SrcFile_prefix), \
		$(addsuffix .$(CC_FILE_SUFFIX), \
			$(MK_GEN_CFG_BASELIST)))

$(MK_SRC_C_NAME)_FILES += \
	$(addprefix $(MicroOs_CPUFAMILYSRC_PATH)\$(MicroOs_SrcFile_prefix), \
		$(addsuffix .$(CC_FILE_SUFFIX), \
			$(MK_SRCCPUFAMILY_BASELIST_C)))

$(MK_SRC_S_NAME)_FILES += \
	$(addprefix $(MicroOs_CPUFAMILYSRC_PATH)\$(MicroOs_SrcFile_prefix), \
		$(addsuffix .$(ASM_FILE_SUFFIX), \
			$(MK_SRCCPUFAMILY_BASELIST_S)))

$(MK_SRC_LIB_NAME)_FILES = $($(MK_SRC_C_NAME)_FILES) $($(MK_SRC_S_NAME)_FILES)

DIRECTORIES_TO_CREATE += $(MicroOs_LIB_PATH)

ifneq ($(BUILD_MODE),LIB)
DIRECTORIES_TO_CREATE += $(MicroOs_LIB_PATH)\$(MK_KERNEL_LIB_NAME)
DIRECTORIES_TO_CREATE += $(MicroOs_LIB_PATH)\$(MK_USER_LIB_NAME)
endif

$(MK_KERNEL_LIB_NAME)_LIB_OUTPUT_PATH = $(MicroOs_LIB_PATH)
$(MK_USER_LIB_NAME)_LIB_OUTPUT_PATH = $(MicroOs_LIB_PATH)

ifneq ($(BUILD_MODE),LIB)

define defineMkKernOutputPATH
$(1)_OBJ_OUTPUT_PATH = $(MicroOs_LIB_PATH)\$(MK_KERNEL_LIB_NAME)
endef

define defineMkUserOutputPATH
$(1)_OBJ_OUTPUT_PATH = $(MicroOs_LIB_PATH)\$(MK_USER_LIB_NAME)
endef

# Some stuff missing here

else

define defineMkKernOutputPATH
$(1)_OBJ_OUTPUT_PATH = $(PROJECT_ROOT)\output\obj
endef

define defineMkUserOutputPATH
$(1)_OBJ_OUTPUT_PATH = $(PROJECT_ROOT)\output\obj
endef

# Some stuff missing here

endif

define defineMkSrcOutputPATH
$(1)_OBJ_OUTPUT_PATH = $(PROJECT_ROOT)\output\obj
endef

# Standard include paths
CC_INCLUDE_PATH += $(MicroOs_OUTPUT_INC)
CC_INCLUDE_PATH += $(MicroOs_INC_PATH)

CPP_INCLUDE_PATH += $(MicroOs_OUTPUT_INC)
CPP_INCLUDE_PATH += $(MicroOs_INC_PATH)

ASM_INCLUDE_PATH += $(MicroOs_OUTPUT_INC)
ASM_INCLUDE_PATH += $(MicroOs_INC_PATH)
ASM_INCLUDE_PATH += $(PROJECT_ROOT)\include


# FIXME: what is this?
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_KERNEL_LIB_NAME)_FILES)))),$(eval $(call defineMkKernOutputPATH,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_USER_LIB_NAME)_FILES)))),$(eval $(call defineMkUserOutputPATH,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_ERROR_LIB_NAME)_FILES)))),$(eval $(call defineMkErrorOutputPATH,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_SRC_C_NAME)_FILES)))),$(eval $(call defineMkSrcOutputPATH,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_SRC_S_NAME)_FILES)))),$(eval $(call defineMkSrcOutputPATH,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_SRC_LIB_NAME)_FILES)))),$(eval $(call defineMkSrcOutputPATH,$(SRC))))


$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_KERNEL_LIB_NAME)_FILES)))),$(eval $(call defineMkKernCC_OPT,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_USER_LIB_NAME)_FILES)))),$(eval $(call defineMkUserCC_OPT,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_ERROR_LIB_NAME)_FILES)))),$(eval $(call defineMkErrorCC_OPT,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_SRC_C_NAME)_FILES)))),$(eval $(call defineMkSrcCC_OPT,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_SRC_S_NAME)_FILES)))),$(eval $(call defineMkSrcCC_OPT,$(SRC))))
$(foreach SRC,$(basename $(notdir $(subst \,/,$($(MK_SRC_LIB_NAME)_FILES)))),$(eval $(call defineMkSrcCC_OPT,$(SRC))))

