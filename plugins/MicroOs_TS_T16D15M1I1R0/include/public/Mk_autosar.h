/* Mk_autosar.h
 *
 * This header file defines the Autosar system in terms of the microkernel's
 * public interface, as far as possible.
 *
 * This file is included by MicroOs.h. It is also included by any public functions that
 * need to use Autosar types.
 *
 * This file contains type definition for functionality implemented as ASIL or QM.
 * It is impossible to move QM definitions out of the microkernel, because the calls
 * to QM-OS functions are ASIL as long as the OS threads aren't running.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_autosar.h 15853 2014-04-17 10:26:58Z dh $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.13 (advisory)
 *  The # and ## preprocessor operators should not be used.
 *
 * Reason:
 *  This construct is mandated by the Autosar OS specification.
 *
 * MISRA-2) Deviated Rule: 19.4 (required)
 *  C macros shall only expand to as braced initializer, a constant, a parenthesized expression, a type qualifier,
 *  a storage class specifier, or a do-while-zero construct.
 *
 * Reason:
 *  This construct (or something similar) is needed to satisfy the Autosar requirement for these declaration
 *  macros. There is nothing to declare; however, the macro has to expand to something that, when terminated
 *  by a semicolon and used at the file level, results in syntactically-correct C code.
 *
 * MISRA-3) Deviated Rule: 5.1 (required)
 *  Identifiers (internal and external) shall not rely on the significance of more than 31 characters.
 *
 * Reason:
 *  These names are mandated by Autosar.
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_NEG_010]
 *  Symbol names shall not contain a negative in the name.
 *
 * Reason:
 *  The name is mandated by AUTOSAR.
*/

/* !LINKSTO Microkernel.Design.Mk_autosar.h, 1
 * !doctype src
*/

#ifndef MK_AUTOSAR_H
#define MK_AUTOSAR_H

#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <public/Mk_error.h>
#include <public/Mk_statustype.h>

#ifndef MK_ASM

/* mk_scheduletablestatustype_e provides an enumeration type containing the states of a schedule table
*/
enum mk_scheduletablestatustype_e
{
	SCHEDULETABLE_UNKNOWNSTATE = 0,
	SCHEDULETABLE_STOPPED = 1,
	SCHEDULETABLE_NEXT = 2,
	SCHEDULETABLE_WAITING = 3,
	SCHEDULETABLE_RUNNING = 4,
	SCHEDULETABLE_RUNNING_AND_SYNCHRONOUS = 5
};


/* mk_objecttypetype_e provides an enumeration type containing all the different kinds of object in AUTOSAR
 * (for use in GetObjectOwnership etc.)
*/
enum mk_objecttypetype_e
{
	OBJECT_TASK,
	OBJECT_ISR,
	OBJECT_ALARM,
	OBJECT_RESOURCE,
	OBJECT_COUNTER,
	OBJECT_SCHEDULETABLE
};


/* These defines are used by RestartType, which is used as one of the parameters to TerminateApplication.
*/
/* Deviation DCG-1 */
#define NO_RESTART		MK_FALSE
#define RESTART			MK_TRUE


/* These defines are used by AccessType, which is used as the return value from GetObjectAccess.
*/
/* Deviation DCG-1 */
#define NO_ACCESS		MK_FALSE
#define ACCESS			MK_TRUE


/* Complete list of all AUTOSAR (including OSEK) types
 * The list is sorted alphabetically unless where impossible (reference type follow their base)
*/
typedef mk_boolean_t						AccessType;
typedef mk_objectid_t						AlarmType;
typedef struct mk_alarmbase_s				AlarmBaseType;
typedef AlarmBaseType					   *AlarmBaseRefType;
typedef mk_objectid_t						ApplicationType;
typedef mk_appstate_t						ApplicationStateType;
typedef ApplicationStateType			   *ApplicationStateRefType;
typedef mk_objectid_t						AppModeType;
typedef mk_objectid_t						CounterType;
typedef mk_uint32_t							EventMaskType;
typedef EventMaskType					   *EventMaskRefType;
typedef mk_objectid_t						ISRType;
typedef mk_boolean_t						ObjectAccessType;
typedef enum mk_objecttypetype_e			ObjectTypeType;
typedef mk_serviceid_t						OSServiceIdType;
typedef mk_size_t							MemorySizeType;
typedef void							   *MemoryStartAddressType;
typedef mk_uint32_t							PhysicalTimeType;
typedef mk_protectionaction_t				ProtectionReturnType;
typedef mk_objectid_t						ResourceType;
typedef mk_boolean_t						RestartType;
typedef enum mk_scheduletablestatustype_e	ScheduleTableStatusType;
typedef ScheduleTableStatusType			   *ScheduleTableStatusRefType;
typedef mk_objectid_t						ScheduleTableType;
/*											StatusType is in Mk_statustype.h */
typedef mk_taskstate_t						TaskStateType;
typedef TaskStateType					   *TaskStateRefType;
typedef mk_objectid_t						TaskType;
typedef TaskType						   *TaskRefType;
typedef mk_uint32_t							TickType;
typedef TickType						   *TickRefType;
typedef mk_objectid_t						TrustedFunctionIndexType;
typedef void							   *TrustedFunctionParameterRefType;


/* mk_alarmbase_s is used for the AUTOSAR AlarmBase type
*/
struct mk_alarmbase_s
{
	TickType maxallowedvalue;
	TickType ticksperbase;
	TickType mincycle;
};


/*
 * !LINKSTO Microkernel.Interface.API.Direct.AutosarMapping, 1
 * !doctype src
*/

/* Complete list of all AUTOSAR (including OSEK) services
 * This is sorted alphabetically.
*/
#define ActivateTask(t)				((StatusType)MK_UsrActivateTask(t))
#define CallTrustedFunction(id, p)	((StatusType)MK_UsrStartForeignThread2((mk_objectid_t) (id),         \
																			 (mk_parametertype_t) (id),  \
																			 (mk_parametertype_t) (p)))
#define CancelAlarm(a)				((StatusType)MK_UsrStartForeignThread1(MK_SC_CancelAlarm, a))
#define ChainTask(t)				((StatusType)MK_UsrChainTask(t))
/* CheckISRMemoryAccess				not implemented */
/* CheckObjectAccess				not implemented */
/* CheckObjectOwnership				not implemented */
/* CheckTaskMemoryAccess			not implemented */
#define ClearEvent(e)				((StatusType)MK_UsrClearEvent(e))

/* !LINKSTO Microkernel.Interface.API.Covered.DisableAllInterrupts, 2
 * !doctype src
*/
#define DisableAllInterrupts()		((void)MK_UsrGetResource(MK_resLockCat1))

/* !LINKSTO Microkernel.Interface.API.Covered.EnableAllInterrupts, 1
 * !doctype src
*/
#define EnableAllInterrupts()		((void)MK_UsrReleaseResource(MK_resLockCat1))

AppModeType GetActiveApplicationMode(void);
StatusType GetAlarm(AlarmType, TickRefType);
StatusType GetAlarmBase(AlarmType, AlarmBaseRefType);
#define GetApplicationID()			((ApplicationType)MK_UsrGetApplicationId())
StatusType GetCounterValue(CounterType, TickRefType);
StatusType GetElapsedCounterValue(CounterType, TickRefType, TickRefType);
StatusType GetEvent(TaskType, EventMaskRefType);
#define GetISRID()					((ISRType)MK_UsrGetIsrId())
#define GetResource(r)				((StatusType)MK_UsrGetResource(r))
StatusType GetScheduleTableStatus(ScheduleTableType, ScheduleTableStatusRefType);
StatusType GetTaskID(TaskRefType);
StatusType GetTaskState(TaskType, TaskStateRefType);
#define IncrementCounter(c)			((StatusType)MK_UsrStartForeignThread1(MK_SC_IncrementCounter, c))
#define NextScheduleTable(s1, s2)	((StatusType)MK_UsrStartForeignThread2(MK_SC_NextScheduleTable, s1, s2))
#define ReleaseResource(r)			((StatusType)MK_UsrReleaseResource(r))

/* !LINKSTO Microkernel.Interface.API.Covered.ResumeAllInterrupts, 1
 * !doctype src
*/
#define ResumeAllInterrupts()		((void)MK_UsrReleaseResource(MK_resLockCat1))

/* !LINKSTO Microkernel.Interface.API.Covered.ResumeOSInterrupts, 1
 * !doctype src
*/
#define ResumeOSInterrupts()		((void)MK_UsrReleaseResource(MK_resLockCat2))

#define Schedule()					((StatusType)MK_UsrSchedule())
#define SetAbsAlarm(a, i, c)		((StatusType)MK_UsrStartForeignThread3(MK_SC_SetAbsAlarm, a, i, c))
#define SetEvent(t, e)				((StatusType)MK_UsrSetEvent(t, e))
#define SetRelAlarm(a, i, c)		((StatusType)MK_UsrStartForeignThread3(MK_SC_SetRelAlarm, a, i, c))
#define SetScheduleTableAsync(s)	((StatusType)MK_UsrStartForeignThread1(MK_SC_SetScheduleTableAsync, s))
#define ShutdownOS(e)				((void)MK_UsrShutdown(e))
#define StartOS(am)					((void)MK_UsrStartOs(am))

/* !LINKSTO Microkernel.Interface.API.Indirect.AutosarMapping.StartScheduleTableRelOrAbs, 1
 * !doctype src
*/
#define StartScheduleTableAbs(s, t)	((StatusType)MK_UsrStartForeignThread3(MK_SC_StartScheduleTable, s, t, MK_FALSE))
#define StartScheduleTableRel(s, t)	((StatusType)MK_UsrStartForeignThread3(MK_SC_StartScheduleTable, s, t, MK_TRUE))

#define StartScheduleTableSynchron(s)	((StatusType)MK_UsrStartForeignThread1(MK_SC_StartScheduleTableSync, s))
#define StopScheduleTable(s)		((StatusType)MK_UsrStartForeignThread1(MK_SC_StopScheduleTable, s))

/* !LINKSTO Microkernel.Interface.API.Covered.SuspendAllInterrupts, 1
 * !doctype src
*/
#define SuspendAllInterrupts()		((void)MK_UsrGetResource(MK_resLockCat1))

/* !LINKSTO Microkernel.Interface.API.Covered.SuspendOSInterrupts, 1
 * !doctype src
*/
#define SuspendOSInterrupts()		((void)MK_UsrGetResource(MK_resLockCat2))

#define SyncScheduleTable(s, g1)	((StatusType)MK_UsrStartForeignThread2(MK_SC_SyncScheduleTable, s, g1))
#define TerminateApplication(a, r)	((StatusType)MK_UsrTerminateApplication(a, r))
#define TerminateTask()				((StatusType)MK_UsrTerminateSelf())
#define WaitEvent(e)				((StatusType)MK_UsrWaitEvent(e))


/* These macros are not useful. The IDs of the tasks etc. are defined as macros that expand to
 * literal constants.
 * We define these macros as a harmless function prototype to swallow the semicolon without
 * annoying some compilers.
*/
/* Deviation MISRA-2 <+4> */
#define DeclareTask(t)		void MK_Idle(void)
#define DeclareEvent(t)		void MK_Idle(void)
#define DeclareResource(t)	void MK_Idle(void)
#define DeclareAlarm(t)		void MK_Idle(void)

/* If C++ support is ever needed, these macros will need defining with an "extern C" qualifier
 * for C++ modules.
*/
/* Deviation MISRA-1, MISRA-2 <+3> */
#define TASK(x)				void OS_TASK_##x(void)
#define ALARMCALLBACK(x)	void OS_ACB_##x(void)
#define ISR(x)				void OS_ISR_##x(void)

/* Some constants required by the AUTOSAR standard
*/
#define INVALID_TASK			((TaskType)MK_INVALID_TASK)
#define INVALID_ISR				((ISRType)MK_INVALID_ISR)
#define INVALID_OSAPPLICATION	((ApplicationType)MK_APPL_NONE)
#define INVALID_SERVICEID		((OSServiceIdType)MK_sid_InvalidServiceId)

/* Complete list of Service IDs defined by AUTOSAR for error-reporting purposes.
 * The list is sorted alphabetically.
 *
 * !LINKSTO Microkernel.Interface.API.ServiceId, 1
 * !doctype src
 */
#define OSServiceId_ActivateTask					MK_sid_ActivateTask
#define OSServiceId_CallTrustedFunction				MK_sid_StartForeignThread
#define OSServiceId_CancelAlarm						MK_sid_CancelAlarm
#define OSServiceId_ChainTask						MK_sid_ChainTask
#define OSServiceId_CheckISRMemoryAccess			INVALID_SERVICEID			/* Not implemented */
#define OSServiceId_CheckObjectAccess				INVALID_SERVICEID			/* Not implemented */
#define OSServiceId_CheckObjectOwnership			INVALID_SERVICEID			/* Not implemented */
#define OSServiceId_CheckTaskMemoryAccess			INVALID_SERVICEID			/* Not implemented */
#define OSServiceId_ClearEvent						MK_sid_ClearEvent
#define OSServiceId_DisableAllInterrupts			INVALID_SERVICEID			/* Identifies as GetResource */
#define OSServiceId_EnableAllInterrupts				INVALID_SERVICEID			/* Identifies as ReleaseResource */
#define OSServiceId_GetActiveApplicationMode		INVALID_SERVICEID			/* Direct call to QM-OS; no errors */
#define OSServiceId_GetAlarm						MK_sid_GetAlarm
#define OSServiceId_GetAlarmBase					MK_sid_GetAlarmBase
#define OSServiceId_GetApplicationID				MK_sid_GetApplicationId
#define OSServiceId_GetCounterValue					MK_sid_GetCounterValue
#define OSServiceId_GetElapsedCounterValue			INVALID_SERVICEID			/* Identifies as GetCounterValue */
#define OSServiceId_GetEvent						MK_sid_GetEvent
#define OSServiceId_GetISRID						MK_sid_GetIsrId
#define OSServiceId_GetResource						MK_sid_GetResource
#define OSServiceId_GetScheduleTableStatus			MK_sid_GetScheduleTableStatus
#define OSServiceId_GetTaskID						MK_sid_GetTaskId
#define OSServiceId_GetTaskState					MK_sid_GetTaskState
#define OSServiceId_IncrementCounter				MK_sid_IncrementCounter
#define OSServiceId_NextScheduleTable				MK_sid_NextScheduleTable
#define OSServiceId_ReleaseResource					MK_sid_ReleaseResource
#define OSServiceId_ResumeAllInterrupts				INVALID_SERVICEID			/* Identifies as ReleaseResource */
#define OSServiceId_ResumeOSInterrupts				INVALID_SERVICEID			/* Identifies as ReleaseResource */
#define OSServiceId_Schedule						MK_sid_Schedule
#define OSServiceId_SetAbsAlarm						MK_sid_SetAbsAlarm
#define OSServiceId_SetEvent						MK_sid_SetEvent
#define OSServiceId_SetRelAlarm						MK_sid_SetRelAlarm
#define OSServiceId_SetScheduleTableAsync			MK_sid_SetScheduleTableAsync
#define OSServiceId_ShutdownOS						MK_sid_Shutdown
#define OSServiceId_SuspendAllInterrupts			INVALID_SERVICEID			/* Identifies as GetResource */
#define OSServiceId_SuspendOSInterrupts				INVALID_SERVICEID			/* Identifies as GetResource */
#define OSServiceId_StartOS							MK_sid_StartOs
#define OSServiceId_StartScheduleTableAbs			MK_sid_StartScheduleTableAbs
#define OSServiceId_StartScheduleTableRel			MK_sid_StartScheduleTableRel
#define OSServiceId_StartScheduleTableSynchron		MK_sid_StartScheduleTableSync
#define OSServiceId_StopScheduleTable				MK_sid_StopScheduleTable
#define OSServiceId_SyncScheduleTable				MK_sid_SyncScheduleTable
#define OSServiceId_TerminateApplication			MK_sid_TerminateApplication
#define OSServiceId_TerminateTask					MK_sid_TerminateSelf
#define OSServiceId_WaitEvent						MK_sid_WaitEvent


#define OSErrorGetServiceId()						((OSServiceIdType)MK_ErrorGetServiceId())


/* Parameter access macros. The values are only guaranteed if the caller is the error hook function
*/
#define OSError_ActivateTask_TaskID()						((TaskType)			MK_ErrorGetParameter(1))

/* Note that Parameters 2 and 3 for StartForeignThread are parameters 1 and 2 for CallTrustedFunction.
*/
/* Deviation MISRA-3 <START> */
#define OSError_CallTrustedFunction_FunctionIndex()			((TrustedFunctionIndexType)MK_ErrorGetParameter(2))
#define OSError_CallTrustedFunction_FunctionParams()		((TrustedFunctionParameterRefType)MK_ErrorGetParameter(3))
/* Deviation MISRA-3 <STOP> */
#define OSError_ChainTask_TaskID()							((TaskType)			MK_ErrorGetParameter(1))
#define OSError_GetTaskID_TaskID()							((TaskRefType)		MK_ErrorGetParameter(1))
#define OSError_GetTaskState_TaskID()						((TaskType)			MK_ErrorGetParameter(1))
#define OSError_GetTaskState_State()						((TaskStateRefType)	MK_ErrorGetParameter(2))
#define OSError_GetResource_ResID()							((ResourceType)		MK_ErrorGetParameter(1))
#define OSError_ReleaseResource_ResID()						((ResourceType)		MK_ErrorGetParameter(1))
#define OSError_SetEvent_TaskID()							((TaskType)			MK_ErrorGetParameter(1))
#define OSError_SetEvent_Mask()								((EventMaskType)	MK_ErrorGetParameter(2))
#define OSError_ClearEvent_Mask()							((EventMaskType)	MK_ErrorGetParameter(1))
#define OSError_WaitEvent_Mask()							((EventMaskType)	MK_ErrorGetParameter(1))
#define OSError_GetEvent_TaskID()							((TaskType)			MK_ErrorGetParameter(1))
#define OSError_GetEvent_Event()							((EventMaskRefType)	MK_ErrorGetParameter(2))
#define OSError_GetAlarm_AlarmID()							((AlarmType)		MK_ErrorGetParameter(1))
#define OSError_GetAlarm_Tick()								((TickRefType)		MK_ErrorGetParameter(2))
#define OSError_GetAlarmBase_AlarmID()						((AlarmType)		MK_ErrorGetParameter(1))
#define OSError_GetAlarmBase_Info()							((AlarmBaseRefType)	MK_ErrorGetParameter(2))
#define OSError_SetRelAlarm_AlarmID()						((AlarmType)		MK_ErrorGetParameter(1))
#define OSError_SetRelAlarm_increment()						((TickType)			MK_ErrorGetParameter(2))
#define OSError_SetRelAlarm_cycle()							((TickType)			MK_ErrorGetParameter(3))
#define OSError_SetAbsAlarm_AlarmID()						((AlarmType)		MK_ErrorGetParameter(1))
#define OSError_SetAbsAlarm_start()							((TickType)			MK_ErrorGetParameter(2))
#define OSError_SetAbsAlarm_cycle()							((TickType)			MK_ErrorGetParameter(3))
#define OSError_CancelAlarm_AlarmID()						((AlarmType)		MK_ErrorGetParameter(1))
#define OSError_StartOS_Mode()								((AppModeType)		MK_ErrorGetParameter(1))
#define OSError_ShutdownOS_Error()							((StatusType)		MK_ErrorGetParameter(1))
#define OSError_IncrementCounter_CounterID()				((CounterType)		MK_ErrorGetParameter(1))
#define OSError_StartScheduleTableRel_ScheduleTableID()		((ScheduleTableType)MK_ErrorGetParameter(1))
#define OSError_StartScheduleTableRel_Offset()				((TickType)			MK_ErrorGetParameter(2))
#define OSError_StartScheduleTableAbs_ScheduleTableID()		((ScheduleTableType)MK_ErrorGetParameter(1))
#define OSError_StartScheduleTableAbs_Tickvalue()			((TickType)			MK_ErrorGetParameter(2))
#define OSError_StartScheduleTableSynchron_ScheduleTableID()	((ScheduleTableType)MK_ErrorGetParameter(1))
#define OSError_StopScheduleTable_ScheduleTableID()			((ScheduleTableType)MK_ErrorGetParameter(1))
#define OSError_SyncScheduleTable_SchedTableID()			((ScheduleTableType)MK_ErrorGetParameter(1))
#define OSError_SyncScheduleTable_GlobalTime()				((TickType)			MK_ErrorGetParameter(2))
#define OSError_SetScheduleTableAsync_ScheduleID()			((ScheduleTableType)MK_ErrorGetParameter(1))
#define OSError_GetCounterValue_CounterID()					((CounterType)		MK_ErrorGetParameter(1))
#define OSError_GetCounterValue_Value()						((TickRefType)		MK_ErrorGetParameter(2))
#define OSError_TerminateApplication_Application()			((ApplicationType)	MK_ErrorGetParameter(1))
#define OSError_TerminateApplication_RestartOption()		((RestartType)		MK_ErrorGetParameter(2))

/* Deviation MISRA-3 <START> */
/*      1234567890123456789012345678901 */
#define OSError_NextScheduleTable_SchedTableID_current()	((ScheduleTableType)MK_ErrorGetParameter(1))
#define OSError_NextScheduleTable_SchedTableID_next()		((ScheduleTableType)MK_ErrorGetParameter(2))
#define OSError_GetScheduleTableStatus_ScheduleID()			((ScheduleTableType)MK_ErrorGetParameter(1))
#define OSError_GetScheduleTableStatus_ScheduleStatus()		((ScheduleTableStatusRefType)MK_ErrorGetParameter(2))
#define OSError_GetElapsedCounterValue_CounterID()			((CounterType)		MK_ErrorGetParameter(1))
#define OSError_GetElapsedCounterValue_PreviousValue()		((TickRefType)		MK_ErrorGetParameter(2))
#define OSError_GetElapsedCounterValue_Value()				((TickRefType)		MK_ErrorGetParameter(3))
/* Deviation MISRA-3 <STOP> */

#endif

#endif

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
