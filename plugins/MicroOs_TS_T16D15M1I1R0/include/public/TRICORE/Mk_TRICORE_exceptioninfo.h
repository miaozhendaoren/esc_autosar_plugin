/* Mk_TRICORE_exceptioninfo.h - header file for published Tricore exception information
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_exceptioninfo.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_EXCEPTIONINFO_H
#define MK_TRICORE_EXCEPTIONINFO_H

#include <public/Mk_basic_types.h>

/* Tricore supports exception information.
*/
#define MK_HWHASEXCEPTIONINFO	1

/* MK_ExceptionClasssTinToType() converts an exception class/TIN compbination to a unique
 * enumeration in mk_exceptiontype_t
 *
 * !LINKSTO	Microkernel.TRICORE.ExceptionHandling.ExceptionInfo, 1
 * !doctype src
*/
#define MK_ExceptionClassTinToType(c,t)		((((mk_uint32_t)(c))<<4)|(t))

#ifndef MK_ASM
/* mk_exceptionclass_e enumerates all the different classes of exception on the Tricore processor family.
*/
enum mk_exceptionclass_e
{
	MK_exc_VirtualAddress = 0,
	MK_exc_InternalProtection = 1,
	MK_exc_InstructionError = 2,
	MK_exc_ContextManagement = 3,
	MK_exc_SystemBus = 4,
	MK_exc_Assertion = 5,
	MK_exc_Syscall = 6,
	MK_exc_NMI = 7,
};
#endif

#define MK_TIN_VirtualAddressFill					0
#define MK_TIN_VirtualAddressProtection				1

#define MK_TIN_PrivilegedInstruction				1
#define MK_TIN_MemoryProtectionRead					2
#define MK_TIN_MemoryProtectionWrite				3
#define MK_TIN_MemoryProtectionExecution			4
#define MK_TIN_MemoryProtectionPeripheralAccess		5
#define MK_TIN_MemoryProtectionNullAddress			6
#define MK_TIN_GlobalRegisterWriteProtection		7

#define MK_TIN_IllegalOpcode						1
#define MK_TIN_UnimplementedOpcode					2
#define MK_TIN_InvalidOperandspecification			3
#define MK_TIN_DataAddressAlignment					4
#define MK_TIN_InvalidLocalMemoryAddress			5

#define MK_TIN_FreeContextListDepletionFcxEqLcx		1
#define MK_TIN_CallDepthOverflow					2
#define MK_TIN_CallDepthUnderflow					3
#define MK_TIN_FreeContextListUnderflowFcxEq0		4
#define MK_TIN_ContextListUnderflowPcxEq0			5
#define MK_TIN_ContextTypePcxiUlWrong				6
#define MK_TIN_NestingErrorRfeNonZeroCallDepth		7

#define MK_TIN_ProgramFetchSynchronousError			1
#define MK_TIN_DataAccessSynchronousError			2
#define MK_TIN_DataAccessAsynchronousError			3
#define MK_TIN_CoprocessorTrapAsynchronousError		4
#define MK_TIN_ProgramMemoryIntegrityError			5
#define MK_TIN_DataMemoryIntegrityError				6
#define MK_TIN_TemporalAsynchronousError			7

#define MK_TIN_ArithmeticOverflow					1
#define MK_TIN_StickyArithmeticOverflow				2

#ifndef MK_ASM
/* mk_exceptiontype_e enumerates all the different types of exception on the Tricore processor family.
 *
 * This is intended to display the trap class and TIN combination symbolically in the debugger.
 * For use in software it is better to use the trap class and TIN identifications.
*/
enum mk_exceptiontype_e
{
	MK_ex_VirtualAddressFill				= 0x00,
	MK_ex_VirtualAddressProtection			= 0x01,

	MK_ex_PrivilegedInstruction				= 0x11,
	MK_ex_MemoryProtectionRead				= 0x12,
	MK_ex_MemoryProtectionWrite				= 0x13,
	MK_ex_MemoryProtectionExecution			= 0x14,
	MK_ex_MemoryProtectionPeripheralAccess	= 0x15,
	MK_ex_MemoryProtectionNullAddress		= 0x16,
	MK_ex_GlobalRegisterWriteProtection		= 0x17,

	MK_ex_IllegalOpcode						= 0x21,
	MK_ex_UnimplementedOpcode				= 0x22,
	MK_ex_InvalidOperandspecification		= 0x23,
	MK_ex_DataAddressAlignment				= 0x24,
	MK_ex_InvalidLocalMemoryAddress			= 0x25,

	MK_ex_FreeContextListDepletionFcxEqLcx	= 0x31,
	MK_ex_CallDepthOverflow					= 0x32,
	MK_ex_CallDepthUnderflow				= 0x33,
	MK_ex_FreeContextListUnderflowFcxEq0	= 0x34,
	MK_ex_CallStackUnderflowPcxEq0			= 0x35,
	MK_ex_ContextTypePcxiUlWrong			= 0x36,
	MK_ex_NestingErrorRfeNonZeroCallDepth	= 0x37,

	MK_ex_ProgramFetchSynchronousError		= 0x41,
	MK_ex_DataAccessSynchronousError		= 0x42,
	MK_ex_DataAccessAsynchronousError		= 0x43,
	MK_ex_CoprocessorTrapAsynchronousError	= 0x44,
	MK_ex_ProgramMemoryIntegrityError		= 0x45,
	MK_ex_DataMemoryIntegrityError			= 0x46,
	MK_ex_TemporalAsynchronousError			= 0x47,

	MK_ex_ArithmeticOverflow				= 0x51,
	MK_ex_StickyArithmeticOverflow			= 0x52,

	MK_ex_SystemCall						= 0x60,		/* For completeness; never reported */
	MK_ex_NonMaskableInterrupt				= 0x70,		/* upcoming versions may be split this to be more detailed */

	MK_ex_UnknownClassTinCombination		= 0xff
};

typedef enum mk_exceptionclass_e mk_exceptionclass_t;
typedef mk_uint32_t mk_exceptiontin_t;
typedef enum mk_exceptiontype_e mk_exceptiontype_t;

/* mk_tricoreexceptioninfo_s - exception info structure
 *
 * Note: tricore prefix is used because it is a tricore implementation of mk_hwexceptioninfo_t
 *
 * excClass and excTin are defined by Infineon:
 * - excClass is the class (the vector number)
 * - excTin is the Trap Identification Number (TIN) that is provided in D15.
 *
 * type is constructed by combining the class and TIN to give a unique number.
 * TBD more registers?
*/
typedef struct mk_tricoreexceptioninfo_s mk_hwexceptioninfo_t;

/*
 * !LINKSTO	Microkernel.TRICORE.ExceptionHandling.ExceptionInfo, 1
 * !doctype src
*/
struct mk_tricoreexceptioninfo_s
{
	/* Hardware-independent fields */
	mk_exceptiontype_t type;		/* Exception type (Unique: combines class and TIN) */
	/* Hardware-dependent fields */
	mk_exceptionclass_t excClass;	/* Exception class (Identifies vector) */
	mk_exceptiontin_t excTin;		/* Exception detail (TIN from D15) */
};
#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
