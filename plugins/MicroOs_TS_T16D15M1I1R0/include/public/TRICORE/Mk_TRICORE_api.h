/* Mk_TRICORE_api.h - processor-specific API for TRICORE processors
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_api.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_API_H
#define MK_TRICORE_API_H

/* There is no TRICORE-specific API.
*/

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
