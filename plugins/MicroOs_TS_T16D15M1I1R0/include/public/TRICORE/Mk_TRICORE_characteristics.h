/* Mk_TRICORE_characteristics.h
 *
 * This file defines the processor characteristics for Tricore
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_characteristics.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_CHARACTERISTICS_H
#define MK_TRICORE_CHARACTERISTICS_H

#include <public/TRICORE/Mk_TRICORE_cpu_characteristics.h>

#define MK_HWWORDLENGTH		32
#define MK_HWENDIAN			MK_LITTLEENDIAN
#define MK_HWSTACKGROWS		MK_STACKGROWSDOWN

#define MK_HWNEGDIV			MK_NEGDIV_ROUNDTOWARDZERO

/* TRICORE implementation does not have microkernel support for read-modify-write transactions.
*/
#define MK_HW_HAS_TRANSACTION_SUPPORT	0


/* All TRICORE processors so far have a 56-bit timer (STM) that is sufficient for the timestamp.
*/
#define MK_HW_HAS_TIMESTAMP	1

/* TRICORE doesn't implement any tickers
*/
#define MK_HWN_TICKER_CHANNELS	0

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
