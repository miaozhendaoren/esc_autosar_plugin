/* Mk_TRICORE_defs.h - TRICORE definitions for derivative selection etc.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_defs.h 17613 2014-10-31 12:14:36Z thdr9337 $
*/
#ifndef MK_TRICORE_DEFS_H
#define MK_TRICORE_DEFS_H

/* CPUs. MK_CPU must be one of these.
 *
 * CAVEAT:
 * The existence of a TRICORE derivative in this list does not imply a
 * commitment or intention to support the derivative.
*/
#define MK_TC277				0x01
#define MK_TC1798				0x02
#define MK_TC23XL				0x03
#define MK_TC22XL				0x04

/* MK_TRICORE_CORE is set in the derivative-specific header.
 * It can take one of the following values.
 *
 * CAVEAT:
 * The existence of a core in the list below does not imply a
 * commitment or intention to support this core.
*/
#define MK_TRICORE_TC131		0x01
#define MK_TRICORE_TC16			0x02
#define MK_TRICORE_TC161		0x03 /* i.e., TC 1.6P & TC1.6E */

/* MK_TRICORE_SCU is set in the derivative-specific header.
 * It can take one of the following values.
*/
#define MK_SCU_TC1798			0x01
#define MK_SCU_TC2XX			0x02

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
