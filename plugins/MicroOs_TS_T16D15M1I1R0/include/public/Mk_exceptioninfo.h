/* Mk_exceptioninfo.h
 *
 * This file includes the processor-specific exception-info file and then declares the
 * exception-info structure.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_exceptioninfo.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_EXCEPTIONINFO_H
#define MK_EXCEPTIONINFO_H

#include <public/Mk_hwsel.h>
#include MK_HWSEL_PUB_CHARACTERISTICS
#include MK_HWSEL_PUB_EXCEPTIONINFO

#ifndef MK_ASM
#if MK_HWHASEXCEPTIONINFO
/* The constant MK_exceptionInfo is either NULL or is a reference to an mk_hwexceptioninfo_t structure in RAM.
 * The indirection permits the feature to be disabled if RAM is in short supply.
*/
extern mk_hwexceptioninfo_t * const MK_exceptionInfo;
#endif
#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
