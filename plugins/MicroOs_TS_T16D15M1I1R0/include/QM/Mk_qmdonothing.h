/* Mk_qmdonothing.h
 *
 * This header file defines a prototype for the QM marker function MK_QmDoNothing().
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_qmdonothing.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_QMDONOTHING_H
#define MK_QMDONOTHING_H

#ifndef MK_ASM
void MK_QmDoNothing(void);
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
