/* MemMapAsm.h
 *
 * This is a dummy file that is used to satisfy the file inclusion in the CGC style checker.
 * It cannot be found in the normal search path for header files.
 *
 * If the customer's application is using the assembler MemMap features, a special version of this file
 * must be created and placed somewhere in the application's search path.
 *
 * Warning: This file has not been developed in accordance with a safety standard (no ASIL)!
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: MemMapAsm.h 9177 2013-04-02 09:05:30Z mist8519 $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_STYLE_013]
 *  In the EB tresos Safety OS project, all source code file names shall start with a
 *  file name prefix as defined in Appendix A, Prefixes and Definitions.
 *
 * Reason:
 *  The file MemMapAsm.h is normally provided by the user. This file is a dummy that is
 *  present to satisfy the file inclusion in the style checker. Inclusion of MemMapAsm.h
 *  is controlled by conditional compilation that is only enabled when the user decides to
 *  use the feature. If this file gets included accidentally, the result is a compile-time error.
*/
/* Deviation DCG-1 <*> */

#ifndef MEMMAPASM_H
#define MEMMAPASM_H

#error "This dummy version of MemMapAsm.h must not be used. Check your Makefiles!"

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
