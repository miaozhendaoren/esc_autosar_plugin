/* Mk_tool_ticgt.h - ticgt toolchain abstraction.
 *
 * This file provides the toolchain abstraction for the TI Code Generation Tools.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_tool_ticgt.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_TOOL_TICGT_H
#define MK_TOOL_TICGT_H

/* MK_PARAM_UNUSED(p) - mark parameter p as unused parameter (or variable).
 * Usage: Function-like.
 */
#define MK_PARAM_UNUSED(p) ((void)(p))

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
