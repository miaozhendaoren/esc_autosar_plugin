/* Mk_resource.h - resource handling
 *
 * This file contains definitions for configuration and handling of resources.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_resource.h 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 5.4 (required)
 *  A tag name shall be a unique identifier.
 *
 * Reason:
 *  False positive because the struct is used in a forward type definition.
 *
 * MISRA-2) Deviated Rule: 8.12 (required)
 *  When an array is declared with external linkage, its size shall be stated
 *  explicitly or defined implicitly by initialisation.
 *
 * Reason:
 *  The size of these arrays is configuration dependent and the configuration
 *  shall not be included in private microkernel headers. The arrays are
 *  guaranteed to by only accessed by the microkernel up to the limit defined
 *  by MK_nResources.
*/

#ifndef MK_RESOURCE_H
#define MK_RESOURCE_H

#include <public/Mk_public_types.h>
#include <private/Mk_interrupt.h>	/* for mk_hwlocklevel_t */
#include <private/Mk_thread.h>
#include <private/Mk_types_forward.h>

#ifndef MK_ASM

typedef struct mk_resourcecfg_s mk_resourcecfg_t;

/* Deviation MISRA-1 */
struct mk_resource_s
{
	mk_threadprio_t ceilingPriority;
	mk_hwlocklevel_t lockLevel;
	mk_int_fast16_t maxCount;
	mk_int_fast16_t count;
	mk_threadprio_t savedPrio;
	mk_hwlocklevel_t savedLevel;
	mk_resource_t *previousTaken;
	mk_thread_t *owner;
};

struct mk_resourcecfg_s
{
	mk_threadprio_t ceilingPriority;
	mk_hwlocklevel_t lockLevel;
	mk_int_fast16_t maxCount;
};

/* MK_RESOURCECFG() - intialization macro for a resource config structure
 *
 * This macro should be used wherever a resourcecfg is initialized, to permit changes
 * in the structure to be easily identified and fixed, and to permit
 * optimizations of the structure if necessary.
*/
#define MK_RESOURCECFG(cpri, lvl, maxc) \
{				\
	(cpri),		\
	(lvl),		\
	(maxc)		\
}

extern const mk_objquantity_t MK_nResources;
/* Deviation MISRA-2 <+2> */
extern const mk_resourcecfg_t MK_resourceCfg[];
extern mk_resource_t MK_resource[];

void MK_InitResources(void);
void MK_ClearResourceList(mk_resource_t *);

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
