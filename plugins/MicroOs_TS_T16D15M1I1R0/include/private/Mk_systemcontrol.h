/* Mk_systemcontrol.h
 *
 * This file contains definitions for the microkernel's system-control.
 * This includes startup and shutdown hooks.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_systemcontrol.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_SYSTEMCONTROL_H
#define MK_SYSTEMCONTROL_H

#include <private/Mk_thread.h>

#ifndef MK_ASM

/* Constant pointer to the thread to use for the shutdown hook.
 * The same thread can be used for both startup and shutdown hook because only one of them will run
 * at the same time.
 * QM-OS calls are not permitted from the startup and shutdown hooks, so the threads will run
 * at high priority. This means that the high-priority OS thread could be used at a pinch.
*/
extern mk_thread_t * const MK_shutdownHookThread;

/* Configuration structures for running the startup- and shutdown-hooks in a thread.
 * There might be no hooks configured, in which case these pointers might be NULL.
 * There is *always* a MK_shutdownThreadConfig!
*/
extern const mk_threadcfg_t * const MK_startupHookThreadConfig;
extern const mk_threadcfg_t * const MK_shutdownHookThreadConfig;
extern const mk_threadcfg_t MK_shutdownThreadConfig;

#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
