/* Mk_TRICORE_wdt.h - Tricore WDT module description
 *
 * This file includes the header file that describes the WDT variant used on the
 * particular Tricore microcontroller that is selected.
 *
 * The WDT (watchdog timer) is part of the SCU (system control unit) hardware peripheral.
 * The WDT is used by the microkernel for the purpose of turning the ENDINIT flag on and off.
 *
 * The name given here to each WDT variant is merely the first microcontroller on which the variant
 * was encountered. The name is not specified by Infineon.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_wdt.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_WDT_H
#define MK_TRICORE_WDT_H

#include <public/TRICORE/Mk_TRICORE_cpu_characteristics.h>
#include <public/Mk_public_types.h>

#if MK_TRICORE_SCU == MK_SCU_TC1798
#include <private/TRICORE/Mk_TRICORE_wdt_tc1xxx.h>
#elif MK_TRICORE_SCU == MK_SCU_TC2XX
#include <private/TRICORE/Mk_TRICORE_wdt_tc2xx.h>
#else
#error "Unknown or unsupported SCU variant. Check your CPU characteristics file!"
#endif

#ifndef MK_ASM
void MK_WriteEndinit(mk_uint32_t);
#endif

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
