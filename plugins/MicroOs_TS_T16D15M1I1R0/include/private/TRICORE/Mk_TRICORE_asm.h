/* Mk_TRICORE_asm.h
 *
 * This file includes the appropriate Mk_TRICORE_asm_xxx.h include file depending on the chosen toolchain.
 *
 * Macros are defined that permit the same source files to be processed by the assemblers of
 * all toolchains. This is done by including the appropriate toolchain file.
 * The assembler memory-map file is also included if appropriate.
 *
 * This file should be included at the very top of all assembly-language files. It must NOT be included
 * by files of any language other than assembler.
 *
 * The Makefiles must ensure that the MK_TOOL macro is defined appropriately on the command line.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_asm.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_ASM_H
#define MK_TRICORE_ASM_H

#ifndef MK_ASM
#error "MK_ASM is not defined. This file is not intended to be used in C files."
#endif

#include <public/Mk_defs.h>

/* Include the appropriate abstraction file for the toolchain
*/
#if (MK_TOOL==MK_tasking)
#include <private/TRICORE/Mk_TRICORE_asm_tasking.h>
#elif (MK_TOOL==MK_gnu)
#include <private/TRICORE/Mk_TRICORE_asm_gnu.h>
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Text section alignment
 *
 * This could be already defined by the user's memmap, so we only define it if it isn't already defined.
*/
#ifndef MK_ASM_ALIGN_TEXT
#define MK_ASM_ALIGN_TEXT	MK_align(16,4)
#endif

/* Vector table alignment
 *
 * These could be already defined by the user's memmap, so we only define them if they aren't already defined.
 *
 * Individual entry alignment applies to both the interrupt and exception vectors.
 * The exception vector table as a whole must be aligned at 256 bytes.
 * The alignment of the interrupt vector table is more complicated and cannot be defined here.
*/
#ifndef MK_ASM_ALIGN_VECTORTABLEENTRY
#define MK_ASM_ALIGN_VECTORTABLEENTRY		MK_align(32,8)
#endif

#ifndef MK_ASM_ALIGN_EXCVECTORTABLE
#define MK_ASM_ALIGN_EXCVECTORTABLE	        MK_align(256,4)
#endif


#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
