/* Mk_TRICORE_ticker.h - ticker definitions for TRICORE
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_ticker.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_TICKER_H
#define MK_TRICORE_TICKER_H

/* TRICORE doesn't implement any tickers.
*/

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
