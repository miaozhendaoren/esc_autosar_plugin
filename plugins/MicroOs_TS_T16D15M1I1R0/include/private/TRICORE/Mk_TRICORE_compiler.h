/* Mk_TRICORE_compiler.h
 *
 * This file includes the appropriate Mk_TRICORE_compiler_xxx.h include file depending on the chosen toolchain.
 *
 * Macros are defined that permit the same source files to be processed by the compilers of
 * all toolchains when using features such as in-line assembly language.
 *
 * The Makefiles must ensure that the MK_TOOL macro is defined appropriately on the command line.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_compiler.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_COMPILER_H
#define MK_TRICORE_COMPILER_H

#include <public/Mk_defs.h>

/* Include the appropriate abstraction file for the toolchain
*/
#if (MK_TOOL==MK_tasking)
#include <private/TRICORE/Mk_TRICORE_compiler_tasking.h>
#elif (MK_TOOL==MK_gnu)
#include <private/TRICORE/Mk_TRICORE_compiler_gnu.h>
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

#endif

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
