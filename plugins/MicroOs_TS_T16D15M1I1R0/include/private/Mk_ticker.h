/* Mk_ticker.h - ticker timer
 *
 * As the ticker timer is a functionality that is not implemented on all CPU families,
 * this file only includes a hardware-specific header file.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_ticker.h 15821 2014-04-16 04:55:15Z masa8317 $
*/

#ifndef MK_TICKER_H
#define MK_TICKER_H

#include <public/Mk_hwsel.h>

#include MK_HWSEL_PRV_TICKER

#ifndef MK_HWN_TICKER_CHANNELS
#error "Architecture has not defined MK_HWN_TICKER_CHANNELS"
#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
