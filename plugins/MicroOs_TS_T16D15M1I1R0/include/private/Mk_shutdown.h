/* Mk_shutdown.h
 *
 * This file contains the declaration of the shutdown function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_shutdown.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_SHUTDOWN_H
#define MK_SHUTDOWN_H

#include <public/Mk_error.h>

/* Hardware-independent shutdown function
*/
void MK_Shutdown(mk_osekerror_t);

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
