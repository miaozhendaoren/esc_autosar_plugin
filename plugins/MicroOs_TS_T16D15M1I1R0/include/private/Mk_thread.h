/* Mk_thread.h - thread configuration and handling
 *
 * This file contains definitions for the hardware-indenpendent part of
 * configuration and handling of threads. It also includes the hardware-specific
 * part.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_thread.h 15851 2014-04-17 10:21:10Z dh $
*/
#ifndef MK_THREAD_H
#define MK_THREAD_H

#include <public/Mk_hwsel.h>
#include <public/Mk_public_types.h>
#include <public/Mk_error.h>

#include MK_HWSEL_PRV_THREAD

#include <private/Mk_accounting.h>
#include <private/Mk_types_forward.h>
#include <private/Mk_event.h>

/* Object IDs for MK_OBJTYPE_KERNEL
*/
#define MK_OBJID_IDLE			0
#define MK_OBJID_MAIN			1
#define MK_OBJID_BOOT			2
#define MK_OBJID_SHUTDOWN		3
#define MK_OBJID_SELFTEST		4

/* Object ID for global hook functions
*/
#define MK_OBJID_GLOBAL			(-1)

#ifndef MK_ASM

/* mk_threadregisters_t is defined for the generator
*/
typedef mk_hwthreadregisters_t mk_threadregisters_t;

typedef struct mk_threadcfg_s mk_threadcfg_t;

typedef mk_int_fast16_t mk_threadprio_t;
typedef void (*mk_threadfunc_t)(void);

/* Thread states. Note: there is no WAITING state. WAITING is a task state,
 * and while a task is WAITING it does not occupy its thread.
*/
enum mk_threadstate_e
{	MK_THS_IDLE = 0,
	MK_THS_READY = 1,
	MK_THS_RUNNING = 2,
	MK_THS_NEW = 3				/* Used for new threads to avoid reporting them as current */
};

typedef enum mk_threadstate_e mk_threadstate_t;

/* mk_thread_s - the thread structure
 *
 * See hardware-specific definition of mk_hwthreadregisters_t for
 * - interrupt level
 * - entry point
 * - parameters
*/
struct mk_thread_s
{
	mk_threadregisters_t *regs;			/* Pointer to the register context */
	const mk_char_t *name;				/* Name of thread (makes debugging easier) */
	mk_thread_t *next;					/* Next thread in queue */
	mk_thread_t *parent;				/* Parent thread (for OS threads etc.) */
	mk_accounting_t accounting;			/* Thread accounting structure */
	mk_threadstate_t state;				/* State of thread (idle, ready, running) */
	mk_threadprio_t queueingPriority;	/* Initial priority of thread */
	mk_threadprio_t runningPriority;	/* Lowest priority of thread when in running state */
	mk_threadprio_t currentPriority;	/* Current thread priority */
	mk_resource_t *lastResourceTaken;	/* Most recently taken resource */
	mk_jobqueue_t *jobQueue;			/* Job queue (could be NULL) */
	mk_eventstatus_t *eventStatus;		/* Event status for extended tasks that called WaitGetClearEvent */
	mk_objectid_t memoryPartition;		/* Index of memory partition */
	mk_objectid_t currentObject;		/* Id of thread's current job */
	mk_objecttype_t objectType;			/* Type of object this thread runs */
	mk_objectid_t applicationId;		/* Id of the thread's OS-Application */
};


/* mk_threadcfg_s - the thread configuration structure
 *
 * !LINKSTO     Microkernel.Memory.Config.General.ThreadRef, 1
 * !doctype     src
*/
struct mk_threadcfg_s
{
	mk_threadregisters_t *regs;			/* Pointer to the register context */
	const mk_char_t *name;				/* Name of thread (makes debugging easier) */
	mk_stackelement_t *initialSp;		/* Initial stack pointer */
	mk_threadfunc_t entry;				/* Thread entry function */
	mk_accountingcfg_t acctCfg;			/* Configuration for accounting (exec budget etc.) */
	mk_hwps_t ps;						/* Initial processor status (processor mode, interrupt status, level etc.) */
	mk_threadprio_t queuePrio;			/* Initial queueing priority */
	mk_threadprio_t runningPriority;	/* Initial run priority */
	mk_objectid_t memoryPartition;		/* Index of memory partition */
	mk_objectid_t currentObject;		/* Id of this thread */
	mk_objecttype_t objectType;			/* Type of object this thread represents */
	mk_objectid_t applicationId;		/* Id of the thread's OS-Application */
};

/* MK_THREADCFG() - intialization macro for a thread config structure
 *
 * This macro should be used wherever a threadcfg is initialized, to permit changes
 * in the structure to be easily identified and fixed, and to permit
 * optimizations of the structure if necessary.
 *
 * The entry function is cast to a void (*)(void) function pointer; the entry function can theoretically be
 * anything as long as the thread creation places parameters in the right place.
 */
#define MK_THREADCFG(reg, name, sp, ent, pm, ilvl, ie, fpu, hws, qpri, rpri, partIdx,	\
					 objid, objtyp, exBgt, lkBgt, aid)									\
{																						\
	(reg),																				\
	(name),																				\
	(sp),																				\
	(mk_threadfunc_t)(ent),																\
	{ (exBgt), (lkBgt) },																\
	MK_HWPS_INIT((pm), (ilvl), (ie), (fpu), (hws)),										\
	(qpri),																				\
	(rpri),																				\
	(partIdx),																			\
	(objid),																			\
	(objtyp),																			\
	(aid)																				\
}

extern const mk_objquantity_t MK_nApps;
extern const mk_objectid_t * const MK_appRestartTasks;
extern mk_appstate_t * const MK_appStates;
#endif

/* Offsets of items in thread data structure, for assembly-language code
 * We shouldn't need many of these...
*/
#define MK_THR_REGS		0

#ifndef MK_ASM

/* Global variables pointing to current thread and thread queue
*/
extern mk_thread_t *MK_threadCurrent;
extern mk_thread_t *MK_threadQueueHead;

/* Global thread structures
*/
extern mk_thread_t MK_idleThread;
extern mk_thread_t MK_osThreadLow;
extern mk_thread_t MK_osThreadHigh;
extern mk_thread_t MK_tfThreadLow;
extern mk_thread_t MK_tfThreadHigh;

/* Global constants pointing to thread configurations
*/
extern const mk_threadcfg_t MK_bootThreadConfig;
extern const mk_threadcfg_t MK_idleThreadConfig;
extern const mk_threadcfg_t MK_initThreadConfig;
extern const mk_threadcfg_t MK_osLowThreadConfig;
extern const mk_threadcfg_t MK_osHighThreadConfig;
extern const mk_threadcfg_t MK_tfLowThreadConfig;
extern const mk_threadcfg_t MK_tfHighThreadConfig;
extern const mk_threadcfg_t MK_selftestThreadConfig;

/* Function prototypes for thread (/-queue) handling
*/
void MK_InitApplications(void);
void MK_Dispatch(void);
void MK_Idle(void);
void MK_EnqueueThread(mk_thread_t *);
void MK_RequeueThread(mk_thread_t *);
void MK_TerminateThread(mk_thread_t *);
void MK_RestartThread(mk_thread_t *, const mk_threadcfg_t *);
void MK_StartThread(mk_thread_t *, const mk_threadcfg_t *);
void MK_FillThreadRegisters(const mk_threadcfg_t *);
mk_thread_t *MK_FindFirstThread(mk_objecttype_t, mk_threadstate_t);
void MK_ThreadQueueEmpty(void);
mk_objectid_t MK_GetApplicationId(void);
void MK_RestartApplication(mk_objectid_t);
void MK_TerminateApplication(mk_objectid_t, mk_boolean_t);

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
