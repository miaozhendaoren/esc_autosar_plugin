/* Mk_jobqueue.h - job queue configuration and handling
 *
 * This file contains defintions for configuration and handling of job queues.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_jobqueue.h 15912 2014-04-24 11:55:06Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 5.4 (required)
 *  A tag name shall be a unique identifier.
 *
 * Reason:
 *  False positive because the struct is used in a forward type definition.
*/

#ifndef MK_JOBQUEUE_H
#define MK_JOBQUEUE_H

#include <public/Mk_basic_types.h>
#include <public/Mk_public_types.h>
#include <private/Mk_types_forward.h>

#ifndef MK_ASM

typedef struct mk_jobqueuecfg_s mk_jobqueuecfg_t;
typedef mk_objectid_t mk_jobid_t;

#define	MK_NOJOB	(-1)	/* Return from MK_JqRemove() if the queue is empty */

struct mk_jobqueuecfg_s
{
	mk_thread_t *thread;	/* Thread that's associated with this job queue */
	mk_jobid_t *base;		/* Address of array used for the ring buffer */
	mk_uint32_t length;		/* Length of the array */
};

/* Deviation MISRA-1 */
struct mk_jobqueue_s
{
	mk_jobid_t *base;		/* Base (copied from cfg on initialization) */
	mk_jobid_t *limit;		/* &base[length], set from cfg on initialization */
	mk_jobid_t *next;		/* The next job in the queue */
	mk_jobid_t *free;		/* The next free slot in the queue */
	mk_uint32_t count;		/* No. of elements in the queue */
	mk_uint32_t length;		/* Only needed for assertion in MK_JqAppend() */
};

/* MK_JOBQUEUECFG() - initialization macro for a job-queue config structure
 *
 * This macro should be used wherever a jobqueuecfg is initialized, to permit changes
 * in the structure to be easily identified and fixed, and to permit
 * optimizations of the structure if necessary.
*/
#define MK_JOBQUEUECFG(thr, base, len) \
{			\
	(thr),	\
	(base),	\
	(len)	\
}

extern const mk_objquantity_t MK_nJobQueues;
extern const mk_jobqueuecfg_t * const MK_jobQueueCfg;
extern mk_jobqueue_t * const MK_jobQueue;

mk_jobid_t MK_JqRemove(mk_jobqueue_t *);
void MK_JqRemoveAllTasksOfApp(mk_jobqueue_t *, mk_objectid_t);
void MK_JqAppend(mk_jobqueue_t *, mk_jobid_t);
void MK_JqInit(void);

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
