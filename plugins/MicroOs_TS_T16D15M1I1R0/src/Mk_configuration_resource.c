/* Mk_configuration_resource.c
 *
 * This file contains the configuration for resources.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_configuration_resource.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <private/Mk_resource.h>
#include <Mk_Cfg.h>

/* The Resource table
 *
 * This is the table of all external resources. i.e. those that can be acquired by calling
 * GetResource(). This includes linked resources, RES_SCHEDULER and the two interrupt-locking
 * resources. Internal resources do not appear in the table.
 *
 * Note: it assumed that there is at least one resource (an interrupt-locking resource),
 * so no provision is made for an empty resource table.
*/
const mk_int_t MK_nResources = MK_NRESOURCES;
const mk_resourcecfg_t MK_resourceCfg[MK_NRESOURCES] = { MK_RESOURCECONFIG };

mk_resource_t MK_resource[MK_NRESOURCES];

const mk_objectid_t MK_resLockCat1 = MK_RESCAT1;
const mk_objectid_t MK_resLockCat2 = MK_RESCAT2;


/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
