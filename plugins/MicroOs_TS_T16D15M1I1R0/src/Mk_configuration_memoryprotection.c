/* Mk_configuration_memoryprotection.c
 *
 * This file contains the configuration for memory protection.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_configuration_memoryprotection.c 15879 2014-04-22 07:39:26Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_memoryprotection.h>
#include <Mk_Cfg.h>

/* !LINKSTO         Microkernel.Memory.Config.General.Tables,1
 * !description     Define the memory protection configuration needed tables.
 * !                Tables are initialized using macros declared in generated
 * !                file which are defined using appropriate entry macros.
 * !                Tables are defined as const.
 * !doctype src
*/

/* Memory regions and region-to-partition mappings.
 *
 * The region table might exist even if the partition table doesn't exist, because
 * of the need to initialize the data/bss sections. This file assumes that the
 * region map table is not empty if the region table is not empty.
 *
 * For the region table table, if the base pointer MK_memoryRegions is NULL the size
 * MK_nMemoryRegions is zero. This fact is used in the code.
*/
const mk_objquantity_t MK_nMemoryRegions = MK_NMEMORYREGIONS;

#if MK_NMEMORYREGIONS > 0

static const mk_memoryregion_t MK_memRegion[MK_NMEMORYREGIONS] = { MK_MEMORYREGIONCONFIG };

const mk_memoryregion_t * const MK_memoryRegions = MK_memRegion;

#else

const mk_memoryregion_t * const MK_memoryRegions = MK_NULL;

#endif

/* Memory partitions
 *
 * The region table might exist even if the partition table doesn't exist, because
 * of the need to initialize the data/bss sections.
 *
 * If the base pointer MK_memoryPartitions is NULL the size MK_nMemoryPartitions is zero.
 * This fact is used in the code.
*/
const mk_objquantity_t MK_nMemoryPartitions = MK_NMEMORYPARTITIONS;

#if MK_NMEMORYPARTITIONS > 0

static const mk_memoryregionmap_t MK_memRegionMap[MK_NMEMORYREGIONMAPS] = { MK_MEMORYREGIONMAPCONFIG };
static const mk_memorypartition_t MK_memPart[MK_NMEMORYPARTITIONS] = { MK_MEMORYPARTITIONCONFIG };

const mk_memorypartition_t * const MK_memoryPartitions = MK_memPart;

#if MK_NMEMORYREGIONS <= 0
#error "Memory partitions are configured but no memory regions. Check your configuration!"
#endif

#else

const mk_memorypartition_t * const MK_memoryPartitions = MK_NULL;

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
