/* Mk_configuration_foreign.c
 *
 * This file contains the configuration for trusted functions.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_configuration_foreign.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_foreign.h>
#include <Mk_Cfg.h>

/* number of trusted functions configured by the user
*/
const mk_objquantity_t MK_nTrustedFunctions   = MK_NTRUSTEDFUNCTIONS;

/* array of trusted functions defined by the user
*/
#if MK_NTRUSTEDFUNCTIONS > 0
static const mk_trustedfunction_t MK_trustedFunctionsTable[MK_NTRUSTEDFUNCTIONS] = { MK_TRUSTEDFUNCTION_CONFIG };
const mk_trustedfunction_t * const MK_trustedFunctionsPtr = MK_trustedFunctionsTable;
#else
const mk_trustedfunction_t * const MK_trustedFunctionsPtr = MK_NULL;
#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
