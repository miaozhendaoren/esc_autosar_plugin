/* Mk_TRICORE_hwsetdynamicmemorypartition_tc161.c
 *
 * This file contains the function MK_HwSetDynamicMemoryPartition() for Tricore processors with
 * TC1.6E/P core architectures.
 * The function programs a dynamic memory partition into the hardware.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_hwsetdynamicmemorypartition_tc161.c 18690 2015-03-05 12:13:57Z nibo2437 $
*/
#include <public/Mk_basic_types.h>
#include <public/Mk_public_types.h>
#include <private/Mk_memoryprotection.h>
#include <private/TRICORE/Mk_TRICORE_mpu.h>
#include <Mk_Cfg.h>

static mk_objectid_t MK_lastDynamicPartition = -1;

/* MK_HwSetDynamicMemoryPartition() - programs the dynamic memory partition into the hardware.
 *
 * This version assumes that the dynamic partitions only contain data regions. The code protection
 * registers are not modified. Any execute-access permissions in the regions will be ignored.
 *
 * This function selects the correct entry from the MPU cache. The actual programming will take
 * place in the assembler function that is called at the end. The assembler function will
 * re-programm all available dynamic regions.
 *
 * This function is empty when the configuration excludes dynamic MPU switching.
 *
 * !LINKSTO Microkernel.TRICORE.MPU.GlobalCodeProtection, 1,
 * !        Microkernel.TRICORE.Function.MK_HwSetDynamicMemoryPartition, 1
 * !doctype src
*/
void MK_HwSetDynamicMemoryPartition(mk_objectid_t partId)
{
#if (MK_DYNREGIONS_MAX > 0) && (MK_NMEMORYPARTITIONS > MK_NSTATICPARTITIONS)
	const mk_mpurwpermission_t *permissions;
	const mk_mpubounds_t *boundaryPairs;

	if ( partId != MK_lastDynamicPartition )
	{
		if ( partId <= MK_MEMPART_KERNEL ) 
		{
			MK_lastDynamicPartition = partId;

			boundaryPairs = &(MK_MpuRegCache[0]); /* doesn't matter */
			permissions = &MK_MemoryPartitionPermissions[0]; /* only global regions */

			MK_WriteDynamicMpuRegisters(boundaryPairs, permissions);
		}
		else if ( partId >= MK_NSTATICPARTITIONS )
		{
			MK_lastDynamicPartition = partId;

			boundaryPairs = &(MK_MpuRegCache[(partId-MK_NSTATICPARTITIONS)*(MK_DYNREGIONS_MAX)]);
			permissions = &MK_MemoryPartitionPermissions[partId];

			MK_WriteDynamicMpuRegisters(boundaryPairs, permissions);
		}
		else
		{
			/* Static partition --> nothing to do. */
		}
	}
#endif /* (MK_TRICORE_NDPR-MK_MEMPART_GLOBAL_SIZE) > 0 */
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
