<?xml version="1.0" encoding="UTF-8"?>

<stylesheet version="2.0" xmlns="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:my="http://www.elektrobit.com/2013/xgen/local" xmlns:x="http://www.elektrobit.com/2013/xgen" xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:fn="http://www.w3.org/2005/xpath-functions">

	<import href="resource://details/generic-text.xsl" />

	<variable name="include-guard" select="my:get-include-guard-name('platform-types-h')" />
	<variable name="my:artifact" select="my:get-artifact-of-type('platform-types-h', $my:module-artifacts)" />
	<variable name="misra-top-comment">
		<text>/*
 * MISRA-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 5.1 (required)
 *   Identifiers (internal and external) shall not rely on the significance of
 *   more then 31 characters.
 *
 *   Reason:
 *   The AUTOSAR specification requires the definition of the macros
 *   with overlong names.
 *
 * MISRA-2) Deviated Rule: 1.4 (required)
 *   The compiler/linker shall be checked to ensure that 31 character
 *   significance and case sensitivity are supported for external identifiers.
 *
 *   Reason:
 *   The AUTOSAR specification requires the definition of the macros
 *   with overlong names.
 *
 * MISRA-3) Deviated Rule: 6.3 (required)
 *   Typedefs that indicate size and signedness should be used in place
 *   of the basic types.
 *
 *   Reason:
 *   Such types need to be defined somewhere and this file provides the
 *   AUTOSAR type definitions with fixed size and signedness.
 */</text>
	</variable>

	<variable name="file-description-comment">
		<text> /* !LINKSTO PLATFORM001,1,
    !        PLATFORM003,1
    !desc Header file Platform_Types.h containing platform types
  */</text>
	</variable>

	<template name="start-file">
		<call-template name="top-comment" />
		<text>#if !(defined </text>
		<value-of select="$include-guard" />
		<text>)</text>
		<value-of select="$my:NL" />
		<text>#define </text>
		<value-of select="$include-guard" />
		<value-of select="$my:NL" />
		<value-of select="$my:NL" />
		<value-of select="my:normalize-end-of-line($misra-top-comment)" />
		<value-of select="$my:NL" />
		<value-of select="$my:NL" />
		<value-of select="$my:NL" />
		<value-of select="my:normalize-end-of-line($file-description-comment)" />
		<value-of select="$my:NL" />
	</template>

	<template name="macros">
		<call-template name="section">
			<with-param name="text">
				<text>macros</text>
			</with-param>
		</call-template>

		<call-template name="subsection">
			<with-param name="text">
				<text>AUTOSAR vendor identification</text>
			</with-param>
		</call-template>

		<call-template name="macro">
			<with-param name="name">
				<text>PLATFORM_VENDOR_ID</text>
			</with-param>
			<with-param name="value" select="my:make-unsigned-value($pc-config/x:parameter[@name='platform-vendor-id']/x:integer-value)" />
			<with-param name="comment">
				<text>\brief AUTOSAR vendor identification: Elektrobit Automotive GmbH</text>
			</with-param>
		</call-template>

        <call-template name="subsection">
            <with-param name="text">
                <text>AUTOSAR module identification</text>
            </with-param>
        </call-template>

        <call-template name="macro">
            <with-param name="name">
                <text>PLATFORM_MODULE_ID</text>
            </with-param>
            <with-param name="value" select="my:make-unsigned-value($pc-config/x:parameter[@name='platform-module-id']/x:integer-value)" />
            <with-param name="comment">
                <text>\brief AUTOSAR vendor identification: Elektrobit Automotive GmbH</text>
            </with-param>
        </call-template>

		<!-- <call-template name="macro"> <with-param name="name"> <text></text> </with-param> <with-param name="value" select=""/> </call-template> -->
	</template>

	<template name="type-definitions">
		<call-template name="section">
			<with-param name="text">
				<text>type-definitions</text>
			</with-param>
		</call-template>

		<apply-templates select="$pc-config/x:type">
			<sort select="my:computeTypeWeight(.)" data-type="number" />
		</apply-templates>
	</template>

	<template match="x:type">
		<text>typedef </text>
		<apply-templates select="x:reference|x:basetype-ref" />
		<text> </text>
		<value-of select="@name" />
		<text>;</text>
		<value-of select="$my:NL" />
	</template>

	<template match="x:reference">
		<value-of select="@type" />
	</template>

	<template match="x:basetype-ref">
		<if test="exists(@signed)">
			<variable name="signed" as="xs:boolean" select="@signed" />
			<value-of select="if($signed) then 'signed' else 'unsigned'" />
			<text> </text>
		</if>
		<value-of select="@type" />
	</template>

	<template name="end-file">
		<text>#endif /* !(defined </text>
		<value-of select="$include-guard" />
		<text>) */</text>
		<value-of select="$my:NL" />
		<call-template name="section">
			<with-param name="text">
				<text>end of file</text>
			</with-param>
		</call-template>
	</template>

</stylesheet>