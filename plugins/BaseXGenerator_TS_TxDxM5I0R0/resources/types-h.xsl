<?xml version="1.0" encoding="UTF-8"?>

<stylesheet version="2.0" xmlns="http://www.w3.org/1999/XSL/Transform" xmlns:my="http://www.elektrobit.com/2013/xgen/local" xmlns:x="http://www.elektrobit.com/2013/xgen" xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:fn="http://www.w3.org/2005/xpath-functions">

	<import href="resource://details/generic-text.xsl" />

	<variable name="include-guard" select="my:get-include-guard-name('types-h')" />
    <variable name="my:artifact" select="my:get-artifact-of-type('types-h', $my:module-artifacts)"/>

	<template name="start-file">
		<call-template name="top-comment" />
		<text>#ifndef </text>
		<value-of select="$include-guard" />
		<value-of select="$my:NL" />
		<text>#define </text>
		<value-of select="$include-guard" />
		<value-of select="$my:NL" />
		<value-of select="$my:NL" />
		<value-of select="my:normalize-end-of-line($misra-h-top-comment)" />
		<value-of select="$my:NL" />
	</template>

	<template name="inclusions">
		<call-template name="section">
			<with-param name="text">
				<text>inclusions</text>
			</with-param>
		</call-template>

        <for-each select="my:get-included-file-names(
            $pc-config/x:type/x:reference
            | $pc-config/x:type/x:struct/x:member,
            $my:artifact,
            ())">
            <call-template name="sysinclude">
                <with-param name="filename" select="." />
            </call-template>
        </for-each>

		<call-template name="sysinclude">
			<with-param name="filename">
				<text>TSAutosar.h</text>
			</with-param>
		</call-template>
		<call-template name="unguarded-macro">
			<with-param name="name">
				<text>TS_RELOCATABLE_CFG_ENABLE</text>
			</with-param>
			<with-param name="value" select="my:on-off(my:get-pre-compile-flag('relocatable-enable'))" />
		</call-template>
		<call-template name="unguarded-macro">
			<with-param name="name">
				<text>TS_PB_CFG_PTR_CLASS</text>
			</with-param>
			<with-param name="value" select="fn:concat(fn:upper-case($my:module), '_APPL_CONST')" />
		</call-template>
		<call-template name="sysinclude">
			<with-param name="filename">
				<text>TSPBConfig_Types.h</text>
			</with-param>
		</call-template>
		
        <value-of select="$my:NL" />
	</template>

	<template name="type-definitions">
		<call-template name="section">
			<with-param name="text">
				<text>type definitions</text>
			</with-param>
		</call-template>

		<apply-templates select="/x:xgen/x:module[@name=$my:module]/x:configuration[@class=$pre-compile]/x:type">
			<sort select="my:computeTypeWeight(.)" data-type="number" />
		</apply-templates>
	</template>

	<template name="end-file">
        <value-of select="$my:NL" />
		<value-of select="my:normalize-end-of-line($h-undefines)" />
		<value-of select="$my:NL" />
		<text>#endif /* </text>
		<value-of select="$include-guard" />
		<text> */</text>
		<value-of select="$my:NL" />
		<call-template name="section">
			<with-param name="text">
				<text>end of file</text>
			</with-param>
		</call-template>
	</template>

</stylesheet>