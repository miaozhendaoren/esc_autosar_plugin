/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2014)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  $FILENAME   : Pwm_17_Gtm_Ver.h                                            **
**                                                                            **
**  $CC VERSION : \main\5 $                                                  **
**                                                                            **
**  $DATE       : 2014-05-27 $                                               **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : This file contains                                         **
**                 - functionality of Pwm driver.                            **
**                                                                            **
**  MAY BE CHANGED BY USER [Yes/No]: No                                       **
**                                                                            **
*******************************************************************************/
/* Traceability : 
                                                                     */



/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#ifndef PWM_17_GTM_VER_H
#define PWM_17_GTM_VER_H

/* Own header file, this includes own configuration file also */
#include "Pwm_17_Gtm.h"

#endif 
/* PWM_17_GTMCCU6_VER_H */
