# \file
#
# \brief AUTOSAR MemIf
#
# This file contains the implementation of the AUTOSAR
# module MemIf.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

MemIf_CORE_PATH      := $(SSC_ROOT)\MemIf_$(MemIf_VARIANT)

MemIf_OUTPUT_PATH    := $(AUTOSAR_BASE_OUTPUT_PATH)

#################################################################
# REGISTRY
SSC_PLUGINS          += MemIf

CC_INCLUDE_PATH      += \
   $(MemIf_CORE_PATH)\include \
   $(MemIf_OUTPUT_PATH)\include
