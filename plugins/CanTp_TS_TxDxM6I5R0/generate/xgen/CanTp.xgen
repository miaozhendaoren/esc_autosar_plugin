<?xml version="1.0" encoding="UTF-8"?>

<?module CanTp?>

<xgen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns="http://www.elektrobit.com/2013/xgen" xmlns:xi="http://www.w3.org/2001/XInclude">

[!CODE!]
[!AUTOSPACING!]
[!INCLUDE "../include/CanTp_PostBuild.m"!][!//
[!INCLUDE "../include/CanTp_Precompile.m"!][!//

[!/* Standard addressing format
*/!][!VAR "CANTP_STANDARD"="num:i(0)"!]
[!/* Extended addressing format
*/!][!VAR "CANTP_EXTENDED"="num:i(1)"!]
[!/* Mixed addressing format
*/!][!VAR "CANTP_MIXED"="num:i(2)"!]
[!/* N-SDU direction is Tx
*/!][!VAR "CANTP_PDU_DIR_TRANSMIT"="num:i(0)"!]
[!/* N-SDU direction is Rx
*/!][!VAR "CANTP_PDU_DIR_RECEIVE"="num:i(1)"!]
[!/* Indicating that no FC Tx PDU is used
*/!][!VAR "CANTP_FC_FRAME_NOT_USED"="num:i(65535)"!]
[!/* physical for 1 to 1 connections
*/!][!VAR "CANTP_PHYSICAL"="num:i(0)"!]
[!/* physical for 1 to n connections
*/!][!VAR "CANTP_FUNCTIONAL"="num:i(1)"!]
[!/* Channel mode type Full Duplex
*/!][!VAR "CANTP_MODE_FULL_DUPLEX"="num:i(0)"!]
[!/* Channel mode type Half Duplex
*/!][!VAR "CANTP_MODE_HALF_DUPLEX"="num:i(1)"!]
[!/* Global state of CanTp is ON
*/!][!VAR "CANTP_OFF"="num:i(0)"!]
[!/* Global state of CanTp is OFF
*/!][!VAR "CANTP_ON"="num:i(1)"!]

  <!-- This is the configuration of module CanTp -->
  <module name="CanTp">

    <!-- This is the Precompile-time configuration of the module. -->
    <configuration class="PreCompile">

      <?artifact directory="include" file="CanTp_Types_Int.h" type="types-h" ?>

      [!VAR "relocatable" = "'false'"!]
      [!IF "node:contains(node:refs(as:modconf('PbcfgM')/PbcfgMBswModules/*/PbcfgMBswModuleRef), as:modconf('CanTp')) = 'true'"!]
      [!IF "as:modconf('PbcfgM')/PbcfgMGeneral/PbcfgMRelocatableCfgEnable = 'true'"!]
      [!VAR "relocatable" = "'true'"!]
      [!ENDIF!]
      [!ELSE!]
      [!IF "as:modconf('CanTp')[1]/CanTpGeneral/CanTpRelocatablePbcfgEnable = 'true'"!]
      [!VAR "relocatable" = "'true'"!]
      [!ENDIF!]
      [!ENDIF!]


      <parameter name="relocatable-enable">
        <boolean-value>[!"$relocatable"!]</boolean-value>
      </parameter>


      <!-- This is the type definition of the root structure -->
      <type name="CanTp_ConfigType">
        <struct>
          <comment><![CDATA[Post-build configuration type]]></comment>
          <member name="PlatformSignature" type="uint32">
            <comment><![CDATA[Used to validate the platform]]></comment>
          </member>
          <member name="LcfgSignature" type="uint32">
            <comment><![CDATA[Used to validate the post build configuration against the link time configuration.]]></comment>
          </member>
          <member name="CfgSignature" type="uint32">
            <comment><![CDATA[Used to validate the post build configuration against the precompile time configuration.]]></comment>
          </member>
          <member name="PublicInfoSignature" type="uint32">
            <comment>
               <![CDATA[Used to validate the post build configuration against the precompile time published info
                        configuration.
               ]]>
            </comment>
          </member>
          <!-- Pointers to post build data structures -->
          <member name="RxNPduLookupTablePtr" type="CanTp_RxNPduLookupEntryType">
            <comment><![CDATA[This pointer contains the address to the N-PDU lookup table.]]></comment>
            <compiler-abstraction>
              <ref2cfg />
            </compiler-abstraction>
          </member>
          <member name="RxNPduTablePtr" type="CanTp_RxNPduTableType">
            <comment><![CDATA[This pointer contains the address to the RX N-PDU table if available.]]></comment>
            <compiler-abstraction>
              <ref2cfg />
            </compiler-abstraction>
          </member>
          <member name="RxNSduConfigPtr" type="CanTp_RxNSduConfigType">
            <comment><![CDATA[This pointer contains the address to the RX N-SDU config array if available.]]></comment>
            <compiler-abstraction>
              <ref2cfg />
            </compiler-abstraction>
          </member>
          <member name="TxNSduConfigPtr" type="CanTp_TxNSduConfigType">
            <comment><![CDATA[This pointer contains the address to the TX N-SDU config array if available.]]></comment>
            <compiler-abstraction>
              <ref2cfg />
            </compiler-abstraction>
          </member>
          [!IF "CanTpGeneral/CanTpGptUsageEnable = 'true'"!]
          <member name="GptChannelPtr" type="CanTp_GptChannelType">
            <comment><![CDATA[This pointer contains the address to the array of channels using general purpose timer.]]></comment>
            <compiler-abstraction>
              <ref2cfg />
            </compiler-abstraction>
          </member>
          [!ENDIF!]
          <member name="MfCtrConfigPtr" type="CanTp_MfCtrConfigType">
            <comment><![CDATA[This pointer contains the address to the main function control config array if available.]]></comment>
            <compiler-abstraction>
              <ref2cfg />
            </compiler-abstraction>
          </member>
          <member name="TxNPduToTpChannelPtr" type="uint8">
            <comment><![CDATA[Array to translate CanIf Tx Confirmation PDU IDs into the CanTp channel.]]></comment>
            <compiler-abstraction>
              <ref2cfg />
            </compiler-abstraction>
          </member>
          <member name="NumberRxNPdus" type="uint16">
            <comment><![CDATA[Number of unequal Rx N-PDU elements.]]></comment>
          </member>
          <member name="NumberTxNSdus" type="uint16">
            <comment><![CDATA[Configured number of TxNSdus.]]></comment>
          </member>
          <member name="NumberRxNSdus" type="uint16">
            <comment><![CDATA[Configured number of RxNSdus.]]></comment>
          </member>
          <member name="NumberTxConfNPdus" type="uint16">
            <comment><![CDATA[Configured number of tx confirmation NPdus (Tx ConfNPdus + RxFcNPdus).]]></comment>
          </member>
          [!IF "CanTpGeneral/CanTpDynamicNSaEnabled = 'true'"!]
          <member name="RxPduTableEntries" type="uint16">
            <comment>
               <![CDATA[This macro defines the numbers of entries in the CanTp_RxNPduTable[].
                        It is used to find PDU's in a loop.
               ]]>
            </comment>
          </member>
          [!ENDIF!]
          <member name="NumberOfChannels" type="uint8">
            <comment><![CDATA[Number of Tx and Rx channels.]]></comment>
          </member>
          <member name="NumberOfTxChannels" type="uint8">
            <comment><![CDATA[Number of Tx channels.]]></comment>
          </member>
        </struct>
      </type>

      <!-- This is the type definition of the Tx PDU config struture -->
      <type name="CanTp_RxNPduLookupEntryType">
          <comment><![CDATA[Type for the N-PDU Lookup Table.]]></comment>
        <reference type="uint16" />
      </type>

      <!--  Type for List of CanTp Rx N-PDUs. -->
      <type name="CanTp_RxNPduTableType">
       <struct>
        <comment><![CDATA[Type for CanTp List of all receivable N-PDUs.]]></comment>
        <member name="Index" type="PduIdType">
          <comment><![CDATA[index in Tx or Rx array]]></comment>
        </member>
        <member name="Next" type="uint16">
          <comment><![CDATA[index of next element with the same N-PDU Id, zero terminates]]></comment>
        </member>
        <member name="AddressingFormat" type="uint8">
          <comment><![CDATA[address format of this N-PDU]]></comment>
        </member>
        <member name="N_SA_AE" type="uint8">
          <comment>
             <![CDATA[expected source address in extended addressing mode or address extension
                     (N_AE) in mixed addressing mode.
             ]]>
          </comment>
        </member>
        <member name="Direction" type="uint8">
          <comment><![CDATA[direction (tx or rx)]]></comment>
        </member>
       </struct>
      </type>

      <!-- Type for CanTp Rx N-SDU configuration. -->
      <type name="CanTp_RxNSduConfigType">
       <struct>
        <comment><![CDATA[Type for CanTp Rx N-SDU configuration.]]></comment>
        <member name="NPduConfIdFc" type="PduIdType">
          <comment><![CDATA[N-Pdu confirmation Id for sent FC frames]]></comment>
        </member>
        <member name="LSduIdFc" type="PduIdType">
          <comment><![CDATA[L-Sdu Id for sending FC frames]]></comment>
        </member>
        <member name="IPduId" type="PduIdType">
          <comment><![CDATA[I-Pdu Id for Rx-UL communication (PduR_CanTpRxIndication)]]></comment>
        </member>
        <member name="NPduIdxF" type="PduIdType">
          <comment><![CDATA[N-Pdu Id for receiving frames from lower layer]]></comment>
        </member>
        <member name="N_Ar" type="uint16">
          <comment><![CDATA[time to transmit N-PDU]]></comment>
        </member>
        <member name="N_Br" type="uint16">
          <comment><![CDATA[time between two FCs]]></comment>
        </member>
        <member name="N_Cr" type="uint16">
          <comment><![CDATA[time until CF receive]]></comment>
        </member>
        <member name="AddressingFormat" type="uint8">
          <comment><![CDATA[addressing format - standard, extended]]></comment>
        </member>
        <member name="Tatype" type="uint8">
          <comment><![CDATA[physical, functional]]></comment>
        </member>
        <member name="PaddingActivation" type="uint8">
          <comment><![CDATA[padding of N-PDUs]]></comment>
        </member>
        <member name="N_TA_AE" type="uint8">
          <comment><![CDATA[extended target address (N_TA) or mixed address extension (N_AE)]]></comment>
        </member>
        <member name="WftMax" type="uint8">
          <comment><![CDATA[max number of FC wait]]></comment>
        </member>
        <member name="BS" type="uint8">
          <comment><![CDATA[block size]]></comment>
        </member>
        <member name="Channel" type="uint8">
          <comment><![CDATA[associated Tx channel ID]]></comment>
        </member>
        <member name="ChannelMode" type="uint8">
          <comment><![CDATA[associated channel mode]]></comment>
        </member>
        <member name="STmin" type="uint8">
          <comment><![CDATA[STmin value to be used for the sender]]></comment>
        </member>
       </struct>
      </type>

      <!-- Type for Tx N-SDU configuration. -->
      <type name="CanTp_TxNSduConfigType">
       <struct>
        <comment><![CDATA[Type for CanTp Tx N-SDU configuration.]]></comment>
        <member name="NPduIdFc" type="PduIdType">
          <comment><![CDATA[N-Pdu Id for incoming FC frames]]></comment>
        </member>
        <member name="NPduConfIdxF" type="PduIdType">
          <comment><![CDATA[N-Pdu confirmation Id for sent SF, FF, CF]]></comment>
        </member>
        <member name="LSduIdxF" type="PduIdType">
          <comment><![CDATA[L-Sdu Id for SF, FF, CF]]></comment>
        </member>
        <member name="IPduConfId" type="PduIdType">
          <comment><![CDATA[I-PDU Id for Tx-UL communication (CopyTxData, TxConfirmation)]]></comment>
        </member>
        <member name="N_As" type="uint16">
          <comment><![CDATA[time to transmit N-PDU]]></comment>
        </member>
        <member name="N_Bs" type="uint16">
          <comment><![CDATA[time to next FC]]></comment>
        </member>
        <member name="N_Cs" type="uint16">
          <comment><![CDATA[time to next CF]]></comment>
        </member>
        <member name="AddressingFormat" type="uint8">
          <comment><![CDATA[defines addres format (standard, extended)]]></comment>
        </member>
        <member name="Tatype" type="uint8">
          <comment><![CDATA[physical, functional]]></comment>
        </member>
        <member name="PaddingActivation" type="uint8">
          <comment><![CDATA[padding of N-PDUs]]></comment>
        </member>
        <member name="N_TA_AE" type="uint8">
          <comment><![CDATA[extended target address (N_TA) or mixed address extension (N_AE)]]></comment>
        </member>
        <member name="Channel" type="uint8">
          <comment><![CDATA[associated Tx channel ID]]></comment>
        </member>
        <member name="ChannelMode" type="uint8">
          <comment><![CDATA[associated channel mode]]></comment>
        </member>
        <member name="CanTpTc" type="boolean">
          <comment><![CDATA[parameter for enabling Tx cancellation]]></comment>
        </member>
       </struct>
      </type>

      [!IF "CanTpGeneral/CanTpGptUsageEnable = 'true'"!]
      <!-- Type for Gpt handling Tx channels -->
      <type name="CanTp_GptChannelType">
        <comment><![CDATA[Type for Gpt channel administration, needed for all Tx channels.]]></comment>
        <reference type="uint32" />
      </type>
      [!ENDIF!]

      <!-- Type of MainFunction counter config for Tx channels. -->
      <type name="CanTp_MfCtrConfigType">
       <struct>
        <comment><![CDATA[Type of MainFunction counter config, needed for all Tx channels.]]></comment>
        <member name="Resolution" type="uint32">
          <comment><![CDATA[Resolution of GPT in ns per ticks.]]></comment>
        </member>
        <member name="CallbackType" type="uint8">
          <comment><![CDATA[GPT or MainFunction callback.]]></comment>
        </member>
       </struct>
      </type>

    </configuration>  <!-- PreCompile -->



    <!-- This is the post-build configuration of the module. -->
    <configuration class="PostBuild" name="[!"node:name(CanTpConfig/*[1])"!]">

      <!-- !LINKSTO CanTp.ASR40.CANTP160_2,1 -->
      <?artifact directory="include" file="CanTp_PBcfg.h" type="postbuild-h" ?>
      <!-- !LINKSTO CanTp.ASR40.CANTP259,1 -->
      <?artifact directory="src" file="CanTp_PBcfg.c" type="postbuild-c" ?>

      <!--
          Type declarations:
          Specifies the content of CanTp_PBcfg.h and CanTp_Types.h
      -->

      <!-- This is the type definition of the layout type of the
           modules' postbuild configuration structure -->
      <type name="CanTp_ConfigLayoutType">
        <struct>
          <!-- Member of the layout type starting with the RootCfg -->
          <member name="RootCfg" type="CanTp_ConfigType"/>
          <member name="RxNPduLookupTable" type="CanTp_RxNPduLookupEntryType" count="[!"num:i($numRxNPdus + 1)"!]">
            <comment><![CDATA[This array contains the N-PDU lookup table.]]></comment>
          </member>
          [!IF "num:i($numRxNSdus + count(CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*[node:exists(CanTpRxFcNPdu)])) > 0"!]
          <member name="RxNPduTable" type="CanTp_RxNPduTableType" count="[!"num:i($numRxNSdus + count(CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*[node:exists(CanTpRxFcNPdu)]))"!]">
            <comment><![CDATA[This array contains the RX N-PDU table.]]></comment>
          </member>
          [!ENDIF!]
          [!IF "$numRxNSdus > 0"!]
          <member name="RxNSduConfig" type="CanTp_RxNSduConfigType" count="[!"num:i($numRxNSdus)"!]">
            <comment><![CDATA[This array contains the RX N-SDU config.]]></comment>
          </member>
          [!ENDIF!]
          [!IF "$numTxNSdus > 0"!]
          <member name="TxNSduConfig" type="CanTp_TxNSduConfigType" count="[!"num:i($numTxNSdus)"!]">
            <comment><![CDATA[This array contains the TX N-SDU config.]]></comment>
          </member>
          [!IF "CanTpGeneral/CanTpGptUsageEnable = 'true'"!]
          <member name="GptChannel" type="CanTp_GptChannelType" count="[!"num:i($numTxChannels)"!]">
            <comment><![CDATA[This array contains information about which channels are using general purpose timer.]]></comment>
          </member>
          [!ENDIF!]
          <member name="MfCtrConfig" type="CanTp_MfCtrConfigType" count="[!"num:i($numTxChannels)"!]">
            <comment><![CDATA[This array contains the main function control config.]]></comment>
          </member>
          [!ENDIF!]
          [!IF "$numTxConfNPdus > 0"!]
          <member name="TxNPduToTpChannel" type="uint8" count="[!"num:i($numTxConfNPdus)"!]">
            <comment><![CDATA[This array contains the CanTp channel index for the N-SDU, to which the
 CanIf Tx confirmation PDU with the ID as index of this array is assigned.]]></comment>
          </member>
          [!ENDIF!]
        </struct>
      </type>

      <type name="CanTp_ConstConfigLayoutType">
        <reference type="CanTp_ConfigLayoutType">
          <compiler-abstraction>
            <!-- !LINKSTO CanTp.ASR40.CANTP152,1 -->
            <const memory-class="CANTP_APPL_CONST" />
          </compiler-abstraction>
        </reference>
      </type>

      <!--
          Type definition and initialisation:
          Specifies the content of CanTp_PBcfg.c
      -->

      <memory-section name="CONFIG_DATA_UNSPECIFIED">
      <instance type="CanTp_ConstConfigLayoutType"
             name="CanTp_ConfigLayout">

        <comment><![CDATA[CanTp post build config. - Static code accesses this config
 solely via the pointer of type CanTp_ConfigType passed to
 \a CanTp_Init() as parameter.]]></comment>

        <!-- CanTp_ConfigLayout.RootCfg -->
        <field>
          <field>
            <int>[!"num:i(asc:getPlatformSignature())"!]</int>
          </field>
          <field>
            <int>[!"asc:getConfigSignature(as:modconf('CanTp')[1]//*[not(child::*) and (node:configclass() = 'Link')])"!]</int>
          </field>
          <field>
            <int>[!"asc:getConfigSignature(node:difference(as:modconf('CanTp')[1]//*[not(child::*) and (node:configclass() = 'PreCompile')],as:modconf('CanTp')[1]/CanTpJumpTable/*))"!]</int>
          </field>
          <field>
            <int>[!"asc:getConfigSignature(node:difference(as:modconf('CanTp')[1]/CommonPublishedInformation//*[not(child::*) and (node:configclass() = 'PublishedInformation') ], as:modconf('CanTp')[1]/CommonPublishedInformation/Release))"!]</int>
          </field>
          <field>
            <ref>
              <struct-field name="RxNPduLookupTable"/>
              <array-field index="0" />
            </ref>
          </field>

          <field>
            <ref>
              [!IF "num:i($numRxNSdus + count(CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*[node:exists(CanTpRxFcNPdu)])) > 0"!]
              <struct-field name="RxNPduTable"/>
              <array-field index="0" />
              [!ENDIF!]
            </ref>
          </field>

          <field>
            <ref>
              [!IF "$numRxNSdus > 0"!]
              <struct-field name="RxNSduConfig"/>
              <array-field index="0" />
              [!ENDIF!]
            </ref>
          </field>

          <field>
            <ref>
              [!IF "$numTxNSdus > 0"!]
              <struct-field name="TxNSduConfig"/>
              <array-field index="0" />
              [!ENDIF!]
            </ref>
          </field>

          [!IF "CanTpGeneral/CanTpGptUsageEnable = 'true'"!]
          <field>
            <ref>
              [!IF "$numTxNSdus > 0"!]
              <struct-field name="GptChannel"/>
              <array-field index="0" />
              [!ENDIF!]
            </ref>
          </field>
          [!ENDIF!]

          <field>
            <ref>
              [!IF "$numTxNSdus > 0"!]
              <struct-field name="MfCtrConfig"/>
              <array-field index="0" />
              [!ENDIF!]
            </ref>
          </field>

          <field>
            <ref>
              [!IF "$numTxConfNPdus > 0"!]
              <struct-field name="TxNPduToTpChannel"/>
              <array-field index="0" />
              [!ENDIF!]
            </ref>
          </field>

          <field>
            <int>[!"num:i($numRxNPdus)"!]</int>
          </field>
          <field>
            <int>[!"num:i($numTxNSdus)"!]</int>
          </field>
          <field>
            <int>[!"num:i($numRxNSdus)"!]</int>
          </field>

          <field>
            <int>[!"num:i($numTxConfNPdus)"!]</int>
          </field>

          [!IF "CanTpGeneral/CanTpDynamicNSaEnabled = 'true'"!]
          <field>
            <int>[!"num:i($numRxNSdus + count(CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*[node:exists(CanTpRxFcNPdu)]))"!]</int>
          </field>
          [!ENDIF!]
          <field>
            <int>[!"num:i($numAllChannels)"!]</int>
          </field>
          <field>
            <int>[!"num:i($numTxChannels)"!]</int>
          </field>
        </field> <!-- CanTp_ConfigLayout.RootCfg -->

        <!-- CanTp_ConfigLayout.RxNPduLookupTable -->
        <field>
          <field>
            <int>0</int>
          </field>
          [!VAR "num" = "0"!]
          [!FOR "I"="0" TO "$numRxNPdus - 1"!]
          [!VAR "num"="$num + count(CanTpConfig/*[1]/CanTpChannel/*/CanTpRxNSdu/*[CanTpRxNPdu/CanTpRxNPduId = $I]) + count(CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*[node:exists(CanTpRxFcNPdu) and (CanTpRxFcNPdu/CanTpRxFcNPduId = $I)])"!]
          <field>
            <int>[!"num:i($num)"!]</int>
          </field>
          [!ENDFOR!]
        </field> <!-- CanTp_ConfigLayout.RxNPduLookupTable -->

        [!IF "num:i($numRxNSdus + count(CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*[node:exists(CanTpRxFcNPdu)])) > 0"!]
        <field>
          [!VAR "idx"="1"!]
          [!FOR "I"="0"  TO "$numRxNPdus - 1"!]
            [!VAR "total"!][!CALL "getNumRxNPdusById", "pduId"="num:i($I)"!][!ENDVAR!]
            [!VAR "now"="1"!]
            [!LOOP "CanTpConfig/*[1]/CanTpChannel/*/CanTpRxNSdu/*[CanTpRxNPdu/CanTpRxNPduId = $I]"!]
              <field>
                <field>
                  <int>[!CALL "getRxNSduIdByNSduRef", "nSduRef"="as:path(.)"!]</int>
                </field>
                <field>
                  [!IF "$now < $total"!]
                  <int>[!"num:i($idx)"!]</int>
                  [!ELSE!]
                  <int>0</int>
                  [!ENDIF!]
                </field>
                [!IF "CanTpRxAddressingFormat = 'CANTP_STANDARD'"!]
                <field>
                  <int>[!"$CANTP_STANDARD"!]</int>
                </field>
                <field>
                  <int>0</int>
                </field>
                [!ELSEIF "CanTpRxAddressingFormat = 'CANTP_EXTENDED'"!]
                <field>
                  <int>[!"$CANTP_EXTENDED"!]</int>
                </field>
                <field>
                  <int>[!"num:i(CanTpNSa/CanTpNSa)"!]</int>
                </field>
                [!ELSE!]
                <field>
                  <int>[!"$CANTP_MIXED"!]</int>
                </field>
                <field>
                  <int>[!"num:i(CanTpNAe/CanTpNAe)"!]</int>
                </field>
                [!ENDIF!]
                <field>
                  <int>[!"$CANTP_PDU_DIR_RECEIVE"!]</int>
                </field>
              </field>
              [!VAR "now"="$now + 1"!]
              [!VAR "idx"="$idx + 1"!]
            [!ENDLOOP!]
            [!LOOP "CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*[node:exists(CanTpRxFcNPdu) and (CanTpRxFcNPdu/CanTpRxFcNPduId = $I)]"!]
            <field>
              <field>
                <int>[!CALL "getTxNSduIdByNSduRef", "nSduRef"="as:path(.)"!]</int>
              </field>
              <field>
                [!IF "$now < $total"!]
                <int>[!"num:i($idx)"!]</int>
                [!ELSE!]
                <int>0</int>
                [!ENDIF!]
              </field>
              [!IF "CanTpTxAddressingFormat = 'CANTP_STANDARD'"!]
              <field>
                <int>[!"$CANTP_STANDARD"!]</int>
              </field>
              <field>
                <int>0</int>
              </field>
              [!ELSEIF "CanTpTxAddressingFormat = 'CANTP_EXTENDED'"!]
              <field>
                <int>[!"$CANTP_EXTENDED"!]</int>
              </field>
              <field>
                <int>[!"num:i(CanTpNSa/CanTpNSa)"!]</int>
              </field>
              [!ELSE!]
              <field>
                <int>[!"$CANTP_MIXED"!]</int>
              </field>
              <field>
                <int>[!"num:i(CanTpNAe/CanTpNAe)"!]</int>
              </field>
              [!ENDIF!]
              <field>
                <int>[!"$CANTP_PDU_DIR_TRANSMIT"!]</int>
              </field>
            </field>
            [!VAR "now"="$now + 1"!]
            [!VAR "idx"="$idx + 1"!]
            [!ENDLOOP!]
          [!ENDFOR!]
        </field>
        [!ENDIF!]

        [!IF "$numRxNSdus > 0"!]
        <field>
           [!FOR "I"="0" TO "$numRxNSdus - 1"!]
           [!VAR "pduRef"!][!CALL "getRxNSduRefByNSduId", "nSduId"="$I"!][!ENDVAR!]
           [!SELECT "as:ref($pduRef)"!]
           <field>
             [!IF "node:exists(CanTpTxFcNPdu)"!]
             <field>
                <int>[!"CanTpTxFcNPdu/CanTpTxFcNPduConfirmationPduId"!]</int>
             </field>
             [!SELECT "as:modconf('CanIf')[1]/CanIfInitCfg/*/CanIfTxPduCfg/*[CanIfTxPduRef = node:current()/CanTpTxFcNPdu/CanTpTxFcNPduRef][1]"!]
             <field>
                <int>[!"CanIfTxPduId"!]</int>
             </field>
             [!ENDSELECT!]
             [!ELSE!]
             <field>
                <int>[!"$CANTP_FC_FRAME_NOT_USED"!]</int>
             </field>
             <field>
                <int>[!"$CANTP_FC_FRAME_NOT_USED"!]</int>
             </field>
             [!ENDIF!]
             <field>
              <int>[!"as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/PduRRoutingPath/*[PduRSrcPdu/PduRSrcPduRef = node:current()/CanTpRxNSduRef][1]/PduRSrcPdu/PduRSourcePduHandleId"!]
              </int>
             </field>
             <field>
                <int>[!"CanTpRxNPdu/CanTpRxNPduId"!]</int>
             </field>
             <field>
               [!IF "node:exists(CanTpNar)"!]
                <int>[!CALL "calcNAbcRsValue", "value"="CanTpNar", "name"="'N_Ar'", "direction"="'Rx'"!]</int>
                [!ELSE!]
                <int>0</int>
                [!ENDIF!]
             </field>
             <field>
                <int>[!CALL "calcNbrValue", "value"="CanTpNbr", "name"="'N_Br'", "direction"="'Rx'"!]</int>
             </field>
             <field>
               [!IF "node:exists(CanTpNcr)"!]
                <int>[!CALL "calcNAbcRsValue", "value"="CanTpNcr", "name"="'N_Cr'", "direction"="'Rx'"!]</int>
                [!ELSE!]
                <int>0</int>
                [!ENDIF!]
             </field>
             <field>
               [!IF "CanTpRxAddressingFormat = 'CANTP_STANDARD'"!]
                <int>[!"$CANTP_STANDARD"!]</int>
                [!ELSEIF "CanTpRxAddressingFormat = 'CANTP_EXTENDED'"!]
                <int>[!"$CANTP_EXTENDED"!]</int>
                [!ELSE!]
                <int>[!"$CANTP_MIXED"!]</int>
                [!ENDIF!]
             </field>
             <field>
               [!IF "CanTpRxTaType = 'CANTP_PHYSICAL'"!]
                <int>[!"$CANTP_PHYSICAL"!]</int>
                [!ELSE!]
                <int>[!"$CANTP_FUNCTIONAL"!]</int>
                [!ENDIF!]
             </field>
             <field>
               [!IF "CanTpRxPaddingActivation = 'CANTP_OFF'"!]
                <int>[!"$CANTP_OFF"!]</int>
                [!ELSE!]
                <int>[!"$CANTP_ON"!]</int>
                [!ENDIF!]
             </field>
             <field>
               [!IF "CanTpRxAddressingFormat = 'CANTP_STANDARD'"!]
                <int>0</int>
                [!ELSEIF "CanTpRxAddressingFormat = 'CANTP_EXTENDED'"!]
                <int>[!"num:i(CanTpNTa/CanTpNTa)"!]</int>
                [!ELSE!]
                <int>[!"num:i(CanTpNAe/CanTpNAe)"!]</int>
             [!ENDIF!]
             </field>
             <field>
               [!IF "node:exists(CanTpRxWftMax)"!]
                <int>[!"num:i(CanTpRxWftMax)"!]</int>
                [!ELSE!]
                <int>0</int>
                [!ENDIF!]
             </field>
             <field>
               [!IF "node:exists(CanTpBs)"!]
                <int>[!"num:i(CanTpBs)"!]</int>
                [!ELSE!]
                <int>0</int>
                [!ENDIF!]
             </field>
             [!VAR "chId"!][!CALL "getChanIdByChanRef", "chanRef"="node:path(../..)"!][!ENDVAR!]
             [!WS "0"!][!/* if the channel is a full duplex channel, the Rx N-SDU is assigned to the Rx part of the channel
                       (ID + Nr. of Tx channels)
             */!][!IF "../../CanTpChannelMode = 'CANTP_MODE_FULL_DUPLEX'"!]
             [!VAR "chId"="$chId + $numTxChannels"!]
             [!ENDIF!]
             <field>
                <int>[!"num:i($chId)"!]</int>
             </field>
             <field>
               [!IF "../../CanTpChannelMode = 'CANTP_MODE_FULL_DUPLEX'"!]
                <int>[!"$CANTP_MODE_FULL_DUPLEX"!]</int>
                [!ELSE!]
                <int>[!"$CANTP_MODE_HALF_DUPLEX"!]</int>
                [!ENDIF!]
             </field>
             <field>
               [!IF "node:exists(CanTpSTmin)"!]
                <int>[!CALL "calcSTmin", "value"="CanTpSTmin"!]</int>
                [!ELSE!]
                <int>0</int>
             [!ENDIF!]
             </field>
           </field>
           [!ENDSELECT!]
           [!ENDFOR!]
        </field>
        [!ENDIF!]

        [!IF "$numTxNSdus > 0"!]
        <field>
          [!FOR "I"="0" TO "$numTxNSdus - 1"!]
          [!VAR "pduRef"!][!CALL "getTxNSduRefByNSduId", "nSduId"="$I"!][!ENDVAR!]
          [!SELECT "as:ref($pduRef)"!]
          <field>
            <field>
              [!IF "node:exists(CanTpRxFcNPdu)"!]
              <int>[!"CanTpRxFcNPdu/CanTpRxFcNPduId"!]</int>
              [!ELSE!]
              <int>[!"$CANTP_FC_FRAME_NOT_USED"!]</int>
              [!ENDIF!]
            </field>
            <field>
              <int>[!"CanTpTxNPdu/CanTpTxNPduConfirmationPduId"!]</int>
            </field>
            [!WS "0"!][!/* NOTE: this will have to be reworked when multiple configuration sets are supported by the CanIf:
            */!][!SELECT "as:modconf('CanIf')[1]/CanIfInitCfg/*/CanIfTxPduCfg/*[CanIfTxPduRef = node:current()/CanTpTxNPdu/CanTpTxNPduRef][1]"!]
            <field>
              <int>[!"CanIfTxPduId"!]</int>
            </field>
            [!ENDSELECT!]
            <field>
              <int>[!"as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/PduRRoutingPath/*/PduRDestPdu/*[PduRDestPduRef = node:current()/CanTpTxNSduRef][1]/PduRDestPduHandleId"!]
              </int>
            </field>
            <field>
              <int>[!CALL "calcNAbcRsValue", "value"="CanTpNas", "name"="'N_As'", "direction"="'Tx'"!]</int>
            </field>
            <field>
              [!IF "node:exists(CanTpNbs)"!]
              <int>[!CALL "calcNAbcRsValue", "value"="CanTpNbs", "name"="'N_Bs'", "direction"="'Tx'"!]</int>
              [!ELSE!]
              <int>0</int>
              [!ENDIF!]
            </field>
            <field>
              <int>[!CALL "calcNAbcRsValue", "value"="CanTpNcs", "name"="'N_Cs'", "direction"="'Tx'"!]</int>
            </field>
            <field>
              [!IF "CanTpTxAddressingFormat = 'CANTP_STANDARD'"!]
              <int>[!"$CANTP_STANDARD"!]</int>
              [!ELSEIF "CanTpTxAddressingFormat = 'CANTP_EXTENDED'"!]
              <int>[!"$CANTP_EXTENDED"!]</int>
              [!ELSE!]
              <int>[!"$CANTP_MIXED"!]</int>
              [!ENDIF!]
            </field>
            <field>
              [!IF "CanTpTxTaType = 'CANTP_PHYSICAL'"!]
              <int>[!"$CANTP_PHYSICAL"!]</int>
              [!ELSE!]
              <int>[!"$CANTP_FUNCTIONAL"!]</int>
              [!ENDIF!]
            </field>
            <field>
              [!IF "CanTpTxPaddingActivation = 'CANTP_OFF'"!]
              <int>[!"$CANTP_OFF"!]</int>
              [!ELSE!]
              <int>[!"$CANTP_ON"!]</int>
              [!ENDIF!]
            </field>
            <field>
              [!IF "CanTpTxAddressingFormat = 'CANTP_STANDARD'"!]
              <int>0</int>
              [!ELSEIF "CanTpTxAddressingFormat = 'CANTP_EXTENDED'"!]
              <int>[!"num:i(CanTpNTa/CanTpNTa)"!]</int>
              [!ELSE!]
              <int>[!"num:i(CanTpNAe/CanTpNAe)"!]</int>
              [!ENDIF!]
            </field>
            <field>
              <int>[!CALL "getChanIdByChanRef", "chanRef"="node:path(../..)"!]</int>
            </field>
            <field>
              [!IF "../../CanTpChannelMode = 'CANTP_MODE_FULL_DUPLEX'"!]
              <int>[!"$CANTP_MODE_FULL_DUPLEX"!]</int>
              [!ELSE!]
              <int>[!"$CANTP_MODE_HALF_DUPLEX"!]</int>
              [!ENDIF!]
            </field>
            <field>
              [!IF "CanTpTc = 'true'"!]
              <int>1</int>
              [!ELSE!]
              <int>0</int>
              [!ENDIF!]
            </field>
          </field>
          [!ENDSELECT!]
          [!ENDFOR!]
        </field> <!-- CanTp_ConfigLayout.TxNSduConfig -->
        [!ENDIF!]

        [!IF "$numTxNSdus > 0"!]
        [!IF "CanTpGeneral/CanTpGptUsageEnable = 'true'"!]
        <field>
          [!FOR "I"="0" TO "$numTxChannels - 1"!]
          [!VAR "userId"!][!CALL "getChanRefByChanId", "chanId"="$I"!][!ENDVAR!]
          [!SELECT "node:ref($userId)"!]
          <field>
            [!IF "CanTpSTminTimeoutHandling = 'Gpt'"!]
            <int>[!"CanTpGptChannelId"!]</int>
            [!ELSE!]
            <int>255</int>
            [!ENDIF!]
          </field>
          [!ENDSELECT!]
          [!ENDFOR!]
        </field> <!-- CanTp_ConfigLayout.GptChannel -->
        [!ENDIF!]
        [!ENDIF!]

        [!IF "$numTxNSdus > 0"!]
        <field>
          [!FOR "I"="0" TO "$numTxChannels - 1"!]
          [!VAR "chanRef"!][!CALL "getChanRefByChanId", "chanId"="$I"!][!ENDVAR!]
          [!SELECT "node:ref($chanRef)"!]
          <field>
             [!IF "CanTpSTminTimeoutHandling = 'CanTpMainFunction'"!]
             <field>
               <int>[!"num:i(as:modconf('CanTp')[1]/CanTpConfig/*[1]/CanTpMainFunctionPeriod*1000000000)"!]</int>
             </field>
             <field>
               <int>1</int>
             </field>
             [!ELSE!]
             <field>
               <int>[!"num:i(CanTpGptChannelResolution)"!]</int>
             </field>
             <field>
               <int>0</int>
             </field>
             [!ENDIF!]
          </field>
          [!ENDSELECT!]
          [!ENDFOR!]
        </field> <!-- CanTp_ConfigLayout.MfCtrConfig -->
        [!ENDIF!]

        [!IF "$numTxConfNPdus > 0"!]
        <field>
          [!FOR "I"="0" TO "$numTxConfNPdus - 1"!]
          <field>
          [!IF "count(CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*[CanTpTxNPdu/CanTpTxNPduConfirmationPduId = $I]) = 1"!]
          [!SELECT "CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*[CanTpTxNPdu/CanTpTxNPduConfirmationPduId = $I]"!]
            <int>[!CALL "getChanIdByChanRef", "chanRef"="node:path(../..)"!]</int>
          [!ENDSELECT!]
          [!ELSEIF "count(CanTpConfig/*[1]/CanTpChannel/*/CanTpRxNSdu/*[CanTpTxFcNPdu/CanTpTxFcNPduConfirmationPduId = $I]) = 1"!]
          [!SELECT "CanTpConfig/*[1]/CanTpChannel/*/CanTpRxNSdu/*[CanTpTxFcNPdu/CanTpTxFcNPduConfirmationPduId = $I]"!]
          [!VAR "chId"!][!CALL "getChanIdByChanRef", "chanRef"="node:path(../..)"!][!ENDVAR!]
          [!IF "../../CanTpChannelMode = 'CANTP_MODE_FULL_DUPLEX'"!]
          [!VAR "chId"="$chId + $numTxChannels"!]
          [!ENDIF!]
            <int>[!"num:i($chId)"!]</int>
          [!ENDSELECT!]
          [!ELSE!]
          [!ERROR!] No channel found for TxConfirmation PduId [!"$I"!].[!ENDERROR!]
          [!ENDIF!]
          </field>
          [!ENDFOR!]
        </field> <!-- CanTp_ConfigLayout.TxNPduToTpChannel -->
        [!ENDIF!]

      </instance> <!-- CanTp_ConfigLayoutType -->
      </memory-section>

    </configuration>
  </module>

  <!-- include the AUTOSAR standard data types, etc. -->
  <xi:include href="Base.xgen" xpointer="element(/1/1)"/>

</xgen>

[!ENDCODE!]