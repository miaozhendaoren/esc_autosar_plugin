/**
 * \file
 *
 * \brief AUTOSAR CanTp
 *
 * This file contains the implementation of the AUTOSAR
 * module CanTp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!CODE!]
[!AUTOSPACING!]
#if(!defined CANTP_SYMBOLICNAMES_PBCFG_H)
#define CANTP_SYMBOLICNAMES_PBCFG_H

[!INCLUDE "CanTp_PostBuild.m"!][!//
/*==================[includes]==============================================*/

/*==================[macros]================================================*/

/* Generate macros for CanTpRxNSdus */
[!LOOP "as:modconf('CanTp')[1]/CanTpConfig/*[1]/CanTpChannel/*/CanTpRxNSdu/*"!]
[!IF "node:exists(CanTpRxNSduId)"!]

/*------------------------ [!"@name"!]---------------------------------- */

[!CALL "GetSymbolName", "ShortNameRef"="'.'", "OldName"="name(../.)"!]
/** \brief Export symbolic name value for [!"../../@name"!]_[!"@name"!] */
#define CanTpConf_[!"$SymbolName"!]   [!"./CanTpRxNSduId"!]U

#if(!defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"../../@name"!]_[!"@name"!]             [!"./CanTpRxNSduId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only 
 *         (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2)
 */
#define CanTp_[!"../../@name"!]_[!"@name"!]       [!"./CanTpRxNSduId"!]U
#endif /* !defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDIF!]

[!IF "node:exists(CanTpRxNPdu/CanTpRxNPduId)"!]
[!CALL "GetSymbolName", "ShortNameRef"="'CanTpRxNPdu'", "OldName"="name(./CanTpRxNPdu)"!]
/** \brief Export symbolic name value for [!"../../@name"!]_[!"@name"!]_CanTpRxNPdu */
#define CanTpConf_[!"$SymbolName"!]  [!"./CanTpRxNPdu/CanTpRxNPduId"!]U

#if (!defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4)        */
#define [!"../../@name"!]_[!"@name"!]_CanTpRxNPdu             [!"./CanTpRxNPdu/CanTpRxNPduId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only 
           (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2)                               */
#define CanTp_[!"../../@name"!]_[!"@name"!]_CanTpRxNPdu       [!"./CanTpRxNPdu/CanTpRxNPduId"!]U
#endif /* !defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDIF!]

[!IF "node:exists(CanTpTxFcNPdu/CanTpTxFcNPduConfirmationPduId)"!]
[!CALL "GetSymbolName", "ShortNameRef"="'./CanTpTxFcNPdu'", "OldName"="name(./CanTpTxFcNPdu)"!]
/** \brief Export symbolic name value for [!"../../@name"!]_[!"@name"!]_CanTpTxFcNPdu */
#define CanTpConf_[!"$SymbolName"!]   [!"./CanTpTxFcNPdu/CanTpTxFcNPduConfirmationPduId"!]U

#if (!defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4)        */
#define [!"../../@name"!]_[!"@name"!]_CanTpTxFcNPdu             [!"./CanTpTxFcNPdu/CanTpTxFcNPduConfirmationPduId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only 
           (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2)                               */
#define CanTp_[!"../../@name"!]_[!"@name"!]_CanTpTxFcNPdu       [!"./CanTpTxFcNPdu/CanTpTxFcNPduConfirmationPduId"!]U
#endif /* !defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDIF!]
[!ENDLOOP!]

/* generate macros for CanTpTxNSdus */

[!LOOP "as:modconf('CanTp')[1]/CanTpConfig/*[1]/CanTpChannel/*/CanTpTxNSdu/*"!][!/*
*/!][!IF "node:exists(CanTpTxNSduId)"!]

/*------------------------ [!"@name"!]---------------------------------- */

[!CALL "GetSymbolName", "ShortNameRef"="'.'", "OldName"="name(../.)"!]
/** \brief Export symbolic name value for [!"../../@name"!]_[!"@name"!] */
#define CanTpConf_[!"$SymbolName"!]           [!"CanTpTxNSduId"!]U

#if(!defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"../../@name"!]_[!"@name"!]                     [!"CanTpTxNSduId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only 
 *         (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2)
 */
#define CanTp_[!"../../@name"!]_[!"@name"!]               [!"CanTpTxNSduId"!]U
#endif /* !defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDIF!]

[!IF "node:exists(CanTpRxFcNPdu/CanTpRxFcNPduId)"!]
[!CALL "GetSymbolName", "ShortNameRef"="'./CanTpRxFcNPdu'", "OldName"="name(./CanTpRxFcNPdu)"!]
/** \brief Export symbolic name value for [!"../../@name"!]_[!"@name"!]_CanTpRxFcNPdu */
#define CanTpConf_[!"$SymbolName"!]  [!"./CanTpRxFcNPdu/CanTpRxFcNPduId"!]U

#if (!defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4)        */
#define [!"../../@name"!]_[!"@name"!]_CanTpRxFcNPdu            [!"./CanTpRxFcNPdu/CanTpRxFcNPduId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only 
           (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2)                               */
#define CanTp_[!"../../@name"!]_[!"@name"!]_CanTpRxFcNPdu      [!"./CanTpRxFcNPdu/CanTpRxFcNPduId"!]U
#endif /* !defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDIF!]

[!IF "node:exists(CanTpTxNPdu/CanTpTxNPduConfirmationPduId)"!]
[!CALL "GetSymbolName", "ShortNameRef"="'./CanTpTxNPdu'", "OldName"="name(./CanTpTxNPdu)"!]
/** \brief Export symbolic name value for [!"../../@name"!]_[!"@name"!]_CanTpTxNPdu */
#define CanTpConf_[!"$SymbolName"!]   [!"./CanTpTxNPdu/CanTpTxNPduConfirmationPduId"!]U

#if (!defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4)        */
#define [!"../../@name"!]_[!"@name"!]_CanTpTxNPdu             [!"./CanTpTxNPdu/CanTpTxNPduConfirmationPduId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only 
           (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2)                               */
#define CanTp_[!"../../@name"!]_[!"@name"!]_CanTpTxNPdu       [!"./CanTpTxNPdu/CanTpTxNPduConfirmationPduId"!]U
#endif /* !defined CANTP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDIF!]
[!ENDLOOP!]

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( CANTP_SYMBOLICNAMES_PBCFG_H ) */
/*==================[end of file]===========================================*/
[!ENDCODE!][!//
