/**
 * \file
 *
 * \brief Autosar FrTp
 *
 * This file contains the implementation of the Autosar
 * module FrTp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2012 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 9.1 (required)
 * All automatic variables shall have been assigned a value before being used.
 *
 * Reason:
 * The affected variable is an array used to store temporary information within
 * FrTp_MainFunction(). Valid access to this local variable is secured by the
 * software.
 */
/******************************************************************************
 **                      Include Section                                     **
 *****************************************************************************/

#include <FrTp_Trace.h>
#include <PduR_FrTp.h>  /* PduR API called by FrTp */
#include <FrTp_Priv.h>  /* data types */
#include <FrTp_Lcfg.h>

/* !LINKSTO FrTp.EB.PublishedInformation,1 */
/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined FRTP_VENDOR_ID) /* configuration check */
#error FRTP_VENDOR_ID must be defined
#endif

#if (FRTP_VENDOR_ID != 1U) /* vendor check */
#error FRTP_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined FRTP_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error FRTP_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FRTP_AR_RELEASE_MAJOR_VERSION != 4U)
#error FRTP_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined FRTP_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error FRTP_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FRTP_AR_RELEASE_MINOR_VERSION != 0U )
#error FRTP_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined FRTP_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error FRTP_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (FRTP_AR_RELEASE_REVISION_VERSION != 3U )
#error FRTP_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined FRTP_SW_MAJOR_VERSION) /* configuration check */
#error FRTP_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FRTP_SW_MAJOR_VERSION != 4U)
#error FRTP_SW_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined FRTP_SW_MINOR_VERSION) /* configuration check */
#error FRTP_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FRTP_SW_MINOR_VERSION < 4U)
#error FRTP_SW_MINOR_VERSION wrong (< 4U)
#endif

#if (!defined FRTP_SW_PATCH_VERSION) /* configuration check */
#error FRTP_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (FRTP_SW_PATCH_VERSION < 5U)
#error FRTP_SW_PATCH_VERSION wrong (< 5U)
#endif
/******************************************************************************
 **                      Local Macros                                        **
 *****************************************************************************/



/******************************************************************************
**                      Variable Definitions                                 **
******************************************************************************/


/*
 * Start variable section declaration
 */
#define FRTP_START_SEC_VAR_INIT_UNSPECIFIED
#include <MemMap.h>

/* channel run-time data  - !LINKSTO FRTP210, 1 */
VAR(FrTp_ChannelType,FRTP_VAR) FrTp_Channel[FRTP_CHAN_NUM];

VAR(FrTp_RuntimeTxPduType,FRTP_VAR) FrTp_RuntimeTxPdu[FRTP_TXPDU_NUM];

/*
 * Stop variable section declaration
 */
#define FRTP_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <MemMap.h>


/*
 * Start variable section declaration
 */
#define FRTP_START_SEC_VAR_FAST_INIT_UNSPECIFIED
#include <MemMap.h>

/* pointer to post-build configuration  - !LINKSTO FRTP210, 1 */
P2CONST(FrTp_ConfigType,FRTP_VAR,FRTP_APPL_CONST) FrTp_RootPtr = NULL_PTR; 
/* module initialization status  - !LINKSTO FRTP210, 1 */
VAR(uint8,FRTP_VAR) FrTp_InitStatus;

/*
 * Stop variable section declaration
 */
#define FRTP_STOP_SEC_VAR_FAST_INIT_UNSPECIFIED
#include <MemMap.h>


/******************************************************************************
**                      Local Function Prototypes                            **
******************************************************************************/
/*
 * Start code section declaration
 */
#define FRTP_START_SEC_CODE
#include <MemMap.h>

STATIC FUNC(void,FRTP_CODE) FrTp_HandleTimers(P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel);
STATIC FUNC(void,FRTP_CODE) FrTp_HandleTxTimer1(P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel);
STATIC FUNC(void,FRTP_CODE) FrTp_HandleTxTimer2(P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel);

#if (FRTP_FULLDUPLEX_ENABLE == STD_ON)
STATIC FUNC(void,FRTP_CODE) FrTp_HandleRxTimer1(P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel);
STATIC FUNC(void,FRTP_CODE) FrTp_HandleRxTimer2(P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel);
#endif /* FRTP_FULLDUPLEX_ENABLE */

STATIC FUNC(void,FRTP_CODE) FrTp_HandleRxTimer3(P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel);

STATIC FUNC(P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR),FRTP_CODE)
    FrTp_FindTxChannel(VAR(PduIdType,AUTOMATIC) TxPduId);

/**
 * \brief Resets all state variables for the given channel.
 */
STATIC FUNC(void,FRTP_CODE) FrTp_ResetChannel
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
);

/*
 * Stop code section declaration
 */
#define FRTP_STOP_SEC_CODE
#include <MemMap.h>

/*
 * Start code section declaration
 */
#define FRTP_START_SEC_CODE
#include <MemMap.h>

/******************************************************************************
**                      Local Function Definitions                           **
******************************************************************************/

STATIC FUNC(void,FRTP_CODE) FrTp_HandleTimers
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{

  DBG_FRTP_HANDLETIMERS_ENTRY(pChannel);
  FrTp_HandleTxTimer1(pChannel);
  FrTp_HandleTxTimer2(pChannel);
#if (FRTP_FULLDUPLEX_ENABLE == STD_ON)
  FrTp_HandleRxTimer1(pChannel);
  FrTp_HandleRxTimer2(pChannel);
#endif /* FRTP_FULLDUPLEX_ENABLE */
  FrTp_HandleRxTimer3(pChannel);

  DBG_FRTP_HANDLETIMERS_EXIT(pChannel);
}


STATIC FUNC(void,FRTP_CODE) FrTp_HandleTxTimer1
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{
    boolean TimerExpiredFlag = FALSE;

    DBG_FRTP_HANDLETXTIMER1_ENTRY(pChannel);

    /* enter exclusive area */
    SchM_Enter_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    /* handle channel timer AS */
    switch(pChannel->Tx_Timer1)
    {
        case 0U: break; /* timer deactivated */
        case 1U: /* timer expired */
        {
            FrTp_StopTimer(pChannel->Tx_Timer1);
            TimerExpiredFlag = TRUE;
            break;
        }
        default:
        {
            pChannel->Tx_Timer1--;
            break; /* timer still running */
        }
    }

    /* exit exclusive area */
    SchM_Exit_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    if( TimerExpiredFlag == TRUE )
    {
        /* in half duplex case, demultiplex timer usage */
#if (FRTP_FULLDUPLEX_ENABLE == STD_OFF)
        if(FrTp_Chnl_IsRxState(pChannel->Tx_State))
        {
            (void)FrTp_FrIfCancelTransmit(pChannel);
            FrTp_RxSm_IndicateAndAbort(pChannel,NTFRSLT_E_TIMEOUT_A);
        }
        else
#endif /* FRTP_FULLDUPLEX_ENABLE */
        {
            (void)FrTp_FrIfCancelTransmit(pChannel);
            FrTp_TxSm_IndicateAndAbort(pChannel,NTFRSLT_E_TIMEOUT_A);
        }
    }

    DBG_FRTP_HANDLETXTIMER1_EXIT(pChannel);
}


STATIC FUNC(void,FRTP_CODE) FrTp_HandleTxTimer2
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{
    boolean TimerExpiredFlag = FALSE;

    DBG_FRTP_HANDLETXTIMER2_ENTRY(pChannel);

    /* enter exclusive area */
    SchM_Enter_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    /* handle channel timer BS */
    switch(pChannel->Tx_Timer2)
    {
        case 0U: break; /* timer deactivated */
        case 1U: /* timer expired */
        {
            FrTp_StopTimer(pChannel->Tx_Timer2);
            TimerExpiredFlag = TRUE;
            break;
        }
        default:
        {
            pChannel->Tx_Timer2--;
            break; /* timer still running */
        }
    }

    /* exit exclusive area */
    SchM_Exit_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    if( TimerExpiredFlag == TRUE )
    {
/* in half duplex case, demultiplex timer usage */
#if (FRTP_FULLDUPLEX_ENABLE == STD_OFF)
        if(FrTp_Chnl_IsRxState(pChannel->Tx_State))
        {
            FrTp_RxSm_IndicateAndAbort(pChannel,NTFRSLT_E_TIMEOUT_CR);
        }
        else
#endif /* FRTP_FULLDUPLEX_ENABLE */
        {
            FrTp_TxSm_IndicateAndAbort(pChannel,NTFRSLT_E_TIMEOUT_BS);
        }
    }

    DBG_FRTP_HANDLETXTIMER2_EXIT(pChannel);
}


#if (FRTP_FULLDUPLEX_ENABLE == STD_ON)
STATIC FUNC(void,FRTP_CODE) FrTp_HandleRxTimer1
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{
    boolean TimerExpiredFlag = FALSE;

    DBG_FRTP_HANDLERXTIMER1_ENTRY(pChannel);

    /* full duplex implementation */

    /* enter exclusive area */
    SchM_Enter_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    /* handle channel timer AR */
    switch(pChannel->Rx_Timer1)
    {
        case 0U: break; /* timer deactivated */
        case 1U:
        {
            FrTp_StopTimer(pChannel->Rx_Timer1);
            TimerExpiredFlag = TRUE;
            break; /* timer expired */
        }
        default:
        {
            pChannel->Rx_Timer1--;
            break; /* timer still running */
        }
    }

    /* exit exclusive area */
    SchM_Exit_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    if( TimerExpiredFlag == TRUE )
    {
        (void)FrTp_FrIfCancelTransmit(pChannel);
        FrTp_RxSm_IndicateAndAbort(pChannel,NTFRSLT_E_TIMEOUT_A);
    }

    DBG_FRTP_HANDLERXTIMER1_EXIT(pChannel);
}


STATIC FUNC(void,FRTP_CODE) FrTp_HandleRxTimer2
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{
    boolean TimerExpiredFlag = FALSE;

    DBG_FRTP_HANDLERXTIMER2_ENTRY(pChannel);

    /* enter exclusive area */
    SchM_Enter_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    /* handle channel timer CR */
    switch(pChannel->Rx_Timer2)
    {
        case 0U: break; /* timer deactivated */
        case 1U:
        {
            FrTp_StopTimer(pChannel->Rx_Timer2);
            TimerExpiredFlag = TRUE;
            break;
        }
        default:
        {
            pChannel->Rx_Timer2--;
            break; /* timer still running */
        }
    }

    /* exit exclusive area */
    SchM_Exit_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    if( TimerExpiredFlag == TRUE )
    {
        FrTp_RxSm_IndicateAndAbort(pChannel,NTFRSLT_E_TIMEOUT_CR);
    }

    DBG_FRTP_HANDLERXTIMER2_EXIT(pChannel);
}
#endif /* FRTP_FULLDUPLEX_ENABLE */


STATIC FUNC(void,FRTP_CODE) FrTp_HandleRxTimer3
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{
    /* enter exclusive area */

    DBG_FRTP_HANDLERXTIMER3_ENTRY(pChannel);
    SchM_Enter_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    /* handle channel timer BR */
    switch(pChannel->Rx_Timer3)
    {
        case 0U: break; /* timer deactivated */
        case 1U:
        {
            FrTp_StopTimer(pChannel->Rx_Timer3);
            break;
        }
        default:
        {
            pChannel->Rx_Timer3--;
            break; /* timer still running */
        }
    }

    /* exit exclusive area */
    SchM_Exit_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    DBG_FRTP_HANDLERXTIMER3_EXIT(pChannel);
}


STATIC FUNC(P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR),FRTP_CODE)
    FrTp_FindTxChannel(VAR(PduIdType,AUTOMATIC) TxPduId)
{
    /* channel iterator variable */
    uint8_least iChannel;

    P2VAR(FrTp_ChannelType, AUTOMATIC, FRTP_VAR) pChannelCandidate = NULL_PTR;

    DBG_FRTP_FINDTXCHANNEL_ENTRY(TxPduId);

    /* iterate over all channels */
    for(iChannel = 0U; iChannel < FRTP_CHAN_NUM; iChannel++)
    {
        /* get pointer to right channel structure */
        CONSTP2VAR(FrTp_ChannelType, AUTOMATIC, FRTP_VAR) pChannel = &FrTp_Channel[iChannel];

        /* get connection associated with this channel */
        CONST(uint8,AUTOMATIC) ConnectionIdx = pChannel->ConnectionIdx;

        /* check if already a channel is assigned for this TxPduId */
        if(ConnectionIdx == (uint8)TxPduId)
        {
            /* yes it is - now check if tx is still idle */
            if(pChannel->Tx_State != FRTP_CHNL_TX_IDLE)
            {
                /* abort further transmission procedure */
                pChannelCandidate = NULL_PTR;
            }
            else
            {
                /* save candidate index */
                pChannelCandidate = pChannel;

                /* abort search, since we must use this channel */
            }
            break;
        }
        /* if this is an unused channel, save as candidate for usage */
        else if((pChannelCandidate == NULL_PTR) && (ConnectionIdx == FRTP_CONNECTION_INVALID))
        {
            /* save free channel as candidate for allocation */
            pChannelCandidate = pChannel;
        }
        else
        {
            /* continue channel search */
        }
    }

    if(pChannelCandidate != NULL_PTR)
    {
        /* allocate this channel structure */
        pChannelCandidate->ConnectionIdx = (uint8)TxPduId;
    }

    /* return index of found/allocated channel */

    DBG_FRTP_FINDTXCHANNEL_EXIT(pChannelCandidate,TxPduId);
    return pChannelCandidate;
}

/**
 * \brief Resets all state variables for the given channel.
 */
STATIC FUNC(void,FRTP_CODE) FrTp_ResetChannel
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel
)
{
    /* enter exclusive area */

    DBG_FRTP_RESETCHANNEL_ENTRY(pChannel);
    SchM_Enter_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    FrTp_StopTimer((pChannel)->Tx_Timer1);
    FrTp_StopTimer((pChannel)->Tx_Timer2);
    (pChannel)->Tx_State = FRTP_CHNL_TX_IDLE;
#if (FRTP_FULLDUPLEX_ENABLE == STD_ON)
    FrTp_StopTimer((pChannel)->Rx_Timer1);
    FrTp_StopTimer((pChannel)->Rx_Timer2);
    (pChannel)->Rx_State = FRTP_CHNL_RX_IDLE;
#endif /* FRTP_FULLDUPLEX_ENABLE */
    (pChannel)->ConnectionIdx = FRTP_CONNECTION_INVALID;

    /* exit exclusive area */
    SchM_Exit_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

    DBG_FRTP_RESETCHANNEL_EXIT(pChannel);
}

/******************************************************************************
**                      Function Definitions                                 **
******************************************************************************/

FUNC( Std_ReturnType, FRTP_CODE) FrTp_IsValidConfig
(
        P2CONST(void,AUTOMATIC,FRTP_APPL_CONST) voidConfigPtr
)
{
    /* Assume an invalid configuration */
    Std_ReturnType RetVal = E_NOT_OK;
    P2CONST(FrTp_ConfigType,AUTOMATIC,FRTP_APPL_CONST) ConfigPtr = voidConfigPtr;

    DBG_FRTP_ISVALIDCONFIG_ENTRY(voidConfigPtr);

    if (ConfigPtr != NULL_PTR)
    {
        /* Check if the configuration fits to the platform */
        if (TS_PlatformSigIsValid(ConfigPtr->PlatformSignature))
        {
            /* Validate the post build configuration against the compile time configuration */
            if (FRTP_CFG_SIGNATURE == ConfigPtr->CfgSignature)
            {
                /* Validate the post build configuration against the link time configuration */
                if (FrTp_LcfgSignature == ConfigPtr->LcfgSignature)
                {
                    /* Validate the Published information in post build configuration against the
                       Published information in compile time configuration*/
                    if(FRTP_PUBLIC_INFO_SIGNATURE == ConfigPtr->PublicInfoSignature)
                    {
                        /* Indicate that the configuration is valid */
                        RetVal = E_OK;
                    }
                }
            }
        }
    }

    DBG_FRTP_ISVALIDCONFIG_EXIT(RetVal,voidConfigPtr);
    return RetVal;
}

FUNC(void,FRTP_CODE) FrTp_Init
(
    P2CONST(FrTp_ConfigType,AUTOMATIC,FRTP_APPL_CONST) PBCfgPtr
)
{
    P2CONST(FrTp_ConfigType,AUTOMATIC,FRTP_APPL_CONST) LocalConfigPtr = PBCfgPtr;

    DBG_FRTP_INIT_ENTRY(PBCfgPtr);

    /* Save the configuration pointer */
#if (FRTP_PBCFGM_SUPPORT_ENABLED == STD_ON)
    /* If the initialization function is called with a null pointer get the configuration from the
     * post build configuration manager */
    if (LocalConfigPtr == NULL_PTR)
    {
        PbcfgM_ModuleConfigPtrType ModuleConfig = NULL_PTR;
        if (E_OK == PbcfgM_GetConfig(
              FRTP_MODULE_ID,
              FRTP_DET_GENERIC_INSTANCE_ID,
              &ModuleConfig))
        {
            LocalConfigPtr = (P2CONST(FrTp_ConfigType, AUTOMATIC, FRTP_APPL_CONST)) ModuleConfig;
        }
    }
#endif /* FRTP_PBCFGM_SUPPORT_ENABLED == STD_OFF */

/* check if development error detection is enabled */
#if (FRTP_DEV_ERROR_DETECT == STD_ON)

    /* check whether parameter FrTp_RootPtr is a NULL_PTR */
    if(LocalConfigPtr == NULL_PTR)
    {
        /* found a NULL_PTR, report to DET */
        (void)Det_ReportError(FRTP_MODULE_ID,
                              FRTP_DET_GENERIC_INSTANCE_ID,
                              FRTP_INIT_SERVICE_ID,
                              FRTP_E_NULL_PTR
                             );
    }
    /* check that configuration pointer is valid */
    else if ( E_OK != FrTp_IsValidConfig(LocalConfigPtr))
    {
        (void)Det_ReportError(FRTP_MODULE_ID,
                              FRTP_DET_GENERIC_INSTANCE_ID,
                              FRTP_INIT_SERVICE_ID,
                              FRTP_E_INVALID_PARAMETER
                             );
    }
    else
#endif /* FRTP_DEV_ERROR_DETECT */

    /* perform functionality */
    {
        uint8_least iChannel; /* channel iterator variable */
        uint8_least iTxPdu;   /* TxPdu iterator variable */

        FrTp_RootPtr = LocalConfigPtr;

        /* loop over channels */
        for(iChannel = 0U; iChannel < FRTP_CHAN_NUM; iChannel++)
        {
            /* get pointer to right channel structure */
            CONSTP2VAR(FrTp_ChannelType, AUTOMATIC, FRTP_VAR) pChannel =
                    &FrTp_Channel[iChannel];

            /* reset channel data structure */
            FrTp_ResetChannel(pChannel);
        }

        /* loop over channels */
        for(iTxPdu = 0U; iTxPdu < FrTp_RootPtr->nNumTxPdus; iTxPdu++)
        {
            /* set all TxPdus to unused */
            FrTp_RuntimeTxPdu[iTxPdu].pChannel = NULL_PTR;
        }

        /* mark module as initialized */
        TS_AtomicAssign8(FrTp_InitStatus, TRUE);

    }

    DBG_FRTP_INIT_EXIT(PBCfgPtr);
}


FUNC(Std_ReturnType,FRTP_CODE) FrTp_Transmit
(
    VAR(PduIdType,AUTOMATIC) FrTpTxPduId,
    P2CONST(PduInfoType,AUTOMATIC,FRTP_APPL_DATA) PduInfoPtr
)
{
    /* variable holding function status code */
    Std_ReturnType RetCode = E_NOT_OK;

    DBG_FRTP_TRANSMIT_ENTRY(FrTpTxPduId,PduInfoPtr);

/* check if development error detection is enabled */
#if (FRTP_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (FrTp_InitStatus == FALSE)
    {
        (void)Det_ReportError(FRTP_MODULE_ID,
                              FRTP_DET_GENERIC_INSTANCE_ID,
                              FRTP_TRANSMIT_SERVICE_ID,
                              FRTP_E_UNINIT
                             );
    }
    /* check whether that LPduIdx is supported */
    else if(FrTpTxPduId >= FrTp_RootPtr->nNumTxConnections)
    {
        /* passed an invalid FrTpTxPduId, report to DET */
        (void)Det_ReportError(FRTP_MODULE_ID,
                              FRTP_DET_GENERIC_INSTANCE_ID,
                              FRTP_TRANSMIT_SERVICE_ID,
                              FRTP_E_INVALID_PDU_SDU_ID
                             );
    }
    /* check whether parameter is a NULL_PTR */
    else if(PduInfoPtr == NULL_PTR)
    {
        /* found a NULL_PTR, report to DET */
        (void)Det_ReportError(FRTP_MODULE_ID,
                              FRTP_DET_GENERIC_INSTANCE_ID,
                              FRTP_TRANSMIT_SERVICE_ID,
                              FRTP_E_NULL_PTR
                             );
    }
    else
#endif /* FRTP_DEV_ERROR_DETECT */

    {

#if (FRTP_UNKNOWN_MSG_LENGTH == STD_OFF)

        /* we detected an unknown SDU length request */
        if(PduInfoPtr->SduLength == 0U)
        {

/* check if development error detection is enabled */
#if (FRTP_DEV_ERROR_DETECT == STD_ON)

            /* make a DET call */
            (void)Det_ReportError(FRTP_MODULE_ID,
                                  FRTP_DET_GENERIC_INSTANCE_ID,
                                  FRTP_TRANSMIT_SERVICE_ID,
                                  FRTP_E_UMSG_LENGTH_ERROR
                                 );

#endif /*FRTP_DEV_ERROR_DETECT */
        }
        else
#endif

        {
            P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel;

            /* enter exclusive area */
            SchM_Enter_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

            /* get pointer to right channel structure */
            pChannel = FrTp_FindTxChannel(FrTpTxPduId);

            /* exit exclusive area */
            SchM_Exit_FrTp_SCHM_FRTP_EXCLUSIVE_AREA_0();

            /* no free channel resource available */
            if(pChannel == NULL_PTR)
            {
/* check if development error detection is enabled */
#if (FRTP_DEV_ERROR_DETECT == STD_ON)

                /* make a DET call */
                (void)Det_ReportError(FRTP_MODULE_ID,
                                      FRTP_DET_GENERIC_INSTANCE_ID,
                                      FRTP_TRANSMIT_SERVICE_ID,
                                      FRTP_E_NO_CHANNEL
                                     );

#endif /* FRTP_DEV_ERROR_DETECT */
            }
            else
            {
                /* trigger Tx-state machine to start transmission procedure */
                /* might fail in case: */
                /* -) functional addressing (no segmentation) cannot
                      transmit the requested message length */
                RetCode = FrTp_TxSm_RequestTransmission(pChannel,PduInfoPtr->SduLength);
            }
         }
    }
    /* return status value */

    DBG_FRTP_TRANSMIT_EXIT(RetCode,FrTpTxPduId,PduInfoPtr);
    return RetCode;
}





#if (FRTP_HAVE_TC == STD_ON)
FUNC(Std_ReturnType,FRTP_CODE) FrTp_CancelTransmit
(
    VAR(PduIdType,AUTOMATIC) FrTpTxPduId
)
{
    /* variable holding function status code */
    Std_ReturnType RetCode = E_NOT_OK;

    DBG_FRTP_CANCELTRANSMIT_ENTRY(FrTpTxPduId);

    TS_PARAM_UNUSED(FrTpTxPduId);

/* check if development error detection is enabled */
#if (FRTP_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (FrTp_InitStatus == FALSE)
    {
        (void)Det_ReportError(FRTP_MODULE_ID,
                              FRTP_DET_GENERIC_INSTANCE_ID,
                              FRTP_CANCELTRANSMIT_SERVICE_ID,
                              FRTP_E_UNINIT
                             );
        RetCode = E_NOT_OK;
    }
    else
#endif /* FRTP_DEV_ERROR_DETECT */
    {
        /* FrTp_CancelTransmit functionality is not supported right now */
        /* Transmission is not cancelled. */
        RetCode = E_NOT_OK;
    }

    DBG_FRTP_CANCELTRANSMIT_EXIT(RetCode,FrTpTxPduId);
    return RetCode;
}
#endif


FUNC(Std_ReturnType,FRTP_CODE) FrTp_CancelReceive
(
    VAR(PduIdType,AUTOMATIC) FrTpRxSduId
)
{
    /* variable holding function status code */
    Std_ReturnType RetCode = E_NOT_OK;

    DBG_FRTP_CANCELRECEIVE_ENTRY(FrTpRxSduId);

    TS_PARAM_UNUSED(FrTpRxSduId);

/* check if development error detection is enabled */
#if (FRTP_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (FrTp_InitStatus == FALSE)
    {
        (void)Det_ReportError(FRTP_MODULE_ID,
                              FRTP_DET_GENERIC_INSTANCE_ID,
                              FRTP_CANCELRECEIVE_SERVICE_ID,
                              FRTP_E_UNINIT
                             );
        RetCode = E_NOT_OK;
    }
    else
#endif /* FRTP_DEV_ERROR_DETECT */
    {
        /* FrTp_CancelReceive functionality is not supported right now. */
        /* Reception is not cancelled. */
        RetCode = E_NOT_OK;
    }

    DBG_FRTP_CANCELRECEIVE_EXIT(RetCode,FrTpRxSduId);
    return RetCode;
}


FUNC(Std_ReturnType,FRTP_CODE) FrTp_ChangeParameter
(
    VAR(PduIdType,AUTOMATIC) FrTpTxPduId,
    VAR(TPParameterType,AUTOMATIC) parameter,
    VAR(FrTp_ParameterValueType,AUTOMATIC) FrTpParameterValue
)
{

    DBG_FRTP_CHANGEPARAMETER_ENTRY(FrTpTxPduId,parameter,FrTpParameterValue);
    TS_PARAM_UNUSED(FrTpTxPduId);
    TS_PARAM_UNUSED(parameter);
    TS_PARAM_UNUSED(FrTpParameterValue);

/* check if development error detection is enabled */
#if (FRTP_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (FrTp_InitStatus == FALSE)
    {
        (void)Det_ReportError(FRTP_MODULE_ID,
                              FRTP_DET_GENERIC_INSTANCE_ID,
                              FRTP_CHANGEPARAMETER_SERVICE_ID,
                              FRTP_E_UNINIT
                             );
    }
#endif /* FRTP_DEV_ERROR_DETECT */

    /* FrTp_ChangeParameter functionality is not supported right now. */

    DBG_FRTP_CHANGEPARAMETER_EXIT(E_NOT_OK,FrTpTxPduId,parameter,FrTpParameterValue);
    return E_NOT_OK;
}


FUNC(void,FRTP_CODE) FrTp_MainFunction(void)
{
    /* check for successfully initialized module */

    DBG_FRTP_MAINFUNCTION_ENTRY();
    if (FrTp_InitStatus == TRUE)
    {
        uint8_least iChannel;                           /* iterator variable for channels */
        P2VAR(FrTp_ChannelType, AUTOMATIC, FRTP_VAR) ActiveChannelTable[FRTP_CHAN_NUM];
        uint8_least ActiveChannelCount = 0U;

        /* loop over each channel and */
        /* a) forward timers */
        /* b) call the receive statemachine transmission routine */
        /* Flow control transmission (from receiver) are preferred over data transmissions   */
        /* we do this just once, since receiver send only one FC frame per cycle per channel */
        for(iChannel = 0U; iChannel < FRTP_CHAN_NUM; iChannel++)
        {
            /* get pointer to right channel structure */
            CONSTP2VAR(FrTp_ChannelType, AUTOMATIC, FRTP_VAR) pChannel = &FrTp_Channel[iChannel];

            FrTp_HandleTimers(pChannel);
            FrTp_RxSm_TransmitSyncPoint(pChannel);
        }

        /* Now iterate over channels as long as there are transmit requests for this cycle
        available. Transmit requests are active as long as data is available AND an TxPdus
        are still available for this channel. Indicate that this is the first transmission
        opportunity in this cycle. */
        for(iChannel = 0U; iChannel < FRTP_CHAN_NUM; iChannel++)
        {
            /* get pointer to right channel structure */
            CONSTP2VAR(FrTp_ChannelType, AUTOMATIC, FRTP_VAR) pChannel = &FrTp_Channel[iChannel];

            /* store whether another transmission trigger would be needed by this channel */
            const boolean FurtherTransmitRequest = FrTp_TxSm_TransmitSyncPoint(pChannel,TRUE);

            /* track channels that need further transmission triggers */
            if(FurtherTransmitRequest)
            {
                ActiveChannelTable[ActiveChannelCount] = pChannel;
                ActiveChannelCount++;
            }
        }

        /* Now iterate over channels as long as there are transmit requests
           for this cycle available. Transmit requests are active as long as
           data is available AND an TxPdus are still available for this channel.
           Indicate that this is not the first transmission opportunity in this cycle. */
        while(ActiveChannelCount != 0U)
        {
            /* store number of currently recorded channels active for transmission */
            const uint8_least ActiveChannelCountOld = ActiveChannelCount;
            uint8_least iActiveChannel;

            /* assume no more transmissions are active */
            ActiveChannelCount = 0U;

            /* iterate over all channels */
            for(iActiveChannel = 0U; iActiveChannel < ActiveChannelCountOld; iActiveChannel++)
            {
                /* store index of active Tx channel */
                /* Deviation MISRA-1 <START> */
                CONSTP2VAR(FrTp_ChannelType, AUTOMATIC, FRTP_VAR) pChannel =
                        ActiveChannelTable[iActiveChannel];
                /* Deviation MISRA-1 <STOP>*/

                /* store whether another transmission trigger would be needed by this channel */
                const boolean FurtherTransmitRequest =
                    FrTp_TxSm_TransmitSyncPoint(pChannel,FALSE);

                /* track channels that need further transmission triggers */
                if(FurtherTransmitRequest)
                {
                    /* store index of still active tx channel */
                    ActiveChannelTable[ActiveChannelCount] = pChannel;
                    ActiveChannelCount++;
                }
            }
        }
    }

    DBG_FRTP_MAINFUNCTION_EXIT();
}


FUNC(uint8,FRTP_CODE) FrTp_GetFreeTxPdu
(
    P2VAR(FrTp_ChannelType,AUTOMATIC,FRTP_VAR) pChannel,
    VAR(uint16,AUTOMATIC) minLength
)
{
    uint8 iTxPdu; /* Pdu iterator */
    uint8 CandidateIdx = FRTP_TXPDU_INVALID;
    CONSTP2CONST(FrTp_ConnectionType,AUTOMATIC,FRTP_APPL_CONST) pConnection =
            FRTP_CFG_GET_PTR_TO_CONNECTION(pChannel->ConnectionIdx);

    DBG_FRTP_GETFREETXPDU_ENTRY(pChannel,minLength);

    if(((pConnection->TpOptions & FRTP_CONNECTION_HASBCLIMIT) != 0U) &&
       (((pChannel->Rx_State & FRTP_CHNL_STATE_DIRECTION_RX) != 0U) ||
        ((pChannel->Tx_CtrDataAck == 0U) && (pChannel->Tx_State == FRTP_CHNL_TX_DATA))
       )
      )
    {
      if(((pChannel->Rx_State & FRTP_CHNL_STATE_DIRECTION_RX) != 0U) &&
         (FrTp_RuntimeTxPdu[pConnection->TxPduStartIdx].pChannel == NULL_PTR)
        )
      {
        CandidateIdx = pConnection->TxPduStartIdx;
      }
      else if((pChannel->Tx_CtrDataAck == 0U) && (pChannel->Tx_State == FRTP_CHNL_TX_DATA))
      {
        /* try to find first largest free tx Pdu */
        /* this loop is exited normally only if there are no PDUs of sufficient size configured. */
        for(iTxPdu = pConnection->TxPduStartIdx; iTxPdu <= pConnection->TxPduEndIdx; iTxPdu++)
        {
          /* get TxPdu information */
          CONSTP2CONST(FrTp_TxPduConfigType, AUTOMATIC, FRTP_APPL_CONST) pTxPdu =
                  FRTP_CFG_GET_PTR_TO_TXPDU(iTxPdu);
          if(pTxPdu->PduLength >= minLength)
          {
            if(FrTp_RuntimeTxPdu[iTxPdu].pChannel == NULL_PTR)
            {
               CandidateIdx = iTxPdu;
            }
            break;
          }
        }
      }
      /* CHECK: NOPARSE */
      else
      {
        /* unreachable */
      }
      /* CHECK: PARSE */
    }
    else
    {
        /* try to find a free tx Pdu (in ascending order!!!) */
        for(iTxPdu = pConnection->TxPduStartIdx; iTxPdu <= pConnection->TxPduEndIdx; iTxPdu++)
        {
            /* is there an unused TxPdu ?? */
            if(FrTp_RuntimeTxPdu[iTxPdu].pChannel == NULL_PTR)
            {
                /* get TxPdu information */
                CONSTP2CONST(FrTp_TxPduConfigType, AUTOMATIC, FRTP_APPL_CONST) pTxPdu =
                        FRTP_CFG_GET_PTR_TO_TXPDU(iTxPdu);

                /* get the length of the Pdu into a local variable */
                const uint32 PduLength = pTxPdu->PduLength;

                /* save the first found candidate PDU */
                if (PduLength >= minLength)
                {
                    CandidateIdx = iTxPdu;
                    break;
                }
            }
        }
    }

    /* return first free TxPdu in this pool */

    DBG_FRTP_GETFREETXPDU_EXIT(CandidateIdx,pChannel,minLength);
    return CandidateIdx;
}

/*
 * Stop code section declaration
 */
#define FRTP_STOP_SEC_CODE
#include <MemMap.h>
