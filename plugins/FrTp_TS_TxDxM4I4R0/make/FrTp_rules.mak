# \file
#
# \brief AUTOSAR FrTp
#
# This file contains the implementation of the AUTOSAR
# module FrTp.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS


#################################################################
# REGISTRY

LIBRARIES_TO_BUILD   += FrTp_src

FrTp_lib_FILES +=


ifndef TS_MERGED_COMPILE
TS_MERGED_COMPILE := TRUE
endif

ifndef TS_FRTP_MERGED_COMPILE
TS_FRTP_MERGED_COMPILE := $(TS_MERGED_COMPILE)
endif

ifndef TS_FRTP_COMPILE_WITH_POSTBUILD
TS_FRTP_COMPILE_WITH_POSTBUILD := TRUE
endif

ifndef TS_BUILD_POST_BUILD_BINARY
TS_BUILD_POST_BUILD_BINARY := FALSE
endif

ifeq ($(TS_FRTP_MERGED_COMPILE),TRUE)

FrTp_src_FILES += \
    $(FrTp_CORE_PATH)\src\FrTp_Merged.c \
    $(FrTp_OUTPUT_PATH)\src\FrTp_Lcfg.c

else

FrTp_src_FILES += \
    $(FrTp_CORE_PATH)\src\FrTp.c \
    $(FrTp_CORE_PATH)\src\FrTp_LL.c \
    $(FrTp_CORE_PATH)\src\FrTp_TxStateMachine.c \
    $(FrTp_CORE_PATH)\src\FrTp_RxStateMachine.c \
    $(FrTp_CORE_PATH)\src\FrTp_GetVersionInfo.c \
    $(FrTp_OUTPUT_PATH)\src\FrTp_Lcfg.c

endif

ifeq ($(TS_FRTP_COMPILE_WITH_POSTBUILD),TRUE)
# Compile with postbuild
FrTp_src_FILES += $(FrTp_OUTPUT_PATH)\src\FrTp_PBcfg.c
endif

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
FrTp_src_FILES :=
endif

# Fill the list with post build configuration files needed to build the post build binary.
FrTp_pbconfig_FILES := $(FrTp_OUTPUT_PATH)/src/FrTp_PBcfg.c

define defineFrTpLibOutputPATH
$(1)_OBJ_OUTPUT_PATH    := $(FrTp_lib_LIB_OUTPUT_PATH)
endef
$(foreach SRC,$(basename $(notdir $(subst \,/,$(FrTp_lib_FILES)))),$(eval $(call defineFrTpLibOutputPATH,$(SRC))))

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES
