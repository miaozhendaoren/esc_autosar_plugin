
#ifndef GTM_IRQ_H
#define GTM_IRQ_H

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Std_Types.h"                                                        

#ifdef OS_KERNEL_TYPE                                                           
#include <Os.h>        /* OS interrupt services */                              
#endif


/*******************************************************************************
**                      Public Macro Definitions                              **
*******************************************************************************/

/************************* interrupt CATEGORY *********************************/

#ifdef IRQ_CAT1
#if (IRQ_CAT1 != 1)
#error IRQ_CAT1 already defined with a wrong value
#endif
#else
#define IRQ_CAT1                    (1)
#endif

#ifdef IRQ_CAT23
#if (IRQ_CAT23 != 2)
#error IRQ_CAT23 already defined with a wrong value
#endif
#else
#define IRQ_CAT23                    (2)
#endif



/* The name of the ISRs shall be the same name than the ISR       *
 * functions provided by Infineon                                 */

/*************************GTMTOM0SR0_ISR*********************************/          

#ifdef GTMTOM0SR0_ISR
#define IRQ_GTM_TOM0SR0_EXIST      STD_ON
#define IRQ_GTM_TOM0_SR0_PRIO      GTMTOM0SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TOM0_SR0_CAT       GTMTOM0SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM0SR0_EXIST      STD_OFF
#endif

/*************************GTMTOM0SR1_ISR*********************************/          

#ifdef GTMTOM0SR1_ISR
#define IRQ_GTM_TOM0SR1_EXIST      STD_ON
#define IRQ_GTM_TOM0_SR1_PRIO      GTMTOM0SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TOM0_SR1_CAT       GTMTOM0SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM0SR1_EXIST      STD_OFF
#endif

/*************************GTMTOM0SR2_ISR*********************************/          

#ifdef GTMTOM0SR2_ISR
#define IRQ_GTM_TOM0SR2_EXIST      STD_ON
#define IRQ_GTM_TOM0_SR2_PRIO      GTMTOM0SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TOM0_SR2_CAT       GTMTOM0SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM0SR2_EXIST      STD_OFF
#endif

/*************************GTMTOM0SR3_ISR*********************************/          

#ifdef GTMTOM0SR3_ISR
#define IRQ_GTM_TOM0SR3_EXIST      STD_ON
#define IRQ_GTM_TOM0_SR3_PRIO      GTMTOM0SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TOM0_SR3_CAT       GTMTOM0SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM0SR3_EXIST      STD_OFF
#endif

/*************************GTMTOM0SR4_ISR*********************************/          

#ifdef GTMTOM0SR4_ISR
#define IRQ_GTM_TOM0SR4_EXIST      STD_ON
#define IRQ_GTM_TOM0_SR4_PRIO      GTMTOM0SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TOM0_SR4_CAT       GTMTOM0SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM0SR4_EXIST      STD_OFF
#endif

/*************************GTMTOM0SR5_ISR*********************************/          

#ifdef GTMTOM0SR5_ISR
#define IRQ_GTM_TOM0SR5_EXIST      STD_ON
#define IRQ_GTM_TOM0_SR5_PRIO      GTMTOM0SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TOM0_SR5_CAT       GTMTOM0SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM0SR5_EXIST      STD_OFF
#endif

/*************************GTMTOM0SR6_ISR*********************************/          

#ifdef GTMTOM0SR6_ISR
#define IRQ_GTM_TOM0SR6_EXIST      STD_ON
#define IRQ_GTM_TOM0_SR6_PRIO      GTMTOM0SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TOM0_SR6_CAT       GTMTOM0SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM0SR6_EXIST      STD_OFF
#endif

/*************************GTMTOM0SR7_ISR*********************************/          

#ifdef GTMTOM0SR7_ISR
#define IRQ_GTM_TOM0SR7_EXIST      STD_ON
#define IRQ_GTM_TOM0_SR7_PRIO      GTMTOM0SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TOM0_SR7_CAT       GTMTOM0SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM0SR7_EXIST      STD_OFF
#endif


/*************************GTMTOM1SR0_ISR*********************************/          

#ifdef GTMTOM1SR0_ISR
#define IRQ_GTM_TOM1SR0_EXIST      STD_ON
#define IRQ_GTM_TOM1_SR0_PRIO      GTMTOM1SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TOM1_SR0_CAT       GTMTOM1SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM1SR0_EXIST      STD_OFF
#endif

/*************************GTMTOM1SR1_ISR*********************************/          

#ifdef GTMTOM1SR1_ISR
#define IRQ_GTM_TOM1SR1_EXIST      STD_ON
#define IRQ_GTM_TOM1_SR1_PRIO      GTMTOM1SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TOM1_SR1_CAT       GTMTOM1SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM1SR1_EXIST      STD_OFF
#endif

/*************************GTMTOM1SR2_ISR*********************************/          

#ifdef GTMTOM1SR2_ISR
#define IRQ_GTM_TOM1SR2_EXIST      STD_ON
#define IRQ_GTM_TOM1_SR2_PRIO      GTMTOM1SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TOM1_SR2_CAT       GTMTOM1SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM1SR2_EXIST      STD_OFF
#endif

/*************************GTMTOM1SR3_ISR*********************************/          

#ifdef GTMTOM1SR3_ISR
#define IRQ_GTM_TOM1SR3_EXIST      STD_ON
#define IRQ_GTM_TOM1_SR3_PRIO      GTMTOM1SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TOM1_SR3_CAT       GTMTOM1SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM1SR3_EXIST      STD_OFF
#endif

/*************************GTMTOM1SR4_ISR*********************************/          

#ifdef GTMTOM1SR4_ISR
#define IRQ_GTM_TOM1SR4_EXIST      STD_ON
#define IRQ_GTM_TOM1_SR4_PRIO      GTMTOM1SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TOM1_SR4_CAT       GTMTOM1SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM1SR4_EXIST      STD_OFF
#endif

/*************************GTMTOM1SR5_ISR*********************************/          

#ifdef GTMTOM1SR5_ISR
#define IRQ_GTM_TOM1SR5_EXIST      STD_ON
#define IRQ_GTM_TOM1_SR5_PRIO      GTMTOM1SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TOM1_SR5_CAT       GTMTOM1SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM1SR5_EXIST      STD_OFF
#endif

/*************************GTMTOM1SR6_ISR*********************************/          

#ifdef GTMTOM1SR6_ISR
#define IRQ_GTM_TOM1SR6_EXIST      STD_ON
#define IRQ_GTM_TOM1_SR6_PRIO      GTMTOM1SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TOM1_SR6_CAT       GTMTOM1SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM1SR6_EXIST      STD_OFF
#endif

/*************************GTMTOM1SR7_ISR*********************************/          

#ifdef GTMTOM1SR7_ISR
#define IRQ_GTM_TOM1SR7_EXIST      STD_ON
#define IRQ_GTM_TOM1_SR7_PRIO      GTMTOM1SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TOM1_SR7_CAT       GTMTOM1SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM1SR7_EXIST      STD_OFF
#endif

/*************************GTMTOM2SR0_ISR*********************************/          

#ifdef GTMTOM2SR0_ISR
#define IRQ_GTM_TOM2SR0_EXIST      STD_ON
#define IRQ_GTM_TOM2_SR0_PRIO      GTMTOM2SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TOM2_SR0_CAT       GTMTOM2SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM2SR0_EXIST      STD_OFF
#endif

/*************************GTMTOM2SR1_ISR*********************************/          

#ifdef GTMTOM2SR1_ISR 
#define IRQ_GTM_TOM2SR1_EXIST      STD_ON
#define IRQ_GTM_TOM2_SR1_PRIO      GTMTOM2SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TOM2_SR1_CAT       GTMTOM2SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM2SR1_EXIST      STD_OFF
#endif

/*************************GTMTOM2SR2_ISR*********************************/          

#ifdef GTMTOM2SR2_ISR
#define IRQ_GTM_TOM2SR2_EXIST      STD_ON
#define IRQ_GTM_TOM2_SR2_PRIO      GTMTOM2SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TOM2_SR2_CAT       GTMTOM2SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM2SR2_EXIST      STD_OFF
#endif

/*************************GTMTOM2SR3_ISR*********************************/          

#ifdef GTMTOM2SR3_ISR
#define IRQ_GTM_TOM2SR3_EXIST      STD_ON
#define IRQ_GTM_TOM2_SR3_PRIO      GTMTOM2SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TOM2_SR3_CAT       GTMTOM2SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM2SR3_EXIST      STD_OFF
#endif

/*************************GTMTOM2SR4_ISR*********************************/          

#ifdef GTMTOM2SR4_ISR
#define IRQ_GTM_TOM2SR4_EXIST      STD_ON
#define IRQ_GTM_TOM2_SR4_PRIO      GTMTOM2SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TOM2_SR4_CAT       GTMTOM2SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM2SR4_EXIST      STD_OFF
#endif

/*************************GTMTOM2SR5_ISR*********************************/          

#ifdef GTMTOM2SR5_ISR
#define IRQ_GTM_TOM2SR5_EXIST      STD_ON
#define IRQ_GTM_TOM2_SR5_PRIO      GTMTOM2SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TOM2_SR5_CAT       GTMTOM2SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM2SR5_EXIST      STD_OFF
#endif

/*************************GTMTOM2SR6_ISR*********************************/          

#ifdef GTMTOM2SR6_ISR
#define IRQ_GTM_TOM2SR6_EXIST      STD_ON
#define IRQ_GTM_TOM2_SR6_PRIO      GTMTOM2SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TOM2_SR6_CAT       GTMTOM2SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM2SR6_EXIST      STD_OFF
#endif

/*************************GTMTOM2SR7_ISR*********************************/          

#ifdef GTMTOM2SR7_ISR
#define IRQ_GTM_TOM2SR7_EXIST      STD_ON
#define IRQ_GTM_TOM2_SR7_PRIO      GTMTOM2SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TOM2_SR7_CAT       GTMTOM2SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM2SR7_EXIST      STD_OFF
#endif

/*************************GTMTOM3SR0_ISR*********************************/          

#ifdef GTMTOM3SR0_ISR
#define IRQ_GTM_TOM3SR0_EXIST      STD_ON
#define IRQ_GTM_TOM3_SR0_PRIO      GTMTOM3SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TOM3_SR0_CAT       GTMTOM3SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM3SR0_EXIST      STD_OFF
#endif

/*************************GTMTOM3SR1_ISR*********************************/          

#ifdef GTMTOM3SR1_ISR
#define IRQ_GTM_TOM3SR1_EXIST      STD_ON
#define IRQ_GTM_TOM3_SR1_PRIO      GTMTOM3SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TOM3_SR1_CAT       GTMTOM3SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM3SR1_EXIST      STD_OFF
#endif

/*************************GTMTOM3SR2_ISR*********************************/          

#ifdef GTMTOM3SR2_ISR
#define IRQ_GTM_TOM3SR2_EXIST      STD_ON
#define IRQ_GTM_TOM3_SR2_PRIO      GTMTOM3SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TOM3_SR2_CAT       GTMTOM3SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM3SR2_EXIST      STD_OFF
#endif

/*************************GTMTOM3SR3_ISR*********************************/          

#ifdef GTMTOM3SR3_ISR
#define IRQ_GTM_TOM3SR3_EXIST      STD_ON
#define IRQ_GTM_TOM3_SR3_PRIO      GTMTOM3SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TOM3_SR3_CAT       GTMTOM3SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM3SR3_EXIST      STD_OFF
#endif

/*************************GTMTOM3SR4_ISR*********************************/          

#ifdef GTMTOM3SR4_ISR
#define IRQ_GTM_TOM3SR4_EXIST      STD_ON
#define IRQ_GTM_TOM3_SR4_PRIO      GTMTOM3SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TOM3_SR4_CAT       GTMTOM3SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM3SR4_EXIST      STD_OFF
#endif

/*************************GTMTOM3SR5_ISR*********************************/          

#ifdef GTMTOM3SR5_ISR
#define IRQ_GTM_TOM3SR5_EXIST      STD_ON
#define IRQ_GTM_TOM3_SR5_PRIO      GTMTOM3SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TOM3_SR5_CAT       GTMTOM3SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM3SR5_EXIST      STD_OFF
#endif

/*************************GTMTOM3SR6_ISR*********************************/          

#ifdef GTMTOM3SR6_ISR
#define IRQ_GTM_TOM3SR6_EXIST      STD_ON
#define IRQ_GTM_TOM3_SR6_PRIO      GTMTOM3SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TOM3_SR6_CAT       GTMTOM3SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM3SR6_EXIST      STD_OFF
#endif

/*************************GTMTOM3SR7_ISR*********************************/          

#ifdef GTMTOM3SR7_ISR
#define IRQ_GTM_TOM3SR7_EXIST      STD_ON
#define IRQ_GTM_TOM3_SR7_PRIO      GTMTOM3SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TOM3_SR7_CAT       GTMTOM3SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM3SR7_EXIST      STD_OFF
#endif

/*************************GTMTOM4SR0_ISR*********************************/          

#ifdef GTMTOM4SR0_ISR
#define IRQ_GTM_TOM4SR0_EXIST      STD_ON
#define IRQ_GTM_TOM4_SR0_PRIO      GTMTOM4SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TOM4_SR0_CAT       GTMTOM4SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM4SR0_EXIST      STD_OFF
#endif

/*************************GTMTOM4SR1_ISR*********************************/          

#ifdef GTMTOM4SR1_ISR
#define IRQ_GTM_TOM4SR1_EXIST      STD_ON
#define IRQ_GTM_TOM4_SR1_PRIO      GTMTOM4SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TOM4_SR1_CAT       GTMTOM4SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM4SR1_EXIST      STD_OFF
#endif

/*************************GTMTOM4SR2_ISR*********************************/          

#ifdef GTMTOM4SR2_ISR
#define IRQ_GTM_TOM4SR2_EXIST      STD_ON
#define IRQ_GTM_TOM4_SR2_PRIO      GTMTOM4SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TOM4_SR2_CAT       GTMTOM4SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM4SR2_EXIST      STD_OFF
#endif

/*************************GTMTOM4SR3_ISR*********************************/          

#ifdef GTMTOM4SR3_ISR
#define IRQ_GTM_TOM4SR3_EXIST      STD_ON
#define IRQ_GTM_TOM4_SR3_PRIO      GTMTOM4SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TOM4_SR3_CAT       GTMTOM4SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM4SR3_EXIST      STD_OFF
#endif

/*************************GTMTOM4SR4_ISR*********************************/          

#ifdef GTMTOM4SR4_ISR
#define IRQ_GTM_TOM4SR4_EXIST      STD_ON
#define IRQ_GTM_TOM4_SR4_PRIO      GTMTOM4SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TOM4_SR4_CAT       GTMTOM4SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM4SR4_EXIST      STD_OFF
#endif

/*************************GTMTOM4SR5_ISR*********************************/          

#ifdef GTMTOM4SR5_ISR
#define IRQ_GTM_TOM4SR5_EXIST      STD_ON
#define IRQ_GTM_TOM4_SR5_PRIO      GTMTOM4SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TOM4_SR5_CAT       GTMTOM4SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM4SR5_EXIST      STD_OFF
#endif

/*************************GTMTOM4SR6_ISR*********************************/          

#ifdef GTMTOM4SR6_ISR
#define IRQ_GTM_TOM4SR6_EXIST      STD_ON
#define IRQ_GTM_TOM4_SR6_PRIO      GTMTOM4SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TOM4_SR6_CAT       GTMTOM4SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM4SR6_EXIST      STD_OFF
#endif

/*************************GTMTOM4SR7_ISR*********************************/          

#ifdef GTMTOM4SR7_ISR
#define IRQ_GTM_TOM4SR7_EXIST      STD_ON
#define IRQ_GTM_TOM4_SR7_PRIO      GTMTOM4SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TOM4_SR7_CAT       GTMTOM4SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TOM4SR7_EXIST      STD_OFF
#endif

/*************************GTMATOM0SR0_ISR*********************************/          

#ifdef GTMATOM0SR0_ISR
#define IRQ_GTM_ATOM0SR0_EXIST      STD_ON
#define IRQ_GTM_ATOM0_SR0_PRIO      GTMATOM0SR0_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM0_SR0_CAT       GTMATOM0SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM0SR0_EXIST      STD_OFF
#endif

/*************************GTMATOM0SR1_ISR*********************************/          

#ifdef GTMATOM0SR1_ISR
#define IRQ_GTM_ATOM0SR1_EXIST      STD_ON
#define IRQ_GTM_ATOM0_SR1_PRIO      GTMATOM0SR1_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM0_SR1_CAT       GTMATOM0SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM0SR1_EXIST      STD_OFF
#endif

/*************************GTMATOM0SR2_ISR*********************************/          

#ifdef GTMATOM0SR2_ISR
#define IRQ_GTM_ATOM0SR2_EXIST      STD_ON
#define IRQ_GTM_ATOM0_SR2_PRIO      GTMATOM0SR2_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM0_SR2_CAT       GTMATOM0SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM0SR2_EXIST      STD_OFF
#endif

/*************************GTMATOM0SR3_ISR*********************************/          

#ifdef GTMATOM0SR3_ISR
#define IRQ_GTM_ATOM0SR3_EXIST      STD_ON
#define IRQ_GTM_ATOM0_SR3_PRIO      GTMATOM0SR3_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM0_SR3_CAT       GTMATOM0SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM0SR3_EXIST      STD_OFF
#endif

/*************************GTMATOM1SR0_ISR*********************************/          

#ifdef GTMATOM1SR0_ISR
#define IRQ_GTM_ATOM1SR0_EXIST      STD_ON
#define IRQ_GTM_ATOM1_SR0_PRIO      GTMATOM1SR0_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM1_SR0_CAT       GTMATOM1SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM1SR0_EXIST      STD_OFF
#endif

/*************************GTMATOM1SR1_ISR*********************************/          

#ifdef GTMATOM1SR1_ISR
#define IRQ_GTM_ATOM1SR1_EXIST      STD_ON
#define IRQ_GTM_ATOM1_SR1_PRIO      GTMATOM1SR1_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM1_SR1_CAT       GTMATOM1SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM1SR1_EXIST      STD_OFF
#endif

/*************************GTMATOM1SR2_ISR*********************************/          

#ifdef GTMATOM1SR2_ISR
#define IRQ_GTM_ATOM1SR2_EXIST      STD_ON
#define IRQ_GTM_ATOM1_SR2_PRIO      GTMATOM1SR2_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM1_SR2_CAT       GTMATOM1SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM1SR2_EXIST      STD_OFF
#endif

/*************************GTMATOM1SR3_ISR*********************************/          

#ifdef GTMATOM1SR3_ISR
#define IRQ_GTM_ATOM1SR3_EXIST      STD_ON
#define IRQ_GTM_ATOM1_SR3_PRIO      GTMATOM1SR3_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM1_SR3_CAT       GTMATOM1SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM1SR3_EXIST      STD_OFF
#endif

/*************************GTMATOM2SR0_ISR*********************************/          

#ifdef GTMATOM2SR0_ISR
#define IRQ_GTM_ATOM2SR0_EXIST      STD_ON
#define IRQ_GTM_ATOM2_SR0_PRIO      GTMATOM2SR0_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM2_SR0_CAT       GTMATOM2SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM2SR0_EXIST      STD_OFF
#endif

/*************************GTMATOM2SR1_ISR*********************************/          

#ifdef GTMATOM2SR1_ISR
#define IRQ_GTM_ATOM2SR1_EXIST      STD_ON
#define IRQ_GTM_ATOM2_SR1_PRIO      GTMATOM2SR1_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM2_SR1_CAT       GTMATOM2SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM2SR1_EXIST      STD_OFF
#endif

/*************************GTMATOM2SR2_ISR*********************************/          

#ifdef GTMATOM2SR2_ISR
#define IRQ_GTM_ATOM2SR2_EXIST      STD_ON
#define IRQ_GTM_ATOM2_SR2_PRIO      GTMATOM2SR2_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM2_SR2_CAT       GTMATOM2SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM2SR2_EXIST      STD_OFF
#endif

/*************************GTMATOM2SR3_ISR*********************************/          

#ifdef GTMATOM2SR3_ISR
#define IRQ_GTM_ATOM2SR3_EXIST      STD_ON
#define IRQ_GTM_ATOM2_SR3_PRIO      GTMATOM2SR3_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM2_SR3_CAT       GTMATOM2SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM2SR3_EXIST      STD_OFF
#endif

/*************************GTMATOM3SR0_ISR*********************************/          

#ifdef GTMATOM3SR0_ISR
#define IRQ_GTM_ATOM3SR0_EXIST      STD_ON
#define IRQ_GTM_ATOM3_SR0_PRIO      GTMATOM3SR0_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM3_SR0_CAT       GTMATOM3SR0_ISR_ISR_CATEGOR
#else
#define IRQ_GTM_ATOM3SR0_EXIST      STD_OFF
#endif

/*************************GTMATOM3SR1_ISR*********************************/          

#ifdef GTMATOM3SR1_ISR
#define IRQ_GTM_ATOM3SR1_EXIST      STD_ON
#define IRQ_GTM_ATOM3_SR1_PRIO      GTMATOM3SR1_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM3_SR1_CAT       GTMATOM3SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM3SR1_EXIST      STD_OFF
#endif

/*************************GTMATOM3SR2_ISR*********************************/          

#ifdef GTMATOM3SR2_ISR
#define IRQ_GTM_ATOM3SR2_EXIST      STD_ON
#define IRQ_GTM_ATOM3_SR2_PRIO      GTMATOM3SR2_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM3_SR2_CAT       GTMATOM3SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM3SR2_EXIST      STD_OFF
#endif

/*************************GTMATOM3SR3_ISR*********************************/          

#ifdef GTMATOM3SR3_ISR
#define IRQ_GTM_ATOM3SR3_EXIST      STD_ON
#define IRQ_GTM_ATOM3_SR3_PRIO      GTMATOM3SR3_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM3_SR3_CAT       GTMATOM3SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM3SR3_EXIST      STD_OFF
#endif

/*************************GTMATOM4SR0_ISR*********************************/          

#ifdef GTMATOM4SR0_ISR
#define IRQ_GTM_ATOM4SR0_EXIST      STD_ON
#define IRQ_GTM_ATOM4_SR0_PRIO      GTMATOM4SR0_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM4_SR0_CAT       GTMATOM4SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM4SR0_EXIST      STD_OFF
#endif

/*************************GTMATOM4SR1_ISR*********************************/          

#ifdef GTMATOM4SR1_ISR
#define IRQ_GTM_ATOM4SR1_EXIST      STD_ON
#define IRQ_GTM_ATOM4_SR1_PRIO      GTMATOM4SR1_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM4_SR1_CAT       GTMATOM4SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM4SR1_EXIST      STD_OFF
#endif

/*************************GTMATOM4SR2_ISR*********************************/          

#ifdef GTMATOM4SR2_ISR
#define IRQ_GTM_ATOM4SR2_EXIST      STD_ON
#define IRQ_GTM_ATOM4_SR2_PRIO      GTMATOM4SR2_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM4_SR2_CAT       GTMATOM4SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM4SR2_EXIST      STD_OFF
#endif

/*************************GTMATOM4SR3_ISR*********************************/          

#ifdef GTMATOM4SR3_ISR
#define IRQ_GTM_ATOM4SR3_EXIST      STD_ON
#define IRQ_GTM_ATOM4_SR3_PRIO      GTMATOM4SR3_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM4_SR3_CAT       GTMATOM4SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM4SR3_EXIST      STD_OFF
#endif

/*************************GTMATOM5SR0_ISR*********************************/          

#ifdef GTMATOM5SR0_ISR
#define IRQ_GTM_ATOM5SR0_EXIST      STD_ON
#define IRQ_GTM_ATOM5_SR0_PRIO      GTMATOM5SR0_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM5_SR0_CAT       GTMATOM5SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM5SR0_EXIST      STD_OFF
#endif

/*************************GTMATOM5SR1_ISR*********************************/          

#ifdef GTMATOM5SR1_ISR
#define IRQ_GTM_ATOM5SR1_EXIST      STD_ON
#define IRQ_GTM_ATOM5_SR1_PRIO      GTMATOM5SR1_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM5_SR1_CAT       GTMATOM5SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM5SR1_EXIST      STD_OFF
#endif

/*************************GTMATOM5SR2_ISR*********************************/          

#ifdef GTMATOM5SR2_ISR
#define IRQ_GTM_ATOM5SR2_EXIST      STD_ON
#define IRQ_GTM_ATOM5_SR2_PRIO      GTMATOM5SR2_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM5_SR2_CAT       GTMATOM5SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM5SR2_EXIST      STD_OFF
#endif

/*************************GTMATOM5SR3_ISR*********************************/          

#ifdef GTMATOM5SR3_ISR
#define IRQ_GTM_ATOM5SR3_EXIST      STD_ON
#define IRQ_GTM_ATOM5_SR3_PRIO      GTMATOM5SR3_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM5_SR3_CAT       GTMATOM5SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM5SR3_EXIST      STD_OFF
#endif

/*************************GTMATOM6SR0_ISR*********************************/          

#ifdef GTMATOM6SR0_ISR
#define IRQ_GTM_ATOM6SR0_EXIST      STD_ON
#define IRQ_GTM_ATOM6_SR0_PRIO      GTMATOM6SR0_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM6_SR0_CAT       GTMATOM6SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM6SR0_EXIST      STD_OFF
#endif

/*************************GTMATOM6SR1_ISR*********************************/          

#ifdef GTMATOM6SR1_ISR
#define IRQ_GTM_ATOM6SR1_EXIST      STD_ON
#define IRQ_GTM_ATOM6_SR1_PRIO      GTMATOM6SR1_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM6_SR1_CAT       GTMATOM6SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM6SR1_EXIST      STD_OFF
#endif

/*************************GTMATOM6SR2_ISR*********************************/          

#ifdef GTMATOM6SR2_ISR
#define IRQ_GTM_ATOM6SR2_EXIST      STD_ON
#define IRQ_GTM_ATOM6_SR2_PRIO      GTMATOM6SR2_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM6_SR2_CAT       GTMATOM6SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM6SR2_EXIST      STD_OFF
#endif

/*************************GTMATOM6SR3_ISR*********************************/          

#ifdef GTMATOM6SR3_ISR
#define IRQ_GTM_ATOM6SR3_EXIST      STD_ON
#define IRQ_GTM_ATOM6_SR3_PRIO      GTMATOM6SR3_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM6_SR3_CAT       GTMATOM6SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM6SR3_EXIST      STD_OFF
#endif

/*************************GTMATOM7SR0_ISR*********************************/          

#ifdef GTMATOM7SR0_ISR
#define IRQ_GTM_ATOM7SR0_EXIST      STD_ON
#define IRQ_GTM_ATOM7_SR0_PRIO      GTMATOM7SR0_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM7_SR0_CAT       GTMATOM7SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM7SR0_EXIST      STD_OFF
#endif

/*************************GTMATOM7SR1_ISR*********************************/          

#ifdef GTMATOM7SR1_ISR
#define IRQ_GTM_ATOM7SR1_EXIST      STD_ON
#define IRQ_GTM_ATOM7_SR1_PRIO      GTMATOM7SR1_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM7_SR1_CAT       GTMATOM7SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM7SR1_EXIST      STD_OFF
#endif

/*************************GTMATOM7SR2_ISR*********************************/          

#ifdef GTMATOM7SR2_ISR
#define IRQ_GTM_ATOM7SR2_EXIST      STD_ON
#define IRQ_GTM_ATOM7_SR2_PRIO      GTMATOM7SR2_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM7_SR2_CAT       GTMATOM7SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM7SR2_EXIST      STD_OFF
#endif

/*************************GTMATOM7SR3_ISR*********************************/          

#ifdef GTMATOM7SR3_ISR
#define IRQ_GTM_ATOM7SR3_EXIST      STD_ON
#define IRQ_GTM_ATOM7_SR3_PRIO      GTMATOM7SR3_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM7_SR3_CAT       GTMATOM7SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM7SR3_EXIST      STD_OFF
#endif


/*************************GTMATOM8SR0_ISR*********************************/          

#ifdef GTMATOM8SR0_ISR
#define IRQ_GTM_ATOM8SR0_EXIST      STD_ON
#define IRQ_GTM_ATOM8_SR0_PRIO      GTMATOM8SR0_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM8_SR0_CAT       GTMATOM8SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM8SR0_EXIST      STD_OFF
#endif

/*************************GTMATOM8SR1_ISR*********************************/          

#ifdef GTMATOM8SR1_ISR
#define IRQ_GTM_ATOM8SR1_EXIST      STD_ON
#define IRQ_GTM_ATOM8_SR1_PRIO      GTMATOM8SR1_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM8_SR1_CAT       GTMATOM8SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM8SR1_EXIST      STD_OFF
#endif

/*************************GTMATOM8SR2_ISR*********************************/          

#ifdef GTMATOM8SR2_ISR
#define IRQ_GTM_ATOM8SR2_EXIST      STD_ON
#define IRQ_GTM_ATOM8_SR2_PRIO      GTMATOM8SR2_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM8_SR2_CAT       GTMATOM8SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM8SR2_EXIST      STD_OFF
#endif

/*************************GTMATOM8SR3_ISR*********************************/          

#ifdef GTMATOM8SR3_ISR
#define IRQ_GTM_ATOM8_SR3EXIST      STD_ON
#define IRQ_GTM_ATOM8_SR3_PRIO      GTMATOM8SR3_ISR_ISR_LEVEL
#define IRQ_GTM_ATOM8_SR3_CAT       GTMATOM8SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ATOM8_SR3EXIST      STD_OFF
#endif

/*************************GTMTIM0SR0_ISR*********************************/          

#ifdef GTMTIM0SR0_ISR
#define IRQ_GTM_TIM0SR0_EXIST      STD_ON
#define IRQ_GTM_TIM0_SR0_PRIO      GTMTIM0SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TIM0_SR0_CAT       GTMTIM0SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM0SR0_EXIST      STD_OFF
#endif

/*************************GTMTIM0SR1_ISR*********************************/          

#ifdef GTMTIM0SR1_ISR
#define IRQ_GTM_TIM0SR1_EXIST      STD_ON
#define IRQ_GTM_TIM0_SR1_PRIO      GTMTIM0SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TIM0_SR1_CAT       GTMTIM0SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM0SR1_EXIST      STD_OFF
#endif

/*************************GTMTIM0SR2_ISR*********************************/          

#ifdef GTMTIM0SR2_ISR
#define IRQ_GTM_TIM0SR2_EXIST      STD_ON
#define IRQ_GTM_TIM0_SR2_PRIO      GTMTIM0SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TIM0_SR2_CAT       GTMTIM0SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM0SR2_EXIST      STD_OFF
#endif

/*************************GTMTIM0SR3_ISR*********************************/          

#ifdef GTMTIM0SR3_ISR
#define IRQ_GTM_TIM0SR3_EXIST      STD_ON
#define IRQ_GTM_TIM0_SR3_PRIO      GTMTIM0SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TIM0_SR3_CAT       GTMTIM0SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM0SR3_EXIST      STD_OFF
#endif

/*************************GTMTIM0SR4_ISR*********************************/          

#ifdef GTMTIM0SR4_ISR
#define IRQ_GTM_TIM0SR4_EXIST      STD_ON
#define IRQ_GTM_TIM0_SR4_PRIO      GTMTIM0SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TIM0_SR4_CAT       GTMTIM0SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM0SR4_EXIST      STD_OFF
#endif

/*************************GTMTIM0SR5_ISR*********************************/          

#ifdef GTMTIM0SR5_ISR
#define IRQ_GTM_TIM0SR5_EXIST      STD_ON
#define IRQ_GTM_TIM0_SR5_PRIO      GTMTIM0SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TIM0_SR5_CAT       GTMTIM0SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM0SR5_EXIST      STD_OFF
#endif

/*************************GTMTIM0SR6_ISR*********************************/          

#ifdef GTMTIM0SR6_ISR
#define IRQ_GTM_TIM0SR6_EXIST      STD_ON
#define IRQ_GTM_TIM0_SR6_PRIO      GTMTIM0SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TIM0_SR6_CAT       GTMTIM0SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM0SR6_EXIST      STD_OFF
#endif

/*************************GTMTIM0SR7_ISR*********************************/          

#ifdef GTMTIM0SR7_ISR
#define IRQ_GTM_TIM0SR7_EXIST      STD_ON
#define IRQ_GTM_TIM0_SR7_PRIO      GTMTIM0SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TIM0_SR7_CAT       GTMTIM0SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM0SR7_EXIST      STD_OFF
#endif

/*************************GTMTIM1SR0_ISR*********************************/          

#ifdef GTMTIM1SR0_ISR
#define IRQ_GTM_TIM1SR0_EXIST      STD_ON
#define IRQ_GTM_TIM1_SR0_PRIO      GTMTIM1SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TIM1_SR0_CAT       GTMTIM1SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM1SR0_EXIST      STD_OFF
#endif

/*************************GTMTIM1SR1_ISR*********************************/          

#ifdef GTMTIM1SR1_ISR
#define IRQ_GTM_TIM1SR1_EXIST      STD_ON
#define IRQ_GTM_TIM1_SR1_PRIO      GTMTIM1SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TIM1_SR1_CAT       GTMTIM1SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM1SR1_EXIST      STD_OFF
#endif

/*************************GTMTIM1SR2_ISR*********************************/          

#ifdef GTMTIM1SR2_ISR
#define IRQ_GTM_TIM1SR2_EXIST      STD_ON
#define IRQ_GTM_TIM1_SR2_PRIO      GTMTIM1SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TIM1_SR2_CAT       GTMTIM1SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM1SR2_EXIST      STD_OFF
#endif

/*************************GTMTIM1SR3_ISR*********************************/          

#ifdef GTMTIM1SR3_ISR
#define IRQ_GTM_TIM1SR3_EXIST      STD_ON
#define IRQ_GTM_TIM1_SR3_PRIO      GTMTIM1SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TIM1_SR3_CAT       GTMTIM1SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM1SR3_EXIST      STD_OFF
#endif

/*************************GTMTIM1SR4_ISR*********************************/          

#ifdef GTMTIM1SR4_ISR
#define IRQ_GTM_TIM1SR4_EXIST      STD_ON
#define IRQ_GTM_TIM1_SR4_PRIO      GTMTIM1SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TIM1_SR4_CAT       GTMTIM1SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM1SR4_EXIST      STD_OFF
#endif

/*************************GTMTIM1SR5_ISR*********************************/          

#ifdef GTMTIM1SR5_ISR
#define IRQ_GTM_TIM1SR5_EXIST      STD_ON
#define IRQ_GTM_TIM1_SR5_PRIO      GTMTIM1SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TIM1_SR5_CAT       GTMTIM1SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM1SR5_EXIST      STD_OFF
#endif

/*************************GTMTIM1SR6_ISR*********************************/          

#ifdef GTMTIM1SR6_ISR
#define IRQ_GTM_TIM1SR6_EXIST      STD_ON
#define IRQ_GTM_TIM1_SR6_PRIO      GTMTIM1SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TIM1_SR6_CAT       GTMTIM1SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM1SR6_EXIST      STD_OFF
#endif

/*************************GTMTIM1SR7_ISR*********************************/          

#ifdef GTMTIM1SR7_ISR
#define IRQ_GTM_TIM1SR7_EXIST      STD_ON
#define IRQ_GTM_TIM1_SR7_PRIO      GTMTIM1SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TIM1_SR7_CAT       GTMTIM1SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM1SR7_EXIST      STD_OFF
#endif


/*************************GTMTIM2SR0_ISR*********************************/          

#ifdef GTMTIM2SR0_ISR
#define IRQ_GTM_TIM2SR0_EXIST      STD_ON
#define IRQ_GTM_TIM2_SR0_PRIO      GTMTIM2SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TIM2_SR0_CAT       GTMTIM2SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM2SR0_EXIST      STD_OFF
#endif

/*************************GTMTIM2SR1_ISR*********************************/          

#ifdef GTMTIM2SR1_ISR
#define IRQ_GTM_TIM2SR1_EXIST      STD_ON
#define IRQ_GTM_TIM2_SR1_PRIO      GTMTIM2SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TIM2_SR1_CAT       GTMTIM2SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM2SR1_EXIST      STD_OFF
#endif

/*************************GTMTIM2SR2_ISR*********************************/          

#ifdef GTMTIM2SR2_ISR
#define IRQ_GTM_TIM2SR2_EXIST      STD_ON
#define IRQ_GTM_TIM2_SR2_PRIO      GTMTIM2SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TIM2_SR2_CAT       GTMTIM2SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM2SR2_EXIST      STD_OFF
#endif

/*************************GTMTIM2SR3_ISR*********************************/          

#ifdef GTMTIM2SR3_ISR
#define IRQ_GTM_TIM2SR3_EXIST      STD_ON
#define IRQ_GTM_TIM2_SR3_PRIO      GTMTIM2SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TIM2_SR3_CAT       GTMTIM2SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM2SR3_EXIST      STD_OFF
#endif

/*************************GTMTIM2SR4_ISR*********************************/          

#ifdef GTMTIM2SR4_ISR
#define IRQ_GTM_TIM2SR4_EXIST      STD_ON
#define IRQ_GTM_TIM2_SR4_PRIO      GTMTIM2SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TIM2_SR4_CAT       GTMTIM2SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM2SR4_EXIST      STD_OFF
#endif

/*************************GTMTIM2SR5_ISR*********************************/          

#ifdef GTMTIM2SR5_ISR
#define IRQ_GTM_TIM2SR5_EXIST      STD_ON
#define IRQ_GTM_TIM2_SR5_PRIO      GTMTIM2SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TIM2_SR5_CAT       GTMTIM2SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM2SR5_EXIST      STD_OFF
#endif

/*************************GTMTIM2SR6_ISR*********************************/          

#ifdef GTMTIM2SR6_ISR
#define IRQ_GTM_TIM2SR6_EXIST      STD_ON
#define IRQ_GTM_TIM2_SR6_PRIO      GTMTIM2SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TIM2_SR6_CAT       GTMTIM2SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM2SR6_EXIST      STD_OFF
#endif

/*************************GTMTIM2SR7_ISR*********************************/          

#ifdef GTMTIM2SR7_ISR
#define IRQ_GTM_TIM2SR7_EXIST      STD_ON
#define IRQ_GTM_TIM2_SR7_PRIO      GTMTIM2SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TIM2_SR7_CAT       GTMTIM2SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM2SR7_EXIST      STD_OFF
#endif

/*************************GTMTIM3SR0_ISR*********************************/          

#ifdef GTMTIM3SR0_ISR
#define IRQ_GTM_TIM3SR0_EXIST      STD_ON
#define IRQ_GTM_TIM3_SR0_PRIO      GTMTIM3SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TIM3_SR0_CAT       GTMTIM3SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM3SR0_EXIST      STD_OFF
#endif

/*************************GTMTIM3SR1_ISR*********************************/          

#ifdef GTMTIM3SR1_ISR
#define IRQ_GTM_TIM3SR1_EXIST      STD_ON
#define IRQ_GTM_TIM3_SR1_PRIO      GTMTIM3SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TIM3_SR1_CAT       GTMTIM3SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM3SR1_EXIST      STD_OFF
#endif

/*************************GTMTIM3SR2_ISR*********************************/          

#ifdef GTMTIM3SR2_ISR
#define IRQ_GTM_TIM3SR2_EXIST      STD_ON
#define IRQ_GTM_TIM3_SR2_PRIO      GTMTIM3SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TIM3_SR2_CAT       GTMTIM3SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM3SR2_EXIST      STD_OFF
#endif

/*************************GTMTIM3SR3_ISR*********************************/          

#ifdef GTMTIM3SR3_ISR
#define IRQ_GTM_TIM3SR3_EXIST      STD_ON
#define IRQ_GTM_TIM3_SR3_PRIO      GTMTIM3SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TIM3_SR3_CAT       GTMTIM3SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM3SR3_EXIST      STD_OFF
#endif

/*************************GTMTIM3SR4_ISR*********************************/          

#ifdef GTMTIM3SR4_ISR
#define IRQ_GTM_TIM3SR4_EXIST      STD_ON
#define IRQ_GTM_TIM3_SR4_PRIO      GTMTIM3SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TIM3_SR4_CAT       GTMTIM3SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM3SR4_EXIST      STD_OFF
#endif

/*************************GTMTIM3SR5_ISR*********************************/          

#ifdef GTMTIM3SR5_ISR
#define IRQ_GTM_TIM3SR5_EXIST      STD_ON
#define IRQ_GTM_TIM3_SR5_PRIO      GTMTIM3SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TIM3_SR5_CAT       GTMTIM3SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM3SR5_EXIST      STD_OFF
#endif

/*************************GTMTIM3SR6_ISR*********************************/          

#ifdef GTMTIM3SR6_ISR
#define IRQ_GTM_TIM3SR6_EXIST      STD_ON
#define IRQ_GTM_TIM3_SR6_PRIO      GTMTIM3SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TIM3_SR6_CAT       GTMTIM3SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM3SR6_EXIST      STD_OFF
#endif

/*************************GTMTIM3SR7_ISR*********************************/          

#ifdef GTMTIM3SR7_ISR
#define IRQ_GTM_TIM3SR7_EXIST      STD_ON
#define IRQ_GTM_TIM3_SR7_PRIO      GTMTIM3SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TIM3_SR7_CAT       GTMTIM3SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM3SR7_EXIST      STD_OFF
#endif

/*************************GTMTIM4SR0_ISR*********************************/          

#ifdef GTMTIM4SR0_ISR
#define IRQ_GTM_TIM4SR0_EXIST      STD_ON
#define IRQ_GTM_TIM4_SR0_PRIO      GTMTIM4SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TIM4_SR0_CAT       GTMTIM4SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM4SR0_EXIST      STD_OFF
#endif

/*************************GTMTIM4SR1_ISR*********************************/          

#ifdef GTMTIM4SR1_ISR 
#define IRQ_GTM_TIM4SR1_EXIST      STD_ON
#define IRQ_GTM_TIM4_SR1_PRIO      GTMTIM4SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TIM4_SR1_CAT       GTMTIM4SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM4SR1_EXIST      STD_OFF
#endif

/*************************GTMTIM4SR2_ISR*********************************/          

#ifdef GTMTIM4SR2_ISR
#define IRQ_GTM_TIM4SR2_EXIST      STD_ON
#define IRQ_GTM_TIM4_SR2_PRIO      GTMTIM4SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TIM4_SR2_CAT       GTMTIM4SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM4SR2_EXIST      STD_OFF
#endif

/*************************GTMTIM4SR3_ISR*********************************/          

#ifdef GTMTIM4SR3_ISR
#define IRQ_GTM_TIM4SR3_EXIST      STD_ON
#define IRQ_GTM_TIM4_SR3_PRIO      GTMTIM4SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TIM4_SR3_CAT       GTMTIM4SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM4SR3_EXIST      STD_OFF
#endif

/*************************GTMTIM4SR4_ISR*********************************/          

#ifdef GTMTIM4SR4_ISR
#define IRQ_GTM_TIM4SR4_EXIST      STD_ON
#define IRQ_GTM_TIM4_SR4_PRIO      GTMTIM4SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TIM4_SR4_CAT       GTMTIM4SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM4SR4_EXIST      STD_OFF
#endif

/*************************GTMTIM4SR5_ISR*********************************/          

#ifdef GTMTIM4SR5_ISR
#define IRQ_GTM_TIM4SR5_EXIST      STD_ON
#define IRQ_GTM_TIM4_SR5_PRIO      GTMTIM4SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TIM4_SR5_CAT       GTMTIM4SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM4SR5_EXIST      STD_OFF
#endif

/*************************GTMTIM4SR6_ISR*********************************/          

#ifdef GTMTIM4SR6_ISR
#define IRQ_GTM_TIM4SR6_EXIST      STD_ON
#define IRQ_GTM_TIM4_SR6_PRIO      GTMTIM4SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TIM4_SR6_CAT       GTMTIM4SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM4SR6_EXIST      STD_OFF
#endif

/*************************GTMTIM4SR7_ISR*********************************/          

#ifdef GTMTIM4SR7_ISR
#define IRQ_GTM_TIM4SR7_EXIST      STD_ON
#define IRQ_GTM_TIM4_SR7_PRIO      GTMTIM4SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TIM4_SR7_CAT       GTMTIM4SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM4SR7_EXIST      STD_OFF
#endif

/*************************GTMTIM5SR0_ISR*********************************/          

#ifdef GTMTIM5SR0_ISR
#define IRQ_GTM_TIM5SR0_EXIST      STD_ON
#define IRQ_GTM_TIM5_SR0_PRIO      GTMTIM5SR0_ISR_ISR_LEVEL
#define IRQ_GTM_TIM5_SR0_CAT       GTMTIM5SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM5SR0_EXIST      STD_OFF
#endif

/*************************GTMTIM5SR1_ISR*********************************/          

#ifdef GTMTIM5SR1_ISR 
#define IRQ_GTM_TIM5SR1_EXIST      STD_ON
#define IRQ_GTM_TIM5_SR1_PRIO      GTMTIM5SR1_ISR_ISR_LEVEL
#define IRQ_GTM_TIM5_SR1_CAT       GTMTIM5SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM5SR1_EXIST      STD_OFF
#endif

/*************************GTMTIM5SR2_ISR*********************************/          

#ifdef GTMTIM5SR2_ISR
#define IRQ_GTM_TIM5SR2_EXIST      STD_ON
#define IRQ_GTM_TIM5_SR2_PRIO      GTMTIM5SR2_ISR_ISR_LEVEL
#define IRQ_GTM_TIM5_SR2_CAT       GTMTIM5SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM5SR2_EXIST      STD_OFF
#endif

/*************************GTMTIM5SR3_ISR*********************************/          

#ifdef GTMTIM5SR3_ISR
#define IRQ_GTM_TIM5SR3_EXIST      STD_ON
#define IRQ_GTM_TIM5_SR3_PRIO      GTMTIM5SR3_ISR_ISR_LEVEL
#define IRQ_GTM_TIM5_SR3_CAT       GTMTIM5SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM5SR3_EXIST      STD_OFF
#endif

/*************************GTMTIM5SR4_ISR*********************************/          

#ifdef GTMTIM5SR4_ISR
#define IRQ_GTM_TIM5SR4_EXIST      STD_ON
#define IRQ_GTM_TIM5_SR4_PRIO      GTMTIM5SR4_ISR_ISR_LEVEL
#define IRQ_GTM_TIM5_SR4_CAT       GTMTIM5SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM5SR4_EXIST      STD_OFF
#endif

/*************************GTMTIM5SR5_ISR*********************************/          

#ifdef GTMTIM5SR5_ISR
#define IRQ_GTM_TIM5SR5_EXIST      STD_ON
#define IRQ_GTM_TIM5_SR5_PRIO      GTMTIM5SR5_ISR_ISR_LEVEL
#define IRQ_GTM_TIM5_SR5_CAT       GTMTIM5SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM5SR5_EXIST      STD_OFF
#endif

/*************************GTMTIM5SR6_ISR*********************************/          

#ifdef GTMTIM5SR6_ISR
#define IRQ_GTM_TIM5SR6_EXIST      STD_ON
#define IRQ_GTM_TIM5_SR6_PRIO      GTMTIM5SR6_ISR_ISR_LEVEL
#define IRQ_GTM_TIM5_SR6_CAT       GTMTIM5SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM5SR6_EXIST      STD_OFF
#endif

/*************************GTMTIM5SR7_ISR*********************************/          

#ifdef GTMTIM5SR7_ISR
#define IRQ_GTM_TIM5SR7_EXIST      STD_ON
#define IRQ_GTM_TIM5_SR7_PRIO      GTMTIM5SR7_ISR_ISR_LEVEL
#define IRQ_GTM_TIM5_SR7_CAT       GTMTIM5SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_TIM5SR7_EXIST      STD_OFF
#endif

/*************************GTMAEI_ISR*********************************/          

#ifdef GTMAEI_ISR
#define IRQ_GTM_AEI_EXIST      STD_ON
#define IRQ_GTM_AEI_PRIO       GTMAEI_ISR_ISR_LEVEL
#define IRQ_GTM_AEI_CAT        GTMAEI_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_AEI_EXIST      STD_OFF
#endif

/*************************GTMARUSR0_ISR*********************************/          

#ifdef GTMARUSR0_ISR
#define IRQ_GTM_ARUSR0_EXIST      STD_ON
#define IRQ_GTM_ARU_SR0_PRIO      GTMARUSR0_ISR_ISR_LEVEL
#define IRQ_GTM_ARU_SR0_CAT       GTMARUSR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ARUSR0_EXIST      STD_OFF
#endif

/*************************GTMARUSR1_ISR*********************************/          

#ifdef GTMARUSR1_ISR
#define IRQ_GTM_ARUSR1_EXIST      STD_ON
#define IRQ_GTM_ARU_SR1_PRIO      GTMARUSR1_ISR_ISR_LEVEL
#define IRQ_GTM_ARU_SR1_CAT       GTMARUSR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ARUSR1_EXIST      STD_OFF
#endif

/*************************GTMARUSR2_ISR*********************************/          

#ifdef GTMARUSR2_ISR
#define IRQ_GTM_ARUSR2_EXIST      STD_ON
#define IRQ_GTM_ARU_SR2_PRIO      GTMARUSR2_ISR_ISR_LEVEL
#define IRQ_GTM_ARU_SR2_CAT       GTMARUSR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ARUSR2_EXIST      STD_OFF
#endif

/*************************GTMBRC_ISR*********************************/          

#ifdef GTMBRC_ISR
#define IRQ_GTM_BRC_EXIST      STD_ON
#define IRQ_GTM_BRC_PRIO       GTMBRC_ISR_ISR_LEVEL
#define IRQ_GTM_BRC_CAT        GTMBRC_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_BRC_EXIST      STD_OFF
#endif

/*************************GTMCMP_ISR*********************************/          

#ifdef GTMCMP_ISR
#define IRQ_GTM_CMP_EXIST      STD_ON
#define IRQ_GTM_CMP_PRIO       GTMCMP_ISR_ISR_LEVEL
#define IRQ_GTM_CMP_CAT        GTMCMP_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_CMP_EXIST      STD_OFF
#endif

/*************************GTMSPE0_ISR*********************************/          

#ifdef GTMSPE0_ISR
#define IRQ_GTM_SPE0_EXIST      STD_ON
#define IRQ_GTM_SPE0_PRIO       GTMSPE0_ISR_ISR_LEVEL
#define IRQ_GTM_SPE0_CAT        GTMSPE0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_SPE0_EXIST      STD_OFF
#endif

/*************************GTMSPE1_ISR*********************************/          

#ifdef GTMSPE1_ISR
#define IRQ_GTM_SPE1_EXIST      STD_ON
#define IRQ_GTM_SPE1_PRIO       GTMSPE1_ISR_ISR_LEVEL
#define IRQ_GTM_SPE1_CAT        GTMSPE1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_SPE1_EXIST      STD_OFF
#endif

/*************************GTMPSM0SR0_ISR*********************************/          

#ifdef GTMPSM0SR0_ISR
#define IRQ_GTM_PSM0SR0_EXIST      STD_ON
#define IRQ_GTM_PSM0_SR0_PRIO      GTMPSM0SR0_ISR_ISR_LEVEL
#define IRQ_GTM_PSM0_SR0_CAT       GTMPSM0SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_PSM0SR0_EXIST      STD_OFF
#endif

/*************************GTMPSM0SR1_ISR*********************************/          

#ifdef GTMPSM0SR1_ISR
#define IRQ_GTM_PSM0SR1_EXIST      STD_ON
#define IRQ_GTM_PSM0_SR1_PRIO      GTMPSM0SR1_ISR_ISR_LEVEL
#define IRQ_GTM_PSM0_SR1_CAT       GTMPSM0SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_PSM0SR1_EXIST      STD_OFF
#endif

/*************************GTMPSM0SR2_ISR*********************************/          

#ifdef GTMPSM0SR2_ISR
#define IRQ_GTM_PSM0SR2_EXIST      STD_ON
#define IRQ_GTM_PSM0_SR2_PRIO      GTMPSM0SR2_ISR_ISR_LEVEL
#define IRQ_GTM_PSM0_SR2_CAT       GTMPSM0SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_PSM0SR2_EXIST      STD_OFF
#endif

/*************************GTMPSM0SR3_ISR*********************************/          

#ifdef GTMPSM0SR3_ISR
#define IRQ_GTM_PSM0SR3_EXIST      STD_ON
#define IRQ_GTM_PSM0_SR3_PRIO      GTMPSM0SR3_ISR_ISR_LEVEL
#define IRQ_GTM_PSM0_SR3_CAT       GTMPSM0SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_PSM0SR3_EXIST      STD_OFF
#endif

/*************************GTMPSM0SR4_ISR*********************************/          

#ifdef GTMPSM0SR4_ISR
#define IRQ_GTM_PSM0_SR4EXIST      STD_ON
#define IRQ_GTM_PSM0_SR4_PRIO      GTMPSM0SR4_ISR_ISR_LEVEL
#define IRQ_GTM_PSM0_SR4_CAT       GTMPSM0SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_PSM0_SR4EXIST      STD_OFF
#endif

/*************************GTMPSM0SR5_ISR*********************************/          

#ifdef GTMPSM0SR5_ISR
#define IRQ_GTM_PSM0SR5_EXIST      STD_ON
#define IRQ_GTM_PSM0_SR5_PRIO      GTMPSM0SR5_ISR_ISR_LEVEL
#define IRQ_GTM_PSM0_SR5_CAT       GTMPSM0SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_PSM0SR5_EXIST      STD_OFF
#endif

/*************************GTMPSM0SR6_ISR*********************************/          

#ifdef GTMPSM0SR6_ISR
#define IRQ_GTM_PSM0SR6_EXIST      STD_ON
#define IRQ_GTM_PSM0_SR6_PRIO      GTMPSM0SR6_ISR_ISR_LEVEL
#define IRQ_GTM_PSM0_SR6_CAT       GTMPSM0SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_PSM0SR6_EXIST      STD_OFF
#endif

/*************************GTMPSM0SR7_ISR*********************************/          

#ifdef GTMPSM0SR7_ISR
#define IRQ_GTM_PSM0SR7_EXIST      STD_ON
#define IRQ_GTM_PSM0_SR7_PRIO      GTMPSM0SR7_ISR_ISR_LEVEL
#define IRQ_GTM_PSM0_SR7_CAT       GTMPSM0SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_PSM0SR7_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR0_ISR*********************************/          

#ifdef GTMDPLLSR0_ISR
#define IRQ_GTM_DPLLSR0_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR0_PRIO      GTMDPLLSR0_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR0_CAT       GTMDPLLSR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR0_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR1_ISR*********************************/          

#ifdef GTMDPLLSR1_ISR
#define IRQ_GTM_DPLLSR1_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR1_PRIO      GTMDPLLSR1_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR1_CAT       GTMDPLLSR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR1_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR2_ISR*********************************/          

#ifdef GTMDPLLSR2_ISR
#define IRQ_GTM_DPLLSR2_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR2_PRIO      GTMDPLLSR2_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR2_CAT       GTMDPLLSR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR2_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR3_ISR*********************************/          

#ifdef GTMDPLLSR3_ISR
#define IRQ_GTM_DPLLSR3_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR3_PRIO      GTMDPLLSR3_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR3_CAT       GTMDPLLSR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR3_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR4_ISR*********************************/          

#ifdef GTMDPLLSR4_ISR
#define IRQ_GTM_DPLLSR4_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR4_PRIO      GTMDPLLSR4_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR4_CAT       GTMDPLLSR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR4_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR5_ISR*********************************/          

#ifdef GTMDPLLSR5_ISR
#define IRQ_GTM_DPLLSR5_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR5_PRIO      GTMDPLLSR5_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR5_CAT       GTMDPLLSR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR5_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR6_ISR*********************************/          

#ifdef GTMDPLLSR6_ISR
#define IRQ_GTM_DPLLSR6_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR6_PRIO      GTMDPLLSR6_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR6_CAT       GTMDPLLSR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR6_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR7_ISR*********************************/          

#ifdef GTMDPLLSR7_ISR
#define IRQ_GTM_DPLLSR7_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR7_PRIO      GTMDPLLSR7_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR7_CAT       GTMDPLLSR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR7_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR8_ISR*********************************/          

#ifdef GTMDPLLSR8_ISR
#define IRQ_GTM_DPLLSR8_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR8_PRIO      GTMDPLLSR8_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR8_CAT       GTMDPLLSR8_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR8_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR9_ISR*********************************/          

#ifdef GTMDPLLSR9_ISR
#define IRQ_GTM_DPLLSR9_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR9_PRIO      GTMDPLLSR9_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR9_CAT       GTMDPLLSR9_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR9_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR10_ISR*********************************/          

#ifdef GTMDPLLSR10_ISR
#define IRQ_GTM_DPLLSR10_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR10_PRIO      GTMDPLLSR10_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR10_CAT       GTMDPLLSR10_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR10_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR11_ISR*********************************/          

#ifdef GTMDPLLSR11_ISR
#define IRQ_GTM_DPLLSR11_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR11_PRIO      GTMDPLLSR11_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR11_CAT       GTMDPLLSR11_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR11_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR12_ISR*********************************/          

#ifdef GTMDPLLSR12_ISR
#define IRQ_GTM_DPLLSR12_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR12_PRIO      GTMDPLLSR12_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR12_CAT       GTMDPLLSR12_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR12_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR13_ISR*********************************/          

#ifdef GTMDPLLSR13_ISR
#define IRQ_GTM_DPLLSR13_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR13_PRIO      GTMDPLLSR13_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR13_CAT       GTMDPLLSR13_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR13_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR14_ISR*********************************/          

#ifdef GTMDPLLSR14_ISR
#define IRQ_GTM_DPLLSR14_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR14_PRIO      GTMDPLLSR14_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR14_CAT       GTMDPLLSR14_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR14_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR15_ISR*********************************/          

#ifdef GTMDPLLSR15_ISR
#define IRQ_GTM_DPLLSR15_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR15_PRIO      GTMDPLLSR15_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR15_CAT       GTMDPLLSR15_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR15_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR16_ISR*********************************/          

#ifdef GTMDPLLSR16_ISR
#define IRQ_GTM_DPLLSR16_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR16_PRIO      GTMDPLLSR16_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR16_CAT       GTMDPLLSR16_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR16_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR17_ISR*********************************/          

#ifdef GTMDPLLSR17_ISR
#define IRQ_GTM_DPLLSR17_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR17_PRIO      GTMDPLLSR17_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR17_CAT       GTMDPLLSR17_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR17_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR18_ISR*********************************/          

#ifdef GTMDPLLSR18_ISR
#define IRQ_GTM_DPLLSR18_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR18_PRIO      GTMDPLLSR18_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR18_CAT       GTMDPLLSR18_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR18_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR19_ISR*********************************/          

#ifdef GTMDPLLSR19_ISR
#define IRQ_GTM_DPLLSR19_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR19_PRIO      GTMDPLLSR19_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR19_CAT       GTMDPLLSR19_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR19_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR20_ISR*********************************/          

#ifdef GTMDPLLSR20_ISR
#define IRQ_GTM_DPLLSR20_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR20_PRIO      GTMDPLLSR20_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR20_CAT       GTMDPLLSR20_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR20_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR21_ISR*********************************/          

#ifdef GTMDPLLSR21_ISR
#define IRQ_GTM_DPLLSR21_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR21_PRIO      GTMDPLLSR21_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR21_CAT       GTMDPLLSR21_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR21_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR22_ISR*********************************/          

#ifdef GTMDPLLSR22_ISR
#define IRQ_GTM_DPLLSR22_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR22_PRIO      GTMDPLLSR22_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR22_CAT       GTMDPLLSR22_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR22_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR23_ISR*********************************/          

#ifdef GTMDPLLSR23_ISR
#define IRQ_GTM_DPLLSR23_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR23_PRIO      GTMDPLLSR23_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR23_CAT       GTMDPLLSR23_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR23_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR24_ISR*********************************/          

#ifdef GTMDPLLSR24_ISR
#define IRQ_GTM_DPLLSR24_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR24_PRIO      GTMDPLLSR24_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR24_CAT       GTMDPLLSR24_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR24_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR25_ISR*********************************/          

#ifdef GTMDPLLSR25_ISR
#define IRQ_GTM_DPLLSR25_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR25_PRIO      GTMDPLLSR25_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR25_CAT       GTMDPLLSR25_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR25_EXIST      STD_OFF
#endif

/*************************GTMDPLLSR26_ISR*********************************/          

#ifdef GTMDPLLSR26_ISR
#define IRQ_GTM_DPLLSR26_EXIST      STD_ON
#define IRQ_GTM_DPLL_SR26_PRIO      GTMDPLLSR26_ISR_ISR_LEVEL
#define IRQ_GTM_DPLL_SR26_CAT       GTMDPLLSR26_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_DPLLSR26_EXIST      STD_OFF
#endif

/*************************GTMMCS0SR0_ISR*********************************/          

#ifdef GTMMCS0SR0_ISR 
#define IRQ_GTM_MCS0SR0_EXIST      STD_ON
#define IRQ_GTM_MCS0_SR0_PRIO      GTMMCS0SR0_ISR_ISR_LEVEL
#define IRQ_GTM_MCS0_SR0_CAT       GTMMCS0SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS0SR0_EXIST      STD_OFF
#endif

/*************************GTMMCS0SR1_ISR*********************************/          

#ifdef GTMMCS0SR1_ISR
#define IRQ_GTM_MCS0SR1_EXIST      STD_ON
#define IRQ_GTM_MCS0_SR1_PRIO      GTMMCS0SR1_ISR_ISR_LEVEL
#define IRQ_GTM_MCS0_SR1_CAT       GTMMCS0SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS0SR1_EXIST      STD_OFF
#endif

/*************************GTMMCS0SR2_ISR*********************************/          

#ifdef GTMMCS0SR2_ISR
#define IRQ_GTM_MCS0SR2_EXIST      STD_ON
#define IRQ_GTM_MCS0_SR2_PRIO      GTMMCS0SR2_ISR_ISR_LEVEL
#define IRQ_GTM_MCS0_SR2_CAT       GTMMCS0SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS0SR2_EXIST      STD_OFF
#endif

/*************************GTMMCS0SR3_ISR*********************************/          

#ifdef GTMMCS0SR3_ISR
#define IRQ_GTM_MCS0SR3_EXIST      STD_ON
#define IRQ_GTM_MCS0_SR3_PRIO      GTMMCS0SR3_ISR_ISR_LEVEL
#define IRQ_GTM_MCS0_SR3_CAT       GTMMCS0SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS0SR3_EXIST      STD_OFF
#endif

/*************************GTMMCS0SR4_ISR*********************************/          

#ifdef GTMMCS0SR4_ISR
#define IRQ_GTM_MCS0SR4_EXIST      STD_ON
#define IRQ_GTM_MCS0_SR4_PRIO      GTMMCS0SR4_ISR_ISR_LEVEL
#define IRQ_GTM_MCS0_SR4_CAT       GTMMCS0SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS0SR4_EXIST      STD_OFF
#endif

/*************************GTMMCS0SR5_ISR*********************************/          

#ifdef GTMMCS0SR5_ISR
#define IRQ_GTM_MCS0SR5_EXIST      STD_ON
#define IRQ_GTM_MCS0_SR5_PRIO      GTMMCS0SR5_ISR_ISR_LEVEL
#define IRQ_GTM_MCS0_SR5_CAT       GTMMCS0SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS0SR5_EXIST      STD_OFF
#endif

/*************************GTMMCS0SR6_ISR*********************************/          

#ifdef GTMMCS0SR6_ISR
#define IRQ_GTM_MCS0SR6_EXIST      STD_ON
#define IRQ_GTM_MCS0_SR6_PRIO      GTMMCS0SR6_ISR_ISR_LEVEL
#define IRQ_GTM_MCS0_SR6_CAT       GTMMCS0SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS0SR6_EXIST      STD_OFF
#endif

/*************************GTMMCS0SR7_ISR*********************************/          

#ifdef GTMMCS0SR7_ISR
#define IRQ_GTM_MCS0SR7_EXIST      STD_ON
#define IRQ_GTM_MCS0_SR7_PRIO      GTMMCS0SR7_ISR_ISR_LEVEL
#define IRQ_GTM_MCS0_SR7_CAT       GTMMCS0SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS0SR7_EXIST      STD_OFF
#endif

/*************************GTMMCS1SR0_ISR*********************************/          

#ifdef GTMMCS1SR0_ISR
#define IRQ_GTM_MCS1SR0_EXIST      STD_ON
#define IRQ_GTM_MCS1_SR0_PRIO      GTMMCS1SR0_ISR_ISR_LEVEL
#define IRQ_GTM_MCS1_SR0_CAT       GTMMCS1SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS1SR0_EXIST      STD_OFF
#endif

/*************************GTMMCS1SR1_ISR*********************************/          

#ifdef GTMMCS1SR1_ISR
#define IRQ_GTM_MCS1SR1_EXIST      STD_ON
#define IRQ_GTM_MCS1_SR1_PRIO      GTMMCS1SR1_ISR_ISR_LEVEL
#define IRQ_GTM_MCS1_SR1_CAT       GTMMCS1SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS1SR1_EXIST      STD_OFF
#endif

/*************************GTMMCS1SR2_ISR*********************************/          

#ifdef GTMMCS1SR2_ISR
#define IRQ_GTM_MCS1SR2_EXIST      STD_ON
#define IRQ_GTM_MCS1_SR2_PRIO      GTMMCS1SR2_ISR_ISR_LEVEL
#define IRQ_GTM_MCS1_SR2_CAT       GTMMCS1SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS1SR2_EXIST      STD_OFF
#endif

/*************************GTMMCS1SR3_ISR*********************************/          

#ifdef GTMMCS1SR3_ISR
#define IRQ_GTM_MCS1SR3_EXIST      STD_ON
#define IRQ_GTM_MCS1_SR3_PRIO      GTMMCS1SR3_ISR_ISR_LEVEL
#define IRQ_GTM_MCS1_SR3_CAT       GTMMCS1SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS1SR3_EXIST      STD_OFF
#endif

/*************************GTMMCS1SR4_ISR*********************************/          

#ifdef GTMMCS1SR4_ISR
#define IRQ_GTM_MCS1SR4_EXIST      STD_ON
#define IRQ_GTM_MCS1_SR4_PRIO      GTMMCS1SR4_ISR_ISR_LEVEL
#define IRQ_GTM_MCS1_SR4_CAT       GTMMCS1SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS1SR4_EXIST      STD_OFF
#endif

/*************************GTMMCS1SR5_ISR*********************************/          

#ifdef GTMMCS1SR5_ISR
#define IRQ_GTM_MCS1SR5_EXIST      STD_ON
#define IRQ_GTM_MCS1_SR5_PRIO      GTMMCS1SR5_ISR_ISR_LEVEL
#define IRQ_GTM_MCS1_SR5_CAT       GTMMCS1SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS1SR5_EXIST      STD_OFF
#endif

/*************************GTMMCS1SR6_ISR*********************************/          

#ifdef GTMMCS1SR6_ISR
#define IRQ_GTM_MCS1SR6_EXIST      STD_ON
#define IRQ_GTM_MCS1_SR6_PRIO      GTMMCS1SR6_ISR_ISR_LEVEL
#define IRQ_GTM_MCS1_SR6_CAT       GTMMCS1SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS1SR6_EXIST      STD_OFF
#endif

/*************************GTMMCS1SR7_ISR*********************************/          

#ifdef GTMMCS1SR7_ISR
#define IRQ_GTM_MCS1SR7_EXIST      STD_ON
#define IRQ_GTM_MCS1_SR7_PRIO      GTMMCS1SR7_ISR_ISR_LEVEL
#define IRQ_GTM_MCS1_SR7_CAT       GTMMCS1SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS1SR7_EXIST      STD_OFF
#endif

/*************************GTMMCS2SR0_ISR*********************************/          

#ifdef GTMMCS2SR0_ISR
#define IRQ_GTM_MCS2SR0_EXIST      STD_ON
#define IRQ_GTM_MCS2_SR0_PRIO      GTMMCS2SR0_ISR_ISR_LEVEL
#define IRQ_GTM_MCS2_SR0_CAT       GTMMCS2SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS2SR0_EXIST      STD_OFF
#endif

/*************************GTMMCS2SR1_ISR*********************************/          

#ifdef GTMMCS2SR1_ISR
#define IRQ_GTM_MCS2SR1_EXIST      STD_ON
#define IRQ_GTM_MCS2_SR1_PRIO      GTMMCS2SR1_ISR_ISR_LEVEL
#define IRQ_GTM_MCS2_SR1_CAT       GTMMCS2SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS2SR1_EXIST      STD_OFF
#endif

/*************************GTMMCS2SR2_ISR*********************************/          

#ifdef GTMMCS2SR2_ISR
#define IRQ_GTM_MCS2SR2_EXIST      STD_ON
#define IRQ_GTM_MCS2_SR2_PRIO      GTMMCS2SR2_ISR_ISR_LEVEL
#define IRQ_GTM_MCS2_SR2_CAT       GTMMCS2SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS2SR2_EXIST      STD_OFF
#endif

/*************************GTMMCS2SR3_ISR*********************************/          

#ifdef GTMMCS2SR3_ISR
#define IRQ_GTM_MCS2SR3_EXIST      STD_ON
#define IRQ_GTM_MCS2_SR3_PRIO      GTMMCS2SR3_ISR_ISR_LEVEL
#define IRQ_GTM_MCS2_SR3_CAT       GTMMCS2SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS2SR3_EXIST      STD_OFF
#endif

/*************************GTMMCS2SR4_ISR*********************************/          

#ifdef GTMMCS2SR4_ISR
#define IRQ_GTM_MCS2SR4_EXIST      STD_ON
#define IRQ_GTM_MCS2_SR4_PRIO      GTMMCS2SR4_ISR_ISR_LEVEL
#define IRQ_GTM_MCS2_SR4_CAT       GTMMCS2SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS2SR4_EXIST      STD_OFF
#endif

/*************************GTMMCS2SR5_ISR*********************************/          

#ifdef GTMMCS2SR5_ISR
#define IRQ_GTM_MCS2SR5_EXIST      STD_ON
#define IRQ_GTM_MCS2_SR5_PRIO      GTMMCS2SR5_ISR_ISR_LEVEL
#define IRQ_GTM_MCS2_SR5_CAT       GTMMCS2SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS2SR5_EXIST      STD_OFF
#endif

/*************************GTMMCS2SR6_ISR*********************************/          

#ifdef GTMMCS2SR6_ISR
#define IRQ_GTM_MCS2SR6_EXIST      STD_ON
#define IRQ_GTM_MCS2_SR6_PRIO      GTMMCS2SR6_ISR_ISR_LEVEL
#define IRQ_GTM_MCS2_SR6_CAT       GTMMCS2SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS2SR6_EXIST      STD_OFF
#endif

/*************************GTMMCS2SR7_ISR*********************************/          

#ifdef GTMMCS2SR7_ISR
#define IRQ_GTM_MCS2SR7_EXIST      STD_ON
#define IRQ_GTM_MCS2_SR7_PRIO      GTMMCS2SR7_ISR_ISR_LEVEL
#define IRQ_GTM_MCS2_SR7_CAT       GTMMCS2SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS2SR7_EXIST      STD_OFF
#endif

/*************************GTMMCS3SR0_ISR*********************************/          

#ifdef GTMMCS3SR0_ISR
#define IRQ_GTM_MCS3SR0_EXIST      STD_ON
#define IRQ_GTM_MCS3_SR0_PRIO      GTMMCS3SR0_ISR_ISR_LEVEL
#define IRQ_GTM_MCS3_SR0_CAT       GTMMCS3SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS3SR0_EXIST      STD_OFF
#endif

/*************************GTMMCS3SR1_ISR*********************************/          

#ifdef GTMMCS3SR1_ISR
#define IRQ_GTM_MCS3SR1_EXIST      STD_ON
#define IRQ_GTM_MCS3_SR1_PRIO      GTMMCS3SR1_ISR_ISR_LEVEL
#define IRQ_GTM_MCS3_SR1_CAT       GTMMCS3SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS3SR1_EXIST      STD_OFF
#endif

/*************************GTMMCS3SR2_ISR*********************************/          

#ifdef GTMMCS3SR2_ISR
#define IRQ_GTM_MCS3SR2_EXIST      STD_ON
#define IRQ_GTM_MCS3_SR2_PRIO      GTMMCS3SR2_ISR_ISR_LEVEL
#define IRQ_GTM_MCS3_SR2_CAT       GTMMCS3SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS3SR2_EXIST      STD_OFF
#endif

/*************************GTMMCS3SR3_ISR*********************************/          

#ifdef GTMMCS3SR3_ISR
#define IRQ_GTM_MCS3SR3_EXIST      STD_ON
#define IRQ_GTM_MCS3_SR3_PRIO      GTMMCS3SR3_ISR_ISR_LEVEL
#define IRQ_GTM_MCS3_SR3_CAT       GTMMCS3SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS3SR3_EXIST      STD_OFF
#endif

/*************************GTMMCS3SR4_ISR*********************************/          

#ifdef GTMMCS3SR4_ISR
#define IRQ_GTM_MCS3SR4_EXIST      STD_ON
#define IRQ_GTM_MCS3_SR4_PRIO      GTMMCS3SR4_ISR_ISR_LEVEL
#define IRQ_GTM_MCS3_SR4_CAT       GTMMCS3SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS3SR4_EXIST      STD_OFF
#endif

/*************************GTMMCS3SR5_ISR*********************************/          

#ifdef GTMMCS3SR5_ISR
#define IRQ_GTM_MCS3SR5_EXIST      STD_ON
#define IRQ_GTM_MCS3_SR5_PRIO      GTMMCS3SR5_ISR_ISR_LEVEL
#define IRQ_GTM_MCS3_SR5_CAT       GTMMCS3SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS3SR5_EXIST      STD_OFF
#endif

/*************************GTMMCS3SR6_ISR*********************************/          

#ifdef GTMMCS3SR6_ISR
#define IRQ_GTM_MCS3SR6_EXIST      STD_ON
#define IRQ_GTM_MCS3_SR6_PRIO      GTMMCS3SR6_ISR_ISR_LEVEL
#define IRQ_GTM_MCS3_SR6_CAT       GTMMCS3SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS3SR6_EXIST      STD_OFF
#endif

/*************************GTMMCS3SR7_ISR*********************************/          

#ifdef GTMMCS3SR7_ISR
#define IRQ_GTM_MCS3SR7_EXIST      STD_ON
#define IRQ_GTM_MCS3_SR7_PRIO      GTMMCS3SR7_ISR_ISR_LEVEL
#define IRQ_GTM_MCS3_SR7_CAT       GTMMCS3SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS3SR7_EXIST      STD_OFF
#endif

/*************************GTMMCS4SR0_ISR*********************************/          

#ifdef GTMMCS4SR0_ISR 
#define IRQ_GTM_MCS4SR0_EXIST      STD_ON
#define IRQ_GTM_MCS4_SR0_PRIO      GTMMCS4SR0_ISR_ISR_LEVEL
#define IRQ_GTM_MCS4_SR0_CAT       GTMMCS4SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS4SR0_EXIST      STD_OFF
#endif

/*************************GTMMCS4SR1_ISR*********************************/          

#ifdef GTMMCS4SR1_ISR
#define IRQ_GTM_MCS4SR1_EXIST      STD_ON
#define IRQ_GTM_MCS4_SR1_PRIO      GTMMCS4SR1_ISR_ISR_LEVEL
#define IRQ_GTM_MCS4_SR1_CAT       GTMMCS4SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS4SR1_EXIST      STD_OFF
#endif

/*************************GTMMCS4SR2_ISR*********************************/          

#ifdef GTMMCS4SR2_ISR
#define IRQ_GTM_MCS4SR2_EXIST      STD_ON
#define IRQ_GTM_MCS4_SR2_PRIO      GTMMCS4SR2_ISR_ISR_LEVEL
#define IRQ_GTM_MCS4_SR2_CAT       GTMMCS4SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS4SR2_EXIST      STD_OFF
#endif

/*************************GTMMCS4SR3_ISR*********************************/          

#ifdef GTMMCS4SR3_ISR
#define IRQ_GTM_MCS4SR3_EXIST      STD_ON
#define IRQ_GTM_MCS4_SR3_PRIO      GTMMCS4SR3_ISR_ISR_LEVEL
#define IRQ_GTM_MCS4_SR3_CAT       GTMMCS4SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS4SR3_EXIST      STD_OFF
#endif

/*************************GTMMCS4SR4_ISR*********************************/          

#ifdef GTMMCS4SR4_ISR
#define IRQ_GTM_MCS4SR4_EXIST      STD_ON
#define IRQ_GTM_MCS4_SR4_PRIO      GTMMCS4SR4_ISR_ISR_LEVEL
#define IRQ_GTM_MCS4_SR4_CAT       GTMMCS4SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS4SR4_EXIST      STD_OFF
#endif

/*************************GTMMCS4SR5_ISR*********************************/          

#ifdef GTMMCS4SR5_ISR
#define IRQ_GTM_MCS4SR5_EXIST      STD_ON
#define IRQ_GTM_MCS4_SR5_PRIO      GTMMCS4SR5_ISR_ISR_LEVEL
#define IRQ_GTM_MCS4_SR5_CAT       GTMMCS4SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS4SR5_EXIST      STD_OFF
#endif

/*************************GTMMCS4SR6_ISR*********************************/          

#ifdef GTMMCS4SR6_ISR
#define IRQ_GTM_MCS4SR6_EXIST      STD_ON
#define IRQ_GTM_MCS4_SR6_PRIO      GTMMCS4SR6_ISR_ISR_LEVEL
#define IRQ_GTM_MCS4_SR6_CAT       GTMMCS4SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS4SR6_EXIST      STD_OFF
#endif

/*************************GTMMCS4SR7_ISR*********************************/          

#ifdef GTMMCS4SR7_ISR
#define IRQ_GTM_MCS4SR7_EXIST      STD_ON
#define IRQ_GTM_MCS4_SR7_PRIO      GTMMCS4SR7_ISR_ISR_LEVEL
#define IRQ_GTM_MCS4_SR7_CAT       GTMMCS4SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS4SR7_EXIST      STD_OFF
#endif

/*************************GTMMCS5SR0_ISR*********************************/          

#ifdef GTMMCS5SR0_ISR
#define IRQ_GTM_MCS5SR0_EXIST      STD_ON
#define IRQ_GTM_MCS5_SR0_PRIO      GTMMCS5SR0_ISR_ISR_LEVEL
#define IRQ_GTM_MCS5_SR0_CAT       GTMMCS5SR0_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS5SR0_EXIST      STD_OFF
#endif

/*************************GTMMCS5SR1_ISR*********************************/          

#ifdef GTMMCS5SR1_ISR
#define IRQ_GTM_MCS5SR1_EXIST      STD_ON
#define IRQ_GTM_MCS5_SR1_PRIO      GTMMCS5SR1_ISR_ISR_LEVEL
#define IRQ_GTM_MCS5_SR1_CAT       GTMMCS5SR1_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS5SR1_EXIST      STD_OFF
#endif

/*************************GTMMCS5SR2_ISR*********************************/          

#ifdef GTMMCS5SR2_ISR
#define IRQ_GTM_MCS5SR2_EXIST      STD_ON
#define IRQ_GTM_MCS5_SR2_PRIO      GTMMCS5SR2_ISR_ISR_LEVEL
#define IRQ_GTM_MCS5_SR2_CAT       GTMMCS5SR2_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS5SR2_EXIST      STD_OFF
#endif

/*************************GTMMCS5SR3_ISR*********************************/          

#ifdef GTMMCS5SR3_ISR
#define IRQ_GTM_MCS5SR3_EXIST      STD_ON
#define IRQ_GTM_MCS5_SR3_PRIO      GTMMCS5SR3_ISR_ISR_LEVEL
#define IRQ_GTM_MCS5_SR3_CAT       GTMMCS5SR3_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS5SR3_EXIST      STD_OFF
#endif

/*************************GTMMCS5SR4_ISR*********************************/          

#ifdef GTMMCS5SR4_ISR
#define IRQ_GTM_MCS5SR4_EXIST      STD_ON
#define IRQ_GTM_MCS5_SR4_PRIO      GTMMCS5SR4_ISR_ISR_LEVEL
#define IRQ_GTM_MCS5_SR4_CAT       GTMMCS5SR4_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS5SR4_EXIST      STD_OFF
#endif

/*************************GTMMCS5SR5_ISR*********************************/          

#ifdef GTMMCS5SR5_ISR
#define IRQ_GTM_MCS5SR5_EXIST      STD_ON
#define IRQ_GTM_MCS5_SR5_PRIO      GTMMCS5SR5_ISR_ISR_LEVEL
#define IRQ_GTM_MCS5_SR5_CAT       GTMMCS5SR5_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS5SR5_EXIST      STD_OFF
#endif

/*************************GTMMCS5SR6_ISR*********************************/          

#ifdef GTMMCS5SR6_ISR
#define IRQ_GTM_MCS5SR6_EXIST      STD_ON
#define IRQ_GTM_MCS5_SR6_PRIO      GTMMCS5SR6_ISR_ISR_LEVEL
#define IRQ_GTM_MCS5_SR6_CAT       GTMMCS5SR6_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS5SR6_EXIST      STD_OFF
#endif

/*************************GTMMCS5SR7_ISR*********************************/          

#ifdef GTMMCS5SR7_ISR
#define IRQ_GTM_MCS5SR7_EXIST      STD_ON
#define IRQ_GTM_MCS5_SR7_PRIO      GTMMCS5SR7_ISR_ISR_LEVEL
#define IRQ_GTM_MCS5_SR7_CAT       GTMMCS5SR7_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_MCS5SR7_EXIST      STD_OFF
#endif

/*************************GTMERRSR_ISR*********************************/          

#ifdef GTMERRSR_ISR
#define IRQ_GTM_ERR_EXIST      STD_ON
#define IRQ_GTM_ERR_SR_PRIO    GTMERRSR_ISR_ISR_LEVEL
#define IRQ_GTM_ERR_SR_CAT     GTMERRSR_ISR_ISR_CATEGORY
#else
#define IRQ_GTM_ERR_EXIST      STD_OFF
#endif

#endif /* #ifndef GTM_IRQ_H */

