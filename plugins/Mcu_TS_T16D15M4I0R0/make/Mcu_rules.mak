# \file
#
# \brief AUTOSAR Mcu
#
# This file contains the implementation of the AUTOSAR
# module Mcu.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Mcu_src

Mcu_src_FILES       += $(Mcu_CORE_PATH)\src\Gtm.c
Mcu_src_FILES       += $(Mcu_CORE_PATH)\src\Gtm_Platform.c
Mcu_src_FILES       += $(Mcu_CORE_PATH)\src\Mcu.c
Mcu_src_FILES       += $(Mcu_CORE_PATH)\src\Mcu_Crc.c
Mcu_src_FILES       += $(Mcu_CORE_PATH)\src\Mcu_Dma.c
Mcu_src_FILES       += $(Mcu_CORE_PATH)\src\Mcu_Platform.c
Mcu_src_FILES       += $(Mcu_CORE_PATH)\src\Mcu_Ver.c
Mcu_src_FILES       += $(Mcu_CORE_PATH)\src\Gtm_Irq.c
Mcu_src_FILES       += $(Mcu_OUTPUT_PATH)\src\Gtm_LCfg.c
Mcu_src_FILES       += $(Mcu_OUTPUT_PATH)\src\Gtm_PBCfg.c
Mcu_src_FILES       += $(Mcu_OUTPUT_PATH)\src\Mcu_PBCfg.c
ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Mcu_src_FILES :=
endif
