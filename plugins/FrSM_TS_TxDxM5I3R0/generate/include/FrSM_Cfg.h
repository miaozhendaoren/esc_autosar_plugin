/**
 * \file
 *
 * \brief AUTOSAR FrSM
 *
 * This file contains the implementation of the AUTOSAR
 * module FrSM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

[!INCLUDE "../include/FrSM_Variables.m"!][!//
#ifndef _FRSM_CFG_H_
#define _FRSM_CFG_H_

/*==================[includes]==============================================*/

#include <Std_Types.h>
#include <TSAutosar.h>          /* TS_PROD_ERR_* defines */
#include <FrSM_Version.h>

/*==================[macros]================================================*/




/*------------------[Defensive programming]---------------------------------*/
[!SELECT "FrSMDefensiveProgramming"!][!//

#if (defined FRSM_DEFENSIVE_PROGRAMMING_ENABLED)
#error FRSM_DEFENSIVE_PROGRAMMING_ENABLED is already defined
#endif
/** \brief Defensive programming usage
 **
 ** En- or disables the usage of the defensive programming */
#define FRSM_DEFENSIVE_PROGRAMMING_ENABLED   [!//
[!IF "(../FrSMGeneral/FrSMDevErrorDetect  = 'true') and (FrSMDefProgEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRSM_PRECONDITION_ASSERT_ENABLED)
#error FRSM_PRECONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Precondition assertion usage
 **
 ** En- or disables the usage of precondition assertion checks */
#define FRSM_PRECONDITION_ASSERT_ENABLED     [!//
[!IF "(../FrSMGeneral/FrSMDevErrorDetect  = 'true') and (FrSMDefProgEnabled = 'true') and (FrSMPrecondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRSM_POSTCONDITION_ASSERT_ENABLED)
#error FRSM_POSTCONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Postcondition assertion usage
 **
 ** En- or disables the usage of postcondition assertion checks */
#define FRSM_POSTCONDITION_ASSERT_ENABLED    [!//
[!IF "(../FrSMGeneral/FrSMDevErrorDetect  = 'true') and (FrSMDefProgEnabled = 'true') and (FrSMPostcondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRSM_UNREACHABLE_CODE_ASSERT_ENABLED)
#error FRSM_UNREACHABLE_CODE_ASSERT_ENABLED is already defined
#endif
/** \brief Unreachable code assertion usage
 **
 ** En- or disables the usage of unreachable code assertion checks */
#define FRSM_UNREACHABLE_CODE_ASSERT_ENABLED [!//
[!IF "(../FrSMGeneral/FrSMDevErrorDetect  = 'true') and (FrSMDefProgEnabled = 'true') and (FrSMUnreachAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRSM_INVARIANT_ASSERT_ENABLED)
#error FRSM_INVARIANT_ASSERT_ENABLED is already defined
#endif
/** \brief Invariant assertion usage
 **
 ** En- or disables the usage of invariant assertion checks */
#define FRSM_INVARIANT_ASSERT_ENABLED        [!//
[!IF "(../FrSMGeneral/FrSMDevErrorDetect  = 'true') and (FrSMDefProgEnabled = 'true') and (FrSMInvariantAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRSM_STATIC_ASSERT_ENABLED)
#error FRSM_STATIC_ASSERT_ENABLED is already defined
#endif
/** \brief Static assertion usage
 **
 ** En- or disables the usage of static assertion checks */
#define FRSM_STATIC_ASSERT_ENABLED           [!//
[!IF "(../FrSMGeneral/FrSMDevErrorDetect  = 'true') and (FrSMDefProgEnabled = 'true') and (FrSMStaticAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDSELECT!][!//



/* standard SWS pre-compile time configuration parameters */

/** \brief Development error detection enable switch */
#define FRSM_DEV_ERROR_DETECT [!IF "$VarFrSMDevErrorDetect"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Version info API enable switch */
#define FRSM_VERSION_INFO_API [!IF "$VarFrSMVersionInfoApi"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief <Cdd>_SyncLossErrorIndication function enable switch */
#define FRSM_SYNC_LOSS_ERROR_INDICATION_ENABLE [!IF "$VarFrSMSyncLossIndicationEnable"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!IF "$VarFrSMSyncLossIndicationEnable"!][!//
/** \brief <Cdd>_SyncLossErrorIndication function name */
#define FRSM_SYNC_LOSS_ERROR_INDICATION_NAME [!"FrSMGeneral/FrSMSyncLossErrorIndicationName"!]
[!ENDIF!][!//

/* vendor specific pre-compile time configuration parameters */

/** \brief EB-specific switch to disable usage of FlexRay transceiver driver */
#define FRSM_FRTRCV_CONTROL_ENABLE [!IF "$VarFrSMFrTrcvControlEnable"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief EB-specific switch to enable optimizations if a single FlexRay cluster is used */
#define FRSM_SINGLE_CLST_OPT_ENABLE [!IF "$VarFrSMSingleClstOptEnable"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief EB-specific switch to disable mode indications from FrSM to ComM */
#define FRSM_COMM_INDICATION_ENABLE [!IF "$VarFrSMComMIndicationEnable"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief EB-specific switch to disable mode indications from FrSM to BswM */
#define FRSM_REPORT_TO_BSMW_ENABLE [!IF "$VarFrSMReportToBswMEnable"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief EB-specific switch to disable API to set transceivers to passive/active mode */
#define FRSM_SETECUPASSIVE_ENABLE [!IF "$VarFrSMSetEcuPassiveEnable"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief EB-specific switch to disable call of \a FrNm_StartupError() */
#define FRSM_FRNM_STARTUPERROR_ENABLE [!IF "$VarFrSMFrNmStartupErrorEnable"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief EB-specific switch to disable support for single slot mode */
#define FRSM_KEYSLOTONLYMODE_ENABLE [!IF "$VarFrSMKeySlotOnlyModeEnable"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!IF "$VarFrSMSyncLossIndicationHeaderEnable"!][!//
/** \brief <Cdd>_SyncLossErrorIndication header file name */
#define FRSM_SYNC_LOSS_ERROR_INDICATION_HEADER <[!"FrSMGeneral/FrSMSyncLossErrorIndicationHeaderName"!]>
[!ENDIF!][!//

/** \brief Switch for DEM to DET reporting for production error FRSM_E_CLUSTER_STARTUP */
#define FRSM_PROD_ERR_HANDLING_CLUSTER_STARTUP   [!//
[!IF "ReportToDem/FrSMClusterStartupReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrSMClusterStartupReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrSMClusterStartupReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRSM_E_CLUSTER_STARTUP */
#define FRSM_E_DEMTODET_CLUSTER_STARTUP          [!"ReportToDem/FrSMClusterStartupReportToDemDetErrorId"!]U
[!ENDIF!][!//


/** \brief Switch for DEM to DET reporting for production error FRSM_E_CLUSTER_SYNC_LOSS */
#define FRSM_PROD_ERR_HANDLING_CLUSTER_SYNC_LOSS [!//
[!IF "ReportToDem/FrSMClusterSyncLossReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrSMClusterSyncLossReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrSMClusterSyncLossReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRSM_E_CLUSTER_SYNC_LOSS */
#define FRSM_E_DEMTODET_CLUSTER_SYNC_LOSS        [!"ReportToDem/FrSMClusterSyncLossReportToDemDetErrorId"!]U
[!ENDIF!][!//



#endif /* _FRSM_CFG_H_ */

/*==================[end of file]===========================================*/
