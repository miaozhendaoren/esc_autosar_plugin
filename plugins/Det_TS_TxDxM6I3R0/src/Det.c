/**
 * \file
 *
 * \brief AUTOSAR Det
 *
 * This file contains the implementation of the AUTOSAR
 * module Det.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */


/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 6.3 (advisory)
 *    typedefs that indicate size and signedness should be used in
 *    place of basic types.
 *
 *    Reason: On windows console systems the MISRA compliant types
 *    need to be translated to C stdlib types.
 *
 *  MISRA-2) Deviated Rule: 20.9 (required)
 *    The input/output library <stdio.h> shall not be used in
 *    production code.
 *
 *    Reason: On windows console systems the library is used to
 *    print error information on the console.
 */

/*==================[inclusions]============================================*/

#include <Det_Trace.h>
#include <Std_Types.h>          /* AUTOSAR standard types */

/* to prevent redeclarations of symbols from Rte_DevelopmentErrorTracer.h by
 * our public headers */
#define DET_INTERNAL_USAGE

#include <Det_BSW.h>            /* public API and module config */
#include <Det_Int.h>            /* internal config dependent file */

/* Rte and SchM includes are handled by the Rte specific comilation
 * units of Det */

#if ((DET_DEFENSIVE_PROGRAMMING_ENABLED == STD_ON)      \
     && (TS_ARCH_FAMILY == TS_WINDOWS))
# include <stdio.h>             /* for fprintf */
#endif

/*==================[macros]================================================*/

/* !LINKSTO DET036,1 */
/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined DET_VENDOR_ID) /* configuration check */
#error DET_VENDOR_ID must be defined
#endif

#if (DET_VENDOR_ID != 1U) /* vendor check */
#error DET_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined DET_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error DET_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (DET_AR_RELEASE_MAJOR_VERSION != 4U)
#error DET_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined DET_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error DET_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (DET_AR_RELEASE_MINOR_VERSION != 0U )
#error DET_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined DET_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error DET_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (DET_AR_RELEASE_REVISION_VERSION != 3U )
#error DET_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined DET_SW_MAJOR_VERSION) /* configuration check */
#error DET_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (DET_SW_MAJOR_VERSION != 6U)
#error DET_SW_MAJOR_VERSION wrong (!= 6U)
#endif

#if (!defined DET_SW_MINOR_VERSION) /* configuration check */
#error DET_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (DET_SW_MINOR_VERSION < 3U)
#error DET_SW_MINOR_VERSION wrong (< 3U)
#endif

#if (!defined DET_SW_PATCH_VERSION) /* configuration check */
#error DET_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (DET_SW_PATCH_VERSION < 0U)
#error DET_SW_PATCH_VERSION wrong (< 0U)
#endif

/*------------------[check the log mod configuration]-----------------------*/

/* check if value of DET_LOGMODE macro is valid */
#if !((DET_LOGMODE == DET_LOGMODE_INTERNAL)         \
      || (DET_LOGMODE == DET_LOGMODE_BREAKPOINT))
# error Det configuration error: invalid value for DET_LOGMODE
#endif

/*------------------[macros for state variable values]----------------------*/

/** \brief Det has not yet been inited */
#define DET_STATE_UNINIT      1U
/** \brief Det is initialized but was not yet been started */
#define DET_STATE_NOT_STARTED 2U
/** \brief Det is idle and ready to send an error if neccessary */
#define DET_STATE_IDLE        3U
/** \brief Det is sending an error and is waiting for the confirmation*/
#define DET_STATE_SENDING     4U

/*==================[type definitions]======================================*/

/** \brief Type used for the DET internal state variable
 *
 * Allowed values:
 * - ::DET_STATE_UNINIT
 * - ::DET_STATE_NOT_STARTED
 * - ::DET_STATE_IDLE
 * - ::DET_STATE_SENDING
 */
typedef uint8 Det_StateType;

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

#if (DET_LOGMODE == DET_LOGMODE_INTERNAL)

#define DET_START_SEC_VAR_NO_INIT_16
/* !LINKSTO DET006,1 */
#include <MemMap.h>

/* see declarations in Det_Internal_Static.h */
VAR(uint16, DET_VAR_NOINIT)              Det_WriteIndex;
VAR(uint16, DET_VAR_NOINIT)              Det_UsedSlots;
VAR(uint16, DET_VAR_NOINIT)              Det_ErrorLost;

#define DET_STOP_SEC_VAR_NO_INIT_16
/* !LINKSTO DET006,1 */
#include <MemMap.h>

#define DET_START_SEC_VAR_NO_INIT_UNSPECIFIED
/* !LINKSTO DET006,1 */
#include <MemMap.h>

VAR(Det_ErrorBufferType, DET_VAR_NOINIT) Det_ErrorBuffer[DET_BUFFERSIZE];

#define DET_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
/* !LINKSTO DET006,1 */
#include <MemMap.h>

#endif

/*==================[internal data]=========================================*/

#define DET_START_SEC_VAR_INIT_8
/* !LINKSTO DET006,1 */
#include <MemMap.h>

/** \brief State of the Det
 *
 * Value is ::DET_STATE_UNINIT until Det is initialized.  When the state is
 * uninitialized the error reports are \e not stored internally.
 */
STATIC VAR(Det_StateType, DET_VAR) Det_State = DET_STATE_UNINIT;

#define DET_STOP_SEC_VAR_INIT_8
/* !LINKSTO DET006,1 */
#include <MemMap.h>

/*==================[external function definitions]=========================*/

#define DET_START_SEC_CODE
/* !LINKSTO DET006,1 */
#include <MemMap.h>

/* Service for initialization of the Development Error Tracer, see declaration
 * in Det_Api_Static.h */
FUNC(void, DET_CODE) Det_Init(void)
{
  DBG_DET_INIT_ENTRY();

#if (DET_LOGMODE == DET_LOGMODE_INTERNAL)

  /* initialize all global variables */

  for (Det_WriteIndex = 0U; Det_WriteIndex < DET_BUFFERSIZE; ++Det_WriteIndex)
  {
    Det_ErrorBuffer[Det_WriteIndex].ModuleId   = 0U;
    Det_ErrorBuffer[Det_WriteIndex].InstanceId = 0U;
    Det_ErrorBuffer[Det_WriteIndex].ApiId      = 0U;
    Det_ErrorBuffer[Det_WriteIndex].ErrorId    = 0U;
  }

  Det_WriteIndex = 0U;
  Det_ErrorLost  = 0U;
  Det_UsedSlots  = 0U;

#else /* DET_LOGMODE == DET_LOGMODE_BREAKPOINT */

  /* nothing to initialize for breakpoint log mode */

#endif /* DET_LOGMODE switch */

  /* state change: UNINIT -> NOT_STARTED */
  Det_State = DET_STATE_NOT_STARTED;

  DBG_DET_INIT_EXIT();
}

/* Service to start the Development Error Tracer, see declaration in
 * Det_Api_Static.h */
FUNC(void, DET_CODE) Det_Start(void)
{
  DBG_DET_START_ENTRY();

  if (Det_State == DET_STATE_NOT_STARTED)
  {
    /* for internal logging and beakpoint mode: NOT_STARTED -> IDLE */
    Det_State = DET_STATE_IDLE;
  }

  DBG_DET_START_EXIT();
}

/* Service for reporting of development errors, see declaration in
 * Det_Api_Static.h */
FUNC(Std_ReturnType, DET_CODE) Det_ASR40_ReportError(
   uint16 ModuleId,
   uint8  InstanceId,
   uint8  ApiId,
   uint8  ErrorId)
{
  DBG_DET_ASR40_REPORTERROR_ENTRY(ModuleId,InstanceId,ApiId,ErrorId);

  if (Det_State != DET_STATE_UNINIT)
  {
      /* call the notification functions which are configured */
      /* !LINKSTO DET035,1 */
      DET_NOTIFICATIONS(ModuleId, InstanceId, ApiId, ErrorId);

#if (DET_LOGMODE == DET_LOGMODE_INTERNAL)

      /* lock access to DET global variables */
      /* !LINKSTO Det.EB.req.errorbuffer.criticalsection,1 */
      Det_Enter_SCHM_DET_EXCLUSIVE_AREA_0();

      /* queue the reported error */
# if DET_REPORTS_TO_KEEP == DET_KEEP_FIRST_REPORTS

      /* Keep first errors reported to the Det */
      if (Det_UsedSlots < DET_BUFFERSIZE)
      {
        Det_ErrorBuffer[Det_WriteIndex].ModuleId   = ModuleId;
        Det_ErrorBuffer[Det_WriteIndex].InstanceId = InstanceId;
        Det_ErrorBuffer[Det_WriteIndex].ApiId      = ApiId;
        Det_ErrorBuffer[Det_WriteIndex].ErrorId    = ErrorId;

        ++Det_WriteIndex; /* increase write index, overflow does not matter */
        ++Det_UsedSlots;  /* increment used slot counter */
      }
      else /* Det_UsedSlots >= DET_BUFFERSIZE */
      {
        if (Det_ErrorLost < ((uint16)0xFFFFU))
        {
          ++Det_ErrorLost;       /* count lost error reports */
        }
      }

# else /* DET_REPORTS_TO_KEEP == DET_KEEP_LAST_REPORTS */

      /* Keep last errors reported to the Det */
      Det_ErrorBuffer[Det_WriteIndex].ModuleId   = ModuleId;
      Det_ErrorBuffer[Det_WriteIndex].InstanceId = InstanceId;
      Det_ErrorBuffer[Det_WriteIndex].ApiId      = ApiId;
      Det_ErrorBuffer[Det_WriteIndex].ErrorId    = ErrorId;

      ++Det_WriteIndex;                     /* increase write index */
      Det_WriteIndex %= DET_BUFFERSIZE;     /* wrap around */

      if (Det_UsedSlots < DET_BUFFERSIZE)
      {
        /* increase used slot counter but not beyond its maximum value */
        ++Det_UsedSlots;
      }
      else
      {
        if (Det_ErrorLost < ((uint16)0xFFFFU))
        {
          ++Det_ErrorLost;       /* count lost error reports */
        }
      }

# endif

      /* unlock access to DET global variables */
      Det_Exit_SCHM_DET_EXCLUSIVE_AREA_0();

#else /* DET_LOGMODE == DET_LOGMODE_BREAKPOINT */

      /* For breakpoint logging mode this function does nothing useful.  The
       * user may set a breakpoint anywhere within this function and the stack
       * backtrace may be analyzed if the debugger/emulator halts at the
       * breakpoint */

#endif /* DET_LOGMODE switch */
  }
   /* else Det_State == DET_STATE_UNINIT: ignore the function call in the
    * uninitialized state. Other modules would report such an development
    * error to the Det but the Det cannot report this to itself. */

  DBG_DET_ASR40_REPORTERROR_EXIT(E_OK,ModuleId,InstanceId,ApiId,ErrorId);
  return E_OK;
}

#if ((DET_ENABLE_ASR32_SERVICE_API == STD_ON) || (DET_DEFAULT_ASR_SERVICE_API == DET_SERVICE_API_ASR32))
/* AUTOSAR 3.2 wraper function for reporting development errors */
FUNC(void, DET_CODE) Det_ASR32_ReportError(
  uint16 ModuleId,
  uint8  InstanceId,
  uint8  ApiId,
  uint8  ErrorId)
{
  DBG_DET_ASR32_REPORTERROR_ENTRY(ModuleId,InstanceId,ApiId,ErrorId);

  /* Casting to void because according to AUTOSAR 3.2 there is no
   * return value*/
  (void) Det_ASR40_ReportError(ModuleId,InstanceId, ApiId, ErrorId);

  DBG_DET_ASR32_REPORTERROR_EXIT(ModuleId,InstanceId,ApiId,ErrorId);
}
#endif


#if (DET_VERSION_INFO_API == STD_ON)
FUNC(void, DET_CODE) Det_GetVersionInfo(
  CONSTP2VAR(Std_VersionInfoType, AUTOMATIC, DET_APPL_DATA) VersionInfoPtr)
{
  DBG_DET_GETVERSIONINFO_ENTRY(VersionInfoPtr);
  if (VersionInfoPtr != NULL_PTR)
  {
    VersionInfoPtr->vendorID         = DET_VENDOR_ID;
    VersionInfoPtr->moduleID         = DET_MODULE_ID;
    VersionInfoPtr->sw_major_version = DET_SW_MAJOR_VERSION;
    VersionInfoPtr->sw_minor_version = DET_SW_MINOR_VERSION;
    VersionInfoPtr->sw_patch_version = DET_SW_PATCH_VERSION;
  }
  else
  {
    /* DET023, DET051, DET052 */
    /* call all notification functions which are configured */
    (void)Det_ASR40_ReportError(
    DET_MODULE_ID, 0U, DET_SID_GET_VERSION_INFO, DET_E_PARAM_POINTER);
  }

   DBG_DET_GETVERSIONINFO_EXIT(VersionInfoPtr);
}
#endif

/*------------------[defensive programming]---------------------------------*/
#if (TS_ARCH_FAMILY == TS_WINDOWS)

#if (DET_PRECONDITION_ASSERT_ENABLED == STD_ON)
FUNC(void, DET_CODE) Det_PreconditionAssertPrint(
  uint8 ModuleId, uint8 InstanceId, uint8 ApiId,
  const char *File, uint32 Line, const char *Function,
  const char *Condition)
{
  DBG_DET_PRECONDITIONASSERTPRINT_ENTRY(ModuleId,InstanceId,ApiId,File,Line,Function,Condition);

  /* Deviation MISRA-1, MISRA-2 <+7> */
  (void)fprintf(
    stderr,
    "%s:%u: In function %s (ApiId %u):\n"
    "error: precondition assertion \"%s\" failed "
    "in instance %u of module %u\n",
    File, (unsigned int)Line, Function, ApiId,
    Condition, InstanceId, ModuleId);

  DBG_DET_PRECONDITIONASSERTPRINT_EXIT(ModuleId,InstanceId,ApiId,File,Line,Function,Condition);
}
#endif

#if (DET_POSTCONDITION_ASSERT_ENABLED == STD_ON)
FUNC(void, DET_CODE) Det_PostconditionAssertPrint(
  uint8 ModuleId, uint8 InstanceId, uint8 ApiId,
  const char *File, uint32 Line, const char *Function,
  const char *Condition)
{
  DBG_DET_POSTCONDITIONASSERTPRINT_ENTRY(ModuleId,InstanceId,ApiId,File,Line,Function,Condition);

  /* Deviation MISRA-1, MISRA-2 <+7> */
  (void)fprintf(
    stderr,
    "%s:%u: In function %s (ApiId %u):\n"
    "error: postcondition assertion \"%s\" failed "
    "in instance %u of module %u\n",
    File, (unsigned int)Line, Function, ApiId,
    Condition, InstanceId, ModuleId);

  DBG_DET_POSTCONDITIONASSERTPRINT_EXIT(ModuleId,InstanceId,ApiId,File,Line,Function,Condition);
}
#endif

#if (DET_UNREACHABLE_CODE_ASSERT_ENABLED == STD_ON)
FUNC(void, DET_CODE) Det_UnreachableCodeAssertPrint(
  uint8 ModuleId, uint8 InstanceId, uint8 ApiId,
  const char *File, uint32 Line, const char *Function)
{
  DBG_DET_UNREACHABLECODEASSERTPRINT_ENTRY(ModuleId,InstanceId,ApiId,File,Line,Function);

  /* Deviation MISRA-1, MISRA-2 <+7> */
  (void)fprintf(
    stderr,
    "%s:%u: In function %s (ApiId %u):\n"
    "error: unreachable code assertion failed "
    "in instance %u of module %u\n",
    File, (unsigned int)Line, Function, ApiId,
    InstanceId, ModuleId);

  DBG_DET_UNREACHABLECODEASSERTPRINT_EXIT(ModuleId,InstanceId,ApiId,File,Line,Function);
}
#endif

#if (DET_INVARIANT_ASSERT_ENABLED == STD_ON)
FUNC(void, DET_CODE) Det_InvariantAssertPrint(
  uint8 ModuleId, uint8 InstanceId, uint8 ApiId,
  const char *File, uint32 Line, const char *Function,
  const char *Condition)
{
  DBG_DET_INVARIANTASSERTPRINT_ENTRY(ModuleId,InstanceId,ApiId,File,Line,Function,Condition);

  /* Deviation MISRA-1, MISRA-2 <+7> */
  (void)fprintf(
    stderr,
    "%s:%u: In function %s (ApiId %u):\n"
    "error: invariant assertion \"%s\" failed "
    "in instance %u of module %u\n",
    File, (unsigned int)Line, Function, ApiId,
    Condition, InstanceId, ModuleId);

  DBG_DET_INVARIANTASSERTPRINT_EXIT(ModuleId,InstanceId,ApiId,File,Line,Function,Condition);
}
#endif

#endif /* if (TS_ARCH_FAMILY == TS_WINDOWS) */

/*==================[internal function definitions]=========================*/

#define DET_STOP_SEC_CODE
/* !LINKSTO DET006,1 */
#include <MemMap.h>

/*==================[end of file]=================================*/
