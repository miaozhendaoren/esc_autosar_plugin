/**
 * \file
 *
 * \brief AUTOSAR Det
 *
 * This file contains the implementation of the AUTOSAR
 * module Det.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]=================================================================*/

#include <Det_Trace.h>
#include <Std_Types.h>                                                 /* AUTOSAR standard types */

#define DET_INTERNAL_USAGE
#include <Det_BSW.h>                                              /* Declaration of Det BSW APIs */

#if (DET_RTE_USAGE == STD_ON)
#if (DET_ENABLE_ASR40_SERVICE_API == STD_ON)
# include <Rte_DevelopmentErrorTracer.h>

/*==================[macros]=====================================================================*/

/*==================[type definitions]===========================================================*/

/*==================[external function declarations]=============================================*/

/*==================[internal function declarations]=============================================*/

/*==================[external constants]=========================================================*/

/*==================[internal constants]=========================================================*/

/*==================[external data]==============================================================*/

/*==================[internal data]==============================================================*/

/*==================[internal function definitions]==============================================*/

/*==================[external function definitions]==============================================*/

#define DET_START_SEC_CODE
/* !LINKSTO DET006,1 */
#include <MemMap.h>

FUNC(Std_ReturnType, DET_CODE) Det_ASR40_Rte_ReportError(
   uint16 pdav0, /* Port defined argument for ModuleId */
   uint8  InstanceId,
   uint8  ApiId,
   uint8  ErrorId)
{
  Std_ReturnType retVal;
  DBG_DET_ASR40_RTE_REPORTERROR_ENTRY(pdav0,InstanceId,ApiId,ErrorId);

  retVal = Det_ASR40_ReportError(pdav0, InstanceId, ApiId, ErrorId);

  DBG_DET_ASR40_RTE_REPORTERROR_EXIT(retVal,pdav0,InstanceId,ApiId,ErrorId);
  return retVal;
}

#define DET_STOP_SEC_CODE
/* !LINKSTO DET006,1 */
#include <MemMap.h>

#endif /* (DET_ENABLE_ASR40_SERVICE_API == STD_ON) */
#endif /* (DET_RTE_USAGE == STD_ON) */

#if ((DET_RTE_USAGE == STD_OFF) || \
     (DET_ENABLE_ASR40_SERVICE_API == STD_OFF))

#include <TSCompiler.h>           /* usage of macro in TSCompiler_Default.h */

/* Avoid empty translation unit according to ISO C90 */
TS_PREVENTEMPTYTRANSLATIONUNIT

#endif /* ((DET_RTE_USAGE == STD_OFF) || (DET_ENABLE_ASR40_SERVICE_API == STD_OFF)) */

/*==================[end of file]================================================================*/
