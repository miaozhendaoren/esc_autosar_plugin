<?xml version="1.0" encoding="UTF-8"?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3_STRICT_COMPACT.xsd">
[!/* do not generate this file if RTE is not used */!][!//
[!SKIPFILE "DetGeneral/DetRteUsage = 'false'"!][!//
[!IF "DetGeneral/DetServiceAPI/DetDefaultASRServiceAPI = 'AUTOSAR_32'"!][!//
[!VAR "swComponentTypePackageName" = "'ComponentTypes'"!][!//
[!VAR "swcImplementationPackageName" = "'Implementations'"!][!//
[!ELSE!][!//
[!VAR "swComponentTypePackageName" = "'SwComponentTypes'"!][!//
[!VAR "swcImplementationPackageName" = "'SwcImplementations'"!][!//
[!ENDIF!][!//
  <AR-PACKAGES>
    <AR-PACKAGE>
      <SHORT-NAME>AUTOSAR_Det</SHORT-NAME>

      <ELEMENTS>
        <SWC-IMPLEMENTATION>
          <SHORT-NAME>Implementation</SHORT-NAME>
          <CODE-DESCRIPTORS>
            <CODE>
              <SHORT-NAME>Code</SHORT-NAME>
              <ARTIFACT-DESCRIPTORS>
                <AUTOSAR-ENGINEERING-OBJECT>
                  <SHORT-LABEL>EngObject</SHORT-LABEL>
                  <CATEGORY>SWSRC</CATEGORY>
                </AUTOSAR-ENGINEERING-OBJECT>
              </ARTIFACT-DESCRIPTORS>
            </CODE>
          </CODE-DESCRIPTORS>
          <PROGRAMMING-LANGUAGE>C</PROGRAMMING-LANGUAGE>
          <RESOURCE-CONSUMPTION>
            <SHORT-NAME>Resources</SHORT-NAME>
          </RESOURCE-CONSUMPTION>
          <SW-VERSION>6.3.0</SW-VERSION>
          <VENDOR-ID>1</VENDOR-ID>
          <BEHAVIOR-REF DEST="SWC-INTERNAL-BEHAVIOR">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/InternalBehavior</BEHAVIOR-REF>
        </SWC-IMPLEMENTATION>
      </ELEMENTS>

      <AR-PACKAGES>
        <AR-PACKAGE>
          <SHORT-NAME>[!"$swComponentTypePackageName"!]</SHORT-NAME>
          <ELEMENTS>
            <SERVICE-SW-COMPONENT-TYPE>
              <SHORT-NAME>DevelopmentErrorTracer</SHORT-NAME>
              <PORTS>
[!IF "DetGeneral/DetServiceAPI/DetEnableASR32ServiceAPI = 'true'"!][!//
<!-- Provide AUTOSAR 3.2 service -->
[!LOOP "SoftwareComponentList/*"!][!//                  
                <P-PORT-PROTOTYPE>
                  <SHORT-NAME>DS_ASR32_[!"node:name(.)"!]</SHORT-NAME>
                  <PROVIDED-COM-SPECS>
                    <SERVER-COM-SPEC>
                      <OPERATION-REF DEST="CLIENT-SERVER-OPERATION">/AUTOSAR_Det/ASR32/PortInterfaces/DET_ASR32_Service/ReportError</OPERATION-REF>
                      <QUEUE-LENGTH>1</QUEUE-LENGTH>
                    </SERVER-COM-SPEC>
                  </PROVIDED-COM-SPECS>
                  <PROVIDED-INTERFACE-TREF DEST="CLIENT-SERVER-INTERFACE">/AUTOSAR_Det/ASR32/PortInterfaces/DET_ASR32_Service</PROVIDED-INTERFACE-TREF>
                </P-PORT-PROTOTYPE>
[!ENDLOOP!][!//
[!ENDIF!][!//
[!IF "DetGeneral/DetServiceAPI/DetEnableASR40ServiceAPI = 'true'"!][!//
<!-- Provide AUTOSAR 4.0 service -->
[!LOOP "SoftwareComponentList/*"!][!//                  
                <P-PORT-PROTOTYPE>
                  <!-- !LINKSTO DET205,1 -->
                  <SHORT-NAME>DS_ASR40_[!"node:name(.)"!]</SHORT-NAME>
                  <PROVIDED-COM-SPECS>
                    <SERVER-COM-SPEC>
                      <OPERATION-REF DEST="CLIENT-SERVER-OPERATION">/AUTOSAR_Det/ASR40/PortInterfaces/DET_ASR40_Service/ReportError</OPERATION-REF>
                      <QUEUE-LENGTH>1</QUEUE-LENGTH>
                    </SERVER-COM-SPEC>
                  </PROVIDED-COM-SPECS>
                  <PROVIDED-INTERFACE-TREF DEST="CLIENT-SERVER-INTERFACE">/AUTOSAR_Det/ASR40/PortInterfaces/DET_ASR40_Service</PROVIDED-INTERFACE-TREF>
                </P-PORT-PROTOTYPE>
[!ENDLOOP!][!//
[!ENDIF!][!//
[!IF "DetGeneral/DetServiceAPI/DetDefaultASRServiceAPI != 'NONE'"!][!//
<!-- Provide default service -->
[!LOOP "SoftwareComponentList/*"!][!//                  
                <P-PORT-PROTOTYPE>
                  <SHORT-NAME>DS_[!"node:name(.)"!]</SHORT-NAME>
                  <PROVIDED-COM-SPECS>
                    <SERVER-COM-SPEC>
                      <OPERATION-REF DEST="CLIENT-SERVER-OPERATION">/AUTOSAR_Det/PortInterfaces/DETService/ReportError</OPERATION-REF>
                      <QUEUE-LENGTH>1</QUEUE-LENGTH>
                    </SERVER-COM-SPEC>
                  </PROVIDED-COM-SPECS>
                  <PROVIDED-INTERFACE-TREF DEST="CLIENT-SERVER-INTERFACE">/AUTOSAR_Det/PortInterfaces/DETService</PROVIDED-INTERFACE-TREF>
                </P-PORT-PROTOTYPE>
[!ENDLOOP!][!//
[!ENDIF!][!//
              </PORTS>
              <INTERNAL-BEHAVIORS>
                <SWC-INTERNAL-BEHAVIOR>
                  <SHORT-NAME>InternalBehavior</SHORT-NAME>
                  <EVENTS>
[!IF "DetGeneral/DetServiceAPI/DetEnableASR32ServiceAPI = 'true'"!][!//
<!-- Provide AUTOSAR 3.2 service -->
[!LOOP "SoftwareComponentList/*"!][!//
                    <OPERATION-INVOKED-EVENT>
                      <SHORT-NAME>EV_ASR32_ReportError_[!"node:name(.)"!]</SHORT-NAME>
                      <START-ON-EVENT-REF DEST="RUNNABLE-ENTITY">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/InternalBehavior/RE_ASR32_ReportError</START-ON-EVENT-REF>
                      <OPERATION-IREF>
                        <CONTEXT-P-PORT-REF DEST="P-PORT-PROTOTYPE">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/DS_ASR32_[!"node:name(.)"!]</CONTEXT-P-PORT-REF>
                        <TARGET-PROVIDED-OPERATION-REF DEST="CLIENT-SERVER-OPERATION">/AUTOSAR_Det/ASR32/PortInterfaces/DET_ASR32_Service/ReportError</TARGET-PROVIDED-OPERATION-REF>
                      </OPERATION-IREF>
                    </OPERATION-INVOKED-EVENT>
[!ENDLOOP!][!//
[!ENDIF!][!//
[!IF "DetGeneral/DetServiceAPI/DetEnableASR40ServiceAPI = 'true'"!][!//
<!-- Provide AUTOSAR 4.0 service -->
[!LOOP "SoftwareComponentList/*"!][!//
                    <OPERATION-INVOKED-EVENT>
                      <SHORT-NAME>EV_ASR40_ReportError_[!"node:name(.)"!]</SHORT-NAME>
                      <START-ON-EVENT-REF DEST="RUNNABLE-ENTITY">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/InternalBehavior/RE_ASR40_ReportError</START-ON-EVENT-REF>
                      <OPERATION-IREF>
                        <CONTEXT-P-PORT-REF DEST="P-PORT-PROTOTYPE">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/DS_ASR40_[!"node:name(.)"!]</CONTEXT-P-PORT-REF>
                        <TARGET-PROVIDED-OPERATION-REF DEST="CLIENT-SERVER-OPERATION">/AUTOSAR_Det/ASR40/PortInterfaces/DET_ASR40_Service/ReportError</TARGET-PROVIDED-OPERATION-REF>
                      </OPERATION-IREF>
                    </OPERATION-INVOKED-EVENT>
[!ENDLOOP!][!//
[!ENDIF!][!//
[!IF "DetGeneral/DetServiceAPI/DetDefaultASRServiceAPI != 'NONE'"!][!//
<!-- Provide default service -->
[!LOOP "SoftwareComponentList/*"!][!//
                    <OPERATION-INVOKED-EVENT>
                      <SHORT-NAME>EV_ReportError_[!"node:name(.)"!]</SHORT-NAME>
                      <START-ON-EVENT-REF DEST="RUNNABLE-ENTITY">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/InternalBehavior/RE_ReportError</START-ON-EVENT-REF>
                      <OPERATION-IREF>
                        <CONTEXT-P-PORT-REF DEST="P-PORT-PROTOTYPE">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/DS_[!"node:name(.)"!]</CONTEXT-P-PORT-REF>
                        <TARGET-PROVIDED-OPERATION-REF DEST="CLIENT-SERVER-OPERATION">/AUTOSAR_Det/PortInterfaces/DETService/ReportError</TARGET-PROVIDED-OPERATION-REF>
                      </OPERATION-IREF>
                    </OPERATION-INVOKED-EVENT>
[!ENDLOOP!][!//
[!ENDIF!][!//
                  </EVENTS>
                  <HANDLE-TERMINATION-AND-RESTART>NO-SUPPORT</HANDLE-TERMINATION-AND-RESTART>
                  <PORT-API-OPTIONS>
[!IF "DetGeneral/DetServiceAPI/DetEnableASR32ServiceAPI = 'true'"!][!//
<!-- Provide AUTOSAR 3.2 service -->
[!LOOP "SoftwareComponentList/*"!][!//
                    <PORT-API-OPTION>
                      <ENABLE-TAKE-ADDRESS>false</ENABLE-TAKE-ADDRESS>
                      <INDIRECT-API>false</INDIRECT-API>
                      <PORT-ARG-VALUES>
                        <!-- !LINKSTO DET206,1 -->
                        <PORT-DEFINED-ARGUMENT-VALUE>
                          <VALUE>
                            <NUMERICAL-VALUE-SPECIFICATION>
                              <VALUE>[!"ModuleId"!]</VALUE>
                            </NUMERICAL-VALUE-SPECIFICATION>
                          </VALUE>
                          <VALUE-TYPE-TREF DEST="IMPLEMENTATION-DATA-TYPE">/AUTOSAR_Platform/ImplementationDataTypes/uint16</VALUE-TYPE-TREF>
                        </PORT-DEFINED-ARGUMENT-VALUE>
                      </PORT-ARG-VALUES>
                      <PORT-REF DEST="P-PORT-PROTOTYPE">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/DS_ASR32_[!"node:name(.)"!]</PORT-REF>
                    </PORT-API-OPTION>
[!ENDLOOP!][!//
[!ENDIF!][!//
[!IF "DetGeneral/DetServiceAPI/DetEnableASR40ServiceAPI = 'true'"!][!//
<!-- Provide AUTOSAR 4.0 service -->
[!LOOP "SoftwareComponentList/*"!][!//
                    <PORT-API-OPTION>
                      <ENABLE-TAKE-ADDRESS>false</ENABLE-TAKE-ADDRESS>
                      <INDIRECT-API>false</INDIRECT-API>
                      <PORT-ARG-VALUES>
                        <PORT-DEFINED-ARGUMENT-VALUE>
                          <VALUE>
                            <NUMERICAL-VALUE-SPECIFICATION>
                              <VALUE>[!"ModuleId"!]</VALUE>
                            </NUMERICAL-VALUE-SPECIFICATION>
                          </VALUE>
                          <VALUE-TYPE-TREF DEST="IMPLEMENTATION-DATA-TYPE">/AUTOSAR_Platform/ImplementationDataTypes/uint16</VALUE-TYPE-TREF>
                        </PORT-DEFINED-ARGUMENT-VALUE>
                      </PORT-ARG-VALUES>
                      <PORT-REF DEST="P-PORT-PROTOTYPE">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/DS_ASR40_[!"node:name(.)"!]</PORT-REF>
                    </PORT-API-OPTION>
[!ENDLOOP!][!//
[!ENDIF!][!//
[!IF "DetGeneral/DetServiceAPI/DetDefaultASRServiceAPI != 'NONE'"!][!//
<!-- Provide default service -->
[!LOOP "SoftwareComponentList/*"!][!//
                    <PORT-API-OPTION>
                      <ENABLE-TAKE-ADDRESS>false</ENABLE-TAKE-ADDRESS>
                      <INDIRECT-API>false</INDIRECT-API>
                      <PORT-ARG-VALUES>
                        <PORT-DEFINED-ARGUMENT-VALUE>
                          <VALUE>
                            <NUMERICAL-VALUE-SPECIFICATION>
                              <VALUE>[!"ModuleId"!]</VALUE>
                            </NUMERICAL-VALUE-SPECIFICATION>
                          </VALUE>
                          <VALUE-TYPE-TREF DEST="IMPLEMENTATION-DATA-TYPE">/AUTOSAR_Platform/ImplementationDataTypes/uint16</VALUE-TYPE-TREF>
                        </PORT-DEFINED-ARGUMENT-VALUE>
                      </PORT-ARG-VALUES>
                      <PORT-REF DEST="P-PORT-PROTOTYPE">/AUTOSAR_Det/[!"$swComponentTypePackageName"!]/DevelopmentErrorTracer/DS_[!"node:name(.)"!]</PORT-REF>
                    </PORT-API-OPTION>
[!ENDLOOP!][!//
[!ENDIF!][!//
                  </PORT-API-OPTIONS>
                  <RUNNABLES>
[!IF "DetGeneral/DetServiceAPI/DetEnableASR32ServiceAPI = 'true'"!][!//
<!-- Provide AUTOSAR 3.2 service -->
                    <RUNNABLE-ENTITY>
                      <SHORT-NAME>RE_ASR32_ReportError</SHORT-NAME>
                      <MINIMUM-START-INTERVAL>0</MINIMUM-START-INTERVAL>
                      <CAN-BE-INVOKED-CONCURRENTLY>true</CAN-BE-INVOKED-CONCURRENTLY>
                      <SYMBOL>Det_ASR32_Rte_ReportError</SYMBOL>
                    </RUNNABLE-ENTITY>
[!ENDIF!][!//
[!IF "DetGeneral/DetServiceAPI/DetEnableASR40ServiceAPI = 'true'"!][!//
<!-- Provide AUTOSAR 4.0 service -->
                    <RUNNABLE-ENTITY>
                      <SHORT-NAME>RE_ASR40_ReportError</SHORT-NAME>
                      <MINIMUM-START-INTERVAL>0</MINIMUM-START-INTERVAL>
                      <CAN-BE-INVOKED-CONCURRENTLY>true</CAN-BE-INVOKED-CONCURRENTLY>
                      <SYMBOL>Det_ASR40_Rte_ReportError</SYMBOL>
                    </RUNNABLE-ENTITY>
[!ENDIF!][!//
[!IF "DetGeneral/DetServiceAPI/DetDefaultASRServiceAPI != 'NONE'"!][!//
<!-- Provide default service -->
                    <RUNNABLE-ENTITY>
                      <SHORT-NAME>RE_ReportError</SHORT-NAME>
                      <MINIMUM-START-INTERVAL>0</MINIMUM-START-INTERVAL>
                      <CAN-BE-INVOKED-CONCURRENTLY>true</CAN-BE-INVOKED-CONCURRENTLY>
                      <SYMBOL>Det_Rte_ReportError</SYMBOL>
                    </RUNNABLE-ENTITY>
[!ENDIF!][!//
                  </RUNNABLES>
                  <SUPPORTS-MULTIPLE-INSTANTIATION>false</SUPPORTS-MULTIPLE-INSTANTIATION>
                </SWC-INTERNAL-BEHAVIOR>
              </INTERNAL-BEHAVIORS>
            </SERVICE-SW-COMPONENT-TYPE>
          </ELEMENTS>
        </AR-PACKAGE>
      </AR-PACKAGES>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
