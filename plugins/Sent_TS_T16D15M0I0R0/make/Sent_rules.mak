# \file
#
# \brief AUTOSAR Sent
#
# This file contains the implementation of the AUTOSAR
# module Sent.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Sent_src

Sent_src_FILES       += $(Sent_CORE_PATH)\src\Sent.c
Sent_src_FILES       += $(Sent_CORE_PATH)\src\Sent_Irq.c
Sent_src_FILES       += $(Sent_OUTPUT_PATH)\src\Sent_LCfg.c
Sent_src_FILES       += $(Sent_OUTPUT_PATH)\src\Sent_PBCfg.c
