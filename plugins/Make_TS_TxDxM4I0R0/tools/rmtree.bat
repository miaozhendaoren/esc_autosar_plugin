@echo off
:start
set source=%1
if "%source%"=="" goto end

REM Substitute slashes with backslashes
set source=%source:/=\%

REM Substitute double backslash with single backslash
REM Reason: A path with a double backslash is interpreted as
REM a network path
set source=%source:\\=\%

if exist %source% rmdir /Q/S %source% || echo Error: Couldn't remove %source%
shift
goto start
:end
