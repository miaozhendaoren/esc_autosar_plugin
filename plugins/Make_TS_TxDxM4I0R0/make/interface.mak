# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# This file belongs to the implementation of the base_make package.
# Interface.mak contains the implementation of the Basic Software
# Makefile Interface.


#################################################################
# The next code block processes the contents of the variable
# LIBRARIES_TO_BUILD.


#################################################################
# The next define returns the output path for a object file.
#
define GET_OBJ_OUTPUT_PATH
$(if $($(strip $(basename $(notdir $(subst \,/,$(1)))))_OBJ_OUTPUT_PATH), $($(strip $(basename $(notdir $(subst \,/,$(1)))))_OBJ_OUTPUT_PATH), $(OBJ_OUTPUT_PATH))
endef

#################################################################
# The next define returns the output path for a library file.
#
define GET_LIB_OUTPUT_PATH
$(if $($(strip $(basename $(notdir $(subst \,/,$(1)))))_LIB_OUTPUT_PATH), $($(strip $(basename $(notdir $(subst \,/,$(1)))))_LIB_OUTPUT_PATH), $(LIB_OUTPUT_PATH))
endef

#################################################################
# The variables with the prefix TEMP_ are only local variables which
# should not used outside the file interface.mak.

#################################################################
# At first a list of all source files (*.c, *.cpp, *.asm) which belongs to a
# library, will be created. If a general directory is specified for all source
# files of a library (_PATH), it will be used as prefix. Afterwards the
# different classes of source files (c, cpp and asm) will be filtered and
# saved in additional temporary variables.
#
# The next example shows how the two libraries oslib and canlib and their
# associated source files os.c, osAsm.asm, can_drv.c can_drv.cpp will be
# processed:
#
# LIBRARIES_TO_BUILD = oslib canlib
#
# oslib_FILES = $(SSC_ROOT)\core\os\src\os.c  $(SSC_ROOT)\core\os\src\osAsm.asm
# canlib_FILES = $(SSC_ROOT)\core\com\can\driver\can_drv.c $(SSC_ROOT)\core\com\can\driver\can_drv.cpp
#
# -->
#
# TEMP_FILES_OUT_OF_LIBRARIES_TO_BUILD = $(SSC_ROOT)\core\os\src\os.c \
#   $(SSC_ROOT)\core\os\src\osAsm.asm                                    \
#   $(SSC_ROOT)\core\com\can\driver\can_drv.c                         \
#   $(SSC_ROOT)\core\com\can\driver\can_state.cpp
#
# TEMP_CC_FILES_OUT_OF_LIBRARIES_TO_BUILD = $(SSC_ROOT)\core\os\src\os.c \
#   $(SSC_ROOT)\core\com\can\driver\can_drv.c
#
# TEMP_ASM_FILES_OUT_OF_LIBRARIES_TO_BUILD = $(SSC_ROOT)\core\os\src\osAsm.asm
#
# TEMP_CPP_FILES_OUT_OF_LIBRARIES_TO_BUILD = $(SSC_ROOT)\core\com\can\driver\can_state.cpp
#
# TEMP_CC_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD  = $(PROJECT_OUTPUT_PATH)\obj\os.obj \
#   $(PROJECT_OUTPUT_PATH)\obj\can_drv.obj
#
# TEMP_ASM_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD =  $(PROJECT_OUTPUT_PATH)\obj\osAsm.obj
#
# TEMP_CPP_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD =  $(PROJECT_OUTPUT_PATH)\obj\can_state.obj
#
ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
LIBRARIES_TO_BUILD := $(LIBRARIES_PBCFG_TO_BUILD)
endif

TEMP_FILES_OUT_OF_LIBRARIES_TO_BUILD = \
$(foreach LIB,$(LIBRARIES_TO_BUILD),\
    $(foreach FILE,\
      $(if $(strip $($(LIB)_FILES)),\
        $($(LIB)_FILES),\
        $(error $(ERROR_HEADER) No source files are specified for the library $(LIB). Please set the variable $(LIB)_FILES.)\
       ),\
      $(if $($(LIB)_PATH),$($(LIB)_PATH)\$(FILE),$(FILE))\
     )\
   )

TEMP_CC_FILES_OUT_OF_LIBRARIES_TO_BUILD  = \
	$(filter %.$(CC_FILE_SUFFIX),$(TEMP_FILES_OUT_OF_LIBRARIES_TO_BUILD))\
	$(filter %.$(CC_FILE_SUFFIX_2),$(TEMP_FILES_OUT_OF_LIBRARIES_TO_BUILD))

TEMP_ASM_FILES_OUT_OF_LIBRARIES_TO_BUILD = \
	$(filter %.$(ASM_FILE_SUFFIX),$(TEMP_FILES_OUT_OF_LIBRARIES_TO_BUILD))\
	$(filter %.$(ASM_FILE_SUFFIX_2),$(TEMP_FILES_OUT_OF_LIBRARIES_TO_BUILD))

TEMP_CPP_FILES_OUT_OF_LIBRARIES_TO_BUILD = \
	$(filter %.$(CPP_FILE_SUFFIX),$(TEMP_FILES_OUT_OF_LIBRARIES_TO_BUILD))\
	$(filter %.$(CPP_FILE_SUFFIX_2),$(TEMP_FILES_OUT_OF_LIBRARIES_TO_BUILD))

TEMP_CC_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD =\
$(foreach SRC, $(TEMP_CC_FILES_OUT_OF_LIBRARIES_TO_BUILD), $(call GET_OBJ_OUTPUT_PATH,$(SRC))\$(basename $(notdir $(subst \,/,$(SRC)))).$(OBJ_FILE_SUFFIX))

TEMP_ASM_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD=\
$(foreach SRC, $(TEMP_ASM_FILES_OUT_OF_LIBRARIES_TO_BUILD), $(call GET_OBJ_OUTPUT_PATH,$(SRC))\$(basename $(notdir $(subst \,/,$(SRC)))).$(OBJ_FILE_SUFFIX))

TEMP_CPP_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD=\
$(foreach SRC, $(TEMP_CPP_FILES_OUT_OF_LIBRARIES_TO_BUILD), $(call GET_OBJ_OUTPUT_PATH,$(SRC))\$(basename $(notdir $(subst \,/,$(SRC)))).$(OBJ_FILE_SUFFIX))


#################################################################
# CC_FILES_TO_BUILD contains a list of all C source files which
# must be compiled and linked. Normally, C source files should be
# specified with the absolute path. If only the file name is specified,
# the source file will be searched in the APPL_PROJECT_PATH
# ( $(PROJECT_ROOT)\source\application).
#
# For example:
# CC_FILES_TO_BUILD = $(PROJECT_ROOT)\source\diag\com1.c \
#    $(PROJECT_ROOT)\source\network\com_config.c \
#    appl1.c appl2.c \
#    $(PROJECT_ROOT)\source\network\test.c
#
# -->
#
# TEMP_CC_FILES_OUT_OF_CC_FILES_TO_BUILD = $(PROJECT_OUTPUT_PATH)\diag\com1.c \
#    $(PROJECT_ROOT)\source\network\com_config.c \
#    $(PROJECT_ROOT)\source\application\appl1.c  \
#    $(PROJECT_ROOT)\source\application\appl2.c  \
#    $(PROJECT_ROOT)\source\network\test.c
#
# TEMP_CC_TO_OBJ_FILE_OUT_OF_CC_FILES_TO_BUILD =  $(PROJECT_OUTPUT_PATH)\obj\com1.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\com_config.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\appl1.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\appl2.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\test.obj
#
ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
TEMP_CC_FILES_OUT_OF_CC_FILES_TO_BUILD =
else
TEMP_CC_FILES_OUT_OF_CC_FILES_TO_BUILD =\
$(foreach SRC, $(CC_FILES_TO_BUILD), \
  $(if $(word 2,$(subst /, ,$(subst \,/,$(SRC)))),\
    $(SRC),\
    $(addprefix $(APPL_PROJECT_PATH)\,$(SRC))\
  )\
)
endif

TEMP_CC_TO_OBJ_FILE_OUT_OF_CC_FILES_TO_BUILD =\
$(foreach SRC, $(TEMP_CC_FILES_OUT_OF_CC_FILES_TO_BUILD), $(call GET_OBJ_OUTPUT_PATH,$(SRC))\$(basename $(notdir $(subst \,/,$(SRC)))).$(OBJ_FILE_SUFFIX))


#################################################################
# The variable CPP_FILES_TO_BUILD will be handled like the variable
# CC_FILES_TO_BUILD. Please read the comment above.
#
ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
TEMP_CPP_FILES_OUT_OF_CPP_FILES_TO_BUILD =
else
TEMP_CPP_FILES_OUT_OF_CPP_FILES_TO_BUILD =\
$(foreach SRC, $(CPP_FILES_TO_BUILD), \
  $(if $(word 2,$(subst /, ,$(subst \,/,$(SRC)))),\
    $(SRC),\
    $(addprefix $(APPL_PROJECT_PATH)\,$(SRC))\
  )\
)
endif

TEMP_CPP_TO_OBJ_FILE_OUT_OF_CPP_FILES_TO_BUILD =\
$(foreach SRC, $(TEMP_CPP_FILES_OUT_OF_CPP_FILES_TO_BUILD), $(call GET_OBJ_OUTPUT_PATH,$(SRC))\$(basename $(notdir $(subst \,/,$(SRC)))).$(OBJ_FILE_SUFFIX))


#################################################################
# The variable ASM_FILES_TO_BUILD will be handled like the variable
# CC_FILES_TO_BUILD. Please read the comment above.
#
ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
TEMP_ASM_FILES_OUT_OF_ASM_FILES_TO_BUILD =
else
TEMP_ASM_FILES_OUT_OF_ASM_FILES_TO_BUILD =\
$(foreach SRC, $(ASM_FILES_TO_BUILD), \
  $(if $(word 2,$(subst /, ,$(subst \,/,$(SRC)))),\
    $(SRC),\
    $(addprefix $(APPL_PROJECT_PATH)\,$(SRC))\
  )\
)
endif

TEMP_ASM_TO_OBJ_FILE_OUT_OF_ASM_FILES_TO_BUILD =\
$(foreach SRC, $(TEMP_ASM_FILES_OUT_OF_ASM_FILES_TO_BUILD), $(call GET_OBJ_OUTPUT_PATH,$(SRC))\$(basename $(notdir $(subst \,/,$(SRC)))).$(OBJ_FILE_SUFFIX))

#################################################################
# For example:
# LIBRARIES_TO_BUILD = oslib canlib
#
# oslib_FILES  = $(SSC_ROOT)\core\os\src\os.c $(SSC_ROOT)\core\os\src\osAsm.asm
# canlib_FILES = $(SSC_ROOT)\core\com\can\driver\can_drv.c $(SSC_ROOT)\core\com\can\driver\can_drv.cpp
#
# CC_FILES_TO_BUILD = $(PROJECT_ROOT)\source\diag\com1.c \
#    $(PROJECT_ROOT)\source\network\com_config.c \
#    appl1.c appl2.c \
#    $(PROJECT_ROOT)\source\network\test.c
#
# ASM_FILES_TO_BUILD = $(PROJECT_ROOT)\source\asm\startup.asm
#
# -->
#
# TEMP_ALL_CC_FILES = $(SSC_ROOT)\core\os\src\os.c \
#    $(SSC_ROOT)\core\com\can\driver\can_drv.c \
#    $(PROJECT_ROOT)\source\diag\com1.c \
#    $(PROJECT_ROOT)\source\network\com_config.c \
#    $(PROJECT_ROOT)\source\application\appl1.c   \
#    $(PROJECT_ROOT)\source\application\appl2.c   \
#    $(PROJECT_ROOT)\source\network\test.c
#
# TEMP_ALL_ASM_FILES = $(SSC_ROOT)\core\os\src\osAsm.asm \
#   $(PROJECT_ROOT)\source\asm\startup.asm
#
# TEMP_ALL_CPP_FILES = $(SSC_ROOT)\core\com\can\driver\can_state.cpp
#
# CC_TO_OBJ_BUILD_LIST = \
#    $(PROJECT_OUTPUT_PATH)\obj\os.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\can_drv.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\com1.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\com_config.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\appl1.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\appl2.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\test.obj
#
# CPP_TO_OBJ_BUILD_LIST = $(PROJECT_OUTPUT_PATH)\obj\can_state.obj
#
# ASM_TO_OBJ_BUILD_LIST = $(PROJECT_OUTPUT_PATH)\obj\osAsm.obj \
#    $(PROJECT_OUTPUT_PATH)\obj\startup.obj



#################################################################
# TEMP_ALL_CC_FILES contains a list of all C source files which
# are configured in the user configuration and in the basic software
# makefile plugins.
ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
TEMP_ALL_CC_FILES =  $(TEMP_CC_FILES_OUT_OF_LIBRARIES_TO_BUILD)
else
TEMP_ALL_CC_FILES = \
	$(TEMP_CC_FILES_OUT_OF_CC_FILES_TO_BUILD) \
	$(TEMP_CC_FILES_OUT_OF_LIBRARIES_TO_BUILD)
endif

#################################################################
# TEMP_ALL_CPP_FILES contains a list of all C++ files which
# are configured in the user configuration and in the basic software
# makefile plugins.
ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
TEMP_ALL_CPP_FILES = $(TEMP_CPP_FILES_OUT_OF_LIBRARIES_TO_BUILD)
else
TEMP_ALL_CPP_FILES =\
	$(TEMP_CPP_FILES_OUT_OF_CPP_FILES_TO_BUILD) \
	$(TEMP_CPP_FILES_OUT_OF_LIBRARIES_TO_BUILD)
endif

#################################################################
# TEMP_ALL_ASM_FILES contains a list of all assembler files which
# are configured in the user configuration and in the basic software
# makefile plugins.
ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
TEMP_ALL_ASM_FILES = $(TEMP_ASM_FILES_OUT_OF_LIBRARIES_TO_BUILD)
else
TEMP_ALL_ASM_FILES =\
	$(TEMP_ASM_FILES_OUT_OF_ASM_FILES_TO_BUILD) \
	$(TEMP_ASM_FILES_OUT_OF_LIBRARIES_TO_BUILD)
endif

#################################################################
# Compiler makefile interface
#################################################################

#################################################################
# The variable CC_TO_OBJ_BUILD_LIST contains a list of all
# object files which must be built out of C source files.
# This variable should be used by the compiler makefile plugin to
# write a rule that compiles all C source files. The define getSourceFile
# could be used to get the C source file for a object file:
#
# For example (in the compiler makefile plugin):
#
# $(CC_TO_OBJ_BUILD_LIST) :
#     <CompilerBinary> <ComilerOptions> -o $@ $(getSourceFile)
#
# The variables CPP_TO_OBJ_BUILD_LIST and ASM_TO_OBJ_BUILD_LIST
# are similar. An example can be found above.
#
CC_TO_OBJ_BUILD_LIST  = \
	$(TEMP_CC_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD)\
	$(TEMP_CC_TO_OBJ_FILE_OUT_OF_CC_FILES_TO_BUILD)

CPP_TO_OBJ_BUILD_LIST = \
	$(TEMP_CPP_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD)\
	$(TEMP_CPP_TO_OBJ_FILE_OUT_OF_CPP_FILES_TO_BUILD)

ASM_TO_OBJ_BUILD_LIST = \
	$(TEMP_ASM_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD)\
	$(TEMP_ASM_TO_OBJ_FILE_OUT_OF_ASM_FILES_TO_BUILD)


#################################################################
# The variable OBJ_TO_LIB_BUILD_LIST contains a list of all libraries.
# The variable LIBRARIES_TO_BUILD is used as input.  OBJ_TO_LIB_BUILD_LIST
# should be used by the compiler makefile plugin.
#
# For example:
# LIBRARIES_TO_BUILD = oslib canlib
#
# -->
#
# OBJ_TO_LIB_BUILD_LIST = $(PROJECT_OUTPUT_PATH)\lib\oslib.lib \
#    $(PROJECT_OUTPUT_PATH)\lib\canlib.lib
#
OBJ_TO_LIB_BUILD_LIST =$(subst /,\,$(foreach LIB,$(LIBRARIES_TO_BUILD),$(call GET_LIB_OUTPUT_PATH,$(LIB))\$(addsuffix .$(LIB_FILE_SUFFIX),$(LIB))))


#################################################################
# To solve the dependencies for source file (which header files are included
# by a C source file) the preprocessor of the gcc will be called for each
# source file to create a dependency makefile. This makefile contains a
# list of all header files which are included by a source file. All dependency
# makefiles are part of the output directory $(PROJECT_OUTPUT_PATH)\depend.
#
# The variable CC_TO_MAK_BUILD_LIST contains a list of all dependency makefiles.
#
# For example:
# CC_TO_MAK_BUILD_LIST = \
#    $(PROJECT_OUTPUT_PATH)\depend\os.mak\
#    $(PROJECT_OUTPUT_PATH)\depend\can_drv.mak \
#    $(PROJECT_OUTPUT_PATH)\depend\com1.mak \
#    $(PROJECT_OUTPUT_PATH)\depend\com_config.mak \
#    $(PROJECT_OUTPUT_PATH)\depend\appl1.mak \
#    $(PROJECT_OUTPUT_PATH)\depend\appl2.mak \
#    $(PROJECT_OUTPUT_PATH)\depend\test.mak
#    ...
#
# In some cases it is not possible or volitional to create a dependency makefile for
# a source file. There are two ways to disable the make depend mechanism for a source file.
#
# If a variable with the basename of a source file followed by the prefix _MAKE_DEPEND has the
# value NO, the dependency mechanism will be ignored and the user could define the correlation
# between the source file and the object file self.
#
# For example:
# appl1_MAKE_DEPEND = NO
#
CC_TO_MAK_BUILD_LIST = $(foreach SRC,$(TEMP_ALL_CC_FILES) $(TEMP_ALL_ASM_FILES) $(TEMP_ALL_CPP_FILES),\
$(if $($(basename $(notdir $(subst \,/,$(SRC))))_MAKE_DEPEND),,$(DEP_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$(SRC)))).$(MAK_FILE_SUFFIX)))

LIB_TO_MAK_BUILD_LIST = $(addsuffix .$(MAK_FILE_SUFFIX),$(addprefix $(DEP_OUTPUT_PATH)\,$(LIBRARIES_TO_BUILD)))

#################################################################
# Besides the possibility to disable the make depend mechanism for
# a single source file it is possible to define a list of "excluded"
# source files.
#
# For example:
# EXCLUDE_MAKE_DEPEND =  $(PROJECT_ROOT)\source\application\appl1.c \
#    $(PROJECT_ROOT)\source\application\appl2.c \
#    $(PROJECT_ROOT)\source\asm\startup.asm
#
# GCC_IGNORE_LIST contains the excluded dependency makefiles and allows
# to filter the variable CC_TO_MAK_BUILD_LIST.
#
# For example:
# GCC_IGNORE_LIST = \
#    $(PROJECT_OUTPUT_PATH)\depend\appl1.mak \
#    $(PROJECT_OUTPUT_PATH)\depend\appl2.mak \
#    $(PROJECT_OUTPUT_PATH)\depend\startup.mak
#
GCC_IGNORE_LIST = $(foreach SRC,$(EXCLUDE_MAKE_DEPEND),$(DEP_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$(SRC)))).$(MAK_FILE_SUFFIX))


##################################################################
# The following define expands to the include pathes (for compiler).
# The include files depend on the parameter $@.
# Therefore the define can only be used in a *.$(OBJ_FILE_SUFFIX) rule.
define GET_CC_INCLUDE_FILES
	$(call unique, $(addprefix $(INCLUDE_PREFIX),$(USER_INCLUDE_PATH) $(if $($(basename $(notdir $(subst \,/,$(@))))_INCLUDE),$($(basename $(notdir $(subst \,/,$(@))))_INCLUDE),$(CC_INCLUDE_PATH))))
endef


##################################################################
# The following define expands to the include pathes (for compiler).
# The include files depend on the parameter $@.
# Therefore the define can only be used in a *.$(OBJ_FILE_SUFFIX) rule.
define GET_CPP_INCLUDE_FILES
	$(call unique, $(addprefix $(INCLUDE_PREFIX),$(if $($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_INCLUDE),$($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_INCLUDE),$(CPP_INCLUDE_PATH))))
endef


##################################################################
# The following define expands to the include pathes (for compiler).
# The include files depend on the parameter $@.
# Therefore the define can only be used in a *.$(OBJ_FILE_SUFFIX) rule.
define GET_ASM_INCLUDE_FILES
	$(call unique, $(addprefix $(INCLUDE_PREFIX),$(if $($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_INCLUDE),$($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_INCLUDE),$(ASM_INCLUDE_PATH))))
endef



##################################################################
# As described above the gcc compiler is used to solve the
# dependencies for source files. Like the normal compiler the
# gcc required the include path for each source file. Because the
# basic software makefile interface differentiates between the
# include pathes for C, C++ and ASM files, some temporary variables
# assign each file suffix the right include path. The define
# GET_GCC_INCLUDE_FILES uses the temporary variables to return the
# right include path for a source file.

TEMP_.$(strip $(CC_FILE_SUFFIX))__   = $(CC_INCLUDE_PATH)
TEMP_.$(strip $(CC_FILE_SUFFIX_2))__ = $(CC_INCLUDE_PATH)
TEMP_.$(strip $(CPP_FILE_SUFFIX))__   = $(CPP_INCLUDE_PATH)
TEMP_.$(strip $(CPP_FILE_SUFFIX_2))__ = $(CPP_INCLUDE_PATH)
TEMP_.$(strip $(ASM_FILE_SUFFIX))__   = $(ASM_INCLUDE_PATH)
TEMP_.$(strip $(ASM_FILE_SUFFIX_2))__ = $(ASM_INCLUDE_PATH)

define GET_GCC_INCLUDE_FILES
	$(call unique, $(addprefix -I,$(if $($(subst .$(MAK_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_INCLUDE),$($(subst .$(MAK_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_INCLUDE), $(TEMP_$(strip $(suffix $(getSourceFile)))__))))
endef


#################################################################
# The following define expands to the build options for the C compiler.
#
# The C compiler flags can be specified globaly for each source files by
# the help of the variable CC_OPT and localy for each file with the suffix
# _CC_OPT. The local definition overrides the global one.
#
# For example:
# CC_OPT = -g -x -f -o3
#
# appl1_CC_OPT = -x -f
#
define GET_CC_BUILD_OPTIONS
	$(if $($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_CC_OPT),$($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_CC_OPT),$(CC_OPT) $(GET_PREPROCESSOR_DEFINES) )
endef

#################################################################
# The following define expands the build options for additional
# options
#
define GET_ADD_CC_BUILD_OPTIONS
	$(if $($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_CC_ADD_OPT),$($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_CC_ADD_OPT),$(_CC_ADD_OPT) $(GET_PREPROCESSOR_DEFINES) )
endef

#################################################################
# The following define expands to the build options for the C++ compiler
# (for details see GET_CC_BUILD_OPTIONS).
#
define GET_CPP_BUILD_OPTIONS
	$(if $($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_CPP_OPT),$($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_CPP_OPT),$(CPP_OPT) $(GET_PREPROCESSOR_DEFINES))
endef

#################################################################
# The following define expands to the build options for the assembler
# (for details see GET_CC_BUILD_OPTIONS).
#
define GET_ASM_BUILD_OPTIONS
	$(if $($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_ASM_OPT),$($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_ASM_OPT),$(ASM_OPT) $(GET_PREPROCESSOR_DEFINES))
endef

#################################################################
# The following define expands to the build options for the assembler preprocessor
# (for details see GET_CC_BUILD_OPTIONS).
#
define GET_ASS_BUILD_OPTIONS
	$(if $($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_ASS_OPT),$($(subst .$(OBJ_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_ASS_OPT),$(ASS_OPT) $(GET_PREPROCESSOR_DEFINES))
endef

#################################################################
# The define GET_LIB_BUILD_OPTIONS expands to the options which are used
# to archive a file.
# (for details see GET_CC_BUILD_OPTIONS).
#
define GET_LIB_BUILD_OPTIONS
	$(if $($(subst .$(LIB_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_LIB_OPT),$($(subst .$(LIB_FILE_SUFFIX),,$(notdir $(subst \,/,$(@))))_LIB_OPT),$(LIB_OPT))
endef

###################################################################
# Compiler makefile plugins should use the object lists CC_TO_OBJ_BUILD_LIST
# CPP_TO_OBJ_BUILD_LIST and ASM_TO_OBJ_BUILD_LIST to write rules that
# call the compiler. Within this compiler call it is necessary to know,
# which source file belongs to an object file.
# For this purpose the define getSourceFile returns the source file that
# belongs to an object file. The input parameter is the object file ($@).
#
# The way of doing this is tricky. For each source file an additional variable will be created with the suffix
# ___OBJ_TO_SOURCE_FILE and the basename of the file.
#
# For example:
# com1___OBJ_TO_SOURCE_FILE       = $(PROJECT_ROOT)\source\diag\com1.c
# com_config___OBJ_TO_SOURCE_FILE = $(PROJECT_ROOT)\source\network\com_config.c
# appl1___OBJ_TO_SOURCE_FILE      = $(PROJECT_ROOT)\source\application\appl1.c
# appl2___OBJ_TO_SOURCE_FILE      = $(PROJECT_ROOT)\source\application\appl2.c
# test___OBJ_TO_SOURCE_FILE       = $(PROJECT_ROOT)\source\network\test.c
#

###################################################################
# The variable TEMP_ALL_ALL_FILES contains a list with all used source files.
TEMP_ALL_ALL_FILES = $(TEMP_ALL_CC_FILES) $(TEMP_ALL_ASM_FILES) $(TEMP_ALL_CPP_FILES)

###################################################################
# The define obj2xxxDependencies and the next foreach statement create
# the additional variables with the ___OBJ_TO_SOURCE_FILE suffix.
#
define obj2xxxDependencies
$(basename $(notdir $(subst \,/,$(1))))___OBJ_TO_SOURCE_FILE = $(1)
endef

$(foreach SRC,$(TEMP_ALL_ALL_FILES),$(eval $(call obj2xxxDependencies,$(strip $(SRC)))))

###################################################################
# The define getSourceFile expands to the source file of an object
# file.
define getSourceFile
$($(basename $(notdir $(subst \,/,$(@))))___OBJ_TO_SOURCE_FILE)
endef



##################################################################
# Check if all configured and not generated file exists.
#

define checkFileExists
$(if $(wildcard $(1)),,$(error The file $(1) does not exist. Please change your configuration) )
endef

$(foreach SRC,$(if $(filter YES,$(ENABLED_CONFIGURATION_CHECKS_DEFINED)),$(filter-out $(GENERATED_SOURCE_FILES), $(TEMP_ALL_ALL_FILES))),$(eval $(call checkFileExists,$(strip $(SRC)))))

###################################################################
# The variable GET_FILES_TO_LINK contains a list of all objects and
# libraries that should be used by the linker/locator call.
ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
GET_FILES_TO_LINK = \
  $(CC_TO_OBJ_BUILD_LIST)
else
GET_FILES_TO_LINK = \
	$(TEMP_CC_TO_OBJ_FILE_OUT_OF_CC_FILES_TO_BUILD) \
	$(TEMP_CPP_TO_OBJ_FILE_OUT_OF_CPP_FILES_TO_BUILD) \
	$(TEMP_ASM_TO_OBJ_FILE_OUT_OF_ASM_FILES_TO_BUILD) \
	$(OBJ_TO_LIB_BUILD_LIST) \
	$(LIBRARIES_LINK_ONLY) \
	$(OBJECTS_LINK_ONLY)
endif




###################################################################
# In some cases the number of files for the linker is too long and
# a file which contains a list of all object and library files should
# be used. The next code block allows to create such a list file.


GET_FILES_TO_LINK_FILE = $(BIN_OUTPUT_PATH)\link.lst
###################################################################
# Build list file for the linker


# Function append_to_file
#
# Append a line of text to a file using the 'echo' command.
#
# Parameter 1: Line to append.
# Parameter 2: file name
#
# NOTE: The newline before the 'endef' is necessary!
define append_to_file
echo $(1) >> $(2)

endef

$(GET_FILES_TO_LINK_FILE) : $(GET_FILES_TO_LINK)
	if exist $@ (del $@)
	@$(foreach linkFile,$(GET_FILES_TO_LINK),$(call append_to_file,$(linkFile),$@))

MAKE_CLEAN_RULES += clean_linker_list_files

clean_linker_list_files :
	@$(RM) "$(OBJ_OUTPUT_PATH)\*.tmp"
	@$(RM) "$(LIB_OUTPUT_PATH)\*.tmp"
	@$(RM) $(GET_FILES_TO_LINK_FILE)



