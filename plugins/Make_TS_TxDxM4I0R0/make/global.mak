# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# This file belongs to the implementation of the base_make package.
# Global.mak is the central makefile which includes all other makefiles.

#################################################################
# The first and main rule is BUILD_ALL.
FIRST_RULE: BUILD_ALL

#################################################################
# Windows backslash handling
BSLASH := \\#

#################################################################
# Empty String
EMPTY_STRING:=

#################################################################
# A single blank
BLANK:= $(EMPTY_STRING) $(EMPTY_STRING)

#################################################################
# unique will take a list of strings and will remove all duplicate
# strings from that list while keeping the order of list elements
unique  = $(strip $(if $(wordlist 2,$(words $(1)),$(1)),\
          $(call unique2,$(firstword $(1)),$(wordlist 2,$(words $(1)),$(1))),\
          $(1)))
## helper function for unique
unique2 = $(if $(wordlist 2,$(words $(2)),$(2)),\
          $(1) $(filter-out $(1),\
          $(call unique2,$(firstword $(2)),$(wordlist 2,$(words $(2)),$(2)))),\
          $(1) $(filter-out $(1),$(2)))

#################################################################
# Define the suffix for all files which implement the interfaces:
# Compiler Makefile Interface
# Board Makefile Interface
# Basic Software Makefile Interface
# Utility Makefile Interface
MAK_FILE_SUFFIX   :=mak
DEFS_FILE_SUFFIX  :=_defs.$(MAK_FILE_SUFFIX)
RULES_FILE_SUFFIX :=_rules.$(MAK_FILE_SUFFIX)
CHECK_FILE_SUFFIX :=_check.$(MAK_FILE_SUFFIX)
CFG_FILE_SUFFIX   :=_cfg.$(MAK_FILE_SUFFIX)

#################################################################
# Build a list of all plugins.
# For example:
# PLUGINS = st10_tasking can os hal st10_269 trace32
#
# The variables COMPILER SOFTWARE_MODULES ... must be part of the
# user makefile (normally makefile.mak).
PLUGINS = $(COMPILER) $(SOFTWARE_MODULES) $(ARCHITECTURE) $(UTILITIES)

PROJECT_OUTPUT_PATH      ?= $(PROJECT_ROOT)\output
GEN_OUTPUT_PATH          ?= $(PROJECT_OUTPUT_PATH)\generated
AUTOSAR_BASE_OUTPUT_PATH ?= $(GEN_OUTPUT_PATH)

#################################################################
# Define default output directories:
#
OBJ_OUTPUT_PATH := $(PROJECT_OUTPUT_PATH)\obj
LIB_OUTPUT_PATH := $(PROJECT_OUTPUT_PATH)\lib
BIN_OUTPUT_PATH := $(PROJECT_OUTPUT_PATH)\bin
TMP_OUTPUT_PATH := $(PROJECT_OUTPUT_PATH)\temp
DEP_OUTPUT_PATH := $(PROJECT_OUTPUT_PATH)\depend
CPP_OUTPUT_PATH := $(PROJECT_OUTPUT_PATH)\preprocess
MAKE_GEN_PATH   := $(GEN_OUTPUT_PATH)\make
MAKE_OUTPUT_PATH:= $(PROJECT_OUTPUT_PATH)\make

STATIC_LIB_OUTPUT_PATH ?= $(PROJECT_OUTPUT_PATH)\static_lib

#################################################################
# Define default ssc core directories:
#
ifeq ($(MAKE_CORE_PATH), )
MAKE_CORE_PATH   := $(SSC_ROOT)\Make_$(Make_VARIANT)
endif
MAKE_PLUGIN_PATH := $(MAKE_CORE_PATH)\make
TOOL_CORE_PATH   := $(MAKE_CORE_PATH)\tools
DOC_CORE_PATH    := $(SSC_ROOT)\doc
CFG_CORE_PATH    := $(SSC_ROOT)\configuration

#################################################################
# Define default project directories:
#
APPL_PROJECT_PATH := $(PROJECT_ROOT)\source\application

#################################################################
# The variable BOARD must be part of the user makefile and defines
# the name of the board folder which must be used.
BOARD_PROJECT_PATH := $(PROJECT_ROOT)\source\boards\$(BOARD)

#################################################################
# Define board, user and global makefile
BOARD_MAKE_FILE  := $(BOARD_PROJECT_PATH)\$(BOARD).$(MAK_FILE_SUFFIX)
GLOBAL_MAKE_FILE := $(if $(wildcard $(subst \,/,$(UTIL_DIR)\global.$(MAK_FILE_SUFFIX))),\
                         $(UTIL_DIR)\global.$(MAK_FILE_SUFFIX),$(SSC_ROOT)\make\global.$(MAK_FILE_SUFFIX)\
                     )

#################################################################
# If the variable USER_MAKE_FILE (user makefile) is already defined,
# it won't be overritten.
ifeq ($(USER_MAKE_FILE),)
USER_MAKE_FILE   := $(UTIL_DIR)\makefile.$(MAK_FILE_SUFFIX)
endif

#################################################################
# Define the names of all other global makefiles which must
# be included.
GLOBAL_DEFS_INCLUDE_FILES  := gui
GLOBAL_CHECK_INCLUDE_FILES :=

ifneq (show_rules,$(MAKECMDGOALS))
GLOBAL_RULES_INCLUDE_FILES += createpath interface
GLOBAL_RULES_INCLUDE_FILES += makeinfo
endif

GLOBAL_RULES_INCLUDE_FILES += cpp
GLOBAL_RULES_INCLUDE_FILES += createdepend
GLOBAL_RULES_INCLUDE_FILES += preprocess

# optional files
GLOBAL_RULES_INCLUDE_FILES += Testing


#################################################################
# The variable MAKE_GLOBAL_RULES contains a list of all global rules.
# This allows the target show_rules to display all available global rules.
MAKE_GLOBAL_RULES += all
all_DESCRIPTION := -			-compile all files (this is the default rule)

#################################################################
# The variable MAKE_CLEAN_RULES allows the registration of target names
# which are used for rules that delete files. This allows the target show_rules
# to display all available rules that can be used to cleanup.
MAKE_CLEAN_RULES  +=

#################################################################
# As above the variable MAKE_CLEAN_LIB_RULES allows the registration of target
# names which are used for rules that delete library files. This allows the
# target show_rules to display all available rules that can be used to
# cleanup.
MAKE_CLEAN_LIB_RULES  +=

#################################################################
# The variable MAKE_ADD_RULES works like MAKE_CLEAN_RULES or MAKE_GLOBAL_RULES.
# It is only used to display available targets.
MAKE_ADD_RULES    += show_rules

#################################################################
# The suffix _DESCRIPTION allows to define a description for
# each target. These descriptions will be used by the target show_rules
# to display all available targets with a description.
#
# For example:
#* --------------------------------------------------
#* General rules:
#* --------------------------------------------------
#* BUILD_ALL -           -make will build all files
#* depend -              -creates all dependency makefiles (output/depend/*.mak)
#* clean_depend -        -deletes all dependency makefiles (output/depend/*.mak)
#*
#* --------------------------------------------------
#* Rules to clean up:
#* --------------------------------------------------
#* os_clean -        -removes all files created by ProOSEK
# ...
show_rules_DESCRIPTION     = -		-show this message


#################################################################
# global commands used in other makefiles
#
ECHO     := echo
COPY     := copy
SHOW     := type
MOVE     := @$(TOOL_CORE_PATH)\move.bat
RMTREE   := @$(TOOL_CORE_PATH)\rmtree.bat
RM       := @$(TOOL_CORE_PATH)\rm.bat
SETERROR := @$(TOOL_CORE_PATH)\errorlevel\errorlevel.exe
MKDIR    := @$(TOOL_CORE_PATH)\mkdir.bat
DOS2UNIX := @$(TOOL_CORE_PATH)\dos2unix\dos2unix.exe -q
MV       := -$(MOVE)

#################################################################
# include *_cfg.mak
#
# - if a *_cfg file can be found in the $(UTIL_DIR)
#   - include this file
# - else if a *_cfg file can be found in the $(MAKE_GEN_PATH)
#   - include this file
#
ifneq ($(PLUGINS), )
include $(foreach PLUGIN, $(PLUGINS),\
  $(if $(wildcard $(subst \,/,$(UTIL_DIR)\$(PLUGIN)$(CFG_FILE_SUFFIX))),\
    $(UTIL_DIR)\$(PLUGIN)$(CFG_FILE_SUFFIX),\
    $(if $(wildcard $(subst \,/,$(MAKE_GEN_PATH)\$(PLUGIN)$(CFG_FILE_SUFFIX))),\
      $(MAKE_GEN_PATH)\$(PLUGIN)$(CFG_FILE_SUFFIX),)\
  )\
)
endif

#################################################################
# include GLOBAL_DEFS_INCLUDE_FILES
#
# - if a global definition file (e.g.: gui.mak) can be found in the $(UTIL_DIR)
#   - include this file
# - else if a global definition file can be found in the $(MAKE_PLUGIN_PATH)
#   - include this file
#
ifneq ($(GLOBAL_DEFS_INCLUDE_FILES), )
include $(foreach PLUGIN,$(GLOBAL_DEFS_INCLUDE_FILES),\
  $(if $(wildcard $(subst \,/,$(UTIL_DIR)\$(PLUGIN).$(MAK_FILE_SUFFIX))),\
    $(UTIL_DIR)\$(PLUGIN).$(MAK_FILE_SUFFIX),\
    $(if $(wildcard $(subst \,/,$(MAKE_PLUGIN_PATH)\$(PLUGIN).$(MAK_FILE_SUFFIX))),\
      $(MAKE_PLUGIN_PATH)\$(PLUGIN).$(MAK_FILE_SUFFIX),)\
  )\
)
endif

#################################################################
# include *_defs.mak
#
# - if a *_defs file can be found in $(UTIL_DIR)
#   - include this file
# - else if a *_defs file can be found in $(MAKE_PLUGIN_PATH)
#   - include this file
# - else if a *_defs file can be found in $(SSC_ROOT)/<PluginVariant>/make
#   - include this file
#
ifneq ($(PLUGINS), )
include $(foreach PLUGIN,$(PLUGINS),\
  $(if $(wildcard $(subst \,/,$(UTIL_DIR)\$(PLUGIN)$(DEFS_FILE_SUFFIX))),\
    $(UTIL_DIR)\$(PLUGIN)$(DEFS_FILE_SUFFIX),\
    $(if $(wildcard $(subst \,/,$(MAKE_PLUGIN_PATH)\$(PLUGIN)$(DEFS_FILE_SUFFIX))),\
      $(MAKE_PLUGIN_PATH)\$(PLUGIN)$(DEFS_FILE_SUFFIX),\
      $(if $(wildcard $(subst \,/,$(SSC_ROOT)\$(PLUGIN)_$($(PLUGIN)_VARIANT)\make\$(PLUGIN)$(DEFS_FILE_SUFFIX))),\
        $(SSC_ROOT)\$(PLUGIN)_$($(PLUGIN)_VARIANT)\make\$(PLUGIN)$(DEFS_FILE_SUFFIX),\
      )\
    )\
  )\
)
endif


#################################################################
# REGISTRY

postBuildBinary_DESCRIPTION           := -     -Generate a post build binary file
MAKE_GLOBAL_RULES += postBuildBinary

#################################################################
# Each basic software package has a name. This name shall be registered
# in the global variable SSC_PLUGINS.
SSC_PLUGINS          += base_make


#################################################################
# Define Basic Software Makefile Interface
#################################################################

#################################################################
# This variable LIBRARIES_TO_BUILD contains a list of the names for
# all libraries that should be built from compiled source files of the basic
# software package.
#
# For example:
# LIBRARIES_TO_BUILD += vendorx_canlib1 vendor_canlib2
#
LIBRARIES_TO_BUILD +=

#################################################################
# In some cases it is not necessary or not desired to build library
# files, only object files shall be created from existing C, C++
# and assembler files. For this propose the three variables
# [CC,CPP,ASM]_FILE_TO_BUILD can be used. Each of these variables contains
# a list of source files that must be compiled. What the corresponding
# compile call looks like is not important.
#
# For example:
#CC_FILES_TO_BUILD  += $(SSC_ROOT)\core\com\can\driver\can_drv1.c \
#                      $(SSC_ROOT)\core\com\can\driver\can_drv2.c \
#                      $(SSC_ROOT)\core\com\can\driver\can_drv3.c \
#                      $(PROJECT_ROOT)\source\network\can\can_conf.c
#
#CPP_FILES_TO_BUILD += $(SSC_ROOT)\core\com\can\state\state1.cpp \
#                      $(SSC_ROOT)\core\com\can\state\state2.cpp
#
#ASM_FILES_TO_BUILD += $(SSC_ROOT)\core\com\can\driver\can_core.asm
#
CC_FILES_TO_BUILD  +=
CPP_FILES_TO_BUILD +=
ASM_FILES_TO_BUILD +=

# Module may add source files to <MISRA_MODULE>_CC_FILES_TO_BUILD.
ifdef MISRA_MODULE
TEMP_CC_FILES_TO_BUILD := $(filter-out $(CC_FILES_TO_BUILD),$($(MISRA_MODULE)_CC_FILES_TO_BUILD))
CC_FILES_TO_BUILD  += $(TEMP_CC_FILES_TO_BUILD)
endif



#################################################################
# It is possible that the basic software bundles contain existing
# libraries and object files that shall be linked only.
#
# For example:
#LIBRARIES_LINK_ONLY += $(SSC_ROOT)\core\com\can\static\canlib.lib
#
#OBJECTS_LINK_ONLY   += $(SSC_ROOT)\core\com\can\static\can_drv1.obj \
#                       $(SSC_ROOT)\core\com\can\static\can_drv2.obj
#
LIBRARIES_LINK_ONLY +=
OBJECTS_LINK_ONLY  +=

#################################################################
# In some cases it is necessary to create directories before
# starting the build process. Each basic software bundle
# can specify directories that must be created.
#
DIRECTORIES_TO_CREATE +=\
	$(OBJ_OUTPUT_PATH) \
	$(LIB_OUTPUT_PATH) \
	$(BIN_OUTPUT_PATH) \
	$(DEP_OUTPUT_PATH) \
	$(CPP_OUTPUT_PATH) \
	$(TMP_OUTPUT_PATH) \
	$(MAKE_OUTPUT_PATH)

#################################################################
# Rules in Makefiles are like targets that allow the execution of a command or a
# sequence of commands. For example a rule can be used to start
# an external configuration tool which allows to adapt the CAN driver
# configuration, or to start a batch job that generates C and H files
# from a project configuration file. Another example is a rule which
# deletes all generated source files that belong to a basic software bundle.
# Normally, it suffices to write only the rule following the GNU makefile
# syntax, but in this case it is not possible to show which rules are
# currently avilable. Therefore all rules defined in Makefile that implement
# the Basic Software Makefile Interface shall be registered in one of the
# six variables MAKE_[CLEAN, GENERATE, COMPILE, DEBUG, CONFIG, ADD]_RULES.
# This allows the base_make package to print out all available rules.
# The six global variables are the registration interfaces for basic
# software specific rules.
MAKE_CLEAN_RULES     +=
MAKE_CLEAN_LIB_RULES +=
MAKE_GENERATE_RULES  +=
MAKE_COMPILE_RULES   +=
MAKE_DEBUG_RULES     +=
MAKE_CONFIG_RULES    +=
MAKE_ADD_RULES       +=


#################################################################
# Some basic software bundles need resources like include paths of other bundles.
# Each basic software bundle shall register its own include paths within a global variable
# that contains all include paths. The basic Software Makefile Interface
# distinguishes three global include paths. One is for the C compiler call,
# one for the C++ compiler call and the last one for the call of the assembler.
# For all three global include variables it is necessary to register
# the directories without any compiler specific prefix. The prefix
# depends on the compiler automatically selected within the core part
# of the Makefiles.
#
# Example:
# CC_INCLUDE_PATH      += $(CAN_PROJECT_PATH) $(CAN_CORE_PATH)
#
# The example shows how the directories CAN_PROJECT_PATH and the CAN_CORE_PATH
# are registered in the include path for the C compiler.
#
CC_INCLUDE_PATH      +=
CCP_INCLUDE_PATH     +=
ASM_INCLUDE_PATH     +=

#################################################################
# In some cases it is necessary to pass some macros to the pre-processor.
# These can be configuration settings or other values. Generally, the syntax,
# that the pre-processor expects for the macro, is tool chain specific.
# Therefore the Basic Software Makefile Interface supports a mechanism to
# declare macros, which is architecture independent.
#
#Example:
# CAN_DRIVER_MODE =singlechannel
# ...
# PREPROCESSOR_DEFINES += drivermode
#
# drivermode_KEY   =CAN_MODE
# drivermode_VALUE =$(CAN_DRIVER_MODE)
#
PREPROCESSOR_DEFINES +=

#################################################################
# The variable GENERATED_SOURCE_FILES is used to define a list of
# all generated source files. Normally, source files must exist at
# the file system before starting the build process. Exceptions
# are the generated source files.
GENERATED_SOURCE_FILES +=

#################################################################
# include *_rules.mak of all basic software plugins.
#
# uses same search rules as for *_defs files
#
ifneq ($(SOFTWARE_MODULES), )
include $(foreach PLUGIN,$(SOFTWARE_MODULES) $(ARCHITECTURE),\
  $(if $(wildcard $(subst \,/,$(UTIL_DIR)\$(PLUGIN)$(RULES_FILE_SUFFIX))),\
    $(UTIL_DIR)\$(PLUGIN)$(RULES_FILE_SUFFIX),\
    $(if $(wildcard $(subst \,/,$(MAKE_PLUGIN_PATH)\$(PLUGIN)$(RULES_FILE_SUFFIX))),\
      $(MAKE_PLUGIN_PATH)\$(PLUGIN)$(RULES_FILE_SUFFIX),\
      $(if $(wildcard $(subst \,/,$(SSC_ROOT)\$(PLUGIN)_$($(PLUGIN)_VARIANT)\make\$(PLUGIN)$(RULES_FILE_SUFFIX))),\
        $(SSC_ROOT)\$(PLUGIN)_$($(PLUGIN)_VARIANT)\make\$(PLUGIN)$(RULES_FILE_SUFFIX),\
      )\
    )\
  )\
)
endif

#################################################################
# include the board makefile (<board>.mak). This file is
# in the directory $(PROJECT_ROOT)\source\boards\<board>\<board>.mak.
#
ifneq ($(wildcard $(subst \,/,$(BOARD_MAKE_FILE))),)
include $(BOARD_MAKE_FILE)
endif


#################################################################
# include  GLOBAL_RULES_INCLUDE_FILES
#
# - if a global makefile (e.g.: inteface.mak) can be found in the $(UTIL_DIR)
#   - include this file
# - else if a global makefile can be found in the $(MAKE_PLUGIN_PATH)
#   - include this file
#
ifneq ($(GLOBAL_RULES_INCLUDE_FILES), )
include $(foreach PLUGIN,$(GLOBAL_RULES_INCLUDE_FILES),\
  $(if $(wildcard $(subst \,/,$(UTIL_DIR)\$(PLUGIN).$(MAK_FILE_SUFFIX))),\
    $(UTIL_DIR)\$(PLUGIN).$(MAK_FILE_SUFFIX),\
    $(if $(wildcard $(subst \,/,$(MAKE_PLUGIN_PATH)\$(PLUGIN).$(MAK_FILE_SUFFIX))),\
      $(MAKE_PLUGIN_PATH)\$(PLUGIN).$(MAK_FILE_SUFFIX),)\
  )\
)
endif

#################################################################
# include *_rules.mak of the compiler and the utility plugin.
#
# - if a *_rules file can be found in the $(UTIL_DIR)
#   - include this file
# - else if a *_rules file can be found in the $(MAKE_PLUGIN_PATH)
#   - include this file
#
ifneq ($(COMPILER) $(UTILITIES), )
include $(foreach PLUGIN,$(COMPILER) $(UTILITIES),\
  $(if $(wildcard $(subst \,/,$(UTIL_DIR)\$(PLUGIN)$(RULES_FILE_SUFFIX))),\
    $(UTIL_DIR)\$(PLUGIN)$(RULES_FILE_SUFFIX),\
    $(if $(wildcard $(subst \,/,$(MAKE_PLUGIN_PATH)\$(PLUGIN)$(RULES_FILE_SUFFIX))),\
      $(MAKE_PLUGIN_PATH)\$(PLUGIN)$(RULES_FILE_SUFFIX),)\
  )\
)
endif

#################################################################
# include dependency makefiles
#
# Include the dependency makefiles only for target 'all'
# and if no argument is given.
ifeq ($(strip $(filter-out all,$(MAKECMDGOALS))),)
include $(foreach MAK,$(CC_TO_MAK_BUILD_LIST),\
  $(if $(wildcard $(subst \,/,$(MAK))),\
    $(MAK),\
  )\
)
include $(foreach MAK,$(LIB_TO_MAK_BUILD_LIST),\
  $(if $(wildcard $(subst \,/,$(MAK))),\
    $(MAK),\
  )\
)

endif
ifeq (,$(filter-out all $(BUILD_ALL) fast_build,$(MAKECMDGOALS)))
  -include $(LIB_TO_MAK_BUILD_LIST)
endif
#################################################################
# BUILD_ALL and all:
# If no target is specified, the rule BUILD_ALL will be the first (
# default) target.
# For example:
# > make
# > make BUILD_ALL
# > make all
#
# The following step will be processed:
# - create the file lib_to_obj_depend.mak ((in createdepend.mak)
# - print information of this make run into $(MAKE_INFO_FILE)
# - generate all missing source files (the necessary rules are specified
#   in the basic software makefile plugins *_rules.mak )
# - trigger the first build target. It is defined in the variable FIRST_BUILD_TARGET which
#   must be part of the compiler makefile plugin. Normally, the first build target is at least one
#   binary file. The binary file(s) requires objects and libaries which must be built before the link
#   process of the binary file can be started.
#
# For example:
#   *.abs, *.hex, *.out requires *.lib, *.a and *.obj, *.o
#   *.lib, *.a          requires *.obj, *.o
#   *.obj, *.o          requires *.c, *.asm, *.s, *.cc, *.cpp *. ...
#  [*.c, *.cpp          requires code generator call ]
#

#BUILD_ALL all:  $(MAKE_INFO_FILE) $(FIRST_BUILD_TARGET)
#	$(START_BLOCK)
#	$(SEPARATOR) ** BUILD ALL PROJECT FILES FINISHED **
#	$(END_BLOCK)

BUILD_ALL all: fast_build

fast_build: $(MAKE_INFO_FILE) $(LIB_TO_MAK_BUILD_LIST) $(OBJ_TO_LIB_BUILD_LIST) $(CC_TO_OBJ_BUILD_LIST) $(FIRST_BUILD_TARGET)
	$(START_BLOCK)
	$(SEPARATOR) Finished Fast Build
	$(END_BLOCK)


# The 'first build target' is usually the final binary
# It depends on the libraries and objects created during the build
# $(CC_TO_OBJ_BUILD_LIST) is part of the depedencies as not all
# object files are packed into libraries
$(FIRST_BUILD_TARGET): $(OBJ_TO_LIB_BUILD_LIST) $(LIBRARIES_LINK_ONLY) $(OBJECTS_LINK_ONLY) | $(MAKE_INL_FILE)


# Object files created from C files depend on the existence of the
# compiler option file $(MAKE_INC_FILE)
# As there is sometimes no target for the final binary create
# linker options $(MAKE_INL_FILE) as well
$(CC_TO_OBJ_BUILD_LIST) : | $(MAKE_INC_FILE) $(MAKE_INL_FILE)

# Object files created from assembler files depend on the existence of the
# assembler option file $(MAKE_INA_FILE)
$(ASM_TO_OBJ_BUILD_LIST) : | $(MAKE_INA_FILE)

# force compiler/assembler/linker option files to be created every time
# make is invoked
# reason: in the current setup it is not possible to
#         provide useful dependencies for the option files
$(MAKE_INC_FILE)  $(MAKE_INL_FILE) $(MAKE_INA_FILE): make_inx_file_force
make_inx_file_force:


# make target to build a post build binary.
# Sets TS_BUILD_POST_BUILD_BINARY and the call make using default target.
postBuildBinary:
	$(MAKE) -f Makefile.mak TS_BUILD_POST_BUILD_BINARY=TRUE



################################################################
# recursive helper to perform a task on chunks of a list
# note: do not place a space in front of argument 5 of the recursive call,
#       ( $(4),<here>$(wordlist... )). This would result in infinite recursion.
#
# args: 1 - variable-name that contains commands to perform
#       2 - additional arguments to $(1)
#       3 - max. number of elements in a chunk
#       4 - $3+1 (sorry, cannot perform arith in make)
#       5 - list
SC_CMD_LISTPERFORM = $(if $(5),\
	$(call $(1),$(2) $(wordlist 1, $(3), $(5))) \
		$(call SC_CMD_LISTPERFORM, $(1), $(2), $(3), \
		$(4),$(wordlist $(4), $(words $(5)), $(5))) )

################################################################
# helper to generate a command that deletes a few objects or libs. This is
# needed for global_clean_lib.
# note: the 3rd line (empty) is really needed to serve as a cmd-separator.
#
# args: 1 - cmds to perform
define SC_CMD_AR_HELPER
	$(1)

endef


##################################################################
# The next target will build all library variants of a module
#
.PHONY: lib $(LIB_VARIANTS)

lib: $(LIB_VARIANTS)

$(LIB_VARIANTS):
# Delete the compiler settings files (since compiler options
# change by LIB_VARIANTS). This will force a rebuild of option
# file
	$(SEPARATOR) building lib variant $@
	$(RM) $(MAKE_INC_FILE)
	$(RM) $(MAKE_INA_FILE)
# reprocess the dependencies
	$(SEPARATOR) rebuild dependencies
	$(MAKE) -f Makefile.mak BUILD_MODE=LIB MODULE=$(MODULE) LIB_VARIANT=$@ clean_depend
	$(MAKE) -f Makefile.mak BUILD_MODE=LIB MODULE=$(MODULE) LIB_VARIANT=$@ depend
	$(SEPARATOR) remove existing lib
	$(MAKE) -f Makefile.mak BUILD_MODE=LIB MODULE=$(MODULE) LIB_VARIANT=$@ clean_lib
# build the library
	$(SEPARATOR) building lib
	$(MAKE) -f Makefile.mak BUILD_MODE=LIB MODULE=$(MODULE) LIB_VARIANT=$@
	$(SEPARATOR) finished lib variant $@


##################################################################
# The next target cleans all generated files as well as all library files.
# Each plugin has the possibility to define own clean_lib rules and register
# those rules to MAKE_CLEAN_LIB_RULES.
#
clean_lib: global_clean_lib $(MAKE_CLEAN_LIB_RULES)
MAKE_CLEAN_RULES += clean_lib
clean_lib_DESCRIPTION = - 		-remove all libraries and object files used to build libraries

##################################################################
# This rule cleans all library and object files and removes the
# folder where libraries are stored by default
global_clean_lib:
	$(START_MESSAGE_CLEAN)	libraries
	$(call SC_CMD_LISTPERFORM,SC_CMD_AR_HELPER, $(RM) -f, 5, 6, $(TEMP_CC_TO_OBJ_FILES_OUT_OF_LIBRARIES_TO_BUILD))
	$(call SC_CMD_LISTPERFORM,SC_CMD_AR_HELPER, $(RM) -f, 5, 6, $(OBJ_TO_LIB_BUILD_LIST))
	$(START_MESSAGE_CLEAN)	library folder
	$(RMTREE) $(LIB_OUTPUT_PATH)


##################################################################
# The next target generates files. Each plugin has the possibility
# to define own generate rules and register this rules in
# MAKE_GENERATE_RULES.
# The 'generate' target must not be processed in parallel.
generate: $(sort $(MAKE_GENERATE_RULES))

ifeq ($(MAKECMDGOALS),generate)
# Ensure that 'make generate' does not run in parallel.
.NOTPARALLEL:
endif

MAKE_GENERATE_RULES += generate
generate_DESCRIPTION = -		-invoke all generator tools

clean_generate:
	$(START_MESSAGE_CLEAN)	generated files
	$(RMTREE) "$(GEN_OUTPUT_PATH)"

MAKE_CLEAN_RULES += clean_generate
clean_generate_DESCRIPTION = -	-remove folder with generated files ($(word $(words $(subst \, ,$(GEN_OUTPUT_PATH))),$(subst \, ,$(GEN_OUTPUT_PATH))))"

##################################################################
# clean: only remove the object files
#
clean: compiler_clean
	$(START_MESSAGE_CLEAN)	object files
	$(call SC_CMD_LISTPERFORM,SC_CMD_AR_HELPER, $(RM) -f, 5, 6, $(TEMP_CC_TO_OBJ_FILE_OUT_OF_CC_FILES_TO_BUILD))
	$(START_MESSAGE_CLEAN)	object/binary folders
	$(RMTREE) $(OBJ_OUTPUT_PATH) $(BIN_OUTPUT_PATH) $(TMP_OUTPUT_PATH)
	$(RM) $(MAKE_INC_FILE) $(MAKE_INA_FILE) $(MAKE_INL_FILE)

MAKE_CLEAN_RULES += clean
clean_DESCRIPTION := -		-remove files created by the compiler and folders with object and binary files ($(subst $(PROJECT_OUTPUT_PATH)\,,$(OBJ_OUTPUT_PATH), $(BIN_OUTPUT_PATH)))

##################################################################
# clean_all: invoke all targets in make_clean_rules
# each plugin has the possibility to define own clean rules and
# register this rules in make_clean_rules.
#
# for example:
#
# in os_rules.mak:
# make_clean_rules += os_clean
# os_clean :
#    del generated os files
#
# in tasking_tricore_rules.mak:
# make_clean_rules += compliler_clean
# compliler_clean :
#    del compiled object files
#    del libraries
#
#
clean_all: $(MAKE_CLEAN_RULES)
	$(START_MESSAGE_CLEAN)	output folder
	$(RMTREE) $(MAKE_OUTPUT_PATH)

MAKE_CLEAN_RULES += clean_all
clean_all_DESCRIPTION := -		-invoke all clean rules

MAKE_ADD_RULES += dump-XX
dump-XX_DESCRIPTION := -		-dump the value of Make variable XX (replace by name)


##################################################################
# Define phony rules.
# If a target like "clean or all" is defined as ".POHNY", "make clean"
# or "make all" will run the commands
# regardless of whether there is a file named "clean" or "all".
.PHONY : BUILD_ALL all clean clean_all clean_lib show_rules

#################################################################
# Create all missing directories.
#
$(call unique,$(DIRECTORIES_TO_CREATE)) :
	$(MKDIR) $@

#################################################################
# The rule show_rules shows all available rules.
#
# For example:
# > make show_rules
#* --------------------------------------------------
#* General rules:
#* --------------------------------------------------
#* BUILD_ALL -           -make will build all files
#* depend -              -creates all dependency makefiles (output/depend/*.mak)
#* clean_depend -        -deletes all dependency makefiles (output/depend/*.mak)
#*
#* --------------------------------------------------
#* Rules to clean up:
#* --------------------------------------------------
#* compiler_clean -      -clean all compiler specific files *.o, *.src ...
#* os_clean -            -removes all files created by the ProOSEK configurator
#*
#* --------------------------------------------------
#* Rules to generate files:
#* --------------------------------------------------
#* os_call_generator -   -generates ProOSEK files (output/os/*.c *.h)
#*
#* --------------------------------------------------
#* Rules to compile files:
#* --------------------------------------------------
#* build_os -            -compile ProOSEK files manually
#* build_dave -          -compile DAVE files manually
#* build_can -           -compile CAN Driver files manually
#

#################################################################
# ALL_RULES is a local target (only used in global.mak) which is
# used to define a new rule that outputs the description of a rule
# by the help of the echo command.
ALL_RULES =  \
	$(MAKE_GLOBAL_RULES) \
	$(MAKE_CLEAN_RULES) \
	$(MAKE_CLEAN_LIB_RULES) \
	$(MAKE_GENERATE_RULES) \
	$(MAKE_COMPILE_RULES) \
	$(MAKE_DEBUG_RULES) \
	$(MAKE_CONFIG_RULES) \
	$(MAKE_ADD_RULES)

# The next block expands to rules which look like these examples:
#
# _debug_depend :
#     @echo * depend -              -creates all dependency makefiles (output/depend/*.mak)
#
# or
# _debug_build_os :
#     @echo * build_os -            -compile ProOSEK files manually
#
$(addprefix __debug,$(ALL_RULES)) :
	$(if $($(subst __debug,,$@)_DESCRIPTION),@echo * $(subst __debug,,$@) $($(subst __debug,,$@)_DESCRIPTION))


#################################################################
# show_rules_<...>_header are the targets that output the
# titles in the make show_rules command.
#
# For example:
#* --------------------------------------------------
#* General rules:
#* --------------------------------------------------
#
SHOW_SEPARATOR := --------------------------------------------------

show_rules_global_header:
	$(SEPARATOR) $(SHOW_SEPARATOR)
	$(SEPARATOR) General rules:
	$(SEPARATOR) $(SHOW_SEPARATOR)

show_rules_clean_header :
	$(SEPARATOR) $(SHOW_SEPARATOR)
	$(SEPARATOR) Rules to clean up:
	$(SEPARATOR) $(SHOW_SEPARATOR)

show_rules_generate_header :
	$(SEPARATOR) $(SHOW_SEPARATOR)
	$(SEPARATOR) Rules to generate files:
	$(SEPARATOR) $(SHOW_SEPARATOR)

show_rules_compile_header  :
	$(SEPARATOR) $(SHOW_SEPARATOR)
	$(SEPARATOR) Rules to compile files:
	$(SEPARATOR) $(SHOW_SEPARATOR)

show_rules_debug_header  :
	$(SEPARATOR) $(SHOW_SEPARATOR)
	$(SEPARATOR) Rules to show debug information:
	$(SEPARATOR) $(SHOW_SEPARATOR)

show_rules_config_header  :
	$(SEPARATOR) $(SHOW_SEPARATOR)
	$(SEPARATOR) Rules to start a configuration tool:
	$(SEPARATOR) $(SHOW_SEPARATOR)

show_rules_add_header  :
	$(SEPARATOR) $(SHOW_SEPARATOR)
	$(SEPARATOR) Additional rules:
	$(SEPARATOR) $(SHOW_SEPARATOR)

show_start_block :
	$(SEPARATOR) $(SHOW_SEPARATOR)
	$(SEPARATOR) Customer Build Environment
	$(SEPARATOR) $(SHOW_SEPARATOR)

show_end_block   :
	$(SEPARATOR) $(SHOW_SEPARATOR)
	$(SEPARATOR) help (-h, --help)-	-prints additional information on usage of make
	$(SEPARATOR) -s 			-silent operation; do not print the commands
	$(SEPARATOR) $(SHOW_SEPARATOR)

#################################################################
# A target like show_rules_global displays something like that:
#
#* --------------------------------------------------
#* General rules:
#* --------------------------------------------------
#* BUILD_ALL -           -make will build all files
#* depend -              -creates all dependency makefiles (output/depend/*.mak)
#* clean_depend -        -deletes all dependency makefiles (output/depend/*.mak)
#
# Show rules shall only display something, if there exists a rule. Otherwise
# just the heading would be displayed, which confuses the user.
#
ifneq ($(words $(MAKE_GLOBAL_RULES)), 0)
show_rules_global    : show_rules_global_header     $(addprefix __debug,$(MAKE_GLOBAL_RULES))
else
show_rules_global    :
endif

ifneq ($(words $(MAKE_CLEAN_RULES)), 0)
show_rules_clean     : show_rules_clean_header      $(addprefix __debug,$(MAKE_CLEAN_RULES))
else
show_rules_clean     :
endif

ifneq ($(words $(MAKE_GENERATE_RULES)), 0)
show_rules_generate  : show_rules_generate_header   $(addprefix __debug,$(MAKE_GENERATE_RULES))
else
show_rules_generate  :
endif

ifneq ($(words $(MAKE_COMPILE_RULES)), 0)
show_rules_compile   : show_rules_compile_header    $(addprefix __debug,$(MAKE_COMPILE_RULES))
else
show_rules_compile   :
endif

ifneq ($(words $(MAKE_DEBUG_RULES)), 0)
show_rules_debug     : show_rules_debug_header      $(addprefix __debug,$(MAKE_DEBUG_RULES))
else
show_rules_debug     :
endif

ifneq ($(words $(MAKE_CONFIG_RULES)), 0)
show_rules_config    : show_rules_config_header     $(addprefix __debug,$(MAKE_CONFIG_RULES))
else
show_rules_config    :
endif

ifneq ($(words $(MAKE_ADD_RULES)), 0)
show_rules_add       : show_rules_add_header        $(addprefix __debug,$(MAKE_ADD_RULES))
else
show_rules_add       :
endif

#################################################################
# The target show_rules is described above.
show_rules : \
	show_start_block \
	show_rules_global \
	show_rules_clean \
	show_rules_generate \
	show_rules_compile \
	show_rules_debug \
	show_rules_config \
	show_rules_add \
	show_end_block


#################################################################
# Show names of all plugins
show_plugins :
	$(SEPARATOR) PLUGINS : $(foreach PLUGIN,$(SSC_PLUGINS),$(PLUGIN))

#################################################################
# Dump the value of a Make variable
dump-%:
	@if not "$($*)"=="" $(ECHO) $($*)
