# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

TRESOS2_CORE_PATH     := $(TRESOS2_BASE)\plugins
TRESOS2_OUTPUT_PATH    = $(AUTOSAR_BASE_OUTPUT_PATH)
TRESOS2_GENERATOR     := $(TRESOS2_BASE)\bin\tresos_cmd.bat

#################################################################
# REGISTRY
SSC_PLUGINS           += tresos2
