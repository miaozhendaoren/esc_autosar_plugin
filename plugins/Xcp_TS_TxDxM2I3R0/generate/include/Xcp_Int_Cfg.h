/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!/*The identation of the generator code is transmited to the output file if INDENT is
not used here.*/!]
[!INDENT "0"!]
[!INCLUDE "Xcp_Vars.m"!][!//
#if (!defined XCP_INT_CFG_H)
#define XCP_INT_CFG_H

/* This file contains the generated Xcp module configuration. */

[!AUTOSPACING!]

[!IF "text:tolower(XcpGeneral/XcpMasterBlockModeSupported) = 'true'"!]
[!IF "XcpGeneral/XcpAddressGranularity = 'BYTE'"!]
[!VAR "XcpMaxBs" = "num:integer(ceiling(255 div (XcpGeneral/XcpMaxCto - 2)))"!]
[!ELSEIF "XcpGeneral/XcpAddressGranularity = 'WORD'"!]
[!VAR "XcpMaxBs" = "num:integer(ceiling(255 div (floor((XcpGeneral/XcpMaxCto - 2) div 2 ))))"!]
[!ELSE!]
[!VAR "XcpMaxBs" = "num:integer(ceiling(255 div (floor((XcpGeneral/XcpMaxCto - 4) div 4 ))))"!]
[!ENDIF!]

[!IF "XcpGeneral/XcpMaxBS < $XcpMaxBs"!]
[!VAR "XcpMaxBs" = "XcpGeneral/XcpMaxBS"!] [!/* Allow the user to configure a smaller value than the maximum possible value which would not result in a waste of resources */!]
[!ENDIF!]

[!ENDIF!] [!/* XcpGeneral/XcpMasterBlockModeSupported = 'true' */!]

[!IF "($XcpMasterBlockModePgmSupported = 'true')"!]
[!IF "XcpGeneral/XcpAddressGranularity = 'BYTE'"!]
[!VAR "XcpMaxBSPgm" = "num:integer(ceiling(255 div (XcpGeneral/XcpMaxCtoPgm - 2)))"!]
[!ELSEIF "XcpGeneral/XcpAddressGranularity = 'WORD'"!]
[!VAR "XcpMaxBSPgm" = "num:integer(ceiling(255 div (floor((XcpGeneral/XcpMaxCtoPgm - 2) div 2 ))))"!]
[!ELSE!]
[!VAR "XcpMaxBSPgm" = "num:integer(ceiling(255 div (floor((XcpGeneral/XcpMaxCtoPgm - 4) div 4 ))))"!]
[!ENDIF!]

[!IF "XcpGeneral/XcpMaxBSPgm < $XcpMaxBSPgm"!]
[!VAR "XcpMaxBSPgm" = "XcpGeneral/XcpMaxBSPgm"!] [!/* Allow the user to configure a smaller value than the maximum possible value which would not result in a waste of resources */!]
[!ENDIF!]

[!ENDIF!] [!/* $XcpMasterBlockModePgmSupported = 'true' */!]

[!IF "$XcpTimestampIsEnabled = 'true'"!]
  [!IF "XcpGeneral/XcpUserTimestampSupported = 'false'"!]
    [!IF "node:refvalid(XcpGeneral/XcpCounterRef) and node:exists(as:ref(XcpGeneral/XcpCounterRef)/OsSecondsPerTick)"!]
      [!VAR "OsSecondsPerTick"="node:refs(XcpGeneral/XcpCounterRef)/OsSecondsPerTick"!]
      [!IF "floor($OsSecondsPerTick) = $OsSecondsPerTick"!] [!/* If OsSecondsPerTick is an integer - we have a multiple of seconds */!]
        [!VAR "TimeStampTicks"="num:i(num:i(text:replaceAll(num:f($OsSecondsPerTick), '\.', '')) div 10)"!]
        [!VAR "TimeStampUnit"="'TIMESTAMP_UNIT_1S'"!]
      [!ELSE!] [!/* OsSecondsPerTick is not an integer - we must use ticks of up to 65535 and units smaller than seconds (us, ms, ns, ps) */!]
        [!VAR "TimeStampTicks"="num:i(text:replaceAll(num:f($OsSecondsPerTick), '\.', ''))"!]
        [!VAR "TimeStampUnit"="(var:set('varName', string-length(substring-after(num:f($OsSecondsPerTick), '.'))) | text:split('TIMESTAMP_UNIT_1S TIMESTAMP_UNIT_100MS TIMESTAMP_UNIT_10MS TIMESTAMP_UNIT_1MS TIMESTAMP_UNIT_100US TIMESTAMP_UNIT_10US TIMESTAMP_UNIT_1US TIMESTAMP_UNIT_100NS TIMESTAMP_UNIT_10NS TIMESTAMP_UNIT_1NS TIMESTAMP_UNIT_100PS TIMESTAMP_UNIT_10PS TIMESTAMP_UNIT_1PS')[position() = $varName + 1])[2]"!]
      [!ENDIF!]
      [!IF "$TimeStampTicks > 65535"!][!ERROR!]Calculated timestamp ticks exceeded allowed value of 65535. Please reduce the OsSecondsPerTick value from the referenced OS counter (XcpCounterRef).[!ENDERROR!][!ENDIF!]
    [!ELSE!] [!/* XcpCounterRef is not correctly configured, set default values */!]
      [!VAR "TimeStampTicks"= "1"!]
      [!VAR "TimeStampUnit"= "'0U'"!]
    [!ENDIF!] [!/* Validity check for XcpCounterRef */!]
  [!ELSE!] [!/* XcpGeneral/XcpUserTimestampSupported = 'true' */!]
    [!VAR "TimeStampUnit"="XcpGeneral/XcpTimestampUnit"!]
    [!VAR "TimeStampTicks"="XcpGeneral/XcpTimestampTicks"!]
  [!ENDIF!] [!/* XcpGeneral/XcpUserTimestampSupported = 'false' */!]
[!ELSE!] [!/* Timestamp is disabled, set default values */!]
   [!VAR "TimeStampTicks"= "1"!]
   [!VAR "TimeStampUnit"= "'0U'"!]
[!ENDIF!]

[!IF "$TimeStampUnit != '0U'"!]
[!VAR "TimeStampUnit"="concat('(uint8)(XCP_',$TimeStampUnit,'_MASK << XCP_TIMESTAMP_UNIT_POS)')"!]
[!ENDIF!]

[!IF "text:tolower(XcpGeneral/XcpOnEthernetEnabled) = 'true'"!]
[!IF "node:exists(XcpConfig/*[1]/XcpPdu/*[@name='XcpRxPdu'][1])"!]
  [!VAR "RxPduRef" = "XcpConfig/*[1]/XcpPdu/*[@name='XcpRxPdu'][1]/XcpRxPduRef"!]
  [!LOOP "as:modconf('SoAd')[1]/SoAdConfig/*/SoAdSocketRoute/*"!]
    [!IF "node:path(node:ref(SoAdSocketRouteDest/*[1]/SoAdRxPduRef))= node:path(node:ref($RxPduRef))"!]
      [!IF "node:refvalid(./SoAdRxSocketConnectionRef)"!]
        [!VAR "SoAdSocketId_Rx" = "num:i(node:ref(./SoAdRxSocketConnectionRef)/SoAdSocketId)"!]
      [!ELSE!][!//
        [!ERROR!]SoAdRxSocketConnectionRef of the SoAdSocketRoute configured for the XCP Rx PDU is not valid in the SoAd module.[!ENDERROR!]
      [!ENDIF!]
    [!ENDIF!]
  [!ENDLOOP!]
[!ENDIF!]
[!ENDIF!]


/*==================[includes]===============================================*/

#include <Std_Types.h>      /* AUTOSAR standard types */
#include <TSAutosar.h>      /* EB specific standard types */
#include <Xcp_Int_Stc.h>    /* Internal static header file */

/*==================[macros]=================================================*/

#if (defined XCP_DEV_ERROR_DETECT)
#error XCP_DEV_ERROR_DETECT already defined
#endif
/** \brief Switch, indicating whether the detection of development errors is
 ** activated or deactivated for XCP */
#define XCP_DEV_ERROR_DETECT         [!//
[!IF "text:tolower(XcpGeneral/XcpDevErrorDetect) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#if (defined XCP_MAIN_FUNCTION_PERIOD)
#error XCP_MAIN_FUNCTION_PERIOD already defined
#endif
/** \brief The period between successive ticks in seconds. */
#define XCP_MAIN_FUNCTION_PERIOD     [!//
[!"XcpGeneral/XcpMainFunctionPeriod"!]


#if (defined XCP_PROD_ERR_HANDLING_RETRY_FAILED)
#error XCP_PROD_ERR_HANDLING_RETRY_FAILED already defined
#endif
/** \brief Switch for DEM to DET reporting for Xcp Retry failure */
#define XCP_PROD_ERR_HANDLING_RETRY_FAILED    [!//
[!IF "ReportToDem/XcpRetryFailedReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/XcpRetryFailedReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//


#if (defined XCP_IDENTIFICATION_FIELD_TYPE)
#error XCP_IDENTIFICATION_FIELD_TYPE already defined
#endif
/** \brief Definition of value indicating the type of Packet Identification Field the slave
 ** will use while transferring DAQ Packets to the master.
 **/
#define XCP_IDENTIFICATION_FIELD_TYPE [!//
XCP_[!"XcpGeneral/XcpIdentificationFieldType"!]_IF_MASK


#if (defined XCP_IDENTIFICATION_TYPE1_LENGTH)
#error XCP_IDENTIFICATION_TYPE1_LENGTH already defined
#endif
/** \brief Definition of Length of the identification information TYPE 1
 * This is the length of ASCII value of ASAM-MC2 filename without path
 * and extension.
 */
[!IF "node:exists(XcpGeneral/XcpASAMMC2Filename)"!][!//
#define XCP_IDENTIFICATION_TYPE1_LENGTH  [!"num:i(string-length(normalize-space(XcpGeneral/XcpASAMMC2Filename)))"!]U
[!ELSE!][!//
#define XCP_IDENTIFICATION_TYPE1_LENGTH  0x00U
[!ENDIF!][!//


[!IF "node:exists(XcpGeneral/XcpASAMMC2Filename)"!][!//
#if (defined XCP_IDENTIFICATION_TYPE_ONE)
#error XCP_IDENTIFICATION_TYPE_ONE already defined
#endif
/** \brief Definition of the value indicating Identification information TYPE 1(One).
 * Type 1(One) identification information is ASAM-MC2 filename without path
 * and extension
 */
#define XCP_IDENTIFICATION_TYPE_ONE  "[!"normalize-space(XcpGeneral/XcpASAMMC2Filename)"!]"
[!ENDIF!][!//


#if (defined XCP_MAX_CTO)
#error XCP_MAX_CTO already defined
#endif
/** \brief The maximum length of XCP command transfer objects (CTO) packet in bytes.*/
#define XCP_MAX_CTO                  [!//
[!"num:integer(XcpGeneral/XcpMaxCto)"!]U


#if (defined XCP_MAX_CTO_PGM)
#error XCP_MAX_CTO_PGM already defined
#endif
/** \brief The maximum length of XCP command transfer objects (CTO) packet in bytes during a flash
 ** programming sequence */
[!IF "text:tolower(XcpGeneral/XcpPgmSupported) = 'true'"!]
#define XCP_MAX_CTO_PGM             [!"num:integer(XcpGeneral/XcpMaxCtoPgm)"!]U
[!ELSE!]
#define XCP_MAX_CTO_PGM             0U
[!ENDIF!]

#if (defined XCP_DAQ_CONFIG_TYPE)
#error XCP_DAQ_CONFIG_TYPE already defined
#endif
/** \brief Switch, indicating whether the DAQ lists that are not PREDEFINED shall be configured
 ** statically or dynamically.
 **/
#define XCP_DAQ_CONFIG_TYPE          [!//
XCP_[!"$XcpDaqConfigType"!]_MASK

#if (defined XCP_MAX_DTO)
#error XCP_MAX_DTO already defined
#endif
/** \brief The maximum length of XCP data transfer objects (DTO) packet in bytes. */
#define XCP_MAX_DTO                  [!//
[!"num:integer(XcpGeneral/XcpMaxDto)"!]U

#if (defined XCP_MIN_DAQ)
#error XCP_MIN_DAQ already defined
#endif
/** \brief The number of predefined, read only DAQ lists on the XCP slave. */
#define XCP_MIN_DAQ                  [!//
[!"num:integer($XcpMinDaq)"!]U

#if (defined XCP_DAQ_COUNT)
#error XCP_DAQ_COUNT already defined
#endif
/** \brief The maximum number of configurable dynamic DAQ lists */
#define XCP_DAQ_COUNT                  [!//
[!"num:integer($XcpDaqCount)"!]U

#if (defined XCP_ODT_COUNT)
#error XCP_ODT_COUNT already defined
#endif
/** \brief Maximum number of ODTs allowed per dynamic DAQ list. */
#define XCP_ODT_COUNT                  [!//
[!"num:integer($XcpOdtCount)"!]U

#if (defined XCP_ODT_ENTRIES_COUNT)
#error XCP_ODT_ENTRIES_COUNT already defined
#endif
/** \brief Maximum number of ODT entries allowed per dynamic ODT. */
#define XCP_ODT_ENTRIES_COUNT                  [!//
[!"num:integer($XcpOdtEntriesCount)"!]U

#if (defined XCP_MAX_EVENT_CHANNEL)
#error XCP_MAX_EVENT_CHANNEL already defined
#endif
/** \brief The number of event channels on the XCP slave. */
#define XCP_MAX_EVENT_CHANNEL        [!//
[!"num:integer($XcpMaxEventChannel)"!]U


#if (defined XCP_MAX_CYCLIC_EVENT_CHANNEL)
#error XCP_MAX_CYCLIC_EVENT_CHANNEL already defined
#endif
/** \brief The number of cyclic event channels on the XCP slave. */
#define XCP_MAX_CYCLIC_EVENT_CHANNEL        [!//
[!"num:integer(count(XcpConfig/*[1]/XcpEventChannel/*[XcpEventChannelTimeCycle > 0]))"!]U

#if (defined XCP_PRESCALER_SUPPORTED)
#error XCP_PRESCALER_SUPPORTED already defined
#endif
/** \brief Switch, indicating whether all DAQ lists support the prescaler for reducing the
 ** transmission period. */
#define XCP_PRESCALER_SUPPORTED      [!//
[!IF "(XcpGeneral/XcpDaqSupported = 'true') and (text:tolower(XcpGeneral/XcpPrescalerSupported) = 'true')"!]XCP_PRESCALER_MASK[!ELSE!](0U)[!ENDIF!]

#if (defined XCP_TIMESTAMP_SUPPORTED)
#error XCP_TIMESTAMP_SUPPORTED already defined
#endif
/** \brief Switch, indicating whether the slave supports time stamped data acquisition and
 ** stimulation. */
#define XCP_TIMESTAMP_SUPPORTED      [!//
[!IF "$XcpTimestampIsEnabled = 'true'"!]XCP_TIMESTAMP_MASK[!ELSE!](0U)[!ENDIF!]

#if (defined XCP_TIMESTAMP_TICKS)
#error XCP_TIMESTAMP_TICKS already defined
#endif
/** \brief Definition of value indicating Timestamp units per tick. */
#define XCP_TIMESTAMP_TICKS          [!"num:integer($TimeStampTicks)"!]U

#if (defined XCP_TIMESTAMP_TYPE)
#error XCP_TIMESTAMP_TYPE already defined
#endif
/** \brief The timestamp type (the number of bytes that the timestamp will have)*/
#define XCP_TIMESTAMP_TYPE           [!//
[!IF "$XcpTimestampIsEnabled = 'true'"!]XCP_[!"XcpGeneral/XcpTimestampType"!]_TS_MASK[!ELSE!]XCP_NO_TIME_STAMP_TS_MASK[!ENDIF!]

#if (defined XCP_TIMESTAMP_UNIT)
#error XCP_TIMESTAMP_UNIT already defined
#endif
/** \brief Definition of value indicating the unit of the time stamp. */
#define XCP_TIMESTAMP_UNIT           [!"$TimeStampUnit"!]

[!IF "$XcpTimestampIsEnabled = 'true'"!]
  [!IF "XcpGeneral/XcpUserTimestampSupported = 'false'"!]
#if (defined XCP_OS_COUNTER)
#error XCP_OS_COUNTER already defined
#endif
/** \brief OS Counter which is used by XCP. */
#define XCP_OS_COUNTER               [!//
[!"name(as:ref(XcpGeneral/XcpCounterRef))"!]
  [!ENDIF!]
[!ENDIF!]

[!IF "text:tolower($StoreDaq)  = 'true'"!]
[!VAR "BlockIdRef" = "as:ref(./XcpGeneral/XcpStoreDaqNvMBlock)/NvMNvramBlockIdentifier"!]
[!VAR "BlockIdLength" = "as:ref(./XcpGeneral/XcpStoreDaqNvMBlock)/NvMNvBlockLength "!]

#if (defined XCP_STORE_DAQ_NVM_BLOCK_ID)
#error XCP_STORE_DAQ_NVM_BLOCK_ID already defined
#endif
/** \brief The NvM block ID used to store the DAQ lists
 **
 **/
#define XCP_STORE_DAQ_NVM_BLOCK_ID   [!"$BlockIdRef"!]U

#if (defined XCP_STORE_DAQ_NVM_BLOCK_LENGTH)
#error XCP_STORE_DAQ_NVM_BLOCK_LENGTH already defined
#endif
/** \brief The length of the NvM block used to store the DAQ lists
 **
 **/
#define XCP_STORE_DAQ_NVM_BLOCK_LENGTH   [!"$BlockIdLength"!]U
[!ENDIF!]

#if (defined XCP_RESOURCE_CAL_PAG)
#error XCP_RESOURCE_CAL_PAG already defined
#endif
/** \brief Switch, indicating whether calibration/paging functionality is available. */
#define XCP_RESOURCE_CAL_PAG         [!//
[!IF "text:tolower(XcpGeneral/XcpCalPagSupported) = 'true'"!]XCP_RESOURCE_CAL_PAG_MASK[!ELSE!](0U)[!ENDIF!]


#if (defined XCP_RESOURCE_DAQ)
#error XCP_RESOURCE_DAQ already defined
#endif
/** \brief Switch, indicating whether synchronous data acquisition functionality is available. */
#define XCP_RESOURCE_DAQ             [!//
[!IF "text:tolower(XcpGeneral/XcpDaqSupported) = 'true'"!]XCP_RESOURCE_DAQ_MASK[!ELSE!](0U)[!ENDIF!]


#if (defined XCP_RESOURCE_STIM)
#error XCP_RESOURCE_STIM already defined
#endif
/** \brief Switch, indicating whether synchronous data stimulation functionality is available. */
#define XCP_RESOURCE_STIM            [!//
[!IF "node:exists(XcpGeneral/XcpStimSupported) and text:tolower(XcpGeneral/XcpStimSupported) = 'true'"!]XCP_RESOURCE_STIM_MASK[!ELSE!](0U)[!ENDIF!]


#if (defined XCP_RESOURCE_PGM)
#error XCP_RESOURCE_PGM already defined
#endif
/** \brief Value of the PGM resource as needed by the protection status (see GET_STATUS command) */
#define XCP_RESOURCE_PGM             [!//
[!IF "text:tolower(XcpGeneral/XcpPgmSupported) = 'true'"!]XCP_RESOURCE_PGM_MASK[!ELSE!](0U)[!ENDIF!]


#if (defined XCP_PGM_SUPPORTED)
#error XCP_PGM_SUPPORTED already defined
#endif
/** \brief Switch, indicating whether programming functionality is available.*/
#define XCP_PGM_SUPPORTED             [!//
STD_[!IF "text:tolower(XcpGeneral/XcpPgmSupported) = 'true'"!]ON[!ELSE!]OFF[!ENDIF!]


#if (defined XCP_ADDRESS_GRANULARITY)
#error XCP_ADDRESS_GRANULARITY already defined
#endif
/** \brief Bit values related to the address granularity of the
 ** COMM_MODE_BASIC parameter in CONNECT command parameter.
 **/
#define XCP_ADDRESS_GRANULARITY      [!//
XCP_BYTE_AG_MASK


#if (defined XCP_ADDRESS_GRANULARITY_SIZE)
#error XCP_ADDRESS_GRANULARITY_SIZE already defined
#endif
/** \brief The size of an element contained at a single address in the slave. */
#define XCP_ADDRESS_GRANULARITY_SIZE [!//
XCP_BYTE_AG_SIZE


#if (defined XCP_MASTER_BLOCK_MODE)
#error XCP_MASTER_BLOCK_MODE already defined
#endif
/** \brief Switch, indicating whether master block mode functionality is available. */
#define XCP_MASTER_BLOCK_MODE        [!//
[!IF "text:tolower(XcpGeneral/XcpMasterBlockModeSupported) = 'true'"!]XCP_MASTER_BLOCK_MODE_MASK[!ELSE!](0U)[!ENDIF!]

#if (defined XCP_MASTER_BLOCK_MODE_SUPPORTED)
#error XCP_MASTER_BLOCK_MODE_SUPPORTED already defined
#endif
/** \brief Switch, indicating whether master block mode functionality is available. */
#define XCP_MASTER_BLOCK_MODE_SUPPORTED        [!//
[!IF "text:tolower(XcpGeneral/XcpMasterBlockModeSupported) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined XCP_MAX_BS)
#error XCP_MAX_BS already defined
#endif
/** \brief Definition of the maximum number of consecutive command packets in a block sequence.
 **  The value represents the initial DOWNLOAD command plus the consecutive DOWNLOAD_NEXT commands.
 **/
#define XCP_MAX_BS                   [!//
[!IF "text:tolower(XcpGeneral/XcpMasterBlockModeSupported) = 'true'"!][!"$XcpMaxBs"!]U[!ELSE!](0U)[!ENDIF!]


#if (defined XCP_MIN_ST)
#error XCP_MIN_ST already defined
#endif
/** \brief The required minimum separation time between the packets of a block
 ** transfer from the master device to the slave device in units of 100 microseconds. */
#define XCP_MIN_ST                   [!//
[!IF "text:tolower(XcpGeneral/XcpMasterBlockModeSupported) = 'true'"!][!"XcpGeneral/XcpMinST"!]U[!ELSE!](0U)[!ENDIF!]


#if (defined XCP_MASTER_BLOCK_MODE_PGM)
#error XCP_MASTER_BLOCK_MODE_PGM already defined
#endif
/** \brief Switch, indicating whether master block mode for programming is available. */
#define XCP_MASTER_BLOCK_MODE_PGM        [!//
[!IF "($XcpMasterBlockModePgmSupported = 'true')"!]XCP_MASTER_BLOCK_MODE_MASK[!ELSE!](0U)[!ENDIF!]

#if (defined XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED)
#error XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED already defined
#endif
/** \brief Switch, indicating whether master block mode for programming is available. */
#define XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED        [!//
[!IF "($XcpMasterBlockModePgmSupported = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#if (defined XCP_MAX_BS_PGM)
#error XCP_MAX_BS_PGM already defined
#endif
/** \brief Definition of the maximum number of consecutive command packets in a block sequence
 **  for flash programming.
 **  The value represents the initial PROGRAM command plus the consecutive PROGRAM_NEXT commands.
 **/
#define XCP_MAX_BS_PGM                   [!//
[!IF "($XcpMasterBlockModePgmSupported = 'true')"!][!"$XcpMaxBSPgm"!]U[!ELSE!](0U)[!ENDIF!]


#if (defined XCP_MIN_ST_PGM)
#error XCP_MIN_ST_PGM already defined
#endif
/** \brief The required minimum separation time, for flash programming, between the packets of a block
 ** transfer from the master device to the slave device in units of 100 microseconds. */
#define XCP_MIN_ST_PGM                   [!//
[!IF "($XcpMasterBlockModePgmSupported = 'true')"!][!"XcpGeneral/XcpMinSTPgm"!]U[!ELSE!](0U)[!ENDIF!]


#if (defined XCP_TIMESTAMP_FIXED)
#error XCP_TIMESTAMP_FIXED already defined
#endif
/** \brief Switch, indicating whether time stamp fixed functionality is available. */
[!IF "(node:exists(XcpGeneral/XcpTimestampFixed) = 'true')"!]
#define XCP_TIMESTAMP_FIXED          [!//
[!IF "text:tolower(XcpGeneral/XcpTimestampFixed) = 'true'"!]XCP_TIMESTAMP_FIXED_MASK[!ELSE!](0U)[!ENDIF!]
[!ELSE!]
#define XCP_TIMESTAMP_FIXED    0U
[!ENDIF!]


#if (defined XCP_GRANULARITY_ODT_ENTRY_SIZE_DAQ)
#error XCP_GRANULARITY_ODT_ENTRY_SIZE_DAQ already defined
#endif
/** \brief Definition of value indicating granularity for size of ODT entry (DIRECTION = DAQ). */
#define XCP_GRANULARITY_ODT_ENTRY_SIZE_DAQ [!//
XCP_BYTE_AG_SIZE


#if (defined XCP_MAX_ODT_ENTRY_SIZE_DAQ)
#error XCP_MAX_ODT_ENTRY_SIZE_DAQ already defined
#endif
/** \brief Definition of value indicating the maximum size of ODT entry (DIRECTION = DAQ). */
[!IF "(node:exists(XcpGeneral/XcpDaqSupported) = 'true')"!]
#define XCP_MAX_ODT_ENTRY_SIZE_DAQ   [!//
[!"XcpGeneral/XcpOdtEntrySizeDaq"!]U
[!ELSE!]
#define XCP_MAX_ODT_ENTRY_SIZE_DAQ   0U
[!ENDIF!]


#if (defined XCP_GRANULARITY_ODT_ENTRY_SIZE_STIM)
#error XCP_GRANULARITY_ODT_ENTRY_SIZE_STIM already defined
#endif
/** \brief Definition of value indicating granularity for size of ODT entry (DIRECTION = STIM). */
#define XCP_GRANULARITY_ODT_ENTRY_SIZE_STIM [!//
XCP_BYTE_AG_SIZE


#if (defined XCP_MAX_ODT_ENTRY_SIZE_STIM)
#error XCP_MAX_ODT_ENTRY_SIZE_STIM already defined
#endif
/** \brief Definition of value indicating maximum size of ODT entry (DIRECTION = STIM). */
[!IF "(node:exists(XcpGeneral/XcpDaqSupported) = 'true')"!]
#define XCP_MAX_ODT_ENTRY_SIZE_STIM  [!//
[!"XcpGeneral/XcpOdtEntrySizeStim"!]U
[!ELSE!]
#define XCP_MAX_ODT_ENTRY_SIZE_STIM   0U
[!ENDIF!]

[!VAR "XcpMaxEntriesPerOdt" = "num:i(0)"!]
  [!LOOP "node:order(XcpConfig/*[1]/XcpDaqList/*, 'XcpDaqListNumber')"!]
    [!IF "(XcpDaqListNumber < XcpGeneral/XcpMinDaq)"!]
      [!VAR "ODTEntryCnt" = "num:i(0)"!]
      [!LOOP "node:order(XcpOdt/*, 'XcpOdtNumber')"!]
        [!VAR "ODTEntryCnt" = "num:i(count(XcpOdtEntry/*))"!]
        [!"$ODTEntryCnt"!]U
        [!IF "$XcpMaxEntriesPerOdt < $ODTEntryCnt"!]
          [!VAR "XcpMaxEntriesPerOdt" = "$ODTEntryCnt"!]
        [!ENDIF!]
      [!ENDLOOP!]
    [!ENDIF!]
  [!ENDLOOP!]
[!IF "XcpGeneral/XcpDaqConfigType = 'DAQ_STATIC'"!]
  [!LOOP "node:order(XcpConfig/*[1]/XcpDaqList/*, 'XcpDaqListNumber')"!]
   [!IF "XcpDaqListNumber >= XcpGeneral/XcpMinDaq"!]
      [!IF "$XcpMaxEntriesPerOdt < ./XcpMaxOdtEntries"!]
        [!VAR "XcpMaxEntriesPerOdt" = "num:i(./XcpMaxOdtEntries)"!]
      [!ENDIF!]
    [!ENDIF!]
  [!ENDLOOP!]
[!ELSE!]
  [!IF "$XcpMaxEntriesPerOdt < num:i(XcpGeneral/XcpOdtEntriesCount)"!]
    [!VAR "XcpMaxEntriesPerOdt" = "num:i(XcpGeneral/XcpOdtEntriesCount)"!]
  [!ENDIF!]
[!ENDIF!]
#if (defined XCP_MAX_ENTRIES_PER_ODT)
#error XCP_MAX_ENTRIES_PER_ODT already defined
#endif
/** \brief The absolute maximum number of ODT Entries in an ODT
 **/
#define XCP_MAX_ENTRIES_PER_ODT   [!" num:i($XcpMaxEntriesPerOdt)"!]U

[!IF "XcpGeneral/XcpDaqConfigType = 'DAQ_DYNAMIC'"!]

#if (defined XCP_MAX_ENTRIES_PER_ODT_DYN)
#error XCP_MAX_ENTRIES_PER_ODT_DYN already defined
#endif
/** \brief The absolute maximum number of ODT Entries in an ODT belonging to a dynamic DAQ list.
 ** The ODT does not carry the timestamp (i.e. timestamp is disabled or not the first ODT in the
 ** DAQ list).
 **/
#define XCP_MAX_ENTRIES_PER_ODT_DYN     [!"num:i(XcpGeneral/XcpOdtEntriesCount)"!]U

#if (defined XCP_MAX_ENTRIES_PER_ODT_DYN_TS)
#error XCP_MAX_ENTRIES_PER_ODT_DYN_TS already defined
#endif
/** \brief The maximum number of ODT Entries in an ODT belonging to a dynamic DAQ list which carries
 ** the timestamp (i.e. timestamp is enabled and the first ODT in the DAQ list).
 **/
[!VAR "XcpMaxEntriesPerOdtTimestamped" = "num:i((XcpGeneral/XcpMaxDto - ($XcpTimestampSize + $XcpPidSize)) div $XcpMinODTEntrySize)"!]
[!IF "num:i(XcpGeneral/XcpOdtEntriesCount) < $XcpMaxEntriesPerOdtTimestamped"!]
  [!VAR "XcpMaxEntriesPerOdtTimestamped" = "num:i(XcpGeneral/XcpOdtEntriesCount)"!]
[!ENDIF!]
#define XCP_MAX_ENTRIES_PER_ODT_DYN_TS  [!"num:i($XcpMaxEntriesPerOdtTimestamped)"!]U
[!ENDIF!]

#if (defined XCP_OVERLOAD_INDICATION_TYPE)
#error XCP_OVERLOAD_INDICATION_TYPE already defined
#endif
/** \brief Definition of value indicating the method used to indicate overload situation to
 ** master. */
#define XCP_OVERLOAD_INDICATION_TYPE [!//
[!IF "text:tolower(XcpGeneral/XcpEventPacketEnabled) = 'true'"!]XCP_EVENT_OL_MASK[!ELSE!]XCP_NO_OVERLOAD_INDICATION_OL_MASK[!ENDIF!]

#if (defined XCP_OPTIMISATION_TYPE)
#error XCP_OPTIMISATION_TYPE already defined
#endif
/** \brief Definition of value indicating the type of optimisation method the master preferably
 ** should use. */
#define XCP_OPTIMISATION_TYPE        [!//
XCP_DEFAULT_OM_MASK

#if (defined XCP_ADDRESS_EXTENSION_TYPE)
#error XCP_ADDRESS_EXTENSION_TYPE already defined
#endif
/** \brief Switch, indicating whether the address extension
 ** of all entries within one ODT or within one DAQ must be the same. */
#define XCP_ADDRESS_EXTENSION_TYPE   [!//
XCP_FREE_AE_MASK

#if (defined XCP_MAXIMUM_ODT)
#error XCP_MAXIMUM_ODT already defined
#endif
/** \brief The number of ODTs allowed for a DAQ.
 **
 ** \note Macro is introduced to have limit on number of ODTs allowed
 ** for a DAQ list.
 **/
[!IF "XcpGeneral/XcpDaqSupported = 'true'"!]
#define XCP_MAXIMUM_ODT             [!//
  [!VAR "Max" = "0"!]
  [!VAR "MaxOdtPerDaqList" = "0"!]
  [!LOOP "XcpConfig/*[1]/XcpDaqList/*"!]
     [!IF "XcpDaqListNumber < $XcpMinDaq"!]
      [!VAR "MaxOdtPerDaqList" = "count(./XcpOdt/*)"!]
      [!IF "$Max < $MaxOdtPerDaqList"!]
        [!VAR "Max" = "$MaxOdtPerDaqList"!]
      [!ENDIF!]
    [!ELSE!]
      [!IF "$Max < XcpMaxOdt"!]
        [!VAR "Max" = "XcpMaxOdt"!]
      [!ENDIF!]
    [!ENDIF!]
  [!ENDLOOP!]
  [!IF "$XcpDaqConfigType = 'DAQ_DYNAMIC'"!]
    [!IF "$Max < $XcpOdtCount"!]
      [!VAR "Max" = "$XcpOdtCount"!]
    [!ENDIF!]
  [!ENDIF!]
  [!"num:i($Max)"!]U
[!ELSE!] [!/* DAQ List support is disabled */!]
#define XCP_MAXIMUM_ODT              0U
[!ENDIF!]

#if (defined XCP_TX_RETRY_COUNT)
#error XCP_TX_RETRY_COUNT already defined
#endif
/** \brief The number of times the data will be retried for transmission.
 **
 ** \note Macro is introduced to have limit on number of transmission retries
 ** when transmission fails.
 **
 **/
#define XCP_TX_RETRY_COUNT           [!//
[!"XcpGeneral/XcpTxRetryCount"!]U

#if (defined XCP_TX_BUS_TO_COUNTER_MAX_VAL)
#error XCP_TX_BUS_TO_COUNTER_MAX_VAL already defined
#endif
/** \brief The number of Xcp_MainFunction calls until the XCP slave is autonomously DISCONNECTED.
 **
 ** Macro is introduced to have the Bus Timeout limit in case the XCP TX processor remains
 ** stuck in one of the following states: TX Trigger/ TX Confirmation, after the pending message
 ** transmition was accepted by the underlying communication.
 **
 **/
#define XCP_TX_BUS_TO_COUNTER_MAX_VAL [!//
[!"num:i(round(num:f(num:div(XcpGeneral/XcpTxBusTimeout, XcpGeneral/XcpMainFunctionPeriod))))"!]U

[!IF "$XcpMaxDaq > 0"!]
  [!VAR "Consistency" = "'ODT'"!]
  [!LOOP "XcpConfig/*[1]/XcpEventChannel/*"!]
    [!IF "XcpEventChannelConsistency = 'EVENT'"!]
      [!VAR "Consistency" = "'EVENT'"!]
      [!BREAK!]
    [!ENDIF!]
    [!IF "XcpEventChannelConsistency = 'DAQ'"!]
      [!VAR "Consistency" = "'DAQ'"!]
    [!ENDIF!]
  [!ENDLOOP!]
  [!VAR "MaxEventChannelMaxDaqList" = "num:max(as:modconf('Xcp')/XcpConfig/*[1]/XcpEventChannel/*/XcpEventChannelMaxDaqList)"!]

#if (defined XCP_DTO_QUEUE_SIZE)
#error XCP_DTO_QUEUE_SIZE already defined
#endif

/** \brief Value indicating the DTO Queue Size.
 **
 ** \note DTO Queue must have enough space to hold the data sampled in a cyclic function.
 ** Based on the consistency configuration of all the events, DTO Queue size will be as follows.
 ** In Event Consistency, it will be
 ** (XCP_MAX_DTO*XCP_MAXIMUM_ODT * Maximum of XcpEventChannelMaxDaqList of all events)
 ** In DAQ Consistency, it will be (XCP_MAX_DTO*XCP_MAXIMUM_ODT)
 ** In ODT Consistency, it will be (XCP_MAX_DTO)
 **
 ** While transmitting the DAQs of an event, the sampling of ODTs of the next event can be
 ** started. So buffer size is set as twice the size needed for data sampled in a cyclic function.
 **
 **/
#define XCP_DTO_QUEUE_SIZE           [!//
  [!IF "$Consistency = 'EVENT'"!] [!//
  (((XCP_MAX_DTO + XCP_DTO_LENGTH_SIZE) * [!"$MaxEventChannelMaxDaqList"!]U * XCP_MAXIMUM_ODT) * 2U)
  [!ELSEIF "$Consistency = 'DAQ'"!] [!//
  (((XCP_MAX_DTO + XCP_DTO_LENGTH_SIZE) * XCP_MAXIMUM_ODT) * 2U)
  [!ELSE!] [!//
  ((XCP_MAX_DTO + XCP_DTO_LENGTH_SIZE) * 2U)
  [!ENDIF!]

#if (defined XCP_STIM_BUFFER_SIZE)
#error XCP_STIM_BUFFER_SIZE already defined
#endif

/** \brief Value indicating the STIM Buffer Size.
 **
 ** \note STIM Buffer must have enough space to hold the data to be stimulated in a cyclic function.
 ** The basic type of STIM buffer is a structure 'Xcp_StimDtoType', which has buffer for
 ** holding data of one STIM DTO. So while defining 'XCP_STIM_BUFFER_SIZE' there is no need for
 ** considering XCP_MAX_DTO, since it is already allocated in structure 'Xcp_StimDtoType'.
 ** Based on the consistency configuration of all the events, STIM Buffer size will be as follows.
 ** In Event Consistency, it will be
 ** (XCP_MAXIMUM_ODT * Maximum of XcpEventChannelMaxDaqList of all events)
 ** In DAQ Consistency, it will be (XCP_MAXIMUM_ODT)
 ** In ODT Consistency, it will be (1)
 **
 ** While stimulating the DAQs of an event, buffering STIM DTOs of the next event can be started.
 ** So buffer size is set as twice the size needed for the data stimulated in a cyclic function.
 **
 **/
#define XCP_STIM_BUFFER_SIZE [!//
  [!IF "$Consistency = 'EVENT'"!] [!//
  (([!"$MaxEventChannelMaxDaqList"!]U * XCP_MAXIMUM_ODT) * 2U)
  [!ELSEIF "$Consistency = 'DAQ'"!] [!//
  ((XCP_MAXIMUM_ODT) * 2U)
  [!ELSE!] [!//
  (2U)
  [!ENDIF!]

[!ENDIF!]


#if (defined XCP_NOOF_PRE_DEFINED_ODTS)
#error XCP_NOOF_PRE_DEFINED_ODTS already defined
#endif
/** \brief Number of predefined or configured ODTs.
 **
 **/
#define XCP_NOOF_PRE_DEFINED_ODTS                          [!"num:i($XcpNoOfPreODTs)"!]U


#if (defined XCP_NOOF_PRE_DEFINED_ODTENTRIES)
#error XCP_NOOF_PRE_DEFINED_ODTENTRIES already defined
#endif
/** \brief Number of predefined or configured ODT entries.
 **
 **/
#define XCP_NOOF_PRE_DEFINED_ODTENTRIES                    [!"num:i($XcpNoOfPreODTEntries)"!]U

/*------------------[PDU configuration options]------------------------------*/

#if (defined XCP_TX_PDU_ID)
#error XCP_TX_PDU_ID already defined
#endif
/** \brief Map the symbolic name of the Tx PDU ID to an internal name available after generation */
#define XCP_TX_PDU_ID     [!//
XcpConf_XcpPdu_[!"name(XcpConfig/*[1]/XcpPdu/*[@name='XcpTxPdu'][1])"!]


[!/*
  If XCP on CAN Enabled.
*/!]
[!IF "text:tolower(./XcpGeneral/XcpOnCanEnabled)  = 'true'"!]

[!VAR "CtoSlaveRxCanID" = "'FAULT'"!][!//
[!IF "node:exists(./XcpConfig/*[1]/XcpCtoInfo/XcpCtoSlavePduRef) = 'true'"!]
  [!VAR "CtoSlaveRxPduRef" = "as:ref(./XcpConfig/*[1]/XcpCtoInfo/XcpCtoSlavePduRef)/XcpRxPduRef"!]
  [!LOOP "as:modconf('CanIf')[1]/CanIfInitCfg/*/CanIfRxPduCfg/*[node:exists(CanIfRxPduCanId)]"!]
    [!IF "node:path(node:ref(CanIfRxPduRef))= node:path(node:ref($CtoSlaveRxPduRef))"!]
      [!VAR "CtoSlaveRxCanID" = "num:i(CanIfRxPduCanId)"!]
      [!IF "CanIfRxPduCanIdType = 'EXTENDED_CAN'"!]
        [!VAR "CtoSlaveRxCanID" = "bit:bitset($CtoSlaveRxCanID,31)"!]
      [!ENDIF!]
    [!ENDIF!]
  [!ENDLOOP!]

#if (defined XCP_CMD_STIM_CAN_ID)
#error XCP_CMD_STIM_CAN_ID already defined
#endif
/** \brief CAN identifier to be used by the master while sending CMD or STIM
 ** packets directly to the xcp slave.
 **
 **/
[!IF "$CtoSlaveRxCanID = 'FAULT'"!][!ERROR!]No valid CAN ID found for the corresponding Rx PDU Reference in CanIf. Check that the reference is valid and the CAN ID is correctly set (CanIfRxPduCanId).[!ENDERROR!][!ENDIF!][!//
#define XCP_CMD_STIM_CAN_ID                       [!"$CtoSlaveRxCanID"!]U

[!ENDIF!]

[!VAR "CtoMasterTxCanID" = "'FAULT'"!][!//
[!IF "node:exists(XcpConfig/*[1]/XcpPdu/*[@name='XcpTxPdu'][1])"!]
  [!VAR "CtoMasterTxPduRef" = "XcpConfig/*[1]/XcpPdu/*[@name='XcpTxPdu'][1]/XcpTxPduRef"!]
  [!LOOP "as:modconf('CanIf')[1]/CanIfInitCfg/*/CanIfTxPduCfg/*"!]
    [!IF "node:path(node:ref(CanIfTxPduRef))= node:path(node:ref($CtoMasterTxPduRef))"!]
      [!VAR "CtoMasterTxCanID" = "num:i(CanIfTxPduCanId)"!]
      [!IF "CanIfTxPduCanIdType = 'EXTENDED_CAN'"!]
        [!VAR "CtoMasterTxCanID" = "bit:bitset($CtoMasterTxCanID,31)"!]
      [!ENDIF!]
    [!ENDIF!]
  [!ENDLOOP!]
  #if (defined XCP_CMD_DAQ_CAN_ID)
  #error XCP_CMD_DAQ_CAN_ID already defined
  #endif
  /** \brief The CAN id for transmitting response packets to the master.
   **
   **/
  [!IF "$CtoMasterTxCanID = 'FAULT'"!][!ERROR!]No Tx PDU configured in CanIf found corresponding to the Tx PDU configured in the XCP slave[!ENDERROR!][!ENDIF!][!//
  #define XCP_CMD_DAQ_CAN_ID                        [!"$CtoMasterTxCanID"!]U
[!ENDIF!]

[!ENDIF!]

[!IF "node:exists(XcpConfig/*[1]/XcpPdu/*[@name='XcpTxPdu'][1])"!]
  [!VAR "CtoMasterTxPduID" = "'FAULT'"!][!//
  [!VAR "CtoMasterTxPduRef" = "XcpConfig/*[1]/XcpPdu/*[@name='XcpTxPdu'][1]/XcpTxPduRef"!]
  [!IF "text:tolower(XcpGeneral/XcpOnCanEnabled)  = 'true'"!]
    [!LOOP "as:modconf('CanIf')[1]/CanIfInitCfg/*/CanIfTxPduCfg/*"!]
      [!IF "node:path(node:ref(CanIfTxPduRef))= node:path(node:ref($CtoMasterTxPduRef))"!]
        [!VAR "CtoMasterTxPduID" = "num:i(CanIfTxPduId)"!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ELSEIF "text:tolower(XcpGeneral/XcpOnEthernetEnabled) = 'true'"!]
    [!LOOP "as:modconf('SoAd')[1]/SoAdConfig/*/SoAdPduRoute/*"!]
      [!IF "node:path(node:ref(SoAdTxPduRef))= node:path(node:ref($CtoMasterTxPduRef))"!]
        [!VAR "CtoMasterTxPduID" = "num:i(SoAdTxPduId)"!]
        [!IF "node:refvalid(./SoAdPduRouteDest/*[1]/SoAdTxSocketConnectionRef)"!]
          [!VAR "SoAdSocketId_Tx" = "num:i(node:ref(./SoAdPduRouteDest/*[1]/SoAdTxSocketConnectionRef)/SoAdSocketId)"!]
        [!ELSE!][!//
          [!ERROR!]SoAdTxSocketConnectionRef of the SoAdPduRoute configured for the XCP TX PDU is not valid in the SoAd module.[!ENDERROR!]
        [!ENDIF!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ELSEIF "text:tolower(XcpGeneral/XcpOnFlexRayEnabled)  = 'true'"!]
    [!LOOP "as:modconf('FrIf')[1]/FrIfConfig/*/FrIfPdu/*[FrIfPduDirection='FrIfTxPdu']"!]
      [!IF "node:path(node:ref(./FrIfPduDirection/FrIfTxPduRef))= node:path(node:ref($CtoMasterTxPduRef))"!]
        [!VAR "CtoMasterTxPduID" = "num:i(./FrIfPduDirection/FrIfTxPduId)"!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDIF!]
#if (defined XCP_RES_TX_PDU_ID)
#error XCP_RES_TX_PDU_ID already defined
#endif
/** \brief The handle id for transmitting response packets to the master.
 ** The value is retrieved from the underlying communication layer interface.
 **/
  [!IF "$CtoMasterTxPduID = 'FAULT'"!][!ERROR!]No Tx PDU found in CanIf or FrIf for the corresponding Xcp Tx PDU. Check that a Tx PDU exists in CanIf or FrIf which points to the same PDU in EcuC as Xcp Tx Pdu points to.[!ENDERROR!][!ENDIF!][!//
#define XCP_RES_TX_PDU_ID                     [!"$CtoMasterTxPduID"!]U

[!ENDIF!]
#if (defined XCP_IS_RX_PDU_ID_VALID)
#error XCP_IS_RX_PDU_ID_VALID already defined
#endif
/** \brief Macro to check if the given Rx PDU ID is configured/valid for this module.
 **
 ** \param[in]      RxPduId  The Rx PDU ID to be validated
 **
 ** \return Result of the operation
 ** \retval TRUE Rx PDU ID is configured/valid
 ** \retval FALSE Rx PDU ID is not configured/valid
 **/
#define XCP_IS_RX_PDU_ID_VALID(RxPduId)  ( ( \
[!VAR "i" = "0"!]
[!INDENT "45"!]
[!LOOP "XcpConfig/*[1]/XcpPdu/*[@name='XcpRxPdu']"!]
                                        [!IF "$i > 0"!]||[!ELSE!][!WS "2"!][!ENDIF!] ( (RxPduId) == [!"XcpRxPduId"!] ) \
  [!VAR "i" = "$i + 1"!]
[!ENDLOOP!]
[!ENDINDENT!]
                                           ) ? TRUE : FALSE )

[!IF "text:tolower(XcpGeneral/XcpOnEthernetEnabled)  = 'true'"!]
#if (defined XCP_SOAD_AUTO_OPEN_SO_CON)
#error XCP_SOAD_AUTO_OPEN_SO_CON already defined
#endif
/** \brief Switch, indicating whether SoAd_OpenSoCon() will be called during Xcp_Init() */
#define XCP_SOAD_AUTO_OPEN_SO_CON             [!//
[!IF "text:tolower(XcpGeneral/XcpAutoOpenSoCon) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#if (defined XCP_SOAD_SOCKET_RX_ID)
#error XCP_SOAD_SOCKET_RX_ID already defined
#endif
/** \brief Id of the Ethernet socket mapped to the Xcp Rx PDU. */
#define XCP_SOAD_SOCKET_RX_ID                    [!"$SoAdSocketId_Rx"!]U

#if (defined XCP_SOAD_SOCKET_TX_ID)
#error XCP_SOAD_SOCKET_TX_ID already defined
#endif
/** \brief Id of the Ethernet socket mapped to the Xcp Tx PDU.
 ** For the TCP protocol, the Id is equal to the Id of the socket mapped to the Xcp Rx PDU */
#define XCP_SOAD_SOCKET_TX_ID                    [!"$SoAdSocketId_Tx"!]U

[!ENDIF!]


#if (defined XCP_SEED_AND_KEY_ENABLED)
#error XCP_SEED_AND_KEY_ENABLED already defined
#endif
/** \brief Switch, indicating whether resource protection with Seed & Key mechanism
 ** is activated or deactivated for XCP */
#define XCP_SEED_AND_KEY_ENABLED         [!//
[!IF "text:tolower(XcpGeneral/XcpSeedAndKeyEnabled) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#if (defined XCP_EVENT_PACKET_ENABLED)
#error XCP_EVENT_PACKET_ENABLED already defined
#endif
/** \brief Switch, indicating whether the transmission of Event packets from the slave device to
 ** the master device is activated or deactivated */
#define XCP_EVENT_PACKET_ENABLED         [!//
[!IF "text:tolower(XcpGeneral/XcpEventPacketEnabled) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#if (defined XCP_EV_CTO_QUEUE_SIZE)
#error XCP_EV_CTO_QUEUE_SIZE already defined
#endif
/** \brief Macro to hold the size of the Event CTO queue based on the value of maximum DAQ list
 ** allocated per Event Channel when STIM is supported else the Event CTO packets will be processed
 ** in a single cyclic function, so size of the Event CTO queue shall be considered as 2 */
#define XCP_EV_CTO_QUEUE_SIZE         [!//
[!IF "node:exists(XcpGeneral/XcpStimSupported) and text:tolower(XcpGeneral/XcpStimSupported) = 'true'"!]
  [!"num:i(num:max(XcpConfig/*[1]/XcpEventChannel/*/XcpEventChannelMaxDaqList))"!]U
[!ELSE!][!//
  2U
[!ENDIF!]


#if (defined XCP_EVENT_CHANNEL_TOTAL_DAQ)
#error XCP_EVENT_CHANNEL_TOTAL_DAQ already defined
#endif
/** \brief The total number of DaqLists that can be associated to events channels on the XCP slave. */
#define XCP_EVENT_CHANNEL_TOTAL_DAQ        [!//
[!"num:i(sum(XcpConfig/*[1]/XcpEventChannel/*/XcpEventChannelMaxDaqList))"!]U


#if (defined XCP_NUM_EVENT_CHANNEL_PRECFG)
#error XCP_NUM_EVENT_CHANNEL_PRECFG already defined
#endif
/** \brief The total number of preconfigured DaqLists that are associated to events channels */
#define XCP_NUM_EVENT_CHANNEL_PRECFG       [!//
[!"num:i(count(XcpConfig/*[1]/XcpEventChannel/*/XcpEventChannelTriggeredDaqListRef/*))"!]U


/** \brief MODIFY_BITS supported */
#define XCP_MODIFY_BITS_SUPPORTED         STD_[!IF "XcpGeneral/XcpModifyBitsSupported = 'true'"!]ON[!ELSE!]OFF[!ENDIF!]


/** \brief Enable/disable the BUILD_CHECKSUM feature. */
#define XCP_BUILD_CHECKSUM_SUPPORT   [!//
[!IF "XcpGeneral/XcpBuildChecksumSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!IF "XcpGeneral/XcpBuildChecksumSupport = 'true'"!]
/** \brief The type of the checksum that the BUILD_CHECKSUM feature will use */
#define XCP_BUILD_CHECKSUM_TYPE   [!//
[!"XcpGeneral/XcpBuildChecksumType"!]
[!ENDIF!]


/** \brief Enable/Disable the Xcp User Timestamp */
#define XCP_USER_TIMESTAMP_SUPPORTED  STD_[!IF "(node:exists(XcpGeneral/XcpUserTimestampSupported) = 'true') and (XcpGeneral/XcpUserTimestampSupported = 'true')"!]ON[!ELSE!]OFF[!ENDIF!]

#if (defined XCP_SLAVE_BLOCK_MODE)
#error XCP_SLAVE_BLOCK_MODE already defined
#endif
/** \brief Switch, indicating whether slave block mode functionality is available. */
#define XCP_SLAVE_BLOCK_MODE         [!//
[!IF "text:tolower(XcpGeneral/XcpSlaveBlockModeSupported) = 'true'"!]XCP_SLAVE_BLOCK_MODE_MASK[!ELSE!](0U)[!ENDIF!]

[!/*If XCP on FlexRay Enabled.*/!]
[!IF "XcpGeneral/XcpOnFlexRayEnabled = 'true'"!]
/** \brief FlexRay node address extension (NAX) */
#define XCP_FLEXRAY_NAX             [!"XcpGeneral/XcpFlxNodeAddress"!]U


[!ENDIF!]
/** \brief Length of the XCP header in bytes */
#define XCP_FRAME_HEADER_LENGTH     [!"num:i($XcpFrameHeaderLength)"!]U

/*==================[type definitions]=======================================*/

 /** \brief Definition for the Bus Timeout Counter value data type used to count the amount of
  ** Xcp_MainFunction cycles until the TX pending message is send by the underlying communication
  ** from the moment the transmision was accepted. */
 typedef uint[!"$BusToTypeSize"!] Xcp_BusTimeoutCounterValueType;

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( XCP_INT_CFG_H ) */
/*==================[end of file]============================================*/
[!ENDINDENT!]
