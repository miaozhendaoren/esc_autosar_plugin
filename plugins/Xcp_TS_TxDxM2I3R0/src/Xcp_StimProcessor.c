/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]============================================*/

#include <Xcp_Trace.h>
#include <Std_Types.h>         /* AUTOSAR standard types */
#include <TSAutosar.h>         /* EB specific standard types */
#include <TSMem.h>             /* EB memory functions */
#include <SchM_Xcp.h>          /* Needed for exclusive area definition */

#include <Xcp.h>               /* Module public API */
#include <Xcp_Int.h>           /* Internal API */
#include <Xcp_UserCallouts.h>  /* XCP user callout functions */

/*==================[macros]================================================*/

#if (defined XCP_GET_SEARCH_KEY)
  #error XCP_GET_SEARCH_KEY already defined
#endif
/** \brief Create a Key by merging the given DAQ Index and ODT Index */
#define XCP_GET_SEARCH_KEY(DaqID, OdtIndex)  ( (((uint32)(DaqID)) << 8U) + ((uint32)(OdtIndex)) )

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)

#define XCP_START_SEC_CODE
#include <MemMap.h>


/** \brief Function to insert a DTO into the STIM Buffer
 **
 ** This function is used to search for a DAQ List Id and ODT index in the
 ** STIM Buffer and insert the given DTO by replacing an existing entry
 ** or by adding a new entry.
 **
 ** \param[in] DaqID        The Id of the DAQ list related to the DTO
 ** \param[in] OdtIndex     The ODT Index related to the DTO
 ** \param[in] DataPtr      Pointer to the data within the DTO
 ** \param[in] DataCount    Number of data bytes within the DTO
 **
 ** \retval E_OK      The DTO was inserted successfully
 ** \retval E_NOT_OK  The DTO could not be inserted because the buffer is full
 */
STATIC FUNC(Std_ReturnType, XCP_CODE) Xcp_InsertOdtToStimBuffer
(
  Xcp_DaqIdType DaqID,
  uint8 OdtIndex,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) DataPtr,
  uint16 DataCount
);


/** \brief Function to remove a DTO entry from the STIM Buffer
 **
 ** This function is used remove an entry from the STIM Buffer.
 **
 ** \param[in] BufferIndex  Index of the DTO entry in the STIM Buffer
 **
 */
STATIC FUNC(void, XCP_CODE) Xcp_RemoveOdtFromStimBuffer
(
  uint16 BufferIndex
);


/** \brief Function to stimulate all ODT entries in an ODT
 **
 ** This function is used to stimulate the data assosiated with all
 ** ODT entries for an ODT in the STIM Buffer.
 **
 ** \param[in] DaqID        The Id of the DAQ List
 ** \param[in] OdtIndex     The ODT Index
 ** \param[in] BufferIndex  Index of the DTO entry in the STIM Buffer
 **
 */
STATIC FUNC(void, XCP_CODE) Xcp_StimulateOdtEntries
(
  Xcp_DaqIdType DaqID,
  uint8 OdtIndex,
  uint16 BufferIndex
);


/** \brief Function to stimulate the data of an ODT
 **
 ** This function is used to search an ODT in the STIM Buffer,
 ** stimulate the data assosiated with the ODT and remove the
 ** DTO entry from the STIM Buffer.
 **
 ** \param[in] DaqID     Index of the DAQ in the DAQ List
 ** \param[in] OdtIndex     The ODT Index
 **
 */
STATIC FUNC(void, XCP_CODE) Xcp_StimulateOdt
(
  Xcp_DaqIdType DaqID,
  uint8 OdtIndex
);


/** \brief Function to stimulate the data of an entire DAQ list
 **
 ** This function is used to search all ODTs for a DAQ in the STIM Buffer,
 ** stimulate the data assosiated with the ODTs and remove the
 ** DTO entries from the STIM Buffer.
 **
 ** \param[in] DaqID     Index of the DAQ in the DAQ List
 **
 */
STATIC FUNC(void, XCP_CODE) Xcp_StimulateDaq
(
  Xcp_DaqIdType DaqID
);


/** \brief Function to stimulate the data for an entire event
 **
 ** This function is used to search the STIM Buffer for each ODT of
 ** the DAQs associated with an event,
 ** stimulate the data assosiated with the ODTs and remove the
 ** DTO entries from the STIM Buffer.
 **
 ** \param[in] EventID      Event ID to process
 **
 */
STATIC FUNC(void, XCP_CODE) Xcp_StimulateEvent
(
  uint16 EventID
);

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/


FUNC(void, XCP_CODE) Xcp_InsertSTIM
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr
)
{
  uint8 OdtIndex;
  Xcp_DaqIdType DaqID;
  uint16 DataCount = 0U;
  Std_ReturnType DtoStatus = E_NOT_OK;
  uint8 HeaderLength = XCP_PID_LENGTH;   /* Length of Identification Field and Timestamp */

  DBG_XCP_INSERTSTIM_ENTRY(SduLength,SduDataPtr);

  /* Get the DAQ Id and the ODT from the DTO packet */
  Xcp_GetDaqIDFromDto(SduDataPtr, &DaqID, &OdtIndex);

  if (DaqID < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType))
  {
    /* DAQ list corresponding to the received DTO */
    CONSTP2CONST(Xcp_DaqType,AUTOMATIC,AUTOMATIC) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);

    if (OdtIndex < DaqListPtr->MaxOdt)
    {
      /* Iterator through ODT entries */
      uint8 i;
      /* Local variable to hold address of the requested ODT */
      P2VAR(Xcp_OdtType, AUTOMATIC, XCP_VAR) OdtPtr;
      /* Get the requested ODT */
      OdtPtr = &(DaqListPtr->OdtList[OdtIndex]);

      /* Prevent access if the ODTs were not assigned in the DAQ list (not just allocated, but an initial
       * ALLOC_ODT_ENTRY is needed for all allocated ODTs to be arranged in the dynamic area, if the DAQ
       * list is dynamically allocated) */
      if (OdtPtr != NULL_PTR)
      {
        /* Prevent access if the ODT does not have the address of its first ODT entry (i.e. there was no
         * SET_DAQ_PTR called at all)*/
        if (OdtPtr->OdtEntry != NULL_PTR)
        {
#if (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK)

          /* Is time stamp supported and this is the first ODT of the DAQ list */
          if ((((DaqListPtr->Mode & XCP_MASK_DAQLIST_TIMESTAMPED_MODE) == XCP_MASK_DAQLIST_TIMESTAMPED_MODE)
              ) &&
              (0U == OdtIndex)
             )
          {
            /* Time stamp size must be decremented from DTO length */
            HeaderLength += XCP_TS_LENGTH;
          }

#endif /* #if (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK) */

          /* for each ODT entry */
          for (i = 0U; i < OdtPtr->NrOfOdtEntries; i++)
          {
            DataCount += (uint16)OdtPtr->OdtEntry[i].Length * XCP_GRANULARITY_ODT_ENTRY_SIZE_DAQ;
          }

          /* Check whether the length is already set */
          if (SduLength != 0U)
          {
            /* Does DTO length match with the ODT size (note length due to a
               possible tail) */
            if (DataCount <= (SduLength - HeaderLength))
            {
              DtoStatus = E_OK;
            }
          }
          else
          {
            /* No need to check the length of the DTO */
            DtoStatus = E_OK;
          }
        }
      }
    }
  }

  if (E_OK == DtoStatus)
  {
    DtoStatus = Xcp_InsertOdtToStimBuffer(DaqID, OdtIndex,
      &SduDataPtr[HeaderLength], DataCount);
  }

  DBG_XCP_INSERTSTIM_EXIT(SduLength,SduDataPtr);
}


FUNC(void, XCP_CODE) Xcp_StimulateData
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex
)
{
  /* Consistency of the event */
  uint8 Consistency = Xcp_EventInfo[EventID].Flag & XCP_EVENT_CHANNEL_CONSISTENCY_MASK;

  DBG_XCP_STIMULATEDATA_ENTRY(EventID,EvDaqIndex,OdtIndex);

  switch (Consistency)
  {
    case XCP_EVENT_CHANNEL_CONSISTENCY_ODT:
    {
      /* Get the DAQ List containing the ODT to be stimulated */
      Xcp_DaqIdType DaqID = Xcp_Event[EventID].DaqIdList[EvDaqIndex];
      /* Stimulate the ODT */
      Xcp_StimulateOdt(DaqID, OdtIndex);
      break;
    }
    case XCP_EVENT_CHANNEL_CONSISTENCY_DAQ:
    {
      /* Get the DAQ List to be stimulated */
      Xcp_DaqIdType DaqID = Xcp_Event[EventID].DaqIdList[EvDaqIndex];
      /* Stimulate the DAQ List */
      Xcp_StimulateDaq(DaqID);
      break;
    }
    default:
    {
      /* Stimulate the Event */
      Xcp_StimulateEvent(EventID);
      break;
    }
  }

  DBG_XCP_STIMULATEDATA_EXIT(EventID,EvDaqIndex,OdtIndex);
}

FUNC(Std_ReturnType, XCP_CODE) Xcp_SearchDaqInStimBuffer(Xcp_DaqIdType DaqID)
{
  /* Position of an ODT in STIM buffer */
  uint16 BufIndex;
  Std_ReturnType RetVal = E_NOT_OK;

  DBG_XCP_SEARCHDAQINSTIMBUFFER_ENTRY(DaqID);

  /* Search the first ODT in the STIM buffer */
  RetVal = Xcp_SearchOdtInStimBuffer(DaqID, 0U, &BufIndex);

  /* If the first ODT is present in STIM Buffer */
  if (RetVal == E_OK)
  {
    uint16 BufIndexLast;       /* Position of last ODT in STIM buffer */
    /* Get the DAQ list based on the DAQ list number */
    CONSTP2CONST(Xcp_DaqType,AUTOMATIC,AUTOMATIC) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);

    /* Search the last ODT in the STIM buffer */
    RetVal = Xcp_SearchOdtInStimBuffer(DaqID, DaqListPtr->MaxOdt - 1U, &BufIndexLast);

    /* Check whether all ODTs are present in STIM Buffer */
    if ((RetVal == E_OK) && (((BufIndexLast - BufIndex) + 1U) != DaqListPtr->MaxOdt))
    {
      RetVal = E_NOT_OK;
    }
    else
    {
      /* DAQ list can be stimulated only if the first ODT entry of the first ODT has length > 0 (ASAM
       * requirement) */
      if ((DaqListPtr->OdtList == NULL_PTR) ||
            ((DaqListPtr->OdtList->OdtEntry == NULL_PTR) ||
             (DaqListPtr->OdtList->OdtEntry->Length == 0U)
            )
         )
      {
        RetVal = E_NOT_OK;
      }
    }
  }

  DBG_XCP_SEARCHDAQINSTIMBUFFER_EXIT(RetVal,DaqID);
  return RetVal;
}


FUNC(Std_ReturnType, XCP_CODE) Xcp_SearchOdtInStimBuffer
(
  Xcp_DaqIdType DaqID,
  uint8 OdtIndex,
  P2VAR(uint16, AUTOMATIC, XCP_VAR) Position
)
{
  Std_ReturnType Found = E_NOT_OK;

  DBG_XCP_SEARCHODTINSTIMBUFFER_ENTRY(DaqID,OdtIndex,Position);

  /* No search required if buffer is empty */
  if (Xcp_StimDtoCount == 0U)
  {
    /* Insert as the first entry */
    *Position = 0U;
  }
  else
  {
    /* Indexes for Binary search */
    uint16 Low = 0U;                          /* Lower Index for Binary search */
    uint16 High = (Xcp_StimDtoCount - 1U);    /* Higher Index for Binary search */
    uint16 Mid =  (High / 2U);                /* Middle Index for Binary search */
    uint32 SearchKey = XCP_GET_SEARCH_KEY(DaqID, OdtIndex);   /* Search Key for Binary search */
    /* Key to compare with */
    uint32 StoredKey =
      XCP_GET_SEARCH_KEY(Xcp_StimDtoBuffer[0U].DaqID, Xcp_StimDtoBuffer[0U].OdtIndex);

    /* If only one element is present in the buffer,
     * check the Search Key with the first element only.
     */
    if (Xcp_StimDtoCount == 1U)
    {
      if (SearchKey == StoredKey)
      {
        Found = E_OK;
      }
      /* The other two cases where searched entry is greater than or less than
       * the first entry are checked at the end. So, no need to repeat the
       * check here.
       */
    }
    else
    {
      /* If the searched entry itself is less than the first entry,
       * no need to search the entire buffer. The new entry should
       * be inserted at index 0.
       */
      if (SearchKey < StoredKey)
      {
        Mid = 0U;
      }
      else
      {
        /* Perform binary search */
        while ((Low <= High) && (Found != E_OK))
        {
          Mid = (Low + High) / 2U;
          StoredKey =
            (uint32)XCP_GET_SEARCH_KEY(Xcp_StimDtoBuffer[Mid].DaqID, Xcp_StimDtoBuffer[Mid].OdtIndex);

          if (SearchKey < StoredKey)
          {
            High = Mid - 1U;
          }
          else if (SearchKey > StoredKey)
          {
            Low = Mid + 1U;
          }
          else
          {
            Found = E_OK;
          }
        }
      }
    }

    /* The final insert position should be found by checking
     * the search entry with the entry upto which the search
     * was made.
     */
    if (SearchKey > StoredKey)
    {
      /* Insert the new element at the 'Mid + 1' position */
      Mid++;
    }
    else
    {
      /* Insert the new element at the 'Mid' position */
    }

    /* 'Mid' contains the position where the new entry
     * should be inserted.
     */
    *Position = Mid;
  }

  DBG_XCP_SEARCHODTINSTIMBUFFER_EXIT(Found,DaqID,OdtIndex,Position);
  return Found;
}

FUNC(void, XCP_CODE) Xcp_FlushStimBuffer
(
  void
)
{
  /* Reset number of elements */

  DBG_XCP_FLUSHSTIMBUFFER_ENTRY();
  Xcp_StimDtoCount = 0U;

  DBG_XCP_FLUSHSTIMBUFFER_EXIT();
}

/*==================[internal function definitions]=========================*/

STATIC FUNC(Std_ReturnType, XCP_CODE) Xcp_InsertOdtToStimBuffer
(
  Xcp_DaqIdType DaqID,
  uint8 OdtIndex,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) DataPtr,
  uint16 DataCount
)
{
  uint16 InsertPos;      /* Position in the STIM buffer to insert the new entry */
  Std_ReturnType MatchFound = E_OK;
  Std_ReturnType ReturnVal = E_OK;

  DBG_XCP_INSERTODTTOSTIMBUFFER_ENTRY(DaqID,OdtIndex,DataPtr,DataCount);

  /* Protect Stim buffer from concurrent access */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  if (Xcp_StimDtoCount == 0U)
  {
    InsertPos = 0U;
    Xcp_StimDtoCount++;
  }
  else
  {
    /* Search the ODT in the STIM buffer */
    MatchFound = Xcp_SearchOdtInStimBuffer(DaqID, OdtIndex, &InsertPos);

    if (E_OK != MatchFound)
    {
      /* If the buffer is already full */
      if (Xcp_StimDtoCount != XCP_STIM_BUFFER_SIZE)
      {
        uint16 Index;

        /* Shift the entries by one position */
        for (Index = Xcp_StimDtoCount; Index > InsertPos; Index--)
        {
          Xcp_StimDtoBuffer[Index] = Xcp_StimDtoBuffer[Index - 1U];
        }

        Xcp_StimDtoCount++;
      }
      else
      {
        ReturnVal = E_NOT_OK;
      }
    }
  }

  if (E_OK == ReturnVal)
  {
    /* Insert the new entry */
    Xcp_StimDtoBuffer[InsertPos].DaqID = DaqID;
    Xcp_StimDtoBuffer[InsertPos].OdtIndex = OdtIndex;
    TS_MemCpy(Xcp_StimDtoBuffer[InsertPos].Data, DataPtr, DataCount);
  }

  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_INSERTODTTOSTIMBUFFER_EXIT(ReturnVal,DaqID,OdtIndex,DataPtr,DataCount);
  return ReturnVal;
}


STATIC FUNC(void, XCP_CODE) Xcp_RemoveOdtFromStimBuffer
(
  uint16 BufferIndex
)
{
  uint16 Index;

  DBG_XCP_REMOVEODTFROMSTIMBUFFER_ENTRY(BufferIndex);

  /* Shrink the Buffer by one position */
  for(Index = BufferIndex; Index < (Xcp_StimDtoCount - 1U); Index++)
  {
    Xcp_StimDtoBuffer[Index] = Xcp_StimDtoBuffer[Index + 1U];
  }

  Xcp_StimDtoCount--;

  DBG_XCP_REMOVEODTFROMSTIMBUFFER_EXIT(BufferIndex);
}

STATIC FUNC(void, XCP_CODE) Xcp_StimulateOdtEntries
(
  Xcp_DaqIdType DaqID,
  uint8 OdtIndex,
  uint16 BufferIndex
)
{
  uint16 DataCount = 0;
  uint8 OdtEntryCount;
  uint8 NrOfOdtEntries;  /* Number of ODT entries the ODT has */
  P2VAR(uint8, AUTOMATIC, XCP_VAR) DataPtr;
  P2VAR(Xcp_OdtEntryType, AUTOMATIC, XCP_VAR) OdtEntryPtr; /* Entry pointer */
  /* Get the DAQ list based on the DAQ list number */
  CONSTP2CONST(Xcp_DaqType,AUTOMATIC,AUTOMATIC) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);

  DBG_XCP_STIMULATEODTENTRIES_ENTRY(DaqID,OdtIndex,BufferIndex);

  NrOfOdtEntries = DaqListPtr->OdtList[OdtIndex].NrOfOdtEntries;
  OdtEntryPtr = DaqListPtr->OdtList[OdtIndex].OdtEntry;

  DataPtr = Xcp_StimDtoBuffer[BufferIndex].Data;

  /* for each ODT entry */
  for (OdtEntryCount = 0U; OdtEntryCount < NrOfOdtEntries; OdtEntryCount++)
  {
    uint16 Length = (uint16)OdtEntryPtr[OdtEntryCount].Length * XCP_GRANULARITY_ODT_ENTRY_SIZE_DAQ;

    if (Length != 0U)
    {
      /* Copy data from STIM DTO buffer to the ODT entry */
      TS_MemCpy(XCP_APPL_GET_ADDRESS( 0U, OdtEntryPtr[OdtEntryCount].Address ),
                &(DataPtr[DataCount]),
                Length);

      /* Increment the data count by length */
      DataCount += Length;
    }
    else
    {
      /* We need to stop as soon as we find an ODT entry with size 0 - ASAM requirement */
      break;
    }
  }

  DBG_XCP_STIMULATEODTENTRIES_EXIT(DaqID,OdtIndex,BufferIndex);
}

STATIC FUNC(void, XCP_CODE) Xcp_StimulateOdt
(
  Xcp_DaqIdType DaqID,
  uint8 OdtIndex
)
{
  uint16 BufferIndex; /* Index in the STIM buffer */

  DBG_XCP_STIMULATEODT_ENTRY(DaqID,OdtIndex);

  /* Protect Stim buffer from concurrent access.
   * Also, ensure the consistency of BufferIndex through
   * Xcp_SearchOdtInStimBuffer, Xcp_StimulateOdtEntries
   * and Xcp_RemoveOdtFromStimBuffer
   */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  if (Xcp_SearchOdtInStimBuffer(DaqID, OdtIndex, &BufferIndex) == E_OK)
  {
    Xcp_StimulateOdtEntries(DaqID, OdtIndex, BufferIndex);
  }

  Xcp_RemoveOdtFromStimBuffer(BufferIndex);

  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_STIMULATEODT_EXIT(DaqID,OdtIndex);
}

STATIC FUNC(void, XCP_CODE) Xcp_StimulateDaq
(
  Xcp_DaqIdType DaqID
)
{
  uint8 OdtIndex; /* Variable to loop through each odt */
  uint16 BufferIndex; /* Index in the STIM buffer */

  DBG_XCP_STIMULATEDAQ_ENTRY(DaqID);

  /* Protect Stim buffer from concurrent access.
   * Also, ensure the consistency of BufferIndex through
   * Xcp_SearchOdtInStimBuffer, Xcp_StimulateOdtEntries
   * and Xcp_RemoveOdtFromStimBuffer
   */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Search the first ODT for the given DAQ is in STIM buffer */
  if (Xcp_SearchOdtInStimBuffer(DaqID, 0U, &BufferIndex) == E_OK)
  {
    /* Get the DAQ list based on the DAQ list number */
    CONSTP2CONST(Xcp_DaqType,AUTOMATIC,AUTOMATIC) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);
    /* All the following ODTs are associated with the DAQ */
    for (OdtIndex = 0U; OdtIndex < DaqListPtr->MaxOdt; OdtIndex++)
    {
      Xcp_StimulateOdtEntries(DaqID, OdtIndex, BufferIndex);

      /* Remove the entry from STIM Buffer.
       * BufferIndex is not required to be incremented
       * As removing the entry already shifts the following entries
       */
      Xcp_RemoveOdtFromStimBuffer(BufferIndex);
    }
  }

  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_STIMULATEDAQ_EXIT(DaqID);
}

STATIC FUNC(void, XCP_CODE) Xcp_StimulateEvent
(
  uint16 EventID
)
{
  uint8 EvDaqIndex; /* Variable to loop through each Daq in the event */

  DBG_XCP_STIMULATEEVENT_ENTRY(EventID);

  for (EvDaqIndex = 0U; EvDaqIndex < Xcp_Event[EventID].DaqIdListCount; EvDaqIndex++)
  {
    /* Get the DAQ list number */
    Xcp_DaqIdType DaqID = Xcp_Event[EventID].DaqIdList[EvDaqIndex];
    /* Get each DAQ list for this event */
    CONSTP2CONST(Xcp_DaqType,AUTOMATIC,AUTOMATIC) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);

    /* Check if DAQ List is running */
    if ((DaqListPtr->Mode & XCP_MASK_DAQLIST_RUNNING) == XCP_MASK_DAQLIST_RUNNING)
    {
      /* Stimulate all the ODTs in the Daq */
      Xcp_StimulateDaq(DaqID);
    }
  }

  DBG_XCP_STIMULATEEVENT_EXIT(EventID);
}

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

#endif

/*==================[end of file]===========================================*/
