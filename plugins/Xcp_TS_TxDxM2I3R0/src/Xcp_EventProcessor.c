/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 17.4 (required)
 *    "Array indexing shall be the only allowed form of pointer arithmetic."
 *
 *    Reason: In special cases pointer arithmetic is more efficient in run
 *    time and/or code size.
 *
 */

/*==================[inclusions]============================================*/

#include <Xcp_Trace.h>
#include <Std_Types.h>         /* AUTOSAR standard types */
#include <TSAutosar.h>         /* EB specific standard types */
#include <TSMem.h>             /* EB memory functions */
#include <SchM_Xcp.h>          /* Needed for exclusive area definition */

#include <Xcp.h>               /* Module public API */
#include <Xcp_Int.h>           /* Module internal interface */
#include <Xcp_UserCallouts.h>  /* XCP user callout functions */

#if (XCP_TIMESTAMP_TYPE != XCP_NO_TIME_STAMP_TS_MASK)

#if (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF)
#include <os.h>                  /* Os counter is required */
#else
#include <Xcp_UserCallouts.h>    /* User Timestamp is supported: XCP User Callouts header */
#endif

#endif /* (XCP_TIMESTAMP_TYPE != XCP_NO_TIME_STAMP_TS_MASK) */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>

#if (XCP_MAX_DAQ != 0U)

/** \brief Function to update the currently processed event
 **
 ** This function checks the consistency of the current processed set event
 ** and sets up the DAQ list/ ODT to be processed depending on the consistency level of that event.
 **
 ** \param[in] EventID     The event channel which needs to be processed
 ** \param[in] EvDaqIndex  The DAQ list which needs to be processed
 ** \param[in] OdtIndex    The ODT index which needs to be processed
 */
STATIC FUNC(void, XCP_CODE) Xcp_UpdateEvent
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex
);

/** \brief Function to update or clear the event for DAQ consistency
 **
 ** This function checks if all the DAQ lists of the currently processed event are covered:
 ** if covered the event will be clear, else the next DAQ list will be selected to be processed.
 **
 ** \param[in] EventID     The Event ID which needs to be processed
 ** \param[in] EvDaqIndex  The DAQ list which needs to be processed
 */
STATIC FUNC(void, XCP_CODE) Xcp_UpdateDaq
(
  uint16 EventID,
  uint8 EvDaqIndex
);

/** \brief Function to update or clear the currently processed event for Odt consistency
 **
 ** This function checks if all the ODTs of the currently processed event are covered: if covered
 ** then the next DAQ list information in the event list will be considered, if all the associated
 ** DAQ lists are covered then the event will be clear.
 **
 ** \param[in] EventID     The Event ID which needs to be processed
 ** \param[in] EvDaqIndex  The Daq Index which needs to be processed
 ** \param[in] OdtIndex    The Odt Index which needs to be processed
 */
STATIC FUNC(void, XCP_CODE) Xcp_UpdateOdt
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex
);

/** \brief Function to Sample the DTOs
 **
 ** This function is used to sample the DTOs depending on the consistency of
 ** the event.
 **
 ** \param[in] EventID     The Event ID which needs to be processed
 ** \param[in] EvDaqIndex  The Daq Index which needs to be processed
 ** \param[in] OdtIndex    The Odt Index which needs to be processed
 */
STATIC FUNC(void, XCP_CODE) Xcp_SampleData
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex
);

/** \brief Function to Sample the Header of the DTO
 **
 ** This function is used to get the PID and time stamp into the DTO.
 **
 ** \param[in] EventID     The Event ID which needs to be processed
 ** \param[in] EvDaqIndex  The Daq Index which needs to be processed
 ** \param[in] OdtIndex    The Odt Index which needs to be processed
 ** \param[out] DTOEntryPtr The DTO Entry information
 */
STATIC FUNC(void, XCP_CODE) Xcp_SampleHeader
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex,
  P2VAR(Xcp_DTOType, AUTOMATIC, XCP_APPL_DATA) DTOEntryPtr
);

/** \brief Function to Dequeue one DTO from DTO Queue.
 **
 ** Use this function to dequeue one DTO from DTO queue.
 ** DTO data bytes will be copied byte by byte from queue head.
 ** Queue head  will be updated to the next CTO length information
 ** in queue and rolled over if necessary.
 ** Number of bytes in queue will also be updated.
 **
 ** \param[out] BufferPtr     Buffer to which DTO shall be dequeued
 */
STATIC FUNC(void, XCP_CODE) Xcp_DeQueueDtoQueue
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA) BufferPtr
);

/** \brief Function to Sample the Data of the DTO
 **
 ** This function is used to get the data from the address, and store the
 ** entire DTO information in the DTO queue.
 **
 ** \param[in] EventID     The Event ID which needs to be processed
 ** \param[in] EvDaqIndex  The Daq Indexwhich needs to be processed
 ** \param[in] OdtIndex    The Odt Index which needs to be processed
 ** \param[inout] DTOEntryPtr The DTO Entry information
 */
STATIC FUNC(void, XCP_CODE) Xcp_SampleDtoData
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex,
  P2VAR(Xcp_DTOType, AUTOMATIC, XCP_APPL_DATA) DTOEntryPtr
);

/** \brief Function to update the DTO queue with the freshly sampled DTO
 ** \param[in] DTOEntry The DTO Entry information
 */
STATIC FUNC(void, XCP_CODE) Xcp_AddDtoInQueue
(
  Xcp_DTOType DTOEntry
);

/** \brief Function to Sample the DAQ for DTO
 **
 ** This function is used to get the data from the address, and store the
 ** entire DTO information in the DTO queue for the entire Daq.
 **
 ** \param[in] EventID     The Event ID which needs to be processed
 ** \param[in] EvDaqIndex  The Daq Indexwhich needs to be processed
 ** \param[out] DTOEntryPtr The DTO Entry information
 */
STATIC FUNC(void, XCP_CODE) Xcp_SampleDaq
(
  uint16 EventID,
  uint8 EvDaqIndex,
  P2VAR(Xcp_DTOType, AUTOMATIC, XCP_APPL_DATA) DTOEntryPtr
);

/** \brief Function to Sample the Event for DTO
 **
 ** This function is used to get the data from the address, and store the
 ** entire DTO information in the DTO queue for the entire Event.
 **
 ** \param[in] EventID     The Event Id which needs to be processed
 ** \param[out] DTOEntryPtr The DTO Entry information
 */
STATIC FUNC(void, XCP_CODE) Xcp_SampleEvent
(
  uint16 EventID,
  P2VAR(Xcp_DTOType, AUTOMATIC, XCP_APPL_DATA) DTOEntryPtr
);

/** \brief Function to find the next ODT to be sampled or stimulated for the currently processed event.
 **
 ** This function is used to find the next ODT to be processed based on the direction of the
 ** DAQ lists for the currently processed event. If the DAQ List direction is 'DAQ',
 ** it calculates the total memory needed for sampling data of the ODT.
 ** If the DAQ List direction is 'STIM', it checks whether the data for the ODT(s) are available
 ** in the STIM buffer.
 **
 ** \param[in] EventID        The event channel which is currently processed
 ** \param[out] DaqListIndex  The DAQ list index which needs to be processed
 ** \param[out] OdtIndex      The Odt Index which needs to be processed
 ** \param[out] MemorySize    The total memory needed for sampling the data of ODT
 **
 ** \retval E_OK      Found the next ODT to process
 ** \retval E_NOT_OK  Could not find the next ODT to process
 */
STATIC FUNC(Std_ReturnType, XCP_CODE)  Xcp_GetNextOdtToProcess
(
  uint16 EventID,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) DaqListIndex,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) OdtIndex,
  P2VAR(uint16, AUTOMATIC, AUTOMATIC) MemorySize
);

/** \brief Function to calculate the total memory needed to store the DTO of a single ODT
 **
 ** This function is used to calculate the total memory needed to store the data of a single ODT
 **
 ** \param[in] DaqListId  The DAQ list Id which needs to be processed
 ** \param[in] OdtIndex    The Odt Index which needs to be processed
 **
 ** \return The amount of memory required in the DTO queue for a single ODT.
 */
STATIC FUNC(uint16, XCP_CODE)  Xcp_GetOdtSize(Xcp_DaqIdType DaqListId, uint8 OdtIndex);

/** \brief Function to calculate the total memory needed to store only the data of a single ODT
 ** in DTO
 **
 ** This function is used to calculate the total memory needed to store the data of a single ODT
 **
 ** \param[in] DaqListId  The DAQ list Id which needs to be processed
 ** \param[in] OdtIndex    The Odt Index which needs to be processed
 **
 ** \return The amount of memory required in the DTO queue for storing the data of a single ODT.
 */
STATIC FUNC(uint16, XCP_CODE)  Xcp_GetDtoDataSize(Xcp_DaqIdType DaqListId, uint8 OdtIndex);

/** \brief Function to calculate the total memory needed to store only the header of a single ODT
 ** in DTO
 **
 ** This function is used to calculate the total memory needed to store the header of a single ODT
 **
 ** \param[in] DaqListId  The DAQ list Id which needs to be processed
 ** \param[in] OdtIndex    The Odt Index which needs to be processed
 **
 ** \return The amount of memory required in the DTO queue for storing the header of a single ODT.
 */
STATIC FUNC(uint16, XCP_CODE)  Xcp_GetDtoHeaderSize(Xcp_DaqIdType DaqListId, uint8 OdtIndex);

/** \brief Function to calculate the total memory needed to store the DTOs of a DAQ list
 **
 ** This function is used to calculate the total memory needed to store the the DTOs of a DAQ list
 **
 ** \param[in] DaqListId  The DAQ list Id which needs to be processed
 **
 ** \return The amount of memory required in the DTO queue to store the DTOs of a DAQ list.
 */
STATIC FUNC(uint16, XCP_CODE)  Xcp_GetDaqSize(Xcp_DaqIdType DaqListId);

/** \brief Function to calculate the total memory needed to store the DTOs of an Event
 **
 ** This function is used to calculate the total memory needed to store the the DTOs of a DAQ Event
 **
 ** \param[in] EventId  The Event which needs to be processed
 **
 ** \return The amount of memory required in the DTO queue to store the DTOs of an Event.
 */
STATIC FUNC(uint16, XCP_CODE)  Xcp_GetEventSize(uint16 EventId);

/** \brief Function to determine whether a DAQ list is ready to be sampled
 **
 ** Use this function before sampling a DAQ List or before calculating memory
 ** required by the DTOs while servicing an event.
 **
 ** This function checks whether the DAQ list is of direction DAQ and whether
 ** the DAQ List is running, if so a DAQ list is ready if its prescaler counter reads 1(one).
 ** Otherwise the prescaler shall be decremented when the DAQ is verified for the first ODT.
 **
 ** \param[in] DaqListNumber  The number of the required DAQ List
 ** \param[in] OdtIndex       The number of the required ODT in the specified DAQ List
 **
 */
STATIC FUNC(void, XCP_CODE) Xcp_IsDAQReadyToBeSampled
(
  Xcp_DaqIdType DaqListNumber,
  uint8 OdtIndex
);


/** \brief Function to find the next ODT to be sampled
 **
 ** This function is used to find the next ODT to be sampled for the currently processed event.
 ** It also calculates the total memory needed for sampling data of the ODT.
 **
 ** \param[in] EventID  The event channel which is currently being processed.
 ** \param[out] DaqListIndex  The DAQ list index which needs to be processed
 ** \param[out] OdtIndex      The Odt Index which needs to be processed
 **
 ** \retval The total memory needed for sampling the data of ODT
 */
STATIC FUNC(uint16, XCP_CODE)  Xcp_GetNextDaqOdt
(
  uint16 EventID,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) DaqListIndex,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) OdtIndex
);

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
/** \brief Function to find the next STIM ODT to be stimulated
 **
 ** Function to find the next STIM ODT to be stimulated for the currently processed event.
 ** It checks whether the data for the all ODT(s) are available
 ** in the STIM buffer based on the consistency.
 **
 ** \param[in] EventID  The ID of the event which is currently being processed
 ** \param[inout] DaqListIndex  Index of the DAQ list which needs to be processed
 ** \param[inout] OdtIndex      The Odt Index which needs to be processed
 **
 ** \retval E_OK      Found the next ODT to process
 ** \retval E_NOT_OK  Could not find the next ODT to process
 */
STATIC FUNC(Std_ReturnType, XCP_CODE)  Xcp_GetNextStimOdt
(
  uint16 EventID,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) DaqListIndex,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) OdtIndex
);
#endif

#if ((XCP_EVENT_PACKET_ENABLED == STD_ON) && (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))
 /** \brief Function to send to the master an EV_STIM_TIMEOUT event.
 **
 ** Function will send the EV_STIM_TIMEOUT to the XCP master with the required information.
 **
 ** \param[in] EventInfoType  The Info type of the event to be sent
 ** \param[in] EventInfo      Event channel number or DAQ list number depending on Info Type
 */
STATIC FUNC(void, XCP_CODE)  Xcp_EmitEventSTIMTimeout
(
  uint8 EventInfoType,
  uint16 EventInfo
);

#endif /* (XCP_EVENT_PACKET_ENABLED == STD_ON) && (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) */

/** \brief Function to concatenate two bytes into one word.
**  \param[in] b1 - first byte to be concatenated
**  \param[in] b2 - second byte to be concatenated
**  \return the calculated word value
**/
STATIC FUNC(uint16, XCP_CODE) Xcp_Uint16FromUint8Queue
(
  const uint8 b1,
  const uint8 b2
);


/** \brief Function to set the event which will be processed in this cycle.
**
** Function will search within the events which were set the highest priority one and mark it as the
** one to be processed in this cycle.
**
** \return The current event which needs to be processed in this main function cycle
*/
STATIC FUNC(uint16, XCP_CODE) Xcp_GetCurrentEventToProcess(void);

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

#define XCP_START_SEC_VAR_8BIT
#include <MemMap.h>

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
/** \brief DaqList status in Event CTO Queue */
STATIC VAR(boolean, XCP_VAR) Xcp_DaqEventCTO = FALSE;
#endif

#define XCP_STOP_SEC_VAR_8BIT
#include <MemMap.h>

/*==================[external function definitions]=========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>


/*------------------[Xcp_ResetEventProcessor]--------------------------------*/

FUNC(void, XCP_CODE) Xcp_ResetEventProcessor
(
  void
)
{
  /* Local variable to hold DAQ List number */
  Xcp_DaqIdType DaqListNumber;
  /* Local variable to hold Event Channel number */
  uint16 EventChannel;

  DBG_XCP_RESETEVENTPROCESSOR_ENTRY();

  /* Reset the runtime configuration for each DAQ List */
  for (DaqListNumber = 0U; DaqListNumber < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType);
       DaqListNumber++)
  {
    /* Reset the running status and selected status */
    Xcp_ResetDaqListData(DaqListNumber);
  }

  /* Initialize Event channel run time properties:
   *  - Reset number of DAQ List associated with the event.
   *  - Reset run time status of the event channel.
   *  - Reset DAQ List Ids associated with the event.
   */
  for (EventChannel = 0U; EventChannel < XCP_MAX_EVENT_CHANNEL; ++EventChannel)
  {
    /* Local variable to hold the index of associated DaqList */
    uint8 EvDaqIndex;

    /* Reset run time status of the event channel */
    Xcp_Event[EventChannel].EventIsSet = FALSE;
    /* Reset number of DAQ Lists associated with the event channel */
    Xcp_Event[EventChannel].DaqIdListCount = Xcp_EventInfo[EventChannel].DaqIdListCount;

    /* Set Event Channel Time Cycle Counter */
    Xcp_Event[EventChannel].TimeCycleCounter = Xcp_EventInfo[EventChannel].CycleCounterMax;

    /* Reset associated DaqId list for the event */
    for (EvDaqIndex = 0U; EvDaqIndex < Xcp_Event[EventChannel].DaqIdListCount; ++EvDaqIndex)
    {
      Xcp_Event[EventChannel].DaqIdList[EvDaqIndex] =
        Xcp_EventInfo[EventChannel].DaqIdList[EvDaqIndex];
    }
  }

  DBG_XCP_RESETEVENTPROCESSOR_EXIT();
}

/*------------------[Xcp_ResetDaqListData]--------------------------------*/

FUNC(void, XCP_CODE) Xcp_ResetDaqListData
(
  Xcp_DaqIdType DaqListNumber
)
{
  DBG_XCP_RESETDAQLISTDATA_ENTRY(DaqListNumber);

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
#if (XCP_MIN_DAQ > 0)
  if (DaqListNumber >= XCP_MIN_DAQ) /* DAQ list is dynamically configured */
#endif
  {
    /* Get the pointer to the DAQ list */
    P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

    /* Reset data */
    DaqListPtr->OdtList = NULL_PTR;
    DaqListPtr->EventId = XCP_INVALID_EVENT_CHANNEL;
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
    DaqListPtr->FirstPID = 0U;
#endif
    DaqListPtr->Flags = 0U;
    DaqListPtr->MaxOdt = 0U;
    DaqListPtr->MaxOdtEntries = 0U;
    DaqListPtr->Mode = 0U;
    DaqListPtr->Prescaler = 0U;
    DaqListPtr->PrescalerCnt = 0U;
    DaqListPtr->Priority = 0U;
    DaqListPtr->Properties = 0U;
  }
#if (XCP_MIN_DAQ > 0)
  else
#endif
#endif /* XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK */
  {
#if ((XCP_MAX_DAQ != 0U) && (XCP_MAX_DAQ > XCP_DAQ_COUNT))
    /* Initialize the DAQ list with data from ROM  */
    TS_MemCpy(XCP_GET_DAQ_LIST_PTR(DaqListNumber),&Xcp_DaqListsDefault.Xcp_Daq[DaqListNumber],
        (uint16)(sizeof(Xcp_DaqType)));
#endif
  }

  DBG_XCP_RESETDAQLISTDATA_EXIT(DaqListNumber);
}

#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

/*------------------[Xcp_ProcessEvents]---------------------------------*/
FUNC(void, XCP_CODE) Xcp_ProcessEvents(void)
{
  uint16 MemorySize = 0U; /* Memory required for sampling */
  uint8 EvDaqIndex = 0U; /* Daq Index to be sampled */
  uint8 OdtIndex = 0U; /* ODT Index to be sampled */
  Std_ReturnType DataStatus = E_NOT_OK;
  boolean ProcessEvent = FALSE;  /* Data sampling or stimulation is required */
  /* the event that needs processing in this cycle */
  uint16 CurrentEventToProcess;

  DBG_XCP_PROCESSEVENTS_ENTRY();

  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Set the current event to process */
  CurrentEventToProcess = Xcp_GetCurrentEventToProcess();

  /* Check if there are entries in the event list */
  while ((CurrentEventToProcess != XCP_INVALID_EVENT_CHANNEL) &&
         ((MemorySize == 0U) && (DataStatus != E_OK ))
        )
  {
    DataStatus = Xcp_GetNextOdtToProcess(CurrentEventToProcess, &EvDaqIndex, &OdtIndex, &MemorySize);

    /* Check if there is any information to be sampled or stimulated */
    if ((0U != MemorySize) || (E_OK == DataStatus))
    {
      /* Is sufficient memory available */
      if (MemorySize <= (XCP_DTO_QUEUE_SIZE - Xcp_DtoQueue.NoOfBytes))
      {
        /* Update the currently processed event with the given information */
        Xcp_UpdateEvent(CurrentEventToProcess, EvDaqIndex, OdtIndex);

        /* Data sampling or stimulation is required */
        ProcessEvent = TRUE;
      }
    }
    else
    {
      /* Clear event */
      Xcp_Event[CurrentEventToProcess].EventIsSet = FALSE;
      /* Set current event as invalid */
      CurrentEventToProcess = XCP_INVALID_EVENT_CHANNEL;
    }
  }

  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  if (TRUE == ProcessEvent)
  {
#if (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK)
    if (MemorySize > 0U)
    {
      /* Sample the data and store the information in DTO Queue */
      Xcp_SampleData(CurrentEventToProcess, EvDaqIndex, OdtIndex);
    }
#endif

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
    if (E_OK == DataStatus)
    {
      /* Stimulate the data */
      Xcp_StimulateData(CurrentEventToProcess, EvDaqIndex, OdtIndex);
    }
#endif
  }

  DBG_XCP_PROCESSEVENTS_EXIT();
}

#endif

/*------------------[Xcp_FlushDTOQueue]-------------------------------------*/
FUNC(void, XCP_CODE) Xcp_FlushDTOQueue(void)
{
  DBG_XCP_FLUSHDTOQUEUE_ENTRY();

  Xcp_DtoQueue.Head      = 0U;  /* Reset queue head */
  Xcp_DtoQueue.Tail      = 0U;  /* Reset queue tail */
  Xcp_DtoQueue.NoOfBytes = 0U;  /* Reset number of bytes in queue */

  DBG_XCP_FLUSHDTOQUEUE_EXIT();
}

/*----------------------------[Xcp_GetDTO]----------------------------------*/

FUNC(Xcp_ErrorType, XCP_CODE) Xcp_GetDTO
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA)         TxBufferPtr,
  P2VAR(PduLengthType, AUTOMATIC, XCP_APPL_DATA) TxLengthPtr,
  const PduLengthType                            SpaceAvailable
)
{
  /* Local variable to hold retrun value */
  Xcp_ErrorType RetVal;

  DBG_XCP_GETDTO_ENTRY(TxBufferPtr,TxLengthPtr,SpaceAvailable);

  /* Enter critical section.*/
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Check whether a response DTO is available */
  if (XCP_IS_DTO_QUEUE_EMPTY() == FALSE)
  {
    /* Local variable to hold length of next response CTO */
    const uint16 LengthOfDTO = Xcp_Uint16FromUint8Queue(
      Xcp_DtoQueue.Queue[Xcp_DtoQueue.Head],
      Xcp_DtoQueue.Queue[(Xcp_DtoQueue.Head + 1U) % XCP_DTO_QUEUE_SIZE]);

    /* Check whether next DTO fits in the available space */
    if (SpaceAvailable >= LengthOfDTO)
    {
      /* Dequeue response DTO from DTO queue to transmit buffer */
      Xcp_DeQueueDtoQueue(TxBufferPtr);
      /* Update transmit data length */
      *TxLengthPtr += (PduLengthType)LengthOfDTO;
      RetVal = XCP_E_OK;
    }
    else
    {
      /* Space not available to copy response DTO packet */
      RetVal = XCP_E_NOT_OK;
    }
  }
  else
  {
    /* DTO packets not available */
    RetVal = XCP_E_NOT_OK;
  }

  /* Exit critical section.*/
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();


  DBG_XCP_GETDTO_EXIT(RetVal,TxBufferPtr,TxLengthPtr,SpaceAvailable);
  return RetVal;
}

/*-----------------------[Xcp_ClearStoppedDtos]------------------------------*/
FUNC(void, XCP_CODE) Xcp_ClearStoppedDtos(void)
{
  uint16 QueueIndex = Xcp_DtoQueue.Head;
  uint16 QueueSize = Xcp_DtoQueue.NoOfBytes;

  DBG_XCP_CLEARSTOPPEDDTOS_ENTRY();

  /* Process all DTOs in the queue */
  while (QueueSize != 0U)
  {
    uint8 OdtIndex;
    uint16 DtoSize;
    Xcp_DaqIdType DaqID;
    /* Variable to hold address of the ongoing DAQ List */
    P2VAR(Xcp_DaqType, AUTOMATIC, XCP_APPL_DATA) DaqListPtr;

    /* Find the size of the current element in Queue */
    DtoSize = Xcp_Uint16FromUint8Queue(Xcp_DtoQueue.Queue[QueueIndex],
      Xcp_DtoQueue.Queue[(QueueIndex + 1U) % XCP_DTO_QUEUE_SIZE]);
    DtoSize += 2U;

    /* Find the Daq Id from DTO info */
    Xcp_GetDaqIDFromDto(&Xcp_DtoQueue.Queue[(QueueIndex + 2U) % XCP_DTO_QUEUE_SIZE],
      &DaqID, &OdtIndex);

    /* Get a pointer to the DAQ list based on the DAQ list Id fetched in the previous call */
    DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);

    /* Check whether DAQ List is already running */
    if (XCP_IS_DAQ_LIST_RUNNING(DaqListPtr) != TRUE)
    {
      uint16 TempSize;

      /* Update the number of bytes in DTO Queue with the size
       * of the DTO to be removed
       */
      Xcp_DtoQueue.NoOfBytes -= DtoSize;

      /* Calculate the number of bytes to be moved in the DTO Queue
       * for the selected DTO to be removed.
       */
      TempSize = Xcp_DtoQueue.NoOfBytes -
        (((XCP_DTO_QUEUE_SIZE + QueueIndex) - Xcp_DtoQueue.Head) % XCP_DTO_QUEUE_SIZE);

      /* Check whether just updating the Head will do the trick */
      if (QueueIndex == Xcp_DtoQueue.Head)
      {
        Xcp_DtoQueue.Head = (Xcp_DtoQueue.Head + DtoSize) % XCP_DTO_QUEUE_SIZE;
        /* The DTO at the updated Head becomes the next DTO to be checked */
        QueueIndex = Xcp_DtoQueue.Head;
      }
      else
      {
        /* Update the Tail with the the size of the DTO to be removed */
        Xcp_DtoQueue.Tail = ((XCP_DTO_QUEUE_SIZE + Xcp_DtoQueue.Tail) - DtoSize) %
          XCP_DTO_QUEUE_SIZE;

        /* If any DTOs are to be moved */
        if (0U != TempSize)
        {
          /* Calculate the Source and destination indexes */
          uint16 SrcIndex = (QueueIndex + DtoSize) % XCP_DTO_QUEUE_SIZE;
          uint16 DstIndex = QueueIndex;

          while (0U != TempSize)
          {
            /* Move the DTOs to replace the DTO being removed */
            Xcp_DtoQueue.Queue[DstIndex] = Xcp_DtoQueue.Queue[SrcIndex];
            DstIndex = (DstIndex + 1U) % XCP_DTO_QUEUE_SIZE;
            SrcIndex = (SrcIndex + 1U) % XCP_DTO_QUEUE_SIZE;
            TempSize--;
          }
        }
      }
    }
    else
    {
      /* Select the next DTO */
      QueueIndex = (QueueIndex + DtoSize) % XCP_DTO_QUEUE_SIZE;
    }

    /* Repeat till all DTOs are processed */
    QueueSize -= DtoSize;
  }

  DBG_XCP_CLEARSTOPPEDDTOS_EXIT();
}

/*-----------------------------[Xcp_ResetEventChannels]---------------------*/

FUNC(void, XCP_CODE) Xcp_ResetEventChannels(void)
{
  /* iterator */
  uint16 i;

  DBG_XCP_RESETEVENTCHANNELS_ENTRY();
  /* Reset all events runtime values */
  for (i=0U; i< XCP_MAX_EVENT_CHANNEL; i++)
  {
    Xcp_Event[i].EventIsSet = FALSE;
    Xcp_Event[i].NextDaqIndex = 0U;
    Xcp_Event[i].NextOdtIndex = 0U;
  }

  DBG_XCP_RESETEVENTCHANNELS_EXIT();
}

/*-----------------------------[Xcp_GetDaqIDFromDto]---------------------*/

FUNC(void, XCP_CODE) Xcp_GetDaqIDFromDto
(
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) DtoPtr,
  P2VAR(Xcp_DaqIdType, AUTOMATIC, AUTOMATIC) DaqID,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) OdtIndex
)
{
  uint8 OdtIndexLocal = DtoPtr[0U];
  Xcp_DaqIdType DaqIDLocal = 0U;

  DBG_XCP_GETDAQIDFROMDTO_ENTRY(DtoPtr,DaqID,OdtIndex);

#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)

  /* Find the DAQ list that has the queried ODT */
  for (DaqIDLocal = 0U; DaqIDLocal < XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(Xcp_DaqIdType);
       DaqIDLocal++)
  {
    /* Get the DAQ list */
    CONSTP2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqIDLocal);
    if (OdtIndexLocal < (DaqListPtr->FirstPID + DaqListPtr->MaxOdt))
    {
      /* We have found our DAQ list that has the ODT corresponding to the PID,
       * calculate the original ODT index */
      OdtIndexLocal -= DaqListPtr->FirstPID;
      break;
    }
  }

#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_BYTE_IF_MASK)

  /* Get the Daq Id directly from DTO */
  DaqIDLocal = DtoPtr[1U];

#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_WORD_IF_MASK)

  /* Get the 16-bit Daq Id directly from DTO */
  DaqIDLocal = XCP_UINT16_FROM_PID(&DtoPtr[1U]);

#else

  /* Get the 16-bit Daq Id directly from DTO */
  DaqIDLocal = XCP_UINT16_FROM_PID(&DtoPtr[2U]);

#endif

  *DaqID = DaqIDLocal;
  *OdtIndex = OdtIndexLocal;

  DBG_XCP_GETDAQIDFROMDTO_EXIT(DtoPtr,DaqID,OdtIndex);
}

/*==================[internal function definitions]=========================*/

/*------------------[Xcp_IsDAQReadyToBeSampled]-----------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_IsDAQReadyToBeSampled
(
  Xcp_DaqIdType DaqListNumber,
  uint8 OdtIndex
)
{
  /* Flag to check if the DAQ list is OK to be sampled now */
  boolean CanBeSampled = FALSE;

  /* Get the DAQ list based on the DAQ list number */
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListNumber);

  DBG_XCP_ISDAQREADYTOBESAMPLED_ENTRY(DaqListNumber,OdtIndex);

  /* DAQ list can be sampled only if the first ODT entry of the first ODT has length > 0 (ASAM
   * requirement) */
  if ((DaqListPtr->OdtList != NULL_PTR) &&
      ((DaqListPtr->OdtList->OdtEntry != NULL_PTR) &&
       (DaqListPtr->OdtList->OdtEntry->Length > 0U)
      )
     )
  {
    /*If the DAQ list active and the direction is DAQ */
    if (((DaqListPtr->Mode & XCP_MASK_DAQLIST_RUNNING) == XCP_MASK_DAQLIST_RUNNING) &&
        ((DaqListPtr->Mode &  XCP_MASK_DAQLIST_DIRECTION) == XCP_MASK_DAQLIST_DIRECTION_DAQ)
       )
    {
#if (XCP_PRESCALER_SUPPORTED == XCP_PRESCALER_MASK)
      /* Check whether prescaler for the DAQ List expired */
      if (DaqListPtr->PrescalerCnt == XCP_PRESCALER_MIN)
      {
        /* DAQ List can be sampled */
        CanBeSampled = TRUE;
      }
      else
      {
        /* If ODT is the first ODT associated with the list, it means that the event
        processing started. Prescaler needs to be decremented once for each occurrence of the event */
        if (OdtIndex == 0U)
        {
          /* Decrement the prescaler count */
          DaqListPtr->PrescalerCnt--;
        }
      }
#else /* prescaler is not supported */
        TS_PARAM_UNUSED(OdtIndex);
        /* DAQ List can be sampled */
        CanBeSampled = TRUE;
#endif
    }
  }

  if (CanBeSampled)
  {
    /* DAQ List is ready to be sampled */
    DaqListPtr->Flags |= XCP_MASK_DAQLIST_READ_READY;
  }
  else
  {
    /* DAQ List is not ready to be sampled */
    DaqListPtr->Flags &= (uint8)(~XCP_MASK_DAQLIST_READ_READY);
  }

  DBG_XCP_ISDAQREADYTOBESAMPLED_EXIT(DaqListNumber,OdtIndex);
}

/*-----------------------[Xcp_DeQueueDtoQueue]------------------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_DeQueueDtoQueue
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA) BufferPtr
)
{
  /* Local variable to hold number of bytes occupied by the first DTO in queue */
  const uint16 DtoSizeInQueue
    = Xcp_Uint16FromUint8Queue(
      Xcp_DtoQueue.Queue[Xcp_DtoQueue.Head],
      Xcp_DtoQueue.Queue[(Xcp_DtoQueue.Head + 1U) % XCP_DTO_QUEUE_SIZE]);

  /* Iterator */
  uint16 i;

  DBG_XCP_DEQUEUEDTOQUEUE_ENTRY(BufferPtr);

  /* Update the head of DTO queue */
  Xcp_DtoQueue.Head += 2U;
  Xcp_DtoQueue.Head %= XCP_DTO_QUEUE_SIZE;

  /* Update total number of bytes in DTO queue */
  Xcp_DtoQueue.NoOfBytes -= 2U;
  /* Copy DTO bytes until all bytes are copied or until queue head needs to be rolled over */
  for (i= 0U; i<DtoSizeInQueue; i++)
  {
    /* Copy DTO bytes to user buffer */
    BufferPtr[i] = Xcp_DtoQueue.Queue[Xcp_DtoQueue.Head];
    /* Update total number of bytes in DTO queue */
    Xcp_DtoQueue.NoOfBytes--;
    /* Update the head of DTO queue */
    ++Xcp_DtoQueue.Head;
    Xcp_DtoQueue.Head %= XCP_DTO_QUEUE_SIZE;
  }

  DBG_XCP_DEQUEUEDTOQUEUE_EXIT(BufferPtr);
}

/*------------------[Xcp_UpdateEvent]--------------------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_UpdateEvent
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex
)
{
  /* Consistency of the event */
  const uint8 Consistency
    = Xcp_EventInfo[EventID].Flag & XCP_EVENT_CHANNEL_CONSISTENCY_MASK;

  DBG_XCP_UPDATEEVENT_ENTRY(EventID,EvDaqIndex,OdtIndex);

  switch (Consistency)
  {
    case XCP_EVENT_CHANNEL_CONSISTENCY_DAQ:
      /* For DAQ consistency */
      Xcp_UpdateDaq(EventID, EvDaqIndex);
      break;

    case XCP_EVENT_CHANNEL_CONSISTENCY_ODT:
      /* For ODT consistency */
      Xcp_UpdateOdt(EventID, EvDaqIndex, OdtIndex);
      break;

    default:
      /* Clear event */
      Xcp_Event[EventID].EventIsSet = FALSE;
      break;
  }

  DBG_XCP_UPDATEEVENT_EXIT(EventID,EvDaqIndex,OdtIndex);
}

/*------------------[Xcp_UpdateDaq]-----------------------------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_UpdateDaq
(
  uint16 EventID,
  uint8 EvDaqIndex
)
{
  DBG_XCP_UPDATEDAQ_ENTRY(EventID,EvDaqIndex);

  /* Increment DAQ Index. */
  EvDaqIndex++;

  /* If not all DAQ lists associated to the event are processed */
  if (EvDaqIndex < Xcp_Event[EventID].DaqIdListCount)
  {
    /* Update the current processed event with next DAQ index. */
    Xcp_Event[EventID].NextDaqIndex = EvDaqIndex;

  }
  else
  {
    /* Clear event */
    Xcp_Event[EventID].EventIsSet = FALSE;
  }

  DBG_XCP_UPDATEDAQ_EXIT(EventID,EvDaqIndex);
}

/*------------------[Xcp_UpdateOdt]-----------------------------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_UpdateOdt
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex
)
{
  /* Get the DAQ list based on the DAQ list number found in the current Event */
  CONSTP2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) DaqListPtr =
      XCP_GET_DAQ_LIST_PTR(Xcp_Event[EventID].DaqIdList[EvDaqIndex]);

  DBG_XCP_UPDATEODT_ENTRY(EventID,EvDaqIndex,OdtIndex);

  /* Increment ODT. */
  OdtIndex++;

  /* If not all ODTs in the DAQ list are covered */
  if (OdtIndex < DaqListPtr->MaxOdt)
  {
    /* Initialize the ODT index to the new Odt Index*/
    Xcp_Event[EventID].NextOdtIndex = OdtIndex;

    /* Update the current processed event with next DAQ index. */
    Xcp_Event[EventID].NextDaqIndex = EvDaqIndex;
  }
  else
  {
    /* Initialize the ODT index to zero for the currently processed event */
    Xcp_Event[EventID].NextOdtIndex = 0U;

    /* Update the Daq information of the currently processed event */
    Xcp_UpdateDaq(EventID, EvDaqIndex);
  }

  DBG_XCP_UPDATEODT_EXIT(EventID,EvDaqIndex,OdtIndex);
}

/*------------------[Xcp_SampleData]----------------------------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_SampleData
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex
)
{
  /* Consistency of the event */
  const uint8 Consistency
    = Xcp_EventInfo[EventID].Flag & XCP_EVENT_CHANNEL_CONSISTENCY_MASK;
  Xcp_DTOType Xcp_DTOEntry; /* DTO information */

  DBG_XCP_SAMPLEDATA_ENTRY(EventID,EvDaqIndex,OdtIndex);

  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_DATA_SAMPLING();

  switch (Consistency)
  {
    case XCP_EVENT_CHANNEL_CONSISTENCY_ODT:
      /* For ODT consistency */
      Xcp_SampleHeader(EventID, EvDaqIndex, OdtIndex, &Xcp_DTOEntry);
      Xcp_SampleDtoData(EventID, EvDaqIndex, OdtIndex, &Xcp_DTOEntry);
      break;

    case XCP_EVENT_CHANNEL_CONSISTENCY_DAQ:
      /* For DAQ consistency */
      Xcp_SampleDaq(EventID, EvDaqIndex, &Xcp_DTOEntry);
      break;

    default:
      /* For Event consistency */
      Xcp_SampleEvent(EventID, &Xcp_DTOEntry);
      break;
  }

  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_DATA_SAMPLING();

  DBG_XCP_SAMPLEDATA_EXIT(EventID,EvDaqIndex,OdtIndex);
}

/*------------------[Xcp_SampleHeader]--------------------------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_SampleHeader
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex,
  P2VAR(Xcp_DTOType, AUTOMATIC, XCP_APPL_DATA) DTOEntryPtr
)
{
  /* Initialize the Data count to zero */
  uint16 DataCount = 0U;
  /* Daq ID information */
  Xcp_DaqIdType DaqID;
  /* The DAQ list to be sampled */
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr;

  DBG_XCP_SAMPLEHEADER_ENTRY(EventID,EvDaqIndex,OdtIndex,DTOEntryPtr);

  /* Get the DAQ list number to be sampled */
  DaqID = Xcp_Event[EventID].DaqIdList[EvDaqIndex];

  /* Get the DAQ list based on the DAQ list number */
  DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);

  /* Is Pid ON */
  if ((DaqListPtr->Mode & XCP_MASK_DAQLIST_PID_MODE) == XCP_MASK_DAQLIST_PID_ON_MODE)
  {
    /* Write OdtIndex as the PID of the DTO. */
    DTOEntryPtr->DTO[DataCount] = OdtIndex;
    DataCount++;

    /* for absolute PID: PID = FIRST_PID + relative_ODT_NUMBER */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)

    DTOEntryPtr->DTO[DataCount - 1U] += DaqListPtr->FirstPID;

#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_BYTE_IF_MASK)

    /* Is absolute DAQ list number (BYTE) configured, get the DAQ ID */
    DTOEntryPtr->DTO[DataCount] = (uint8) DaqID;
    DataCount++;

#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_WORD_ALIGNED_IF_MASK)

    /* Is absolute DAQ list number (WORD, aligned) configured, leave a byte
     * for alignment and set it to the Xcp slave global configured value for fill bytes */
    DTOEntryPtr->DTO[DataCount] = XCP_RESERVED_BYTE;
    DataCount++;

#endif

    /* Is absolute DAQ list number (WORD) or (WORD, aligned) configured,
       get the DAQ ID */
#if ((XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_WORD_ALIGNED_IF_MASK) || \
     (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_WORD_IF_MASK))
    Xcp_WriteWordToByteArray(&DTOEntryPtr->DTO[DataCount], DaqID);
    DataCount += XCP_SIZE_OF_WORD;
#endif
  }

#if (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK)

  /* Is time stamp supported and this is the first Odt of the DAQ list */
  if ((((DaqListPtr->Mode & XCP_MASK_DAQLIST_TIMESTAMPED_MODE) ==
                              XCP_MASK_DAQLIST_TIMESTAMPED_MODE)) &&
      (0U == OdtIndex))
  {
    Xcp_TimestampType TimeStamp; /* The Timestamp value */

#if (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF)
    /* OS Timestamp */
    TickType CounterValue; /* The counter value */

    /* Get the counter value */
    (void) GetCounterValue(XCP_OS_COUNTER, &CounterValue);
    TimeStamp = (Xcp_TimestampType)CounterValue;
#else
    /* User Timestamp - Use the user provided function to get timestamps */
    TimeStamp = Xcp_ApplGetTimestamp();
#endif /* (XCP_USER_TIMESTAMP_SUPPORTED == STD_OFF) */

    /* Store the value of the time stamp depending on the configuration */
#if (XCP_TIMESTAMP_TYPE == XCP_ONE_BYTE_TS_MASK)
    DTOEntryPtr->DTO[DataCount] = TimeStamp;
    DataCount++;
#elif (XCP_TIMESTAMP_TYPE == XCP_TWO_BYTE_TS_MASK)
    Xcp_WriteWordToByteArray(&DTOEntryPtr->DTO[DataCount], TimeStamp);
    DataCount += XCP_SIZE_OF_WORD;
#elif (XCP_TIMESTAMP_TYPE == XCP_FOUR_BYTE_TS_MASK)
    Xcp_WriteDwordToByteArray(&DTOEntryPtr->DTO[DataCount], TimeStamp);
    DataCount += XCP_SIZE_OF_DWORD;
#endif
  }

#endif /* (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK) */

  /* Store the Data count */
  DTOEntryPtr->Length = DataCount;

  DBG_XCP_SAMPLEHEADER_EXIT(EventID,EvDaqIndex,OdtIndex,DTOEntryPtr);
}

/*------------------[Xcp_SampleDtoData]-------------------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_SampleDtoData
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex,
  P2VAR(Xcp_DTOType, AUTOMATIC, XCP_APPL_DATA) DTOEntryPtr
)
{
  /* Initialize the Data count */
  uint16 DataCount = DTOEntryPtr->Length;
  /* Initialize the Odt Entry count to zero */
  uint8 OdtEntryCount;
  /* Entry pointer */
  P2VAR(Xcp_OdtEntryType, AUTOMATIC, XCP_VAR) OdtEntryPtr;
  /* The DAQ list to be sampled */
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr;

  DBG_XCP_SAMPLEDTODATA_ENTRY(EventID,EvDaqIndex,OdtIndex,DTOEntryPtr);

  /* Get the DAQ list from the current event */
  DaqListPtr = XCP_GET_DAQ_LIST_PTR(Xcp_Event[EventID].DaqIdList[EvDaqIndex]);

  /* Get the pointer to the first ODT entry in this ODT */
  OdtEntryPtr = DaqListPtr->OdtList[OdtIndex].OdtEntry;
  /* for each ODT entry */
  for (OdtEntryCount = 0U; OdtEntryCount < DaqListPtr->OdtList[OdtIndex].NrOfOdtEntries; OdtEntryCount++)
  {
    /* The length of the entry */
    uint16 Length = (uint16)OdtEntryPtr[OdtEntryCount].Length * (uint16)XCP_GRANULARITY_ODT_ENTRY_SIZE_DAQ;

    if (Length != 0U)
    {

      /* Store the Address information into the DTO Entry */
      TS_MemCpy(
        &DTOEntryPtr->DTO[DataCount],
        XCP_APPL_GET_ADDRESS( 0U, OdtEntryPtr[OdtEntryCount].Address ),
        Length);


      DataCount += Length;        /* Increment the data count by length */
    }
    else
    {
      /* We need to stop as soon as we find an ODT entry with size 0 - ASAM requirement */
      break;
    }
  }
  /* If sampling of all ODTs in the DAQ list finished */
  if ((DaqListPtr->MaxOdt - 1U) == OdtIndex)
  {
    /* Reset the prescaler value */
    DaqListPtr->PrescalerCnt = DaqListPtr->Prescaler;
  }

  /* Store the Data count */
  DTOEntryPtr->Length = DataCount;

  /* Protect concurrent access to the DTO Queue */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  /* Put the sampled DTO in the outgoing queue */
  Xcp_AddDtoInQueue(*DTOEntryPtr);

  /* Exit the internals critical section*/
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_SAMPLEDTODATA_EXIT(EventID,EvDaqIndex,OdtIndex,DTOEntryPtr);
}

/*------------------[Xcp_AddDtoInQueue]-------------------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_AddDtoInQueue
(
  Xcp_DTOType DTOEntry
)
{
  /* iterator */
  uint16 i;

  /* Get the tail of the DTO queue */
  uint16 DtoTail = Xcp_DtoQueue.Tail;

  DBG_XCP_ADDDTOINQUEUE_ENTRY(DTOEntry);

  /* Store length information to be prefixed with the DTO data. */
#if (CPU_BYTE_ORDER == LOW_BYTE_FIRST)
  /* LSB */
  Xcp_DtoQueue.Queue[DtoTail] = (uint8)(DTOEntry.Length & XCP_FIRST_BYTE_MASK);
#else
  /* MSB */
  Xcp_DtoQueue.Queue[DtoTail]
    = (uint8)((DTOEntry.Length & XCP_SECOND_BYTE_MASK) >> XCP_NUMBER_OF_BITS_IN_BYTE);
#endif

  /* Increment the tail */
  ++DtoTail;
  DtoTail %= XCP_DTO_QUEUE_SIZE;

#if (CPU_BYTE_ORDER == LOW_BYTE_FIRST)
  /* MSB */
  Xcp_DtoQueue.Queue[DtoTail]
    = (uint8)(( DTOEntry.Length & XCP_SECOND_BYTE_MASK) >> XCP_NUMBER_OF_BITS_IN_BYTE);
#else
  /* LSB */
  Xcp_DtoQueue.Queue[DtoTail] = (uint8)(DTOEntry.Length & XCP_FIRST_BYTE_MASK);
#endif

  /* Increment the tail */
  ++DtoTail;
  DtoTail %= XCP_DTO_QUEUE_SIZE;

  /* Store the DTO information */
  for (i = 0U; i < DTOEntry.Length; i++)
  {
    Xcp_DtoQueue.Queue[DtoTail] = DTOEntry.DTO[i];

    /* Increment the tail */
    ++DtoTail;
    DtoTail %= XCP_DTO_QUEUE_SIZE;
  }

  /* update the tail of the queue */
  Xcp_DtoQueue.Tail = DtoTail;

  /*
  ** Update the number of bytes in the queue including
  ** length information prefixed with the DTO data.
  */
  Xcp_DtoQueue.NoOfBytes += (DTOEntry.Length + XCP_SIZE_OF_WORD);

  DBG_XCP_ADDDTOINQUEUE_EXIT(DTOEntry);
}

/*------------------[Xcp_SampleDaq]-----------------------------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_SampleDaq
(
  uint16 EventID,
  uint8 EvDaqIndex,
  P2VAR(Xcp_DTOType, AUTOMATIC, XCP_APPL_DATA) DTOEntryPtr
)
{
  /* Variable to loop through each ODT */
  uint8 OdtIndex;
  /* The DAQ list to be sampled */
  P2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) DaqListPtr;

  DBG_XCP_SAMPLEDAQ_ENTRY(EventID,EvDaqIndex,DTOEntryPtr);

  /* Get the DAQ list copy */
  DaqListPtr = XCP_GET_DAQ_LIST_PTR(Xcp_Event[EventID].DaqIdList[EvDaqIndex]);

  /* Are all the ODTs covered */
  for (OdtIndex = 0U; OdtIndex < DaqListPtr->MaxOdt; OdtIndex++)
  {
    /* Sample the header and Data */
    Xcp_SampleHeader(EventID, EvDaqIndex, OdtIndex, DTOEntryPtr);
    Xcp_SampleDtoData(EventID, EvDaqIndex, OdtIndex, DTOEntryPtr);
  }

  DBG_XCP_SAMPLEDAQ_EXIT(EventID,EvDaqIndex,DTOEntryPtr);
}

/*------------------[Xcp_SampleEvent]---------------------------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_SampleEvent
(
  uint16 EventID,
  P2VAR(Xcp_DTOType, AUTOMATIC, XCP_APPL_DATA) DTOEntryPtr
)
{
  uint8 DaqIdListCount; /* The number of DAQ IDs of this event */
  uint8 EvDaqIndex; /* Variable to loop through each Daq */

  DBG_XCP_SAMPLEEVENT_ENTRY(EventID,DTOEntryPtr);

  /* Get the number of DAQ lists associated to this event */
  DaqIdListCount = Xcp_Event[EventID].DaqIdListCount;

  /* For all DAQ lists associated with this event, sample them */
  for (EvDaqIndex = 0U; EvDaqIndex < DaqIdListCount; EvDaqIndex++)
  {
    /* DAQ list associated to this event */
    P2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) DaqListPtr;

    /* Get a DAQ list copy */
    DaqListPtr = XCP_GET_DAQ_LIST_PTR(Xcp_Event[EventID].DaqIdList[EvDaqIndex]);

    /* If DAQ List can be sampled */
    if ((DaqListPtr->Flags & XCP_MASK_DAQLIST_READ_READY) == XCP_MASK_DAQLIST_READ_READY)
    {
      /* Sample all the ODTs in the DAQ list */
      Xcp_SampleDaq(EventID, EvDaqIndex, DTOEntryPtr);
    }
  }

  DBG_XCP_SAMPLEEVENT_EXIT(EventID,DTOEntryPtr);
}

/*------------------[Xcp_GetNextOdtToProcess]----------------------------------------*/
STATIC FUNC(Std_ReturnType, XCP_CODE)  Xcp_GetNextOdtToProcess
(
  uint16 EventID,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) DaqListIndex,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) OdtIndex,
  P2VAR(uint16, AUTOMATIC, AUTOMATIC) MemorySize
)
{
  uint8 EvDaqIndex = Xcp_Event[EventID].NextDaqIndex;
  P2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) DaqListPtr;
  uint8 EvOdtIndex = Xcp_Event[EventID].NextOdtIndex;
  Std_ReturnType DataStatus = E_NOT_OK;
  uint16 CalculatedMemory = 0U;

  DBG_XCP_GETNEXTODTTOPROCESS_ENTRY(EventID,DaqListIndex,OdtIndex,MemorySize);

  if ((Xcp_EventInfo[EventID].Flag & XCP_EVENT_CHANNEL_CONSISTENCY_MASK) ==
        XCP_EVENT_CHANNEL_CONSISTENCY_EVENT)
  {
    /* Calculate the memory required for the event */
    CalculatedMemory = Xcp_GetEventSize(EventID);

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
    DataStatus = E_OK;
    /* Loop through all the DAQ lists of the event */
    do
    {
      DaqListPtr = XCP_GET_DAQ_LIST_PTR(Xcp_Event[EventID].DaqIdList[EvDaqIndex]);

      /* Check whether DAQ List is active */
      if (((DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION) == XCP_MASK_DAQLIST_DIRECTION_STIM) &&
          ((DaqListPtr->Mode & XCP_MASK_DAQLIST_RUNNING) == XCP_MASK_DAQLIST_RUNNING)
         )
      {
        /* Check whether all ODTs for the DAQ List are in the buffer */
        DataStatus = Xcp_SearchDaqInStimBuffer(Xcp_Event[EventID].DaqIdList[EvDaqIndex]);
      }

      EvDaqIndex++;

    }while ((DataStatus == E_OK) && (EvDaqIndex < Xcp_Event[EventID].DaqIdListCount));

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
    if (DataStatus != E_OK)
    {
      /* Emit an Event CTO with Event channel number as info */
      Xcp_EmitEventSTIMTimeout(XCP_EV_STIM_TIMEOUT_INFO_EVENT, EventID);
    }
#endif /* XCP_EVENT_PACKET_ENABLED == STD_ON */
#endif /* XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK */
  }
  else
  {
    /* Is the Daq index present in the Event Id */
    while ((EvDaqIndex < Xcp_Event[EventID].DaqIdListCount) &&
           ((CalculatedMemory == 0U) && (E_OK != DataStatus))
          )
    {
      DaqListPtr = XCP_GET_DAQ_LIST_PTR(Xcp_Event[EventID].DaqIdList[EvDaqIndex]);

      if ((DaqListPtr->Mode & XCP_MASK_DAQLIST_DIRECTION) == XCP_MASK_DAQLIST_DIRECTION_DAQ)
      {
#if (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK)
        CalculatedMemory = Xcp_GetNextDaqOdt(EventID, &EvDaqIndex, &EvOdtIndex);
#endif
      }
      else /* STIM */
      {
#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
        DataStatus = Xcp_GetNextStimOdt(EventID, &EvDaqIndex, &EvOdtIndex);
#endif
      }
    }
  }

  /* Return the index of the DAQ list and ODT to be processed */
  *DaqListIndex = EvDaqIndex;
  *OdtIndex = EvOdtIndex;
  /* Return the memory required */
  *MemorySize = CalculatedMemory;

  DBG_XCP_GETNEXTODTTOPROCESS_EXIT(DataStatus,EventID,DaqListIndex,OdtIndex,MemorySize);

  /* Return total memory needed for processing the DAQ list and ODT*/
  return DataStatus;
}


/*------------------[Xcp_GetNextDaqOdt]----------------------------------------*/
STATIC FUNC(uint16, XCP_CODE)  Xcp_GetNextDaqOdt
(
  uint16 EventID,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) DaqListIndex,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) OdtIndex
)
{
  uint8 EvDaqIndex = *DaqListIndex;
  uint8 EvOdtIndex = *OdtIndex;
  uint16 MemorySize = 0U; /* Holds the calculated total memory */
  Xcp_DaqIdType DaqID = Xcp_Event[EventID].DaqIdList[EvDaqIndex];
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);

  DBG_XCP_GETNEXTDAQODT_ENTRY(EventID,DaqListIndex,OdtIndex);

  /* ODT Consistency */
  if ((Xcp_EventInfo[EventID].Flag & XCP_EVENT_CHANNEL_CONSISTENCY_MASK) ==
        XCP_EVENT_CHANNEL_CONSISTENCY_ODT)
  {
    /* Identify whether DAQ is ready to be sampled */
    Xcp_IsDAQReadyToBeSampled(DaqID, EvOdtIndex);
    /* If DAQ list can be sampled */
    if ((DaqListPtr->Flags & XCP_MASK_DAQLIST_READ_READY) == XCP_MASK_DAQLIST_READ_READY)
    {
      /* Get memory size required for next readable ODT.
         Stop when a sizeable(here size > 0) ODT is encountered */
      while((MemorySize == 0U) && (EvOdtIndex < DaqListPtr->MaxOdt))
      {
        /* Calculate the memory required for the ODT */
        MemorySize = Xcp_GetOdtSize(DaqID, EvOdtIndex);

        /* Check whether the ODT was cleared (All entries are of size 0(zero) */
        if(MemorySize == 0U)
        {
          /* Iterate to next ODT as the current ODT was cleared */
          EvOdtIndex++;
        }
      }
      /* If the DAQ list is cleared */
      if(MemorySize == 0U)
      {
        /* Reset the prescaler value so that once the DAQ list is populated
        events are prescaled properly */
        DaqListPtr->PrescalerCnt = DaqListPtr->Prescaler;
      }
    }
    if(0U == MemorySize)
    {
      /* Get the next DAQ associated with the event */
      EvDaqIndex++;
      EvOdtIndex = 0U;
    }
  }
  /* DAQ Consistency */
  else
  {
    /* Identify whether DAQ is ready to be sampled */
    Xcp_IsDAQReadyToBeSampled(DaqID, EvOdtIndex);
    /* If DAQ list can be sampled */
    if ((DaqListPtr->Flags & XCP_MASK_DAQLIST_READ_READY) == XCP_MASK_DAQLIST_READ_READY)
    {
      /* Calculate the memory required for the DAQ list */
      MemorySize = Xcp_GetDaqSize(DaqID);
      /* If the DAQ list is cleared */
      if(0U == MemorySize)
      {
        /* Reset the prescaler value so that once the DAQ list is populated
        events are prescaled properly */
        DaqListPtr->PrescalerCnt = DaqListPtr->Prescaler;
      }
    }
    if(0U == MemorySize)
    {
      /* Get the next DAQ associated with the event */
      EvDaqIndex++;
      EvOdtIndex = 0U;
    }
  }

  /* Return the index of the DAQ list and ODT to be processed */
  *DaqListIndex = EvDaqIndex;
  *OdtIndex = EvOdtIndex;

  DBG_XCP_GETNEXTDAQODT_EXIT(MemorySize,EventID,DaqListIndex,OdtIndex);

  /* Return total memory needed for processing the DAQ list and ODT*/
  return MemorySize;
}

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)

/*------------------[Xcp_GetNextStimOdt]----------------------------------------*/
STATIC FUNC(Std_ReturnType, XCP_CODE)  Xcp_GetNextStimOdt
(
  uint16 EventID,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) DaqListIndex,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) OdtIndex
)
{
  /* The DAQ list to be sampled */
  P2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) DaqListPtr;
  uint8 EvDaqIndex = *DaqListIndex;
  uint8 EvOdtIndex = *OdtIndex;
  Xcp_DaqIdType DaqID = Xcp_Event[EventID].DaqIdList[EvDaqIndex];
  Std_ReturnType StimStatus = E_NOT_OK;

  DBG_XCP_GETNEXTSTIMODT_ENTRY(EventID,DaqListIndex,OdtIndex);

  /* Get the DAQ list based on the DAQ list number */
  DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);

  if ((Xcp_EventInfo[EventID].Flag & XCP_EVENT_CHANNEL_CONSISTENCY_MASK) ==
        XCP_EVENT_CHANNEL_CONSISTENCY_ODT)
  {
    /* Check whether DAQ List is active */
    if ((DaqListPtr->Mode & XCP_MASK_DAQLIST_RUNNING) == XCP_MASK_DAQLIST_RUNNING)
    {
      /* DAQ list can be stimulated only if the first ODT entry of the first ODT has length > 0 (ASAM
       * requirement) */
      if ((DaqListPtr->OdtList == NULL_PTR) ||
            ((DaqListPtr->OdtList->OdtEntry == NULL_PTR) ||
             (DaqListPtr->OdtList->OdtEntry->Length == 0U)
            )
         )
      {
        StimStatus = E_NOT_OK;
      }
      else
      {
        /* Initialize the flag for every DAQ list */
        if(EvOdtIndex == 0)
        {
          Xcp_DaqEventCTO = FALSE;
        }
        while ((StimStatus != E_OK) && (EvOdtIndex < DaqListPtr->MaxOdt))
        {
          uint16 BufIndex;       /* Position of an ODT in STIM buffer */

          /* Search the ODT in the STIM buffer */
          StimStatus = Xcp_SearchOdtInStimBuffer(DaqID, EvOdtIndex, &BufIndex);
          /* Check if the ODT corresponds to a DAQ list which is already queued in Event CTO */
          if ((E_OK != StimStatus) && (Xcp_DaqEventCTO == FALSE))
          {
            Xcp_DaqEventCTO = TRUE;
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
            /* Emit an Event CTO with DAQList number as info */
            Xcp_EmitEventSTIMTimeout(XCP_EV_STIM_TIMEOUT_INFO_DAQLIST, DaqID);
#endif
          }
          /* Ignore the value of Buffer Index */
          TS_PARAM_UNUSED(BufIndex);

          if (E_OK != StimStatus)
          {
            /* Iterate to next ODT */
            EvOdtIndex++;
          }
        }
      }
    }

    if (E_OK != StimStatus)
    {
      /* Get the next DAQ associated with the event */
      EvDaqIndex++;
      EvOdtIndex = 0U;
    }
  }
  /* DAQ Consistency */
  else
  {
    /* Check whether DAQ List is active */
    if ((DaqListPtr->Mode & XCP_MASK_DAQLIST_RUNNING) == XCP_MASK_DAQLIST_RUNNING)
    {
      StimStatus = Xcp_SearchDaqInStimBuffer(DaqID);
    }

    if (E_OK != StimStatus)
    {
      /* Get the next DAQ associated with the event */
      EvDaqIndex++;
      EvOdtIndex = 0U;
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
      /* Emit event XCP_EV_STIM_TIMEOUT with DAQ ID as info type*/
      Xcp_EmitEventSTIMTimeout(XCP_EV_STIM_TIMEOUT_INFO_DAQLIST, DaqID);
#endif
    }
  }

  /* Return the index of the DAQ list, ODT to be processed */
  *DaqListIndex = EvDaqIndex;
  *OdtIndex = EvOdtIndex;

  DBG_XCP_GETNEXTSTIMODT_EXIT(StimStatus,EventID,DaqListIndex,OdtIndex);

  /* Return the status of the STIM data */
  return StimStatus;
}

#endif

/*------------------[Xcp_GetOdtSize]----------------------------------------*/

STATIC FUNC(uint16, XCP_CODE)  Xcp_GetOdtSize(Xcp_DaqIdType DaqListId, uint8 OdtIndex)
{
  /* Holds the calculated total memory */
  uint16 TotalMemory = 0U;

  DBG_XCP_GETODTSIZE_ENTRY(DaqListId,OdtIndex);

  /* Calculate the size of Data for the DTO of the DAQ list */
  TotalMemory = Xcp_GetDtoDataSize(DaqListId, OdtIndex);

  /*
  ** DTO header size needs to be calculated only if any data is configured in
  ** the ODT
  */
  if (TotalMemory > 0U)
  {
    /* Update the total memory by adding the size of header */
    TotalMemory += Xcp_GetDtoHeaderSize(DaqListId, OdtIndex);

    /* Length information to be prefixed with the DTO data. */
    TotalMemory += XCP_SIZE_OF_WORD;
  }

  DBG_XCP_GETODTSIZE_EXIT(TotalMemory,DaqListId,OdtIndex);
  return TotalMemory;
}

/*------------------[Xcp_GetDtoDataSize]------------------------------------*/

STATIC FUNC(uint16, XCP_CODE)  Xcp_GetDtoDataSize(Xcp_DaqIdType DaqListId, uint8 OdtIndex)
{
  /* Holds the calculated total memory */
  uint16 TotalMemory = 0U;
  /* Holds the index of ODT entry */
  uint8 OdtEntryCount = 0U;
  /* The DAQ List corresponding to the DAQ list ID */
  CONSTP2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListId);

  DBG_XCP_GETDTODATASIZE_ENTRY(DaqListId,OdtIndex);

  /* Prevent access if the ODTs were not assigned in the DAQ list (not just allocated, but an initial
   * ALLOC_ODT_ENTRY is needed for all allocated ODTs to be arranged in the dynamic area, if the DAQ
   * list is dynamically allocated) */
  if (&DaqListPtr->OdtList[OdtIndex] != NULL_PTR)
  {
    /* Points to the first ODT entry of the DAQ list */
    P2VAR(Xcp_OdtEntryType, AUTOMATIC, XCP_VAR)  OdtEntryPtr = DaqListPtr->OdtList[OdtIndex].OdtEntry;
    /* Prevent access if the ODT does not have the address of its first ODT entry (i.e. there was no
     * SET_DAQ_PTR called at all) */
    if (OdtEntryPtr != NULL_PTR)
    {
      /* loop through all the ODT entries of the ODT and calculate the total size for the data of ODT */
      for (OdtEntryCount = 0U; OdtEntryCount < DaqListPtr->OdtList[OdtIndex].NrOfOdtEntries; OdtEntryCount++)
      {
        if (OdtEntryPtr[OdtEntryCount].Length != 0U)
        {
          TotalMemory += (uint16)OdtEntryPtr[OdtEntryCount].Length * XCP_GRANULARITY_ODT_ENTRY_SIZE_DAQ;
        }
        else
        {
          /* We need to stop as soon as we find an ODT entry with size 0 - ASAM requirement */
          break;
        }
      }
    }
  }

  DBG_XCP_GETDTODATASIZE_EXIT(TotalMemory,DaqListId,OdtIndex);
  return TotalMemory;
}

/*------------------[Xcp_GetDaqSize]----------------------------------------*/

STATIC FUNC(uint16, XCP_CODE)  Xcp_GetDaqSize(Xcp_DaqIdType DaqListId)
{
  /* Holds the calculated total memory */
  uint16 TotalMemory = 0U;
  /* Holds the index of ODT */
  uint8 ODTIndex = 0U;
  /* The DAQ list corresponding to the received DAQ list number */
  CONSTP2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListId);

  DBG_XCP_GETDAQSIZE_ENTRY(DaqListId);

  /* loop through all the ODTs of the DAQ list and calculate the total size for the DAQ list */
  while (ODTIndex < DaqListPtr->MaxOdt)
  {
    TotalMemory += Xcp_GetOdtSize(DaqListId, ODTIndex);
    ODTIndex++;
  }

  DBG_XCP_GETDAQSIZE_EXIT(TotalMemory,DaqListId);
  return TotalMemory;
}

/*------------------[Xcp_GetEventSize]----------------------------------------*/

STATIC FUNC(uint16, XCP_CODE)  Xcp_GetEventSize(uint16 EventId)
{
  /* Holds the calculated total memory */
  uint16 TotalMemory = 0U;
  /* Holds the index of Daq */
  uint8 EvDaqIndex = 0U;
  /* Number of DAQ lists configured for the event */
  uint8 EvDaqListCount = Xcp_Event[EventId].DaqIdListCount;
  /* Points to the DAQ list corresponding to the event */
  P2VAR(Xcp_DaqIdType, AUTOMATIC, XCP_VAR) EvDaqListPtr = Xcp_Event[EventId].DaqIdList;

  DBG_XCP_GETEVENTSIZE_ENTRY(EventId);

  /* Loop through all the DAQ lists of the event to calculate the total size */
  while(EvDaqIndex < EvDaqListCount)
  {
    /* Local variable to hold DAQ List number */
    Xcp_DaqIdType DaqID = EvDaqListPtr[EvDaqIndex];
    /* The DAQ list */
    P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqID);
    /* Identify whether DAQ is ready to be sampled.
       For event consistency the whole DAQ will be considered and
       thus the parameter OdtIndex is set to 0(zero)*/
    Xcp_IsDAQReadyToBeSampled(DaqID, 0U);
    if((DaqListPtr->Flags & XCP_MASK_DAQLIST_READ_READY) == XCP_MASK_DAQLIST_READ_READY)
    {
      /* Local variable to read DAQ size */
      uint16 DaqDatSize;

      /* Get DAQ size */
      DaqDatSize = Xcp_GetDaqSize(DaqID);
      /* If DAQ List is cleared */
      if(DaqDatSize == 0U)
      {
        /* Reset the prescaler value so that once the DAQ list is populated
        events are prescaled properly */
        DaqListPtr->PrescalerCnt = DaqListPtr->Prescaler;
      }
      /* Add up the size memory size required for the DAQ List*/
      TotalMemory += DaqDatSize;
    }
    EvDaqIndex++;
  }

  DBG_XCP_GETEVENTSIZE_EXIT(TotalMemory,EventId);
  return TotalMemory;
}


/*------------------[Xcp_GetDtoHeaderSize]----------------------------------------*/

STATIC FUNC(uint16, XCP_CODE)  Xcp_GetDtoHeaderSize(Xcp_DaqIdType DaqListId, uint8 OdtIndex)
{
  /* Holds the calculated total memory */
  uint16 TotalMemory = 0;
  /* The DAQ list to be sampled */
  CONSTP2CONST(Xcp_DaqType, AUTOMATIC, AUTOMATIC) DaqListPtr = XCP_GET_DAQ_LIST_PTR(DaqListId);

  DBG_XCP_GETDTOHEADERSIZE_ENTRY(DaqListId,OdtIndex);

  /* Is Pid enabled */
  if ((DaqListPtr->Mode & XCP_MASK_DAQLIST_PID_MODE) == XCP_MASK_DAQLIST_PID_ON_MODE)
  {
    /* Is absolute PID configured: PID = FIRST_PID + relative_ODT_NUMBER */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)

    /* Increment the memory size by 1, since identifier field consists of 1 byte PID */
    TotalMemory++;

    /* Is absolute DAQ list number (BYTE) configured */
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_BYTE_IF_MASK)

    /* Increment the memory size by 2, since identifier field consists of 1 byte PID and
       1 byte DAQ List index */
    TotalMemory += XCP_SIZE_OF_WORD;

    /* Is absolute DAQ list number (WORD) configured */
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_WORD_IF_MASK)

    /* Increment the memory size by 3, since identifier field consists of 1 byte PID and
       2 byte DAQ List index */
    TotalMemory += XCP_SIZE_OF_PID_RELATIVE_WORD;

    /* Is absolute DAQ list number (WORD, aligned) configured */
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_WORD_ALIGNED_IF_MASK)

    /* Increment the memory size by 4, since identifier field consists of 1 byte PID,
       1 byte alignment and 2 byte DAQ List index */
    TotalMemory += XCP_SIZE_OF_DWORD;

#endif
  }

  /* Is time stamp enabled  */
#if (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK)

  /* If time stamp is enabled for the DAQ list and
     the first ODT of the DAQ list is being processed */
  if ((((DaqListPtr->Mode & XCP_MASK_DAQLIST_TIMESTAMPED_MODE) == XCP_MASK_DAQLIST_TIMESTAMPED_MODE)) &&
      (OdtIndex == 0U))
  {
    /* Update the required total memory depending up on configuration */

    /* if one byte time stamp is configured */
#if (XCP_TIMESTAMP_TYPE == XCP_ONE_BYTE_TS_MASK)

    /* Increment the required total memory by one */
    TotalMemory++;

/* if two byte time stamp is configured */
#elif (XCP_TIMESTAMP_TYPE == XCP_TWO_BYTE_TS_MASK)

    /* Increment the required total memory by two */
    TotalMemory += XCP_SIZE_OF_WORD;

/* if four byte time stamp is configured */
#elif (XCP_TIMESTAMP_TYPE == XCP_FOUR_BYTE_TS_MASK)

    /* Increment the required total memory by four */
    TotalMemory += XCP_SIZE_OF_DWORD;

#endif
  }
#else

  TS_PARAM_UNUSED(OdtIndex);

#endif /* #if (XCP_TIMESTAMP_SUPPORTED == XCP_TIMESTAMP_MASK) */

  DBG_XCP_GETDTOHEADERSIZE_EXIT(TotalMemory,DaqListId,OdtIndex);
  return TotalMemory;
}

#if ((XCP_EVENT_PACKET_ENABLED == STD_ON) && (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))
/*--------------------------[Xcp_EmitEventSTIMTimeout]-----------------------------*/

STATIC FUNC(void, XCP_CODE)  Xcp_EmitEventSTIMTimeout
(
  uint8 EventInfoType,
  uint16 EventInfo
)
{
  DBG_XCP_EMITEVENTSTIMTIMEOUT_ENTRY(EventInfoType,EventInfo);

  /* Protect concurrent access to Event CTO Queue */
  SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  if(Xcp_EventCTOIndex < XCP_EV_CTO_QUEUE_SIZE)
  {
    /* Emit event XCP_EV_STIM_TIMEOUT with Event number as info type*/
    Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[0U] = XCP_EV_PID;
    Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[1U] = XCP_EV_STIM_TIMEOUT;
    Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[2U] = EventInfoType;/* Info type: Event */
    Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[3U] = XCP_RESERVED_BYTE;
    Xcp_WriteWordToByteArray(&Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[4U], EventInfo);
    Xcp_EventCTOQueue[Xcp_EventCTOIndex].Length = 0x06U;
    Xcp_EventCTOIndex++;
  }
  SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

  DBG_XCP_EMITEVENTSTIMTIMEOUT_EXIT(EventInfoType,EventInfo);
}
#endif /* (XCP_EVENT_PACKET_ENABLED == STD_ON) && (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) */

/*--------------------------[Xcp_Uint16FromUint8Queue]-----------------------------*/

STATIC FUNC(uint16, XCP_CODE) Xcp_Uint16FromUint8Queue
(
  const uint8 b1,
  const uint8 b2
)
{
  uint16 assembledWord;

  DBG_XCP_UINT16FROMUINT8QUEUE_ENTRY(b1,b2);

#if (CPU_BYTE_ORDER == LOW_BYTE_FIRST)
  assembledWord = (uint16)((uint16)((uint16)b2 << 8U) | (uint16)b1);
#else
  assembledWord = (uint16)((uint16)((uint16)b1 << 8U) | (uint16)b2);
#endif

  DBG_XCP_UINT16FROMUINT8QUEUE_EXIT(assembledWord,b1,b2);
  return assembledWord;
}

/*--------------------------[Xcp_GetCurrentEventToProcess]-----------------------------*/

STATIC FUNC(uint16, XCP_CODE) Xcp_GetCurrentEventToProcess(void)
{
  /* Local value holding the maximum event priority */
  sint16 MaxEventPriority = -1;
  /* iterator */
  uint16 i;
  /* the event that needs processing in this cycle */
  uint16 CurrentEventToProcess = XCP_INVALID_EVENT_CHANNEL;

  DBG_XCP_GETCURRENTEVENTTOPROCESS_ENTRY();

  for (i=0U; i< XCP_MAX_EVENT_CHANNEL; i++)
  {
    /* If event was set */
    if (Xcp_Event[i].EventIsSet == TRUE)
    {
      /* If the event has the highest priority up to now */
      if ((sint16)Xcp_EventInfo[i].Priority > MaxEventPriority)
      {
        /* We have a new maximum priority found */
        MaxEventPriority = (sint16)Xcp_EventInfo[i].Priority;
        /* Set the new event as it has the highest priority */
        CurrentEventToProcess = i;
      }
    }
  }

  DBG_XCP_GETCURRENTEVENTTOPROCESS_EXIT(CurrentEventToProcess);
  return CurrentEventToProcess;
}

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

#endif /* #if (XCP_MAX_DAQ != 0U) */

/*==================[end of file]===========================================*/
