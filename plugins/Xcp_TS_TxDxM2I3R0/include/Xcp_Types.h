#if (!defined XCP_TYPES_H)
#define XCP_TYPES_H

/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]============================================*/

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/** \brief Type for the configuration data (placeholder because of pre-compile
 **        time configuration support).
 */
typedef struct
{
  uint32 Dummy;   /**< Dummy variable to have valid C code */
} Xcp_ConfigType;



/** \brief Xcp return type for EB specific API functions */
typedef enum
{
  XCP_OK,               /**< Function was successful */
  XCP_NOT_OK,           /**< Development error happened */
  XCP_NOT_INITIALIZED,  /**< Xcp module is not initialized */
  XCP_NOT_CONNECTED,    /**< Xcp slave is not connected to the master. */
  XCP_OVERLOAD,         /**< Event is already pending (OVERLOAD situation) */
  XCP_NO_ACTIVE_LIST    /**< No active list associated (Xcp_SetEvent()) */
} Xcp_ReturnType;

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/

#endif /* if !defined( XCP_TYPES_H ) */
