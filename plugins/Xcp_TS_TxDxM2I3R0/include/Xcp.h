/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined XCP_H)
#define XCP_H

/*==================[includes]===============================================*/

#include <ComStack_Types.h>   /* AUTOSAR Com Stack header */

#include <Xcp_Version.h>      /* This modules' version declarations */
#include <Xcp_Types.h>        /* Xcp types to be published */
#include <Xcp_Cfg.h>          /* Generated module configuration */

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
  #include <Nvm.h>            /* NvM API */
#endif

/*==================[macros]=================================================*/

#if (defined XCP_INSTANCE_ID)
#error XCP_INSTANCE_ID already defined
#endif
/** \brief Id of instance of Xcp */
#define XCP_INSTANCE_ID                       0U

/*------------------[error codes]--------------------------------------------*/

#if (defined XCP_E_INV_POINTER)
#error XCP_E_INV_POINTER already defined
#endif
/** \brief Error Code.
 **
 ** API is invoked with invalid pointer. */
#define XCP_E_INV_POINTER                       0x01U

#if (defined XCP_E_NOT_INITIALIZED)
#error XCP_E_NOT_INITIALIZED already defined
#endif
/** \brief Error Code.
 **
 ** API is called before complete initialization. */
#define XCP_E_NOT_INITIALIZED                   0x02U

#if (defined XCP_E_INVALID_PDUID)
#error XCP_E_INVALID_PDUID already defined
#endif
/** \brief Error Code.
 **
 ** API called with wrong PDU ID. */
#define XCP_E_INVALID_PDUID                     0x03U

#if (defined XCP_E_NULL_POINTER)
#error XCP_E_NULL_POINTER already defined
#endif
/** \brief Error Code.
 **
 ** Null pointer has been passed as an argument. */
#define XCP_E_NULL_POINTER                      0x12U

#if (defined XCP_E_INVALID_NVM_BLOCK_LENGTH)
#error XCP_E_INVALID_NVM_BLOCK_LENGTH already defined
#endif
/** \brief Error Code thrown when the NvM block needed to store the DAQ lists has an invalid length
 **
 ** The storage of DAQ lists in NV memory requires a NvM block to be configured correctly. This error
 ** describes the situation when the block required by XCP to store the DAQ list configuration has
 ** an invalid length. The length of the NvM block must match the one of the runtime structure of
 ** the DAQ lists. In order to determine the correct length, the easiest way is to check with the
 ** debugger the size of the runtime DAQ lists information (hint: breakpoint where this error is
 ** thrown).
 **
 ** */
#define XCP_E_INVALID_NVM_BLOCK_LENGTH          0x20U

#if (defined XCP_E_INVALID_EVENT)
#error XCP_E_INVALID_EVENT already defined
#endif
/** \brief Error Code.
 **
 ** The error is triggered by Xcp_SetEvent() if
 ** - The given event ID (parameter) is invalid, i.e. there is no event with that ID or
 ** - The given event ID belongs to a periodic event, for which this function shall not be called. */
#define XCP_E_INVALID_EVENT                     0x40U

/*------------------[macros for service IDs]---------------------------------*/

#if (defined XCP_SID_INIT)
#error XCP_SID_INIT already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for Xcp_Init(). */
#define XCP_SID_INIT                            0x00U

#if (defined XCP_SID_GET_VERSION_INFO)
#error XCP_SID_GET_VERSION_INFO already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for Xcp_GetVersionInfo(). */
#define XCP_SID_GET_VERSION_INFO                0x01U

#if (defined XCP_SID_IF_TX_CONFIRMATION)
#error XCP_SID_IF_TX_CONFIRMATION already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of the general service ID for Xcp_<If>TxConfirmation() functions. */
#define XCP_SID_IF_TX_CONFIRMATION              0x02U

#if (defined XCP_SID_IF_RX_INDICATION)
#error XCP_SID_IF_RX_INDICATION already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of the general service ID for Xcp_<If>RxIndication() functions. */
#define XCP_SID_IF_RX_INDICATION                0x03U

#if (defined XCP_SID_MAIN_FUNCTION)
#error XCP_SID_MAIN_FUNCTION already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for Xcp_MainFunction(). */
#define XCP_SID_MAIN_FUNCTION                   0x04U

#if (XCP_ON_FLEXRAY_ENABLED == STD_ON)

#if (defined XCP_SID_FRIF_TRIGGER_TRANSMIT)
#error XCP_SID_FRIF_TRIGGER_TRANSMIT already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for Xcp_FrIfTriggerTransmit(). */
#define XCP_SID_FRIF_TRIGGER_TRANSMIT           0x05U

#endif /* XCP_ON_FLEXRAY_ENABLED == STD_ON */


#if (defined XCP_SID_SET_EVENT)
#error XCP_SID_SET_EVENT already defined
#endif
/** \brief XCP API service ID.
 **
 ** Definition of service ID for Xcp_SetEvent() API. */
#define XCP_SID_SET_EVENT                      0x80U

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_OFF)

#if (defined XCP_SID_SOAD_SO_CON_MODE_CHG)
#error XCP_SID_SOAD_SO_CON_MODE_CHG already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for Xcp_SoAdSoConModeChg(). */
#define XCP_SID_SOAD_SO_CON_MODE_CHG            0x81U

#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_OFF */
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>

#if (XCP_VERSION_INFO_API == STD_ON)

/** \brief Return the modules version information
 **
 ** This function provides the information to module vendor ID, module ID and
 ** software version major.minor.patch
 **
 ** Precondition: ::XCP_VERSION_INFO_API = ::STD_ON
 **
 ** \param[out]  VersionInfoPtr  Pointer to where to store the version
 **                              information of this module.
 **
 ** \ServiceID{::XCP_SID_GET_VERSION_INFO}
 ** \Reentrancy{Non-Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, XCP_CODE) Xcp_GetVersionInfo
(
  P2VAR(Std_VersionInfoType, AUTOMATIC, XCP_APPL_DATA) VersionInfoPtr
);

#endif

/*---------------[Interface ECU State Manager <--> Xcp]---------------------*/

/** \brief Initializes the XCP.
 **
 ** This function initializes interfaces and variables of the AUTOSAR XCP
 ** module.
 **
 ** Precondition: None.
 **
 ** \param[in]  Xcp_ConfigPtr  Pointer to a selected configuration structure.
 **
 ** \ServiceID{::XCP_SID_INIT}
 ** \Reentrancy{Non-Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, XCP_CODE) Xcp_Init
(
  P2CONST(Xcp_ConfigType, AUTOMATIC, XCP_APPL_CONST) Xcp_ConfigPtr
);

/*------------------[Scheduled functions]-----------------------------------*/

/** \brief Scheduled function of the XCP module.
 **
 ** Scheduled function of the XCP module.
 **
 ** Precondition: None
 **
 ** \ServiceID{::XCP_SID_MAIN_FUNCTION} */
extern FUNC(void, XCP_CODE) Xcp_MainFunction
(
  void
);

/** \brief Function to set a DAQ list sampling event.
 **
 ** This function sets an event for the given event channel to trigger the
 ** sampling of the associated DAQ list(s). This event will then be handled by
 ** one of the following Xcp_MainFunction() calls.
 **
 ** Precondition: The event channel must be a non-cyclic event channel, i.e.
 ** the parameter XcpEventChannelTimeCycle is set to 0 for that event channel.
 **
 ** \param[in]  EventId  XCP event channel ID.
 **
 ** \return Result of the operation.
 ** \retval XCP_OK     Setting the event was successful.
 ** \retval XCP_NOT_OK Setting the event failed (a development error
 **                    happened).
 ** \retval XCP_NOT_INITIALIZED Setting the event failed (module is not
 **                             initialized).
 ** \retval XCP_NOT_CONNECTED   Setting the event failed (slave is in the
 **                             state DISCONNECTED).
 ** \retval XCP_OVERLOAD        Setting the event failed (event is already
 **                             set).
 ** \retval XCP_NO_ACTIVE_LIST  Setting the event failed (no active list is
 **                             assigned to that event).
 **
 ** \ServiceID{::XCP_SID_SET_EVENT}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(Xcp_ReturnType, XCP_CODE) Xcp_SetEvent
(
  uint16 EventId
);

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( XCP_H ) */
/*==================[end of file]============================================*/
