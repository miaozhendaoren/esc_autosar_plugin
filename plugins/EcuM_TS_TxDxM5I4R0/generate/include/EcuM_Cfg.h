/**
 * \file
 *
 * \brief AUTOSAR EcuM
 *
 * This file contains the implementation of the AUTOSAR
 * module EcuM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO EcuM2993,1 */
#if (!defined ECUM_CFG_H)
#define ECUM_CFG_H

/*==================[includes]==============================================*/

#include <Os.h>                 /* OS AppMode */
#include <Std_Types.h>          /* AUTOSAR standard types */

/*==================[macros]================================================*/

#if (defined ECUM_EB_SERVICE_API_NONE)
#error ECUM_EB_SERVICE_API_NONE already defined
#endif
/** \brief Definition that no service API is set as default **/
#define ECUM_EB_SERVICE_API_NONE 0U

#if (defined ECUM_EB_SERVICE_API_ASR32)
#error ECUM_EB_SERVICE_API_ASR32 already defined
#endif
/** \brief Definition that AUTOSAR 3.2 service API is set as default **/
#define ECUM_EB_SERVICE_API_ASR32 32U

#if (defined ECUM_EB_SERVICE_API_ASR40)
#error ECUM_EB_SERVICE_API_ASR40 already defined
#endif
/** \brief Definition that AUTOSAR 4.0 service API is set as default **/
#define ECUM_EB_SERVICE_API_ASR40 40U

#if (defined ECUM_EB_ENABLE_ASR32_SERVICE_API)
#error ECUM_EB_ENABLE_ASR32_SERVICE_API already defined
#endif
/** \brief Definition whether AUTOSAR 3.2 service API for EcuM is enabled **/
#define ECUM_EB_ENABLE_ASR32_SERVICE_API                   [!//
[!IF "EcuMGeneral/EcuMServiceAPI/EcuMEnableASR32ServiceAPI = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined ECUM_EB_ENABLE_ASR40_SERVICE_API)
#error ECUM_EB_ENABLE_ASR40_SERVICE_API already defined
#endif
/** \brief Definition whether AUTOSAR 4.0 service API for EcuM is enabled **/
#define ECUM_EB_ENABLE_ASR40_SERVICE_API                   [!//
[!IF "EcuMGeneral/EcuMServiceAPI/EcuMEnableASR40ServiceAPI = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined ECUM_EB_DEFAULT_ASR_SERVICE_API)
#error ECUM_EB_DEFAULT_ASR_SERVICE_API already defined
#endif
/** \brief Definition of default service API **/
#define ECUM_EB_DEFAULT_ASR_SERVICE_API                    [!//
[!IF "EcuMGeneral/EcuMServiceAPI/EcuMDefaultASRServiceAPI = 'AUTOSAR_32'"!]ECUM_EB_SERVICE_API_ASR32
[!ELSEIF "EcuMGeneral/EcuMServiceAPI/EcuMDefaultASRServiceAPI = 'AUTOSAR_40'"!]ECUM_EB_SERVICE_API_ASR40
[!ELSE!]ECUM_EB_SERVICE_API_NONE
[!ENDIF!]

/** \brief Switch for DET usage */
/* !LINKSTO EcuM2983,1 */
#define ECUM_DEV_ERROR_DETECT      [!//
[!IF "EcuMGeneral/EcuMDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Switch for Defensive behaviour usage */
#define ECUM_DEF_BEHAVIOUR_ENABLED [!//
[!IF "EcuMGeneral/EcuMEnableDefBehaviour = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined ECUM_INCLUDE_DET)
#error ECUM_INCLUDE_DET already defined
#endif
/** \brief Initialize Det module at startup **/
#define ECUM_INCLUDE_DET           [!//
[!IF "EcuMGeneral/EcuMIncludeDet = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined ECUM_INCLUDE_DEM)
#error ECUM_INCLUDE_DEM already defined
#endif
/** \brief Initialize Dem module at startup **/
#define ECUM_INCLUDE_DEM           [!//
[!IF "EcuMGeneral/EcuMIncludeDem = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined ECUM_INCLUDE_RTE)
#error ECUM_INCLUDE_RTE already defined
#endif
/** \brief Check if Rte is used in EcuM **/
#define ECUM_INCLUDE_RTE           [!//
[!IF "EcuMGeneral/EcuMRteUsage = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined ECUM_USE_BOOTTARGET_SEC)
#error ECUM_USE_BOOTTARGET_SEC already defined
#endif
/** \brief Use special memory section for EcuM_BootTarget */
#define ECUM_USE_BOOTTARGET_SEC    [!//
[!IF "EcuMGeneral/EcuMUseBoottargetSec = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined ECUM_VERSION_INFO_API)
#error ECUM_VERSION_INFO_API already defined
#endif
/** \brief Provide EcuM_GetVersionInfo() API !*/
#define ECUM_VERSION_INFO_API      [!//
[!IF "EcuMGeneral/EcuMVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined ECUM_PROVIDE_SHUTDOWN_HOOK)
#error ECUM_PROVIDE_SHUTDOWN_HOOK already defined
#endif
/** \brief Provide minimalistic ShutdownHook() implementation */
#define ECUM_PROVIDE_SHUTDOWN_HOOK [!//
[!IF "EcuMGeneral/EcuMProvideShutdownHook = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Switch for Reset Loop Detection usage */
#define ECUM_RESET_LOOP_DETECTION  [!//
[!IF "EcuMFlexGeneral/EcuMResetLoopDetection = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define ECUM_ECUM_FIXED_SUPPORT    [!//
[!IF "node:exists(EcuMConfiguration/*[1]/EcuMFixedConfiguration) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#if (defined ECUM_STORED_SHUTDOWN_OPERATIONS)
#error ECUM_STORED_SHUTDOWN_OPERATIONS already defined
#endif
/** \brief Size of the array which contains all the recent
 shutdown information. Used to implement EcuM_GetNextRecentShutdown() */
#define ECUM_STORED_SHUTDOWN_OPERATIONS   [!//
[!"EcuMFlexGeneral/EcuMStoredShutdownOperations"!]U
/*------------------[DEM/DET error configuration]---------------------------*/

#if (defined ECUM_PROD_ERR_RAM_CHECK_FAILURE)
#error ECUM_PROD_ERR_RAM_CHECK_FAILURE already defined
#endif
/** \brief Switch for DEM to DET reporting Ram Check Failure */
#define ECUM_PROD_ERR_RAM_CHECK_FAILURE                 [!//
[!IF "ReportToDem/EcuMRamChkFailedReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/EcuMRamChkFailedReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

#if (defined ECUM_PROD_ERR_CFG_DATA_INCONSISTENT)
#error ECUM_PROD_ERR_CFG_DATA_INCONSISTENT already defined
#endif
/** \brief Switch for DEM to DET reporting Configuration Data Inconsistent
 **
 ** This preprocessor macro belongs to a feature which is not supported by the
 ** current implementation. It is available for completeness reasons only. */
#define ECUM_PROD_ERR_CFG_DATA_INCONSISTENT             [!//
[!IF "ReportToDem/EcuMCfgDataInconsistentReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/EcuMCfgDataInconsistentReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/EcuMRamChkFailedReportToDem = 'DET'"!][!//
/** \brief DET error Id for Ram check failure */
#define ECUM_E_DEMTODET_RAM_CHECK_FAILED                [!//
[!"ReportToDem/EcuMRamChkFailedReportToDemDetErrorId"!]U

[!ENDIF!][!//
[!//
[!IF "ReportToDem/EcuMCfgDataInconsistentReportToDem = 'DET'"!][!//
/** \brief DET error Id for Inconsistent Configuration Data */
#define ECUM_E_DEMTODET_CONFIGURATION_DATA_INCONSISTENT [!//
[!"ReportToDem/EcuMCfgDataInconsistentReportToDemDetErrorId"!]U
[!ENDIF!][!//

#if (defined ECUM_PROD_ERR_ALL_RUN_REQUESTS_KILLED)
#error ECUM_PROD_ERR_ALL_RUN_REQUESTS_KILLED already defined
#endif
/** \brief Switch for DEM reporting of all run requests killed */
#define ECUM_PROD_ERR_ALL_RUN_REQUESTS_KILLED [!//
[!IF "node:exists(as:modconf('EcuM')[1]/EcuMConfiguration/*[1]/EcuMCommonConfiguration/EcuMDemEventParameterRefs/ECUM_E_ALL_RUN_REQUESTS_KILLED)"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

/*------------------[User defined initialization switches]------------------*/
[!SELECT "EcuMConfiguration/*[1]/EcuMCommonConfiguration"!][!//

/** \brief Switch for usage of EcuM_Al_DriverInitZero */
#define ECUM_INITLIST_ZERO      [!//
[!IF "node:exists(EcuMDriverInitListZero)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Switch for usage of EcuM_Al_DriverInitOne */
#define ECUM_INITLIST_ONE       [!//
[!IF "node:exists(EcuMDriverInitListOne)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Switch for usage of EcuM_Al_DriverRestart */
#define ECUM_INITLIST_RESTART   [!//
[!IF "node:exists(EcuMDriverRestartList)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/*------------------[EcuM Flex User Config]--------------------------------------*/

/* Symbolic names which can be used for all function argument of type
 * EcuM_UserType */
[!LOOP "../EcuMFlexConfiguration/EcuMFlexUserConfig/*"!][!//
#if (defined EcuMConf_[!"name(..)"!]_[!"name(.)"!])
#error EcuMConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif
/** \brief Identifier for the EcuM user "[!"name(.)"!]" */
#define EcuMConf_[!"name(..)"!]_[!"name(.)"!]   [!"EcuMFlexUser"!]U

#if (!defined ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]   [!"EcuMFlexUser"!]U

#if (defined EcuM_[!"name(.)"!])
#error EcuM_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define EcuM_[!"name(.)"!]   [!"EcuMFlexUser"!]U
#endif /* ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */

[!ENDLOOP!][!//
[!// The following macros are also generated by the Rte
[!IF "../../../EcuMGeneral/EcuMRteUsage = 'false'"!]
[!LOOP "../EcuMFixedConfiguration/EcuMFixedUserConfig/*"!][!//
#if (defined EcuMConf_[!"name(..)"!]_[!"name(.)"!])
#error EcuMConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif
#define EcuMConf_[!"name(..)"!]_[!"name(.)"!]   [!"EcuMFixedUser"!]U
#if (!defined ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
#define [!"name(.)"!]   [!"EcuMFixedUser"!]U
#if (defined EcuM_[!"name(.)"!])
#error EcuM_[!"name(.)"!] already defined
#endif
#define EcuM_[!"name(.)"!]   [!"EcuMFixedUser"!]U
#endif /* ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//
[!//
[!ENDIF!][!//
#define ECUM_FIXED_NUM_FIXED_USERS [!"num:i(count(../EcuMFixedConfiguration/EcuMFixedUserConfig/*))"!]U
/*------------------[Wakeup Sources]----------------------------------------*/

[!LOOP "EcuMWakeupSource/*"!][!//
#if (defined EcuMConf_[!"name(..)"!]_[!"name(.)"!])
#error EcuMConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif
/** \brief Identifier for wakeup source [!"name(.)"!] */
#define EcuMConf_[!"name(..)"!]_[!"name(.)"!]   [!"bit:bitset(0, EcuMWakeupSourceId)"!]U

#if (!defined ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]   [!"bit:bitset(0, EcuMWakeupSourceId)"!]U

#if (defined EcuM_[!"name(.)"!])
#error EcuM_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define EcuM_[!"name(.)"!]   [!"bit:bitset(0, EcuMWakeupSourceId)"!]U
#endif /* ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */

[!ENDLOOP!][!//
[!//
/** \brief Number of wakeup sources */
#define ECUM_WKSCONFIGNUM          [!//
[!"num:i(count(EcuMWakeupSource/*))"!]U

/** \brief Maximum value of Wakeup Source ID */
#define ECUM_WKS_ID_MAXVAL          [!//
[!"num:i(num:max(EcuMWakeupSource/*/EcuMWakeupSourceId))"!]U

/** \brief Bit mask of all wakeup sources */
#define ECUM_WKSMASKALL (EcuM_WakeupSourceType)(\
[!LOOP "EcuMWakeupSource/*"!][!//
   EcuMConf_[!"name(..)"!]_[!"name(.)"!] | \
[!ENDLOOP!][!//
   0U)

/** \brief Bit mask of wakeup sources with no validation All standard WKS
 * require no validation */
#define ECUM_WKSMASKNOVAL (EcuM_WakeupSourceType)(\
[!LOOP "EcuMWakeupSource/*[not(node:exists(EcuMValidationTimeout))]"!][!//
   EcuMConf_[!"name(..)"!]_[!"name(.)"!] | \
[!ENDLOOP!][!//
   0U)

/** \brief Bit mask of wakeup sources with ComM channel */
#define ECUM_WKSMASKCOMMCH (EcuM_WakeupSourceType)(\
[!LOOP "EcuMWakeupSource/*[node:refexists(EcuMComMChannelRef)]"!][!//
   EcuMConf_[!"name(..)"!]_[!"name(.)"!] | \
[!ENDLOOP!][!//
   0U)

/** \brief Switch to enable handling of wakeup sources with ComM channel */
#define ECUM_WKS_COMMCH_ENABLED    [!//
[!IF "count(node:refs(EcuMWakeupSource/*/EcuMComMChannelRef)) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/*------------------[Sleep modes]-------------------------------------------*/
/* !LINKSTO EcuM2957,1 */
/** \brief number of sleep modes */
#define ECUM_SLEEPMODECONFIGNUM    [!//
[!"num:i(count(EcuMSleepMode/*))"!]U

[!LOOP "node:order(EcuMSleepMode/*,'node:value(EcuMSleepModeId)')"!][!//
#if (defined EcuMConf_[!"name(..)"!]_[!"name(.)"!])
#error EcuMConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif

/** \brief Identifier for sleep mode [!"name(.)"!] */
#define EcuMConf_[!"name(..)"!]_[!"name(.)"!]   [!"num:inttohex(EcuMSleepModeId)"!]U

#if (!defined ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]   [!"num:inttohex(EcuMSleepModeId)"!]U

#if (defined EcuM_[!"name(.)"!])
#error EcuM_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define EcuM_[!"name(.)"!]   [!"num:inttohex(EcuMSleepModeId)"!]U
#endif /* ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */

/** \brief Wakeup event mask of sleep mode [!"name(.)"!] */
#define ECUM_[!"name(.)"!]_WKUP_MASK (\
[!LOOP "EcuMWakeupSourceMask/*"!][!//
EcuMConf_[!"name(as:ref(.)/..)"!]_[!"name(as:ref(.))"!] | \
[!ENDLOOP!][!//
0U)
[!ENDLOOP!][!//


[!ENDSELECT!]
/*------------------[Mcu Mode]----------------------------------------------*/
[!SELECT "EcuMConfiguration/*[1]/EcuMFlexConfiguration"!][!//
[!//
[!//An xdm check ensures that the reference is valid for EcuMNormalMcuModeRef.
#if (defined [!//
McuConf_[!"node:name(node:dtos(as:ref(./EcuMNormalMcuModeRef)))"!]_[!"name(as:ref(EcuMNormalMcuModeRef))"!])
#define ECUM_NORMALMCUMODE   [!//
McuConf_[!"node:name(node:dtos(as:ref(./EcuMNormalMcuModeRef)))"!]_[!"name(as:ref(EcuMNormalMcuModeRef))"!]  [!//
/* [!"as:ref(EcuMNormalMcuModeRef)/McuMode"!]U */
#else
#define ECUM_NORMALMCUMODE   [!//
[!"name(as:ref(EcuMNormalMcuModeRef))"!]   [!//
/* [!"as:ref(EcuMNormalMcuModeRef)/McuMode"!]U */
#endif

/*------------------[Reset Modes]----------------------------------------------*/
/* !LINKSTO EcuM4005,1 */
[!LOOP "EcuMResetMode/*"!][!//
#if (defined EcuMConf_[!"name(..)"!]_[!"name(.)"!])
#error EcuMConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif
/** \brief Identifier for reset modes [!"name(.)"!] */
#define EcuMConf_[!"name(..)"!]_[!"name(.)"!]   [!"EcuMResetModeId"!]U

#if (!defined ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]   [!"EcuMResetModeId"!]U

#if (defined EcuM_[!"name(.)"!])
#error EcuM_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define EcuM_[!"name(.)"!]   [!"EcuMResetModeId"!]U
#endif /* ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */

[!ENDLOOP!][!//

/*------------------[Shutdown Causes]----------------------------------------------*/
/* !LINKSTO EcuM4007,1 */
[!LOOP "EcuMShutdownCause/*"!][!//
#if (defined EcuMConf_[!"name(..)"!]_[!"name(.)"!])
#error EcuMConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif
/** \brief Identifier for reset modes [!"name(.)"!] */
#define EcuMConf_[!"name(..)"!]_[!"name(.)"!]   [!"EcuMShutdownCauseId"!]U

#if (!defined ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
[!// The following macros are also generated by the Rte
[!IF "../../../../../EcuMGeneral/EcuMRteUsage = 'false'"!]
/* Generating the symbolic name for only user-configured
 * shutdown casues, since all pre-configured shutdown causes are already
 * defined in EcuM.h. */
[!IF "(EcuMShutdownCauseId) > 3"!]
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]   [!"EcuMShutdownCauseId"!]U
[!ENDIF!]
[!ENDIF!][!//

#if (defined EcuM_[!"name(.)"!])
#error EcuM_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define EcuM_[!"name(.)"!]   [!"EcuMShutdownCauseId"!]U
#endif /* ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */

[!ENDLOOP!][!//
/** \brief Number of shutdown causes */
#define ECUM_SHUTDOWNCAUSECONFIGNUM          [!//
[!"num:i(count(EcuMShutdownCause/*))"!]U

/*------------------[EcuM Alarm Clock]----------------------------------------------*/

[!LOOP "EcuMAlarmClock/*"!][!//
#if (defined EcuMConf_[!"name(..)"!]_[!"name(.)"!])
#error EcuMConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif
/** \brief Identifier for reset modes [!"name(.)"!] */
#define EcuMConf_[!"name(..)"!]_[!"name(.)"!]   [!"EcuMAlarmClockId"!]U

#if (!defined ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]   [!"EcuMAlarmClockId"!]U

#if (defined EcuM_[!"name(.)"!])
#error EcuM_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define EcuM_[!"name(.)"!]   [!"EcuMAlarmClockId"!]U
#endif /* ECUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */

[!ENDLOOP!][!//

[!ENDSELECT!]
/*==================[type definitions]======================================*/

/* !LINKSTO EcuM4038,1 */
/** \brief Type for EcuM configuration */
typedef struct EcuM_ConfigStruct EcuM_ConfigType;

/* !LINKSTO EcuM2801,1 */
/** \brief Structure definition of EcuM configuration
 *
 * This structure must be defined publically for the public variable
 * declarations later on in this file. */
struct EcuM_ConfigStruct
{
  /** \brief OS Application Mode started by default. **/
  AppModeType DefaultAppMode;

  /* *** Init list related parameters *** */

  P2FUNC(void, ECUM_CODE, DriverInitOne)(P2CONST(
    struct EcuM_ConfigStruct, AUTOMATIC, ECUM_CONST) ConfigPtr);
  P2FUNC(void, ECUM_CODE, DriverRestart)(P2CONST(
    struct EcuM_ConfigStruct, AUTOMATIC, ECUM_CONST) ConfigPtr);

  /* *** Shutdown/Sleep related parameters *** */

  /** \brief State of the default shutdown target. **/
  uint8 DefaultShutdownTarget;

  /** \brief Mode of the default shutdown target (only required if
   * DefaultShutdownState is equal to ECUM_STATE_SLEEP or ECUM_STATE_RESET). **/
  uint8 DefaultMode;
[!IF "node:exists(EcuMConfiguration/*[1]/EcuMFixedConfiguration) = 'true'"!][!//
  uint32 RunTimeout;
[!ENDIF!][!//
};

/*==================[external function declarations]========================*/
[!SELECT "EcuMConfiguration/*[1]"!][!//
#define ECUM_START_SEC_CODE
#include <MemMap.h>

[!IF "node:exists(EcuMCommonConfiguration/EcuMDriverInitListOne)"!][!//
/** \brief Callout for driver initialization sequence one for EcuM.
 **
 ** This is the configuration specific callout called from
 ** EcuM_AL_DriverInitOne().  This function contains the Initialization
 ** of Modules configured in EcuMDriverInitListOne
 **
 ** \param[in] ConfigPtr Pointer to the EcuM post-build configuration which
 ** contains pointers to all other BSW module post-build configurations. */
extern FUNC(void, ECUM_APPL_CODE) EcuM_DriverInitListOne
(
  P2CONST(EcuM_ConfigType, AUTOMATIC, ECUM_CONST) ConfigPtr
);
[!ENDIF!][!//

[!IF "node:exists(EcuMCommonConfiguration/EcuMDriverRestartList)"!][!//
/** \brief Callout for restarting drivers for EcuM.
 **
 ** This is the configuration specific callout called from
 ** EcuM_AL_DriverRestart().  This function contains the Initialization
 ** of Modules configured in EcuMDriverRestartList
 **
 ** \param[in] ConfigPtr Pointer to the EcuM post-build configuration which
 ** contains pointers to all other BSW module post-build configurations. */
extern FUNC(void, ECUM_APPL_CODE) EcuM_DriverRestartList
(
  P2CONST(EcuM_ConfigType, AUTOMATIC, ECUM_CONST) ConfigPtr
);
[!ENDIF!][!//

[!ENDSELECT!][!//

[!IF "EcuMGeneral/EcuMProvideShutdownHook = 'true'"!][!//
/** \brief Hook routine called by Os during shutdown.
 **
 ** This hook routine is called by the operating system when the
 ** OS service ShutdownOS has been called. This routine is called
 ** during the operating system shut down.
 **
 ** If enabled by configuration, the EcuM provides a minimalistic
 ** implementation of the ShutdownHook() callback function for the Os.
 **
 ** \param[in] errorID Implementation dependent error value */

extern void ShutdownHook(StatusType errorID);
[!ENDIF!][!//

#define ECUM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

#define ECUM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief EcuM configuration for [!"name(EcuMConfiguration/*[1])"!]
 **
 ** This EcuM configuration may be be returned by
 ** EcuM_DeterminePbConfiguration().  The name of this element is
 ** configuration dependent. */
extern CONST(EcuM_ConfigType, ECUM_CONST) [!"name(EcuMConfiguration/*[1])"!];
#define ECUM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( ECUM_CFG_H ) */
/*==================[end of file]===========================================*/
