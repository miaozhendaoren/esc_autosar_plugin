[!/**
 * \file
 *
 * \brief AUTOSAR NvM
 *
 * This file contains the implementation of the AUTOSAR
 * module NvM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!/*
=== !LINKSTO NVM544,1 ===
=== define maximum space needed for CRC ===
*/!][!IF "count(NvMBlockDescriptor/*[(NvMBlockUseCrc = 'true') and (NvMBlockCrcType='NVM_CRC32')]) > 0"!][!/*
   */!][!VAR "maxCrcSize"="4"!][!/*
*/!][!ELSEIF "count(NvMBlockDescriptor/*[(NvMBlockUseCrc = 'true') and (NvMBlockCrcType='NVM_CRC16')]) > 0"!][!/*
   */!][!VAR "maxCrcSize"="2"!][!/*
*/!][!ELSE!][!/*
   */!][!VAR "maxCrcSize"="1"!][!/*   
*/!][!ENDIF!][!/*

=== calculate the maximum space needed for internal buffer ===
  */!][!VAR "crcSize" = "0"!][!/*
  */!][!VAR "blockIdSize" = "0"!][!/*
  */!][!VAR "maxBlockSize"="0"!][!/* 
  */!][!LOOP "NvMBlockDescriptor/*[((NvMBlockUseCrc = 'true') and (NvMUserProvidesSpaceForBlockAndCrc != 'true')) or (NvMWriteVerification = 'true') or (NvMStaticBlockIDCheck = 'true') or(NvMBlockUseSyncMechanism = 'true')]"!][!/*
  */!][!IF "NvMBlockUseCrc = 'true'"!][!/*
    */!][!IF "NvMBlockCrcType = 'NVM_CRC32'"!][!/*
      */!][!VAR "crcSize"="4"!][!/* 
      */!][!ELSEIF "NvMBlockCrcType = 'NVM_CRC16'"!][!/*
      */!][!VAR "crcSize"="2"!][!/* 
    */!][!ELSE!][!/*
      */!][!VAR "crcSize"="1"!][!/* 
    */!][!ENDIF!][!/*
  */!][!ENDIF!][!/*
  */!][!IF "NvMStaticBlockIDCheck = 'true'"!][!VAR "blockIdSize"="2"!][!ENDIF!][!/*
  */!][!VAR "blockSize"="NvMNvBlockLength + $blockIdSize + $crcSize"!][!/*
  */!][!IF "$blockSize > $maxBlockSize"!][!/*
     */!][!VAR "maxBlockSize"="$blockSize"!][!/*
  */!][!ENDIF!][!/*
*/!][!ENDLOOP!][!/*

=== calculate the space needed for RAM CRC buffer ===
*/!][!VAR "CrcBufSize" = "0"!][!/*
*/!][!LOOP "NvMBlockDescriptor/*[(NvMBlockUseCrc = 'true') and (NvMCalcRamBlockCrc = 'true')]"!][!/*
   */!][!IF "NvMBlockCrcType = 'NVM_CRC8'"!][!/*
      */!][!VAR "CrcBufSize" = "$CrcBufSize + 1"!][!/*
      */!][!ELSEIF "NvMBlockCrcType = 'NVM_CRC16'"!][!/*
      */!][!VAR "CrcBufSize" = "$CrcBufSize + 2"!][!/*
   */!][!ELSE!][!/*
      */!][!VAR "CrcBufSize" = "$CrcBufSize + 4"!][!/*
   */!][!ENDIF!][!/*
*/!][!ENDLOOP!][!/*

=== calculate the space needed for RAM Write verification buffer ===
*/!][!VAR "writeVerificationBufferSize"="0"!][!/*
*/!][!VAR "maxVerificationSize"="0"!][!/*
*/!][!IF "count(NvMBlockDescriptor/*[(NvMWriteVerification = 'true')])>0"!][!/* 
  */!][!LOOP "NvMBlockDescriptor/*[(NvMWriteVerification = 'true')]"!][!/*
  */!][!VAR "writeVerificationBufferSize"="NvMWriteVerificationDataSize"!][!/*
  */!][!IF "$writeVerificationBufferSize > $maxVerificationSize"!][!/*
     */!][!VAR "maxVerificationSize"="$writeVerificationBufferSize"!][!/*
  */!][!ENDIF!][!/*
*/!][!ENDLOOP!][!/*
*/!][!ENDIF!]