/**
 * \file
 *
 * \brief AUTOSAR NvM
 *
 * This file contains the implementation of the AUTOSAR
 * module NvM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!INCLUDE "NvM_Include.m"!][!//
/* !LINKSTO NVM030,1 */
[!INCLUDE "NvM_Checks.m"!][!//
#if (!defined NVM_CFG_H)
#define NVM_CFG_H
/* !LINKSTO NVM028,1 */
/*==================[inclusions]============================================*/

/* !LINKSTO NVM689,1 */
#include <NvM_Types.h>        /* contains NvM specific types. NVM689 */
#include <Std_Types.h>

[!/* Each configured NvM block should have corresponding number of blocks configured in Fee or Ea */!]
[!/* Their BlockNumber has to match with NvMNvBlockBaseNumber + DataSetNumber */!]
[!LOOP "NvMBlockDescriptor/*[(NvMNvramBlockIdentifier > 1) and (NvMNvBlockNum > 1)]"!][!/*
*/!][!VAR "blockDesc"="NvMNvBlockNum"!][!/*
*/!][!IF "(node:exists(NvMTargetBlockReference/NvMNameOfFeeBlock))"!][!/*
*/!][!VAR "blockNumber"="node:ref(NvMTargetBlockReference/NvMNameOfFeeBlock)/FeeBlockNumber"!][!/*
*/!][!ELSEIF "(node:exists(NvMTargetBlockReference/NvMNameOfEaBlock))"!][!/*
*/!][!VAR "blockNumber"="node:ref(NvMTargetBlockReference/NvMNameOfEaBlock)/EaBlockNumber"!][!/*
*/!][!ENDIF!][!/*
*/!][!FOR "I"="0" TO "$blockDesc - 1"!][!/*
*/!][!IF "(node:exists(NvMTargetBlockReference/NvMNameOfFeeBlock)) and
          (count(node:ref(NvMTargetBlockReference/NvMNameOfFeeBlock)/../../FeeBlockConfiguration/*[FeeBlockNumber = ($blockNumber + $I)]) != 1)"!][!/*
*/!][!ERROR!]There should exist exactly one block in Fee with Block number = [!"num:i($blockNumber + $I)"!] for [!"node:name(.)"!][!ENDERROR!][!/*
*/!][!ELSEIF "(node:exists(NvMTargetBlockReference/NvMNameOfEaBlock)) and
              (count(node:ref(NvMTargetBlockReference/NvMNameOfEaBlock)/../../EaBlockConfiguration/*[EaBlockNumber = ($blockNumber + $I)]) != 1)"!][!/*
*/!][!ERROR!]There should exist exactly one block in Ea with Block number = [!"num:i($blockNumber + $I)"!] for [!"node:name(.)"!][!ENDERROR!][!/*
*/!][!ENDIF!][!/*
*/!][!ENDFOR!][!/*
*/!][!ENDLOOP!]

/*==================[macros]================================================*/

/** \brief Macro switch for DET usage */
#define NVM_DEV_ERROR_DETECT             [!//
[!IF "NvMCommon/NvMDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Defines whether a Rte module configuration is available.
 **        If it is available then it is automatically used by the NVRAM Manager.
 **        STD_ON:  Rte configuration is available.
 **        STD_OFF: Rte configuration is not available.
 **/
#define NVM_INCLUDE_RTE                  [!//
[!IF "NvMCommon/NvMRteUsage = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Disables or enables the API NvM_SetRamBlockStatus()
 **/
#define NVM_SET_RAM_BLOCK_STATUS_API     [!//
[!IF "NvMCommon/NvMSetRamBlockStatusApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Disables or enables the API NvM_VersionInfoApi()
 **/
#define NVM_VERSION_INFO_API             [!//
[!IF "NvMCommon/NvMVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Dis- or enable the recovery of blocks()
 **/
#define NVM_ADVANCED_RECOVERY            [!//
[!IF "num:i(count(NvMBlockDescriptor/*[NvMAdvancedRecovery = 'true'])) != 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined NVM_PROD_ERR_HANDLING_INTEGRITY_FAILED)
#error NVM_PROD_ERR_HANDLING_INTEGRITY_FAILED already defined
#endif
/** \brief Switch for DEM to DET reporting Integrity Failure
 **/
#define NVM_PROD_ERR_HANDLING_INTEGRITY_FAILED     [!//
[!IF "ReportToDem/NvMIntegrityFailedReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/NvMIntegrityFailedReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

#if (defined NVM_PROD_ERR_HANDLING_REQ_FAILED)
#error NVM_PROD_ERR_HANDLING_REQ_FAILED already defined
#endif
/** \brief Switch for DEM to DET reporting Request Failure
 **/
#define NVM_PROD_ERR_HANDLING_REQ_FAILED           [!//
[!IF "ReportToDem/NvMRequestFailedReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/NvMRequestFailedReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

#if (defined NVM_PROD_ERR_HANDLING_LOSS_OF_REDUNDANCY)
#error NVM_PROD_ERR_HANDLING_LOSS_OF_REDUNDANCY already defined
#endif
/** \brief Switch for DEM to DET reporting Loss of Redundancy
 **/
#define NVM_PROD_ERR_HANDLING_LOSS_OF_REDUNDANCY   [!//
[!IF "ReportToDem/NvMLossOfRedundancyReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/NvMLossOfRedundancyReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

#if (defined NVM_PROD_ERR_HANDLING_QUEUE_OVERFLOW)
#error NVM_PROD_ERR_HANDLING_QUEUE_OVERFLOW already defined
#endif
/** \brief Switch for DEM to DET reporting Queue Overflow
 **/
#define NVM_PROD_ERR_HANDLING_QUEUE_OVERFLOW       [!//
[!IF "ReportToDem/NvMQueueOverflowReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/NvMQueueOverflowReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

#if (defined NVM_PROD_ERR_HANDLING_VERIFY_FAILED)
#error NVM_PROD_ERR_HANDLING_VERIFY_FAILED already defined
#endif
/** \brief Switch for DEM to DET reporting Verify Failure
 **/
#define NVM_PROD_ERR_HANDLING_VERIFY_FAILED        [!//
[!IF "ReportToDem/NvMVerifyFailedReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/NvMVerifyFailedReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

#if (defined NVM_PROD_ERR_HANDLING_WRITE_PROTECTED)
#error NVM_PROD_ERR_HANDLING_WRITE_PROTECTED already defined
#endif
/** \brief Switch for DEM to DET reporting Write Protection
 **/
#define NVM_PROD_ERR_HANDLING_WRITE_PROTECTED      [!//
[!IF "ReportToDem/NvMWriteProtectedReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/NvMWriteProtectedReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

#if (defined NVM_PROD_ERR_HANDLING_WRONG_BLOCK_ID)
#error NVM_PROD_ERR_HANDLING_WRONG_BLOCK_ID already defined
#endif
/** \brief Switch for DEM to DET reporting Wrong Block ID
 **/
#define NVM_PROD_ERR_HANDLING_WRONG_BLOCK_ID       [!//
[!IF "ReportToDem/NvMWrongBlockIdReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/NvMWrongBlockIdReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!/*

*/!][!IF "ReportToDem/NvMIntegrityFailedReportToDem = 'DET'"!][!//

#if (defined NVM_E_DEMTODET_INTEGRITY_FAILED)
#error NVM_E_DEMTODET_INTEGRITY_FAILED already defined
#endif
/** \brief Switch for DEM to DET reporting Integrity Failure
 **/
#define NVM_E_DEMTODET_INTEGRITY_FAILED            [!"num:i(ReportToDem/NvMIntegrityFailedReportToDemDetErrorId)"!]U
[!ENDIF!][!/*

*/!][!IF "ReportToDem/NvMRequestFailedReportToDem = 'DET'"!][!//

#if (defined NVM_E_DEMTODET_REQ_FAILED)
#error NVM_E_DEMTODET_REQ_FAILED already defined
#endif
/** \brief Switch for DEM to DET reporting Request Failure
 **/
#define NVM_E_DEMTODET_REQ_FAILED                  [!"num:i(ReportToDem/NvMRequestFailedReportToDemDetErrorId)"!]U
[!ENDIF!][!/*

*/!][!IF "ReportToDem/NvMLossOfRedundancyReportToDem = 'DET'"!][!//

#if (defined NVM_E_DEMTODET_LOSS_OF_REDUNDANCY)
#error NVM_E_DEMTODET_LOSS_OF_REDUNDANCY already defined
#endif
/** \brief Switch for DEM to DET reporting Loss of Redundancy
 **/
#define NVM_E_DEMTODET_LOSS_OF_REDUNDANCY          [!"num:i(ReportToDem/NvMLossOfRedundancyReportToDemDetErrorId)"!]U
[!ENDIF!][!/*

*/!][!IF "ReportToDem/NvMQueueOverflowReportToDem = 'DET'"!][!//

#if (defined NVM_E_DEMTODET_QUEUE_OVERFLOW)
#error NVM_E_DEMTODET_QUEUE_OVERFLOW already defined
#endif
/** \brief Switch for DEM to DET reporting Queue Overflow
 **/
#define NVM_E_DEMTODET_QUEUE_OVERFLOW              [!"num:i(ReportToDem/NvMQueueOverflowReportToDemDetErrorId)"!]U
[!ENDIF!][!/*

*/!][!IF "ReportToDem/NvMVerifyFailedReportToDem = 'DET'"!][!//

#if (defined NVM_E_DEMTODET_VERIFY_FAILED)
#error NVM_E_DEMTODET_VERIFY_FAILED already defined
#endif
/** \brief Switch for DEM to DET reporting Verify Failure
 **/
#define NVM_E_DEMTODET_VERIFY_FAILED               [!"num:i(ReportToDem/NvMVerifyFailedReportToDemDetErrorId)"!]U
[!ENDIF!][!/*

*/!][!IF "ReportToDem/NvMWriteProtectedReportToDem = 'DET'"!][!//

#if (defined NVM_E_DEMTODET_WRITE_PROTECTED)
#error NVM_E_DEMTODET_WRITE_PROTECTED already defined
#endif
/** \brief Switch for DEM to DET reporting Write Protection
 **/
#define NVM_E_DEMTODET_WRITE_PROTECTED             [!"num:i(ReportToDem/NvMWriteProtectedReportToDemDetErrorId)"!]U
[!ENDIF!][!/*

*/!][!IF "ReportToDem/NvMWrongBlockIdReportToDem = 'DET'"!][!//

#if (defined NVM_E_DEMTODET_WRONG_BLOCK_ID)
#error NVM_E_DEMTODET_WRONG_BLOCK_ID already defined
#endif
/** \brief Switch for DEM to DET reporting Wrong Block ID
 **/
#define NVM_E_DEMTODET_WRONG_BLOCK_ID              [!"num:i(ReportToDem/NvMWrongBlockIdReportToDemDetErrorId)"!]U
[!ENDIF!]

/** \brief special value: no Dem event id configured
 **        (0 is not a valid value for Dem_EventIdType)
 **/
#define NVM_INVALID_DEM_EVENTID        0U

/** \brief Switch for integrity failure Dem event Id
 **/
#define NVM_INTEGRITY_FAILED_DEM_EVENT_ID [!//
[!IF "node:exists(NvmDemEventParameterRefs/NVM_E_INTEGRITY_FAILED)"!][!//
[!"num:i(node:ref(NvmDemEventParameterRefs/NVM_E_INTEGRITY_FAILED)/DemEventId)"!]U
[!ELSE!][!//
NVM_INVALID_DEM_EVENTID
[!ENDIF!][!//

/** \brief Switch for request failure Dem event Id
 **/
#define NVM_REQ_FAILED_DEM_EVENT_ID    [!//
[!IF "node:exists(NvmDemEventParameterRefs/NVM_E_REQ_FAILED)"!][!//
[!"num:i(node:ref(NvmDemEventParameterRefs/NVM_E_REQ_FAILED)/DemEventId)"!]U
[!ELSE!][!//
NVM_INVALID_DEM_EVENTID
[!ENDIF!][!//

/** \brief Switch for redundancy loss Dem event Id
 **/
#define NVM_REDUNDANCY_LOSS_DEM_EVENT_ID [!//
[!IF "node:exists(NvmDemEventParameterRefs/NVM_E_LOSS_OF_REDUNDANCY)"!][!//
[!"num:i(node:ref(NvmDemEventParameterRefs/NVM_E_LOSS_OF_REDUNDANCY)/DemEventId)"!]U
[!ELSE!][!//
NVM_INVALID_DEM_EVENTID
[!ENDIF!][!//

/** \brief Switch for queue overflow Dem event Id
 **/
#define NVM_QUEUE_OVERFLOW_DEM_EVENT_ID [!//
[!IF "node:exists(NvmDemEventParameterRefs/NVM_E_QUEUE_OVERFLOW)"!][!//
[!"num:i(node:ref(NvmDemEventParameterRefs/NVM_E_QUEUE_OVERFLOW)/DemEventId)"!]U
[!ELSE!][!//
NVM_INVALID_DEM_EVENTID
[!ENDIF!][!//

/** \brief Switch for verify failed Dem event Id
 **/
#define NVM_VERIFY_FAILED_DEM_EVENT_ID [!//
[!IF "node:exists(NvmDemEventParameterRefs/NVM_E_VERIFY_FAILED)"!][!//
[!"num:i(node:ref(NvmDemEventParameterRefs/NVM_E_VERIFY_FAILED)/DemEventId)"!]U
[!ELSE!][!//
NVM_INVALID_DEM_EVENTID
[!ENDIF!][!//

/** \brief Switch for write protection failure Dem event Id
 **/
#define NVM_WRITE_PROT_DEM_EVENT_ID    [!//
[!IF "node:exists(NvmDemEventParameterRefs/NVM_E_WRITE_PROTECTED)"!][!//
[!"num:i(node:ref(NvmDemEventParameterRefs/NVM_E_WRITE_PROTECTED)/DemEventId)"!]U
[!ELSE!][!//
NVM_INVALID_DEM_EVENTID
[!ENDIF!][!//

/** \brief Switch for wrong block Id Dem event Id
 **/
#define NVM_WRONG_BLOCK_DEM_EVENT_ID   [!//
[!IF "node:exists(NvmDemEventParameterRefs/NVM_E_WRONG_BLOCK_ID)"!][!//
[!"num:i(node:ref(NvmDemEventParameterRefs/NVM_E_WRONG_BLOCK_ID)/DemEventId)"!]U
[!ELSE!][!//
NVM_INVALID_DEM_EVENTID
[!ENDIF!][!//

/** \brief Symbolic name for Block 0.
 **/
#define NVM_BLOCK_MULTI                0U

[!LOOP "NvMBlockDescriptor/*"!][!//
#if (defined NvMConf_NvMBlockDescriptor_[!"name(.)"!])
#error NvMConf_NvMBlockDescriptor_[!"name(.)"!] already defined
#endif
/** \brief  Symbolic name for Block [!"NvMNvramBlockIdentifier"!] */
#define NvMConf_NvMBlockDescriptor_[!"name(.)"!]   [!"NvMNvramBlockIdentifier"!]U

#if (!defined NVM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]   [!"NvMNvramBlockIdentifier"!]U

#if (defined NvM_[!"name(.)"!])
#error NvM_[!"name(.)"!] already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define NvM_[!"name(.)"!]   [!"NvMNvramBlockIdentifier"!]U
#endif /* NVM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */

[!ENDLOOP!][!//
[!//
/** \brief Defines the number of all configured blocks including the reserved Block 0.
 **/
#define NVM_TOTAL_NUMBER_OF_NVRAM_BLOCKS [!//
[!"num:i(count(NvMBlockDescriptor/*) + 1)"!]U

/** \brief Defines whether the immediate priority queue is used.
 **
 **        Range:
 **        STD_ON  : Immediate priority queue is used.
 **        STD_OFF : Immediate priority queue is not usesd.
 **/
#define NVM_JOB_PRIORITIZATION           [!//
[!IF "NvMCommon/NvMJobPrioritization = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Defines the size of the internal data buffer
 **/
#define NVM_INTERNAL_BUFFER_SIZE         [!//
[!"num:i($maxBlockSize)"!]U

/** \brief Defines the size of the NvM internal mirror for explicit synchronization
 **/
#define NVM_MIRROR_SIZE                  [!//
[!IF "count(NvMBlockDescriptor/*[NvMBlockUseSyncMechanism = 'true']) > 0"!][!//
[!"num:i($maxBlockSize)"!]U[!//
[!ELSE!][!//
0U[!//
[!ENDIF!][!//

/** \brief Defines the size of the buffer for storing RAM block CRC values.
 **/
#define NVM_RAMBLOCK_CRC_BUFFER_SIZE     [!//
[!"num:i($CrcBufSize)"!]U

/** \brief Defines the size of the buffer for storing CRC values for redundant block.
 **/
#define NVM_STORED_CRC_SIZE              [!//
[!"num:i($maxCrcSize)"!]U

/** \brief The size of the standard job queue.
 **        One entry is reserved for WriteAll or ReadAll requests,
 **        so the configured value is incremented by one.
 **/
#define NVM_SIZE_STANDARD_JOB_QUEUE      [!//
[!"num:i(NvMCommon/NvMSizeStandardJobQueue + 1)"!]U

/** \brief The size of the immediate job queue.
 **/
#define NVM_SIZE_IMMEDIATE_JOB_QUEUE     [!//
[!IF "node:exists(NvMCommon/NvMSizeImmediateJobQueue)"!][!//
[!"num:i(NvMCommon/NvMSizeImmediateJobQueue)"!][!ELSE!]0[!ENDIF!]U

/** \brief The RAM data buffer for the configuration Id shall be defined
 **        according the following rules:
 **
 **        if (for block 1:    NvMBlockUseCrc     == TRUE
 **                   && NvMCalcRamBlockCrc == TRUE
 **                   && NvMBlockCrcType    == NVM_CRC_TYPE_8
 **           )
 **        then   'VAR(uint16,NVM_VAR)        NvM_ConfigurationId[2];'
 **        else
 **        if (for block 1:    NvMBlockUseCrc     == TRUE
 **                   && NvMCalcRamBlockCrc == TRUE
 **                   && NvMBlockCrcType    == NVM_CRC_TYPE_16
 **           )
 **        then   'VAR(uint16,NVM_VAR)        NvM_ConfigurationId[2];'
 **        else
 **        if (for block 1:    NvMBlockUseCrc     == TRUE
 **                          && NvMCalcRamBlockCrc == TRUE
 **                          && NvMBlockCrcType    == NVM_CRC_TYPE_32
 **           )
 **        then  'VAR(uint16,NVM_VAR)  NvM_ConfigurationId[3];'
 **        else  'VAR(uint16,NVM_VAR) NvM_ConfigurationId[1];'
 **/[!/*
*/!][!LOOP "NvMBlockDescriptor/*[NvMNvramBlockIdentifier = 1]"!][!/*
   */!][!IF "(node:exists(NvMCalcRamBlockCrc) and node:exists(NvMBlockCrcType)) and (NvMBlockUseCrc = 'true') and (NvMCalcRamBlockCrc = 'true')"!][!/*
     */!][!IF "NvMBlockCrcType = 'NVM_CRC8'"!]
#define NVM_CONFIGURATIONID_SIZE  2U[!/*
     */!][!ELSEIF "NvMBlockCrcType = 'NVM_CRC16'"!]
#define NVM_CONFIGURATIONID_SIZE  2U[!/*
     */!][!ELSE!]
#define NVM_CONFIGURATIONID_SIZE  3U[!/*
     */!][!ENDIF!][!/*
   */!][!ELSE!]
#define NVM_CONFIGURATIONID_SIZE  1U[!/*
   */!][!ENDIF!][!/*
*/!][!ENDLOOP!]


/** \brief Defines the maximum number of user data bytes for which the CRC
 **        calculation is not interrupted.
 **/
#define NVM_CRC_NUM_OF_BYTES             [!//
[!"num:i(NvMCommon/NvMCrcNumOfBytes)"!]U

/** \brief Defines the number of bits set aside for data set selection
 **/
#define NVM_DATASET_SELECTION_BITS       [!//
[!"num:i(NvMCommon/NvMDatasetSelectionBits)"!]U

/** \brief Defines the number of retries to let the application copy data to or from
 **        the NvM module's mirror before postponing the current job.
 **/
#define NVM_REPEAT_MIRROR_OPERATIONS     [!//
[!"num:i(NvMCommon/NvMRepeatMirrorOperations)"!]U

/** \brief determines if NvM_WriteAll() and NvM_ReadAll() must switch to fast mode.
 **/
#define NVM_DRV_MODE_SWITCH              [!//
[!IF "NvMCommon/NvMDrvModeSwitch = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Defines the number of NVRAM Blocks which have
 **        the configuration parameter NvMResistantToChangedSw set to false.
 **/
[!VAR "NumberOfNvMResistantToChangedSwBlocks" = "count(NvMBlockDescriptor/*[(NvMResistantToChangedSw = 'false')])"!][!//
#define NVM_NUMBER_OF_RESISTANT_TO_CHANGED_SW_BLOCKS    [!//
[!"num:i($NumberOfNvMResistantToChangedSwBlocks)"!]U

/** \brief Defines the number of NVRAM Blocks which have
 **        the configuration parameter NvMSelectBlockForReadAll set to true.
 **        Block 0 is reserved and independent of this parameter Block 1 is read as part of the NvM_ReadAll.
 **/
[!VAR "NumberOfNvMSelectBlockForReadAllBlocks" = "count(NvMBlockDescriptor/*[(NvMNvramBlockIdentifier > 1) and (node:exists(NvMSelectBlockForReadAll)) and (NvMSelectBlockForReadAll = 'true')])"!][!//
#define NVM_NUMBER_OF_SELECT_BLOCK_FOR_READALL_BLOCKS   [!//
[!"num:i($NumberOfNvMSelectBlockForReadAllBlocks)"!]U

/** \brief Defines the number of NVRAM Blocks which have
 **        the configuration parameter NvMSelectBlockForWriteAll set to true.
  **/
[!VAR "NumberOfWriteAllBlocks" =  "count(NvMBlockDescriptor/*[(node:exists(NvMSelectBlockForWriteAll)) and (NvMSelectBlockForWriteAll = 'true')])"!][!//
#define NVM_NUMBER_OF_SELECT_BLOCK_FOR_WRITEALL   [!//
[!"num:i($NumberOfWriteAllBlocks)"!]U

/** \brief Defines the number of NVRAM Blocks which have
 **        the configuration parameter NvMBlockManagementType
 **        set to NVM_BLOCK_DATASET.
 **/
[!VAR "NumberOfNvMDatasetBlocks" = "count(NvMBlockDescriptor/*[(NvMBlockManagementType  = 'NVM_BLOCK_DATASET')])"!][!//
#define NVM_NUMBER_OF_DATASET_BLOCKS                    [!//
[!"num:i($NumberOfNvMDatasetBlocks)"!]U

/** \brief Defines the number of NVRAM Blocks which have
 **        the configuration parameter NvMInitBlockCallback set to not null
 **        NvMProvideRteInitBlockPort is set to true.
 **/
[!VAR "NumberOfNvMInitBlockCallback" = "count(NvMBlockDescriptor/*[(node:exists(NvMInitBlockCallback)) and (NvMInitBlockCallback  != '')])"!][!//
[!VAR "NumberOfNvMInitBlockCallback" =  "$NumberOfNvMInitBlockCallback + count(NvMBlockDescriptor/*[(NvMProvideRteInitBlockPort = 'true')])"!][!//
#define NVM_NUMBER_OF_INIT_BLOCK_CALLBACK               [!//
[!"num:i($NumberOfNvMInitBlockCallback)"!]U

/** \brief Defines the number of NVRAM Blocks which have
 **        the configuration parameter NvMSingleBlockCallback set to not null.
 **/
[!VAR "NumberOfNvMSingleBlockCallback" = "count(NvMBlockDescriptor/*[(node:exists(NvMSingleBlockCallback)) and (NvMSingleBlockCallback  != '' )])"!][!//
[!VAR "NumberOfNvMSingleBlockCallback" = "$NumberOfNvMSingleBlockCallback + count(NvMBlockDescriptor/*[(NvMProvideRteJobFinishedPort = 'true')])"!][!//
#define NVM_NUMBER_OF_SINGLE_BLOCK_CALLBACK             [!//
[!"num:i($NumberOfNvMSingleBlockCallback)"!]U

/** \brief Defines the number of NVRAM Blocks which have
 **        the configuration parameter NvMBlockUseCrc set to true and
 **        NvMBlockCrcType set to NVM_CRC8.
 **/
[!VAR "NumberOfCalcCrc8Blocks" = "count(NvMBlockDescriptor/*[(NvMBlockUseCrc = 'true') and (node:exists(NvMBlockCrcType)) and (NvMBlockCrcType = 'NVM_CRC8')])"!][!//
#define NVM_NUMBER_OF_CALC_CRC8_BLOCKS                 [!//
[!"num:i($NumberOfCalcCrc8Blocks)"!]

/** \brief Defines the number of NVRAM Blocks which have
 **        the configuration parameter NvMBlockUseCrc set to true and
 **        NvMBlockCrcType set to NVM_CRC16.
 **/
[!VAR "NumberOfCalcCrc16Blocks" = "count(NvMBlockDescriptor/*[(NvMBlockUseCrc = 'true') and (node:exists(NvMBlockCrcType)) and (NvMBlockCrcType = 'NVM_CRC16')])"!][!//
#define NVM_NUMBER_OF_CALC_CRC16_BLOCKS                 [!//
[!"num:i($NumberOfCalcCrc16Blocks)"!]

/** \brief Defines the number of NVRAM Blocks which have
 **        the configuration parameter NvMBlockUseCrc set to true and
 **        NvMBlockCrcType set to NVM_CRC32.
 **/
[!VAR "NumberOfCalcCrc32Blocks" = "count(NvMBlockDescriptor/*[(NvMBlockUseCrc = 'true') and (node:exists(NvMBlockCrcType)) and (NvMBlockCrcType = 'NVM_CRC32')])"!][!//
#define NVM_NUMBER_OF_CALC_CRC32_BLOCKS                 [!//
[!"num:i($NumberOfCalcCrc32Blocks)"!]

/** \brief Defines whether BswM job status info is enabled/disabled
 **        by checking NvMBswMMultiBlockJobStatusInformation set to true.
 **/
#define NVM_BSWM_MULTI_BLOCK_JOB_STATUS_INFO            [!//
[!IF "NvMCommon/NvMBswMMultiBlockJobStatusInformation = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Defines whether BswM block status info is enabled/disabled
 **        by checking the number of NVRAM blocks having
 **        NvMBswMBlockStatusInformation set to true.
 **/
#define NVM_BSWM_BLOCK_STATUS_INFO                      [!//
[!IF "count(NvMBlockDescriptor/*[(NvMBswMBlockStatusInformation = 'true')]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Defines whether Write verification is turned on or not
 **        by checking the number of NVRAM Blocks having NvMWriteVerification set to true.
 **/
#define NVM_WRITE_VERIFICATION                          [!//
[!IF "count(NvMBlockDescriptor/*[(NvMWriteVerification = 'true')]) != 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Defines the size of the Write verification buffer.
 **/
#define NVM_WRITE_VERIFICATION_BUFFER_SIZE              [!//
[!"num:i($maxVerificationSize)"!]


/** \brief Defines whether Static Block Id check is turned on or not
 **        by checking the number of NVRAM Blocks having NvMStaticBlockIDCheck set to true.
 **/
#define NVM_STATIC_BLOCKID_CHECK                        [!//
[!IF "count(NvMBlockDescriptor/*[(NvMStaticBlockIDCheck = 'true')]) != 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Defines whether Explicit Synchronization is turned on or not
 **        by checking the number of NVRAM Blocks having NvMBlockUseSyncMechanism set to true.
 **/
#define NVM_SYNC_MECHANISM                              [!//
[!IF "count(NvMBlockDescriptor/*[(NvMBlockUseSyncMechanism = 'true')]) != 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Defines whether any block is configured with immediate priority
 **        by checking the number of NVRAM Blocks having NvMBlockJobPriority set to zero.
 **/
#define NVM_NUMBER_OF_IMMEDIATE_PRIORITY_BLOCKS         [!//
[!"num:i(count(NvMBlockDescriptor/*[(NvMBlockJobPriority = 0)]))"!]U

/** \brief Defines whether any block is configured with write block once
 **        by checking the number of NVRAM Blocks having NvMWriteBlockOnce set to zero.
 **/
#define NVM_WRITE_BLOCK_ONCE                            [!//
[!IF "count(NvMBlockDescriptor/*[(NvMWriteBlockOnce = 'true')]) != 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Defines whether any block is configured with write retry
 **        by checking the number of NVRAM Blocks having NvMMaxNumOfWriteRetries set to non-zero.
 **/
#define NVM_NUMBER_OF_WRITE_RETRY_BLOCKS                [!//
[!"num:i(count(NvMBlockDescriptor/*[(NvMMaxNumOfWriteRetries > 0)]))"!]U

/** \brief Defines whether any block is configured with read retry
 **        by checking the number of NVRAM Blocks having NvMMaxNumOfReadRetries set to non-zero.
 **/
#define NVM_NUMBER_OF_READ_RETRY_BLOCKS                 [!//
[!"num:i(count(NvMBlockDescriptor/*[(NvMMaxNumOfReadRetries > 0)]))"!]U

/** \brief Defines the NvM Api config class 1.
 **/
#define NVM_API_CONFIG_CLASS_1                          0U

/** \brief Defines the NvM Api config class 2.
 **/
#define NVM_API_CONFIG_CLASS_2                          1U

/** \brief Defines the NvM Api config class 3.
 **/
#define NVM_API_CONFIG_CLASS_3                          2U

/** \brief Defines API config class
 **        NVM_API_CONFIG_CLASS_3 - all API functions are available
 **        NVM_API_CONFIG_CLASS_2 - an intermediate set API functions is available
 **        NVM_API_CONFIG_CLASS_1 - a minimum set of API functions is available
 **/
#define NVM_API_CONFIG_CLASS                            [!//
[!"NvMCommon/NvMApiConfigClass"!]

/** \brief Preprocessor switch to enable/disable the polling mode in the NVRAM Manager
 **        and at the same time disable/enable the callback functions useable by lower layers.
 **
 **        STD_ON:  Polling mode enabled/callbacks disabled
 **        STD_OFF: Polling mode disabled/callbacks enabled
 **/
#define NVM_POLLING_MODE                 [!//
[!IF "NvMCommon/NvMPollingMode ='true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Preprocessor switch to enable the dynamic configuration management handling
 **        by the NvM_ReadAll request.
 **        STD_ON: Dynamic configuration management handling enabled.
 **        STD_OFF: Dynamic configuration management handling disabled.
 **/
#define NVM_DYNAMIC_CONFIGURATION                 [!//
[!IF "NvMCommon/NvMDynamicConfiguration = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Hook functions macros for ReadBlock and WriteBlock. They are called after
 **        the NvM calls the MemIf_Read and before it calls the NvM MemIf_Write
 **        function respectively.
 **/
#define NVM_READ_BLOCK_HOOK(BlockNum, RamBlockDataAddress, BlockLength) \
[!IF "NvMCommon/NvMReadBlockHook = 'true'"!][!//
  NvM_ReadBlockHook(BlockNum, RamBlockDataAddress, BlockLength)
[!ELSE!][!//
  do {} while (0)
[!ENDIF!][!//

#define NVM_WRITE_BLOCK_HOOK(BlockNum, RamBlockDataAddress, BlockLength) \
[!IF "NvMCommon/NvMWriteBlockHook = 'true'"!][!//
  NvM_WriteBlockHook(BlockNum, RamBlockDataAddress, BlockLength)
[!ELSE!][!//
  do {} while (0)
[!ENDIF!][!//

/** \brief Multi block callback function macro. It is called
 **        if the NvM multi request NvM_WriteAll() or NvM_ReadAll() is
 **        finished.
 **/
#define NVM_MULTI_BLOCK_CALLBACK(sid,errorStatus) \
[!IF "(node:exists(NvMCommon/NvMMultiBlockCallback)) and (NvMCommon/NvMMultiBlockCallback != '')"!][!//
  [!"NvMCommon/NvMMultiBlockCallback"!]((sid),(errorStatus))
[!ELSE!][!//
  do { } while (FALSE)
[!ENDIF!][!//

/** \brief Rte admin port macro used for declaring NvM_SetBlockProtection()
 **        when Rte usage is enabled but Rte admin port is disabled for all
 **        configured blocks.
 **/
#define NVM_ADMIN_PORT_USED                       [!//
[!IF "count(NvMBlockDescriptor/*[(NvMProvideRteAdminPort = 'true')]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Rte service port macro used for declaring NvM_GetErrorStatus()
 **        when Rte usage is enabled but Rte service port is disabled for all
 **        configured blocks.
 **/
#define NVM_SERVICE_PORT_USED                       [!//
[!IF "count(NvMBlockDescriptor/*[(NvMProvideRteServicePort = 'true')]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined NVM_SERVICE_API_NONE)
#error NVM_SERVICE_API_NONE already defined
#endif
/** \brief Definition that no default service API is set **/
#define NVM_SERVICE_API_NONE   0U

#if (defined NVM_SERVICE_API_ASR32)
#error NVM_SERVICE_API_ASR32 already defined
#endif
/** \brief Definition that AUTOSAR 3.2 service API is set as default **/
#define NVM_SERVICE_API_ASR32   32U

#if (defined NVM_SERVICE_API_ASR40)
#error NVM_SERVICE_API_ASR40 already defined
#endif
/** \brief Definition that AUTOSAR 4.0 service API is set as default **/
#define NVM_SERVICE_API_ASR40   40U

[!VAR "ServiceAPIEnabled" = "node:exists(NvMCommon/NvMServiceAPI)"!] [!//
[!IF "$ServiceAPIEnabled = 'true'"!] [!//
#if (defined NVM_ENABLE_ASR32_SERVICE_API) /* To prevent double declaration */
#error NVM_ENABLE_ASR32_SERVICE_API already defined
#endif /* if (defined NVM_ENABLE_ASR32_SERVICE_API) */

/** \brief Definition whether AUTOSAR 3.2 service API is enabled **/
#define NVM_ENABLE_ASR32_SERVICE_API                   [!//
[!IF "NvMCommon/NvMServiceAPI/NvMEnableASR32ServiceAPI"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined NVM_ENABLE_ASR40_SERVICE_API) /* To prevent double declaration */
#error NVM_ENABLE_ASR40_SERVICE_API already defined
#endif /* if (defined NVM_ENABLE_ASR40_SERVICE_API) */

/** \brief Definition whether AUTOSAR 4.0 service API is enabled **/
#define NVM_ENABLE_ASR40_SERVICE_API                   [!//
[!IF "NvMCommon/NvMServiceAPI/NvMEnableASR40ServiceAPI"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined NVM_DEFAULT_ASR_SERVICE_API) /* To prevent double declaration */
#error NVM_DEFAULT_ASR_SERVICE_API already defined
#endif /* if (defined NVM_DEFAULT_ASR_SERVICE_API) */

/** \brief Definition of default service API **/
#define NVM_DEFAULT_ASR_SERVICE_API                    [!//
[!IF "NvMCommon/NvMServiceAPI/NvMDefaultASRServiceAPI = 'AUTOSAR_32'"!]NVM_SERVICE_API_ASR32
[!ELSEIF "NvMCommon/NvMServiceAPI/NvMDefaultASRServiceAPI = 'AUTOSAR_40'"!]NVM_SERVICE_API_ASR40
[!ELSE!]NVM_SERVICE_API_NONE
[!ENDIF!][!//
[!ELSE!] [!//
#if (defined NVM_ENABLE_ASR32_SERVICE_API) /* To prevent double declaration */
#error NVM_ENABLE_ASR32_SERVICE_API already defined
#endif /* if (defined NVM_ENABLE_ASR32_SERVICE_API) */

/** \brief Definition whether AUTOSAR 3.2 service API is enabled **/
#define NVM_ENABLE_ASR32_SERVICE_API                   STD_OFF

#if (defined NVM_ENABLE_ASR40_SERVICE_API) /* To prevent double declaration */
#error NVM_ENABLE_ASR40_SERVICE_API already defined
#endif /* if (defined NVM_ENABLE_ASR40_SERVICE_API) */

/** \brief Definition whether AUTOSAR 4.0 service API is enabled **/
#define NVM_ENABLE_ASR40_SERVICE_API                  STD_OFF

#if (defined NVM_DEFAULT_ASR_SERVICE_API) /* To prevent double declaration */
#error NVM_DEFAULT_ASR_SERVICE_API already defined
#endif /* if (defined NVM_DEFAULT_ASR_SERVICE_API) */

/** \brief Definition of default service API **/
#define NVM_DEFAULT_ASR_SERVICE_API                   NVM_SERVICE_API_NONE
[!ENDIF!]
/*================[variables user application data section]================*/
#define NVM_START_CONFIG_DATA_APPL_DATA
#include <MemMap.h>

/** \brief Stores the calculated CRC values in RAM
 **/
[!IF "$maxCrcSize=1"!]
extern VAR(uint8,NVM_APPL_DATA) NvM_CalcCrc_CalcBuffer;
[!ELSEIF "$maxCrcSize=2"!]
extern VAR(uint16,NVM_APPL_DATA) NvM_CalcCrc_CalcBuffer;
[!ELSE!]
extern VAR(uint32,NVM_APPL_DATA) NvM_CalcCrc_CalcBuffer;
[!ENDIF!]

#define NVM_STOP_CONFIG_DATA_APPL_DATA
#include <MemMap.h>
/*===============[end of variables user application data section]==========*/

/*==================[external function declarations]========================*/

#define NVM_START_SEC_CODE
#include <MemMap.h>

[!IF "NvMCommon/NvMReadBlockHook = 'true'"!][!//
/** \brief Hook function for ReadBlock
 **
 **        The NvM_ReadBlockHook() provides a pre-read hook functionality.
 **        After reading the data from the underlying layer, the hook function
 **        is called.
 **
 ** \param[in] BlockNum The block identifier of the currently processed block.
 ** \param[in] RamBlockDataAddress Pointer to a permanent RAM block. If
 **            no permanent RAM block is configured, it returns NULL_PTR
 ** \param[in] BlockLength Length of the currently processed block
 **
 ** \Synchronicity{Synchronous} */
extern FUNC(void, NVM_CODE) NvM_ReadBlockHook
(
  VAR(uint16, AUTOMATIC)                 BlockNum,
  P2VAR(uint8, AUTOMATIC, NVM_APPL_DATA) RamBlockDataAddress,
  VAR(uint16, AUTOMATIC)                 BlockLength
);

[!ENDIF!][!//
[!IF "NvMCommon/NvMWriteBlockHook = 'true'"!][!//
/** \brief Hook function for WriteBlock
 **
 **        The NvM_WriteBlockHook() provides a post-write hook functionality.
 **        Before writing the data to the underlying layer, the hook function is
 **        called.
 **
 ** \param[in] BlockNum The block identifier of the currently processed block.
 ** \param[in] RamBlockDataAddress Pointer to a permanent RAM block. If
 **            no permanent RAM block is configured, it returns NULL_PTR
 ** \param[in] BlockLength Length of the currently processed block
 **
 ** \Synchronicity{Synchronous} */
extern FUNC(void, NVM_CODE) NvM_WriteBlockHook
(
  VAR(uint16, AUTOMATIC)                 BlockNum,
  P2VAR(uint8, AUTOMATIC, NVM_APPL_DATA) RamBlockDataAddress,
  VAR(uint16, AUTOMATIC)                 BlockLength
);
[!ENDIF!][!//

#define NVM_STOP_SEC_CODE
#include <MemMap.h>

/*------------------[Defensive programming]---------------------------------*/
[!SELECT "NvMDefensiveProgramming"!][!//

#if (defined NVM_DEFENSIVE_PROGRAMMING_ENABLED)
#error NVM_DEFENSIVE_PROGRAMMING_ENABLED is already defined
#endif
/** \brief Defensive programming usage
 **
 ** En- or disables the usage of the defensive programming */
#define NVM_DEFENSIVE_PROGRAMMING_ENABLED   [!//
[!IF "(../NvMCommon/NvMDevErrorDetect  = 'true') and (NvMDefProgEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined NVM_PRECONDITION_ASSERT_ENABLED)
#error NVM_PRECONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Precondition assertion usage
 **
 ** En- or disables the usage of precondition assertion checks */
#define NVM_PRECONDITION_ASSERT_ENABLED     [!//
[!IF "(../NvMCommon/NvMDevErrorDetect  = 'true') and (NvMDefProgEnabled = 'true') and (NvMPrecondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined NVM_POSTCONDITION_ASSERT_ENABLED)
#error NVM_POSTCONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Postcondition assertion usage
 **
 ** En- or disables the usage of postcondition assertion checks */
#define NVM_POSTCONDITION_ASSERT_ENABLED    [!//
[!IF "(../NvMCommon/NvMDevErrorDetect  = 'true') and (NvMDefProgEnabled = 'true') and (NvMPostcondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined NVM_UNREACHABLE_CODE_ASSERT_ENABLED)
#error NVM_UNREACHABLE_CODE_ASSERT_ENABLED is already defined
#endif
/** \brief Unreachable code assertion usage
 **
 ** En- or disables the usage of unreachable code assertion checks */
#define NVM_UNREACHABLE_CODE_ASSERT_ENABLED [!//
[!IF "(../NvMCommon/NvMDevErrorDetect  = 'true') and (NvMDefProgEnabled = 'true') and (NvMUnreachAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined NVM_INVARIANT_ASSERT_ENABLED)
#error NVM_INVARIANT_ASSERT_ENABLED is already defined
#endif
/** \brief Invariant assertion usage
 **
 ** En- or disables the usage of invariant assertion checks */
#define NVM_INVARIANT_ASSERT_ENABLED        [!//
[!IF "(../NvMCommon/NvMDevErrorDetect  = 'true') and (NvMDefProgEnabled = 'true') and (NvMInvariantAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined NVM_STATIC_ASSERT_ENABLED)
#error NVM_STATIC_ASSERT_ENABLED is already defined
#endif
/** \brief Static assertion usage
 **
 ** En- or disables the usage of static assertion checks */
#define NVM_STATIC_ASSERT_ENABLED           [!//
[!IF "(../NvMCommon/NvMDevErrorDetect  = 'true') and (NvMDefProgEnabled = 'true') and (NvMStaticAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDSELECT!][!//

/*==================[ end of external function declarations]================*/
#endif /* if !defined( NVM_CFG_H ) */
/*==================[end of file NvM_Cfg.h]==================================*/
