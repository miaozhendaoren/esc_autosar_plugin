/**
 * \file
 *
 * \brief AUTOSAR NvM
 *
 * This file contains the implementation of the AUTOSAR
 * module NvM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!INCLUDE "../include/NvM_Include.m"!][!//

/*
 *  MISRA-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule 19.1
 *           #include statements in a file should only be preceded by other
 *           preprocessor directives or comments.
 *           Reason:
 *           The statements '#define NVM_STOP_SEC_CODE - #include <MemMap.>' at
 *           the end of a code section are required by the AUTOSAR memory mapping
 *           specification which must be implemented for NvM.
 *
 *  MISRA-2) Deviated Rule 11.1
 *           Conversions shall not be performed between a pointer to a function
 *           and any type other than an integral type.
 *           Reason:
 *           The conversion is necessary to initialize the constants with
 *           NULL_PTR.
 *
 *  MISRA-3) Deviated Rule 19.13 (advisory)
 *           The # and ## preprocessor operators should not be used.
 *           Reason:
 *           Concatenation(##) operator is used by pre compile time parameter.It doesn't 
 *           have any negative impact on code. 
 *
 *  MISRA-4) Deviated Rule 13.7 (required)
 *           Boolean operations whose results are invariant shall not be permitted.
 *           Reason:
 *           The portion of code is necessary and has no side effects.
 *
 */

/*==================[inclusions]=============================================*/

#include <TSAutosar.h>

/* prevent redundant declarations of RTE types */
#define NVM_INTERNAL_USE
/* !LINKSTO NVM554,1 */
#include <NvM.h>
#include <NvM_FunctionIntern.h> /* for initialization of NvM_CalcCrc */
#include <NvM_DataIntern.h>   /* extern definition of NvM_ConfigurationId */
#include <NvM_CalcCrc.h>      /* Extern definitions of unit 'CalcCrc'. */
#include <NvM_UsrHdr.h>

#if (NVM_INCLUDE_RTE == STD_ON)
# include <Rte_NvM.h>
# include <Rte_NvMData.h>
#endif

/*==================[macros]================================================*/

[!IF "count(NvMBlockDescriptor/*[NvMNvramBlockIdentifier > 1 and NvMExtraBlockChecks = 'true']) > 0"!][!//
/** \brief Concatenate the arguments X and Y after they are expanded by the
 ** preprocessor.
 **
 ** These are just helper macros used in the definition of NVM_ASSERT_STC().
 **/
#define NVM_JOIN(X,Y)  NVM_JOIN1(X,Y)
/* Deviation MISRA-3 */
#define NVM_JOIN1(X,Y) X##Y

/** \brief Check static assertion at compile time
 **
 ** \param expr logical expression to be evaluated at compile time
 **
 ** If \a expr evaluates to FALSE the compiler will abort with an error
 ** because array with a negative number of elements are forbidden in C.  If
 ** \a expr is TRUE the macro expands to a legal typedef.
 **
 ** The usage of this macro does \e not lead to additional resource usage in
 ** the resulting binary code.
 **
 ** The macro should be used were preprocessor #if directives cannot evaluate
 ** expressions but where these expressions can still be evaluated before
 ** runtime. E.g. expressions containig sizeof() functions used on types may
 ** be checked: NVM_ASSERT_STC(sizeof(Mod_SomeType) < 16U)
 **
 ** \note Macro may only be used at places in the code where typedefs are
 ** allowed.
 **/
/* Deviation MISRA-3 <+2> */
#define NVM_ASSERT_STC(expr)\
 typedef uint8 NVM_JOIN(AssertFailedInLine,__LINE__)[(expr)?1:-1]

[!LOOP "NvMBlockDescriptor/*[NvMNvramBlockIdentifier > 1 and NvMExtraBlockChecks = 'true']"!][!//
[!IF "(node:exists(NvMRamBlockDataAddress)) and (NvMRamBlockDataAddress != '')"!][!//
/* BlockIdentifier: [!"NvMNvramBlockIdentifier"!] - RamBlockDataAddress: [!"NvMRamBlockDataAddress"!] */
/* Length = [!"NvMNvBlockLength"!]U */
/* Deviation MISRA-4 <+2> */
NVM_ASSERT_STC([!"NvMNvBlockLength"!]U == sizeof([!"text:replace(text:replace(NvMRamBlockDataAddress,'^\&',''),'\[[0-9]+\]$','')"!]));
[!ENDIF!][!//
[!IF "(node:exists(NvMRomBlockDataAddress)) and (NvMRomBlockDataAddress != '')"!][!//
/* BlockIdentifier: [!"NvMNvramBlockIdentifier"!] - RomBlockDataAddress: [!"NvMRomBlockDataAddress"!] */
/* Length = [!"NvMNvBlockLength"!]U */
/* Deviation MISRA-4 <+2> */
NVM_ASSERT_STC([!"NvMNvBlockLength"!]U == sizeof([!"text:replace(text:replace(NvMRomBlockDataAddress,'^\&',''),'\[[0-9]+\]$','')"!]));
[!ENDIF!][!//
[!ENDLOOP!][!//

[!ENDIF!][!//

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

#define NVM_START_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>
/* !LINKSTO NVM138,1, NVM143,1, NVM443,1, NVM321,1 ,NVM140,1 */
CONST(NvM_BlockDescriptorType,NVM_CONST) NvM_BlockDescriptorTable[NVM_TOTAL_NUMBER_OF_NVRAM_BLOCKS]=
{
  /** \brief  block 0:  NvM internal block, used to manage multi block requests. */[!/*
   *   Configuration: Block 0 must not be configurable by the user.
   *   The following default values must be set automatically.
*/!]
  {
    NULL_PTR,    /* ramBlockDataAddress */
    NULL_PTR,    /* romBlockDataAddress */
    NULL_PTR,    /* initBlockCallback */
    NULL_PTR,    /* singleBlockCallback */[!/*
    */!][!IF "count(NvMBlockDescriptor/*[(NvMBlockUseSyncMechanism = 'true')]) != 0"!][!/*
    */!]NULL_PTR,    /* readRamFromNvCallback */
    NULL_PTR,    /* writeRamToNvCallback */[!/*
    */!][!ENDIF!]
    0U,  /* blockDesc */
    0U,  /* BlockBaseNumber */
    0U,  /* nvBlockLength */
    0xFFFFU,  /* ramBlockCrcIndex */[!/*
    */!][!IF "count(NvMBlockDescriptor/*[(NvMWriteVerification = 'true')]) != 0"!]
    0U,   /* verifyDataSize */[!/*
    */!][!ENDIF!]
    0U,  /* nvBlockNum */
    0U,  /* romBlockNum */[!/*
    */!][!IF "NvMCommon/NvMJobPrioritization = 'true'"!]
    0U,  /* blockJobPriority */[!/*
    */!][!ENDIF!][!/*
    */!][!IF "count(NvMBlockDescriptor/*[(NvMMaxNumOfWriteRetries > 0)]) != 0"!]
    0U,  /* writeRetryLimit */[!/*
    */!][!ENDIF!][!/*
    */!][!IF "count(NvMBlockDescriptor/*[(NvMMaxNumOfReadRetries > 0)]) != 0"!]
    0U  /* readRetryLimit */[!/*
    */!][!ENDIF!]
  }[!/*
   */!][!VAR "CRCLength"="0"!][!/*
   */!][!LOOP "node:order(NvMBlockDescriptor/*, 'NvMNvramBlockIdentifier')"!],

  /* NVM_BLOCK_[!"name(.)"!] */[!/*
   */!]
  {[!/*
    */!]
    [!IF "(node:exists(NvMRamBlockDataAddress)) and (NvMRamBlockDataAddress != '')"!][!"NvMRamBlockDataAddress"!][!ELSE!]NULL_PTR[!ENDIF!], /* ramBlockDataAddress */
    [!IF "(node:exists(NvMRomBlockDataAddress)) and (NvMRomBlockDataAddress != '')"!][!"NvMRomBlockDataAddress"!][!ELSE!]NULL_PTR[!ENDIF!], /* romBlockDataAddress */
    [!IF "((NvMProvideRteInitBlockPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and (../../NvMCommon/NvMServiceAPI/NvMDefaultASRServiceAPI != 'NONE'))"!][!/*
       */!]&Rte_Call_PNInitBlock_[!"name(.)"!]_InitBlock[!/*
       */!][!ELSEIF "((NvMProvideRteInitBlockPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and(../../NvMCommon/NvMServiceAPI/NvMEnableASR40ServiceAPI = 'true'))"!][!/*
       */!]&Rte_Call_NvM_PNInitBlock_ASR40_[!"name(.)"!]_InitBlock[!/*
       */!][!ELSEIF "((NvMProvideRteInitBlockPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and(../../NvMCommon/NvMServiceAPI/NvMEnableASR32ServiceAPI = 'true'))"!][!/*
       */!]&Rte_Call_NvM_PNInitBlock_ASR32_[!"name(.)"!]_InitBlock[!/*
       */!][!ELSEIF "(node:exists(NvMInitBlockCallback)) and (NvMInitBlockCallback != '')"!][!/*
     */!]&[!"NvMInitBlockCallback"!][!/*
    */!][!/*
    */!][!ELSE!][!/*
       */!]NULL_PTR[!/*
    */!][!ENDIF!], /* initBlockCallback */
    [!IF "((NvMProvideRteJobFinishedPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and (../../NvMCommon/NvMServiceAPI/NvMDefaultASRServiceAPI != 'NONE'))"!][!/*
       */!]&Rte_Call_PNJobFinished_[!"name(.)"!]_JobFinished[!/*
       */!][!ELSEIF "((NvMProvideRteJobFinishedPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and (../../NvMCommon/NvMServiceAPI/NvMEnableASR40ServiceAPI = 'true'))"!][!/*
       */!]&Rte_Call_NvM_PNJobFinished_ASR40_[!"name(.)"!]_JobFinished[!/*
       */!][!ELSEIF "((NvMProvideRteJobFinishedPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and(../../NvMCommon/NvMServiceAPI/NvMEnableASR32ServiceAPI = 'true'))"!][!/*
       */!]&Rte_Call_NvM_PNJobFinished_ASR32_[!"name(.)"!]_JobFinished[!/*
    */!][!ELSEIF "(node:exists(NvMSingleBlockCallback)) and (NvMSingleBlockCallback != '')"!][!/*
     */!]&[!"NvMSingleBlockCallback"!][!/*
    */!][!/*
    */!][!ELSE!][!/*
       */!]NULL_PTR[!/*
    */!][!ENDIF!], /* singleBlockCallback */[!/*
    */!][!IF "count(../../NvMBlockDescriptor/*[(NvMBlockUseSyncMechanism = 'true')]) != 0"!]
    [!IF "((NvMProvideRteMirrorPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and(../../NvMCommon/NvMServiceAPI/NvMDefaultASRServiceAPI != 'NONE'))"!][!/*
       */!]&Rte_Call_PMirror_[!"name(.)"!]_ReadRamBlockFromNvm[!/*
       */!][!ELSEIF "((NvMProvideRteMirrorPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and(../../NvMCommon/NvMServiceAPI/NvMEnableASR40ServiceAPI = 'true'))"!][!/*
       */!]&Rte_Call_NvM_PMirror_ASR40_[!"name(.)"!]_ReadRamBlockFromNvm[!/*
       */!][!ELSEIF "((NvMProvideRteMirrorPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and(../../NvMCommon/NvMServiceAPI/NvMEnableASR32ServiceAPI = 'true'))"!][!/*
       */!]&Rte_Call_NvM_PMirror_ASR32_[!"name(.)"!]_ReadRamBlockFromNvm[!/*
       */!][!ELSEIF "not(node:empty(NvMReadRamBlockFromNvCallback))"!][!/*
       */!]&[!"NvMReadRamBlockFromNvCallback"!][!/*
       */!][!/*
    */!][!ELSE!][!/*
       */!]NULL_PTR[!/*
    */!][!ENDIF!], /* readRamFromNvCallback */
    [!IF "((NvMProvideRteMirrorPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and (../../NvMCommon/NvMServiceAPI/NvMDefaultASRServiceAPI != 'NONE'))"!][!/*
       */!]&Rte_Call_PMirror_[!"name(.)"!]_WriteRamBlockToNvm[!/*
       */!][!ELSEIF "((NvMProvideRteMirrorPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and (../../NvMCommon/NvMServiceAPI/NvMEnableASR40ServiceAPI = 'true'))"!][!/*
       */!]&Rte_Call_NvM_PMirror_ASR40_[!"name(.)"!]_WriteRamBlockToNvm[!/*
       */!][!ELSEIF "((NvMProvideRteMirrorPort = 'true') and (node:exists(../../NvMCommon/NvMServiceAPI)) and (../../NvMCommon/NvMServiceAPI/NvMEnableASR32ServiceAPI = 'true'))"!][!/*
       */!]&Rte_Call_NvM_PMirror_ASR32_[!"name(.)"!]_WriteRamBlockToNvm[!/*
       */!][!ELSEIF "not(node:empty(NvMWriteRamBlockToNvCallback))"!][!/*
       */!]&[!"NvMWriteRamBlockToNvCallback"!][!/*
    */!][!/*
    */!][!ELSE!][!/*
       */!]NULL_PTR[!/*
    */!][!ENDIF!], /* writeRamToNvCallback */[!/*
    */!][!ENDIF!][!/*
    === Caclulate blockDesc ==    */!]
    [!VAR "blockDesc"="0"!][!/*
    */!][!IF "NvMBlockUseCrc = 'true'"!][!/*
    */!][!IF "(node:exists(NvMCalcRamBlockCrc)) and (NvMCalcRamBlockCrc = 'true')"!][!VAR "blockDesc"="$blockDesc + 8"!][!ENDIF!][!/*
    */!][!IF "(node:exists(NvMBlockCrcType)) and (NvMBlockCrcType = 'NVM_CRC32')"!][!VAR "blockDesc"="$blockDesc + 3"!][!/*
    */!][!ELSEIF "(node:exists(NvMBlockCrcType)) and (NvMBlockCrcType = 'NVM_CRC16')"!][!VAR "blockDesc"="$blockDesc + 2"!][!/*
    */!][!ELSEIF "(node:exists(NvMBlockCrcType)) and (NvMBlockCrcType = 'NVM_CRC8')"!][!VAR "blockDesc"="$blockDesc + 1"!][!ENDIF!][!/*
    */!][!ENDIF!][!/*
    */!][!IF "NvMBlockWriteProt = 'true'"!][!VAR "blockDesc"="$blockDesc + 16"!][!ENDIF!][!/*
    */!][!IF "NvMWriteBlockOnce = 'true'"!][!VAR "blockDesc"="$blockDesc + 32"!][!ENDIF!][!/*
    */!][!IF "NvMResistantToChangedSw = 'true'"!][!VAR "blockDesc"="$blockDesc + 64"!][!ENDIF!][!/*
    */!][!IF "NvMBlockManagementType = 'NVM_BLOCK_REDUNDANT'"!][!VAR "blockDesc"="$blockDesc + 128"!][!ENDIF!][!/*
    */!][!IF "NvMBlockManagementType = 'NVM_BLOCK_DATASET'"!][!VAR "blockDesc"="$blockDesc + 256"!][!ENDIF!][!/*
    */!][!IF "(node:exists(NvMSelectBlockForReadAll)) and (NvMSelectBlockForReadAll = 'true')"!][!VAR "blockDesc"="$blockDesc + 512"!][!ENDIF!][!/*
    */!][!IF "(node:exists(NvMSelectBlockForWriteAll)) and (NvMSelectBlockForWriteAll = 'true')"!][!VAR "blockDesc"="$blockDesc + 262144"!][!ENDIF!][!/*
    */!][!VAR "blockDesc"="$blockDesc +  bit:shl(NvMNvramDeviceId, 10)"!][!/*
    === 'Use internal buffer' flag : Set Bit 13 (+8192) if internal buffer shall be used ===
    Internal buffer shall be used if Explicit synchronization is disabled and
    1. Crc is enabled but is not stored inside the RAM block.
    2. Write Verification or Static BlockID Check is enabled.
    */!][!IF "(NvMBlockUseSyncMechanism = 'false') and
              (((NvMBlockUseCrc = 'true') and (NvMUserProvidesSpaceForBlockAndCrc != 'true')) or
               (NvMWriteVerification = 'true') or (NvMStaticBlockIDCheck = 'true'))"!][!/*
    */!][!VAR "blockDesc"="$blockDesc + 8192"!][!ENDIF!][!/*
    === 'Advanced Recovery' flag : Set Bit 14 (+16384) if Advanced Recovery shall be used === */!][!/*
    */!][!IF "NvMAdvancedRecovery = 'true'"!][!VAR "blockDesc"="$blockDesc + 16384"!][!ENDIF!][!/*
    === 'Write Verification' flag : Set Bit 15 (+32768) if Write Verification shall be used === */!][!/*
    */!][!IF "NvMWriteVerification = 'true'"!][!VAR "blockDesc"="$blockDesc + 32768"!][!ENDIF!][!/*
    === 'Static Block Id' flag : Set Bit 16 (+65536) if Static Block Id shall be used === */!][!/*
    */!][!IF "NvMStaticBlockIDCheck = 'true'"!][!VAR "blockDesc"="$blockDesc + 65536"!][!ENDIF!][!/*
    === 'Use Mirror' flag : Set Bit 17 (+131072) if Explicit Synchronization shall be used === */!][!/*
    */!][!IF "NvMBlockUseSyncMechanism = 'true'"!][!VAR "blockDesc"="$blockDesc + 131072"!][!ENDIF!][!/*
    === 'Job status info' flag : Set Bit 19 (+524288) if BswM Block
    status information shall be used === */!][!/*
    */!][!IF "NvMBswMBlockStatusInformation = 'true'"!][!VAR "blockDesc"="$blockDesc + 524288"!][!ENDIF!][!/*
    */!][!"num:inttohex($blockDesc,8)"!]U,  /* blockDesc */
    [!"NvMNvBlockBaseNumber"!]U,  /* nvBlockBaseNumber */
    [!"NvMNvBlockLength"!]U,  /* nvBlockLength */
    [!IF "(node:exists(NvMCalcRamBlockCrc)) and (NvMBlockUseCrc = 'true') and (NvMCalcRamBlockCrc = 'true')"!][!/*
    */!][!"num:i($CRCLength)"!]U[!/*
    */!][!IF "(node:exists(NvMBlockCrcType)) and (NvMBlockCrcType = 'NVM_CRC8')"!][!/*
      */!][!VAR "CRCLength"="$CRCLength + 1"!][!/*
    */!][!ELSEIF "(node:exists(NvMBlockCrcType)) and (NvMBlockCrcType = 'NVM_CRC16')"!][!/*
      */!][!VAR "CRCLength"="$CRCLength + 2"!][!/*
      */!][!ELSEIF "(node:exists(NvMBlockCrcType)) and (NvMBlockCrcType = 'NVM_CRC32')"!][!/*
      */!][!VAR "CRCLength"="$CRCLength + 4"!][!/*
    */!][!ENDIF!][!/*
    */!][!ELSE!][!/*
    */!]0xFFFFU[!/*
    */!][!ENDIF!], /* ramBlockCrcIndex */
    [!IF "count(../../NvMBlockDescriptor/*[(NvMWriteVerification = 'true')]) != 0"!][!/*
    */!][!IF "node:exists(NvMWriteVerificationDataSize)"!][!/*
    */!][!"num:i(NvMWriteVerificationDataSize)"!]U,  /* verifyDataSize */[!/*
    */!][!ELSE!][!/*
    */!]1U,  /* verifyDataSize */
    [!ENDIF!][!/*
    */!][!ENDIF!]
    [!"num:i(NvMNvBlockNum)"!]U, /* nvBlockNum */
    [!"num:i(NvMRomBlockNum)"!]U, /* romBlockNum */
    [!IF "../../NvMCommon/NvMJobPrioritization = 'true'"!][!/*
    */!][!"num:i(NvMBlockJobPriority)"!]U, /* blockJobPriority */
    [!ENDIF!][!/*
    */!][!IF "count(../../NvMBlockDescriptor/*[(NvMMaxNumOfWriteRetries > 0)]) != 0"!][!/*
    */!][!"num:i(NvMMaxNumOfWriteRetries)"!]U,  /* writeRetryLimit */
    [!ENDIF!][!/*
    */!][!IF "count(../../NvMBlockDescriptor/*[(NvMMaxNumOfReadRetries > 0)]) != 0"!][!/*
    */!][!"NvMMaxNumOfReadRetries"!]U  /* readRetryLimit */[!/*
    */!][!ENDIF!]
  }[!/*
*/!][!ENDLOOP!]
}; /* NvM_BlockDescriptorTable */

#define NVM_STOP_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

#define NVM_START_CONFIG_DATA_APPL_DATA
#include <MemMap.h>

[!IF "$maxCrcSize=1"!]
VAR(uint8,NVM_APPL_DATA) NvM_CalcCrc_CalcBuffer;
[!ELSEIF "$maxCrcSize=2"!]
VAR(uint16,NVM_APPL_DATA) NvM_CalcCrc_CalcBuffer;
[!ELSE!]
VAR(uint32,NVM_APPL_DATA) NvM_CalcCrc_CalcBuffer;
[!ENDIF!]

#define NVM_STOP_CONFIG_DATA_APPL_DATA
#include <MemMap.h>

#define NVM_START_CONFIG_DATA_APPL_CONST
#include <MemMap.h>
/* !LINKSTO NVM034,1 */
CONST(uint16,NVM_APPL_CONST) NvM_CompiledConfigurationId = [!"num:inttohex(NvMCommon/NvMCompiledConfigId)"!]U;

#define NVM_STOP_CONFIG_DATA_APPL_CONST
#include <MemMap.h>

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

/*==================[end of file]============================================*/
