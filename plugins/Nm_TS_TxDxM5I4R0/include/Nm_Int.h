#if (!defined NM_INT_H)
#define NM_INT_H

/**
 * \file
 *
 * \brief AUTOSAR Nm
 *
 * This file contains the implementation of the AUTOSAR
 * module Nm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]============================================*/

#include <Nm.h>                 /* Nm public API incl. module config. */

#if (NM_DEV_ERROR_DETECT == STD_ON)
/* !LINKSTO Nm243,1 */
#include <Det.h>               /* Det API */
#endif

/*==================[macros]================================================*/

/** \brief Coordination cluster index special value for channels which do not
 * belong to a coordination cluster */
#define NM_CC_IDX_INVALID   0xFFU

/** \brief Macro defines for setting Nm-channel-status as
 **        Nm-channel is requested by ComM */
#define NM_CHANNEL_REQUESTED_BY_COMM 0x01U
/** \brief Macro defines for setting Nm-channel-status as
 **        Nm-channel is not requested by ComM */
#define NM_NOT_CHANNEL_REQUESTED_BY_COMM 0xfeU

/** \brief Macro defines for setting Nm-channel-status as
 **        Nm-channel is participating in the coordinated shutdown */
#define NM_CHANNEL_PARTICIPATING_IN_CLUSTER 0x02U
/** \brief Macro defines for setting Nm-channel-status as
 **        Nm-channel is not participating in the coordinated shutdown */
#define NM_NOT_CHANNEL_PARTICIPATING_IN_CLUSTER 0xfdU

/** \brief Macro defines for setting Nm-channel-status as
 **        Nm-channel is asleep */
#define NM_CHANNEL_ASLEEP 0x04U
/** \brief Macro defines for setting Nm-channel-status as
 **        Nm-channel is not asleep */
#define NM_NOT_CHANNEL_ASLEEP 0xfbU

/** \brief Macro defines for setting Nm-channel-status as channel
 **        has no active remote nodes - all remote nodes are inactive */
#define NM_CHANNEL_REMOTE_NODES_INACTIVE 0x08U
/** \brief Macro defines for setting Nm-channel-status as channel
 **        has active remote nodes - some remote nodes are active */
#define NM_NOT_CHANNEL_REMOTE_NODES_INACTIVE 0xf7U

/** \brief Id of instance of Nm module */
#define NM_INSTANCE_ID     0U

/* define various macros used for development error reporting, if development
 * error detection is enabled */
#if (NM_DEV_ERROR_DETECT == STD_ON)

/** \brief Macro for reporting an error to Det
 **
 ** \param[in] SID Service ID of the API function
 ** \param[in] ERROR_CODE Error code reported to Det module */
#define NM_DET_REPORT_ERROR(SID,ERROR_CODE)                            \
  (void)Det_ReportError(NM_MODULE_ID, NM_INSTANCE_ID, (SID), (ERROR_CODE))

#endif /* if (NM_DEV_ERROR_DETECT == STD_ON) */

#if (defined NM_IS_CHANNEL)
#error NM_IS_CHANNEL is already defined
#endif

/** \brief Answers if channel \p handle is in status \p status
 **
 ** \param[in] handle the channel to test
 ** \param[in] the status to ask for */
#define NM_IS_CHANNEL(handle, status)  \
  ((Nm_ChannelStatus[(Nm_ChanIdxFromNetworkHandle[(handle)])] & (status)) != 0U)

/** \brief Answers if channel \p handle is not in status \p status
 **
 ** \param[in] handle the channel to test
 ** \param[in] status the status to ask for */
#define NM_IS_NOT_CHANNEL(handle, status) \
  ((Nm_ChannelStatus[(Nm_ChanIdxFromNetworkHandle[(handle)])] & (status)) == 0U)

/** \brief resets the channel \p handle to the value given by \p status
 **
 ** \param[in] handle the channel to reset
 ** \param[in] status the value to reset with */
#define NM_RESET_CHANNEL(handle, status) \
  (Nm_ChannelStatus[(Nm_ChanIdxFromNetworkHandle[(handle)])] = (status))

/** \brief sets the \p status of channel \p handle
 **
 ** \param[in] handle the channel to set
 ** \param[in] status the status to set */
#define NM_SET_CHANNEL(handle, status) \
  (Nm_ChannelStatus[(Nm_ChanIdxFromNetworkHandle[(handle)])] |= (status))

/** \brief unsets the \p status of channel \p handle
 **
 ** \param[in] handle the channel to unset
 ** \param[in] status the status to unset */
#define NM_UNSET_CHANNEL(handle, status) \
  (Nm_ChannelStatus[(Nm_ChanIdxFromNetworkHandle[(handle)])] &= (status))

/** \brief Macro definition to access the
 ** correct channel for the NetworkHandle */
#define NM_CHANNEL_CONFIG(NetworkHandle) \
   Nm_ChannelConfig[Nm_ChanIdxFromNetworkHandle[(NetworkHandle)]]

/** \brief Special Value for the undefined channels */
#define NM_INVALID_CHANNEL_ID      0xFFU

/** \brief Macro definition to check the undefined Network Handle */
#define NM_IS_NETWORK_HANDLE_VALID(NetworkHandle) \
          (((NetworkHandle) > NM_MAX_CHANNELID) || \
           (Nm_ChanIdxFromNetworkHandle[(NetworkHandle)] == NM_INVALID_CHANNEL_ID))

/*---------------[Network Management State(NMS) defines]-----------------*/
#if (NM_STATE_CHANGE_IND_ENABLED == STD_ON)

/** \brief Macro to identify call to Nm_StateChangeNotification() for a
 * transition which does not requires Com indication */
#define NM_NMS_RESET                0U

#if !(defined NM_RM_BSM)
/** \brief Macro to indicate Network Management State (NMS)
 * NM in state RepeatMessage (transition from BusSleepMode) */
#define NM_RM_BSM                   1U
#endif
#if !(defined NM_RM_PBSM)
/** \brief Macro to indicate Network Management State (NMS)
 * NM in state RepeatMessage (transition from PrepareBusSleepMode) */
#define NM_RM_PBSM                  2U
#endif
#if !(defined NM_NO_RM)
/** \brief Macro to indicate Network Management State (NMS)
 * NM in state NormalOperation (transition from RepeatMessage) */
#define NM_NO_RM                    4U
#endif
#if !(defined NM_NO_RS)
/** \brief Macro to indicate Network Management State (NMS)
 * NM in state NormalOperation (transition from ReadySleep) */
#define NM_NO_RS                    8U
#endif
#if !(defined NM_RM_RS)
/** \brief Macro to indicate Network Management State (NMS)
 * NM in state RepeatMessage (transition from ReadySleep) */
#define NM_RM_RS                    16U
#endif
#if !(defined NM_RM_NO)
/** \brief Macro to indicate Network Management State (NMS)
 * NM in state RepeatMessage (transition from NormalOperation) */
#define NM_RM_NO                    32U
#endif
/** \brief Macro to indicate no ComSignal reference exists
 ** for a particular channel */
#define NM_COM_SIGNAL_INVALID       0xFFFFU

#endif

/*==================[type definitions]======================================*/

#if (NM_COORDINATOR_SUPPORT_ENABLED == STD_ON)

/** \brief States of single coordination cluster */
typedef enum
{
  /** wait for the shutdown conditions to become true.  This is checked in all
   * relevant Nm functions syncronously. */
  NM_CC_STATE_WAIT_FOR_SHUTDOWN,
  /** wait for zynchronization if a synchronizing network is part of the cluster */
  NM_CC_STATE_WAIT_ZYNCHRONIZATION,
  /** Timers are initialized for the shutdown */
  NM_CC_STATE_PREPARE_SHUTDOWN,
  /** The shutdown sequence is currently running */
  NM_CC_STATE_SHUTDOWN_INITIATED,
  /** A shudown was aborted and the conditions are rechecked in the next main
   * function call. */
  NM_CC_STATE_SHUTDOWN_ABORTED
} Nm_CcStateType;

#endif /* NM_COORDINATOR_SUPPORT_ENABLED == STD_ON */

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

#if (NM_COORDINATOR_SUPPORT_ENABLED == STD_ON)

#define NM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief Pointer to array of NM channels of a cluster */
extern CONSTP2CONST(NetworkHandleType, NM_CONST, NM_CONST)
  Nm_CcNmChannels[NM_CC_NUM];

#define NM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

#define NM_START_SEC_CONST_8
#include <MemMap.h>

/** \brief Number of NM channels in a coordinated cluster */
extern CONST(uint8, NM_CONST)
  Nm_CcNmChannelsNum[NM_CC_NUM];

#define NM_STOP_SEC_CONST_8
#include <MemMap.h>

#endif

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

#if ((NM_DEV_ERROR_DETECT == STD_ON) || (NM_COORDINATOR_SUPPORT_ENABLED == STD_ON))

#define NM_START_SEC_VAR_INIT_8
#include <MemMap.h>

/** \brief Intialization information of Nm module */
extern VAR(boolean, NM_VAR) Nm_Initialized;

#define NM_STOP_SEC_VAR_INIT_8
#include <MemMap.h>

#endif

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
#endif /* if !defined( NM_INT_H ) */
