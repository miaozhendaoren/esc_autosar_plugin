#if (!defined NM_CFG_H)
#define NM_CFG_H
/**
 * \file
 *
 * \brief AUTOSAR Nm
 *
 * This file contains the implementation of the AUTOSAR
 * module Nm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

[!AUTOSPACING!][!//
[!INCLUDE "Nm_Cfg.m"!][!//
[!SELECT "NmGlobalConfig/NmGlobalProperties"!][!//

/*==================[includes]==============================================*/

#include <NmStack_Types.h>  /* Nm stack specific types, includes StdTypes.h */
[!IF "count(../../NmChannelConfig/*[NmStateReportEnabled = 'true']) != 0"!][!//
#include <Com.h>        /* Com interface */
[!ENDIF!][!//

/* Include header files for Generic NMs*/
[!IF "$GenericNmEnabled = 'true'"!][!//
[!FOR "arrIndx" = "1" TO "count(text:split($GenericUniqNm))"!][!//
#include<[!"text:split($GenericUniqNm)[position() = $arrIndx]"!].h>
[!ENDFOR!][!//
[!ENDIF!][!//

/*==================[macros]================================================*/

/** \brief Switch for DET usage */
#define NM_DEV_ERROR_DETECT            [!//
[!IF "NmDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Provide API function Nm_GetVersionInfo() */
#define NM_VERSION_INFO_API            [!//
[!IF "NmVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!ENDSELECT!][!//
[!//
[!SELECT "NmGlobalConfig/NmGlobalFeatures"!][!//
/** \brief enable support of the Passive Mode. */
#define NM_PASSIVE_MODE_ENABLED        [!//
[!IF "count(../../NmChannelConfig/*[NmPassiveModeEnabled='false']) = 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Provide API for getting/setting user data of Nm messages*/
#define NM_USER_DATA_ENABLED           [!//
[!IF "NmUserDataEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Enable/Disable setting of NMUserData via SW-C */
#define NM_COM_USER_DATA_ENABLED           [!//
[!IF "NmComUserDataSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief enable the source node identifier. */
#define NM_NODE_ID_ENABLED             [!//
[!IF "NmNodeIdEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief enable the Request Repeat Message Request support. */
#define NM_NODE_DETECTION_ENABLED      [!//
[!IF "NmNodeDetectionEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief enable remote sleep indication support. This
 * feature is required for gateway nodes only */
#define NM_REMOTE_SLEEP_IND_ENABLED    [!//
[!IF "NmRemoteSleepIndEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief enable the Communication Control support. */
#define NM_COM_CONTROL_ENABLED         [!//
[!IF "NmComControlEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Enable bus synchronization support */
#define NM_BUS_SYNCHRONIZATION_ENABLED [!//
[!IF "NmBusSynchronizationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Enable Nm coordinator functionality */
#define NM_COORDINATOR_SUPPORT_ENABLED [!//
[!IF "NmCoordinatorSupportEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Enable optional interface Nm_StateChangeNotification() */
#define NM_STATE_CHANGE_IND_ENABLED [!//
[!IF "NmStateChangeIndEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Enable optional interface Nm_PduRxIndication() */
#define NM_PDU_RX_INDICATION_ENABLED [!//
[!IF "NmPduRxIndicationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Enable optional interface Nm_RepeatMessageIndication() */
#define NM_REPEAT_MESSAGE_INDICATION [!//
[!IF "NmRepeatMsgIndEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Enable optional interface Nm_RepeatMessageIndication() */
#define NM_CAR_WAKEUPRX_INDICATION [!//
[!IF "node:exists(NmCarWakeUpRxEnabled) and (NmCarWakeUpRxEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Enable State change notification callout */
#define NM_STATE_CHANGE_NOTIFICATION_CALLOUT [!//
[!IF "node:exists(NmStateChangeNotificationCallout) and (NmStateChangeNotificationCallout = 'true') "!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define NM_STATE_REPORT_ENABLED [!//
[!IF "count(../../NmChannelConfig/*[NmStateReportEnabled = 'true']) != 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!ENDSELECT!][!//

[!//
[!LOOP "NmChannelConfig/*"!][!//
#if (defined NmConf_[!"name(..)"!]_[!"name(.)"!])
#error NmConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif
/** \brief Symbolic name for the NM channel "[!"name(.)"!]" */
#define NmConf_[!"name(..)"!]_[!"name(.)"!]   [!"NmChannelId"!]U

#if (!defined NM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) [!"NmChannelId"!] */
#define [!"name(.)"!]                     [!"NmChannelId"!]U

#if (defined Nm_[!"name(.)"!])
#error Nm_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define Nm_[!"name(.)"!]   [!"NmChannelId"!]U
#endif /* NM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

/** \brief greatest value of configured Nm Channel ID */
#define NM_MAX_CHANNELID [!"num:max(NmChannelConfig/*/NmChannelId)"!]U

/** \brief Number of Nm channels */
#define NM_CHANNEL_NUM                 [!"num:i(count(NmChannelConfig/*))"!]U

[!IF "NmGlobalConfig/NmGlobalFeatures/NmCoordinatorSupportEnabled = 'true' and node:exists(NmChannelConfig/*/NmCoordClusterIndex)"!][!//
[!/* Though this symbol is only used internally we generate it in this
   * header to eliminate the need for a dedicated Nm_Int_Cfg.h file */!][!//
/** \brief Number of coordinated clusters */
#define NM_CC_NUM                      [!//
[!"num:i(num:order(NmChannelConfig/*/NmCoordClusterIndex)[last()] + 1)"!]U
[!/* NmCoordClusterIndex is zero based and consecutive, order the indices and
   * take largest index plus 1 */!][!//
[!ENDIF!][!//

/** \brief Number of used underlying BusNm modules */
#define NM_BUSNM_NUM                   [!"num:i($BusNmNum)"!]U
/** \brief Flag indicating if CanNm is used */
#define NM_CANNM_USED                  [!//
[!IF "$CanNmEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
/** \brief Flag indicating if FrNm is used */
#define NM_FRNM_USED                   [!//
[!IF "$FrNmEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
/** \brief Flag indicating if UdpNm is used */
#define NM_UDPNM_USED                   [!//
[!IF "$UdpNmEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
/** \brief Flag indicating the use of generic NMs */
#define NM_GENERICNM_USED                   [!//
[!IF "$GenericNmEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!SELECT "NmGlobalConfig/NmGlobalFeatures"!]
[!IF "$BusNmNum = 1"!][!//
[!VAR "BusNmName" = "text:split($BusNmList)[position() = 1]"!][!//

#define NM_BUSNM_PASSIVE_START_UP(nmNetworkHandle)      \
  [!"$BusNmName"!]_PassiveStartUp(nmNetworkHandle)
#define NM_BUSNM_GET_STATE(nmNetworkHandle, nmStatePtr, nmModePtr)      \
  [!"$BusNmName"!]_GetState((nmNetworkHandle), (nmStatePtr), (nmModePtr))
[!IF "count(../../NmChannelConfig/*[NmPassiveModeEnabled ='false']) != 0"!][!//
#define NM_BUSNM_NETWORK_REQUEST(NetworkHandle)         \
  [!"$BusNmName"!]_NetworkRequest(NetworkHandle)
#define NM_BUSNM_NETWORK_RELEASE(NetworkHandle)         \
  [!"$BusNmName"!]_NetworkRelease(NetworkHandle)
[!ENDIF!]
[!IF "NmUserDataEnabled = 'true'"!][!//
#define NM_BUSNM_GET_USER_DATA(nmNetworkHandle, nmUserDataPtr)          \
  [!"$BusNmName"!]_GetUserData((nmNetworkHandle), (nmUserDataPtr))
[!IF "(count(../../NmChannelConfig/*[NmPassiveModeEnabled ='false']) != 0) and (NmComUserDataSupport = 'false')"!][!//
#define NM_BUSNM_SET_USER_DATA(NetworkHandle, nmUserDataPtr) \
  [!"$BusNmName"!]_SetUserData((NetworkHandle), (nmUserDataPtr))
[!ENDIF!][!//
[!ENDIF!][!//
[!IF "NmNodeIdEnabled = 'true'"!][!//
#define NM_BUSNM_GET_NODE_IDENTIFIER(NetworkHandle, nmNodeIdPtr)        \
  [!"$BusNmName"!]_GetNodeIdentifier((NetworkHandle), (nmNodeIdPtr))
#define NM_BUSNM_GET_LOCAL_NODE_IDENTIFIER(NetworkHandle, nmNodeIdPtr)  \
  [!"$BusNmName"!]_GetLocalNodeIdentifier((NetworkHandle), (nmNodeIdPtr))
[!ENDIF!][!//
[!IF "NmNodeDetectionEnabled = 'true'"!][!//
#define NM_BUSNM_REPEAT_MESSAGE_REQUEST(NetworkHandle)  \
  [!"$BusNmName"!]_RepeatMessageRequest(NetworkHandle)
[!ENDIF!][!//
[!IF "(NmNodeIdEnabled = 'true') or (NmNodeDetectionEnabled = 'true') or (NmUserDataEnabled = 'true')"!][!//
#define NM_BUSNM_GET_PDU_DATA(NetworkHandle, nmPduData) \
  [!"$BusNmName"!]_GetPduData((NetworkHandle), (nmPduData))
[!ENDIF!][!//

[!IF "NmRemoteSleepIndEnabled = 'true'"!][!//
#define NM_BUSNM_CHECK_REMOTE_SLEEP_INDICATION(                         \
  nmNetworkHandle, nmRemoteSleepIndPtr)                                 \
  [!"$BusNmName"!]_CheckRemoteSleepIndication(                          \
    (nmNetworkHandle), (nmRemoteSleepIndPtr))
[!ENDIF!][!//

[!IF "NmBusSynchronizationEnabled = 'true'"!][!//
#define NM_BUSNM_REQUEST_BUS_SYNCHRONIZATION(NetworkHandle)     \
  [!"$BusNmName"!]_RequestBusSynchronization(NetworkHandle)
[!ENDIF!][!//

[!IF "NmComControlEnabled = 'true'"!][!//
[!IF "($BusNmName = 'CanNm') or ($BusNmName = 'FrNm') "!][!//
#define NM_BUSNM_DISABLE_COMMUNICATION(NetworkHandle)   \
  [!"$BusNmName"!]_DisableCommunication(NetworkHandle)
#define NM_BUSNM_ENABLE_COMMUNICATION(NetworkHandle)    \
  [!"$BusNmName"!]_EnableCommunication(NetworkHandle)
[!ELSE!][!//
#define NM_BUSNM_DISABLE_COMMUNICATION(NetworkHandle)   \
  (TS_PARAM_UNUSED(NetworkHandle), E_NOT_OK)
#define NM_BUSNM_ENABLE_COMMUNICATION(NetworkHandle)    \
  (TS_PARAM_UNUSED(NetworkHandle), E_NOT_OK)
[!ENDIF!][!//
[!ENDIF!][!//

[!ELSE!][!/* $BusNmNum = 1 */!][!//
/* multiple BusNm */

/** \brief Retrieve the index of the BusNm
 **
 ** The macro evaluates to the index of the underlying bus being looked up.
 **
 ** \param[in] NmChannelId Nm Channel ID */
#define NM_GET_BUSNM_IDX(NmChannelId) (Nm_ChannelConfig[(Nm_ChanIdxFromNetworkHandle[(NmChannelId)])].BusNmId)

#define NM_BUSNM_PASSIVE_START_UP(nmNetworkHandle)              \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(nmNetworkHandle)].PassiveStartUp( \
    nmNetworkHandle)
#define NM_BUSNM_GET_STATE(nmNetworkHandle, nmStatePtr, nmModePtr)      \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(nmNetworkHandle)].GetState(               \
    (nmNetworkHandle), (nmStatePtr), (nmModePtr))
[!IF "count(../../NmChannelConfig/*[NmPassiveModeEnabled ='false']) != 0"!]
#define NM_BUSNM_NETWORK_REQUEST(NetworkHandle)               \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].NetworkRequest( \
    NetworkHandle)
#define NM_BUSNM_NETWORK_RELEASE(NetworkHandle)               \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].NetworkRelease( \
    NetworkHandle)
[!ENDIF!][!//
[!IF "NmUserDataEnabled = 'true'"!][!//
#define NM_BUSNM_GET_USER_DATA(nmNetworkHandle, nmUserDataPtr)          \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(nmNetworkHandle)].GetUserData(            \
    (nmNetworkHandle), (nmUserDataPtr))
[!IF "(count(../../NmChannelConfig/*[NmPassiveModeEnabled ='false']) != 0) and (NmComUserDataSupport = 'false')"!]
#define NM_BUSNM_SET_USER_DATA(NetworkHandle, nmUserDataPtr)    \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].SetUserData(      \
    (NetworkHandle), (nmUserDataPtr))
[!ENDIF!][!//
[!ENDIF!][!//
[!IF "NmNodeIdEnabled = 'true'"!][!//
#define NM_BUSNM_GET_NODE_IDENTIFIER(NetworkHandle, nmNodeIdPtr)      \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].GetNodeIdentifier(      \
    (NetworkHandle), (nmNodeIdPtr))
#define NM_BUSNM_GET_LOCAL_NODE_IDENTIFIER(NetworkHandle, nmNodeIdPtr)  \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].GetLocalNodeIdentifier(   \
    (NetworkHandle), (nmNodeIdPtr))
[!ENDIF!][!//
[!IF "NmNodeDetectionEnabled = 'true'"!][!//
#define NM_BUSNM_REPEAT_MESSAGE_REQUEST(NetworkHandle)                \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].RepeatMessageRequest(   \
    NetworkHandle)
[!ENDIF!][!//
[!IF "(NmNodeIdEnabled = 'true') or (NmNodeDetectionEnabled = 'true') or (NmUserDataEnabled = 'true')"!][!//
#define NM_BUSNM_GET_PDU_DATA(NetworkHandle, nmPduData)       \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].GetPduData(     \
    (NetworkHandle), (nmPduData))
[!ENDIF!][!//

[!IF "NmRemoteSleepIndEnabled = 'true'"!][!//
#define NM_BUSNM_CHECK_REMOTE_SLEEP_INDICATION(                         \
  nmNetworkHandle, nmRemoteSleepIndPtr)                                 \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(nmNetworkHandle)].CheckRemoteSleepIndication( \
    (nmNetworkHandle), (nmRemoteSleepIndPtr))
[!ENDIF!][!//

[!IF "NmBusSynchronizationEnabled = 'true'"!][!//
#define NM_BUSNM_REQUEST_BUS_SYNCHRONIZATION(NetworkHandle)             \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].RequestBusSynchronization( \
    NetworkHandle)
[!ENDIF!][!//

[!IF "NmComControlEnabled = 'true'"!][!//
#define NM_BUSNM_DISABLE_COMMUNICATION(NetworkHandle)                   \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].DisableCommunication(     \
    NetworkHandle)
#define NM_BUSNM_ENABLE_COMMUNICATION(NetworkHandle)                    \
  Nm_BusNmFp[NM_GET_BUSNM_IDX(NetworkHandle)].EnableCommunication(      \
    NetworkHandle)
[!ENDIF!][!//

[!ENDIF!][!/* $BusNmNum = 1 */!][!//

/*==================[type definitions]======================================*/

[!INDENT "0"!]
  [!IF "(num:i($BusNmNum) > 1) or
        (NmCoordinatorSupportEnabled = 'true') or
        (count(../../NmChannelConfig/*[NmStateReportEnabled = 'true']) != 0) or
        (num:i(count(../../NmChannelConfig/*)) > 1)"!]
  /** \brief Type holding a channel configuration */
    typedef struct
    {
      [!IF "NmCoordinatorSupportEnabled = 'true'"!]
        [!WS "2"!]uint16            ShutdownDelayTimer;
      [!ENDIF!]
      [!IF "count(../../NmChannelConfig/*[NmStateReportEnabled = 'true']) != 0"!]
        [!WS "2"!]Com_SignalIdType  NmComHandleId;
      [!ENDIF!]
      [!IF "num:i($BusNmNum) > 1"!]
        [!WS "2"!]/** Index of underlying BusNm module */
        [!WS "2"!]uint8             BusNmId;
      [!ENDIF!]
      [!IF "NmCoordinatorSupportEnabled = 'true'"!]
        [!WS "2"!]uint8             CoordClusterIndex;
        [!WS "2"!]boolean           NmChannelSleepMaster;
        [!WS "2"!]boolean           NmSynchronizingNetwork;
      [!ENDIF!]
      [!WS "2"!]boolean             PassiveModeEnabled;
    } Nm_ChannelConfigType;
  [!ENDIF!]
[!ENDINDENT!]
[!ENDSELECT!]
/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

[!/* though Nm_ChannelConfig[] does not need to be generated it is a array
   * needed by Nm.h and Nm_Cbk.h.  Instead of creating a common header
   * included by Nm.h and Nm_Cbk.h we simply declare the array here */!][!//

#define NM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

#if ( (NM_BUSNM_NUM > 1U)                        ||     \
      (NM_COORDINATOR_SUPPORT_ENABLED == STD_ON) ||     \
      (NM_STATE_REPORT_ENABLED == STD_ON)        ||     \
      (NM_CHANNEL_NUM > 1U)                             \
    )
/** \brief Array holding the configuration of each channel */
extern CONST(Nm_ChannelConfigType, NM_CONST) Nm_ChannelConfig[NM_CHANNEL_NUM];
#endif

#define NM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

#define NM_START_SEC_CONST_8
#include <MemMap.h>

#if ( (NM_BUSNM_NUM > 1U)                         ||    \
      (NM_COORDINATOR_SUPPORT_ENABLED == STD_ON)  ||    \
      (NM_DEV_ERROR_DETECT == STD_ON)             ||    \
      (NM_STATE_REPORT_ENABLED == STD_ON)         ||    \
      (NM_CHANNEL_NUM > 1U)                             \
    )
extern CONST(uint8, NM_CONST) Nm_ChanIdxFromNetworkHandle[NM_MAX_CHANNELID + 1U];
#endif

#define NM_STOP_SEC_CONST_8
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
#endif /* if !defined( NM_CFG_H ) */
