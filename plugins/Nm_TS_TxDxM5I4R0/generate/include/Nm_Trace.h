/**
 * \file
 *
 * \brief AUTOSAR Nm
 *
 * This file contains the implementation of the AUTOSAR
 * module Nm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined NM_TRACE_H)
#define NM_TRACE_H
/*==================[inclusions]============================================*/

[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>
[!ENDIF!]

/*==================[macros]================================================*/

#ifndef DBG_NM_INIT_ENTRY
/** \brief Entry point of function Nm_Init() */
#define DBG_NM_INIT_ENTRY()
#endif

#ifndef DBG_NM_INIT_EXIT
/** \brief Exit point of function Nm_Init() */
#define DBG_NM_INIT_EXIT()
#endif

#ifndef DBG_NM_MAINFUNCTION_ENTRY
/** \brief Entry point of function Nm_MainFunction() */
#define DBG_NM_MAINFUNCTION_ENTRY()
#endif

#ifndef DBG_NM_MAINFUNCTION_EXIT
/** \brief Exit point of function Nm_MainFunction() */
#define DBG_NM_MAINFUNCTION_EXIT()
#endif

#ifndef DBG_NM_PASSIVESTARTUP_ENTRY
/** \brief Entry point of function Nm_PassiveStartUp() */
#define DBG_NM_PASSIVESTARTUP_ENTRY(a)
#endif

#ifndef DBG_NM_PASSIVESTARTUP_EXIT
/** \brief Exit point of function Nm_PassiveStartUp() */
#define DBG_NM_PASSIVESTARTUP_EXIT(a,b)
#endif

#ifndef DBG_NM_NETWORKREQUEST_ENTRY
/** \brief Entry point of function Nm_NetworkRequest() */
#define DBG_NM_NETWORKREQUEST_ENTRY(a)
#endif

#ifndef DBG_NM_NETWORKREQUEST_EXIT
/** \brief Exit point of function Nm_NetworkRequest() */
#define DBG_NM_NETWORKREQUEST_EXIT(a,b)
#endif

#ifndef DBG_NM_NETWORKRELEASE_ENTRY
/** \brief Entry point of function Nm_NetworkRelease() */
#define DBG_NM_NETWORKRELEASE_ENTRY(a)
#endif

#ifndef DBG_NM_NETWORKRELEASE_EXIT
/** \brief Exit point of function Nm_NetworkRelease() */
#define DBG_NM_NETWORKRELEASE_EXIT(a,b)
#endif

#ifndef DBG_NM_DISABLECOMMUNICATION_ENTRY
/** \brief Entry point of function Nm_DisableCommunication() */
#define DBG_NM_DISABLECOMMUNICATION_ENTRY(a)
#endif

#ifndef DBG_NM_DISABLECOMMUNICATION_EXIT
/** \brief Exit point of function Nm_DisableCommunication() */
#define DBG_NM_DISABLECOMMUNICATION_EXIT(a,b)
#endif

#ifndef DBG_NM_ENABLECOMMUNICATION_ENTRY
/** \brief Entry point of function Nm_EnableCommunication() */
#define DBG_NM_ENABLECOMMUNICATION_ENTRY(a)
#endif

#ifndef DBG_NM_ENABLECOMMUNICATION_EXIT
/** \brief Exit point of function Nm_EnableCommunication() */
#define DBG_NM_ENABLECOMMUNICATION_EXIT(a,b)
#endif

#ifndef DBG_NM_SETUSERDATA_ENTRY
/** \brief Entry point of function Nm_SetUserData() */
#define DBG_NM_SETUSERDATA_ENTRY(a,b)
#endif

#ifndef DBG_NM_SETUSERDATA_EXIT
/** \brief Exit point of function Nm_SetUserData() */
#define DBG_NM_SETUSERDATA_EXIT(a,b,c)
#endif

#ifndef DBG_NM_GETUSERDATA_ENTRY
/** \brief Entry point of function Nm_GetUserData() */
#define DBG_NM_GETUSERDATA_ENTRY(a,b)
#endif

#ifndef DBG_NM_GETUSERDATA_EXIT
/** \brief Exit point of function Nm_GetUserData() */
#define DBG_NM_GETUSERDATA_EXIT(a,b,c)
#endif

#ifndef DBG_NM_GETPDUDATA_ENTRY
/** \brief Entry point of function Nm_GetPduData() */
#define DBG_NM_GETPDUDATA_ENTRY(a,b)
#endif

#ifndef DBG_NM_GETPDUDATA_EXIT
/** \brief Exit point of function Nm_GetPduData() */
#define DBG_NM_GETPDUDATA_EXIT(a,b,c)
#endif

#ifndef DBG_NM_REPEATMESSAGEREQUEST_ENTRY
/** \brief Entry point of function Nm_RepeatMessageRequest() */
#define DBG_NM_REPEATMESSAGEREQUEST_ENTRY(a)
#endif

#ifndef DBG_NM_REPEATMESSAGEREQUEST_EXIT
/** \brief Exit point of function Nm_RepeatMessageRequest() */
#define DBG_NM_REPEATMESSAGEREQUEST_EXIT(a,b)
#endif

#ifndef DBG_NM_GETNODEIDENTIFIER_ENTRY
/** \brief Entry point of function Nm_GetNodeIdentifier() */
#define DBG_NM_GETNODEIDENTIFIER_ENTRY(a,b)
#endif

#ifndef DBG_NM_GETNODEIDENTIFIER_EXIT
/** \brief Exit point of function Nm_GetNodeIdentifier() */
#define DBG_NM_GETNODEIDENTIFIER_EXIT(a,b,c)
#endif

#ifndef DBG_NM_GETLOCALNODEIDENTIFIER_ENTRY
/** \brief Entry point of function Nm_GetLocalNodeIdentifier() */
#define DBG_NM_GETLOCALNODEIDENTIFIER_ENTRY(a,b)
#endif

#ifndef DBG_NM_GETLOCALNODEIDENTIFIER_EXIT
/** \brief Exit point of function Nm_GetLocalNodeIdentifier() */
#define DBG_NM_GETLOCALNODEIDENTIFIER_EXIT(a,b,c)
#endif

#ifndef DBG_NM_CHECKREMOTESLEEPINDICATION_ENTRY
/** \brief Entry point of function Nm_CheckRemoteSleepIndication() */
#define DBG_NM_CHECKREMOTESLEEPINDICATION_ENTRY(a,b)
#endif

#ifndef DBG_NM_CHECKREMOTESLEEPINDICATION_EXIT
/** \brief Exit point of function Nm_CheckRemoteSleepIndication() */
#define DBG_NM_CHECKREMOTESLEEPINDICATION_EXIT(a,b,c)
#endif

#ifndef DBG_NM_GETSTATE_ENTRY
/** \brief Entry point of function Nm_GetState() */
#define DBG_NM_GETSTATE_ENTRY(a,b,c)
#endif

#ifndef DBG_NM_GETSTATE_EXIT
/** \brief Exit point of function Nm_GetState() */
#define DBG_NM_GETSTATE_EXIT(a,b,c,d)
#endif

#ifndef DBG_NM_GETVERSIONINFO_ENTRY
/** \brief Entry point of function Nm_GetVersionInfo() */
#define DBG_NM_GETVERSIONINFO_ENTRY(a)
#endif

#ifndef DBG_NM_GETVERSIONINFO_EXIT
/** \brief Exit point of function Nm_GetVersionInfo() */
#define DBG_NM_GETVERSIONINFO_EXIT(a)
#endif

#ifndef DBG_NM_NETWORKSTARTINDICATION_ENTRY
/** \brief Entry point of function Nm_NetworkStartIndication() */
#define DBG_NM_NETWORKSTARTINDICATION_ENTRY(a)
#endif

#ifndef DBG_NM_NETWORKSTARTINDICATION_EXIT
/** \brief Exit point of function Nm_NetworkStartIndication() */
#define DBG_NM_NETWORKSTARTINDICATION_EXIT(a)
#endif

#ifndef DBG_NM_NETWORKMODE_ENTRY
/** \brief Entry point of function Nm_NetworkMode() */
#define DBG_NM_NETWORKMODE_ENTRY(a)
#endif

#ifndef DBG_NM_NETWORKMODE_EXIT
/** \brief Exit point of function Nm_NetworkMode() */
#define DBG_NM_NETWORKMODE_EXIT(a)
#endif

#ifndef DBG_NM_BUSSLEEPMODE_ENTRY
/** \brief Entry point of function Nm_BusSleepMode() */
#define DBG_NM_BUSSLEEPMODE_ENTRY(a)
#endif

#ifndef DBG_NM_BUSSLEEPMODE_EXIT
/** \brief Exit point of function Nm_BusSleepMode() */
#define DBG_NM_BUSSLEEPMODE_EXIT(a)
#endif

#ifndef DBG_NM_PREPAREBUSSLEEPMODE_ENTRY
/** \brief Entry point of function Nm_PrepareBusSleepMode() */
#define DBG_NM_PREPAREBUSSLEEPMODE_ENTRY(a)
#endif

#ifndef DBG_NM_PREPAREBUSSLEEPMODE_EXIT
/** \brief Exit point of function Nm_PrepareBusSleepMode() */
#define DBG_NM_PREPAREBUSSLEEPMODE_EXIT(a)
#endif

#ifndef DBG_NM_REMOTESLEEPINDICATION_ENTRY
/** \brief Entry point of function Nm_RemoteSleepIndication() */
#define DBG_NM_REMOTESLEEPINDICATION_ENTRY(a)
#endif

#ifndef DBG_NM_REMOTESLEEPINDICATION_EXIT
/** \brief Exit point of function Nm_RemoteSleepIndication() */
#define DBG_NM_REMOTESLEEPINDICATION_EXIT(a)
#endif

#ifndef DBG_NM_REMOTESLEEPCANCELLATION_ENTRY
/** \brief Entry point of function Nm_RemoteSleepCancellation() */
#define DBG_NM_REMOTESLEEPCANCELLATION_ENTRY(a)
#endif

#ifndef DBG_NM_REMOTESLEEPCANCELLATION_EXIT
/** \brief Exit point of function Nm_RemoteSleepCancellation() */
#define DBG_NM_REMOTESLEEPCANCELLATION_EXIT(a)
#endif

#ifndef DBG_NM_SYNCHRONIZATIONPOINT_ENTRY
/** \brief Entry point of function Nm_SynchronizationPoint() */
#define DBG_NM_SYNCHRONIZATIONPOINT_ENTRY(a)
#endif

#ifndef DBG_NM_SYNCHRONIZATIONPOINT_EXIT
/** \brief Exit point of function Nm_SynchronizationPoint() */
#define DBG_NM_SYNCHRONIZATIONPOINT_EXIT(a)
#endif

#ifndef DBG_NM_STATECHANGENOTIFICATION_ENTRY
/** \brief Entry point of function Nm_StateChangeNotification() */
#define DBG_NM_STATECHANGENOTIFICATION_ENTRY(a,b,c)
#endif

#ifndef DBG_NM_STATECHANGENOTIFICATION_EXIT
/** \brief Exit point of function Nm_StateChangeNotification() */
#define DBG_NM_STATECHANGENOTIFICATION_EXIT(a,b,c)
#endif

#ifndef DBG_NM_COORDINATEDSHUTDOWNHANDLER_ENTRY
/** \brief Entry point of function Nm_CoordinatedShutdownHandler() */
#define DBG_NM_COORDINATEDSHUTDOWNHANDLER_ENTRY(a)
#endif

#ifndef DBG_NM_COORDINATEDSHUTDOWNHANDLER_EXIT
/** \brief Exit point of function Nm_CoordinatedShutdownHandler() */
#define DBG_NM_COORDINATEDSHUTDOWNHANDLER_EXIT(a)
#endif

#ifndef DBG_NM_ABORTSHUTDOWN_ENTRY
/** \brief Entry point of function Nm_AbortShutdown() */
#define DBG_NM_ABORTSHUTDOWN_ENTRY(a,b)
#endif

#ifndef DBG_NM_ABORTSHUTDOWN_EXIT
/** \brief Exit point of function Nm_AbortShutdown() */
#define DBG_NM_ABORTSHUTDOWN_EXIT(a,b)
#endif

#ifndef DBG_NM_ISSYNCHRONIZATIONREQUIRED_ENTRY
/** \brief Entry point of function Nm_IsSynchronizationRequired() */
#define DBG_NM_ISSYNCHRONIZATIONREQUIRED_ENTRY(a)
#endif

#ifndef DBG_NM_ISSYNCHRONIZATIONREQUIRED_EXIT
/** \brief Exit point of function Nm_IsSynchronizationRequired() */
#define DBG_NM_ISSYNCHRONIZATIONREQUIRED_EXIT(a,b)
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined NM_TRACE_H) */
/*==================[end of file]===========================================*/
