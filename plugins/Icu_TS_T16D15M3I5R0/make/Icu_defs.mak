# \file
#
# \brief AUTOSAR Icu
#
# This file contains the implementation of the AUTOSAR
# module Icu.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

Icu_CORE_PATH      := $(SSC_ROOT)\Icu_$(Icu_VARIANT)
Icu_OUTPUT_PATH    := $(AUTOSAR_BASE_OUTPUT_PATH)

Icu_GEN_FILES      += $(Icu_OUTPUT_PATH)\include\Icu_17_GtmCcu6_Cfg.h
Icu_GEN_FILES      += $(Icu_OUTPUT_PATH)\src\Icu_17_GtmCcu6_PBCfg.c

TRESOS_GEN_FILES   += $(Icu_GEN_FILES)

CC_INCLUDE_PATH    += $(Icu_CORE_PATH)\include
CC_INCLUDE_PATH    += $(Icu_OUTPUT_PATH)\include

