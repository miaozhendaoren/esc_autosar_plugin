# \file
#
# \brief AUTOSAR Icu
#
# This file contains the implementation of the AUTOSAR
# module Icu.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Icu_src

Icu_src_FILES       += $(Icu_CORE_PATH)\src\Icu_17_GtmCcu6.c
Icu_src_FILES       += $(Icu_CORE_PATH)\src\Icu_17_GtmCcu6_Safe.c
Icu_src_FILES       += $(Icu_CORE_PATH)\src\Icu_17_GtmCcu6_Ver.c
Icu_src_FILES       += $(Icu_CORE_PATH)\src\Eru_Irq.c
Icu_src_FILES       += $(Icu_CORE_PATH)\src\Icu_17_GtmCcu6_Irq.c

Icu_src_FILES       += $(Icu_OUTPUT_PATH)\src\Icu_17_GtmCcu6_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Icu_src_FILES :=
endif
