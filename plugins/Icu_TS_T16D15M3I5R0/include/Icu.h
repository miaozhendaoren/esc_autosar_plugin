#if !defined( ICU_H )
#define ICU_H

#include <Icu_17_GtmCcu6.h>

#define ICU_AR_RELEASE_MAJOR_VERSION ICU_17_GTMCCU6_AR_RELEASE_MAJOR_VERSION
#define ICU_AR_RELEASE_MINOR_VERSION ICU_17_GTMCCU6_AR_RELEASE_MINOR_VERSION

/* Global Type Definitions */
typedef Icu_17_GtmCcu6_ModeType                       Icu_ModeType;
typedef Icu_17_GtmCcu6_ChannelType                    Icu_ChannelType;
typedef Icu_17_GtmCcu6_InputStateType                 Icu_InputStateType;
typedef Icu_17_GtmCcu6_ConfigType                     Icu_ConfigType;
typedef Icu_17_GtmCcu6_ActivationType                 Icu_ActivationType;
typedef Icu_17_GtmCcu6_ValueType                      Icu_ValueType;
typedef Icu_17_GtmCcu6_DutyCycleType                  Icu_DutyCycleType;
typedef Icu_17_GtmCcu6_IndexType                      Icu_IndexType;
typedef Icu_17_GtmCcu6_EdgeNumberType                 Icu_EdgeNumberType;
typedef Icu_17_GtmCcu6_MeasurementModeType            Icu_MeasurementModeType;
typedef Icu_17_GtmCcu6_SignalMeasurementPropertyType  Icu_SignalMeasurementPropertyType;
typedef Icu_17_GtmCcu6_TimestampBufferType            Icu_TimestampBufferType;

/* Global Function Declarations */
#define Icu_Init                      Icu_17_GtmCcu6_Init
#define Icu_DeInit                    Icu_17_GtmCcu6_DeInit
#define Icu_SetMode                   Icu_17_GtmCcu6_SetMode
#define Icu_DisableWakeup             Icu_17_GtmCcu6_DisableWakeup
#define Icu_EnableWakeup              Icu_17_GtmCcu6_EnableWakeup
#define Icu_CheckWakeup               Icu_17_GtmCcu6_CheckWakeup
#define Icu_SetActivationCondition    Icu_17_GtmCcu6_SetActivationCondition
#define Icu_DisableNotification       Icu_17_GtmCcu6_DisableNotification
#define Icu_EnableNotification        Icu_17_GtmCcu6_EnableNotification
#define Icu_GetInputState             Icu_17_GtmCcu6_GetInputState
#define Icu_StartTimestamp            Icu_17_GtmCcu6_StartTimestamp
#define Icu_StopTimestamp             Icu_17_GtmCcu6_StopTimestamp
#define Icu_GetTimestampIndex         Icu_17_GtmCcu6_GetTimestampIndex
#define Icu_ResetEdgeCount            Icu_17_GtmCcu6_ResetEdgeCount
#define Icu_EnableEdgeCount           Icu_17_GtmCcu6_EnableEdgeCount
#define Icu_EnableEdgeDetection       Icu_17_GtmCcu6_EnableEdgeDetection
#define Icu_DisableEdgeDetection      Icu_17_GtmCcu6_DisableEdgeDetection
#define Icu_DisableEdgeCount          Icu_17_GtmCcu6_DisableEdgeCount
#define Icu_GetEdgeNumbers            Icu_17_GtmCcu6_GetEdgeNumbers
#define Icu_StartSignalMeasurement    Icu_17_GtmCcu6_StartSignalMeasurement
#define Icu_StopSignalMeasurement     Icu_17_GtmCcu6_StopSignalMeasurement
#define Icu_GetTimeElapsed            Icu_17_GtmCcu6_GetTimeElapsed
#define Icu_GetDutyCycleValues        Icu_17_GtmCcu6_GetDutyCycleValues
#define Icu_GetVersionInfo            Icu_17_GtmCcu6_GetVersionInfo

#endif