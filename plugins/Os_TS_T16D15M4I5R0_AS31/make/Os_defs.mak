# \file
#
# \brief AUTOSAR Os
#
# This file contains the implementation of the AUTOSAR
# module Os.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2014 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

# define release variant for conditionals in generic makefiles
OS_RELEASE_SUFFIX := AS31

# redirect AS31 to common OS plugin
include $(SSC_ROOT)\Os_TS_T16D15M4I5R0\make\Os_defs.mak
