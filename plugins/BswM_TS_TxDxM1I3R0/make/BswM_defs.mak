# \file
#
# \brief AUTOSAR BswM
#
# This file contains the implementation of the AUTOSAR
# module BswM.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

BswM_CORE_PATH         := $(SSC_ROOT)\BswM_$(BswM_VARIANT)

BswM_OUTPUT_PATH       := $(AUTOSAR_BASE_OUTPUT_PATH)

#################################################################
# REGISTRY
SSC_PLUGINS            += BswM
BswM_DEPENDENT_PLUGINS := base_make tresos
BswM_VERSION           := 1.00.00
BswM_DESCRIPTION       := BswM Basic Software Makefile PlugIn for Autosar
CC_INCLUDE_PATH        +=    \
   $(BswM_CORE_PATH)\include \
   $(BswM_OUTPUT_PATH)\include
