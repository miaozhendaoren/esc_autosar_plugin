# \file
#
# \brief AUTOSAR BswM
#
# This file contains the implementation of the AUTOSAR
# module BswM.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

#################################################################
# REGISTRY

BswM_src_FILES      := $(BswM_OUTPUT_PATH)\src\BswM.c

LIBRARIES_TO_BUILD += BswM_src

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES




