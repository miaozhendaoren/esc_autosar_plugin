/**
 * \file
 *
 * \brief AUTOSAR BswM
 *
 * This file contains the implementation of the AUTOSAR
 * module BswM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined BSWM_LINSM_H)
#define BSWM_LINSM_H

/* !LINKSTO     BswM0026,1
 * !description Definition of the BswM_LinSM.h for the LinSM.
 */

#include <BswM_Cfg.h>   /* Required for macro resolution */

#if (BSWM_LINSM_API_ENABLED == STD_ON)
/*==================[includes]===============================================*/

#include <BswM.h>       /* Module public API         */
#include <LinSM.h>      /* LinSM API                 */

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

#define BSWM_START_SEC_CODE
#include <MemMap.h>


/** \brief Indicates current state (Called by LinSM).
 **
 ** This function is called by LinSM to indicate its current state.
 **
 ** Precondition: None
 **
 ** \param[in]    Network      - The LIN channel that the indicated state
 **                              corresponds to.
 **               CurrentState - The current state of the LIN channel.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{9}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */

extern FUNC(void, BSWM_CODE) BswM_LinSM_CurrentState
(
  NetworkHandleType Network,
  LinSM_ModeType CurrentState
);

/** \brief Indicates the LIN channel's currently active schedule table (Called by LinSM).
 **
 ** This function is called by LinSM to indicate the currently active schedule table
 ** for a specific LIN channel.
 **
 ** Precondition: None
 **
 ** \param[in]    Network         - The LIN channel that the schedule table switch have
 **                                 occurred on.
 **               CurrentSchedule - The currently active schedule table of the
 **                                 LIN channel.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x0A}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */

extern FUNC(void, BSWM_CODE) BswM_LinSM_CurrentSchedule
(
    NetworkHandleType Network,
    LinIf_SchHandleType CurrentSchedule
);

#define BSWM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/
#endif /* (BSWM_LINSM_API_ENABLED == STD_ON) */

#endif /* if !defined( BSWM_LINSM_H ) */
/*==================[end of file]============================================*/
