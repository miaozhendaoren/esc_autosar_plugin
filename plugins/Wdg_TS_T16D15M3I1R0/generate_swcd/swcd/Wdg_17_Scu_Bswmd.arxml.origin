<!--
/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2014)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Wdg_17_Scu_Bswmd.arxml $                                   **
**                                                                           **
**  $CC VERSION : \main\17 $                                                 **
**                                                                           **
**  $DATE       : 2014-08-26 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains                                         **
**                - XML Data Model for WDG driver                            **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/
/*  TRACEABILITY: [cover parentID=SAS_AS4XX_WDG_PR672,
    SAS_AS4XX_WDG_PR673,SAS_AS4XX_WDG_PR674,SAS_AS4XX_WDG_PR675,
	SAS_AS4XX_WDG_PR676,SAS_AS4XX_WDG_PR677,SAS_AS4XX_WDG_PR681,
	SAS_AS4XX_WDG_PR683,SAS_AS4XX_WDG_PR684,SAS_AS4XX_WDG_PR685,
    SAS_AS4XX_WDG_PR686,SAS_AS4XX_WDG_PR687,SAS_AS4XX_WDG_PR688,
	SAS_AS4XX_WDG_PR689, SAS_AS4XX_WDG_PR690,SAS_AS4XX_WDG_PR693,
	SAS_AS4XX_WDG_PR694,SAS_AS4XX_WDG_PR695,SAS_AS4XX_WDG_PR669]
    [/cover]                                                                  */
-->

<AUTOSAR xmlns="http://autosar.org/schema/r4.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3_STRICT.xsd"> 
<AR-PACKAGES>
    <AR-PACKAGE>
        <SHORT-NAME>AUTOSAR_Wdg</SHORT-NAME>
        <AR-PACKAGES>
            <AR-PACKAGE>
                <SHORT-NAME>BswModuleDescriptions</SHORT-NAME>
                <CATEGORY>STANDARD</CATEGORY>
                <ELEMENTS>
                    <BSW-MODULE-DESCRIPTION>
                        <SHORT-NAME>Wdg</SHORT-NAME>
                        <LONG-NAME>
                            <L-4 L="EN">WDG Driver</L-4>
                        </LONG-NAME>
                        <CATEGORY>BSW_MODULE</CATEGORY>
                        <MODULE-ID>102</MODULE-ID>
                        <PROVIDED-ENTRYS>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Wdg/BswModuleEntrys/Wdg_Init</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Wdg/BswModuleEntrys/Wdg_SetMode</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Wdg/BswModuleEntrys/Wdg_17_Scu_SafeTrigger</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Wdg/BswModuleEntrys/Wdg_17_Scu_SetOffMode</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Wdg/BswModuleEntrys/Wdg_SetTriggerCondition</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Wdg/BswModuleEntrys/Wdg_Cbk_GtmNotificationSRV</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                        </PROVIDED-ENTRYS>
                        <BSW-MODULE-DEPENDENCYS>
                            <BSW-MODULE-DEPENDENCY>
                                <SHORT-NAME>DetDependency</SHORT-NAME>
                                <TARGET-MODULE-ID>15</TARGET-MODULE-ID>
                                <REQUIRED-ENTRYS>
                                    <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                        <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Det/BswModuleEntrys/Det_ReportError</BSW-MODULE-ENTRY-REF>
                                    </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                </REQUIRED-ENTRYS>
                            </BSW-MODULE-DEPENDENCY>
                            <BSW-MODULE-DEPENDENCY>
                                <SHORT-NAME>DemDependency</SHORT-NAME>
                                <TARGET-MODULE-ID>54</TARGET-MODULE-ID>
                                <REQUIRED-ENTRYS>
                                    <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                        <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Dem/BswModuleEntrys/Dem_ReportErrorStatus</BSW-MODULE-ENTRY-REF>
                                    </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                </REQUIRED-ENTRYS>
                            </BSW-MODULE-DEPENDENCY>
                        </BSW-MODULE-DEPENDENCYS>
                        <INTERNAL-BEHAVIORS>
                          <BSW-INTERNAL-BEHAVIOR>
                            <SHORT-NAME>WdgBehavior</SHORT-NAME>
                            <EXCLUSIVE-AREAS>
                              <EXCLUSIVE-AREA>
                                <SHORT-NAME>TimerHandling</SHORT-NAME>
                              </EXCLUSIVE-AREA>
                              <EXCLUSIVE-AREA>
                                <SHORT-NAME>TimerHandling</SHORT-NAME>
                              </EXCLUSIVE-AREA>
                              <EXCLUSIVE-AREA>
                                <SHORT-NAME>TimerHandling</SHORT-NAME>
                              </EXCLUSIVE-AREA>
                              <EXCLUSIVE-AREA>
                                <SHORT-NAME>TimerHandling</SHORT-NAME>
                              </EXCLUSIVE-AREA>
                              <EXCLUSIVE-AREA>
                                <SHORT-NAME>TimerHandling</SHORT-NAME>
                              </EXCLUSIVE-AREA>
                              <EXCLUSIVE-AREA>
                                <SHORT-NAME>TimerHandling</SHORT-NAME>
                              </EXCLUSIVE-AREA>
                            </EXCLUSIVE-AREAS>
                            <STATIC-MEMORYS>
                              <VARIABLE-DATA-PROTOTYPE>
                                <SHORT-NAME>Wdg_TimeoutCounter</SHORT-NAME>
                                <SW-DATA-DEF-PROPS>
                                  <SW-DATA-DEF-PROPS-VARIANTS>
                                    <SW-DATA-DEF-PROPS-CONDITIONAL>
                                    <BASE-TYPE-REF DEST="SW-BASE-TYPE"
                                      >/AUTOSAR_Platform/BaseTypes/uint32</BASE-TYPE-REF>
                                    </SW-DATA-DEF-PROPS-CONDITIONAL>
                                  </SW-DATA-DEF-PROPS-VARIANTS>
                                </SW-DATA-DEF-PROPS>
                                <TYPE-TREF DEST="AUTOSAR-DATA-TYPE"
                                >/AUTOSAR_Platform/BaseTypes/uint32</TYPE-TREF>
                              </VARIABLE-DATA-PROTOTYPE>
                              <VARIABLE-DATA-PROTOTYPE>
                                <SHORT-NAME>Wdg_DriverState</SHORT-NAME>
                                <SW-DATA-DEF-PROPS>
                                  <SW-DATA-DEF-PROPS-VARIANTS>
                                    <SW-DATA-DEF-PROPS-CONDITIONAL>
                                    <IMPLEMENTATION-DATA-TYPE-REF DEST = "IMPLEMENTATION-DATA-TYPE"
                                      >/AUTOSAR_Wdg/ImplementationDataTypes/Wdg_StatusType</IMPLEMENTATION-DATA-TYPE-REF>
                                    </SW-DATA-DEF-PROPS-CONDITIONAL>
                                  </SW-DATA-DEF-PROPS-VARIANTS>
                                </SW-DATA-DEF-PROPS>
                                <TYPE-TREF DEST = "IMPLEMENTATION-DATA-TYPE"
                                >/AUTOSAR_Wdg/ImplementationDataTypes/Wdg_StatusType</TYPE-TREF>
                              </VARIABLE-DATA-PROTOTYPE>
                              <VARIABLE-DATA-PROTOTYPE>
                                <SHORT-NAME>Wdg_DriverMode</SHORT-NAME>
                                <SW-DATA-DEF-PROPS>
                                  <SW-DATA-DEF-PROPS-VARIANTS>
                                    <SW-DATA-DEF-PROPS-CONDITIONAL>
                                    <IMPLEMENTATION-DATA-TYPE-REF DEST = "IMPLEMENTATION-DATA-TYPE"
                                      >/AUTOSAR_Wdg/ImplementationDataTypes/WdgIf_ModeType</IMPLEMENTATION-DATA-TYPE-REF>
                                    </SW-DATA-DEF-PROPS-CONDITIONAL>
                                  </SW-DATA-DEF-PROPS-VARIANTS>
                                </SW-DATA-DEF-PROPS>
                                <TYPE-TREF DEST = "IMPLEMENTATION-DATA-TYPE"
                                >/AUTOSAR_Wdg/ImplementationDataTypes/WdgIf_ModeType</TYPE-TREF>
                              </VARIABLE-DATA-PROTOTYPE>
                            </STATIC-MEMORYS>
                            <ENTITYS>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Wdg_Init</SHORT-NAME>
                                <CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                  <CAN-ENTER-EXCLUSIVE-AREA-REF DEST="EXCLUSIVE-AREA">/AUTOSAR_Wdg/BswModuleDescriptions/Wdg/WdgBehavior/TimerHandling</CAN-ENTER-EXCLUSIVE-AREA-REF>
                                </CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Wdg/BswModuleEntrys/Wdg_17_Scu_Init</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Wdg_SetMode</SHORT-NAME>
                                <CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                  <CAN-ENTER-EXCLUSIVE-AREA-REF DEST="EXCLUSIVE-AREA">/AUTOSAR_Wdg/BswModuleDescriptions/Wdg/WdgBehavior/TimerHandling</CAN-ENTER-EXCLUSIVE-AREA-REF>
                                </CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Wdg/BswModuleEntrys/Wdg_17_Scu_SetMode</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Wd_SafeTrigger</SHORT-NAME>
                                <CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                  <CAN-ENTER-EXCLUSIVE-AREA-REF DEST="EXCLUSIVE-AREA">/AUTOSAR_Wdg/BswModuleDescriptions/Wdg/WdgBehavior/TimerHandling</CAN-ENTER-EXCLUSIVE-AREA-REF>
                                </CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Wdg/BswModuleEntrys/Wdg_17_Scu_SafeTrigger</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Wdg_SetOffMode</SHORT-NAME>
                                <CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                  <CAN-ENTER-EXCLUSIVE-AREA-REF DEST="EXCLUSIVE-AREA">/AUTOSAR_Wdg/BswModuleDescriptions/Wdg/WdgBehavior/TimerHandling</CAN-ENTER-EXCLUSIVE-AREA-REF>
                                </CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Wdg/BswModuleEntrys/Wdg_17_Scu_SetOffMode</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Wdg_SetTriggerCondition</SHORT-NAME>
                                <CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                  <CAN-ENTER-EXCLUSIVE-AREA-REF DEST="EXCLUSIVE-AREA">/AUTOSAR_Wdg/BswModuleDescriptions/Wdg/WdgBehavior/TimerHandling</CAN-ENTER-EXCLUSIVE-AREA-REF>
                                </CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Wdg/BswModuleEntrys/Wdg_17_Scu_SetTriggerCondition</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Wdg_Cbk_GtmNotificationSRV</SHORT-NAME>
                                <CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                  <CAN-ENTER-EXCLUSIVE-AREA-REF DEST="EXCLUSIVE-AREA">/AUTOSAR_Wdg/BswModuleDescriptions/Wdg/WdgBehavior/TimerHandling</CAN-ENTER-EXCLUSIVE-AREA-REF>
                                </CAN-ENTER-EXCLUSIVE-AREA-REFS>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Wdg/BswModuleEntrys/Wdg_17_Scu_Cbk_GtmNotificationSRV</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                            </ENTITYS>
                          </BSW-INTERNAL-BEHAVIOR>
                        </INTERNAL-BEHAVIORS>
                    </BSW-MODULE-DESCRIPTION>
                </ELEMENTS>
            </AR-PACKAGE>
            <AR-PACKAGE>
                <SHORT-NAME>BswModuleEntrys</SHORT-NAME>
                <CATEGORY>STANDARD</CATEGORY>
                <ELEMENTS>
                    <BSW-MODULE-ENTRY>
                        <SHORT-NAME>Wdg_17_Scu_SafeTrigger</SHORT-NAME>
                        <SERVICE-ID>6</SERVICE-ID>
                        <IS-REENTRANT>false</IS-REENTRANT>
                        <IS-SYNCHRONOUS>true</IS-SYNCHRONOUS>
                        <CALL-TYPE>REGULAR</CALL-TYPE>
                        <EXECUTION-CONTEXT>TASK</EXECUTION-CONTEXT>
                        <SW-SERVICE-IMPL-POLICY>STANDARD</SW-SERVICE-IMPL-POLICY>
                        <ARGUMENTS>
                          <SW-SERVICE-ARG>
                            <SHORT-NAME>SeqPassword</SHORT-NAME>
                            <SW-DATA-DEF-PROPS>
                              <SW-DATA-DEF-PROPS-VARIANTS>
                                <SW-DATA-DEF-PROPS-CONDITIONAL>
                                <BASE-TYPE-REF DEST="SW-BASE-TYPE"
                                >/AUTOSAR_Platform/BaseTypes/uint16</BASE-TYPE-REF>
                                </SW-DATA-DEF-PROPS-CONDITIONAL>
                              </SW-DATA-DEF-PROPS-VARIANTS>
                            </SW-DATA-DEF-PROPS>
                          </SW-SERVICE-ARG>
                          <SW-SERVICE-ARG>
                            <SHORT-NAME>ResetPass</SHORT-NAME>
                            <SW-DATA-DEF-PROPS>
                              <SW-DATA-DEF-PROPS-VARIANTS>
                                <SW-DATA-DEF-PROPS-CONDITIONAL>
                                <BASE-TYPE-REF DEST="SW-BASE-TYPE"
                                >/AUTOSAR_Platform/BaseTypes/uint16</BASE-TYPE-REF>
                                </SW-DATA-DEF-PROPS-CONDITIONAL>
                              </SW-DATA-DEF-PROPS-VARIANTS>
                            </SW-DATA-DEF-PROPS>
                          </SW-SERVICE-ARG>
                          <SW-SERVICE-ARG>
                            <SHORT-NAME>TimerCheck
</SHORT-NAME>
                            <SW-DATA-DEF-PROPS>
                              <SW-DATA-DEF-PROPS-VARIANTS>
                                <SW-DATA-DEF-PROPS-CONDITIONAL>
                                <BASE-TYPE-REF DEST="SW-BASE-TYPE"
                                >/AUTOSAR_Platform/BaseTypes/uint16</BASE-TYPE-REF>
                                </SW-DATA-DEF-PROPS-CONDITIONAL>
                              </SW-DATA-DEF-PROPS-VARIANTS>
                            </SW-DATA-DEF-PROPS>
                          </SW-SERVICE-ARG>
                        </ARGUMENTS>
                    </BSW-MODULE-ENTRY>
                    <BSW-MODULE-ENTRY>
                        <SHORT-NAME>Wdg_17_Scu_SetOffMode</SHORT-NAME>
                        <SERVICE-ID>7</SERVICE-ID>
                        <IS-REENTRANT>false</IS-REENTRANT>
                        <IS-SYNCHRONOUS>true</IS-SYNCHRONOUS>
                        <CALL-TYPE>REGULAR</CALL-TYPE>
                        <EXECUTION-CONTEXT>TASK</EXECUTION-CONTEXT>
                        <SW-SERVICE-IMPL-POLICY>STANDARD</SW-SERVICE-IMPL-POLICY>
                        <ARGUMENTS>
                          <SW-SERVICE-ARG>
                            <SHORT-NAME>SeqPassword1</SHORT-NAME>
                            <SW-DATA-DEF-PROPS>
                              <SW-DATA-DEF-PROPS-VARIANTS>
                                <SW-DATA-DEF-PROPS-CONDITIONAL>
                                <BASE-TYPE-REF DEST="SW-BASE-TYPE"
                                >/AUTOSAR_Platform/BaseTypes/uint16</BASE-TYPE-REF>
                                </SW-DATA-DEF-PROPS-CONDITIONAL>
                              </SW-DATA-DEF-PROPS-VARIANTS>
                            </SW-DATA-DEF-PROPS>
                          </SW-SERVICE-ARG>
                          <SW-SERVICE-ARG>
                            <SHORT-NAME>SeqPassword2</SHORT-NAME>
                            <SW-DATA-DEF-PROPS>
                              <SW-DATA-DEF-PROPS-VARIANTS>
                                <SW-DATA-DEF-PROPS-CONDITIONAL>
                                <BASE-TYPE-REF DEST="SW-BASE-TYPE"
                                >/AUTOSAR_Platform/BaseTypes/uint16</BASE-TYPE-REF>
                                </SW-DATA-DEF-PROPS-CONDITIONAL>
                              </SW-DATA-DEF-PROPS-VARIANTS>
                            </SW-DATA-DEF-PROPS>
                          </SW-SERVICE-ARG>
                        </ARGUMENTS>
                    </BSW-MODULE-ENTRY>
                </ELEMENTS>
            </AR-PACKAGE>
            <AR-PACKAGE>
                <SHORT-NAME>Implementations</SHORT-NAME>
                <ELEMENTS>
                    <BSW-IMPLEMENTATION>
                        <SHORT-NAME>Wdg</SHORT-NAME>
                        <CODE-DESCRIPTORS>
                            <CODE>
                                <SHORT-NAME>Files</SHORT-NAME>
                                <ARTIFACT-DESCRIPTORS>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>ssc::src::Wdg_17_Scu.c</SHORT-LABEL>
                                        <CATEGORY>SWSRC</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>ssc::src::Wdg_17_Scu_Safe.c</SHORT-LABEL>
                                        <CATEGORY>SWSRC</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>ssc::inc::Wdg_17_Scu_Safe.h</SHORT-LABEL>
                                        <CATEGORY>SWHDR</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>ssc::inc::Wdg_17_Scu.h</SHORT-LABEL>
                                        <CATEGORY>SWHDR</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>mak::wdg_17_scu_infineon_tricore::ssc::mak::infineon_wdg_17_scu_cfg.mak</SHORT-LABEL>
                                        <CATEGORY>SWMAKE</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>mak::wdg_17_scu_infineon_tricore::ssc::mak::infineon_wdg_17_scu_check.mak</SHORT-LABEL>
                                        <CATEGORY>SWMAKE</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>mak::wdg_17_scu_infineon_tricore::ssc::mak::infineon_wdg_17_scu_defs.mak</SHORT-LABEL>
                                        <CATEGORY>SWMAKE</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>mak::wdg_17_scu_infineon_tricore::ssc::mak::infineon_wdg_17_scu_rules.mak</SHORT-LABEL>
                                        <CATEGORY>SWMAKE</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                </ARTIFACT-DESCRIPTORS>
                            </CODE>
                        </CODE-DESCRIPTORS>
                        <COMPILERS>
                            <COMPILER>
                                <SHORT-NAME>TASKING_COMPILE</SHORT-NAME>
                                <NAME>TASKING</NAME>
                                <OPTIONS>&quot;--core=tc1.6.x -t -Wa-gAHLs --emit-locals=-equ,-symbols -Wa-Ogs -Wa--error-limit=42 --iso=90 --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2ROP --tradeoff=4 -g --source -D_TASKING_C_TRICORE_=1 $(CALLFUNCTION)&quot;</OPTIONS>
                                <VENDOR>Tasking</VENDOR>
                                <VERSION>Tasking</VERSION>
                            </COMPILER>
                            <COMPILER>
                                <SHORT-NAME>DIAB_COMPILE</SHORT-NAME>
                                <NAME>DIAB</NAME>
                                <OPTIONS>&quot;-Xdialect-c89 -Xno-common -Xalign-functions=4 -Xfp-fast -Xsection-split=1 -Xkeep-assembly-file=2 -g3 -ew4084 -ei4177,4550,4549,4068,5388 -XO -D_DIABDATA_C_TRICORE_=1 $(CALLFUNCTION)&quot;</OPTIONS>
                                <VENDOR>WindRiver</VENDOR>
                                <VERSION>WindRiver</VERSION>
                            </COMPILER>
                            <COMPILER>
                                <SHORT-NAME>GNU_COMPILE</SHORT-NAME>
                                <NAME>GNU</NAME>
                                <OPTIONS>&quot;-save-temps=obj -std=iso9899:1990 -ansi -fno-asm -ffreestanding -Wundef -Wp,-std=iso9899:1990 -fno-short-enums -fpeel-loops -falign-functions=4 -frecord-gcc-switches -fsection-anchors -funsigned-bitfields -ffunction-sections -nostartfiles -O3 -g3 -W -Wall -Wuninitialized -DTRIBOARD_TC27XX -mcpu=tc27xx -D_GNU_C_TRICORE_=1 $(CALLFUNCTION)&quot;</OPTIONS>
                                <VENDOR>Hightec</VENDOR>
                                <VERSION>Hightec</VERSION>
                            </COMPILER>
                        </COMPILERS>
                        <GENERATED-ARTIFACTS>
                            <DEPENDENCY-ON-ARTIFACT>
                                <SHORT-NAME>Wdg_PBCfg_c</SHORT-NAME>
                                <ARTIFACT-DESCRIPTOR>
                                    <SHORT-LABEL>generate::src::Wdg_17_Scu_PBCfg.c</SHORT-LABEL>
                                    <CATEGORY>SWSRC</CATEGORY>
                                </ARTIFACT-DESCRIPTOR>
                                <USAGES>
                                    <USAGE>CODEGENERATION</USAGE>
                                </USAGES>
                            </DEPENDENCY-ON-ARTIFACT>
                            <DEPENDENCY-ON-ARTIFACT>
                                <SHORT-NAME>Wdg_Cfg_h</SHORT-NAME>
                                <ARTIFACT-DESCRIPTOR>
                                    <SHORT-LABEL>generate::inc::Wdg_17_Scu_Cfg.h</SHORT-LABEL>
                                    <CATEGORY>SWHDR</CATEGORY>
                                </ARTIFACT-DESCRIPTOR>
                                <USAGES>
                                    <USAGE>CODEGENERATION</USAGE>
                                </USAGES>
                            </DEPENDENCY-ON-ARTIFACT>
                            <DEPENDENCY-ON-ARTIFACT>
                                <SHORT-NAME>Wdg_xdm</SHORT-NAME>
                                <ARTIFACT-DESCRIPTOR>
                                    <SHORT-LABEL>config::Wdg_17_Scu.xdm</SHORT-LABEL>
                                    <CATEGORY>SWTOOL</CATEGORY>
                                </ARTIFACT-DESCRIPTOR>
                                <USAGES>
                                    <USAGE>CODEGENERATION</USAGE>
                                </USAGES>
                            </DEPENDENCY-ON-ARTIFACT>
                            <DEPENDENCY-ON-ARTIFACT>
                                <SHORT-NAME>Wdg_bmd</SHORT-NAME>
                                <ARTIFACT-DESCRIPTOR>
                                    <SHORT-LABEL>autosar::Wdg_17_Scu.bmd</SHORT-LABEL>
                                    <CATEGORY>SWTOOL</CATEGORY>
                                </ARTIFACT-DESCRIPTOR>
                                <USAGES>
                                    <USAGE>CODEGENERATION</USAGE>
                                </USAGES>
                            </DEPENDENCY-ON-ARTIFACT>
                        </GENERATED-ARTIFACTS>
                        <LINKERS>
                            <LINKER>
                                <SHORT-NAME>TASKING_LINK</SHORT-NAME>
                                <NAME>TASKING</NAME>
                                <OPTIONS>&quot;-t -d_mcal_pjt.lsl -D__CPU__=tc27x -Cmpe:vtc -OtXYCL --error-limit=42 -M -mcrfiklsmnoduq -lc -lfp -lrt&quot;</OPTIONS>
                                <VENDOR>Tasking</VENDOR>
                                <VERSION>Tasking</VERSION>
                            </LINKER>
                            <LINKER>
                                <SHORT-NAME>DIAB_LINK</SHORT-NAME>
                                <NAME>DIAB</NAME>
                                <OPTIONS>&quot;-m6 &quot;_mcal_pjt.dld&quot; -lc&quot;</OPTIONS>
                                <VENDOR>WindRiver</VENDOR>
                                <VERSION>WindRiver</VERSION>
                            </LINKER>
                            <LINKER>
                                <SHORT-NAME>GNU_LINK</SHORT-NAME>
                                <NAME>GNU</NAME>
                                <OPTIONS>&quot;-T&quot;_mcal_pjt.ld&quot; -nostartfiles -Wl,--allow-multiple-definition -Wl,--cref -Wl,--oformat=elf32-tricore -mcpu=tc27xx -Wl,--mem-holes -Wl,--extmap=&quot;a&quot;&quot;</OPTIONS>
                                <VENDOR>Hightec</VENDOR>
                                <VERSION>Hightec</VERSION>
                            </LINKER>
                        </LINKERS>
                        <PROGRAMMING-LANGUAGE>C</PROGRAMMING-LANGUAGE>
                        <RESOURCE-CONSUMPTION>
                            <SHORT-NAME>ResourceConsumption</SHORT-NAME>
                            <MEMORY-SECTIONS>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>WDG_17_SCU_CONST_32BIT</SHORT-NAME>
                                    <ALIGNMENT>32</ALIGNMENT>
                                    <SIZE>4</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/CONST</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>WDG_17_SCU_VAR_32BIT</SHORT-NAME>
                                    <ALIGNMENT>32</ALIGNMENT>
                                    <SIZE>4</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/VAR</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>WDG_17_SCU_VAR_16BIT</SHORT-NAME>
                                    <ALIGNMENT>16</ALIGNMENT>
                                    <SIZE>12</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/VAR</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>WDG_17_SCU_VAR_UNSPECIFIED</SHORT-NAME>
                                    <ALIGNMENT>UNSPECIFIED</ALIGNMENT>
                                    <SIZE>6</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/VAR</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>WDG_17_SCU_POSTBUILDCFG</SHORT-NAME>
                                    <ALIGNMENT>UNSPECIFIED</ALIGNMENT>
                                    <SIZE>48</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/CONFIG_DATA</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>WDG_17_SCU_CODE</SHORT-NAME>
                                    <ALIGNMENT>32</ALIGNMENT>
                                    <SIZE>1682</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/CODE</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                            </MEMORY-SECTIONS>
                            <SECTION-NAME-PREFIXS>
                                <SECTION-NAME-PREFIX>
                                    <SHORT-NAME>WDG_17_SCU</SHORT-NAME>
                                    <SYMBOL>WDG_17_SCU</SYMBOL>
                                </SECTION-NAME-PREFIX>
                            </SECTION-NAME-PREFIXS>
                        </RESOURCE-CONSUMPTION>
                        <SW-VERSION>0.0.0</SW-VERSION>
                        <VENDOR-ID>17</VENDOR-ID>
                        <AR-RELEASE-VERSION>4.0.3</AR-RELEASE-VERSION>
                        <BEHAVIOR-REF DEST="BSW-INTERNAL-BEHAVIOR">/AUTOSAR_Wdg/BswModuleDescriptions/Wdg/WdgBehavior</BEHAVIOR-REF>
                        <VENDOR-API-INFIX>Scu</VENDOR-API-INFIX>
                        <VENDOR-SPECIFIC-MODULE-DEF-REFS>
                                <VENDOR-SPECIFIC-MODULE-DEF-REF DEST="ECUC-MODULE-DEF">/AURIX/Wdg</VENDOR-SPECIFIC-MODULE-DEF-REF>
                        </VENDOR-SPECIFIC-MODULE-DEF-REFS>
                    </BSW-IMPLEMENTATION>
                </ELEMENTS>
            </AR-PACKAGE>
        </AR-PACKAGES>
    </AR-PACKAGE>
</AR-PACKAGES>
</AUTOSAR>
