#!/usr/bin/perl -w
#
# genld-SH4-renesasobj-gnuld.pl
#          - generate Gnu linker script to link renesas build objects
#
# $Id: genld-SH4-renesasobj-gnuld.pl 17602 2014-02-03 12:09:35Z tojo2507 $
#
# Usage:
# genld-gnu.pl outfile sysname objdir [-app|-task|-isr name objfile [...]] ...
#
# System, application and file names cannot contain spaces.
#
# Copyright 1998-2014 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#$DBG = 0;

print STDERR "Usage: $0 board-script-file num-taskstack-slots outfile system-name obj-dir group-align [app-spec ...]\n" and die
	if ( !defined $ARGV[1] );

$boardLinkerScript = shift;

$numTaskStackSlots = shift;
#$numKernStackSlots = shift; <-- needed for further versions

$outname = $ARGV[0];
$sysname = $ARGV[1];
$objdir = $ARGV[2];
$objdir =~ s%\\%/%g;        # replace dos \ into /
$objdir .='/';              # add ending /
$objdir =~ s%/+%/%g;        # strip /
$groupalign = $ARGV[3];


open(OUTFILE, ">$outname") or die "Unable to open $outname for writing\n";


my @appnames;	# Application/task names.
my @objtypes;	# Object types (APP or TASK).
my @first;		# First file in each application.
my @last;		# Last file (+1) in each application.
my @files;		# File names.
my @objdescr;	# Object type names.

# Get the date
my ($mm, $hh, $DD, $MM, $YY) = (localtime(time))[1,2,3,4,5];

my $genyear = sprintf("%04d", ($YY+1900));
my $gendate = sprintf("%04d-%02d-%02d %02d:%02d", ($YY+1900), ($MM+1), $DD, $hh, $mm);


# Parse all the app-specs in the command line.
$i = 4;			# Runs through ARGV
$j = 0;			# Counts no. of applications
$k = 0;			# Counts total no. of files.
$newapp = 1;	# So we don't save on the first time.
$objtype = 0;	# APP, TASK or ISR; 0 is "invalid"
$APP = 1;
$TASK = 2;
$ISR = 3;
$objdescr[0] = "XXX";
$objdescr[$APP] = "Application";
$objdescr[$TASK] = "Task";
$objdescr[$ISR] = "ISR";

while ( defined $ARGV[$i] ) {
	if ( $ARGV[$i] eq "-app" ) {
		# Introducing a new app.
		# Set the last file of the previous object if not the first time
		if ( ! $newapp ) {
			$last[$j] = $k;
			$j++;
		}
		$newapp = 1;
		$objtype = $APP;
	} elsif ( $ARGV[$i] eq "-task" ) {
		# Introducing a new task.
		# Set the last file of the previous object if not the first time
		if ( ! $newapp ) {
			$last[$j] = $k;
			$j++;
		}
		$newapp = 1;
		$objtype = $TASK;
	} elsif ( $ARGV[$i] eq "-isr" ) {
		# Introducing a new ISR.
		# Set the last file of the previous object if not the first time
		if ( ! $newapp ) {
			$last[$j] = $k;
			$j++;
		}
		$newapp = 1;
		$objtype = $ISR;
	} elsif ( $newapp ) {
		$appname = $ARGV[$i];
		$newapp = 0;

		die "Don't know whether $appname is an APPLICATION, TASK or ISR!\n"
			if ( !( ($objtype == $APP) ||
					($objtype == $TASK) ||
					($objtype == $ISR) ) );

		# Save the app name in the array.
		$appnames[$j] = $appname;
		$objtypes[$j] = $objtype;

		# Save the first file name for the app.
		$first[$j] = $k;
	} else {

		$file = $ARGV[$i];

		# Remove possible object-file suffixes.
		$file =~ s/\.o$//;
		$file =~ s/\.obj$//;

		# Save the file name in the array.
		$files[$k] = $file;
		$k++;
	}

	$i++;
}

if ( ! $newapp ) {
	$last[$j] = $k;
	$j++;
}

$n_apps = $j;

# Now write the output file

print OUTFILE qq~
/* Linker script for the $sysname system.
 *
 * GENERATED FILE: DO NOT EDIT
 * Generated by:
 *      $0
 *  on $gendate
 *
 * IMPORTANT A:
 *            All object files explicitly used within this
 *            linker script may not be specified second time within the linker
 *            command line. This results in multiple defined symbols.
 *            It is possible, but in this case the object path given by command
 *            line must be equal to the linker script used (string cmp eaqual).
 *            Mixed usage of relative or absolute path \ or / are not possible.
 * IMPORTANT B:
 *            Check the path of the object files after copy of the generated script.
 */
~;

# Open board script file and read int
open(DATA, $boardLinkerScript)||die "can not open file: $boardLinkerScript:\n";
my @lines = <DATA>;
foreach $line (@lines)
{
  print OUTFILE "$line";
}

print OUTFILE qq~
SECTIONS {

    EXCEPTIONTABLE : {
        ___EXCEPTION_TABLE_START = . ;  /* symbol used to simplify the offset creation */
        *(EXCEPTIONTABLE_0)
        . = ___EXCEPTION_TABLE_START + 0x0F0;
        *(KEYCODE)
        . = ___EXCEPTION_TABLE_START + 0x100;
        *(EXCEPTIONTABLE_100)
        . = ___EXCEPTION_TABLE_START + 0x0400;
        *(EXCEPTIONTABLE_400)
        . = ___EXCEPTION_TABLE_START + 0x0600;
        *(EXCEPTIONTABLE_600)
    } > EXCEPT

    OS_SYS_CODE : {
        *(SYSTEMSTART)
        *(OS_SYS_CODE)
    } > OS_SYS

    .text : {
        ___GLBL_TEXT_START = .;

~;
for ( $i = 0; $i < $n_apps; $i++ ) {
	$appname = $appnames[$i];
	if ( $first[$i] >= $last[$i] ) {
	    print OUTFILE "            /* $objdescr[$objtypes[$i]]: $appname --- NO FILES */\n";
    }
    else
    {
		for ( $j = $first[$i]; $j < $last[$i]; $j++ ) {
			$file = $files[$j];
			print OUTFILE "            /* $objdescr[$objtypes[$i]]: $appname */\n";
			print OUTFILE "        $objdir$file.o(P);\n";
		}
	}
}
print OUTFILE qq~
        *(P)
        ___GLBL_TEXT_END = .;
    } > ROM

    .rodata : {
        ___GLBL_RODATA_START = .;

~;
for ( $i = 0; $i < $n_apps; $i++ ) {
	$appname = $appnames[$i];
	if ( $first[$i] >= $last[$i] ) {
	    print OUTFILE "            /* $objdescr[$objtypes[$i]]: $appname --- NO FILES */\n";
    }
    else
    {
		for ( $j = $first[$i]; $j < $last[$i]; $j++ ) {
			$file = $files[$j];
			print OUTFILE "            /* $objdescr[$objtypes[$i]]: $appname */\n";
			print OUTFILE "        $objdir$file.o(C);\n";
		}
	}
}
# currently no explicit alignment for the stack section
print OUTFILE qq~
        *(C)
        ___GLBL_RODATA_END = .;
        *(R)
        ___IDATSTART = .;       /* data init can start here */
    } > ROM

    /* common os stack section.
     * Includes always available kernel stack. Kernel stack should be
     * located at start of ram. Overflow results in access to no existing
     * address that is catched by exception */
    .os_stacks_common :
    {
        *(Bos_kernstack)
        ___GLBL_STARTUPSTACK_START = .; /* stack grows down, therefore start is end of os_kernstack section */
        *(Bos_ehookstack)
        *(Bos_shookstack)
        *(Bos_taskstack)
    } > RAM

    /* stack slots are used if application based stack sharing is disabled
     * Each slot must be aligned to be separable */
    .os_stacks_slots :
    {
~;
for ( $i = 0; $i < $numTaskStackSlots; $i++ ) {
  print OUTFILE"        . = ALIGN($groupalign);\n";
  print OUTFILE"        *(Bos_taskstack_slot$i)\n";
}
print OUTFILE qq~
    } > RAM

    /* Possible memory protection stack sharing variants
     * secifies memory protected layout according configuration
     *    -    global stack sharing (default)
     *    - or application based stack sharing
     *
     * global stack sharing (default)
     *   All object stacks are located in .os_stack_slots.
     *   Application objects (%) related .stack_% section are not used/empty.
     *
     * application based stack sharing
     *   (not Autosar confrom, but reduce the number of needed TLBs because
     *    stack and application data can be covered together)
     *   .os_stacks_slots section is generally not used (in future it could
     *   be that we place some trusted stacks there)
     *   The application related object stacks are placed into .stack_%
     *   Where % is the application name. The application data can be located
     *   directly behind.
     *
     * For both schema
     *   It must be possible that isr/tasks always can have their private data.
     *   Therefore task/isr grouped {.data_% and .bss_%} must always aligned.
     */

~;

$datastart = "___IDATSTART";

for ( $i = 0; $i < $n_apps; $i++ ) {
	$appname = $appnames[$i];
	if ( $first[$i] >= $last[$i] ) {
		print OUTFILE "    /* $objdescr[$objtypes[$i]]: $appname --- NO FILES */\n";
		print OUTFILE "    ___DATA_$appname = 0;\n";
		print OUTFILE "    ___DEND_$appname = 0;\n";
		print OUTFILE "    ___IDAT_$appname = $datastart;\n";
		print OUTFILE "    ___IEND_$appname = $datastart;\n";

	} else {
		if ( $objtypes[$i] == $APP ) {
		    print OUTFILE "    /************************************************\n";
		    print OUTFILE "     * $objdescr[$objtypes[$i]]: $appname\n";
		    print OUTFILE "    ************************************************/\n";
		    print OUTFILE "    .stack_$appname :\n";
		    print OUTFILE "    {\n";
		    print OUTFILE "        /* see description above */\n";
		    print OUTFILE "        . =  ALIGN($groupalign);\n";
		    print OUTFILE "        /* aligned placement to detect stack overflow without overwrite of data */\n";
			print OUTFILE "        $objdir$appname-gen.o(Bos_stack_$appname)\n";
			print OUTFILE "        $objdir$appname-gen.o(Bos_taskstack_$appname)\n";
			print OUTFILE "    } > RAM\n";
    	    print OUTFILE "    .data_$appname : AT($datastart)\n";
	    	print OUTFILE "    {\n";
	    	print OUTFILE "        /* no align needed because this is done by previous .stack_* section */\n" ;
		}
		else
		{
		   print OUTFILE "    /* $objdescr[$objtypes[$i]]: $appname */\n";
    	   print OUTFILE "    .data_$appname : AT($datastart)\n";
	       print OUTFILE "    {\n";
	       print OUTFILE "        . = ALIGN($groupalign) ;\n";
		}
		print OUTFILE "        ___DATA_$appname = .;\n";
		for ( $j = $first[$i]; $j < $last[$i]; $j++ ) {
			$file = $files[$j];
			print OUTFILE "        $objdir$file.o(D)\n";
		}
		print OUTFILE "    } > RAM\n";
		print OUTFILE "    .bss_$appname :\n";
		print OUTFILE "    {\n";
		for ( $j = $first[$i]; $j < $last[$i]; $j++ ) {
			$file = $files[$j];
			print OUTFILE "        $objdir$file.o(B)\n";
		}
		print OUTFILE "        ___DEND_$appname = .;\n";
		print OUTFILE "    } > RAM\n";
		print OUTFILE "    ___IDAT_$appname = $datastart;\n";
		print OUTFILE "    ___IEND_$appname = ___IDAT_$appname + SIZEOF(.data_$appname);\n";
	}
	print OUTFILE "\n";
	$datastart = "___IEND_$appname";
}
print OUTFILE qq~
    .data : AT($datastart) {
        . =  ALIGN($groupalign); /* force alignment of global data/bss */
        ___GLBL_RAM_START = .;
        ___GLBL_DATA_START = .;
        *(D)
        ___GLBL_DATA_END = .;
    } > RAM
    .bss : {
        ___GLBL_BSS_START = .;
        *(B)
        ___GLBL_BSS_END = .;
        . = ALIGN(4);
        ___GLBL_HEAP_START = .;
        . = . + 1024;
        ___GLBL_HEAP_END = .;
        ___GLBL_RAM_END = .;
    } > RAM

    ___INITDATA = $datastart;

    ___GLBL_INIT_START = ___INITDATA;

    ___GLBL_TEXT_SIZE = ___GLBL_TEXT_END - ___GLBL_TEXT_START;
    ___GLBL_RAM_SIZE = ___GLBL_RAM_END - ___GLBL_RAM_START;
    ___GLBL_RODATA_SIZE = ___GLBL_RODATA_END - ___GLBL_RODATA_START;
    ___GLBL_HEAP_SIZE = ___GLBL_HEAP_END - ___GLBL_HEAP_START;

    __GLBL_RAMDESC_USED = __GLBL_RAMDESC_GNU;

  /* These sections are DWARF-2 debug sections. They contain
   * ELF relocations and must be located at zero.
  */
    . = 0x0;
    .debug_aranges		: { *(.debug_aranges)	}
    . = 0x0;
    .debug_pubnames		: { *(.debug_pubnames)	}
    . = 0x0;
    .debug_info			: { *(.debug_info)	}
    . = 0x0;
    .debug_abbrev		: { *(.debug_abbrev)	}
    . = 0x0;
    .debug_line			: { *(.debug_line)	}
}
~;


# Editor settings: DO NOT DELETE
# vi:set ts=4:
