#!/usr/bin/perl

# small simple script to convert the SH4 s3record
# (including virtual addresses) to the physical address
# by simple clear of upper address byte (the real HW only clears the upper 3 bits)

# memory overflow and section overlapping not checked, detected and handled

####################################
# check input
####################################
if( $#ARGV < 0 )
{
   print "usage: $0 s3recFile";
   exit;
}
my $file=$ARGV[0];
if( ! -f $file )
{
    die "can not open S3record input file.";
}
open (IN,"<$file") or die "Could not open file $file.";
my @content=<IN>;
close IN;

foreach my $line (@content)
{
    $line=~s/\n|\r//g;

    my $prefix=substr($line,0,2);
    my $count=substr($line,2,2);
    my $address=substr($line,4,8);
    my $data=substr($line,12,length($line)-12-2);
    my $chksum=substr($line,length($line)-2,2);

    if( $prefix eq 'S3')
    {
        my @adr=split('',$address);
        # higest address bit set
        my $offset=hex($adr[0]);
        if( $offset > 8 )
        {
            # move P1,P2,P3 to the physical space
            $adr[0]=0;
            $address=join('',@adr);
            # update checksum
            my @chk=split('',$chksum);
            $chk[0]=sprintf("%X",(hex($chk[0])+$offset)%16);
            $chksum=join('',@chk);
        }
        print "$prefix"."$count"."$address"."$data"."$chksum"."\n";
    }
    else
    {
        print "$line\n";
    }
}