#!/usr/bin/perl -w
#
# genld-ARM-realview.pl - generate linker script for RealView RVCT, ARM variant.
#
# $Id: genld-ARM-realview.pl 20508 2015-02-19 13:32:26Z stpo8218 $
#
# Usage:
# genld-ARM-realview.pl (outfile) objdir (ignored) (ignored) [-app|-task|-isr name objfile [...]] ...
#
# System, application and file names cannot contain spaces.
#
# Copyright 1998-2015 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

print STDERR "Usage: $0 outfile objdir (ignored) (ignored) [app_spec ...]\n"
	and die if (! defined $ARGV[1]);

my $outname = shift;
my $objdir = getDotlessPath(shift);

my $sysname = shift; # FUTURE: remove parameter
my $ramorflash = shift; # FUTURE: remove parameter

my $glbltextsize = 0;
my $glblconstsize = 0;
my $glbldatasize = 0;

open(OUTFILE, ">$outname") or die "$0: Unable to open $outname for writing: $!\n";


# Return a path that does not contain "..". Poor man's implementation of
# realpath(3).
sub getDotlessPath
{
	my $pathName = shift;
	# replaces backslashes with slashes.
	$pathName =~ s/\\/\//g;

	my @pathparts = split(/\//, $pathName);
	my $ret = "";
	my $skipNext = 0;

	for my $p (reverse @{pathparts})
	{
		if ($p eq '..')
		{
			$skipNext++;
		}
		else
		{
			if ($skipNext == 0)
			{
				$ret = $p . "/" . $ret;
			}
			else
			{
				$skipNext--;
			}
		}
	}

	return $ret;
}

# Convert a decimal or hexadecimal string to a numeric value.
# Returns the numeric value.
sub HexDecStringToNumber
{
	my $strNumber = shift; # String to convert.
	my $base = 10;
	my $ret = 0;

	# check, if a Hex prefix is present
	if (substr($strNumber, 0, 2) eq "0x")
	{
		$base = 16;
		# cut off '0x'
		$strNumber = substr($strNumber, 2, length($strNumber) - 2);
	}

	# convert to decimal. Note: The shipped perl does not include the POSIX
	# module, so we have to do it by hand.
	for (my $i = 0; $i < length($strNumber); $i++)
	{
		my $digit = ord(substr($strNumber, $i, 1)) - ord('0');
		$ret = $ret * $base;
		$ret += $digit;
	}

	return $ret;
}

# ===========================================================================
# parse command line
# ===========================================================================

my @apps;	# Application/task names.
my @stacks; # Stack sections and MPU sizes

# Get the date
my ($mm, $hh, $DD, $MM, $YY) = (localtime(time))[1,2,3,4,5];
my $gendate = sprintf("%04d-%02d-%02d %02d:%02d", ($YY+1900), ($MM+1), $DD, $hh, $mm);

# 2nd: Parse all the app-specs in the command line.
# ---------------------------------------------------------------------------
my %objdescs =
(
	'app' => "Application",
	'task' => 'Task',
	'isr' => 'ISR',
);

for(my $i=0; $i < @ARGV; $i++)
{
	if ( $ARGV[$i] =~ /^-(app|task|isr)$/ )
	{
		die "Too few parameters" unless $i<($#ARGV + 1);

		# Save the app name in the array.
		unshift @apps,
		{
			'name' => $ARGV[++$i],
			'regionsize' => HexDecStringToNumber($ARGV[++$i]),
			'type' => $objdescs{$1},
			'files' => [ ],
		};
	}
	elsif ( $ARGV[$i] eq '-stack' )
	{
		# -stack STACKSECTION MPUSIZE
		die "Too few parameters!" unless ($i + 1) < $#ARGV;

		# remember section and MPU size.
		push @stacks,
		{
			'section' => $ARGV[++$i],			# Note: Section is without preceding '.'.
			'regionsize' => HexDecStringToNumber($ARGV[++$i]),
		};
	}
	elsif ( $ARGV[$i] eq '-glbtextsize')
	{
		$glbltextsize = HexDecStringToNumber($ARGV[++$i]);
	}
	elsif ( $ARGV[$i] eq '-glbconstsize')
	{
		$glblconstsize = HexDecStringToNumber($ARGV[++$i]);
	}
	elsif ( $ARGV[$i] eq '-glbdatasize')
	{
		$glbldatasize = HexDecStringToNumber($ARGV[++$i]);
	}
	else
	{

		my $file = $ARGV[$i];
		push @{$apps[0]{'files'}}, $file;
	}
}

# restore input order
@apps = reverse @apps;
# sort according to region size to minimize alignment gaps
@apps = sort { $a->{'regionsize'} cmp $b->{'regionsize'} } @apps;
# sort stacks according to region size as well.
@stacks = sort { $a->{'regionsize'} cmp $b->{'regionsize'} } @stacks;

# ===========================================================================
# local functions
# ===========================================================================

# Print symbols for an unused section.
# Since we cannot create symbols, we define an empty section.
sub RegionUnused
{
	my $name = shift;			# exact name of the section (used in symbols)
	my $comment = shift;	# informative comment about section

	print OUTFILE <<EOF;
	/* Section $name is unused: */
	/* ${comment} */
	${name}_data +0 EMPTY 0
	{
		/* nothing */
	}
	${name}_ALIGN_HOLE +0 EMPTY 0
	{
		/* nothing */
	}

EOF
}



# print one output section representing a memory region
# by a list of objects from which .data and .bss is extracted
#
# A memory region is mapped to two output sections. The first one
# contains the initialized data, the second one the uninitialized
# data.
# The kernel initializes the initialized data from the ROM image
# using from the range __IDAT_<name> to __IEND_<name>. The remainder
# of the region (i.e. __DATA_<name> + (__IEND_<name> - __IDAT_<name>))
# must be the BSS portion and is initialized with zero.
sub RegionByObjects
{
	my $name = shift;			# exact name of section (used in symbols)
	my $comment = shift;		# informative comment about section
	my $objects = shift;		# objects, from which data and bss is extracted
	my $palign = shift;			# align and pad section to palign bytes

	print OUTFILE <<EOF;
	/* ${comment} */
	${name}_data AlignExpr(+0, $palign)
	{
EOF
	for my $object (@{$objects})
	{
		print OUTFILE "\t\t${object}(+RW-DATA)\n";
	}

	print OUTFILE <<EOF;
	}
	${name}_bss +0 UNINIT
	{
EOF
	for my $object (@{$objects})
	{
		print OUTFILE "\t\t${object}(+ZI)\n";
	}

	print OUTFILE <<EOF;
	}
	${name}_ALIGN_HOLE AlignExpr(+0, $palign) EMPTY 0
	{
		/* nothing */
	}
EOF
}

# print one output section representing a memory region
# by an input section name. The input section is mapped as a NOLOAD section (like .bss).
sub RegionBySection
{
	my $name = shift;			# exact name of section (used in symbols)
	my $comment = shift;		# informative comment about section
	my $inputSec = shift;		# name of input section
	my $palign = shift;			# align and pad section to palign bytes

	print OUTFILE <<EOF;
	/* ${comment} */
	${name}_data AlignExpr(+0, $palign) UNINIT $palign
	{
		*(${inputSec})
	}
	${name}_ALIGN_HOLE AlignExpr(+0, $palign) EMPTY 0
	{
		/* nothing */
	}

EOF
}

# generate OS stack sections
sub genStackSections
{
	my $i = 0;

	foreach my $stack (@stacks)
	{
		my $section = '.' . $stack->{'section'};
		my $regionsize = $stack->{'regionsize'};
		my $stackname = "os_stack_$i";

		if ($regionsize != 0)
		{
			RegionBySection($stackname, "Os Stack $i", $section, $regionsize);
		}
		else
		{
			RegionUnused($stackname, "Os Stack $i");
		}
		$i++;
	}
}


# ===========================================================================
# header:
# ===========================================================================
print OUTFILE <<EOF;
#! armcc -E
/* Linker script for ARM RealView (ARM).
 *
 * GENERATED FILE: DO NOT EDIT
 * Generated by $0 on $gendate
 *
 */
EOF

# Print contents of memory definitions file.
open(my $BMD, '<', "$objdir/../generated/board_memory_defs.ldscript")
	or die "Could not open file <$objdir/../generated/board_memory_defs.ldscript> $!";

while (my $line = <$BMD>) {
	print OUTFILE "$line";
}
close $BMD;

# ===========================================================================
# static: exception table
# ===========================================================================
print OUTFILE <<EOF;

LOAD_EXCEPTIONTABLE EXCTABLE ABSOLUTE EXCTABLE_SIZE
{
	VMA_EXCEPTIONTABLE EXCTABLE FIXED EXCTABLE_SIZE
	{
		*(ExceptionTable)
	}
}
EOF

# ===========================================================================
# static: Huge load region that comprises all code and data
# ===========================================================================
print OUTFILE <<EOF;
LOAD_FLASH FLASH NOCOMPRESS FLASH_SIZE
{
	/* Dummy to set initial FLASH address. */
	OS_DUMMY_FLASH_BEGIN FLASH EMPTY 0
	{
		/* nothing */
	}
	/* Constants */
	OS_CONSTANTS +0	ALIGN $glblconstsize ABSOLUTE $glblconstsize
	{
		*(+RO-DATA)
	}
	OS_CONSTANTS_ALIGN_HOLE +0 ALIGN $glblconstsize EMPTY 0
	{
		/* nothing */
	}

	TEXT +0 ALIGN $glbltextsize ABSOLUTE $glbltextsize
	{
		*(.os_text*)
		*(+RO-CODE)
	}
	/* After the text section, we do not place an alignment gap to place
	 * the ROM image of initialized data right behind it. Memory protection
	 * is not yet active when the data is initialized.
	 *
	 * However, this makes (parts of) the ROM image executable. To avoid this,
	 * place an alignment gap here at the expense of wasted flash.
	*/

	/* Set VMA location pointer to begin of RAM.
	 * Also force alignment and create a section from which we can take the
	 * start symbol.
	 * (global RAM comprises all data!).
	*/
	OS_GLBL_RAM_START AlignExpr(RAM, $glbldatasize) EMPTY 0
	{
		/* nothing */
	}

EOF

# ===========================================================================
# Stacks: BSS sections
# ===========================================================================
genStackSections();

# ===========================================================================
# Tasks / Isrs / Applications: Private data / bss section.
# ===========================================================================

foreach my $app (@apps)
{
	my $appname = $app->{'name'};
	my $objtype = $app->{'type'};
	my $files   = $app->{'files'};
	my $regionsize = $app->{'regionsize'};

	if (   ($objtype eq $objdescs{"app"})
		|| ($objtype eq $objdescs{"task"})
		|| ($objtype eq $objdescs{"isr"}) )
	{
		if ($regionsize == 0)
		{
			RegionUnused($appname, "$objtype $appname data/bss");
		}
		else
		{
			RegionByObjects($appname,
											"$objtype
											$appname data/bss",
											\@$files,
											$regionsize);
		}
	}
	else
	{
		die("Unknown type $objtype!");
	}
}

# ===========================================================================
# static portions: .data/.bss, .const and .text.
# ===========================================================================

# Note: You might want to reorder .text/.const to minimize alignment gaps.
print OUTFILE <<EOF;
	/* Anonymous .data/.bss section. Part of the global ram section, hence
	 * no special alignment is required.
	*/
	OS_ANON_data AlignExpr(+0, 4)
	{
		*(+RW-DATA)
	}
	OS_ANON_bss +0 UNINIT
	{
		*(+ZI)
	}
	/* Alignment hole + end marker of the global RAM section. */
	OS_GLBL_RAM_END AlignExpr(+0, $glbldatasize) EMPTY 0
	{
		/* empty */
	}

EOF
# ===========================================================================
# Footer:
# ===========================================================================

print OUTFILE <<EOF;
}
EOF

# Editor settings: DO NOT DELETE
# vi:set ts=2 sw=2:
