# \file
#
# \brief AUTOSAR Os
#
# This file contains the implementation of the AUTOSAR
# module Os.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2014 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

# This file defines the used lib suffixes.
# Each flag has a global prepocessor define and one upper case letter in
# the lib suffix.

OS_LIB_EMPTY=
OS_LIB_SPACE=$(OS_LIB_EMPTY) $(OS_LIB_EMPTY)

#############################################
# common lib flag definitions
#############################################
OS_COMMON_LIB_FLAG_DEFAULT=o
OS_COMMON_LIB_FLAGS = os_kerneltype os_trace os_clz os_devsanity os_memmodel os_memmap os_multicore

#############
# kernel interface
#
# syscall, no syscall, MicroOs
os_kerneltype_KEY=OS_KERNEL_TYPE
ifeq ($(OS_KERNEL_TYPE),MICROKERNEL)
os_kerneltype_VALUE=OS_MICROKERNEL
os_kerneltype_FLAG=M
else
ifeq ($(OS_KERNEL_TYPE),SYSTEM_CALL)
os_kerneltype_VALUE=OS_SYSTEM_CALL
os_kerneltype_FLAG=S
else
ifeq ($(OS_KERNEL_TYPE),FUNCTION_CALL)
os_kerneltype_VALUE=OS_FUNCTION_CALL
os_kerneltype_FLAG=F
else
ifneq ($(OS_KERNEL_TYPE),)
$(error "supported OS_KERNEL_TYPE are MICROKERNEL, SYSTEM_CALL or FUNCTION_CALL but is currently $(OS_KERNEL_TYPE)")
endif
endif
endif
endif

#############
# trace
#
os_trace_KEY=OS_USE_TRACE
ifeq ($(OS_TRACE),TRUE)
os_trace_VALUE =1
os_trace_FLAG  =T
else
os_trace_VALUE =0
os_trace_FLAG  =$(OS_COMMON_LIB_FLAG_DEFAULT)
endif

#############
# clz
#
os_clz_KEY=OS_USE_CLZ_QUEUE_ALGORITHM
ifeq ($(OS_USE_LINKED_LIST_TASK_QUEUE),TRUE)
os_clz_VALUE   =0
os_clz_FLAG    =L
else
os_clz_VALUE   =1
os_clz_FLAG    =R
endif


#############
# devsanity
#
os_devsanity_KEY =OS_DEVELOPMENT_SANITY_CHECKS
ifeq ($(OS_DEVELOPMENT_SANITY_CHECKS),TRUE)
os_devsanity_VALUE =1
os_devsanity_FLAG  =S
else
os_devsanity_VALUE =0
os_devsanity_FLAG  =$(OS_COMMON_LIB_FLAG_DEFAULT)
endif

#############
# memmodel
#
ifneq ($(MEMORY_MODEL),)
OS_MEMORY_MODEL=$(MEMORY_MODEL)
endif
OS_MEMORY_MODEL?=DEFAULT
os_memmodel_KEY =OS_MEMORY_MODEL
os_memmodel_VALUE=OS_MM_$(OS_MEMORY_MODEL)

ifeq ($(OS_MEMORY_MODEL),FAR)
os_memmodel_FLAG=F
else
ifeq ($(OS_MEMORY_MODEL),TINY)
os_memmodel_FLAG=T
else
ifeq ($(OS_MEMORY_MODEL),NEAR)
os_memmodel_FLAG=N
else
ifeq ($(OS_MEMORY_MODEL),SMALL)
os_memmodel_FLAG=S
else
ifeq ($(OS_MEMORY_MODEL),HUGE)
os_memmodel_FLAG=H
else
ifeq ($(OS_MEMORY_MODEL),BANKED)
os_memmodel_FLAG=B
else
ifeq ($(OS_MEMORY_MODEL),DEFAULT)
os_memmodel_FLAG=$(OS_COMMON_LIB_FLAG_DEFAULT)
else
$(error "Memory model: $(OS_MEMORY_MODEL) not supported")
endif
endif
endif
endif
endif
endif
endif

#############
# memmap
#
os_memmap_KEY =OS_MEMMAP

ifeq ($(OS_INCLUDE_MEMMAP_SUPPORT),TRUE)
os_memmap_VALUE =1
os_memmap_FLAG =M
else
os_memmap_VALUE = 0
os_memmap_FLAG =$(OS_COMMON_LIB_FLAG_DEFAULT)
endif

#############
# multicore
#
os_multicore_KEY =OS_MULTICORE

ifeq ($(OS_MULTICORE_TYPE),MANY)
os_multicore_VALUE =OS_MANY
os_multicore_FLAG =M
OS_AUTOSAR_SPINLOCKS=TRUE
else
ifeq ($(OS_MULTICORE_TYPE),DUAL)
os_multicore_VALUE =OS_DUAL
os_multicore_FLAG =D
OS_AUTOSAR_SPINLOCKS=TRUE
else
ifeq ($(OS_MULTICORE_TYPE),SINGLE)
os_multicore_VALUE = OS_SINGLE
os_multicore_FLAG =S
else
ifneq ($(OS_MULTICORE_TYPE),)
$(error "supported values for OS_MULTICORE_TYPE are SINGLE, DUAL or MANY but is currently $(OS_MULTICORE_TYPE)")
endif
endif
endif
endif

#######################################################
# get architecture specific lib flags
#
include $(AutosarOS_CORE_PATH)/make/$(OS_ARCH)/Os_libsuffix_$(OS_ARCH).mak

#######################################################
# prepare all defaults
#
define OS_DefineDefaultLibFlag
$(1)_FLAG?=$(OS_COMMON_LIB_FLAG_DEFAULT)
endef
define OS_GetLibFlag
$($(1)_FLAG)
endef

$(foreach flag,$(OS_COMMON_LIB_FLAGS),$(eval $(call OS_DefineDefaultLibFlag,$(flag))))
$(foreach flag,$(OS_ARCH_LIB_FLAGS),$(eval $(call OS_DefineDefaultLibFlag,$(flag))))

#######################################################
# FINALLY: construct the flag string and provide one global preprocessor
#          define for each used flag
#
OS_COMMON_LIB_FLAG_STRING:=$(subst $(OS_LIB_SPACE),$(OS_LIB_EMPTY),$(foreach flag,$(OS_COMMON_LIB_FLAGS),$(call OS_GetLibFlag,$(flag))))
OS_ARCH_LIB_FLAG_STRING:=$(subst $(OS_LIB_SPACE),$(OS_LIB_EMPTY),$(foreach flag,$(OS_ARCH_LIB_FLAGS),$(call OS_GetLibFlag,$(flag))))

ifneq ($(OS_ARCH_LIB_FLAG_STRING),)
OS_ARCH_LIB_FLAG_STRING:=_$(OS_ARCH_LIB_FLAG_STRING)
endif

OS_LIB_SUFFIX=$(OS_COMMON_LIB_FLAG_STRING)$(OS_ARCH_LIB_FLAG_STRING)

# publish preprocessor defines
PREPROCESSOR_DEFINES += $(OS_COMMON_LIB_FLAGS) $(OS_ARCH_LIB_FLAGS)

#########################
# Editor settings: DO NOT DELETE
# vi:set ts=4:
