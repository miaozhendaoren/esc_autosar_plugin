/* Os_configuration_microkernel.c
 *
 * This file contains the architecture-independent kernel configuration data for a microkernel-based system
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_configuration_microkernel.c 20487 2015-02-18 11:33:00Z stpo8218 $
*/

#include <Os_configuration.h>
#include <Os_kernel.h>
#include <Os_removeapi.h>
#include <Mk_qmboard.h>
#include <private/Mk_oscall.h>

#if (OS_IOC_ENABLED==1)
#include <Ioc/Ioc_kern.h>
#endif /* OS_IOC_ENABLED */

#ifndef OS_GENERATION_ID_OS_H
#error "OS_GENERATION_ID_OS_H is not defined."
#endif

#ifndef OS_GENERATION_ID_OS_CONF_H
#error "OS_GENERATION_ID_OS_CONF_H is not defined"
#endif

#if defined(OS_GENERATION_ID_OS_H) && defined(OS_GENERATION_ID_OS_CONF_H)

#if (OS_GENERATION_ID_OS_H == OS_GENERATION_ID_OS_CONF_H)

#include <memmap/Os_mm_const32_begin.h>
const os_uint32_t OS_configurationId = OS_GENERATION_ID_OS_H;
#include <memmap/Os_mm_const32_end.h>

#else
#error "OS_GENERATION_ID_OS_H and OS_GENERATION_ID_OS_CONF_H are different"
#endif

#endif

/*!
 * OS_configMode
 *
 * This constant holds various aspects of the kernel's configuration.
 * There are sevaral bitfields, representing configuration modes
 *  - BCC1/BCC2/ECC1/ECC2
 *  - STANDARD/EXTENDED
 *  - etc. See Os_kernel.h
*/
#include <memmap/Os_mm_const16_begin.h>
const os_uint32_t OS_configMode = OS_CONFIGMODE;
#include <memmap/Os_mm_const16_end.h>

/*!
 * OS_nStartModes
 *
 * This constant contains the number of startup modes that have been
 * configured. There must be at least 1 startup mode.
*/
#if OS_NSTARTMODES==0

#error "Configuration error: there must be at least 1 startup mode"

#else

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nStartModes = OS_NSTARTMODES;
#include <memmap/Os_mm_const8_end.h>

#endif

/*!
 * OS_startModeTasks
 *
 * This array contains, for each defined start mode (of which there is
 * at least one) an index into the OS_autoStartTasks array.
 *
 * OS_autoStartTasks
 *
 * This array contains the ids of the tasks that should be started for
 * each start mode. Each mode's tasks are terminated by the null task id.
 * The index of the first entry for each mode is stored in the appropriate
 * location in OS_startModeTasks
*/
#ifdef OS_STARTMODETASKS

#define OS_CFG_ACTIVATEAUTOTASKS	OS_ActivateAutoTasks

#include <memmap/Os_mm_const16_begin.h>
const os_uint16_t OS_startModeTasks[OS_NSTARTMODES] = { OS_STARTMODETASKS };
#include <memmap/Os_mm_const16_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_taskid_t OS_autoStartTasks[] = { OS_AUTOSTARTTASKS };
#include <memmap/Os_mm_const_end.h>

#endif

/*!
 * OS_startModeAlarms
 *
 * This array contains, for each defined start mode (of which there is
 * at least one) an index into the OS_autoStartAlarms array.
 *
 * OS_autoStartAlarms
 *
 * This array contains the ids, times and cycles of the alarms that should
 * be started for each start mode. Each mode's alarms are terminated by the
 * null alarm id.
 * The index of the first entry for each mode is stored in the appropriate
 * location in OS_startModeAlarms
*/
#ifdef OS_STARTMODEALARMS

#define OS_CFG_ACTIVATEAUTOALARMS	OS_ActivateAutoAlarms

#include <memmap/Os_mm_const16_begin.h>
const os_uint16_t OS_startModeAlarms[OS_NSTARTMODES] = { OS_STARTMODEALARMS };
#include <memmap/Os_mm_const16_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_autoalarm_t OS_autoStartAlarms[] = { OS_AUTOSTARTALARMS };
#include <memmap/Os_mm_const_end.h>

#endif

/*!
 * OS_startModeSchedules
 *
 * This array contains, for each defined start mode (of which there is
 * at least one) an index into the OS_autoStartSchedules array.
 *
 * OS_autoStartSchedules
 *
 * This array contains the ids and offsets of the schedule tables that should
 * be started for each start mode. Each mode's schedule tables are terminated by the
 * null schedule table id.
 * The index of the first entry for each mode is stored in the appropriate
 * location in OS_startModeSchedules
*/
#ifdef OS_STARTMODESCHEDULES

#define OS_CFG_ACTIVATEAUTOSCHEDULES	OS_ActivateAutoSchedules

#include <memmap/Os_mm_const16_begin.h>
const os_uint16_t OS_startModeSchedules[OS_NSTARTMODES] = { OS_STARTMODESCHEDULES };
#include <memmap/Os_mm_const16_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_autoschedule_t OS_autoStartSchedules[] = { OS_AUTOSTARTSCHEDULES };
#include <memmap/Os_mm_const_end.h>

#endif

/* Applications
*/
#if OS_NAPPS <= 0
const mk_uint32_t OS_appTable[1] = { 0 };
#else
const mk_uint32_t OS_appTable[OS_NAPPS] = { 0 };
#endif

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nApps = OS_NAPPS;
#include <memmap/Os_mm_const8_end.h>

/*!
 * OS_nCounters
 *
 * This constant contains the number of counters configured.
 *
 * OS_counter
 *
 * This array contains the static part of the counters.
 *
 * We can omit the IncrementCounter system call if all the counters
 * are hardware counters; ie if NCOUNTERS is <= NHWTIMERS
*/
#if OS_NCOUNTERS==0

#define OS_K_GETCOUNTERVALUE		OS_KernUnknownSyscall
#define OS_K_GETELAPSEDCOUNTERVALUE	OS_KernUnknownSyscall

#define OS_CTRBASE				OS_NULL
#define OS_CTRDYNAMICBASE		OS_NULL

#else

#define OS_CFG_INITCOUNTERS		OS_InitCounters
#define OS_CTRBASE				OS_counter
#define OS_CTRDYNAMICBASE		OS_counterDynamic

#include <memmap/Os_mm_const_begin.h>
const os_counter_t OS_counter[OS_NCOUNTERS] = { OS_COUNTER };
#include <memmap/Os_mm_const_end.h>

#endif

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nCounters = OS_NCOUNTERS;
#include <memmap/Os_mm_const8_end.h>


#include <memmap/Os_mm_const_begin.h>
const os_counter_t * const OS_counterTableBase = OS_CTRBASE;
os_counterdynamic_t * const OS_counterDynamicBase = OS_CTRDYNAMICBASE;
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_nAlarms
 *
 * This constant contains the number of OSEK alarms configured.
 *
 * OS_totalAlarms
 *
 * This constant contains the total number of alarms configured. The
 * figure includes alarms that are present for other purposes such as
 * schedule tables etc.
 *
 * OS_alarm
 *
 * This array contains the static part of the alarms.
*/
#define OS_TOTALALARMS	(OS_NALARMS + OS_NSCHEDULES + OS_NINTERNALALARMS)

#if OS_NALARMS==0

/* No real alarms. We can set all related system calls to "Unknown system call".
 * Note that there may be some alarms in the table due to
 * configured ScheduleTables, but these are not started or
 * manipulated using the normal alarm APIs.
*/
#define OS_K_GETALARM			OS_KernUnknownSyscall
#define OS_K_SETRELALARM		OS_KernUnknownSyscall
#define OS_K_SETABSALARM		OS_KernUnknownSyscall
#define OS_K_CANCELALARM		OS_KernUnknownSyscall

#endif

#if OS_TOTALALARMS==0
/* No alarms. We can turn off the initialisation function.
*/

#define OS_ALARMBASE			OS_NULL
#define OS_ALARMDYNAMICBASE		OS_NULL
#define OS_KILLALARMFUNC		(os_killalarmfunc_t)OS_NullFunction

#else

#define OS_CFG_INITALARMS		OS_InitAlarms
#define OS_ALARMBASE			OS_alarm
#define OS_ALARMDYNAMICBASE		OS_alarmDynamic

#ifdef OS_EXCLUDE_KILLALARM
#define OS_KILLALARMFUNC		(os_killalarmfunc_t)OS_NullFunction
#else
#define OS_KILLALARMFUNC		OS_KillAlarm
#endif

#include <memmap/Os_mm_const_begin.h>
const os_alarm_t OS_alarm[OS_TOTALALARMS] = { OS_ALARM };
#include <memmap/Os_mm_const_end.h>

#if OS_NALARMCALLBACKS != 0
#include <memmap/Os_mm_const_begin.h>
const os_alarmcallback_t OS_alarmCallback[OS_NALARMCALLBACKS] = { OS_ALARMCALLBACK };
#include <memmap/Os_mm_const_end.h>
#endif

#endif

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nAlarms = OS_NALARMS;
const os_uint8_t OS_totalAlarms = OS_TOTALALARMS;
#include <memmap/Os_mm_const8_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_alarm_t * const OS_alarmTableBase = OS_ALARMBASE;
os_alarmdynamic_t * const OS_alarmDynamicBase = OS_ALARMDYNAMICBASE;
const os_killalarmfunc_t OS_killAlarmFunc = OS_KILLALARMFUNC;
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_nHwTimers
 *
 * This constant contains the number of hardware timers that need
 * initialising.
 *
 * OS_hwTimer
 *
 * This array contains an element for each hardware timer that needs
 * initialising.
 *
 * The format of the array is architecture-dependent, but includes a pointer
 * to a function that takes the address of the os_hwt_t as its
 * only parameter. Timer drivers therefore need to be written to conform
 * to this specification.
*/
const os_uint8_t OS_nHwTimers = OS_NHWTIMERS;

#if OS_NHWTIMERS!=0

#define OS_CFG_INITTIMERS	OS_InitTimers

#include <memmap/Os_mm_var_begin.h>
os_timervalue_t OS_hwtLastValue[OS_NHWTIMERS];
#include <memmap/Os_mm_var_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_hwt_t OS_hwTimer[OS_NHWTIMERS] = { OS_HWTIMER };
#include <memmap/Os_mm_const_end.h>

#endif

/*
 * OS_timeStampTimer
 *
 * Configuration for timestamp (used by CPU load measurement and possibly others)
 *
 * If not configured, we choose OS_NULL. This supports older versions of the generator.
*/
#if OS_USEGENERICTIMESTAMP

#ifdef OS_TIMESTAMPTIMERINDEX

#define OS_TIMESTAMPTIMER				&OS_hwTimer[OS_TIMESTAMPTIMERINDEX]

#else

#define OS_TIMESTAMPTIMER				OS_NULL

#endif

#include <memmap/Os_mm_const_begin.h>
const os_hwt_t * const OS_timeStampTimer = OS_TIMESTAMPTIMER;
#include <memmap/Os_mm_const_end.h>

#endif

#if defined(OS_INITTIMESTAMP)
#define OS_CFG_INITTIMESTAMP		OS_INITTIMESTAMP
#endif

/*!
 * OS_nSchedules
 *
 * This constant contains the number of schedule tables configured.
 *
 * OS_schedule
 *
 * This array contains an element for each schedule table configured.
 *
 * OS_stEntries
 *
 * This array contains all the expiry points for all the configured
 * schedule tables.
*/
#if OS_NSCHEDULES==0

#define OS_K_STARTSCHEDULETABLE			OS_KernUnknownSyscall
#define OS_K_STARTSCHEDULETABLESYNCHRON	OS_KernUnknownSyscall
#define OS_K_CHAINSCHEDULETABLE			OS_KernUnknownSyscall
#define OS_K_STOPSCHEDULETABLE			OS_KernUnknownSyscall
#define OS_K_SYNCSCHEDULETABLE			OS_KernUnknownSyscall
#define OS_K_SETSCHEDULETABLEASYNC		OS_KernUnknownSyscall
#define OS_K_GETSCHEDULETABLESTATUS		OS_KernUnknownSyscall

#define OS_SCHEDULEBASE					OS_NULL
#define OS_SCHEDULEDYNAMICBASE			OS_NULL
#define OS_KILLSCHEDULEFUNC				(os_killschedulefunc_t)OS_NullFunction
#define OS_SYNCHRONISEFUNC				OS_NULL

#else

#define OS_SYNCHRONISEFUNC				OS_Synchronise


#define OS_CFG_INITSCHEDULES			OS_InitSchedules
#define OS_SCHEDULEBASE					OS_schedule
#define OS_SCHEDULEDYNAMICBASE			OS_scheduleDynamic
#define OS_KILLSCHEDULEFUNC				OS_KillSchedule

#include <memmap/Os_mm_const_begin.h>
static const os_scheduleentry_t OS_stEntries[OS_NSTENTRIES] = { OS_STENTRIES };
const os_schedule_t OS_schedule[OS_NSCHEDULES] = { OS_STCONFIG };
#include <memmap/Os_mm_const_end.h>

#endif

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nSchedules = OS_NSCHEDULES;
#include <memmap/Os_mm_const8_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_schedule_t * const OS_scheduleTableBase = OS_SCHEDULEBASE;
os_scheduledynamic_t * const OS_scheduleDynamicBase = OS_SCHEDULEDYNAMICBASE;
const os_killschedulefunc_t OS_killScheduleFunc = OS_KILLSCHEDULEFUNC;
const os_synchronisefunc_t OS_synchroniseFunc = OS_SYNCHRONISEFUNC;
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_CFG_STARTTICKERS
 *
 * Allows a generated function void OS_StartTickers(void) to start all the internal drivers and GPT channels
 * that are configured to 'tick' software counters.
 * OS_StartTickers() must be called after all hardware initialisation is done.
 *
 * CAVEAT:
 * We assume that Gpt_Init() has already been called, which is not particularly pleasant.
 * We also assume that the Gpt API functions can be called safely from within the kernel. Since the
 * MCAL drivers and the OS are not tightly integrated this might not be the case. If there are
 * problems, address your complaints to those responsible for the Autosar Layered Architecture.
 *
 * If there are problems with any of the above points, define the macro OS_INHIBIT_GPT_STARTUP = 1 in
 * board.h, and call the function OS_StartGptTickers() from a task (typically the Init task), after
 * Gpt_Init() has been called.
 *
*/
#ifndef OS_INHIBIT_GPT_STARTUP
#define OS_INHIBIT_GPT_STARTUP	0
#endif

#ifndef OS_CFG_STARTTICKERS

#if (OS_NINTERNALTICKERS != 0)

#define OS_CFG_STARTTICKERS		OS_StartTickers

#elif !OS_INHIBIT_GPT_STARTUP && (OS_NGPTTICKERS != 0)

#define OS_CFG_STARTTICKERS		OS_StartTickers

#endif

#endif

/*!
 * OS_startupCheckFunc
 *
 * This array contains pointers to functions that will
 * be called by OS_StartupChecks().
 *
 * If the startup checks are not enabled the OS_StartupChecks() function is not
 * called, so the array isn't needed.
*/
#if 0
#if (OS_CONFIGMODE & OS_STARTUPCHECK)
#define OS_CFG_STARTUPCHECKS	OS_StartupChecks

#include <memmap/Os_mm_const_begin.h>
os_schkfunc_t const OS_startupCheckFunc[] =
{
#if OS_NCOUNTERS != 0
	OS_StartupChecksCounter,
#endif

#if OS_NALARMS != 0
	OS_StartupChecksAlarm,
#endif

#if OS_NHWTIMERS != 0
	OS_StartupChecksTimer,
#endif

#if OS_NSCHEDULES != 0
	OS_StartupChecksSchedule,
#endif

	OS_NULL			/* Terminator: must be at end */
};
#include <memmap/Os_mm_const_end.h>
#endif
#endif

/*!
 * OS_initFunc
 *
 * This array contains pointers to initialisation functions that will
 * be called by OS_KernStartOs(). The entries will all be
 * specific initialisation functions.
 * The array is initialised using macros that are defined earlier in
 * this file depending on the generated configuration. Exception:
 * OS_CFG_INITARCH is normally defined by the architecture.
 * Macros that are not defined are not included in the list.
 * The list is null-terminated.
 *
 * This file therefore defines which init functions are called, and in
 * what order.
*/
#include <memmap/Os_mm_const_begin.h>
os_initfunc_t const OS_initFunc[] =
{
#ifdef OS_CFG_STARTUPCHECKS
	OS_CFG_STARTUPCHECKS,
#endif

#ifdef OS_CFG_INITTIMERS
	OS_CFG_INITTIMERS,
#endif

#ifdef OS_CFG_INITCOUNTERS
	OS_CFG_INITCOUNTERS,
#endif

#ifdef OS_CFG_INITALARMS
	OS_CFG_INITALARMS,
#endif

#ifdef OS_CFG_INITSCHEDULES
	OS_CFG_INITSCHEDULES,
#endif

#ifdef OS_CFG_ACTIVATEAUTOTASKS
	OS_CFG_ACTIVATEAUTOTASKS,
#endif

#ifdef OS_CFG_ACTIVATEAUTOALARMS
	OS_CFG_ACTIVATEAUTOALARMS,
#endif

#ifdef OS_CFG_ACTIVATEAUTOSCHEDULES
	OS_CFG_ACTIVATEAUTOSCHEDULES,
#endif

#ifdef OS_CFG_STARTTICKERS
	OS_CFG_STARTTICKERS,
#endif

	OS_NULL
};
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_counterDynamic
 *
 * The dynamic state of all counters, one entry per counter.
 * The 'head' member must be set to OS_NULLALARM during StartOS().
*/
#if OS_NCOUNTERS!=0
os_counterdynamic_t OS_counterDynamic[OS_NCOUNTERS];
#endif

/*!
 * OS_alarmDynamic
 *
 * The dynamic state of all alarms, one entry per alarm.
*/
#if OS_TOTALALARMS!=0
os_alarmdynamic_t OS_alarmDynamic[OS_TOTALALARMS];
#endif

/*!
 * OS_scheduleDynamic
 *
 * The dynamic state of all schedule tablss, one entry per schedule table.
*/
#if OS_NSCHEDULES!=0
os_scheduledynamic_t OS_scheduleDynamic[OS_NSCHEDULES];
#endif

/*!
 * IncrementCounter
 *
 * Do we need the IncrementCounter/IncrementCounter system call?
 *
 * Without execution timing, if the number of counters is the same as the number of timers
 * there are no software counters, so we can omit the system call.
 * If execution timing is configured there's one hardware timer for execution timing, so we only omit
 * the system call if the number of counters is strictly less than the number of timers.
*/
#if OS_NCOUNTERS<=OS_NHWTIMERS
#define OS_K_INCREMENTCOUNTER		OS_KernUnknownSyscall
#endif

/*!
 * OS_callTable
 *
 * This is the table of kernel functions used by the microkernel's OS-call dispatcher
 * to route OS calls to the correct function.
 * The entries in here must be in exactly the right order as given by
 * the indices in Os_syscalls.h
 *
 * We start by defining macros for all known system calls, but only if they
 * are not already defined. The configuration sections above can define
 * the macros as OS_KernUnknownSyscall if the feature is not
 * required.
*/
/* OS_KernDisableInterruptSource and OS_KernEnableInterruptSource are patched out for the moment
*/
#define OS_K_DISABLEINTERRUPTSOURCE		OS_KernUnknownSyscall
#define OS_K_ENABLEINTERRUPTSOURCE		OS_KernUnknownSyscall

#ifndef OS_K_SETRELALARM
#define OS_K_SETRELALARM				OS_KernSetRelAlarm
#endif

#ifndef OS_K_SETABSALARM
#define OS_K_SETABSALARM				OS_KernSetAbsAlarm
#endif

#ifndef OS_K_CANCELALARM
#define OS_K_CANCELALARM				OS_KernCancelAlarm
#endif

#ifndef OS_K_INCREMENTCOUNTER
#define OS_K_INCREMENTCOUNTER			OS_KernIncrementCounter
#endif

#ifndef OS_K_STARTSCHEDULETABLE
#define OS_K_STARTSCHEDULETABLE			OS_KernStartScheduleTable
#endif

#ifndef OS_K_STARTSCHEDULETABLESYNCHRON
#define OS_K_STARTSCHEDULETABLESYNCHRON	OS_KernStartScheduleTableSynchron
#endif

#ifndef OS_K_CHAINSCHEDULETABLE
#define OS_K_CHAINSCHEDULETABLE			OS_KernChainScheduleTable
#endif

#ifndef OS_K_STOPSCHEDULETABLE
#define OS_K_STOPSCHEDULETABLE			OS_KernStopScheduleTable
#endif

#ifndef OS_K_SYNCSCHEDULETABLE
#define OS_K_SYNCSCHEDULETABLE			OS_KernSyncScheduleTable
#endif

#ifndef OS_K_SETSCHEDULETABLEASYNC
#define OS_K_SETSCHEDULETABLEASYNC		OS_KernSetScheduleTableAsync
#endif

#ifndef OS_K_TERMINATEAPPLICATION
#define OS_K_TERMINATEAPPLICATION		OS_KernTerminateApplication
#endif

#ifndef OS_K_SIMTIMERADVANCE
#define OS_K_SIMTIMERADVANCE			OS_KernUnknownSyscall
#endif

#ifndef OS_K_GETCOUNTERVALUE
#define OS_K_GETCOUNTERVALUE			OS_MkwGetCounterValue
#endif

#ifndef OS_K_GETALARM
#define OS_K_GETALARM					OS_MkwGetAlarm
#endif


#include <memmap/Os_mm_const_begin.h>
mk_oscall_t const OS_callTable[MK_N_OSCALL] =
{
	(mk_oscall_t)OS_K_SETRELALARM,					/* 0 */
	(mk_oscall_t)OS_K_SETABSALARM,					/* 1 */
	(mk_oscall_t)OS_K_CANCELALARM,					/* 2 */
	(mk_oscall_t)OS_K_INCREMENTCOUNTER,				/* 3 */
	(mk_oscall_t)OS_K_STARTSCHEDULETABLE,			/* 4 */
	(mk_oscall_t)OS_K_STARTSCHEDULETABLESYNCHRON,	/* 5 */
	(mk_oscall_t)OS_K_CHAINSCHEDULETABLE,			/* 6 */
	(mk_oscall_t)OS_K_STOPSCHEDULETABLE,			/* 7 */
	(mk_oscall_t)OS_K_SYNCSCHEDULETABLE,			/* 8 */
	(mk_oscall_t)OS_K_SETSCHEDULETABLEASYNC,		/* 9 */
	(mk_oscall_t)OS_K_SIMTIMERADVANCE,				/* 10 */
	(mk_oscall_t)OS_K_GETCOUNTERVALUE,				/* 11 */
	(mk_oscall_t)OS_K_GETALARM,						/* 12 */
	(mk_oscall_t)OS_MKTerminateApplication,			/* 13 */
	(mk_oscall_t)OS_KernStartOs						/* 14 - must be last! */
};
#include <memmap/Os_mm_const_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
