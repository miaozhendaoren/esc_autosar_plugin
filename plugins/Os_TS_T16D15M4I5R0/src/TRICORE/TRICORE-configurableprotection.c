/* TRICORE-configurableprotection.c
 *
 * This file contains special configurable versions of the OS_EnterProtectedMode,
 * OS_SetProtection, OS_SetProtectionInit, OS_SetIsrProtection and OS_SetHookProtection.
 *
 * To use this file, simply compile it and link it into your application along with the
 * rest of your object files. The functions in thie file will be used in preference to those
 * in the kernel libraries.
 *
 * The variable OS_enableMemoryProtection is the configuration variable. It must be set
 * to the desired value (zero or non-zero) at startup, before calling StartOS. Afterwards
 * it must never be modified.
 *
 * If OS_enableMemoryProtection is non-zero, the memory protection works as configured.
 * If OS_enableMemoryProtection is zero, the memory protection unit is never configured, thus
 * permitting the debugger to use it for on-chip breakpoints.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-configurableprotection.c 19478 2014-10-27 08:56:06Z mist9353 $
*/

#include <Os_kernel.h>

/*!
 * OS_DPRn_ACCESS, OS_CPRn_ACCESS
 *
 * These macros define the access modes for data protection registers
 *
 * DPR0 - used for application data/bss segment, therefore READ/WRITE.
 * DPR1 - used for code/constant segment, therefore READONLY.
 * DPR2 - used for task/ISR data/bss segment, therefore READ/WRITE.
 * DPR3 - used for task/ISR stack segment, therefore READ/WRITE.
 *
 * The CPRM value is configurable to permit debugging of protected applications.
*/
#define DPR0_ACCESS  (OS_DPM_RE0 | OS_DPM_WE0)
#define DPR1_ACCESS  (OS_DPM_RE1)
#define DPR2_ACCESS  (OS_DPM_RE2 | OS_DPM_WE2)
#define DPR3_ACCESS  (OS_DPM_RE3 | OS_DPM_WE3)

#define DPRM_VALUE   (DPR0_ACCESS|DPR1_ACCESS|DPR2_ACCESS|DPR3_ACCESS)

/*!
 * __GLBL_TEXT_START and __GLBL_TEXT_END mark the boundary of the area of
 * memory that is executable by non-privileged tasks.
 *
 * __GLBL_RODATA_START and __GLBL_RODATA_END mark the boundary of ROM memory
 * that is readable by non-privileged tasks.
 *
 * __GLBL_DATA_START and __GLBL_DATA_END mark the boundary of RAM memory that
 * is readable by non-privileged tasks.
 *
 * These are not true variables; they are symbols set up by the linker.
 *
 * The extent of the area that non-privileged tasks are allowed to read
 * runs from the lower of __GLBL_RODATA_START and __GLBL_DATA_START to
 * the higher of __GLBL_RODATA_END and __GLBL_DATA_END. This is because
 * there are not enough DPRs to permit two separate regions to be defined.
 * It is assumed that reading from the addresses between the ROM and RAM
 * cannot cause harmful side-effects.
 *
*/
/* CHECK: SAVE
 * CHECK: RULE 401 OFF (these are linker-generated symbols, not variables)
*/
extern os_char_t __GLBL_TEXT_START; extern os_char_t __GLBL_TEXT_END;
extern os_char_t __GLBL_RODATA_START; extern os_char_t __GLBL_RODATA_END;
extern os_char_t __GLBL_DATA_START; extern os_char_t __GLBL_DATA_END;
/* CHECK: RESTORE
*/

#include <memmap/Os_mm_const_begin.h>
const os_uint32_t OS_globalCpr[OS_N_CPR * 2] =
{
	(os_uint32_t)&__GLBL_TEXT_START,
	(os_uint32_t)&__GLBL_TEXT_END,
	0,	/* If these are ever used, macro TRICORE_CheckGlobalMemoryAccess() */
	0	/* needs attention */
};
#include <memmap/Os_mm_const_end.h>

#include <memmap/Os_mm_var_begin.h>
os_uint32_t OS_globalDpr1[2];
#include <memmap/Os_mm_var_end.h>

/*!
 * OS_enableMemoryProtection
 *
 * This variable enables/disables memory protection in the OS kernel
 *
 * If the variable is FALSE (zero), the kernel does not modify the contents
 * of the memory protection unit and the debugger can use it for breakpoints etc.
 * If the variable is TRUE (non-zero), the kernel provides full memory protection. Debugging
 * with on-chip breakpoints is not possible
 *
 * The variable must be set at startup, before calling StartOS. Afterwards, its contents
 * MUST NOT be modified.
*/
#include <memmap/Os_mm_var8_begin.h>
os_uint8_t OS_enableMemoryProtection = OS_TRUE;
#include <memmap/Os_mm_var8_end.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_EnterProtectedMode
 *
 * This function switches the CPU from "normal" mode in which it starts
 * to "proteced" mode. To do this, the PROTEN bit of SYSCON is set to 1.
 *
 * Before that can happen, however, the appropriate IO mode (SU) and
 * and protection register set (0) must be selected in PSW. Then the
 * registers of protection register set 0 are initialised to give read,
 * write and execute access.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
*/
void OS_EnterProtectedMode(void)
{
	os_unsigned_t pswVal;

	if ( OS_enableMemoryProtection )
	{
		OS_SetProtectionInit();

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)

		/* First setup the whole memory. */
		OS_MTCR(OS_CPR0_L, OS_kernCpr[0]);
		OS_MTCR(OS_CPR0_U, OS_kernCpr[1]);

		/* Then setup the whole .text as executable */
		OS_MTCR(OS_CPR1_L, OS_globalCpr[0]);
		OS_MTCR(OS_CPR1_U, OS_globalCpr[1]);

		/* Now the data ranges */
		OS_MTCR(OS_DPR4_L, OS_kernDpr[0]);
		OS_MTCR(OS_DPR4_U, OS_kernDpr[1]);

		OS_MTCR(OS_DPR3_L, OS_globalDpr1[0]);
		OS_MTCR(OS_DPR3_U, OS_globalDpr1[1]);

		/* Setup set 0 for supervisor mode */

		OS_MTCR(OS_DPRE_0,0x18);
		OS_MTCR(OS_DPWE_0,0x18);
		OS_MTCR(OS_CPXE_0,0x01);

		/* Setup set 1 for user mode */

		OS_MTCR(OS_DPRE_1,0x0f);
		OS_MTCR(OS_DPWE_1,0x07);
		OS_MTCR(OS_CPXE_1,0x02);

#else
		OS_SetPr0(OS_kernCpm, OS_kernCpr, OS_kernDpm, OS_kernDpr);
#endif

		/* Select PRS 0, supervisor mode, enable write to global address
		 * registers, disable call depth counting.
		*/
		pswVal = OS_MFCR(OS_PSW);
		pswVal &= ~(OS_PSW_PRS | OS_PSW_IO | OS_PSW_GW | OS_PSW_CDE | OS_PSW_CDC);
		pswVal |= (OS_PSW_PRS_0 | OS_PSW_IO_SU | OS_PSW_GW | OS_PSW_CDC_DIS);
		OS_MTCR(OS_PSW, pswVal);

		/* Turn on protected mode
		*/
		OS_MTCR(OS_SYSCON, OS_SYSCON_PROTEN);
	}
}

/*!
 * OS_SetProtection
 *
 * This function loads the protection parameters for the given task
 * into the memory protection unit.
 *
 * Protection register set 1 is used for tasks of non-privileged
 * applications. The code protection registers are initialised
 * as follows:
 *
 * CPR0: entire .text section
 * CPR1: not used
 *
 * The data protection registers are initialised as follows:
 *
 * DPR0: application data (read/write)
 * DPR1: global data (read-only)
 * DPR2: task/isr private data (read/write)
 * DPR3: stack (read/write)
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
*/
void OS_SetProtection(const os_task_t *t)
{
	const os_appcontext_t *a = OS_GET_APP(t->app);

#if (OS_TRICOREARCH != OS_TRICOREARCH_16EP)
	os_uint32_t dpr[OS_N_DPR * 2];
#endif

	/* If the application is a privileged application, it will be running
	 * in protection register set 0. If this is the case, there's no need
	 * to load PRS1
	*/
	if ( OS_enableMemoryProtection && !OS_AppIsTrusted(a) )
	{
#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
		/* Program data ranges 0 to 2
		*/

		OS_MTCR(OS_DPR0_L, (os_uint32_t)a->dataStart);
		OS_MTCR(OS_DPR0_U, (os_uint32_t)a->dataEnd);

		OS_MTCR(OS_DPR1_L, (os_uint32_t)t->dataStart);
		OS_MTCR(OS_DPR1_U, (os_uint32_t)t->dataEnd);

		OS_MTCR(OS_DPR2_L, (os_uint32_t)&t->stackBase[0]);
		OS_MTCR(OS_DPR2_U, (os_uint32_t)&t->stackBase[0] + t->stackLen);
#else
		/* Program protection register set 1.
		*/
		dpr[0] = (os_uint32_t)a->dataStart;
		dpr[1] = (os_uint32_t)a->dataEnd;
		dpr[2] = (os_uint32_t)OS_globalDpr1[0];
		dpr[3] = (os_uint32_t)OS_globalDpr1[1];
		dpr[4] = (os_uint32_t)t->dataStart;
		dpr[5] = (os_uint32_t)t->dataEnd;
		dpr[6] = (os_uint32_t)&t->stackBase[0];
		dpr[7] = dpr[6] + t->stackLen;

		OS_SetPr1(OS_userCpm, OS_globalCpr, DPRM_VALUE, dpr);
#endif
	}
}

/*!
 * OS_SetProtectionInit
 *
 * This function initialises the global DPR1 variables used by SetProtection.
 * It is called once at startup, probably by EnterProtectedMode
*/
void OS_SetProtectionInit(void)
{
	if ( ((os_uint32_t)&__GLBL_RODATA_START) < ((os_uint32_t)&__GLBL_DATA_START) )
	{
		OS_globalDpr1[0] = (os_uint32_t)&__GLBL_RODATA_START;
		OS_globalDpr1[1] = (os_uint32_t)&__GLBL_DATA_END;
	}
	else
	{
		OS_globalDpr1[0] = (os_uint32_t)&__GLBL_DATA_START;
		OS_globalDpr1[1] = (os_uint32_t)&__GLBL_RODATA_END;
	}
}

/*!
 * OS_SetIsrProtection
 *
 * This function loads the protection parameters for the given ISR
 * into the memory protection unit.
 *
 * Protection register set 1 is used for ISRs of non-privileged
 * applications. The code protection registers are initialised
 * as follows:
 *
 * CPR0: entire .text section
 * CPR1: not used
 *
 * The data protection registers are initialised as follows:
 *
 * DPR0: application data (read/write)
 * DPR1: global data (read-only)
 * DPR2: task/isr private data (read/write)
 * DPR3: stack (read/write)
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
*/
void OS_SetIsrProtection(const os_isr_t *isr, os_isrdynamic_t *id)
{
	const os_appcontext_t *a = OS_GET_APP(isr->app);
#if (OS_TRICOREARCH != OS_TRICOREARCH_16EP)
	os_uint32_t dpr[OS_N_DPR * 2];
#endif
	/* If the application is a privileged application, it will be running
	 * in protection register set 0. If this is the case, there's no need
	 * to load PRS1
	*/
	if ( OS_enableMemoryProtection && !OS_AppIsTrusted(a) )
	{

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
		/* Program protection range 0 to 2
		 */

		OS_MTCR(OS_DPR0_L, (os_uint32_t)a->dataStart);
		OS_MTCR(OS_DPR0_U, (os_uint32_t)a->dataEnd);

		OS_MTCR(OS_DPR1_L, (os_uint32_t)isr->dataStart);
		OS_MTCR(OS_DPR1_U, (os_uint32_t)isr->dataEnd);

		OS_MTCR(OS_DPR2_L, (os_uint32_t)id->c.stackLimit - isr->stackLen);
		OS_MTCR(OS_DPR2_U, (os_uint32_t)id->c.stackLimit);
#else
		/*  Program protection register set 1.
		*/
		dpr[0] = (os_uint32_t)a->dataStart;
		dpr[1] = (os_uint32_t)a->dataEnd;
		dpr[2] = (os_uint32_t)OS_globalDpr1[0];
		dpr[3] = (os_uint32_t)OS_globalDpr1[1];
		dpr[4] = (os_uint32_t)isr->dataStart;
		dpr[5] = (os_uint32_t)isr->dataEnd;
		dpr[7] = (os_uint32_t)id->c.stackLimit;
		dpr[6] = dpr[7] - isr->stackLen;

		OS_SetPr1(OS_userCpm, OS_globalCpr, DPRM_VALUE, dpr);
#endif
	}
}

/*!
 * OS_SetHookProtection
 *
 * This function loads the protection parameters for the given hook
 * into the memory protection unit.
 *
 * Protection register set 1 is used for hooks of non-privileged
 * applications. The code protection registers are initialised
 * as follows:
 *
 * CPR0: entire .text section
 * CPR1: not used
 *
 * The data protection registers are initialised as follows:
 *
 * DPR0: application data (read/write)
 * DPR1: global data (read-only)
 * DPR2: not used
 * DPR3: stack (read/write)
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
 *
*/
void OS_SetHookProtection
(	const os_appcontext_t *app,
	os_savedcontext_t *ctxt,
	os_size_t stackLen
)
{
#if (OS_TRICOREARCH != OS_TRICOREARCH_16EP)
	os_uint32_t dpr[OS_N_DPR * 2];
#endif

	/* If the application is a privileged application, it will be running
	 * in protection register set 0. If this is the case, there's no need
	 * to load PRS1
	*/
	if ( OS_enableMemoryProtection && (app->flags & OS_APP_TRUSTED) == 0 )
	{
#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
		/* Program protection ranges 0 to 2
		*/

		OS_MTCR(OS_DPR0_L, (os_uint32_t)app->dataStart);
		OS_MTCR(OS_DPR0_U, (os_uint32_t)app->dataEnd);

		OS_MTCR(OS_DPR1_L, (os_uint32_t)OS_NULL);
		OS_MTCR(OS_DPR1_U, (os_uint32_t)OS_NULL);

		OS_MTCR(OS_DPR2_L, (os_uint32_t)ctxt->stackLimit - stackLen);
		OS_MTCR(OS_DPR2_U, (os_uint32_t)ctxt->stackLimit);
#else
		/* Program protection register set 1.
		*/
		dpr[0] = (os_uint32_t)app->dataStart;
		dpr[1] = (os_uint32_t)app->dataEnd;
		dpr[2] = (os_uint32_t)OS_globalDpr1[0];
		dpr[3] = (os_uint32_t)OS_globalDpr1[1];
		dpr[4] = (os_uint32_t)OS_NULL;
		dpr[5] = (os_uint32_t)OS_NULL;
		dpr[7] = (os_uint32_t)ctxt->stackLimit;
		dpr[6] = dpr[7] - stackLen;

		OS_SetPr1(OS_userCpm, OS_globalCpr, DPRM_VALUE, dpr);
#endif
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
