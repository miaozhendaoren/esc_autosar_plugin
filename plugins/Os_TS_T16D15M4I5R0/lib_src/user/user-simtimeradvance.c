/* user-simtimeradvance.c
 *
 * This file contains the OS_SimTimerAdvance function for non-systemcall kernels
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: user-simtimeradvance.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 12.10 (required)
 * The comma operator shall not be used.
 *
 * Reason:
 * For function-call kernels, OS API is wrapped in macros. The macro wrappers
 * need to use the comma operator to form an expression evaluating to the
 * correct result.
 *
 *
 * MISRA-2) Deviated Rule: 12.13 (advisory)
 * The increment (++) and decrement (--) operators should not be mixed with
 * other operators in an expression.
 *
 * Reason:
 * For function-call kernels, OS API is wrapped in macros. The macro wrappers
 * need to handle an "in kernel" flag so that the increment operator has to be
 * used in combination with the comma operator.
 */

#include <Os_api.h>
#include <Os_osek.h>
#include <Os_simtimer.h>

#if (OS_KERNEL_TYPE != OS_FUNCTION_CALL)
#error "This file is not required for system-call kernels. Check your Makefiles!"
#endif

#include <memmap/Os_mm_code_begin.h>

/*
 * OS_SimTimerAdvance
*/
os_result_t OS_SimTimerAdvance(os_unsigned_t tmr, os_uint32_t incr)
{
	/* Deviation MISRA-1, MISRA-2 */
	return OS_UserSimTimerAdvance(tmr, incr);
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
