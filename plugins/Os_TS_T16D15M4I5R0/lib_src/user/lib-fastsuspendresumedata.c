/* lib-fastsuspendresumedata.c
 *
 * This file contains the nesting counts and saved status variables for
 * the fast suspend/resume functions.
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: lib-fastsuspendresumedata.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_var_begin.h>

/*!
 * OS_fastSuspendResumeStatus
 *
 * This data structure contains the lock levels and nesting counters for the
 * fast suspend/resume APIs
*/
os_fastsuspendresumestatus_t OS_fastSuspendResumeStatus; /* = {0,0,0,0} */

#include <memmap/Os_mm_var_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
