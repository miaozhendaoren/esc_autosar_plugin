/* kern-errext-RunSchedule.c - kernel error database for STATUS=EXTENDED
 *
 * This is a generated file - DO NOT EDIT
 *
 * Generated by errdb-to-c.pl on 2015-03-02 16:07
 *
 * (c) 2015 Elektrobit Automotive GmbH
*/

#include <Os_kernel.h>
#include <Os_error.h>

#include <memmap/Os_mm_const_begin.h>
static const os_errorentry_t OS_errtblRunSchedule[] =
{
  {
    OS_E_STATE,
    OS_ERROR_NotChained,
    OS_ACTION_RETURN|OS_ACTION_ERRORHOOK
  }
};

const os_serviceinfo_t OS_svc_RunSchedule =
{
    OS_errtblRunSchedule,
    OS_SID_RunSchedule,
    1
};
#include <memmap/Os_mm_const_end.h>
