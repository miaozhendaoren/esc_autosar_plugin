/* kern-ioccheckmemoryaccess.c
 *
 * This file contains the IOC helper function OS_KernIocCheckMemoryAccess
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-ioccheckmemoryaccess.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#include <Os_types.h>
#include <Os_kernel.h>
#include <Os_userkernel.h>
#include <Ioc/Ioc.h>
#include <Ioc/Ioc_kern.h>

os_memoryaccess_t OS_KernIocCheckMemoryAccess( const void *data, os_size_t len )
{
	os_memoryaccess_t access;

	/* check data pointer */
	if ( OS_GetInFunction() == OS_INTASK )
	{
		access = OS_KernCheckTaskMemoryAccess(OS_GetTaskCurrent()->taskId, data, len);
	}
	else if ( OS_GetInFunction() == OS_INCAT2 )
	{
		access = OS_KernCheckIsrMemoryAccess(OS_GetIsrCurrent(), data, len);
	}
	else
	{
		access = 0;
	}

	return access;
}


/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
