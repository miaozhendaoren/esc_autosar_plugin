/* kern-data.c
 *
 * This file contains the OS kernel data that does not contain
 * configuration-specific values.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-data.c 18179 2014-05-16 17:31:58Z stpo8218 $
*/

#include <Os_kernel.h>
#if OS_KERNEL_TYPE != OS_MICROKERNEL
#include <Os_timestamp.h>
#include <Os_taskqueue.h>
#endif

#include <memmap/Os_mm_var8_begin.h>
/*!
 * OS_mode
 *
 * This variable holds the startup mode. The content is only valid after
 * the kernel has been started (OS_inFunction != OS_INSTARTUP)
*/
os_uint8_t OS_mode;			/* = 0 */

/*!
 * OS_inFunction
 *
 * This variable indicates whether we are currently running in an ISR,
 * hook function or task. The values it takes are:
 *  OS_INTASK
 *  OS_INCAT1
 *  OS_INCAT2
 *  OS_INxxxHOOK
 *  ... and others
 *
 * The "startup" level is defined to be 0.
*/
os_uint8_t OS_inFunction;	/* = 0 */

#include <memmap/Os_mm_var8_end.h>

#if OS_KERNEL_TYPE != OS_MICROKERNEL

#include <memmap/Os_mm_var_begin.h>
/*!
 * OS_taskQueueHead
 *
 * Pointer to first task in the task queue, for convenience. Is always
 * equal to OS_taskPtrs[OS_taskActivations[0]]
 *
*/
const os_task_t *OS_taskQueueHead;	/* = OS_NULL */

/*!
 * OS_taskCurrent
 *
 * This is a pointer to the currently executing task. When
 * the task is running, this will be the same as the task at the head
 * of the queue (see OS_taskQueueHead). When leaving the kernel,
 * the scheduler will be called if this is not the case. At this point,
 * OS_NULL means that the most recently executing task was terminated.
*/
const os_task_t *OS_taskCurrent;	/* = OS_NULL */

/*!
 * OS_masterPrioWord
 *
 * This word has a bit set for each word in the OS_slavePrioWord array that
 * has one of more bits set. If the number of priorities will fit into a
 * single word, OS_slavePrioWord[0] is always selected and OS_masterPrioWord
 * is never referenced.
 *
 * The OS_slavePrioWord array must be in Os_configuration.c because its size depends
 * on the number of priority levels.
*/
#if !OS_LCFG_TASKQUEUE_SMALL
os_clzword_t OS_masterPrioWord;		/* = 0 */
#endif

/*!
 * OS_isrCurrent
 *
 * This is the id of the currently-executing ISR (or OS_NULLISR
 * if executing at task level)
*/
os_isrid_t OS_isrCurrent = OS_NULLISR;

#include <memmap/Os_mm_var_end.h>

#include <memmap/Os_mm_var8_begin.h>

/*!
 * OS_inKernel
 *
 * This variable indicates whether we are currently running in the
 * kernel or not. Zero means a task is executing. This means that we
 * must switch to the kernel stack if an exception or interrupt occurs.
 *
 * On some architectures (typically those with a system-call mechanism)
 * this is a flag. On others, including the non-system-call kernels,
 * it is a counter.
 *
 * On non-system-call kernels it starts with value 1 so that if the user calls
 * OS_SuspendAllInterrupts() etc. before StartOS(), the dispatcher doesn't get
 * called on exit. This means that StartOS() must set the counter to 0.
*/
#if (OS_KERNEL_TYPE==OS_FUNCTION_CALL)
os_uint8_t OS_inKernel = 1;
#else
os_uint8_t OS_inKernel;			/* = 0 */
#endif

/*!
 * OS_nestSuspendOs,OS_oldSuspendOs
 * OS_nestSuspendAll,OS_oldSuspendAll
 *
 * These variables are the nesting counters and old-level stores for
 * Suspend/ResumeOSInterrupts and Suspend/ResumeAllInterrupts when called
 * from a non-task context. When the interrupt management functions are
 * called from a task context, per-task variables in the task dynamic
 * structure are used. The XXXAll variables are also used by
 * Disable/EnableAllInterrupts.
*/
os_uint8_t OS_nestSuspendOs;	/* = 0 */
os_uint8_t OS_nestSuspendAll;	/* = 0 */
#include <memmap/Os_mm_var8_end.h>
#include <memmap/Os_mm_var_begin.h>
os_oldlevel_t OS_oldSuspendOs;	/* = 0 */
os_oldlevel_t OS_oldSuspendAll;	/* = 0 */

/*!
 * OS_isrLastRes
 *
 * This variable is the head of a chain of resources taken at
 * interrupt level. The head of the chain is saved and set to NULLRESOURCE
 * by the interrupt entry code. On exit it must still be NULLRESOURCE,
 * otherwise the ISR has not released all resources that it takes. After
 * cleanup, the original value is restored.
*/
os_resourceid_t OS_isrLastRes = OS_NULLRESOURCE;

/*!
 * OS_errorStatus
 *
 * This data structure holds the last recorded error. It is set up
 * in OS_Error() just before the error hook is called.
 *
 * Its contents can be read using OS_GetErrorStatus()
*/
os_errorstatus_t OS_errorStatus;	/* = 0 */

/*!
 * OS_accounting
 *
 * This data structure holds the current task/ISR accounting information.
 * When a task gets preempted, the etbRemaining field is stored into
 * the task's context. During an ISR, the etbRemaining field is stored on
 * the stack.
*/
os_accounting_t OS_accounting;		/* = 0 */

#include <memmap/Os_mm_var_end.h>

/*!
 * OS_errorHookNesting
 *
 * This variable contains 3 bits, one for each type of error hook.
 * If the bit is set, we're already nested inside the hook.
 * This is to prevent recursive nesting of the hook functions when
 * errors are caused by the hook functions themselves.
*/
#include <memmap/Os_mm_var8_begin.h>
os_uint8_t OS_errorHookNesting;		/* = 0 */
#include <memmap/Os_mm_var8_end.h>

/*!
 * OS_appsStarted
 *
 * This variable is set to true when all applications have been started (all application-specific
 * startup hooks have been called). Until then, application-specific error and shutdown hooks are
 * inhibited.
 *
 * Among other things, this prevents the application-specific hooks from being called before their
 * page tables have been initialised.
*/
#include <memmap/Os_mm_var8_begin.h>
os_uint8_t OS_appsStarted;		/* = 0 */
#include <memmap/Os_mm_var8_end.h>

#include <memmap/Os_mm_var_begin.h>
/*!
 * OS_hookApp
 *
 * This variable contains the address of the current application's
 * appcontext structure. The content of the variable is only valid
 * during application-specific error-, startup- and shutdown-hooks.
 * It contains OS_NULL at all other times.
*/
const os_appcontext_t *OS_hookApp;	/* = OS_NULL */

/*!
 * OS_sHookContext, OS_eHookContext
 *
 * These variables store the kernel context from which a call to an
 * application specific hook function must return. The return must
 * be via a kernel trap of some sort.
 * OS_sHookContext is for the startup and shutdown hooks.
 * OS_eHookContext is for the error hook, which needs a separate context
 * since it might get called during one of the other hook functions.
*/
os_hookcontext_t OS_sHookContext;	/* = {0} */
os_hookcontext_t OS_eHookContext;	/* = {0} */

/*!
 * OS_inInterrupt
 *
 * This variable is used to determine when we are in an interrupt, and
 * so whether to switch to the interrupt stack. Its type and semantics
 * are architecture-dependent.
 * Some architectures do not need this variable, so if the type macro is
 * not defined we do not declare the variable.
 *
 * The initial value of the variable should be 1 because the startup code
 * runs on the kernel stack.
*/
#ifdef OS_TYPEOF_ININTERRUPT
OS_TYPEOF_ININTERRUPT OS_inInterrupt = 1;
#endif

#include <memmap/Os_mm_var_end.h>
#endif

/*
 * OS_timeStampValue, OS_lastTimeStampTime
 *
 * Data for the generic timestamp (used by CPU load measurement and possibly others) if
 * the architecture doesn't provide a special timestamp timer.
*/
#if OS_USEGENERICTIMESTAMP
#include <memmap/Os_mm_var_begin.h>

os_timervalue_t OS_lastTimeStampTime;	/* = 0 */
os_timestamp_t OS_timeStampValue;		/* = 0 */

#include <memmap/Os_mm_var_end.h>
#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
