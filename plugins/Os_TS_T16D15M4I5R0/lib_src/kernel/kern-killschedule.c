/* kern-killschedule.c
 *
 * This file contains the OS_KillSchedule function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-killschedule.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KillSchedule
 *
 * The schedule is stopped and its state is set to STOPPED or QUARANTINED,
 * depending on the 2nd parameter.
 * A schedule never goes from the QUARANTINED to the STOPPED state as a
 * result of this call.
 *
 * Schedules chained after this table are also set to STOPPED.
*/
os_result_t OS_KillSchedule
(	os_scheduleid_t sid,
	os_schedulestatus_t state
)
{
	const os_schedule_t *ss;
	os_scheduledynamic_t *sd;
	os_scheduleid_t s;

	os_result_t r = OS_E_OK;
	os_intstatus_t is;

	s = sid;

	while ( s != OS_NULLSCHEDULE )
	{
		ss = &OS_schedule[s];
		sd = &OS_scheduleDynamic[s];

		/* Disabling interrupts here is OK for the current case when
		 * a ST can only be chained onto a running ST (ie. max chain length
		 * is 2. If longer chains are ever permitted this might need to
		 * be reconsidered
		*/
		is = OS_IntDisable();

		sd->adjRemaining = 0;
		sd->next = 0;
		sd->status = state;

		/* OS_KillAlarm complains if the alarm wasn't in the queue, but this is
		 * not an error here - so we ignore the return value. */
		(void) OS_KillAlarm(ss->alarm, OS_ALARM_IDLE);

		s = sd->chain;
		sd->chain = OS_NULLSCHEDULE;

		/* See comment above by IntDisable()
		*/
		OS_IntRestore(is);

		/* All subsequent schedules in the chain are set to STOPPED,
		 * not QUARANTINED to prevent collateral damage. If they
		 * are also to be quarantined, that will be done explicitly
		*/
		state = OS_ST_STOPPED;
	}

	return r;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
