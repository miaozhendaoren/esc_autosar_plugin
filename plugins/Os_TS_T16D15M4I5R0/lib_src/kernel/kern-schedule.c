/* kern-schedule.c
 *
 * This file contains the OS_KernSchedule function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-schedule.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 12.5 (required)
 * The operands of a logical && or || shall be primary-expressions.
 *
 * Reason:
 * The next statement is not a violation, regardless of what the checker claims.
 * Chaining primary expressions via || or && does not violate this rule.
 */
#define OS_SID	OS_SID_Schedule
#define OS_SIF	OS_svc_Schedule

#include <Os_kernel.h>
#include <Os_taskqueue.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernSchedule implements the API Schedule
 *
 * If the current task is non-preemptable it is requeued with
 * its true priority. However, since the task is already running,
 * it is enqueued AHEAD of those with equal priority, because they
 * might want to use the same context.
 *
 * Interrupts are enabled on entry. We must disable them before doing
 * anything and restore them afterwards.
 *
 * !LINKSTO Kernel.API.TaskManagement.Schedule.API, 1
*/
os_result_t OS_KernSchedule(void)
{
	os_intstatus_t is;
	os_result_t r = OS_E_OK;
	os_taskdynamic_t *td;
#if !OS_USE_CLZ_QUEUE_ALGORITHM
	os_tasklink_t link;
	os_tasklink_t next;
#endif

	OS_TRACE_SCHEDULE_ENTRY();

	if ( OS_inFunction == OS_INTASK )
	{
		td = OS_taskCurrent->dynamic;

		if ( !OS_InterruptEnableCheckFromTask(OS_IEC_OSEKEXTRA, td) )
		{
			r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_NULL);
		}
		else
		if ( td->lastRes != OS_NULLRESOURCE )
		{
			/* !LINKSTO Kernel.API.TaskManagement.Schedule.Resources, 1
			 *
			 * If this requirement is ever relaxed, the requeue priority
			 * (below) will need to be adjusted so that it is not lower
			 * than that of the highest-priority resource that has been
			 * taken.
			*/
			r = OS_ERROR(OS_ERROR_HoldsResource, OS_NULL);
		}
		else
		{
			is = OS_IntDisable();

			/* Requeue the task with its base priority if there's something
			 * with a higher priority waiting. We only do this if the task
			 * is in the running state --- it could have been killed at ISR
			 * level just before disabling interrupts.
			 *
			 * !LINKSTO Kernel.API.TaskManagement.Schedule.HigherPrioTask, 1
			 * !LINKSTO Kernel.API.TaskManagement.Schedule.LowerPrioTask, 1
			 * !LINKSTO Kernel.API.TaskManagement.Schedule.Schedule, 1
			 * !LINKSTO Kernel.API.TaskManagement.Schedule.InternalResource, 1
			*/
#if OS_USE_CLZ_QUEUE_ALGORITHM
			if ( (td->state == OS_TS_RUNNING) &&
				 (OS_taskCurrent->queuePrio != OS_taskCurrent->runPrio) )
			{
				td->prio =  OS_taskCurrent->queuePrio;
				OS_DequeueTask(OS_taskCurrent, OS_taskCurrent->runPrio);
			}
#else
			link = OS_CurrentLink(OS_taskCurrent, td);
			next = OS_taskActivations[link];

			/* Deviation MISRA-1 */
			if ( (td->state == OS_TS_RUNNING) &&
				 (next != 0) &&
				 (OS_taskCurrent->queuePrio < OS_FindPrio(next)) )
			{
				td->prio =  OS_taskCurrent->queuePrio;
				OS_RequeueDown(link, td->prio);
			}
#endif

			OS_IntRestore(is);
		}
	}
	else
	{
		/* !LINKSTO Kernel.API.TaskManagement.Schedule.ISRC2, 1
		*/
		r = OS_ERROR(OS_ERROR_WrongContext, OS_NULL);
	}

	OS_TRACE_SCHEDULE_EXIT_P(r);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserSchedule</name>
  <synopsis>Voluntarily yield the CPU</synopsis>
  <syntax>
    os_result_t OS_UserSchedule(void)
  </syntax>
  <description>
    <code>OS_UserSchedule()</code> allows the calling task to yield the
    CPU voluntarily. Active tasks whose running priorities are lower than the
    running priority of the current task but higher that its base priority
    are allowed to run. <code>OS_UserSchedule()</code> returns when there
    are no more such tasks.
    <para>Tasks get a higher running priority than their base priority when they
    are preemptive or have an internal resource allocated to them.</para>
    <para>A task that holds a standard resource is not permitted to call
    <code>OS_UserSchedule()</code> since this would interfere with
    the resource's ceiling priority.</para>
  </description>
  <availability>
    <code>OS_UserSchedule()</code> may only be called from a task.
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
