/* kern-canwrite.c
 *
 * This file contains the OS_CanWrite function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-canwrite.c 20018 2014-12-08 06:16:37Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_CanWrite
 *
 * Returns TRUE if the given application is permitted to write to the
 * whole of the specified memory area.
 *
 * LIMITATION: If two of the regions to which writing is permitted are
 * contiguous and the queried region straddles the two contiguous regions,
 * the test will still fail. However, this is not considered to be a problem,
 * since correctly-defined C data structures will always lie entirely inside
 * one region.
 *
 * !LINKSTO Kernel.Autosar.ServiceErrors.Address, 2
 * !LINKSTO Kernel.Autosar.ServiceErrors.Address.Alignment, 1
*/

os_boolean_t OS_CanWrite(const void *base, os_size_t len)
{
	os_address_t first;
	os_address_t last;
	os_boolean_t result = OS_FALSE;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;

	first = (os_address_t)base;
	last = first + len;

	if ( (first == 0) || (last <= first) )
	{
		/* Indicates parameter oddity - null pointer, or the specified
		 * region wraps over the highest address, or then length is zero.
		 * This can cause false permission to be granted if not trapped.
		 *
		 * Fails (result is OS_FALSE)
		*/
	}
	else
	if ( OS_MisalignedData(base, len) )
	{
		/* Indicates data is misaligned and cannot be written.
		 *
		 * Fails (result is OS_FALSE)
		*/
	}
	else
	if ( OS_inFunction == OS_INTASK )
	{
		/* Called from a task.
		*/
		app = OS_GET_APP(OS_taskCurrent->app);

		if ( OS_AppIsTrusted(app) )
		{
			/* Trusted tasks can write anywhere
			*/
			result = OS_TRUE;
		}
		else
		if ( (first >= OS_TASK_STACK_START) && (last <= OS_TASK_STACK_END) )
		{
			/* Non-trusted tasks can write to their own stack
			*/
			result = OS_TRUE;
		}
		else
		if ( OS_IS_WITHIN_APP_REGION(first, last) )
		{
			/* Non-trusted tasks can write to their application's common
			 * data area.
			*/
			result = OS_TRUE;
		}
		else
		if ( OS_IS_WITHIN_TASK_REGION(first, last) )
		{
			/* Non-trusted tasks can write to their own private data
			 * area.
			*/
			result = OS_TRUE;
		}
		else
		{
			/* Fails (result is OS_FALSE) */
		}
	}
	else
	if ( OS_inFunction == OS_INCAT2 )
	{
		/* Called from a Category 2 ISR.
		*/
		const os_isr_t *isr;
		os_isrdynamic_t *isrd;

		isr = &OS_isrTableBase[OS_isrCurrent];
		isrd = &OS_isrDynamicBase[OS_isrCurrent];
		app = OS_GET_APP(isr->app);

		if ( OS_AppIsTrusted(app) )
		{
			/* Trusted ISRs can write anywhere
			*/
			result = OS_TRUE;
		}
		else
		if ( OS_IS_WITHIN_APP_REGION(first, last) )
		{
			/* Non-trusted ISRs can write to their application's common
			 * data area.
			*/
			result = OS_TRUE;
		}
		else
		if ( OS_IS_WITHIN_ISR_REGION(first, last) )
		{
			/* Non-trusted ISRs can write to their own private data
			 * area.
			*/
			result = OS_TRUE;
		}
		else
		if ( (OS_GetIsrStackLimit(isrd) != OS_NULL) &&
			 (first >= ((os_address_t)OS_GetIsrStackLimit(isrd) - isr->stackLen)) &&
			 (last <= (os_address_t)OS_GetIsrStackLimit(isrd)) )
		{
			/* Note: the condition above is not a MISRA violation, no matter what your checker claims. */

			/* Non-trusted ISRs can write to their own private stack area.
			*/
			result = OS_TRUE;
		}
		else
		{
			/* Fails (result is OS_FALSE) */
		}
	}
	else
	{
		/* Called from some other context. All contexts other than tasks
		 * and category 2 ISRs are presumed to be privileged.
		*/
		result = OS_TRUE;
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
