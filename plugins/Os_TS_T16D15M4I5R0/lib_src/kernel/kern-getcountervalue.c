/* kern-getcountervalue.c
 *
 * This file contains the OS_KernGetCounterValue function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getcountervalue.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/

#define OS_SID	OS_SID_GetCounterValue
#define OS_SIF	OS_svc_GetCounterValue

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernGetCounterValue
 *
 * The current value of the specified counter
 * is written to the indicated location.
 *
 * !LINKSTO Kernel.Autosar.API.SystemServices.GetCounterValue, 1
*/
os_result_t OS_KernGetCounterValue(os_counterid_t c, os_tick_t *out)
{
	os_result_t r = OS_E_OK;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)c);
	OS_SAVE_PARAMETER_N(1,(os_paramtype_t)out);

	OS_TRACE_GETCOUNTERVALUE_ENTRY(c);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( OS_ArchCanWrite(out, sizeof(*out)) == 0 )
	{
		r = OS_ERROR(OS_ERROR_WriteProtect, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_IsValidCounterId( c ) )
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.GetCounterValue.Invalid, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidCounterId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		/* Get the value
		*/
		*out = OS_GetCount(c, &OS_counterTableBase[c]);
	}

	OS_TRACE_GETCOUNTERVALUE_EXIT_P(r,c);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserGetCounterValue</name>
  <synopsis>Get the current value of the counter</synopsis>
  <syntax>
    os_result_t OS_UserGetCounterValue
    (   os_counterid_t c, /@ ID of the counter @/
        os_tick_t *out    /@ Where to put the answer @/
    )
  </syntax>
  <description>
    <code>OS_UserGetCounterValue()</code> places the current value of
    the specified counter in the designated <code>out</code> variable.
    If the counter does not exist or another error is detected, the
    <code>out</code> variable remains unchanged.
    If this system service is called from an ISR of higher priority
    than the counter's own ISR, the count value might occasionally be slightly
    less than expected, but this will reflect the state of the alarms in the counter's
    queue.
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
