/* kern-activateautoschedules.c
 *
 * This file contains the OS_ActivateAutoSchedules function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-activateautoschedules.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_ActivateAutoSchedules()
 *
 * Activate all schedule tables that are specified for the requested mode
 *
 * !LINKSTO Kernel.Autosar.ScheduleTable.Autostart, 1
*/
void OS_ActivateAutoSchedules(void)
{
	os_uint16_t idx;
	const os_autoschedule_t *as;
	os_scheduleid_t s;
	const os_schedule_t *ss;

	idx = OS_startModeSchedules[OS_mode];
	as = &OS_autoStartSchedules[idx];
	s = as->schedule;

	while ( s < OS_nSchedules )
	{
		ss = &OS_scheduleTableBase[s];

		if ( as->method == OS_STARTMETHOD_SYNC )
		{
			/* !LINKSTO Kernel.Autosar.ScheduleTable.Autostart.Synchronously, 1
			*/
			/* when called during autostart, DoStartScheduleTableSynchron can't
			 * fail, so we can safely ignore its return value
			*/
			(void) OS_DoStartScheduleTableSynchron(s, ss);
		}
		else
		{
			/* An incorrectly generated method will start "relative".
			 *
			 * !LINKSTO Kernel.Autosar.ScheduleTable.Autostart.Absolute, 1
			 * !LINKSTO Kernel.Autosar.ScheduleTable.Autostart.Relative, 1
			*/
			/* when called during autostart, DoStartScheduleTable can't fail, so
			 * we can safely ignore its return value
			*/
			(void) OS_DoStartScheduleTable(s, ss, OS_alarmTableBase[ss->alarm].counter,
											as->offset, (as->method != OS_STARTMETHOD_ABS));
		}
		idx++;
		as = &OS_autoStartSchedules[idx];
		s = as->schedule;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
