/* kern-enqueuelink.c
 *
 * This file contains the OS_EnqueueLink function.
 * The function is only necessary when multiple activations is enabled, but is
 * always present in the library.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-enqueuelink.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_EnqueueLink
 *
 * The specified task activation is inserted into the task activation queue
 * in priority order behind those with equal priority.
 *
 * !LINKSTO Kernel.Architecture.ConformanceClasses.NonSuspendedTasksBCC, 1
 * !LINKSTO Kernel.Architecture.ConformanceClasses.NonSuspendedTasksECC, 1
 *		The number of tasks in the ready queue is not limited here.
 *
 * !LINKSTO Kernel.Architecture.ConformanceClasses.TasksPerPrio, 1
 *		The number of tasks per priority is not limited here.
 *
 * !LINKSTO Kernel.TaskManagement.Scheduling.PrioQueue, 1
 * !LINKSTO Kernel.TaskManagement.Scheduling, 1
 * !LINKSTO Kernel.TaskManagement, 1
 *		'>=' in the while loop --> FIFO order.
*/
void OS_EnqueueLink(os_tasklink_t tAct, os_prio_t prio)
{
	os_tasklink_t qAct = OS_taskActivations[0];
	os_tasklink_t lAct = 0;

	while ( (qAct != 0) && (OS_FindPrio(qAct) >= prio) )
	{
		lAct = qAct;
		qAct = OS_taskActivations[qAct];
	}

	/* Now we insert tAct between lAct and qAct and recompute the queue head.
	*/
	OS_taskActivations[lAct] = tAct;
	OS_taskActivations[tAct] = qAct;
	OS_taskQueueHead = OS_taskPtrs[OS_taskActivations[0]];
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
