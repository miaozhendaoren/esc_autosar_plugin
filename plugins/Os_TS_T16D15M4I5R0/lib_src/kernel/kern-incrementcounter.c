/* kern-incrementcounter.c
 *
 * This file contains the OS_KernIncrementCounter function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-incrementcounter.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#define OS_SID	OS_SID_IncrementCounter
#define OS_SIF	OS_svc_IncrementCounter

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernIncrementCounter
 *
 * The specified counter is advanced by 1. The alarm at the head of the
 * queue (if there is one) is decremented. If it reaches zero, the
 * alarm and all subsequent alarms with zero delta-count are triggered.
 *
 * After all the checks have been made, OS_AdvanceCounter does the job.
 *
 * !LINKSTO Kernel.Autosar.Counter.Types.SOFTWARE, 1
*/
os_result_t OS_KernIncrementCounter(os_counterid_t c)
{
	const os_counter_t *cs;
	os_result_t r = OS_E_OK;
	os_intstatus_t is;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)c);

	OS_TRACE_INCREMENTCOUNTER_ENTRY(c);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_OSEKEXTRA) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( ! OS_IsValidCounterId( c ) )
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.IncrementCounter.Invalid, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidCounterId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		cs = &OS_counterTableBase[c];

		app = OS_CurrentApp();

		if ( !OS_HasPermission(app, cs->permissions) )
		{
			r = OS_ERROR(OS_ERROR_Permission, OS_GET_PARAMETER_VAR());
		}
		else
		if ( OS_CounterIsHw(cs) )
		{
			r = OS_ERROR(OS_ERROR_CounterIsHw, OS_GET_PARAMETER_VAR());
		}
		else
		{
			/* Everything OK: we can advance the counter
			 *
			 * The return value of IncrementCounter() is not changed if OS_AdvanceCounter()
			 * detects an error - this will be done asynchronously via error hook callback.
			 * Therefore, the return value of OS_AdvanceCounter is ignored.
			 * !LINKSTO Kernel.Autosar.API.SystemServices.IncrementCounter.AlarmError, 1
			*/
			is = OS_IntDisable();
			(void) OS_AdvanceCounter(cs, &OS_counterDynamicBase[c], 1, is);
			OS_IntRestore(is);
		}
	}

	OS_TRACE_INCREMENTCOUNTER_EXIT_P(r,c);

	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserIncrementCounter</name>
  <synopsis>Increment a counter</synopsis>
  <syntax>
    os_result_t OS_UserIncrementCounter
    (   os_counterid_t c    /@ ID of the counter @/
    )
  </syntax>
  <description>
    <code>OS_UserIncrementCounter()</code> increments a counter. If any
    alarms attached to the counter expire as a result, the configured
    action for that alarm is performed. The alarm action always runs in
    the context of the kernel.
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
