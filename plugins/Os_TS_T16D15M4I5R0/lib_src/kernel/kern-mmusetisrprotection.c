/* kern-mmusetisrprotection.c
 *
 * This file contains the OS_SetIsrProtection function for architectures with an MMU.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmusetisrprotection.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_SetIsrProtection
 *
 * This function loads the page table for the given ISR into the MMU
 *
 * If the ISR already "owns" the MMU nothing is done. This avoids constantly reprogramming
 * the MMU when a non-trusted ISR interrupts or is interrupted by a trusted task or ISR
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
*/

void OS_SetIsrProtection(const os_isr_t *isr, os_isrdynamic_t *isrd)
{
	if ( (OS_mmuOwnerType == OS_OBJ_ISR) && (OS_mmuOwnerId == isr->isrId) )
	{
		/* ISR already owns the MMU, so there's nothing to do.
		*/
	}
	else
	if ( OS_IsrUsesMmu(isrd) )
	{
		OS_mmuOwnerType = OS_OBJ_ISR;
		OS_mmuOwnerId = isr->isrId;

		OS_FillMmuForIsr(isrd);
	}
	else
	{
		/* MISRA :-( */
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
