/* kern-startreslocktiming.c
 *
 * This file contains the OS_StartResLockTiming function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startreslocktiming.c 18959 2014-09-25 13:25:33Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: UnusedVariable
 *   Variable is never used.
 *
 * Reason: Not an issue, variable will be used if timing protection is enabled.
 */

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_StartResLockTiming
 *
 * This function starts resource-lock timing for the resource.
 *
 * This function is not called if the resource lock is not being timed.
 * Interrupts are disabled.
*/
void OS_StartResLockTiming
(	os_resourcedynamic_t *rd,
	os_tick_t tLim
)
{
	os_tick_t used;
	os_tick_t remaining;

	OS_PARAM_UNUSED(rd);

	OS_ResetExecTimingInterrupt();
	used = OS_GetTimeUsed();

	OS_accounting.etUsed += used;

	if ( OS_accounting.inFunction == OS_INTASK )
	{
		if ( OS_GET_ACCT(((OS_taskCurrent->flags & OS_TF_MEASUREEXEC) != 0)
				&& (OS_taskCurrent->accounting->etMax < OS_accounting.etUsed)) )
		{
			OS_SET_ACCT(OS_taskCurrent->accounting->etMax = OS_accounting.etUsed);
		}
	}
	else
	if ( OS_accounting.inFunction == OS_INCAT2 )
	{	/* Possible diagnostic TOOLDIAG-1 <1> */
		const os_isr_t *isr = &OS_isrTableBase[OS_isrCurrent];

		if ( OS_GET_ACCT(((isr->flags & OS_ISRF_MEASUREEXEC) != 0)
				&& (isr->accounting->etMax < OS_accounting.etUsed)) )
		{
			OS_SET_ACCT(isr->accounting->etMax = OS_accounting.etUsed);
		}
	}
	else
	{
		/* MISRA-C */
	}

	if ( (OS_accounting.etLimit != 0) && (OS_accounting.etUsed >= OS_accounting.etLimit) )
	{
		OS_ExceedExecTime();
	}
	else
	{
		if ( OS_accounting.etLimit == 0 )
		{
			remaining = OS_MAXTICK;
		}
		else
		{
			remaining = OS_accounting.etLimit - OS_accounting.etUsed;
		}

		OS_SET_RLOCK(rd->etUsed = OS_accounting.etUsed);
		OS_SET_RLOCK(rd->etLimit = OS_accounting.etLimit);
		OS_SET_RLOCK(rd->etType = OS_accounting.etType);

		OS_accounting.inFunction = OS_inFunction;
		OS_accounting.etUsed = 0;

		if (tLim >= remaining)
		{	/* old remaining time is shorter than new time */
			/* use old time for accounting (and leave accounting type unchanged) */
			OS_accounting.etLimit = remaining;
		}
		else
		{	/* new limit is smaller then remaining time */
			/* use new time for accounting and set new accounting type */
			OS_accounting.etLimit = tLim;
			OS_accounting.etType = OS_ACC_RESLOCK;
		}

		OS_SetExecTimingInterrupt(OS_accounting.frc, OS_accounting.etLimit);
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
