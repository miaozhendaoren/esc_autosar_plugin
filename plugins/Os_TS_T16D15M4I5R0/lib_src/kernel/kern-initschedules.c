/* kern-initschedules.c
 *
 * This file contains the OS_InitSchedules function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-initschedules.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an initialization array to make them more readable and maintainable.
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitSchedules
 *
 * The 'chain' of each alarm needs to be set to NULLSCHEDULE.
 * This is because the default bss value (0) is a valid schedule id.
*/
void OS_InitSchedules(void)
{
	os_unsigned_t i = OS_nSchedules;
	os_scheduledynamic_t *s = OS_scheduleDynamic;

	while ( i > 0 )
	{
		s->chain = OS_NULLSCHEDULE;
		/* Deviation MISRA-1 */
		s++;
		i--;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
