/* kern-mmuisrmapper.c
 *
 * This file contains the OS_MmuIsrMapper function
 *
 * The MMU mapper creates a memory map for the MMU for a non-trusted ISR. The memory map specifies all memory
 * regions that are accessible by a given non-trusted ISR.
 *
 * At the moment, it is assumed that there is a single contiguous region of RAM into which
 * all stacks and data are linked. Since there are up to 3 writable regions, this memory must
 * be partitioned into up to 7 regions. A further region is needed for the read-only constants
 * in ROM, giving 8 in total.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmuisrmapper.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#define OS_SID OS_SID_MemoryManagement
#define OS_SIF OS_svc_MemoryManagement

#include <Os_kernel.h>
#include <Os_mmu.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/* This sets dimensions on the region map (below)
*/
#define MAX_REGIONS		8

/*!
 * OS_MmuIsrMapper
 *
 * This function creates an MMU mapping for the data areas of a given ISR.
*/
os_result_t OS_MmuIsrMapper(os_isrid_t isrid)
{
	os_pagemap_t chunkmap[MAX_REGIONS];
	os_pagemap_t pagemap[OS_MMU_MAX_PAGES];
	os_archpagemap_t *archpagemap;
	os_int_t nchunks = 0;
	os_int_t npages = 0;
	os_result_t result = OS_E_OK;

	const os_isr_t *isrs = &OS_isrTableBase[isrid];
	os_isrdynamic_t *isrd = &OS_isrDynamicBase[isrid];
	const os_appcontext_t *app = OS_GET_APP(isrs->app);

	/* First, work out how many memory areas there are, and where they are.
	*/
	chunkmap[0].base = (os_address_t)OS_GetIsrStackBase(isrs);

	if ( chunkmap[0].base != (os_address_t)OS_NULL )
	{
		chunkmap[0].size = isrs->stackLen;
		chunkmap[0].flags = OS_MEMPERM_UR | OS_MEMPERM_UW;
		nchunks = 1;
	}

	if ( isrs->dataStart != OS_NULL )
	{
		chunkmap[nchunks].base = (os_address_t)isrs->dataStart;
		chunkmap[nchunks].size = isrs->dataEnd - isrs->dataStart;		/* Assumes ptrs are to byte */
		chunkmap[nchunks].flags = OS_MEMPERM_UR | OS_MEMPERM_UW;
		nchunks++;
	}

	if ( (app != OS_NULL) && (app->dataStart != OS_NULL) )
	{
		chunkmap[nchunks].base = (os_address_t)app->dataStart;
		chunkmap[nchunks].size = app->dataEnd - app->dataStart;			/* Assumes ptrs are to byte */
		chunkmap[nchunks].flags = OS_MEMPERM_UR | OS_MEMPERM_UW;
		nchunks++;
	}

	/* Optimise the chunks (sort, pagify and merge) and add the read-only data chunks
	*/
	nchunks = OS_MmuOptimise(chunkmap, nchunks);
	nchunks = OS_MmuPadRamReadonly(chunkmap, nchunks);

	/* Convert the chunks to pages. Returns -1 if there's a problem. This
	 * writes the page map into pagemap[] array.
	*/
	npages = OS_MmuMapPages(chunkmap, nchunks, pagemap, OS_nFreePageMaps);

	if ( npages < 0 )
	{
		result = OS_PANIC(OS_PANIC_InsufficientPageMaps);
	}
	else
	if ( npages > 0 )
	{
		archpagemap = OS_ArchMmuPageMapTranslate(pagemap, npages);

		if ( archpagemap == OS_NULL )
		{
			result = OS_PANIC(OS_PANIC_InsufficientHeap);
		}

		OS_ArchSetIsrPagemap(isrd, npages, archpagemap);
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
