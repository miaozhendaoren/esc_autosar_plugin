/* kern-stopreslocktiming.c
 *
 * This file contains the OS_StopResLockTiming function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-stopreslocktiming.c 18659 2014-08-27 08:50:00Z mist8519 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: UnusedVariable
 *   Variable is never used.
 *
 * Reason: Not an issue, variable will be used if timing protection is enabled.
 */

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_StopResLockTiming
 *
 * This function stops interrupt-lock timing for the resource. If execution
 * time monitoring was previously running the time remaining is computed
 * and monitoring is restarted.
 *
 * This function is only called if the resource lock was being timed.
 * Interrupts are disabled.
*/
void OS_StopResLockTiming(os_resourcedynamic_t *rd)
{
	os_tick_t used;

	OS_PARAM_UNUSED(rd);

	OS_ResetExecTimingInterrupt();
	used = OS_GetTimeUsed();

	OS_accounting.etUsed += used + OS_GET_RLOCK(rd->etUsed);
	OS_accounting.etLimit = OS_GET_RLOCK(rd->etLimit);
	OS_accounting.etType = OS_GET_RLOCK(rd->etType);

	if ( OS_accounting.inFunction == OS_INTASK )
	{
		if ( OS_GET_ACCT(((OS_taskCurrent->flags & OS_TF_MEASUREEXEC) != 0)
				&& (OS_taskCurrent->accounting->etMax < OS_accounting.etUsed)) )
		{
			OS_SET_ACCT(OS_taskCurrent->accounting->etMax = OS_accounting.etUsed);
		}
	}
	else
	if ( OS_accounting.inFunction == OS_INCAT2 )
	{	/* Possible diagnostic TOOLDIAG-1 <1> */
		const os_isr_t *isr = &OS_isrTableBase[OS_isrCurrent];

		if ( OS_GET_ACCT(((isr->flags & OS_ISRF_MEASUREEXEC) != 0)
				&& (isr->accounting->etMax < OS_accounting.etUsed)) )
		{
			OS_SET_ACCT(isr->accounting->etMax = OS_accounting.etUsed);
		}
	}
	else
	{
		/* MISRA-C */
	}


	if ( OS_accounting.etLimit > 0 )
	{
		if ( OS_accounting.etUsed >= OS_accounting.etLimit )
		{
			OS_ExceedExecTime();
		}
		else
		{
			OS_SetExecTimingInterrupt(OS_accounting.frc, (OS_accounting.etLimit - OS_accounting.etUsed));
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
