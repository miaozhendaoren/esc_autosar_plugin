/* kern-countleadingzeros.c
 *
 * This file contains the OS_CountLeadingZeros function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-countleadingzeros.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

#if OS_SIZEOF_CLZWORD==4

#define OS_MASK16	0xffff0000u
#define OS_MASK8	0xff00ff00u
#define OS_MASK4	0xf0f0f0f0u
#define OS_MASK2	0xccccccccu
#define OS_MASK1	0xaaaaaaaau

#elif OS_SIZEOF_CLZWORD==2

#define OS_MASK8	0xff00u
#define OS_MASK4	0xf0f0u
#define OS_MASK2	0xccccu
#define OS_MASK1	0xaaaau

#else
#error "OS_SIZEOF_CLZWORD is not defined. Check your architecture header files!"
#endif

/* OS_CountLeadingZeros
 *
 * This function returns the number of leading zeros in the binary representation of the input parameter.
 *
 * How it works:
 * =============
 *
 * First of all, if the word is zero we return NO-OF-BITS-IN-WORD.
 * The remainder of the function is a binary search for the most significant bit.
 * We start with a count of 0.
 * At each stage we are examining the upper n/2 bits of the n bits that remain to be examined.
 * If the upper n/2 bits are all zero (determined by an AND mask) we add n/2 to the count.
 * If one or more of the upper n/2 bits is non-zero, we discard the lower n/2 bits by retaining the
 * result of the AND mask.
 * The masks used in each successive stage contain alternately n/2 1s followed by n/2 0s. At each stage
 * we have discarded either the lower n/2 bits (explicitly) or the upper n/2 bits (implicitly, by determining
 * that they're all zero), so we "focus in" on the MSB.
 *
 * This masking is done instread of the more obvious mask and shift on efficiency grounds. On some processors
 * a multi-bit shift takes several cycles. Furthermore, the result of the AND should already be present in a register,
 * so no further operation is needed. An optimising compiler ought to be able to make the code very small,
 * but in some cases it might be better to implement this function in assembly language. Some processors
 * even have count-leading-zeros available as an instruction, which should obviously be used if possible.
 *
 * It would be possible to place the stages in a loop, but an optimising compiler might well unroll the loop,
 * and in any case the function needs to be as fast as possible, so leaving the loop unrolled in the source
 * code would force that even when optimising for size, since this function needs to be as fast as possible.
*/
os_int_t OS_CountLeadingZeros(os_clzword_t word)
{
	os_int_t nlz = 0;

	if ( word == 0 )
	{
		nlz = OS_SIZEOF_CLZWORD * 8;
	}
	else
	{
#if OS_SIZEOF_CLZWORD > 2
		if ( (word & OS_MASK16) == 0 )
		{
			nlz = 16;
		}
		else
		{
			word &= OS_MASK16;
		}
#endif

		if ( (word & OS_MASK8) == 0 )
		{
			nlz += 8;
		}
		else
		{
			word &= OS_MASK8;
		}

		if ( (word & OS_MASK4) == 0 )
		{
			nlz += 4;
		}
		else
		{
			word &= OS_MASK4;
		}

		if ( (word & OS_MASK2) == 0 )
		{
			nlz += 2;
		}
		else
		{
			word &= OS_MASK2;
		}

		if ( (word & OS_MASK1) == 0 )
		{
			nlz += 1;
		}
	}

	return nlz;
}

#include <memmap/Os_mm_code_end.h>

/* Unit test code.
 *
 * For a unit test, compile just this file on host PC with -DOS_UNIT_TEST.
 *
 * OS_ARCH=OS_LINUX
 * OS_CPU=OS_LINUX32
 * OS_TOOL=OS_gnu
 *
 * The size of the word that the function operates on can be varied by defining OS_SIZEOF_CLZWORD as 2 or 4
 *
 * Then run the resulting programs from the command line. The final result ("Test passed" or "Test failed")
 * indicates success or failure.
 *
 * Look in asc_Os/test/cunit for details
 *
 * Disable check_c parser
 * CHECK: NOPARSE
 *

*/
#ifdef OS_UNIT_TEST

#include <stdio.h>

#if OS_SIZEOF_CLZWORD == 4

#define NTEST	43

unsigned testdata[NTEST] =
{
	0x00000000, 0x00000001, 0x00000002, 0x00000004, 0x00000008,
				0x00000010, 0x00000020, 0x00000040, 0x00000080,
				0x00000100, 0x00000200, 0x00000400, 0x00000800,
				0x00001000, 0x00002000, 0x00004000, 0x00008000,
				0x00010000, 0x00020000, 0x00040000, 0x00080000,
				0x00100000, 0x00200000, 0x00400000, 0x00800000,
				0x01000000, 0x02000000, 0x04000000, 0x08000000,
				0x10000000, 0x20000000, 0x40000000, 0x80000000,
				0x00000003, 0x00000005, 0x0000000a,
				0x00000016,
				0x00000123,
				0x00001794,
				0x0001cf57,
				0x00139bd0,
				0x01788954,
				0x1845ef99
};

unsigned testresult[NTEST] =
{	32, 31, 30, 29, 28,
        27, 26, 25, 24,
        23, 22, 21, 20,
        19, 18, 17, 16,
        15, 14, 13, 12,
        11, 10,  9,  8,
         7,  6,  5,  4,
         3,  2,  1,  0,
		30, 29, 28,
        27,
        23,
        19,
        15,
        11,
         7,
         3
};

const char fmt[] = "data = 0x%08x : nlz = %2d (%s)\n";

#elif OS_SIZEOF_CLZWORD == 2

#define NTEST	23

unsigned testdata[NTEST] =
{
	0x0000, 0x0001, 0x0002, 0x0004, 0x0008,
			0x0010, 0x0020, 0x0040, 0x0080,
			0x0100, 0x0200, 0x0400, 0x0800,
			0x1000, 0x2000, 0x4000, 0x8000,
			0x0003, 0x0005, 0x000a,
			0x0016,
			0x0123,
			0x1794,
};

unsigned testresult[NTEST] =
{	16, 15, 14, 13, 12,
        11, 10,  9,  8,
         7,  6,  5,  4,
         3,  2,  1,  0,
		14, 13, 12,
        11,
         7,
         3
};

const char fmt[] = "data = 0x%04x : nlz = %2d (%s)\n";

#endif

int main(int argc, char **argv)
{
	int i;
	int fail, nfail = 0;
	int nlz;

	if ( OS_SIZEOF_CLZWORD != sizeof(os_clzword_t) )
	{
		printf("OS_SIZEOF_CLZWORD != sizeof(os_clzword_t) --- (%d,%d)\n", OS_SIZEOF_CLZWORD, sizeof(os_clzword_t));
		printf("Test needs configuring for your host\n");
		return 1;
	}

	for ( i = 0; i < NTEST; i++ )
	{
		nlz = OS_CountLeadingZeros(testdata[i]);
		fail = (nlz != testresult[i]);
		if ( fail )
		{
			nfail++;
		}

		printf(fmt, testdata[i], nlz, fail ? "Incorrect" : "OK");
	}

	if ( nfail == 0 )
	{
		printf("Test passed\n");
	}
	else
	{
		printf("Test failed\n");
	}

	return (nfail != 0);
}

#endif

/*
 * Re-enable check_c parser
 * CHECK: PARSE
*/

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
