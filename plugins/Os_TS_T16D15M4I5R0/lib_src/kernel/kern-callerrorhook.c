/* kern-callerrorhook.c
 *
 * This file contains the application-specific error hook wrapper.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-callerrorhook.c 17718 2014-02-17 15:31:32Z tojo2507 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 11.1 (required)
 * Conversions shall not be performed between a pointer to a function and
 * any type other than an integral type.
 *
 * Reason:
 * Depending on the architecture, the prototype of common assembly code for
 * setting up a hook's environment might use another function pointer type
 * than the one given here.
 *
 *
 * MISRA-2) Deviated Rule: 10.1 (required)
 * Implicit conversion of the binary ? left hand operand of underlying type
 * 'signed int' to 'signed char' that is not a wider integer type of the
 * same signedness
 *
 * Reason:
 * Tooling problem: all operands of the "?" operator in question are boolean.
 *
 *
 * MISRA-3) Deviated Rule: 16.2 (required)
 * Functions shall not call themselves, either directly or indirectly.
 *
 * Reason:
 * Centralized error-handling facility (OS_Error, OS_ErrorAction) which
 * provides several different reactions for detected errors, including
 * system shutdown or the call of a hook function. Functions for
 * triggering these reactions (e.g. OS_CallErrorHook, OS_Shutdown)
 * also handle errors using OS_Error. Implementation of OS_Error and
 * OS_ErrorAction ensures that no recursion is possible.
*/

#define OS_SID	OS_SID_HookHandler
#define OS_SIF	OS_svc_HookHandler

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_CallErrorHook() - call an application's error hook
 *
 * This function calls the error hook of an application.
 *
 * On entry, inKernel and inFunction are already set appropriately.
 *
 * Non-trusted error hooks have no explicit timing protection.
 * In most cases they run within the timing protection of the task or
 * ISR that caused the error.
 * Furthermore, practically no system services can be used by the
 * error hook functions, so there is little need to guard against
 * things like incorrect nesting of Get/ReleaseResource().
 * Suspend/ResumeOSInterrupts() are both NO-OPs when called from
 * error hooks.
 * However, the Suspend/Resume/Enable/DisableAllInterrupts() * pairs
 * are allowed, so here we save the nesting information and the current
 * interrupt level and start a new nesting context. On return from
 * the hook function we check for correct nesting, and in any case
 * restore the old nesting count and old level, and set the interrupt
 * level back the way it was.
 *
 * The hook function is called via the OS_ArchCallErrorHook wrapper.
 * This wrapper stores its own context as a way of returning from a killed
 * hook function.
 *
 * !LINKSTO Kernel.Autosar.OsApplication.Trust.Nontrusted, 2
 * !LINKSTO Kernel.Autosar.OsApplication.Trust.Trusted, 2
*/
/* Deviation MISRA-3 */
void OS_CallErrorHook(const os_appcontext_t *app, os_result_t code)
{
	const os_appcontext_t *saveHookApp;
	os_uint8_t saveNestSuspendAll;
	os_uint8_t saveOldSuspendAll;
	os_uint8_t saveInFunction;
	os_intstatus_t is;
	OS_HOOK_SAVE_INKERNEL_DECL

	/* Save and set the variable that tracks the current application
	 * for hook functions. We do this here so that error handling has
	 * has access to it.
	*/
	saveHookApp = OS_hookApp;
	OS_hookApp = app;

	/* Check that there's enough kernel stack for this hook.
	 *
	 * Should this check be enabled with STACKCHECK or permanently?
	 * At the moment it's always enabled.
	*/
	/* Deviation MISRA-2 */
	if ( OS_InsufficientStackForErrHook(app) )
	{
		/* can't propagate the return value of OS_ERROR -> ignore it */
		(void) OS_ERROR(OS_ERROR_InsufficientStack, OS_NULL);
	}
	else
	{
		saveInFunction = OS_inFunction;
		OS_inFunction = OS_INERRORHOOK;

		/* Interrupts are already disabled, but we "disable" here again
		 * to get the current state to recover from an error in the hook
		 * function. We also save the nesting state for the services
		 * that can be used inside the hook function.
		*/
		is = OS_IntDisableConditional();
		saveNestSuspendAll = OS_nestSuspendAll;
		saveOldSuspendAll = OS_oldSuspendAll;
		OS_nestSuspendAll = 0;

		/* Now call the hook. The value of OS_inKernel is saved on the "counting" architectures only,
		 * and only in the "killable" branch.
		*/
		if ( OS_CallAppEHookDirectly() )
		{
			(*(app->errorHook))(code);
		}
		else
		{
			OS_HOOK_SAVE_INKERNEL();
			/* Deviation MISRA-1 */
			OS_ArchCallErrorHook(app, code);

			/* The exception handler might "return" here directly without
			 * executing the tail end of ArchCallErrorHook() -
			 * architecture-dependent.
			*/
			OS_HOOK_RESTORE_INKERNEL();

			if ( (app->flags & OS_APP_TRUSTED) == 0 )
			{
				if ( OS_isrCurrent < OS_nInterrupts )
				{
					OS_SETISRPROTECTION(&OS_isrTableBase[OS_isrCurrent], &OS_isrDynamicBase[OS_isrCurrent]);
				}
				else
				if ( OS_taskCurrent != OS_NULL )
				{
					OS_SETPROTECTION(OS_taskCurrent);
				}
				else
				{
					/* MISRA-C */
				}
			}
		}

		if ( OS_nestSuspendAll != 0 )
		{
			/* Calls to Suspend/ResumeAllInterrupts() inside the
			 * error hook function were not correctly nested.
			 *
			 * !LINKSTO Kernel.Feature.RuntimeChecks.CheckSuspendResumeNesting, 1
		     * !LINKSTO Kernel.Feature.RuntimeChecks, 1
			 */
			/* can't propagate the return value of OS_ERROR -> ignore it */
			(void) OS_ERROR(OS_ERROR_InterruptDisabled, OS_NULL);
		}

		/* Restore the saved interrupt status
		*/
		OS_nestSuspendAll = saveNestSuspendAll;
		OS_oldSuspendAll = saveOldSuspendAll;
		OS_IntRestore(is);
		OS_inFunction = saveInFunction;
	}

	/* Restore the saved current application for hook functions
	*/
	OS_hookApp = saveHookApp;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
