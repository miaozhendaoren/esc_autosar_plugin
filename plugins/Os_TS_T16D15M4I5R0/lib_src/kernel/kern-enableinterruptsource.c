/* kern-enableinterruptsource.c
 *
 * This file contains the OS_KernEnableInterruptSource function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-enableinterruptsource.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/

#define OS_SID	OS_SID_EnableInterruptSource
#define OS_SIF	OS_svc_EnableInterruptSource

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernEnableInterruptSource implements the API EnableInteruptSource
 *
 * The interrupt source corresponding to the given parameter is enabled.
*/
os_result_t OS_KernEnableInterruptSource(os_isrid_t i)
{
	os_result_t r = OS_E_OK;
	const os_isr_t *isr;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)i);

	OS_TRACE_ENABLEINTERRUPTSOURCE_ENTRY(i);
	if ( i >= OS_nInterrupts )
	{
		r = OS_ERROR(OS_ERROR_InvalidIsrId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		/* !LINKSTO Kernel.Feature.InterruptSource.API.EnableInterruptSource, 1
		*/
		isr = &OS_isrTable[i];

		OS_EnableIsr(isr);
	}

	OS_TRACE_ENABLEINTERRUPTSOURCE_EXIT_P(r,i);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserEnableInterruptSource</name>
  <synopsis>Enable the Specified Interrupt Source </synopsis>
  <syntax>
    os_result_t OS_UserEnableInterruptSource(os_isrid_t)
  </syntax>
  <description>
    <code>OS_UserEnableInterruptSource()</code> enables the specified
    interrupt source.
    </description>
  <availability>
    No restrictions.
  </availability>
  <returns>E_OS_OK = success</returns>
  <returns>E_OS_ID = the isr id was invalid</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
