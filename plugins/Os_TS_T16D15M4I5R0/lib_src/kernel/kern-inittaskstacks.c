/* kern-inittaskstacks.c
 *
 * This file contains the OS_InitTaskStacks function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-inittaskstacks.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an initialization array or filling the stack to make them more
 * readable and maintainable.
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitTaskStacks()
 *
 * Set all task stacks to 0xee (or whatever the fill pattern is)
*/
void OS_InitTaskStacks(void)
{
	os_unsigned_t slen;
	os_stackelement_t *p;
	os_taskid_t t;
	const os_task_t *tp;

	tp = OS_taskTable;
	for ( t=0; t < OS_nTasks; t++ )
	{
		p = tp->stackBase;

		/* Previously there was some code here that omitted to initialise stacks that were
		 * already initialised ( "if ( *p == OS_STACKFILL )" ), but that is not guaranteed
		 * to work, especially around a warm reboot.
		 * So we've removed the test, and now all stacks are initialised even if they've
		 * already been initialised. The disadvantage is a small increase in startup time.
		 * The old code also didn't take account of the fact that the shared stacks might
		 * be of different sizes.
		*/
		slen = tp->stackLen/sizeof(os_stackelement_t);
		while ( slen > 0 )
		{
			*p = OS_STACKFILL;
			/* Deviation MISRA-1 */
			p++;
			slen--;
		}

		/* Deviation MISRA-1 */
		tp++;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
