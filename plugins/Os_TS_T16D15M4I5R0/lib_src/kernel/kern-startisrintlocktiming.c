/* kern-startisrintlocktiming.c
 *
 * This file contains the OS_StartIsrIntLockTiming function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startisrintlocktiming.c 18959 2014-09-25 13:25:33Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if timing protection is enabled.
 */

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_StartIsrIntLockTiming
 *
 * This function starts interrupt-lock timing for the ISR. If execution
 * time monitoring is already running the time remaining is computed
 * and stored in the global save location for this purpose.
*/
void OS_StartIsrIntLockTiming(os_isrid_t i, os_intlocktype_t locktype)
{
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_isr_t *isr;
	os_tick_t locktime;
	os_tick_t used;
	os_tick_t remaining;

	isr = &OS_isrTable[i];
	locktime = OS_GET_ILOCK((locktype == OS_LOCKTYPE_OS) ? isr->osLockTime : isr->allLockTime);

	if ( locktime != 0 )
	{
		OS_ResetExecTimingInterrupt();
		used = OS_GetTimeUsed();

		OS_accounting.etUsed += used;

		if ( OS_GET_ACCT(((isr->flags & OS_ISRF_MEASUREEXEC) != 0)
				&& (isr->accounting->etMax < OS_accounting.etUsed)) )
		{
			OS_SET_ACCT(isr->accounting->etMax = OS_accounting.etUsed);
		}

		if ( (OS_accounting.etLimit != 0) && (OS_accounting.etUsed >= OS_accounting.etLimit) )
		{
			OS_ExceedExecTime();
		}
		else
		{
			if ( OS_accounting.etLimit == 0 )
			{
				remaining = OS_MAXTICK;
			}
			else
			{
				remaining = OS_accounting.etLimit - OS_accounting.etUsed;
			}

			if ( locktype == OS_LOCKTYPE_OS )
			{
				OS_accounting.osSaveType = OS_accounting.etType;
				OS_accounting.osSaveUsed = OS_accounting.etUsed;
				OS_accounting.osSaveLimit = OS_accounting.etLimit;
			}
			else
			{
				OS_accounting.allSaveType = OS_accounting.etType;
				OS_accounting.allSaveUsed = OS_accounting.etUsed;
				OS_accounting.allSaveLimit = OS_accounting.etLimit;
			}

			/* Never allow an ISR to extend its execution budget by disabling
			 * interrupts!
			*/
			OS_accounting.etUsed = 0;

			if (locktime >= remaining)
			{	/* new limit is greater or equal the remaining time */
				/* use remaining time and leave accounting type as it was */
				OS_accounting.etLimit = remaining;
			}
			else
			{	/* new limit is smaller then remaining time */
				/* use new time for accounting and set new accounting type */
				OS_accounting.etLimit = locktime;
				OS_accounting.inFunction = OS_INCAT2;
				OS_accounting.etType = OS_ACC_INTLOCK;
			}

			OS_SetExecTimingInterrupt(OS_accounting.frc, OS_accounting.etLimit);
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
