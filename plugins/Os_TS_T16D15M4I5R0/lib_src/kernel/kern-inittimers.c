/* kern-inittimers.c
 *
 * This file contains the OS_InitTimers function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-inittimers.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an initialization array to make them more readable and maintainable.
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitTimers
 *
 * Each configured timer is initialised by calling its driver's "INIT" function.
 *
 * !LINKSTO Kernel.Autosar.Counter.Types.HARDWARE.Initialisation, 1
*/
void OS_InitTimers(void)
{
	os_unsigned_t i = OS_nHwTimers;
	const os_hwt_t *t = OS_hwTimer;

	while ( i > 0 )
	{
		OS_HwtInit(t);

		/* Deviation MISRA-1 */
		t++;
		i--;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
