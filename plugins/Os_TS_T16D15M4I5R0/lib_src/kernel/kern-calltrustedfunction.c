/* kern-calltrustedfunction.c
 *
 * This file contains the OS_KernCallTrustedFunction function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-calltrustedfunction.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 16.7 (required)
 * A pointer parameter in a function prototype should be declared as pointer
 * to const if the pointer is not used to modify the addressed object.
 *
 * Reason:
 * The "parms" parameter is given to a trusted function that may in fact
 * modify the addressed object. For some architectures, calling this trusted
 * function may be implemented in a way that the MISRA-C checker can't
 * deduce this fact.
*/

#define OS_SID	OS_SID_CallTrustedFunction
#define OS_SIF	OS_svc_CallTrustedFunction

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernCallTrustedFunction implements the API CallTrustedFunction
 *
 * Trusted functions are executed with kernel privileges on the kernel
 * stack in a kernel environment. The latter means that system calls
 * behave slightly differently.
 *
 * Calls that would not normally return immediately to the caller in a task
 * environment, such as ChainTask, TerminateTask, WaitEvent, Schedule and
 * ActivateTask will immediately return to the caller. Any context switch
 * that results from a system call inside a trusted function will happen
 * when the trusted function returns to its caller.
 *
 * If the trusted function has been called from an ISR context, ActivateTask
 * would do this in any case and the others are not permitted, so the
 * trusted function has to be written to handle this possibility in any case.
 *
 * !LINKSTO Kernel.Autosar.TrustedFunctions, 2
*/
os_result_t OS_KernCallTrustedFunction
(	os_functionid_t fid,
	/* Deviation MISRA-1 */
	void *parms
)
{
	os_result_t r = OS_E_OK;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
	const os_function_t *fs;
	OS_PARAMETERACCESS_DECL
	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)fid);
	OS_SAVE_PARAMETER_N(1,(os_paramtype_t)parms);

	OS_TRACE_CALLTRUSTEDFUNCTION_ENTRY(fid);

	/* !LINKSTO Kernel.Autosar.API.SystemServices.CallTrustedFunction.CalledByIsr, 1
	 * !LINKSTO Kernel.Autosar.API.SystemServices.CallTrustedFunction.CalledByTask, 1
	*/
	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( fid >= OS_nFunctions )
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.CallTrustedFunction.UnknownFunction, 1
		 * !LINKSTO Kernel.Autosar.TrustedFunctions.NotConfigured, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidFunctionId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		fs = &OS_functionTableBase[fid];

		app = OS_CurrentApp();

		if ( !OS_HasPermission(app, fs->permissions) )
		{
			r = OS_ERROR(OS_ERROR_Permission, OS_GET_PARAMETER_VAR());
		}
		else
		{
			os_address_t upperSp;
			os_address_t lowerSp;
			os_address_t lowerBound;
			os_address_t upperBound;

#if OS_STACKGROWS==OS_STACKGROWSDOWN
			OS_GetCallerSp(&upperSp);
			lowerSp = upperSp - fs->stackLen;
#else
			OS_GetCallerSp(&lowerSp);
			upperSp = lowerSp + fs->stackLen;
#endif

			OS_GetTrustedFunctionStackBounds(&lowerBound, &upperBound);

			if ( (lowerSp < lowerBound) || (upperSp > upperBound) || (lowerSp > upperSp) )
			{
				r = OS_ERROR(OS_ERROR_StackError, OS_GET_PARAMETER_VAR());
			}
			else
			{
				OS_PushTrustedFunction(fid, fs->function, parms);
			}
		}
	}

	OS_TRACE_CALLTRUSTEDFUNCTION_EXIT_P(r,fid);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserCallTrustedFunction</name>
  <synopsis>Call a trusted function</synopsis>
  <syntax>
    os_result_t OS_UserCallTrustedFunction
    (   os_functionid_t fid,    /@ Id of function @/
        void *parms             /@ Pointer parameter to pass @/
    )
  </syntax>
  <description>
    <para>
    <code>OS_UserCallTrustedFunction()</code> calls the referenced
    trusted function with the parameter supplied, provided that the
    caller is in a permitted context and has permission to make the call.
    </para><para>
    It is recommended to make trusted functions as short as possible,
    doing only those jobs such as accessing peripheral devices that can
    only be done with full privileges. It is not recommended to call
    OSEK or AUTOSAR system services from a trusted function.
    </para><para>
    However, if it is absolutely necessary to use system services
    from a trusted function, please take careful note of the following
    restrictions and differences in semantic behaviour:
    </para><para>
    The trusted function is called in a kernel environment, which means that
    all system calls that it makes will return immediately to the caller;
    any resulting task switch will not happen until the trusted function
    returns, thus affecting the calling task but not the trusted function.
    </para><para>
    If the trusted function has been called from an ISR (category 2) context,
    the system services that it can call are restricted accordingly. Calling
    a system service that is not permitted will result in an error code
    being returned to the trusted function. In normal status mode it
    is possible that the calling application could have been terminated.
    </para>
  </description>
  <availability>
    No restrictions.
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>
