/* kern-stopisrintlocktiming.c
 *
 * This file contains the OS_StopIsrIntLockTiming function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-stopisrintlocktiming.c 18407 2014-07-08 07:54:12Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if timing protection is enabled.
 */

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_StopIsrIntLockTiming
 *
 * This function stops interrupt-lock timing for the isr. If execution
 * time monitoring was previously running the time remaining is computed
 * and monitoring is restarted.
*/
void OS_StopIsrIntLockTiming(os_isrid_t i, os_intlocktype_t locktype)
{
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_isr_t *isr;
	os_tick_t locktime;
	os_tick_t used;

	isr = &OS_isrTable[i];
	locktime = OS_GET_ILOCK((locktype == OS_LOCKTYPE_OS) ? isr->osLockTime : isr->allLockTime);

	if ( locktime != 0 )
	{
		OS_ResetExecTimingInterrupt();
		used = OS_GetTimeUsed();

		if ( locktype == OS_LOCKTYPE_OS )
		{
			OS_accounting.etUsed += used + OS_accounting.osSaveUsed;
			OS_accounting.etLimit = OS_accounting.osSaveLimit;
			OS_accounting.etType = OS_accounting.osSaveType;
		}
		else
		{
			OS_accounting.etUsed += used + OS_accounting.allSaveUsed;
			OS_accounting.etLimit = OS_accounting.allSaveLimit;
			OS_accounting.etType = OS_accounting.allSaveType;
		}

		if ( OS_GET_ACCT(((isr->flags & OS_ISRF_MEASUREEXEC) != 0)
				&& (isr->accounting->etMax < OS_accounting.etUsed)) )
		{
			OS_SET_ACCT(isr->accounting->etMax = OS_accounting.etUsed);
		}

		if ( OS_accounting.etLimit != 0 )
		{
			if ( OS_accounting.etUsed >= OS_accounting.etLimit )
			{
				OS_ExceedExecTime();
			}
			else
			{
				OS_SetExecTimingInterrupt(OS_accounting.frc, (OS_accounting.etLimit - OS_accounting.etUsed));
			}
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
