/* kern-mkwgetcountervalue.c
 *
 * This file contains the OS_MkwGetCounterValue function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: mkw-getcountervalue.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#if OS_KERNEL_TYPE != OS_MICROKERNEL
#error "This file is only needed for a microkernel build! Check your makefiles!"
#endif

#include <memmap/Os_mm_code_begin.h>

/* OS_MkwGetCounterValue
 *
 * This function provides a wrapper for OS_KernGetCounterValue so that it can be called
 * in an OS thread in the microkernel
*/
os_tick_t OS_MkwGetCounterValue(os_counterid_t c)
{
	os_tick_t localcount;
	os_result_t result = OS_KernGetCounterValue(c, &localcount);

#if MK_HAS_DUAL_RETURN_VALUE
	MK_UsrTerminateSelfWithValue(result, localcount);	/* Does not return */
#else	/* TEMPORARY */
	if ( result != OS_E_OK )
	{
		localcount = 0;
	}
#endif

	/* Not reached if MK_HAS_DUAL_RETURN_VALUE
	*/
	return localcount;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
