/* kern-mmumappages.c
 *
 * This file contains the OS_MmuMapPages function
 *
 * This function converts the specified region maps into true page maps.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmumappages.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_MmuMapper
 *
 * This function creates an MMU mapping for the data areas of a given task.
*/
os_int_t OS_MmuMapPages
(	os_pagemap_t *chunkmap,		/* Input: Map of all the "chunks" (memory regions) */
	os_int_t nchunks,			/* No. of chunks in the chunkmap */
	os_pagemap_t *pagemap,		/* Output: Map of all pages */
	os_int_t maxpages			/* Max. no of pages allowed */
)
{
	os_int_t npages = 0;
	os_int_t i;

	/* Lets see if we can map them in the MMU without having to jigger* around with the MMU
	 * during the runtime of the task or ISR.
	*/
	for ( i = 0; i < nchunks; i++ )
	{
		npages += OS_MmuPageMap(chunkmap[i].base, chunkmap[i].size, chunkmap[i].flags, OS_NULL);
	}

	if ( npages > maxpages )
	{
		/* No, there are too many pages. Return an error code
		*/
		npages = -1;
	}
	else
	{
		/* It fits, Dandini, it fits!
		 * Repeat the above procedure, but this time write the page mappings into the output array.
		*/
		npages = 0;

		for ( i = 0; i < nchunks; i++ )
		{
			npages += OS_MmuPageMap(chunkmap[i].base, chunkmap[i].size, chunkmap[i].flags, &pagemap[npages]);
		}
	}

	/* Keep a note of the largest number of pages needed for any individual task, ISR or app-hook.
	*/
	if ( OS_maxObjectMaps < npages )
	{
		OS_maxObjectMaps = npages;
	}

	return npages;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
