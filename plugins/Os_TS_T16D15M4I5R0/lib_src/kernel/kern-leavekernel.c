/* kern-leavekernel.c
 *
 * This file contains the OS_LeaveKernel function for user-mode kernels.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-leavekernel.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#define OS_SID	OS_SID_Dispatch
#define OS_SIF	OS_svc_Dispatch

#include <Os_kernel.h>
#include <Os_panic.h>
#include <Os_taskqueue.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_LeaveKernel
 *
 * This function is called to leave the kernel. It is only used in
 * user-mode kernels (when no system-call interface is used).
 *
 * The inKernel counter is tested. If it is 1, a task-switch is tested for and
 * performed if necessary. If inKernel is not 1, it is decremented.
*/
os_unsigned_t OS_LeaveKernel(os_unsigned_t result)
{
	if ( OS_ExtraCheck(OS_inKernel == 0) )
	{
		/* Serious kernel error
		*/
		/* can't propagate the return value of OS_PANIC -> ignore it */
		(void) OS_PANIC(OS_PANIC_IncorrectKernelNesting);
	}
	else
	if ( OS_inKernel <= 1 )
	{
		os_intstatus_t is;

		if ( OS_KernelStackOverflow() )
		{
			/* can't propagate the return value of OS_ERROR -> ignore it */
			(void) OS_ERROR(OS_ERROR_KernelStackOverflow, OS_NULL);
		}

		/* Leaving the kernel. The testing for a task switch must be done with interrupts disabled.
		*/
		is = OS_IntDisableConditional();

		if ( OS_taskQueueHead == OS_taskCurrent )
		{
			if ( OS_taskCurrent == OS_NULL )
			{
				/* Nothing to do - will end in idle task
				*/
				OS_FreeCurrentContext();
				OS_Dispatch(OS_TS_READY_SYNC);		/* Never returns */
			}
			else
			{
				/* Three possibilities for current task's state here:
				 *  RUNNING - no task switch, just return to caller
				 *  READY_SYNC - task waited for an event which was set in an ISR
				 *  NEW - task has chained self, or terminated with 2nd activation queued, or been killed & reactivated
				*/
				if ( OS_taskCurrent->dynamic->state == OS_TS_RUNNING )
				{
					/* No task switch - return to caller.
					 * But first, set the task's running priority if necessary.
					*/
					if ( OS_taskCurrent->dynamic->prio < OS_taskCurrent->runPrio )
					{
						OS_taskCurrent->dynamic->prio = OS_taskCurrent->runPrio;
#if OS_USE_CLZ_QUEUE_ALGORITHM
						OS_InsertTask(OS_taskCurrent, OS_taskCurrent->runPrio);
#endif
					}

				}
				else if ( OS_taskCurrent->dynamic->state > OS_TS_MAX_TERMINATING )
				{
					/* ASCOS-2610 */
					OS_SaveTaskContextSync(&OS_taskCurrent->dynamic->context);
				}
				else
				{
					/* Current task is terminating and restarting
					*/
					OS_FreeCurrentContext();
					OS_StoreTaskSp();					/* For stack check */
					OS_Dispatch(OS_TS_READY_SYNC);		/* Never returns */
				}
			}
		}
		else
		{
			if ( ( OS_taskCurrent == OS_NULL ) || ( OS_taskCurrent->dynamic->state <= OS_TS_MAX_TERMINATING ) )
			{
				OS_FreeCurrentContext();
				if ( OS_taskCurrent != OS_NULL )
				{
					OS_StoreTaskSp();					/* For stack check */
				}
				OS_Dispatch(OS_TS_READY_SYNC);		/* Never returns */
			}
			else
			{
				/* ASCOS-2610 */
				OS_SaveTaskContextSync(&OS_taskCurrent->dynamic->context);
			}
		}

		OS_inKernel = 0;
		OS_IntRestore(is);
	}
	else
	{
		OS_inKernel--;
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
