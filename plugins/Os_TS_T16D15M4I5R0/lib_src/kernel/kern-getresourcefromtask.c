/* kern-getresourcefromtask.c
 *
 * This file contains the OS_GetResourceFromTask function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getresourcefromtask.c 18659 2014-08-27 08:50:00Z mist8519 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#define OS_SID	OS_SID_GetResource
#define OS_SIF	OS_svc_GetResource

#include <Os_kernel.h>
#include <Os_taskqueue.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_GetResourceFromTask
 *
 * This function implements part of the GetResource system service in the kernel, when the caller is a Task
*/
os_result_t OS_GetResourceFromTask(os_resourceid_t r, os_resourcedynamic_t *rd, os_paramtype_t *p)
{
	os_result_t result = OS_E_OK;
	const os_resource_t *rs = &OS_resourceTableBase[r];
	os_intstatus_t is;
	os_taskdynamic_t *td;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
#if OS_USE_CLZ_QUEUE_ALGORITHM
	os_prio_t taskPrio;
#else
	os_tasklink_t link;
#endif

	OS_PARAM_UNUSED(p);

	/* !LINKSTO Kernel.API.ResourceManagement.GetResource.Task, 1
	*/
	app = OS_GET_APP(OS_taskCurrent->app);

	if ( !OS_HasPermission(app, rs->permissions) )
	{
		result = OS_ERROR(OS_ERROR_Permission, p);
	}
	else
	if ( OS_taskCurrent->queuePrio > rs->prio )
	{
		/* !LINKSTO Kernel.API.ResourceManagement.GetResource.LowCeilingPriority, 1
		 * !LINKSTO Kernel.API.ResourceManagement.ReleaseResource.LowCeilingPriority, 1
		*/
		result = OS_ERROR(OS_ERROR_ResourcePriorityError, p);
	}
	else
	{
		td = OS_taskCurrent->dynamic;

		/* Now we must disable interrupts to prevent an ISR that
		 * takes the resource from overwriting what we write into
		 * the dynamic structure.
		*/
		is = OS_IntDisable();

		/* If the task is no longer running (perhaps killed by
		 * a timeout interrupt) we don't want to occupy the resource
		*/
		if ( td->state == OS_TS_RUNNING )
		{
			/* Chain the resource into the task's resource queue.
			 * !LINKSTO Kernel.API.ResourceManagement.GetResource.NestedResources, 1
			*/
			rd->next = td->lastRes;
			td->lastRes = r;

			/* Store the old task priority into the resource's
			 * save location.
			*/
			rd->lastPrio = td->prio;

			/* Set current task as taker of the resource
			*/
			rd->takenBy = OS_TpToTid(OS_taskCurrent);

			/* Start resource-lock timing if necessary
			*/
			if ( OS_GET_RLOCK((OS_taskCurrent->resourceLockTime != OS_NULL) &&
					(OS_GET_RLOCK(OS_taskCurrent->resourceLockTime[r]) != 0)) )
			{
				OS_STARTRESLOCKTIMING(rd, OS_taskCurrent->resourceLockTime[r]);
			}

			/* If resource's priority is higher, set the new task
			 * priority.
			 * We bail out here if the task is no longer running. It could have been shot down
			 * during OS_STARTRESLOCKTIMING().
			 * !LINKSTO Kernel.ResourceManagement.PriorityCeiling, 1
			*/
			if ( (td->state == OS_TS_RUNNING) && (rs->prio > rd->lastPrio) )
			{
				/* Raise the task's priority.
				 *
				 * !LINKSTO Kernel.API.ResourceManagement.GetResource.PriorityCeiling, 1
				*/
#if OS_USE_CLZ_QUEUE_ALGORITHM
				taskPrio = (rs->prio > OS_maxPrio) ? ((os_prio_t) OS_maxPrio) : rs->prio;
				if ( td->prio < taskPrio )
				{
					OS_InsertTask(OS_taskCurrent, taskPrio);
				}
				td->prio = rs->prio;
#else
				if ( OS_taskQueueHead == OS_taskCurrent )
				{
					/* The task is still at the head of the queue, so just do it.
					*/
					td->prio = rs->prio;
				}
				else
				{
					/* The task is no longer at the head of the queue. A
					 * higher-priority task has become active as a result of an interrupt.
					*/
					link = OS_CurrentLink(OS_taskCurrent, td);
					td->prio = rs->prio;
					OS_RequeueUp(link, td->prio);
				}
#endif

				if ( OS_IsIsrPrio(rs->prio) )
				{
					/* This sets the current interrupt level, so we must
					 * not restore the saved interrupt status afterwards.
					*/
					OS_SetIsrResourceLevel(rs->prio);
					OS_IntRestoreHardLock(is);
				}
				else
				{
					/* Interrupt status is restored in this branch.
					*/
					OS_IntRestore(is);
				}
			}
			else
			{
				/* Interrupt status is restored in this branch.
				*/
				OS_IntRestore(is);
			}
		}
		else
		{
			/* Killed task - just restore interrupt status.
			*/
			OS_IntRestore(is);
		}

		/* No call to OS_IntRestore() here. See comments above
		*/
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
