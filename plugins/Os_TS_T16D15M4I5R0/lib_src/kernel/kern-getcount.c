/* kern-getcount.c
 *
 * This file contains the OS_GetCount function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getcount.c 17973 2014-04-16 09:26:25Z stpo8218 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_GetCount
 *
 * The current tick value of the counter is returned.
 *
 * For software counters it is simply the "current" value.
 *
 * For hardware counters, the time elapsed on the hardware must be added to the current value
 * and any alarms that expire as a reult must be processed. If the counter is locked, the
 * current value is returned. This means that the value will reflect the state of the alarm queue.
*/
os_tick_t OS_GetCount(os_counterid_t c, const os_counter_t *cs)
{
	os_tick_t count;
	os_counterdynamic_t *cd;

	cd = &OS_counterDynamicBase[c];

	if ( OS_CounterIsHw(cs) && (cd->lock == 0) )
	{
		/* For a hardware counter that is not locked we need to update the counter.
		*/
		OS_CtrUpdate(cs, cd);
	}

	OS_ATOMIC_ASSIGN_32(count, cd->current);

	return count;
}

#include <memmap/Os_mm_code_end.h>

/* API entries for documentation
 * CHECK: NOPARSE

<api API="OS_INTERNAL">
  <name>OS_GetCount</name>
  <synopsis>Get the current value of the counter</synopsis>
  <syntax>
    os_tick_t OS_GetCount
    (   os_counterid_t c,      /@ ID of the counter @/
        const os_counter_t *cs /@ Counter's static data @/
    )
  </syntax>
  <description>
    <code>OS_GetCount()</code> returns the current value of the specified
    counter. If the counter is a hardware counter that is locked, the best
    approximation is the current value according to the counter's dynamic
    data. This happens if the function is called in an ISR that interrupts
    the counter ISR.
  </description>
</api>

 * CHECK: PARSE
*/

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
