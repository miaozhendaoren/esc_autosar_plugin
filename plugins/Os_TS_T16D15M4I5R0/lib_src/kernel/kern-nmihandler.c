/* kern-nmihandler.c
 *
 * This file contains the OS_NmiHandler function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-nmihandler.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#define OS_SID	OS_SID_TrapHandler
#define OS_SIF	OS_svc_TrapHandler

#include <Os_kernel.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_NmiHandler()
 *
 * THe default action for NMI is simply to report the problem.
 * If the application demands a different kind of handling, simply
 * ensure that an application-specific version of this function is
 * linked instead.
*/
void OS_NmiHandler(void)
{
	/* can't propagate the return value of OS_PANIC -> ignore it */
	(void) OS_PANIC(OS_PANIC_UnhandledNmi);
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
