/* kern-taskspoutofbounds.c
 *
 * This file contains the OS_TaskSpOutOfBounds function. It returns TRUE if the
 * task's SP lies outside the permitted boundary.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-taskspoutofbounds.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 11.3 (advisory)
 * A cast should not be performed between a pointer type and an integral type.
 *
 * Reason:
 * Pointer arithmetic of this type is well-defined, and this is the only reliable
 * way to perform the desired operation in standard C.
 */

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_TaskSpOutOfBounds
 *
 * This function returns TRUE if the task's stack pointer lies outside the
 * permitted boundaries.
 *
 * The code here assumes that (in the case of STACKGROWSDOWN at least) the
 * stack model is "decrement before store", so that a stack pointer that is
 * pointing just outside the stack at the top is perfectly OK.
 *
 * If the stack pointer provided is OS_NULL we return FALSE (==OK) because
 * on some architectures a stack-pointer might not be available sometimes.
 * (see TRICORE)
 *
 * There are two flavours of this function, depending on whether the
 * architecture macros OS_GetTaskSp and OS_GetTaskSpForce return an os_address_t
 * or an os_stackelement_t*. The latter is the preferred return value for new
 * architectures because on some architectures the casting to a scalar type doesn't
 * give the expected results.
*/
#if OS_GETTASKSP_PTR
os_boolean_t OS_TaskSpOutOfBounds(os_stackelement_t *sp, const os_task_t *tp)
{
	os_boolean_t result = OS_FALSE;
	os_int32_t nElem;

	if ( sp != 0 )
	{
		/* This implementation relies on the fact that in the C specification,
		 * the "difference" beween two pointers of the same type is defined to be
		 * the number of elements of that type that lie between them.
		*/

		/* Deviation MISRA-1 */
		nElem = sp - (os_stackelement_t *)(tp->stackBase);

		if ( (nElem < 0) || (nElem > (tp->stackLen / sizeof(os_stackelement_t))) )
		{
			result = OS_TRUE;
		}
	}

	return result;
}
#else
os_boolean_t OS_TaskSpOutOfBounds(os_address_t sp, const os_task_t *tp)
{
	os_address_t base;

	base = (os_address_t)tp->stackBase;

	return ( sp != 0 ) && ( (sp < base) || (sp > (base + tp->stackLen)) );
}
#endif

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
