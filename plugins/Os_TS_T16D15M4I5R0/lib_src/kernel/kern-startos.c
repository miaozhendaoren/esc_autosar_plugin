/* kern-startos.c
 *
 * This file contains the OS_KernStartOs function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startos.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an configuration array to make them more readable and maintainable.
*/

#define OS_SID	OS_SID_StartOs
#define OS_SIF	OS_svc_StartOs

#include <Os_kernel.h>

/* The behaviour of StartOS has to be radically different when running under the microkernel.
 * Many of the differences are achieved by restricting or changing the set of InitFuncs,
 * but the rest of the code needs adapting too.
 *
 * We define the macros here because they are only used here. There's no point constructing a
 * special header file for them
*/
#if OS_KERNEL_TYPE == OS_MICROKERNEL

#define OS_StartupInterruptsSuspended()	OS_FALSE
#define OS_ErrorHandlingForStartOs()	OS_TRUE

#else

#include <Os_cpuload_kernel.h>

#define OS_StartupInterruptsSuspended()	( (OS_iecMode >= OS_IEC_AUTOSAR) && \
                                            ( (OS_nestSuspendAll != 0) || (OS_nestSuspendOs != 0) ) )
#define OS_ErrorHandlingForStartOs()	OS_ErrorHandlingForVoidApi()

#endif

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernStartOs
 *
 * The OS is started.
 *
 *  - If the OS is already running --> ERROR
 *  - Initialise stacks.
 *	- Activate all AUTOSTART tasks and alarms for this mode.
 *	- Perform all other initialisation required before returning.
 *
 * !LINKSTO Kernel.API.OSControl.StartOS.API, 1
 *
 * If the kernel variables contain their correct initial values when StartOS is first called,
 * the first call to StartOS can never return. This is because on return from OS_KernStartOs(),
 * there is no task ready and the dispatcher ends in the idle loop. This happens even if an error
 * condition such as interrupts-disabled or parameter-out-of-range is detected.
 *
 * !LINKSTO Kernel.API.OSControl.StartOS.NoReturn, 1
 * !LINKSTO Kernel.Autosar.OSEK.Differences.StartOS, 1
*/
void OS_KernStartOs(os_uint8_t mode)
{
	os_intstatus_t is;
	const os_initfunc_t * fct;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)mode);

	OS_TRACE_STARTOS_ENTRY(mode);

	is = OS_IntDisableAll();

	if ( OS_GetInFunction() != OS_INBOOT )
	{
		if ( OS_ErrorHandlingForStartOs() )
		{
			/* can't propagate the return value of OS_ERROR -> ignore it */
			(void) OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
		}
		OS_IntRestoreAll(is);
	}
	else
	if ( OS_StartupInterruptsSuspended() )
	{
		if ( OS_ErrorHandlingForStartOs() )
		{
			/* can't propagate the return value of OS_ERROR -> ignore it */
			(void) OS_ERROR(OS_ERROR_InterruptDisabled, OS_NULL);
		}
		OS_IntRestoreAll(is);
	}
	else
	if ( mode >= OS_nStartModes )
	{
		if ( OS_ErrorHandlingForStartOs() )
		{
			/* can't propagate the return value of OS_ERROR -> ignore it */
			(void) OS_ERROR(OS_ERROR_InvalidStartMode, OS_GET_PARAMETER_VAR());
		}
		OS_IntRestoreAll(is);
	}
	else
	{
		/* !LINKSTO Kernel.API.OSControl.StartOS.StartOS, 1
		*/
		OS_mode = mode;

		OS_SetInFunction(OS_ININTERNAL);

		fct = &OS_initFunc[0];
		while (*fct != OS_NULL)
		{
			(*fct)();
			/* Deviation MISRA-1 */
			fct++;
		}

#if OS_KERNEL_TYPE != OS_MICROKERNEL
		/* Set the "appsStarted" flag here. The applications haven't been started yet, but they're in
		 * a state where we assume that the error hooks can be called if necessary.
		*/
		OS_appsStarted = 1;

		/* !LINKSTO Kernel.API.Hooks.StartupHook.API, 1
		 * !LINKSTO Kernel.API.Hooks.StartupHook.Startup, 1
		 * !LINKSTO Kernel.HookRoutines.PrioISRC2, 1
		 *		Interrupts are disabled here
		*/
		OS_CALLSTARTUPHOOK();


		/* Call all the application-specific startup hooks. This is done
		 * AFTER the global startup hook, according to the Autosar spec.
		 * It is also done after auto-started objects have been started,
		 * so that if the application is terminated due to a protection
		 * violation its tasks get killed.
		 *
		 * !LINKSTO Kernel.Autosar.OsApplication.ApplicationHooks.StartupHook.Order, 1
		*/
		/* Deviation MISRA-1 */
		OS_CALLAPPSTARTUPHOOKS();

		OS_hookApp = OS_NULL;

		/* On non-system-call kernels we set the inKernel flag
		 * to 1 to force a dispatcher call on exit.
		*/
#if (OS_KERNEL_TYPE==OS_FUNCTION_CALL)
		OS_inKernel = 1;
#endif

		/* Initialise the CPU load measurement. This has to be done as close as possible to the
		 * end so that the startup load doesn't adversely affect the measurements.
		*/
		OS_INITMEASURECPULOAD();
#endif
	}

	OS_TRACE_STARTOS_EXIT_P(mode);
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserStartOs</name>
  <synopsis>Start the OS</synopsis>
  <syntax>
    void OS_KernStartOs
    (   os_uint8_t mode    /@ Startup mode @/
    )
  </syntax>
  <description>
    <code>OS_UserStartOs()</code> starts the OS. The <code>mode</code>
    parameter determines the set of tasks and alarms that should be
    started automatically.
    <para>
    After the kernel data structures have been initialised, the
    startup hook is called, if it has been configured.
    </para>
    <para>
    Normally <code>OS_UserStartOs()</code> does not return. If the OS
    has already been started or the <code>mode</code>parameter is not valid
    the function could return, depending on how the error handler is defined
    to handle the error.
    </para>
  </description>
  <availability>
    <code>OS_UserStartOs()</code> can only be called once, from outside
    the OS. It is typically called from the system's <code>main()</code>
    function.
  </availability>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
