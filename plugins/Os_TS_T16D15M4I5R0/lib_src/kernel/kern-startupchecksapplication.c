/* kern-startupchecksapplication.c
 *
 * This file contains the OS_StartupChecksApplication function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startupchecksapplication.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_StartupChecksApplication
*/
os_result_t OS_StartupChecksApplication(void)
{
	os_result_t result = OS_E_OK;
	os_unsigned_t i;
	const os_appcontext_t *app;
	os_permission_t perms = 0;

	for ( i = 0; i < OS_nApps; i++ )
	{
		app = &OS_appTable[i];

		if (  app->appId != i )
		{
			result = OS_PANIC(OS_PANIC_SCHK_AppIdIsNotIndexInAppTable);
		}

		if (  *app->buildId != OS_configurationId )
		{
			result = OS_PANIC(OS_PANIC_SCHK_AppConfigurationIdMismatch);
		}

		if ( app->restartTask != OS_NULLTASK )
		{
			if ( OS_GET_APP(OS_taskTableBase[app->restartTask].app) != app )
			{
				result = OS_PANIC(OS_PANIC_SCHK_AppRestartTaskNotMemberOfApp);
			}
		}

		if ( app->permission == 0 )
		{
			result = OS_PANIC(OS_PANIC_SCHK_AppHasNoPermissions);
		}

		if ( (perms & app->permission) != 0 )
		{
			result = OS_PANIC(OS_PANIC_SCHK_AppPermissionBitNotUnique);
		}

		perms |= app->permission;
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
