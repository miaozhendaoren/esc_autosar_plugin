/* kern-startupcheckstask.c
 *
 * This file contains the OS_StartupChecksTask function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startupcheckstask.c 19142 2014-10-08 12:23:27Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.10 (required)
 *  In the definition of a function-like macro each instance of a parameter shall be enclosed
 *  in parentheses unless it is used as the operand of # or ##
 *
 * Reason:
 *  Parameter is used to designate a struct member. Parentheses can't be used here.
*/
#include <Os_kernel.h>
#include <Os_panic.h>

/* This macro is used for checking the assmbler offsets into the task structures.
 * MISRA note:
 * The 0 cast to a pointer isn't a OS_NULL dereference, since we only take the address and then cast it to
 * unsigned.
*/
/* Deviation MISRA-1 */
#define OS_OFFSET(typ, f)		((os_unsigned_t)&((typ *)0)->f)

#include <memmap/Os_mm_code_begin.h>

/* MISRA-C checkers want prototypes for static helpers */
static os_result_t OS_CheckTaskAccountingConfig(const os_task_t *ts);

/* Checks the accounting / timing configuration
*/
static os_result_t OS_CheckTaskAccountingConfig(const os_task_t *ts)
{
	os_result_t result = OS_E_OK;

	if ( ((ts->flags & OS_TF_MEASUREEXEC) != 0) && (OS_GET_ACCT(ts->accounting) == OS_NULL) )
	{
		result = OS_PANIC(OS_PANIC_SCHK_TaskWithMeasureExecButNoAccountingStructure);
	}

	if ( (OS_GET_TP(ts->execBudget) != 0) ||
		 (OS_GET_ILOCK(ts->osLockTime) != 0) ||
		 (OS_GET_ILOCK(ts->allLockTime) != 0) ||
		 (OS_GET_RLOCK(ts->resourceLockTime) != OS_NULL) )
	{
		if ( OS_GET_ACCT(ts->accounting) == OS_NULL )
		{
			result = OS_PANIC(OS_PANIC_SCHK_TaskWithExecTimeLimitButNoAccountingStructure);
		}

		/* This check is here because there's an implicit assumption in SuspendOSInterrupts that
		 * if any kind of interrupts have been locked there's a time limit already running that is less
		 * than or equal to the OS lock limit, so no new timing is started.
		 *
		 * The check won't fail if both are zero - that's OK.
		 * It *will* fail if osLockTime != 0 and allLockTime == 0 - that's OK.
		*/
		if ( OS_GET_ILOCK(ts->osLockTime) < OS_GET_ILOCK(ts->allLockTime) )
		{
			result = OS_PANIC(OS_PANIC_SCHK_TaskWithOsIntLockTimeLessThanAllIntLockTime);
		}
	}

	return result;
}

/* OS_StartupChecksTask
 *
 * This function performs a variety of consistency and sanity checks on the task configuration
 * data and other tables to try to ensure that the generator is behaving itself as far as we can
 * tell.
*/
os_result_t OS_StartupChecksTask(void)
{
	os_unsigned_t n;
	const os_task_t *ts;
	os_result_t result = OS_E_OK;
	os_result_t tempres;

	for ( n = 0; n < OS_nTasks; n++ )
	{
		ts = OS_TidToTp(n);

		if ( OS_TpToTid(ts) != n )
		{
			result = OS_PANIC(OS_PANIC_SCHK_TaskIdIsNotIndexInTaskTable);
		}

		if ( (OS_GET_APP(ts->app) != OS_NULL) &&
				((OS_GET_APP(ts->app->permission) & OS_GET_APP(ts->permissions)) == 0) )
		{
			result = OS_PANIC(OS_PANIC_SCHK_OwningApplicationHasNoPermission);
		}

		if ( ts->queuePrio > ts->runPrio )
		{
			result = OS_PANIC(OS_PANIC_SCHK_TaskWithRunPrioLowerThanQueuePrio);
		}

#if OS_USE_CLZ_QUEUE_ALGORITHM
		if ( (ts->flags & OS_TF_NONPREEMPT) != 0 )
		{
			if ( ts->runPrio < OS_maxPrio )
			{
				result = OS_PANIC(OS_PANIC_SCHK_NonPremptiveTaskWithLowRunPrio);
			}
		}
#endif

		if ( (ts->flags & OS_TF_EXTENDED) != 0 )
		{
			if ( (OS_configMode & OS_ECC) == 0 )
			{
				result = OS_PANIC(OS_PANIC_SCHK_ExtendedTaskInBasicConformanceClass);
			}

			if ( ts->maxAct != 0 )
			{
				result = OS_PANIC(OS_PANIC_SCHK_ExtendedTaskWithMultipleActivations);
			}
		}

		if ( ((OS_configMode & OS_CC2) == 0) && (ts->maxAct != 0) )
		{
			result = OS_PANIC(OS_PANIC_SCHK_MultipleActivationsInConformanceClass1);
		}

		tempres = OS_CheckTaskAccountingConfig(ts);
		if (tempres != OS_E_OK)
		{
			result = tempres;
	    }		
	}



	if ( (OS_OFFSET(os_task_t, dynamic)        != (os_unsigned_t)OS_T_DYNA)  ||
		 (OS_OFFSET(os_task_t, runPrio)        != (os_unsigned_t)OS_T_RPRIO) ||
		 (OS_OFFSET(os_taskdynamic_t, context) != (os_unsigned_t)OS_T_CTXT)  ||
		 (OS_OFFSET(os_taskdynamic_t, prio)    != (os_unsigned_t)OS_T_PRIO)  ||
		 (OS_OFFSET(os_taskdynamic_t, state)   != (os_unsigned_t)OS_T_STAT)  )
	{
		result = OS_PANIC(OS_PANIC_SCHK_TaskAssemblerOffsetsAreIncorrect);
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
