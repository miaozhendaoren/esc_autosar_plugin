/* kern-findhighesttask.c
 *
 * This file contains the functions OS_FindHighestTask(),...
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-findhighesttask.c 18434 2014-07-10 06:01:07Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if scalability class 2 is used.
 */

#include <Os_kernel.h>
#include <Os_taskqueue.h>
#include <Os_panic.h>

/*!
 * OS_FindHighestTask
 *
 * This function finds the highest-priority active (ready or running) task.
 *
 * First the index of the most significant bit of the master word is found
*/
#include <memmap/Os_mm_code_begin.h>

os_priorityqueueentry_t OS_FindHighestTask(void)
{
	os_int_t index;
	os_priorityqueueentry_t highTask = OS_NULLTASK;
	/* Possible diagnostic TOOLDIAG-1 <2> */
	const os_priorityqueue_t *q;
	const os_priorityqueuedynamic_t *qd;

	if ( OS_GetTopLevelPrioWord() != 0 )
	{
		index = OS_FindSlaveIndex();

		/* Sanity check for master delivering a slave that is unoccupied.
		 * If there's only one level this cannot fail and the compiler should
		 * optimise the code away because the test (OS_slavePrioWord[0]==0)
		 * is already known to be false because we're in this branch
		 * (OS_GetTopLevelPrioWord() is OS_slavePrioWord[0])
		*/
		if ( OS_SLAVEWORDNOTOCCUPIED(index) )
		{
			/* PANIC!!! Slave slot is not occupied */
			/* can't propagate the return value of OS_PANIC -> ignore it */
			(void) OS_PANIC(OS_PANIC_SlaveSlotNotOccupied);
		}
		else
		{
			index = (OS_CLZWORD_NBITS * index) + OS_CLZ(OS_slavePrioWord[index]);

			/* Reverse the order; the priority queue array is in ascending order! */
			index = OS_nPriorities - (index + 1);

			q = &OS_priorityQueue[index];
			qd = OS_Get_PriorityQueueDynamic(q);
			highTask = OS_Get_PriorityQueueHead(index, q, qd);
		}
	}

	return highTask;
}

#include <memmap/Os_mm_code_end.h>

/* Unit test code.
 *
 * For a unit test, compile just this file on host PC with -DOS_UNIT_TEST.
 *
 * Then run the resulting programs from the command line. The final result ("Test passed" or "Test failed")
 * indicates success or failure.
 *
 * See also: asc_Os/test/cunit/Makefile
 *
 * Disable check_c parser
 * CHECK: NOPARSE
 *
*/
#ifdef OS_UNIT_TEST

#include <Os_configuration.h>
#include <stdio.h>

const os_uint32_t OS_configMode = OS_CONFIGMODE;
const os_int_t OS_nPriorities = OS_NPRIORITIES;

/* OS_prioritySlot[]
 *
 * For the tests, each element contains a value equal to its index. This is just so that
 * we can verify that the correct slot has been used.
*/
os_priorityqueueentry_t OS_prioritySlot[] =
{	 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
    32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
    48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63
};

os_clzword_t OS_masterPrioWord;
os_clzword_t OS_slavePrioWord[2];

#if OS_NPRIORITIES <= 32
/* Test configuration for low number of tasks/resource
*/
#define N_TESTS				32

#else
/* Test configuration for high number of tasks + resources
*/
#define N_TESTS				40

#endif

#if (OS_CONFIGMODE & OS_CC2) == 0
/* Test configuration for BCC1/ECC1
*/

#define EXPECTED_VALUE(n)	(OS_NPRIORITIES - 1 - n)

#else
/* Test configuration for BCC2/ECC2
*/
#define EXPECTED_VALUE(n)	expected_values[n]

os_priorityqueueentry_t expected_values[] =
{
#if OS_NPRIORITIES > 32
	45, 44, 43, 42, 41, 40, 39, 38,
#endif
	37, 36,
	35, 34, 33, 32, 31, 30, 29, 28, 27, 26,
	25, 23, 22, 20, 19, 17, 16, 14, 13, 11,
	10,	8,	7,	6,	5,	4,	3,	2,	1,	0
};

#endif

/* CHECK: RULE 306 OFF (easier to read tables in rows/columns) */
const os_priorityqueue_t OS_priorityQueue[40] =
{	{	OS_NULL,						 &OS_prioritySlot[0],  &OS_slavePrioWord[1], 0x01000000, 0x40000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[1],  &OS_slavePrioWord[1], 0x02000000, 0x40000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[2],  &OS_slavePrioWord[1], 0x04000000, 0x40000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[3],  &OS_slavePrioWord[1], 0x08000000, 0x40000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[4],  &OS_slavePrioWord[1], 0x10000000, 0x40000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[5],  &OS_slavePrioWord[1], 0x20000000, 0x40000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[6],  &OS_slavePrioWord[1], 0x40000000, 0x40000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[7],  &OS_slavePrioWord[1], 0x80000000, 0x40000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[8],  &OS_slavePrioWord[0], 0x00000001, 0x80000000, 1 },
	{	&OS_priorityQueueDynamic[0], &OS_prioritySlot[9],  &OS_slavePrioWord[0], 0x00000002, 0x80000000, 2 },
	{	OS_NULL,						 &OS_prioritySlot[11], &OS_slavePrioWord[0], 0x00000004, 0x80000000, 1 },
	{	&OS_priorityQueueDynamic[1], &OS_prioritySlot[12], &OS_slavePrioWord[0], 0x00000008, 0x80000000, 2 },
	{	OS_NULL,						 &OS_prioritySlot[14], &OS_slavePrioWord[0], 0x00000010, 0x80000000, 1 },
	{	&OS_priorityQueueDynamic[2], &OS_prioritySlot[15], &OS_slavePrioWord[0], 0x00000020, 0x80000000, 2 },
	{	OS_NULL,						 &OS_prioritySlot[17], &OS_slavePrioWord[0], 0x00000040, 0x80000000, 1 },
	{	&OS_priorityQueueDynamic[3], &OS_prioritySlot[18], &OS_slavePrioWord[0], 0x00000080, 0x80000000, 2 },
	{	OS_NULL,						 &OS_prioritySlot[20], &OS_slavePrioWord[0], 0x00000100, 0x80000000, 1 },
	{	&OS_priorityQueueDynamic[4], &OS_prioritySlot[21], &OS_slavePrioWord[0], 0x00000200, 0x80000000, 2 },
	{	OS_NULL,						 &OS_prioritySlot[23], &OS_slavePrioWord[0], 0x00000400, 0x80000000, 1 },
	{	&OS_priorityQueueDynamic[5], &OS_prioritySlot[24], &OS_slavePrioWord[0], 0x00000800, 0x80000000, 2 },
	{	OS_NULL,						 &OS_prioritySlot[26], &OS_slavePrioWord[0], 0x00001000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[27], &OS_slavePrioWord[0], 0x00002000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[28], &OS_slavePrioWord[0], 0x00004000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[29], &OS_slavePrioWord[0], 0x00008000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[30], &OS_slavePrioWord[0], 0x00010000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[31], &OS_slavePrioWord[0], 0x00020000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[32], &OS_slavePrioWord[0], 0x00040000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[33], &OS_slavePrioWord[0], 0x00080000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[34], &OS_slavePrioWord[0], 0x00100000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[35], &OS_slavePrioWord[0], 0x00200000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[36], &OS_slavePrioWord[0], 0x00400000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[37], &OS_slavePrioWord[0], 0x00800000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[38], &OS_slavePrioWord[0], 0x01000000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[39], &OS_slavePrioWord[0], 0x02000000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[40], &OS_slavePrioWord[0], 0x04000000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[41], &OS_slavePrioWord[0], 0x08000000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[42], &OS_slavePrioWord[0], 0x10000000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[43], &OS_slavePrioWord[0], 0x20000000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[44], &OS_slavePrioWord[0], 0x40000000, 0x80000000, 1 },
	{	OS_NULL,						 &OS_prioritySlot[45], &OS_slavePrioWord[0], 0x80000000, 0x80000000, 1 }
};
/* CHECK: RULE 306 ON */

os_priorityqueuedynamic_t OS_priorityQueueDynamic[6] =
{	{	1, 0	},	/*  0 */
	{	1, 0	},	/*  1 */
	{	1, 0	},	/*  2 */
	{	1, 0	},	/*  3 */
	{	1, 0	},	/*  4 */
	{	1, 0	},	/*  5 */
};


int main(int argc, char **argv)
{
	int	nfail = 0;
	int i;
	os_priorityqueueentry_t task;
	char *msg;

#if OS_NPRIORITIES > 32
	os_clzword_t *slave = &OS_slavePrioWord[0];

	OS_masterPrioWord = 0xffffffff;
	OS_slavePrioWord[0] = 0xffffffff;
	OS_slavePrioWord[1] = 0xffffffff;
#else
	OS_masterPrioWord = 0x80000000;
	OS_slavePrioWord[0] = 0xffffffff;
#endif


	for ( i = 0; i < N_TESTS; i++ )
	{
		task = OS_FindHighestTask();

		if ( task == EXPECTED_VALUE(i) )
		{
			msg = "OK";
		}
		else
		{
			msg = "FAIL";
			nfail++;
		}

#if OS_NPRIORITIES > 32
		printf("master = 0x%08x, slave = (0x%08x,0x%08x),task = %d  expected = %d (%s)\n", OS_masterPrioWord,
			OS_slavePrioWord[0], OS_slavePrioWord[1], task, EXPECTED_VALUE(i), msg);

		*slave = (*slave >> 1) & 0x7fffffff;
		if ( *slave == 0 )
		{
			slave++;
			OS_masterPrioWord = (OS_masterPrioWord >> 1) & 0x7fffffff;
		}
#else
		printf("slave-0 = 0x%08x, task = %d expected = %d (%s)\n", OS_slavePrioWord[0], task, EXPECTED_VALUE(i), msg);

		OS_slavePrioWord[0] = (OS_slavePrioWord[0] >> 1) & 0x7fffffff;
#endif
	}

	if ( nfail == 0 )
	{
		printf("Test passed\n");
	}
	else
	{
		printf("Test failed\n");
	}

	return (nfail != 0);
}

#endif

/*
 * Re-enable check_c parser
 * CHECK: PARSE
*/

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
