/* kern-dequeuetask.c
 *
 * This file contains the OS_DequeueTask function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-dequeuetask.c 18407 2014-07-08 07:54:12Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: UnusedVariable
 *   Variable is never used.
 *
 * Reason: Not an issue, variable will be used if scalability class 2 is used.
 */

#include <Os_kernel.h>
#include <Os_taskqueue.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_DequeueTask
 *
 * The specified task is removed from the head of its given priority queue the new priority is set.
*/
void OS_DequeueTask(const os_task_t *tp, os_prio_t prio)
{
	const os_priorityqueue_t *q = &OS_priorityQueue[prio];
	/* Possible diagnostic TOOLDIAG-1 <1> */
	os_priorityqueuedynamic_t *qd = OS_Get_PriorityQueueDynamic(q);
	os_priorityqueueentry_t newTask;

	if ( OS_DEVSANITYCHECK(OS_Get_PriorityQueueHead(prio, q, qd) != tp->taskId) )
	{
		/* PANIC!!! Task is not in priority queue */
		/* can't propagate the return value of OS_PANIC -> ignore it */
		(void) OS_PANIC(OS_PANIC_TaskIsNotInPriorityQueue);
	}
	else
	{
		OS_Remove_PriorityQueueHead(prio, q, qd);

		/* If the queue becomes empty, clear its bit(s) in the priority-word(s)
		 * and recalculate OS_taskQueueHead
		*/
		newTask = OS_Get_PriorityQueueHeadAfterRemoval(q, qd);
		if ( newTask == OS_NULLTASK )
		{
			/* No more tasks at this priority, so we need to clear the appropriate bits in the
			 * the master/slave words and if the dequeued task was at the queue head, recalculate
			 * the new queue head from scratch.
			*/
			OS_Clr_PriorityBits(q);

			if ( OS_taskQueueHead == tp )
			{
				newTask = OS_FindHighestTask();

				OS_taskQueueHead = (newTask == OS_NULLTASK) ? OS_NULL : &OS_taskTableBase[newTask];
			}
		}
		else
		{
			/* More tasks at the same priority, so if the dequeued task was at the queue head
			 * we simply set it to the next task in the same priority queue.
			*/
			if ( OS_taskQueueHead == tp )
			{
				OS_taskQueueHead = &OS_taskTableBase[newTask];
			}
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Unit test code.
 *
 * For a unit test, compile just this file on host PC with -DOS_UNIT_TEST.
 *
 * OS_ARCH=OS_LINUX
 * OS_CPU=OS_LINUX32
 * OS_TOOL=OS_gnu
 *
 * Then run the resulting programs from the command line. The final result ("Test passed" or "Test failed")
 * indicates success or failure.
 *
 * Look in asc_Os/test/cunit for details
 *
 * Disable check_c parser
 * CHECK: NOPARSE
 *
*/
#ifdef OS_UNIT_TEST

#include <stdio.h>
#include <Os_configuration.h>

const os_uint32_t OS_configMode = OS_CONFIGMODE;
const os_int_t OS_nPriorities = OS_NPRIORITIES;


#define NPRIOSLOTS	64
os_priorityqueueentry_t OS_prioritySlot[NPRIOSLOTS];

os_clzword_t OS_masterPrioWord;
os_clzword_t OS_slavePrioWord[2];
const os_task_t *OS_taskQueueHead;

#if OS_NPRIORITIES <= OS_CLZWORD_NBITS

const os_priorityqueue_t OS_priorityQueue[32] =
{	{	OS_NULL, OS_NULL, OS_NULL, 0x00000001, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000002, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000004, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000008, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000010, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000020, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000040, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000080, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000100, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000200, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000400, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000800, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00001000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00002000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00004000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00008000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00010000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00020000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00040000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00080000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00100000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00200000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00400000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00800000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x01000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x02000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x04000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x08000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x10000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x20000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x40000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x80000000, 0, 1 },
};
#else

const os_priorityqueue_t OS_priorityQueue[OS_NPRIORITIES] =
/*      dynamic                       queue                 slave                 slaveBit    masterBit   nEntries */
{	{	OS_NULL,						  &OS_prioritySlot[0],  &OS_slavePrioWord[1], 0x80000000, 0x80000000, 1 },
	{	&OS_priorityQueueDynamic[0],  &OS_prioritySlot[1],  &OS_slavePrioWord[1], 0x40000000, 0x80000000, 2 },
	{	&OS_priorityQueueDynamic[1],  &OS_prioritySlot[3],  &OS_slavePrioWord[0], 0x80000000, 0x40000000, 3 },
	{	&OS_priorityQueueDynamic[2],  &OS_prioritySlot[6],  &OS_slavePrioWord[0], 0x40000000, 0x40000000, 2 },
	{	OS_NULL,						  &OS_prioritySlot[8],  &OS_slavePrioWord[0], 0x20000000, 0x40000000, 1 },
};

os_priorityqueuedynamic_t OS_priorityQueueDynamic[3];

/* Test specific data
*/
#define NUM_TEST_PRIO	5					/* No of priorities to test */
os_int_t num_act[] = { 1, 2, 3, 2, 1 };		/* No of activations at each priority */

os_clzword_t expected_toplevel[9] =
{ 0xffffffff, 0xffffffff, 0x7fffffff, 0x7fffffff, 0x7fffffff, 0x7fffffff, 0x7fffffff, 0x7fffffff, 0x3fffffff };

os_clzword_t expected_slave0[9] =
{ 0xe0000000, 0xe0000000, 0xe0000000, 0xe0000000, 0xe0000000, 0x60000000, 0x60000000, 0x20000000, 0x00000000 };

os_clzword_t expected_slave1[9] =
{ 0x40000000, 0x40000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000 };


#endif

os_taskdynamic_t task1d;
os_taskdynamic_t task2d;

const os_task_t OS_taskTable[2] =
{	{	&task1d,	/* dynamic */
		OS_NULL,	/* accounting */
		OS_NULL,	/* app */
		OS_NULL,	/* entry */
		OS_NULL,	/* stackBase */
		OS_NULL,	/* dataStart */
		OS_NULL,	/* dataEnd */
		OS_NULL,	/* idataStart */
		OS_NULL,	/* idataEnd */
		0,			/* execBudget */
		0,			/* osLockTime */
		0,			/* allLockTime */
		OS_NULL,	/* resourceLockTime */
		0,			/* permissions */
		0,			/* linkBase */
		0,			/* stackLen */
		0,			/* rateMonitor */
		0,			/* queuePrio */
		0,			/* runPrio */
		0,			/* maxAct */
		0,			/* flags */
		0			/* taskId */
	},
	{	&task2d,	/* dynamic */
		OS_NULL,	/* accounting */
		OS_NULL,	/* app */
		OS_NULL,	/* entry */
		OS_NULL,	/* stackBase */
		OS_NULL,	/* dataStart */
		OS_NULL,	/* dataEnd */
		OS_NULL,	/* idataStart */
		OS_NULL,	/* idataEnd */
		0,			/* execBudget */
		0,			/* osLockTime */
		0,			/* allLockTime */
		OS_NULL,	/* resourceLockTime */
		0,			/* permissions */
		0,			/* linkBase */
		0,			/* stackLen */
		0,			/* rateMonitor */
		0,			/* queuePrio */
		0,			/* runPrio */
		0,			/* maxAct */
		0,			/* flags */
		1			/* taskId */
	}
};

/* These variables are global so that the stub OS_FindHighestTask() function can determine
 * the test position
*/
os_prio_t prio;
os_int_t act;
int nfail = 0;

int main(int argc, char **argv)
{
	int fail;
	int i;
	int slot, filled_slots = 0;
	os_uint32_t expected_toplevel_inverted = 0;
	int tnum = 0;

#if OS_NPRIORITIES <= OS_CLZWORD_NBITS
	/* Simple test case: 1 task per priority
	*/

	/* Fill all priority slots with the same task
	*/
	for ( i = 0; i < NPRIOSLOTS; i++ )
	{
		OS_prioritySlot[i] = 0;
	}

	OS_slavePrioWord[0] = 0xffffffff;
	OS_taskQueueHead = (const os_task_t *)0xdeadbeef;

	for ( prio = 0; prio < OS_NPRIORITIES; prio++ )
	{
		OS_DequeueTask(&OS_taskTable[0], prio);

		fail = 0;

		if ( prio == 0 )
		{
			/* Prio 0: not overwritten because it is not the task we're dequeueing
			 * OS_FindHighestTask() shouldn't get called. If it does, OS_taskQueueHead
			 * gets overwritten with task2.
			*/
			if ( OS_taskQueueHead != (const os_task_t *)0xdeadbeef )
			{
				printf("FAIL: OS_taskQueueHead overwritten\n");
				fail = 1;
			}
		}
		else
		if ( prio == 1 )
		{
			/* Prio 1: should be overwritten with task2
			*/
			if ( OS_taskQueueHead != &OS_taskTable[1] )
			{
				printf("FAIL: OS_taskQueueHead not overwritten\n");
				fail = 1;
			}
		}
		else
		{
			/* Prio 2-31: should be overwritten with OS_NULL
			*/
			if ( OS_taskQueueHead != OS_NULL )
			{
				printf("FAIL: OS_taskQueueHead not overwritten\n");
				fail = 1;
			}
		}

		OS_taskQueueHead = &OS_taskTable[0];

		for ( i = 0; i < NPRIOSLOTS; i++ )
		{
			if ( i == prio )
			{
				if ( OS_prioritySlot[i] != OS_NULLTASK )
				{
					printf("FAIL: task still found in slot %d\n", i);
					fail = 1;
				}
			}
			else
			{
				if ( OS_prioritySlot[i] != OS_taskTable[0].taskId )
				{
					printf("FAIL: Found a task in slot %d\n", i);
					fail = 1;
				}
			}

			OS_prioritySlot[i] = OS_taskTable[0].taskId;
		}

		expected_toplevel_inverted = expected_toplevel_inverted | (1 << prio);

		if ( OS_slavePrioWord[0] != ~expected_toplevel_inverted )
		{
			fail = 1;
		}

		printf("Prio %d: master 0x%08x (%s)\n", prio, OS_masterPrioWord, fail ? "FAIL" : "OK" );

		if ( fail )
		{
			nfail++;
		}
	}

#else
	/* More complicated test case: some priorities can have 2 tasks
	 * We can also use a specially-constructed set of priority queues that only has valid data in the first
	 * 5 entries. The data is not meaningful in the sense of a real OS but is constructed for testing.
	*/
	/* Fill all priority slots with the same task
	*/
	for ( i = 0; i < NPRIOSLOTS; i++ )
	{
		OS_prioritySlot[i] = 1;
	}

	OS_masterPrioWord = 0xffffffff;
	OS_slavePrioWord[0] = 0xe0000000;
	OS_slavePrioWord[1] = 0xc0000000;
	OS_taskQueueHead = (const os_task_t *)0xdeadbeef;

	for ( prio = 0; prio < NUM_TEST_PRIO; prio++ )
	{
		for ( act = 0; act < num_act[prio]; act++ )
		{
			OS_DequeueTask(&OS_taskTable[0], prio);

			fail = 0;

			if ( prio == 0 )
			{
				/* Prio 0: not overwritten because it is not the task we're dequeueing
				 * OS_FindHighestTask() shouldn't get called. If it does, OS_taskQueueHead
				 * gets overwritten with task2.
				*/
				if ( OS_taskQueueHead != (const os_task_t *)0xdeadbeef )
				{
					printf("FAIL: OS_taskQueueHead overwritten\n");
					fail = 1;
				}
			}
			else
			if ( prio < 4 )
			{
				/* Prio 1-3: should be overwritten with task2
				*/
				if ( OS_taskQueueHead != &OS_taskTable[1] )
				{
					printf("FAIL: OS_taskQueueHead not overwritten\n");
					fail = 1;
				}
			}
			else
			{
				/* Prio 4: should be overwritten with OS_NULL
				*/
				if ( OS_taskQueueHead != OS_NULL )
				{
					printf("FAIL: OS_taskQueueHead not overwritten\n");
					fail = 1;
				}
			}

			OS_taskQueueHead = &OS_taskTable[0];

			filled_slots++;
			slot = -1;
			for ( i = 0; i < NPRIOSLOTS; i++ )
			{
				if ( i < filled_slots )
				{
					if ( OS_prioritySlot[i] == OS_NULLTASK )
					{
						slot = i;
					}
					else
					{
						printf("FAIL: Found a task in slot %d\n", i);
						fail = 1;
					}
				}
				else
				{
					if ( OS_prioritySlot[i] != OS_taskTable[1].taskId )
					{
						printf("FAIL: task not found in slot %d\n", i);
						fail = 1;
					}
				}
			}

			if ( (OS_masterPrioWord != expected_toplevel[tnum]) ||
				 (OS_slavePrioWord[0] != expected_slave0[tnum]) ||
				 (OS_slavePrioWord[1] != expected_slave1[tnum]) )
			{
				printf("Expected prio. words (tnum=%d): master 0x%08x slave %08x %08x\n", tnum,
							expected_toplevel[tnum], expected_slave0[tnum], expected_slave1[tnum]);
				fail = 1;
			}

#if 0 /* debugging */
			printf("%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",
					OS_prioritySlot[0], OS_prioritySlot[1], OS_prioritySlot[2], OS_prioritySlot[3],
					OS_prioritySlot[4], OS_prioritySlot[5], OS_prioritySlot[6], OS_prioritySlot[7],
					OS_prioritySlot[8], OS_prioritySlot[9]);
#endif

			printf("Prio %d, act %d: last slot %d master 0x%08x slave %08x %08x (%s)\n", prio, act, slot,
						OS_masterPrioWord, OS_slavePrioWord[0], OS_slavePrioWord[1],
						fail ? "FAIL" : "OK" );

			if ( fail )
			{
				nfail++;
			}

			tnum++;
		}
	}

#endif

	if ( nfail == 0 )
	{
		printf("Test passed\n");
	}
	else
	{
		printf("Test failed\n");
	}

	return (nfail != 0);
}

/* Stub function to test functionality of OS_DequeueTask()
*/
os_priorityqueueentry_t OS_FindHighestTask(void)
{
	os_priorityqueueentry_t result = OS_NULLTASK;

#if 0	/* debugging */
	printf("OS_FindHighestTask() called --- ");
#endif

	if ( OS_taskQueueHead != &OS_taskTable[0] )
	{
		printf("ERROR: OS_FindHighestTask() called when dequeued task is not at queue head!\n");
		nfail++;
	}

#if OS_NPRIORITIES <= OS_CLZWORD_NBITS
	if ( prio <= 1 )
	{
		result = 1;
	}
#else
	if ( prio < 4 )
	{
		result = 1;
	}
#endif

	return result;
}

#endif

/*
 * Re-enable check_c parser
 * CHECK: PARSE
*/

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
