/* kern-returnfromcall.c
 *
 * This file contains the OS_KernReturnFromCall function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-returnfromcall.c 17718 2014-02-17 15:31:32Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/*
 * OS_KernReturnFromCall
 *
 * This function is the kernel function that is called in response to the ReturnFromCall system call.
 * If it is called in the context of a Category-2 ISR or an application hook function, it attempts
 * to kill the ISR or hook function. If that fails, the system gets shut down.
 *
 * If the system call is invoked from any other calling context it silently returns.
*/
void OS_KernReturnFromCall(void)
{
	os_isrdynamic_t *isrDynamic;

	if ( OS_inFunction == OS_INCAT2 )
	{
		isrDynamic = &OS_isrDynamicBase[OS_isrCurrent];
		if ( OS_KillCalledContext(&isrDynamic->c) != OS_E_OK)
		{
			/* If the ISR cannot be killed for some reason we
			 * must shut down
			*/
			/* can't propagate the return value of OS_PANIC -> ignore it */
			(void) OS_PANIC(OS_PANIC_IsrCannotBeKilled);
		}
	}
	else
	if ( OS_inFunction == OS_INSTARTUPHOOK )
	{
		if ( !OS_AppIsNull(OS_hookApp) )
		{
			if ( OS_KillCalledContext(&OS_sHookContext.c) != OS_E_OK)
			{
				/* If the hook cannot be killed for some reason we
				 * must shut down
				*/
				/* can't propagate the return value of OS_PANIC -> ignore it */
				(void) OS_PANIC(OS_PANIC_HookCannotBeKilled);
			}
		}
	}
	else
	if ( OS_inFunction == OS_INSHUTDOWNHOOK )
	{
		if ( !OS_AppIsNull(OS_hookApp) )
		{
			if ( OS_KillCalledContext(&OS_sHookContext.c) != OS_E_OK)
			{
				/* If the hook cannot be killed for some reason we
				 * must shut down without calling any further hooks.
				*/
				OS_ShutdownNoHooks();
			}
		}
	}
	else
	if ( OS_inFunction == OS_INERRORHOOK )
	{
		if ( !OS_AppIsNull(OS_hookApp) )
		{
			if ( OS_KillCalledContext(&OS_eHookContext.c) != OS_E_OK)
			{
				/* If the hook cannot be killed for some reason we
				 * must shut down
				*/
				/* can't propagate the return value of OS_PANIC -> ignore it */
				(void) OS_PANIC(OS_PANIC_HookCannotBeKilled);
			}
		}
	}
	else
	{
		/* MISRA-C */
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
