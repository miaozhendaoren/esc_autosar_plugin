/* kern-enqueuetask.c
 *
 * This file contains the OS_EnqueueTask function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-enqueuetask.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_taskqueue.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_EnqueueTask
 *
 * The specified task is added to the end of the priority fifo of the given priority.
 *
 * !LINKSTO Kernel.Architecture.ConformanceClasses.NonSuspendedTasksBCC, 1
 * !LINKSTO Kernel.Architecture.ConformanceClasses.NonSuspendedTasksECC, 1
 *		The generator creates queues of appropriate dimensions such that all tasks can be active simultaneously
 *
 * !LINKSTO Kernel.Architecture.ConformanceClasses.TasksPerPrio, 1
 *		The number of tasks per priority is determined by the generator.
 *
 * !LINKSTO Kernel.TaskManagement.Scheduling.PrioQueue, 1
 * !LINKSTO Kernel.TaskManagement.Scheduling, 1
 * !LINKSTO Kernel.TaskManagement, 1
 *		When there are multiple tasks per priority, the priority queue is a FIFO ring buffer.
*/
void OS_EnqueueTask(const os_task_t *tp, os_prio_t prio)
{
	const os_priorityqueue_t *q = &OS_priorityQueue[prio];

	if ( OS_DEVSANITYCHECK(OS_PriorityQueueIsFull(prio, q)) )
	{
		/* PANIC!!! Priority queue is already full */
		/* can't propagate the return value of OS_PANIC -> ignore it */
		(void) OS_PANIC(OS_PANIC_PriorityQueueAlreadyFull);
	}

	/* Add the task to the back of the priority queue
	*/
	OS_Append_PriorityQueue(prio, q, OS_Get_PriorityQueueDynamic(q), tp->taskId);

	/* Set the queue's bit in the master word
	*/
	OS_Set_PriorityBits(q);

	if ( (OS_taskQueueHead == OS_NULL) || (prio > OS_taskQueueHead->dynamic->prio) )
	{
		OS_taskQueueHead = tp;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Unit test code.
 *
 * For a unit test, compile just this file on host PC with -DOS_UNIT_TEST.
 *
 * OS_ARCH=OS_LINUX
 * OS_CPU=OS_LINUX32
 * OS_TOOL=OS_gnu
 *
 * Then run the resulting programs from the command line. The final result ("Test passed" or "Test failed")
 * indicates success or failure.
 *
 * Look in asc_Os/test/cunit for details
 *
 * Disable check_c parser
 * CHECK: NOPARSE
 *
*/
#ifdef OS_UNIT_TEST

#include <stdio.h>
#include <Os_configuration.h>

const os_uint32_t OS_configMode = OS_CONFIGMODE;
const os_int_t OS_nPriorities = OS_NPRIORITIES;


#define NPRIOSLOTS	64
os_priorityqueueentry_t OS_prioritySlot[NPRIOSLOTS];

os_clzword_t OS_masterPrioWord;
os_clzword_t OS_slavePrioWord[2];
const os_task_t *OS_taskQueueHead;

#if OS_NPRIORITIES <= OS_CLZWORD_NBITS

const os_priorityqueue_t OS_priorityQueue[32] =
{	{	OS_NULL, OS_NULL, OS_NULL, 0x00000001, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000002, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000004, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000008, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000010, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000020, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000040, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000080, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000100, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000200, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000400, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00000800, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00001000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00002000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00004000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00008000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00010000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00020000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00040000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00080000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00100000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00200000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00400000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x00800000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x01000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x02000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x04000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x08000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x10000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x20000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x40000000, 0, 1 },
	{	OS_NULL, OS_NULL, OS_NULL, 0x80000000, 0, 1 },
};
#else

const os_priorityqueue_t OS_priorityQueue[OS_NPRIORITIES] =
{	{	&OS_priorityQueueDynamic[0],  &OS_prioritySlot[0],  &OS_slavePrioWord[1], 0x01000000, 0x40000000, 1 },
	{	&OS_priorityQueueDynamic[1],  &OS_prioritySlot[1],  &OS_slavePrioWord[1], 0x02000000, 0x80000000, 2 },
	{	&OS_priorityQueueDynamic[2],  &OS_prioritySlot[3],  &OS_slavePrioWord[1], 0x04000000, 0x04000000, 3 },
	{	&OS_priorityQueueDynamic[3],  &OS_prioritySlot[6],  &OS_slavePrioWord[0], 0x08000000, 0x00400000, 2 },
	{	&OS_priorityQueueDynamic[4],  &OS_prioritySlot[8],  &OS_slavePrioWord[0], 0x10000000, 0x00040000, 1 },
};

os_priorityqueuedynamic_t OS_priorityQueueDynamic[5];

/* Test spapcific data
*/
#define NUM_TEST_PRIO	5					/* No of priorities to test */
os_int_t num_act[] = { 1, 2, 3, 2, 1 };		/* No of activations at each priority */

os_clzword_t expected_master[NUM_TEST_PRIO] =
{ 0x40000000, 0xc0000000, 0xc4000000, 0xc4400000, 0xc4440000 };

os_clzword_t expected_slave0[NUM_TEST_PRIO] =
{ 0x00000000, 0x00000000, 0x00000000, 0x08000000, 0x18000000 };

os_clzword_t expected_slave1[NUM_TEST_PRIO] =
{ 0x01000000, 0x03000000, 0x07000000, 0x07000000, 0x07000000 };

#endif

os_taskdynamic_t task1d;

const os_task_t task1 =
{	&task1d,	/* dynamic */
	OS_NULL,		/* accounting */
	OS_NULL,		/* app */
	OS_NULL,		/* entry */
	OS_NULL,		/* stackBase */
	OS_NULL,		/* dataStart */
    OS_NULL,		/* dataEnd */
    OS_NULL,		/* idataStart */
    OS_NULL,		/* idataEnd */
    0,			/* execBudget */
    0,			/* osLockTime */
    0,			/* allLockTime */
    OS_NULL,		/* resourceLockTime */
    0,			/* permissions */
    0,			/* linkBase */
    0,			/* stackLen */
    0,			/* rateMonitor */
    0,			/* queuePrio */
    0,			/* runPrio */
    0,			/* maxAct */
    0,			/* flags */
    42,			/* taskId */
};

os_taskdynamic_t task2d;

const os_task_t task2 =
{	&task2d,	/* dynamic */
	OS_NULL,		/* accounting */
	OS_NULL,		/* app */
	OS_NULL,		/* entry */
	OS_NULL,		/* stackBase */
	OS_NULL,		/* dataStart */
    OS_NULL,		/* dataEnd */
    OS_NULL,		/* idataStart */
    OS_NULL,		/* idataEnd */
    0,			/* execBudget */
    0,			/* osLockTime */
    0,			/* allLockTime */
    OS_NULL,		/* resourceLockTime */
    0,			/* permissions */
    0,			/* linkBase */
    0,			/* stackLen */
    0,			/* rateMonitor */
    0,			/* queuePrio */
    0,			/* runPrio */
    0,			/* maxAct */
    0,			/* flags */
    66,			/* taskId */
};

int main(int argc, char **argv)
{
	int fail, nfail = 0;
	os_prio_t prio;
	os_int_t act;
	int i;
	int slot;
	int filled_slots;

	for ( i = 0; i < NPRIOSLOTS; i++ )
	{
		OS_prioritySlot[i] = OS_NULLTASK;
	}

#if OS_NPRIORITIES <= OS_CLZWORD_NBITS
	/* Simple test case: 1 task per priority
	*/
	for ( prio = 0; prio < OS_NPRIORITIES; prio++ )
	{
		OS_taskQueueHead = &task2;
		OS_EnqueueTask(&task1, prio);

		fail = 0;
		slot = -1;
		for ( i = 0; i < NPRIOSLOTS; i++ )
		{
			if ( i == prio )
			{
				if ( OS_prioritySlot[i] == task1.taskId )
				{
					slot = i;
				}
				else
				{
					printf("FAIL: task not found in slot %d\n", i);
					fail = 1;
				}
			}
			else
			{
				if ( OS_prioritySlot[i] != OS_NULLTASK )
				{
					printf("FAIL: Found a task in slot %d\n", i);
					fail = 1;
				}
			}

			OS_prioritySlot[i] = OS_NULLTASK;
		}

		if ( OS_slavePrioWord[0] != (1 << prio) )
		{
			fail = 1;
		}

		printf("Prio %d: master 0x%08x, slave %08x %08x (%s)\n", prio, OS_masterPrioWord, OS_slavePrioWord[0],
				OS_slavePrioWord[1], fail ? "FAIL" : "OK" );

		OS_slavePrioWord[0] = 0;

		if ( fail )
		{
			nfail++;
		}
	}

#else
	/* More complicated test case: some priorities can have 2 tasks
	 * We can cheat here. The enqueue function does not test for multiple activations; it simply
	 * adds the task to the end of the priority queue. So we can enqueue the task as often as we like
	 * as long as we don't exceed the limit for any individual queue.
	 * We can also use a specially-constructed set of priority queues that only has valid data in the first
	 * 5 entries. The data is not meaningful in the sense of a real OS but is constructed for testing.
	*/
	OS_taskQueueHead = OS_NULL;
	task2d.prio = 1;
	filled_slots = 0;
	for ( prio = 0; prio < NUM_TEST_PRIO; prio++ )
	{
		for ( act = 0; act < num_act[prio]; act++ )
		{
			fail = 0;

			OS_EnqueueTask(&task1, prio);

			if ( prio == 1 )
			{
				if ( OS_taskQueueHead != &task2 )
				{
					printf("FAIL: OS_taskQueueHead overwritten\n");
					fail = 1;
				}
			}
			else
			{
				/* prio 0: overwritten because it's OS_NULL
				 * prio >=2: overwritten because task1 is higher priority
				*/
				if ( OS_taskQueueHead != &task1 )
				{
					printf("FAIL: OS_taskQueueHead not overwritten\n");
					fail = 1;
				}
			}

			OS_taskQueueHead = &task2;

			filled_slots++;
			slot = -1;
			for ( i = 0; i < NPRIOSLOTS; i++ )
			{
				if ( i < filled_slots )
				{
					if ( OS_prioritySlot[i] == task1.taskId )
					{
						slot = i;
					}
					else
					{
						printf("FAIL: task not found in slot %d\n", i);
						fail = 1;
					}
				}
				else
				{
					if ( OS_prioritySlot[i] != OS_NULLTASK )
					{
						printf("FAIL: Found a task in slot %d\n", i);
						fail = 1;
					}
				}
			}

			if ( (OS_masterPrioWord != expected_master[prio]) ||
				 (OS_slavePrioWord[0] != expected_slave0[prio]) ||
				 (OS_slavePrioWord[1] != expected_slave1[prio]) )
			{
				printf("Expected prio. words: master 0x%08x slave %08x %08x\n",
							expected_master[prio], expected_slave0[prio], expected_slave1[prio]);
				fail = 1;
			}

			printf("Prio %d, act %d: last slot %d master 0x%08x slave %08x %08x (%s)\n", prio, act, slot,
						OS_masterPrioWord, OS_slavePrioWord[0], OS_slavePrioWord[1],
						fail ? "FAIL" : "OK" );

			if ( fail )
			{
				nfail++;
			}
		}
	}

#endif

	if ( nfail == 0 )
	{
		printf("Test passed\n");
	}
	else
	{
		printf("Test failed\n");
	}

	return (nfail != 0);
}

#endif

/*
 * Re-enable check_c parser
 * CHECK: PARSE
*/

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
