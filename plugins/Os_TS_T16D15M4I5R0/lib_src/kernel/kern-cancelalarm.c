/* kern-cancelalarm.c
 *
 * This file contains the OS_KernCancelAlarm function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-cancelalarm.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#define OS_SID OS_SID_CancelAlarm
#define OS_SIF OS_svc_CancelAlarm

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernCancelAlarm
 *
 * The alarm is cancelled, provided it is currently in use.
 *
 * !LINKSTO Kernel.API.Alarms.CancelAlarm.API, 1
*/
os_result_t OS_KernCancelAlarm(os_alarmid_t a)
{
	os_result_t r = OS_E_OK;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_alarm_t *as;
	os_alarmdynamic_t *ad;
	os_intstatus_t is;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)a);

	OS_TRACE_CANCELALARM_ENTRY(a);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( ! OS_IsValidAlarmId( a ) )
	{
		/* !LINKSTO Kernel.API.Alarms.CancelAlarm.InvalidAlarm, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidAlarmId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		as = &OS_alarmTableBase[a];

		app = OS_CurrentApp();

		if ( !OS_HasPermission(app, as->permissions) )
		{
			r = OS_ERROR(OS_ERROR_Permission, OS_GET_PARAMETER_VAR());
		}
		else
		{
			ad = &OS_alarmDynamicBase[a];

			is = OS_IntDisable();

			if ( ad->inUse == OS_ALARM_INUSE )
			{
				/* !LINKSTO Kernel.API.Alarms.CancelAlarm.CancelAlarm, 1
				*/
				r = OS_KillAlarm(a, OS_ALARM_IDLE);
			}
			else
			{
				/* !LINKSTO Kernel.API.Alarms.CancelAlarm.UnusedAlarm, 1
				*/
				r = OS_ERROR(OS_ERROR_AlarmNotInUse, OS_GET_PARAMETER_VAR());
			}

			OS_IntRestore(is);
		}
	}

	OS_TRACE_CANCELALARM_EXIT_P(r,a);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserCancelAlarm</name>
  <synopsis>Cancel an alarm</synopsis>
  <syntax>
    os_result_t OS_UserCancelAlarm
    (   os_alarmid_t a    /@ ID of the alarm @/
    )
  </syntax>
  <description>
    <code>OS_UserCancelAlarm()</code> resets the expiration time of
    the specified alarm.
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
