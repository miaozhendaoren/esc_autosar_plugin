/* kern-startupchecksalarm.c
 *
 * This file contains the OS_StartupChecksAlarm function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startupchecksalarm.c 18407 2014-07-08 07:54:12Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#include <Os_kernel.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_StartupChecksAlarm
*/
os_result_t OS_StartupChecksAlarm(void)
{
	os_result_t result = OS_E_OK;
	os_unsigned_t i;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_alarm_t *alarm;

	for ( i = 0; i < OS_nAlarms; i++ )
	{
		alarm = &OS_alarm[i];

		if ( (OS_GET_APP(alarm->app) != OS_NULL) &&
				((OS_GET_APP(alarm->app->permission) & OS_GET_APP(alarm->permissions)) == 0) )
		{
			result = OS_PANIC(OS_PANIC_SCHK_OwningApplicationHasNoPermission);
		}
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
