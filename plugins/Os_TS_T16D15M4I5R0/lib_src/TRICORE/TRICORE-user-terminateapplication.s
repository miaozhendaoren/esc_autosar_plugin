/* TRICORE-user-terminateapplication.s
 *
 * This file contains the system function OS_UserTerminateApplication()
 * Generated by the makesc.pl script
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_tool.h>
#include <Os_syscalls.h>

	_TEXT
	_GLOBAL	OS_UserTerminateApplication
	_GTYPE(OS_UserTerminateApplication,@function)
	_GLOBAL	__callee.OS_UserTerminateApplication.da.i.i
	_GTYPE(__callee.OS_UserTerminateApplication.da.i.i,@function)
	_GLOBAL	TerminateApplication
	_GTYPE(TerminateApplication,@function)
	_GLOBAL	__callee.TerminateApplication.da.i.i
	_GTYPE(__callee.TerminateApplication.da.i.i,@function)

/*!
 *	OS_UserTerminateApplication()
 *
 *	This routine makes the system call to OS_SC_TerminateApplication
*/

_TTYPE(OS_UserTerminateApplication,FUNC)
_TTYPE(__callee.OS_UserTerminateApplication.da.i.i,FUNC)
_TTYPE(TerminateApplication,FUNC)
_TTYPE(__callee.TerminateApplication.da.i.i,FUNC)

	_entry
	syscall	_IMM(#,OS_SC_TerminateApplication)
	ret

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
