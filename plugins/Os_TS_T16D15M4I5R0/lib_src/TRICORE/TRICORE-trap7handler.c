/* TRICORE-trap7handler.c
 *
 * This file contains the OS_Trap7Handler function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-trap7handler.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#define OS_SID OS_SID_ArchTrapHandler
#define OS_SIF OS_svc_ArchTrapHandler

#include <Os_kernel.h>
#include <TRICORE/Os_TRICORE_cpu.h>

/* Note: The tc1.6 architecture doesn't differ from the tc1.3.1 architecture
 * regarding the trap 7 handler. Hence also the TC1798, which is tc1.6
 * must use TRICORE-trap7handler-tc131.c.
 */
#if (OS_CPU == OS_TC1797) \
    || (OS_CPU == OS_TC1798) \
	|| (OS_CPU == OS_TC1791) \
	|| (OS_CPU == OS_TC1793) \
	|| (OS_CPU == OS_TC277)
#error "This version of the trap 7 handler doesn't support TC1791/TC1793/TC1797/TC1798/TC277. Check your makefiles!"
#error "Use TRICORE-trap7handler-tc131.c instead."
#endif

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_Trap7Handler()
 *
 * This function handles TRICORE class 7 traps.
 *
 * Class 7 traps are not really exception traps at all. They are
 * caused by one of the following:
 *  - NMI
 *  - Watchdog timeout
 *  - Loss of lock in the PLL
*/
void OS_Trap7Handler(void)
{
	os_uint32_t nmisr;

#if (OS_CPU == OS_TC27XFPGA) || (OS_CPU == OS_TC2D5)
	nmisr = 1;
#else
	nmisr = OS_pwr.pwr_nmisr;
#endif

	/* Some Tricore variants don't appear to set the NMI_EXT bit for
	 * external NMI.
	*/
	if ( (nmisr == 0) || ((nmisr & OS_NMI_EXT) != 0) )
	{
		OS_NmiHandler();
	}

	if ( (nmisr & OS_NMI_PLL) != 0 )
	{
		/* can't propagate the return value of OS_ERROR -> ignore it */
		(void) OS_ERROR(OS_ERROR_PllLockLost, OS_NULL);
	}

	if ( (nmisr & OS_NMI_WDT) != 0 )
	{
		/* can't propagate the return value of OS_ERROR -> ignore it */
		(void) OS_ERROR(OS_ERROR_WatchdogTimeout, OS_NULL);
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
