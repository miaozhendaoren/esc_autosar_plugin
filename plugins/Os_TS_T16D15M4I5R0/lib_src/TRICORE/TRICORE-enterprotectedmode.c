/* TRICORE-enterprotectedmode.c
 *
 * This file contains the OS_EnterProtectedMode function for TRICORE.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-enterprotectedmode.c 17643 2014-02-06 13:53:17Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_EnterProtectedMode
 *
 * This function switches the CPU from "normal" mode in which it starts
 * to "proteced" mode. To do this, the PROTEN bit of SYSCON is set to 1.
 *
 * Before that can happen, however, the appropriate IO mode (SU) and
 * and protection register set (0) must be selected in PSW. Then the
 * registers of protection register set 0 are initialised to give read,
 * write and execute access.
 *
 * For OS_TRICOREARCH_16EP a different MPU handling is required. Instead of fixed mappings from range
 * registers to sets, the range registers are flexible maped into a set. This mean that we
 * fix the values of most data/code ranges. The current used mapping is the following:
 *
 * CPR0: 0x8000000 to 0xffffffff [SV] Fix
 * CPR1: entire .text section (execute) [User] Fix
 *
 * DPR0: application data (read/write) [User,SV]
 * DPR1: task/isr private data (read/write) [User,SV]
 * DPR2: stack (read/write) [User,SV]
 * DPR3: global data (read-only) [User,SV] Fix
 * DPR4: whole memory (read/write) [SV] Fix
 *
 * Furthermore 2 protection sets are used:
 * Set 0 for supervisor
 * Set 1 for user modes (Tasks, etc.)
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
 */
void OS_EnterProtectedMode(void)
{
	os_uint32_t pswVal;

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)

	/* First setup the whole memory. */
	OS_MTCR(OS_CPR0_L, OS_kernCpr[0]);
	OS_MTCR(OS_CPR0_U, OS_kernCpr[1]);

	/* Then setup the whole .text as executable */
	OS_MTCR(OS_CPR1_L, OS_globalCpr[0]);
	OS_MTCR(OS_CPR1_U, OS_globalCpr[1]);

	/* Now the data ranges */
	OS_MTCR(OS_DPR4_L, OS_kernDpr[0]);
	OS_MTCR(OS_DPR4_U, OS_kernDpr[1]);

	OS_MTCR(OS_DPR3_L, OS_globalDpr1[0]);
	OS_MTCR(OS_DPR3_U, OS_globalDpr1[1]);

	/* Setup set 0 for supervisor mode */

	OS_MTCR(OS_DPRE_0,0x18);
	OS_MTCR(OS_DPWE_0,0x18);
	OS_MTCR(OS_CPXE_0,0x01);

	/* Setup set 1 for user mode */

	OS_MTCR(OS_DPRE_1,0x0f);
	OS_MTCR(OS_DPWE_1,0x07);
	OS_MTCR(OS_CPXE_1,0x02);

#else
	OS_SetPr0(OS_kernCpm, OS_kernCpr, OS_kernDpm, OS_kernDpr);
#endif

	/* Select PRS 0, supervisor mode, enable write to global address
	 * registers, disable call depth counting.
	*/
	pswVal = OS_MFCR(OS_PSW);
	pswVal &= (os_uint32_t) ~((os_uint32_t) OS_PSW_PRS | OS_PSW_IO | OS_PSW_GW | OS_PSW_CDE | OS_PSW_CDC);
	pswVal |= (OS_PSW_PRS_0 | OS_PSW_IO_SU | OS_PSW_GW | OS_PSW_CDC_DIS);
	OS_MTCR(OS_PSW, pswVal);

	/* Turn on protected mode
	*/
	OS_MTCR(OS_SYSCON, OS_SYSCON_PROTEN);
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
