/* TRICORE-findtasksp.c
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-findtasksp.c 17637 2014-02-05 15:58:52Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_FindTaskSp() - get a (pending) task's stack pointer.
 *
 * Given a task's context (PCXI), this function returns that stack pointer.
 * The stack pointer can be found in the first upper-context in the given
 * list.
*/
void *OS_FindTaskSp(os_uint32_t pcxi)
{
	os_uint32_t cxi = pcxi;
	os_uppercx_t *cx;
	void *sp = OS_NULL;
	os_boolean_t looking = 1;

	do {
		if ( cxi == 0 )
		{
			/* End of list (unexpected). Just return OS_NULL
			*/
			looking = 0;
		}
		else
		{
			cx = OS_AddrFromCx(cxi);

			if ( (cxi & OS_PCXI_UL) == 0 )
			{
				/* Lower context. Get the index of the next CSA and
				 * continue. It doesn't matter that our pointer type
				 * is wrong - the next PCXI is always in the same place.
				*/
				cxi = cx->pcxi;
			}
#if OS_CPU == OS_TC1797
			/* Temporary workaround for suspected problem with the TC1797
			 * The CPU appears to accept interrupts after a SYSCALL instruction. The only
			 * bad result of this is that there's an extra CSA in the task list that
			 * contains the state at the Trap 6 vector. Unfortunately that's the one
			 * where we assume the task's SP is, but it contains the kernel SP.
			 *
			 * To work around this, we skip over all upper contexts whose PSW.IS flag is set.
			*/
			else
			if ( (cx->psw & OS_PSW_IS) != 0 )
			{
				/* Upper context, but one belongin to a SYSCALL (or other trap/interrupt
				 * that has been incorrectly interrupted). Get the index of the next CSA and
				 * continue. It doesn't matter that our pointer type
				 * is wrong - the next PCXI is always in the same place.
				*/
				cxi = cx->pcxi;
			}
#endif
			else
			{
				/* Upper context. Extract the SP and stop looking
				*/
				looking = 0;
				sp = (void *)cx->a10;
			}
		}
	} while ( looking );

	return sp;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
