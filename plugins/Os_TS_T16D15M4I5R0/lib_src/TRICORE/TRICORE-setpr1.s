/* TRICORE-setpr1.s
 *
 * This file contains the function OS_SetPr1
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-setpr1.s 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_kernel.h>
#include <Os_tool.h>
#include <TRICORE/Os_TRICORE_core.h>

/*!
 * OS_SetPr1() - sets up protection register set 1 (code and data)
 *
 * This routine transfers a full set of protection register values into
 * code protection register set 1 and data protection register set 1,
 * including the associated protection mode registers.
 *
 * The protection register ranges are first loaded into data registers,
 * thus (hopefully) avoiding the need for dsync/isync before and after
 * each mtcr.
 *
 * The code protection registers and CPM are not written if the cpm
 * value (d4) is zero. This should permit debugging of systems
 * while retaining data protection.
 *
 * The function's prototype is
 * void OS_SetPr1
 * (   unsigned cpm,	/@ in register d4 @/
 *     unsigned *cpr,	/@ in register a4 @/
 *     unsigned dpm,	/@ in register d5 @/
 *     unsigned *dpr,	/@ in register a5 @/
 * )
*/
	_TEXT
	_GLOBAL	OS_SetPr1
	_GTYPE(OS_SetPr1,@function)
	_GLOBAL	__callee.OS_SetPr1.da.v.ipip
	_GTYPE(__callee.OS_SetPr1.da.v.ipip,@function)

_TTYPE(OS_SetPr1,FUNC)
_TTYPE(__callee.OS_SetPr1.da.v.ipip,FUNC)
	_entry

	ld.w	d8,[a5]0		/* Load all 8 DPR registers */
	ld.w	d9,[a5]4
	ld.w	d10,[a5]8
	ld.w	d11,[a5]12
	ld.w	d12,[a5]16
	ld.w	d13,[a5]20
	ld.w	d14,[a5]24
	ld.w	d15,[a5]28

	jeq		d4,_IMM(#,0),pr1Partial

	ld.w	d0,[a4]0		/* Load all 4 CPR registers */
	ld.w	d1,[a4]4
	ld.w	d2,[a4]8
	ld.w	d3,[a4]12

	_dsync

	mtcr	_IMM(#,OS_DPR1_0L),d8	/* Program DPR1 */
	mtcr	_IMM(#,OS_DPR1_0U),d9
	mtcr	_IMM(#,OS_DPR1_1L),d10
	mtcr	_IMM(#,OS_DPR1_1U),d11
	mtcr	_IMM(#,OS_DPR1_2L),d12
	mtcr	_IMM(#,OS_DPR1_2U),d13
	mtcr	_IMM(#,OS_DPR1_3L),d14
	mtcr	_IMM(#,OS_DPR1_3U),d15
	mtcr	_IMM(#,OS_DPM1),d5		/* Program DPM1 */

	mtcr	_IMM(#,OS_CPR1_0L),d0	/* Program CPR1 */
	mtcr	_IMM(#,OS_CPR1_0U),d1
	mtcr	_IMM(#,OS_CPR1_1L),d2
	mtcr	_IMM(#,OS_CPR1_1U),d3
	mtcr	_IMM(#,OS_CPM1),d4		/* Program CPM1 */

	isync

	ret

pr1Partial:
	_dsync

	mtcr	_IMM(#,OS_DPR1_0L),d8	/* Program DPR1 */
	mtcr	_IMM(#,OS_DPR1_0U),d9
	mtcr	_IMM(#,OS_DPR1_1L),d10
	mtcr	_IMM(#,OS_DPR1_1U),d11
	mtcr	_IMM(#,OS_DPR1_2L),d12
	mtcr	_IMM(#,OS_DPR1_2U),d13
	mtcr	_IMM(#,OS_DPR1_3L),d14
	mtcr	_IMM(#,OS_DPR1_3U),d15
	mtcr	_IMM(#,OS_DPM1),d5		/* Program DPM1 */

	isync

	ret

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
