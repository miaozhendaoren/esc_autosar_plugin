/* TRICORE-catkhandler.s
 *
 * This file contains the category K interrupt entry handler routine for CPU load monitoring.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-catkhandler.s 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_kernel.h>
#include <Os_tool.h>
#include <TRICORE/Os_TRICORE_core.h>
#include <TRICORE/Os_TRICORE_stm.h>
#include <Os_cpuload_kernel.h>

	_GLOBAL		OS_CatKHandler
	_EXTERN		OS_CatKEntry
	_EXTERN		OS_cpuLoad

/* OS_CatKHandler
 *
 * This routine is the interrupt entry routine for Category K interrupts when CPU load monitoring
 * is enabled. It is called by the interrupt vector stub and exits via a jump to OS_CatKEntry,
 * preserving the parameter in D4, so that it appears as if OS_CatKEntry
 * has been called directly from the vector.
 *
 * Note: when CPU load monitoring is disabled the vector stub should call OS_CatKEntry directly.
 * Thus there is no conditional compilation here for excluding CPU load monitoring.
 *
 * Conditions on entry:
 *    parameter for OS_CatKEntry is in d4.
 *    interrupts have been globally ENABLED by the BISR in the vector. Thus we can safely
 *    use DISABLE and ENABLE to protect access to the STM.
 *    both lower and upper contexts have been saved, so all registers are available for use.
 *
 * !LINKSTO Kernel.InterruptProcessing.InterruptScheduling, 1
*/
	_TEXT

OS_CatKHandler:

	movh.a		a15,_IMM(#,_hiword(OS_cpuLoad))			/* Load address of OS_cpuLoad structure */
	lea			a15,[a15]_loword(OS_cpuLoad)

	ld.w		d15, [a15]OS_CPULOAD_BUSYNESTINGCOUNTER								/* Load the busy nesting counter */
	jz			d15, OS_LeaveIdle						/* Jump if zero: leaving idle */
	add			d15, _IMM(#,1)							/* Non-zero: just increment */
	st.w		[a15]OS_CPULOAD_BUSYNESTINGCOUNTER, d15
	j			OS_CatKEntry

OS_LeaveIdle:
	disable												/* Prevent ANY interrupt while leaving idle and reading STM */
	mov			d15, _IMM(#,1)							/* Set busy nesting counter to 1 */
	st.w		[a15]OS_CPULOAD_BUSYNESTINGCOUNTER, d15

	ld.w		d8, (OS_STM_BASE+OS_STM_TIM0_OFFSET)	/* Latches upper bits into stm_cap */
	ld.w		d9, (OS_STM_BASE+OS_STM_CAP_OFFSET)
	enable
	st.d		[a15]OS_CPULOAD_IDLEEXITTIME, e8		/* Store whole of stm (56 bits) */

	j			OS_CatKEntry



/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
