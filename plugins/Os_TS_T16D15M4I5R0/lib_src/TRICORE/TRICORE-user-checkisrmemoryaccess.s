/* TRICORE-user-checkisrmemoryaccess.s
 *
 * This file contains the system function OS_UserCheckIsrMemoryAccess()
 * Generated by the makesc.pl script
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_tool.h>
#include <Os_syscalls.h>

	_TEXT
	_GLOBAL	OS_UserCheckIsrMemoryAccess
	_GTYPE(OS_UserCheckIsrMemoryAccess,@function)
	_GLOBAL	__callee.OS_UserCheckIsrMemoryAccess.da.i.ipi
	_GTYPE(__callee.OS_UserCheckIsrMemoryAccess.da.i.ipi,@function)
	_GLOBAL	CheckISRMemoryAccess
	_GTYPE(CheckISRMemoryAccess,@function)
	_GLOBAL	__callee.CheckISRMemoryAccess.da.i.ipi
	_GTYPE(__callee.CheckISRMemoryAccess.da.i.ipi,@function)

/*!
 *	OS_UserCheckIsrMemoryAccess()
 *
 *	This routine makes the system call to OS_SC_CheckIsrMemoryAccess
*/

_TTYPE(OS_UserCheckIsrMemoryAccess,FUNC)
_TTYPE(__callee.OS_UserCheckIsrMemoryAccess.da.i.ipi,FUNC)
_TTYPE(CheckISRMemoryAccess,FUNC)
_TTYPE(__callee.CheckISRMemoryAccess.da.i.ipi,FUNC)

	_entry
	syscall	_IMM(#,OS_SC_CheckIsrMemoryAccess)
	ret

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
