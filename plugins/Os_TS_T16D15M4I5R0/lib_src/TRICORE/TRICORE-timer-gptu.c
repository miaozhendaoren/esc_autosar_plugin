/* timerTRICOREgptu.c - Tricore GPTU driver for ProOSEK/Cert
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-timer-gptu.c 17974 2014-04-16 10:30:04Z tojo2507 $
 *
 * modification history
 * --------------------
 * $Log$
 * Revision 1.6  2005/04/21 13:38:38  dh
 * Update to latest kernel driver interface.
 *
 * Revision 1.5  2005/01/19 11:14:04  dh
 * Rename HW timer descriptor data type.
 *
 * Revision 1.4  2004/12/01 07:25:12  dh
 * Remove some F I X M E comments.
 *
 * Revision 1.3  2004/10/07 05:55:08  dh
 * Error message if no GPTUs are present on CPU variant.
 *
 * Revision 1.2  2004/08/19 09:04:06  dh
 * Fix some CheckC complaints.
 *
 * Revision 1.1  2004/08/18 07:45:26  dh
 * First check-in.
 *
*/

#include <Os_kernel.h>
#include <Os_tool.h>

#if defined EB_STATIC_CHECK

os_uint32_t	OS_gptu_dummy;

#else

#if defined(N_GPTU) && (N_GPTU > 0)
#define INCLUDE_GPTU_DRIVER
#else
#error "No GPTUs are present"
#endif

/*!
 * gptudescr_t
 *
 * This structure describes how to program a Tricore GPTU timer (not
 * timer 2) to generate simple periodic interrupts.
 *
 * The ***Mask fields are used to clear certain bits of a register before
 * setting those bits to the desired value.
 * The ***Val fields are the desired values.
 *
 * irsVal sets the timer to be a 32-bit counter. Segment A is driven
 * from the clock. Segments B, C and D are driven from the output of the
 * previous segment. Segments A, B and C are reloaded on overflow of the
 * next segment. Segment D reloads on overflow. The timer is an up counter
 * that reloads on overflow. Therefore the reload value is the negative
 * of the interval required.
 *
 * otsVal sets all timer outputs to be from segment D.
 *
 * srselVal selects service request node 0 for timer 0 and service request
 * node 1 for timer 1.
 *
 * runBits lets all segments run. No mask is required.
*/
#ifdef INCLUDE_GPTU_DRIVER
typedef struct gptudescr_s gptudescr_t;

struct gptudescr_s
{
	osekmp_uint32_t	irsMask;
	osekmp_uint32_t	irsVal;
	osekmp_uint32_t	otsMask;
	osekmp_uint32_t	otsVal;
	osekmp_uint32_t	srselMask;
	osekmp_uint32_t	srselVal;
	osekmp_uint32_t runBits;
};

#include <memmap/Os_mm_const_begin.h>
const gptudescr_t gptuDesc[2] =
{ {	~(GPTU_T0INS | GPTU_T0REL),
	( (GPTU_T0AINS & GPTU_TxyINS_CLK) |
	  ((GPTU_T0BINS | GPTU_T0CINS | GPTU_T0DINS) & GPTU_TxyINS_CARRY) |
	  (GPTU_T0AREL | GPTU_T0BREL | GPTU_T0CREL) ),
	~(GPTU_SOUT0 | GPTU_STRG0 | GPTU_SSR0),
	( GPTU_SS_TxDOFLO & ( GPTU_SOUT00 | GPTU_SOUT01 |
						  GPTU_STRG00 | GPTU_STRG01 |
						  GPTU_SSR00  | GPTU_SSR01) ),
	~GPTU_GTSSR0,
	( GPTU_GTSSRx_SR00 & GPTU_GTSSR0 ),
	GPTU_T0ABCDRUN
  },
  {	~(GPTU_T1INS | GPTU_T1REL),
	( (GPTU_T1AINS & GPTU_TxyINS_CLK) |
	  ((GPTU_T1BINS | GPTU_T1CINS | GPTU_T1DINS) & GPTU_TxyINS_CARRY) |
	  (GPTU_T1AREL | GPTU_T1BREL | GPTU_T1CREL) ),
	~(GPTU_SOUT1 | GPTU_STRG1 | GPTU_SSR1),
	( GPTU_SS_TxDOFLO & ( GPTU_SOUT10 | GPTU_SOUT11 |
						  GPTU_STRG10 | GPTU_STRG11 |
						  GPTU_SSR10  | GPTU_SSR11) ),
	~GPTU_GTSSR1,
	( GPTU_GTSSRx_SR10 & GPTU_GTSSR1 ),
	GPTU_T1ABCDRUN
  }
};
#include <memmap/Os_mm_const_end.h>

#endif

#include <memmap/Os_mm_code_begin.h>

/*!
 * TRICORE_GptuTimerStart()
 *
 * Initialises and starts the GPTU timer as specified in the HW timer descriptor.
*/
void TRICORE_GptuTimerStart
(	const osekmp_hwt_t *td,
	osekmp_uint32_t period,
	osekmp_uint8_t relabs
)
{
	tricore_gptu_t *gp = &gptu[td->major];
	tricore_gptublk_t *gpblk = &gp->gptu_tblk[td->minor];
	const gptudescr_t *gd = &gptuDesc[td->minor];
	osekmp_uint32_t old;
	osekmp_uint32_t new;

	COV();

	/* Stop the timer, then program the chains and the outputs.
	*/
	gp->gptu_t012run &= ~gd->runBits;
	gp->gptu_t01irs = (gp->gptu_t01irs & gd->irsMask) | gd->irsVal;
	gp->gptu_t01ots = (gp->gptu_t01ots & gd->otsMask) | gd->otsVal;

	/* Set the reload register to 0 and find the address of the counter.
	*/
	gpblk->gptu_rdcba = 0;

	if ( ECOV(relabs == OSEKMP_HWT_CMPREL) )
	{
		COV();

		/* Subtract the period from the current counter value.
		*/
		old = gpblk->gptu_dcba;
		new = old - period;
		gpblk->gptu_dcba = new;

		/* If the time set is already in the past, trigger the interrupt
		*/
		if ( (new <= old) )
		{
			gp->gptu_gtsrc[GPTU_SrcIndex(td->minor)] |= ISR_SETR;
		}
	}
	else
	if ( ECOV(relabs == OSEKMP_HWT_CTRREL) )
	{
		COV();

		/* Set the counter to the period (negated).
		*/
		new = -period;
		gpblk->gptu_dcba = new;

		/* If the time set is already in the past, trigger the interrupt
		*/
		if ( (new == 0) )
		{
			gp->gptu_gtsrc[GPTU_SrcIndex(td->minor)] |= ISR_SETR;
		}
	}
	else
	{
		COV();

		/* Set the compare register to the absolute value specified.
		 * This is a tricky one, since we don't actually have a counter
		 * to get an absolute value from. However, we can use the STM
		 * to provide the FRC for us. We set our counter to the
		 * difference. The interval needed is then "period - STM", but
		 * we must negate thatm so "STM - period" is calculated.
		 *
		 * There is the built-in assumption that the STM is running at the
		 * same rate ...
		 *
		 * This value cannot be in the past - there is no concept of "past"
		*/
		gpblk->gptu_dcba = stm.stm_tim0 - period;
	}

	/* Select the service request node
	*/
	gp->gptu_gtsrsel = (gp->gptu_gtsrsel & gd->srselMask) | gd->srselVal;

	/* Start the timer.
	*/
	gp->gptu_t012run |= gd->runBits;
}

/*!
 * TRICORE_GptuTimerReload()
 *
 * Stops the GPTU timer as specified in the HW timer descriptor.
*/
void TRICORE_GptuTimerStop(const osekmp_hwt_t *td)
{
	tricore_gptu_t *gp = &gptu[td->major];
	const gptudescr_t *gd = &gptuDesc[td->minor];

	COV();

	/* Stop all the timer segments and disconnect the counters.
	*/
	gp->gptu_t012run &= ~gd->runBits;
	gp->gptu_t01irs &= gd->irsMask;
	gp->gptu_t01ots &= gd->otsMask;
	gp->gptu_gtsrsel &= gd->srselMask;
}

/*!
 * TRICORE_GptuTimerReload()
 *
 * Reloads GPTU comparator as specified in the HW timer descriptor ready for
 * the next interrupt.
 *
 * The code here allows for the possibility that the time of the next interrupt
 * has already passed. If this happens, the interrupt is triggered manually
 * and will be serviced as soon as interrupts are re-enabled.
*/
void TRICORE_GptuTimerReload(const osekmp_hwt_t *td, osekmp_uint32_t period)
{
	osekmp_uint32_t old;
	osekmp_uint32_t new;
	tricore_gptu_t *gp = &gptu[td->major];
	tricore_gptublk_t *gpblk = &gp->gptu_tblk[td->minor];

	old = gpblk->gptu_dcba;
	new = old - period;
	gpblk->gptu_dcba = new;

	if ( (new <= old) )
	{
		gp->gptu_gtsrc[GPTU_SrcIndex(td->minor)] |= ISR_SETR;
	}
}

/*!
 * TRICORE_GptuTimerRead()
 *
 * Reads the timer and returns the result.
 *
 * The second parameter defines what kind of information is needed:
 *   OSEKMP_HWT_LEFT    time remaining until next interrupt.
 *   OSEKMP_HWT_TIMER   current free-running timer time. STM is used!
 *   OSEKMP_HWT_COMPARE current comparator value
 * The "comparator value" is manufactured from the time remaining and
 * the current STM value.
*/
osekmp_timervalue_t TRICORE_GptuTimerRead
(	const osekmp_hwt_t *td,
	osekmp_uint8_t what
)
{
	tricore_gptu_t *gp = &gptu[td->major];
	tricore_gptublk_t *gpblk = &gp->gptu_tblk[td->minor];
	osekmp_timervalue_t answer = 0xdeadbea1;

	if ( what == OSEKMP_HWT_LEFT )
	{
		answer = -gpblk->gptu_dcba;
	}
	else
	if ( what == OSEKMP_HWT_TIMER )
	{
		answer = stm.stm_tim0;
	}
	else
	if ( what == OSEKMP_HWT_COMPARE )
	{
		answer = stm.stm_tim0 - gpblk->gptu_dcba;
	}

	return answer;
}

#include <memmap/Os_mm_code_end.h>

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
