/* TRICORE-idle.s
 *
 * This file contains the OS_TRICORE_Idle function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-idle.s 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_kernel.h>
#include <Os_tool.h>
#include <TRICORE/Os_TRICORE_core.h>

#ifndef OS_Halt
#define OS_Halt()
#endif

/*!
 * OS_TricoreIdle()
 *
 * This function provides the body of the idle loop for the TRICORE processor.
 *
 * The idle loop is implemented using a function to provide an optimisation
 * barrier for the compiler. This prevents the compiler making assumptions
 * about the use of global variables during the idle loop. See kern-dispatch.c
 * for details. It also permits the user to customise the idle loop - for example
 * to place a halt instruction or something similar in it.
 *
 * On entry: the inKernel flag has been cleared, so that an ISR will dispatch a task if
 * necessary. It is therefore safe to put a halt intruction into the idle loop.
 * The function should not return, but if it does so the dispatcher simply calls it
 * again.
 *
 * The function has been recoded in assembly language so that the stack can be safely
 * discarded before enabling interrupts without having to make assumptions about the
 * code that the compiler has generated. The old C code is retained as documentation at the end.
*/
	_TEXT
	_GLOBAL	OS_TricoreIdle
	_GTYPE(OS_TricoreIdle,@function)
	_GLOBAL	__callee.OS_TricoreIdle.da.v.v
	_GTYPE(__callee.OS_TricoreIdle.da.v.v,@function)

_TTYPE(OS_TricoreIdle,FUNC)
_TTYPE(__callee.OS_TricoreIdle.da.v.v,FUNC)
	_entry

	disable								/* Disable interrupts, to be safe */

	_mfcr	d0, _IMM(#,OS_PSW)			/* Clear the IS bit of PSW */
	mov.u	d1, _IMM(#,OS_PSW_IS)
	andn	d0, d0, d1
	_mtcr	_IMM(#,OS_PSW), d0			/* Now logically on task stack (though we're not using any stack) */

	_mfcr	d0, _IMM(#,OS_ICR)			/* Clear the CCPN field of the ICR */
	mov.u	d1, _IMM(#,OS_ICR_CCPN)
	mov.u	d2, _IMM(#,OS_ICR_IE)
	andn	d0, d0, d1
	or		d0, d0, d2
	_mtcr	_IMM(#,OS_ICR), d0			/* Interrupts level now zero */

	enable								/* Enable interrupts */

OS_TricoreIdleLoop:
	OS_Halt()							/* Halt instruction, if needed */
	j		OS_TricoreIdleLoop

#if 0		/* Former C code, retained as documentation */
#ifndef OS_Halt
#define OS_Halt()	OS_NOP()
#endif

void OS_TricoreIdle(void)
{
	/* We could free the PCXI list here to save time in the task switch on return from interrupt.
	 * However, this might cause further interrupt latency. A nice solution might be to free a single CSA
	 * each time around the loop.
	*/

	OS_MTSP(OS_iSpInitial); /* Best to code this in assembler */

	OS_IntEnable();

	for (;;)
	{
		OS_Halt();
	}
}
#endif


/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
