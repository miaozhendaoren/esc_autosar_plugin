/* TRICORE-trap5handler.c
 *
 * This file contains the OS_Trap5Handler function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-trap5handler.c 18458 2014-07-15 08:40:00Z ingi2575 $
*/

#define OS_SID OS_SID_ArchTrapHandler
#define OS_SIF OS_svc_ArchTrapHandler

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_Trap5Handler()
 *
 * This function handles TRICORE class 5 traps.
 *
 * Class 5 traps occur when the code specifically asks the CPU to check
 * for an error condition.
 *
 * The trap is therefore handled as a non-fatal exception, which
 * causes termination/quarantine of the task that caused the error.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.CPU.Exception, 1
*/
void OS_Trap5Handler(void)
{
	os_uppercx_t *ucx;
	os_lowercx_t *lcx;
	os_uint32_t pcxi;
	OS_PH_PARAMETERACCESS_DECL

	/* Ensure that the function call is complete before reading PCXI
	 * This might not be necessary if errata-workaround
	 * OS_TRICORE_BF_05 is necessary, because then there's a DSYNC
	 * in the function entry anyway.
	*/
	OS_CxDsync();

	pcxi = OS_MFCR(OS_PCXI);

	/* If either of the cx values turns out to be OS_NULL, we have a serious
	 * problem.
	*/
	ucx = OS_AddrFromCx(pcxi);
	lcx = OS_AddrFromCx(ucx->pcxi);

	/* The OS_TrapEntry routine places important information
	 * into the upper context, like the old values of the inKernel and
	 * inFunction flags. The trap identification number is also there.
	 * The exception PC (return address for the trap handler) is in
	 * the lower context that is the next in the list.
	*/
	OS_PH_SAVE_PARAMETER_N(0,lcx->a11);
	OS_PH_SAVE_PARAMETER_N(1,5);
	OS_PH_SAVE_PARAMETER_N(2,ucx->d15);

	switch ( ucx->d15 )
	{
	case 1:		/* Arithmetic overflow */
	case 2:		/* Sticky arithmetic overflow */
		/* can't propagate the return value of OS_ERROR -> ignore it */
		(void) OS_ERROR(OS_ERROR_ArithmeticTrap, OS_PH_GET_PARAMETER_VAR());
		break;

	default:	/* Unknown - not reachable? */
		/* can't propagate the return value of OS_ERROR -> ignore it */
		(void) OS_ERROR(OS_ERROR_FatalException, OS_PH_GET_PARAMETER_VAR());
		break;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
