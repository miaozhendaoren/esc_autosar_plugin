/* TRICORE-trustedfunctionwrapper.s
 *
 * This file contains the function OS_TrustedFunctionWrapper
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-trustedfunctionwrapper.s 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_kernel.h>
#include <Os_tool.h>
#include <TRICORE/Os_TRICORE_core.h>

/*!
 * OS_TrustedFunctionWrapper() - calls a trusted function
 *
 * This routine calls the trusted function whose address is in
 * A15 with the parameter that is in A14
 *
 * This function is only ever used as a "return value" from an
 * artificially-constructed calling context. It MUST NEVER be
 * called from normal C code.
 *
 * All this stub does is to copy the parameters from A14 and D15 to the
 * expected places (A4 and D4) and then call the trusted function. When
 * the trusted function returns, this wrapper must place the
 * expected return value ***from the system call*** into d2.
 * The expected return value is OS_E_OK, which is zero.
 * The return value from the trusted function itself (if any) is discarded -
 * the function ought to be void anyway.
*/
	_TEXT
	_GLOBAL	OS_TrustedFunctionWrapper
	_GTYPE(OS_TrustedFunctionWrapper,@function)
	_GLOBAL	__callee.OS_TrustedFunctionWrapper.da.v.v
	_GTYPE(__callee.OS_TrustedFunctionWrapper.da.v.v,@function)


_TTYPE(OS_TrustedFunctionWrapper,FUNC)
_TTYPE(__callee.OS_TrustedFunctionWrapper.da.v.v,FUNC)

	mov.aa	a4,a14
	mov		d4,d15
	_jinop
	calli	a15
	mov		d2,_IMM(#,OS_E_OK)
	rfe

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
