/* TRICORE-setisrprotection.c
 *
 * This file contains the OS_SetIsrProtection function for TRICORE.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-setisrprotection.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

/*!
 * __GLBL_TEXT_START and __GLBL_TEXT_END mark the boundary of the area of
 * memory that is executable by non-privileged tasks.
 *
 * __GLBL_RODATA_START and __GLBL_RODATA_END mark the boundary of ROM memory
 * that is readable by non-privileged tasks.
 *
 * __GLBL_DATA_START and __GLBL_DATA_END mark the boundary of RAM memory that
 * is readable by non-privileged tasks.
 *
 * These are not true variables; they are symbols set up by the linker.
 *
 * The extent of the area that non-privileged tasks are allowed to read
 * runs from the lower of __GLBL_RODATA_START and __GLBL_DATA_START to
 * the higher of __GLBL_RODATA_END and __GLBL_DATA_END. This is because
 * there are not enough DPRs to permit two separate regions to be defined.
 * It is assumed that reading from the addresses between the ROM and RAM
 * cannot cause harmful side-effects.
 *
*/
/* CHECK: SAVE
 * CHECK: RULE 401 OFF (these are linker-generated symbols, not variables)
*/
extern os_char_t __GLBL_TEXT_START; extern os_char_t __GLBL_TEXT_END;
extern os_char_t __GLBL_RODATA_START; extern os_char_t __GLBL_RODATA_END;
extern os_char_t __GLBL_DATA_START; extern os_char_t __GLBL_DATA_END;
/* CHECK: RESTORE
*/

/*!
 * DPRn_ACCESS, CPRn_ACCESS
 *
 * These macros define the access modes for data protection registers
 *
 * DPR0 - used for application data/bss segment, therefore READ/WRITE.
 * DPR1 - used for code/constant segment, therefore READONLY.
 * DPR2 - used for task/ISR data/bss segment, therefore READ/WRITE.
 * DPR3 - used for task/ISR stack segment, therefore READ/WRITE.
 *
 * The CPRM value is configurable to permit debugging of protected applications.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.Application.Data.Owner, 1
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.Application.Data.Read, 1
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.Application.Data.Write, 1
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.TaskISR.Data.Owner, 1
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.TaskISR.Data.WriteSameApp, 1
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.TaskISR.Data.WriteOtherApp, 1
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.TaskISR.Stack.Owner, 1
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.TaskISR.Stack.WriteSameApp, 1
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.TaskISR.Stack.WriteOtherApp, 1
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection.Kernel.DataStack.Write, 2
*/
#define DPR0_ACCESS  (OS_DPM_RE0 | OS_DPM_WE0)
#define DPR1_ACCESS  (OS_DPM_RE1)
#define DPR2_ACCESS  (OS_DPM_RE2 | OS_DPM_WE2)
#define DPR3_ACCESS  (OS_DPM_RE3 | OS_DPM_WE3)

#define DPRM_VALUE   (DPR0_ACCESS|DPR1_ACCESS|DPR2_ACCESS|DPR3_ACCESS)

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_SetIsrProtection
 *
 * This function loads the protection parameters for the given ISR
 * into the memory protection unit.
 *
 * Protection register set 1 is used for ISRs of non-privileged
 * applications. The code protection registers are initialised
 * by default.
 *
 * For TC2*: The data protection registers are initialised as follows:
 *
 * DPR0: application data (read/write)
 * DPR1: task/isr private data (read/write)
 * DPR2: stack (read/write)
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
*/
void OS_SetIsrProtection(const os_isr_t *isr, os_isrdynamic_t *id)
{
	const os_appcontext_t *a = OS_GET_APP(isr->app);
#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
#else
	os_uint32_t dpr[OS_N_DPR * 2];
#endif

	/* If the application is a privileged application, it will be running
	 * in protection register set 0. If this is the case, there's no need
	 * to load PRS1
	*/
	if (!OS_AppIsTrusted(a) )
	{
#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
		/* Program protection range 0 to 2
		 */	
				
		OS_MTCR(OS_DPR0_L, (os_uint32_t)a->dataStart);
		OS_MTCR(OS_DPR0_U, (os_uint32_t)a->dataEnd);
		
		OS_MTCR(OS_DPR1_L, (os_uint32_t)isr->dataStart);
		OS_MTCR(OS_DPR1_U, (os_uint32_t)isr->dataEnd);
		
		OS_MTCR(OS_DPR2_L, (os_uint32_t)id->c.stackLimit - isr->stackLen);
		OS_MTCR(OS_DPR2_U, (os_uint32_t)id->c.stackLimit);

#else
	   /*  Program protection register set 1.
		*/
		dpr[0] = (os_uint32_t)a->dataStart;
		dpr[1] = (os_uint32_t)a->dataEnd;
		dpr[2] = (os_uint32_t)OS_globalDpr1[0];
		dpr[3] = (os_uint32_t)OS_globalDpr1[1];
		dpr[4] = (os_uint32_t)isr->dataStart;
		dpr[5] = (os_uint32_t)isr->dataEnd;
		dpr[7] = (os_uint32_t)id->c.stackLimit;
		dpr[6] = dpr[7] - isr->stackLen;

		OS_SetPr1(OS_userCpm, OS_globalCpr, DPRM_VALUE, dpr);
#endif
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
