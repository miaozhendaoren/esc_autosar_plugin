/* TRICORE-user-clearevent.s
 *
 * This file contains the system function OS_UserClearEvent()
 * Generated by the makesc.pl script
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_tool.h>
#include <Os_syscalls.h>

	_TEXT
	_GLOBAL	OS_UserClearEvent
	_GTYPE(OS_UserClearEvent,@function)
	_GLOBAL	__callee.OS_UserClearEvent.da.i.i
	_GTYPE(__callee.OS_UserClearEvent.da.i.i,@function)
	_GLOBAL	ClearEvent
	_GTYPE(ClearEvent,@function)
	_GLOBAL	__callee.ClearEvent.da.i.i
	_GTYPE(__callee.ClearEvent.da.i.i,@function)

/*!
 *	OS_UserClearEvent()
 *
 *	This routine makes the system call to OS_SC_ClearEvent
*/

_TTYPE(OS_UserClearEvent,FUNC)
_TTYPE(__callee.OS_UserClearEvent.da.i.i,FUNC)
_TTYPE(ClearEvent,FUNC)
_TTYPE(__callee.ClearEvent.da.i.i,FUNC)

	_entry
	syscall	_IMM(#,OS_SC_ClearEvent)
	ret

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
