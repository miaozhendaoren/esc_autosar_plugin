/* TRICORE-wrinteendinit.c
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-writeendinit.c 18072 2014-04-25 14:23:28Z olme8414 $
*/

#include <Os_Version.h>
#include <Os_kernel.h>
#include <Os_tool.h>
#include <TRICORE/Os_defs_TRICORE.h>

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
#include <TRICORE/Os_TRICORE_aurix_wdt.h>
#else
#include <TRICORE/Os_TRICORE_cpu.h>
#include <TRICORE/Os_TRICORE_pwr.h>
#endif

#include <memmap/Os_mm_code_begin.h>

/* A helper function for Aurix.
 * Keeping the desired value in the first parameter means that the compiled code
 * does not need to shuffle registers around before loading the second parameter.
*/
#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
static void OS_WriteEndinitAurix(os_uint32_t, os_tricore_aurix_wdt_t*);
#endif

/* OS_WriteEndinit() - write to the ENDINIT bit
 *
 * This routine writes the parameter (either 0 or 1) to the ENDINIT bit.
 * A secret handshake is required before this can be done.
 *
 * If the processor is a member of the Aurix family, the helper function is called.
 *
 * Otherwise, read on ...
 *
 * WDTCON0 is unlocked by writing to it all the h/w password bits read from
 * the corresponding bit positions in WDTCON1. The lock bit (WDTLCK) is
 * inverted.
 * Then the ENDINIT bit can be written to WDTCON0, along with the once-more
 * inverted lock bit to lock the register again.
 *
 * As long as ENDINIT is clear, a watchdog is active. If ENDINIT
 * remains clear for longer than some predetermined time, a trap of
 * some kind is triggered (NMI or RESET). So it's best to disable ALL
 * interrupts for the period between setting ENDINIT to 0 and setting it
 * back to 1 again, and to keep this time as short as possible.
*/
void OS_WriteEndinit(os_uint32_t v)
{
#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)

	/* Aurix
	*/
	OS_WriteEndinitAurix(v, OS_cpuWdt);

#else

	/* Tricore 1.3 and 1.6
	*/
	os_uint32_t w0, w1, ei;

	/* ei is a word with the ENDINIT bit set to either 0 or 1 depending on the parameter.
	 * All other bits are zero.
	*/
	ei = ( v == 0 ) ? 0u : OS_ENDINIT;

	OS_DSYNC();

	/* Perform "Password Access" to WDT_CON0 and unlock it for the modification of ENDINIT.
	 * The expression contains unnecessary typecasts because of MISRA "underlying type" rules.
	*/
	w1 = OS_pwr.pwr_wdtcon1 & (OS_WDTDR | OS_WDTIR);
	w0 = ( ( (OS_pwr.pwr_wdtcon0 & (os_uint32_t) ~((os_uint32_t)OS_WDTHPW0_MSK)) | OS_WDTHPW1_MSK ) ^ OS_WDTLCK ) | w1;
	OS_pwr.pwr_wdtcon0 = w0;

	/* Perform "Modify Access" to WDT_CON0 and write the desired value of ENDINIT.
	 * The expression contains unnecessary typecasts because of MISRA "underlying type" rules.
	*/
	OS_pwr.pwr_wdtcon0 = ( (w0 & (os_uint32_t) ~((os_uint32_t)OS_WDTHPW0_MSK | OS_ENDINIT)) ^ OS_WDTLCK ) | ei;

	/* Wait until ENDINIT really is the expected value.
	*/
	while ( (OS_pwr.pwr_wdtcon0 & OS_ENDINIT) != ei )
	{
		/* If the system stops here, something went wrong. Probably the password access.
		*/
	}

#endif
}

/* The remainder of the file is only for Aurix
*/
#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)

/* OS_WriteEndinitAurix
 *
 * This helper function perform an ENDINIT modification on a WDTCON0 register
 * on Aurix platforms. This is an internal helper function.
 *
 * This routine writes the parameter (either 0 or 1) to the Safety-ENDINIT bit.
 * A secret handshake is required before this can be done.
 *
*/
static void OS_WriteEndinitAurix(os_uint32_t v, os_tricore_aurix_wdt_t *pWdt)
{
	os_uint32_t modify, password, ei, wmode, lsfr, lsb;
	OS_DSYNC();

	/* This is the target value of the ENDINIT bit
	*/
	ei = ( v == 0 ) ? 0u : OS_ENDINIT;

	/* This is a local copy of the WDTxSR, from which we use the PAS and TCS bits.
	*/
	wmode = pWdt->wdt_sr;

	/* Read the current password and re-invert the bits that are supplied inverted.
	 * Also clear the LCK bit.
	*/
	password = ((pWdt->wdt_con[0] ^ OS_WDT_PW_INV) & ((os_uint32_t)~(os_uint32_t)OS_WDT_LCK)) | OS_ENDINIT;

	/* Calculate the value to use in the modify access.
	 * This value sets the LCK bit again and has ENDINIT as requested.
	*/
	modify = (password & ((os_uint32_t)~(os_uint32_t)OS_ENDINIT)) | OS_WDT_LCK | ei;

	if ( (wmode & OS_WDT_PAS) != 0 )
	{
		/* Calculate the next password from the current password. This first step isolates
		 * the existing password.
		*/
		lsfr = password & OS_WDT_PW;

		/* Calculate XOR of bits 15, 14, 13 and 3 into bit 2. The other bits contain junk after all the
		 * shifting and XORing, so discard them.
		*/
		lsb = ((lsfr >> 13) ^ (lsfr >> 12) ^ (lsfr >> 11) ^ (lsfr >> 1)) & OS_WDT_PW_LSB;

		/* Calculate the lsfr shifted left. Discard the upper bit of the field.
		 * This shifts a zero into the LSB.
		*/
		lsfr = (lsfr << 1) & OS_WDT_PW;

		/* Put the shifted password and the calculated lsb back into the password field.
		*/
		password = (password & ((os_uint32_t)~(os_uint32_t)OS_WDT_PW)) | lsfr | lsb;
	}

	if ( (wmode & OS_WDT_TCS) != 0 )
	{
		/* Calculate the time estimate for the password. This uses the fact that the TIM field of
		 * WDTxSR is in the same place (upper 16 bits) as the REL field of WDTxCON0.
		 * The TIMEGUESS must be shifted up there too.
		*/
		password = (password & ~OS_WDT_REL) | ( (~(pWdt->wdt_sr + OS_WDT_TIMEGUESS)) & OS_WDT_REL);
	}

	/* Perform "Password Access" to WDTxCON0 to clear the LCK bit and enable modification of ENDINIT.
	*/
	pWdt->wdt_con[0] = password;

	/* Perform "Modify Access" to WDTxCON0 to change ENDINIT and set the LCK bit again.
	*/
	pWdt->wdt_con[0] = modify;

	/* Wait until ENDINIT really is the expected value.
	*/
	while ( (pWdt->wdt_con[0] & OS_ENDINIT) != ei )
	{
		/* If the processor hangs here, the password was probably wrong. */
	}
}

/* OS_WriteSafetyEndinit() - write to the Safety-ENDINIT bit
 *
 * It calls OS_WriteEndinitAurix() to do the job.
*/
void OS_WriteSafetyEndinit(os_uint32_t v)
{
	OS_WriteEndinitAurix(v, OS_safetyWdt);
}

#endif

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
