/* Ioc_TriggerCbk.h
 *
 * Type and Constants for Ioc Notification callbacks
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Ioc_TriggerCbk.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef IOC_TRIGGERCBK_H
#define IOC_TRIGGERCBK_H

/** \brief type of callback functions */
typedef void (* Ioc_CallbackType)(void);

/** \brief pointer to array of pointers to array of callback-functions
 *
 * This is an array with the callback-functions that
 * are called cross-core. This array contains for every core
 * a pointer to an array with the callback-function pointers
 * for this core. This array is generated by the Ioc generator
 */
extern Ioc_CallbackType const * const * const Ioc_CallbacksBase;

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
