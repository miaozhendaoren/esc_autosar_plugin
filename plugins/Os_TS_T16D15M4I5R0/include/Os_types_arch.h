/* Os_types_arch.h
 *
 * Architecture-dependent types are included from the appropriate ARCH-types.h
 * include file depending on the chosen architecture.
 *
 * The Makefiles must ensure that the OS_ARCH and OS_CPU macros are
 * defined appropriately on the command line.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_types_arch.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef __OS_TYPES_ARCH_H
#define __OS_TYPES_ARCH_H

#include <Os_defs.h>

#if (OS_ARCH==OS_PA)
#include <PA/Os_types_PA.h>
#elif (OS_ARCH==OS_TRICORE)
#include <TRICORE/Os_types_TRICORE.h>
#elif (OS_ARCH==OS_V850)
#include <V850/Os_types_V850.h>
#elif (OS_ARCH==OS_RH850)
#include <RH850/Os_types_RH850.h>
#elif (OS_ARCH==OS_NEWARCH)
#include <NEWARCH/Os_types_NEWARCH.h>
#elif (OS_ARCH==OS_PIKEOS)
#include <PIKEOS/Os_types_PIKEOS.h>
#elif (OS_ARCH==OS_S12X)
#include <S12X/Os_types_S12X.h>
#elif (OS_ARCH==OS_XC2000)
#include <XC2000/Os_types_XC2000.h>
#elif (OS_ARCH==OS_EASYCAN)
#include <EASYCAN/Os_types_EASYCAN.h>
#elif (OS_ARCH==OS_MB91)
#include <MB91/Os_types_MB91.h>
#elif (OS_ARCH==OS_R32C)
#include <R32C/Os_types_R32C.h>
#elif (OS_ARCH==OS_WINDOWS)
#include <WINDOWS/Os_types_WINDOWS.h>
#elif (OS_ARCH==OS_SH2)
#include <SH2/Os_types_SH2.h>
#elif (OS_ARCH==OS_SH4)
#include <SH4/Os_types_SH4.h>
#elif (OS_ARCH==OS_ARM)
#include <ARM/Os_types_ARM.h>
#elif (OS_ARCH==OS_LINUX)
#include <LINUX/Os_types_LINUX.h>
#elif (OS_ARCH==OS_DSPIC33)
#include <DSPIC33/Os_types_DSPIC33.h>
#else
#error "Unsupported OS_ARCH defined. Check your Makefiles!"
#endif

/* Os types (non-microkernel variant) */

/*!
 * os_tick_t
 *
 * Tick counter - only if not already defined
*/
#ifndef OS_ARCH_TICK_T
#ifndef OS_ASM
typedef os_uint32_t os_tick_t;
#endif
#define OS_MAXTICK		OS_U(0xffffffff)
#define OS_SIZEOF_TICK	4
#endif

/*!
 * os_alarmbase_t
 *
 * Alarmbase structure for GetAlarmBase
*/
#ifndef OS_ASM
typedef struct os_alarmbase_s os_alarmbase_t;

struct os_alarmbase_s
{
	os_tick_t	maxallowedvalue;
	os_tick_t	ticksperbase;
	os_tick_t	mincycle;
};
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
