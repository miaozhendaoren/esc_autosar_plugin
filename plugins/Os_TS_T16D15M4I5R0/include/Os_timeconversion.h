/* Os_timeconversion.h - collection of timer conversion macros.
 *
 * This file defines a collection of time conversion macros (ns-to-ticks and ticks-to-ns)
 * for various frequencies.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Os_timeconversion.h 20165 2015-01-19 07:38:33Z dh $
*/

#ifndef OS_TIMECONVERSION_H
#define OS_TIMECONVERSION_H

#include <Os_types.h>

/* Commonly-used macros:
 *
 * OS_DivMulDiv() performs the divide-multiply-divide operation used in many ns-to-ticks calculations
 * OS_MulDivMul() performs the multiply-divide-multiply operation used in many ticks-to-ns calculations
 * OS_SatMulDivMul() performs OS_MulDivMul(), but with a saturation test first.
*/
#define OS_DivMulDiv(x, d1, m, d2)			(((((os_uint32_t)(x))/(d1))*(m))/(d2))
#define OS_MulDivMul(x, m1, d, m2)			(((((os_uint32_t)(x))*(m1))/(d))*(m2))
/* Deviation DCG-1 */
#define OS_SatMulDivMul(x, m1, d, m2, max)	(((x)>(max)) ? 0xffffffffu : OS_MulDivMul((x),(m1),(d),(m2)))

/* Clock frequency 4 MHz
 *
 * Factor is 4000000/1000000000
 *         = 4/1000
 *         = 1/250
 * No mul-div-mul needed.
*/
#define OS_NsToTicks_4000000(ns)                ((ns)/250u)
#define OS_TicksToNs_4000000(tx)                (((tx) > 17179869u) ? 0xffffffffu : ((tx)*250u))

/* Clock frequency 8 MHz
 *
 * Factor is 8000000/1000000000
 *         = 8/1000
 *         = 1/125
 * No mul-div-mul needed.
*/
#define OS_NsToTicks_8000000(ns)			((ns)/125u)
#define OS_TicksToNs_8000000(tx)			(((tx) > 34359738u) ? 0xffffffffu : ((tx)*125u))

/* Clock frequency 16 MHz
 *
 * Factor is 16000000/1000000000
 *         = 16/1000
 *         = 2/125
 * 125 = 5*25 and 5>2, so 5 is the first division factor
*/
#define OS_NsToTicks_16000000(ns)	OS_DivMulDiv((ns), 5u, 2u, 25u)
#define OS_TicksToNs_16000000(tx)	OS_SatMulDivMul((tx), 25u, 2u, 5u, 68719476u)

/* Clock frequency 20 MHz
 *
 * Factor is 20000000/1000000000
 *         = 20/1000
 *         = 1/50
 * No mul-div-mul needed.
*/
#define OS_NsToTicks_20000000(ns)	((ns)/50u)
#define OS_TicksToNs_20000000(tx)	( ((tx) > 85899345u) ? 0xffffffffu : ((tx)*50u) )

/* Clock frequency 25 MHz
 *
 * Factor is 25000000/1000000000
 *         = 25/1000
 *         = 1/40
 * No mul-div-mul needed.
*/
#define OS_NsToTicks_25000000(ns)               ((ns)/40u)
#define OS_TicksToNs_25000000(tx)               (((tx) > 107374182u) ? 0xffffffffu : ((tx)*40u))

/* Clock frequency 32 MHz
 *
 * Factor is 32000000/1000000000
 *         = 32/1000
 *         = 4/125
 * 125 = 5*25 and 5>4, so 5 is the first division factor
*/
#define OS_NsToTicks_32000000(ns)	OS_DivMulDiv((ns), 5u, 4u, 25u)
#define OS_TicksToNs_32000000(tx)	OS_SatMulDivMul((tx), 25u, 4u, 5u, 137438953u)

/* Clock frequency 37.5 MHz
 *
 * Factor is 37500000/1000000000
 *         = 375/10000
 *         = 3/80
 * 80 = 4 * 20 and 4>3, so 4 is the first division factor
*/
#define OS_NsToTicks_37500000(ns)               OS_DivMulDiv((ns), 4u, 3u, 20u)
#define OS_TicksToNs_37500000(tx)               OS_SatMulDivMul((tx), 20u, 3u, 4u, 161061273u)

/* Clock frequency 40 MHz
 *
 * Factor is 40000000/1000000000
 *         = 40/1000
 *         = 1/25
 * No mul-div-mul needed.
*/
#define OS_NsToTicks_40000000(ns)	((ns)/25u)
#define OS_TicksToNs_40000000(tx)	( ((tx) > 171798691u) ? 0xffffffffu : ((tx)*25u) )

/* Clock frequency 48 MHz
 *
 * Factor is 48000000/1000000000
 *         = 48/1000
 *         = 6/125
 * 125 = 25*5 and 25>6, so 25 is the first division factor
*/
#define OS_NsToTicks_48000000(ns)	OS_DivMulDiv((ns), 25u, 6u, 5u)
#define OS_TicksToNs_48000000(tx)	OS_SatMulDivMul((tx), 5u, 6u, 25u, 206158430u)

/* Clock frequency 55 MHz
 *
 * Factor is 55000000/1000000000
 *         = 55/1000
 *         = 11/200
 * 200 = 20*10, and 20>11, so 20 is the first division factor
*/
#define OS_NsToTicks_55000000(ns)	OS_DivMulDiv((ns), 20u, 11u, 10u)
#define OS_TicksToNs_55000000(tx)	OS_SatMulDivMul((tx), 10u, 11u, 20u, 236223201u)

/* Clock frequency 60 MHz
 *
 * Factor is 60000000/1000000000
 *         = 60/1000
 *         = 3/50
 * 50 = 25*2, and 25>3, so 25 is the first division factor
*/
#define OS_NsToTicks_60000000(ns)	OS_DivMulDiv((ns), 25u, 3u, 2u)
#define OS_TicksToNs_60000000(tx)	OS_SatMulDivMul((tx), 2u, 3u, 25u, 257698037u)

/* Clock frequency 64 MHz
 *
 * Factor is 64000000/1000000000
 *         = 64/1000
 *         = 8/125
 * 125 = 25*5 and 25>8, so 25 is the first division factor
*/
#define OS_NsToTicks_64000000(ns)	OS_DivMulDiv((ns), 25u, 8u, 5u)
#define OS_TicksToNs_64000000(tx)	OS_SatMulDivMul((tx), 5u, 8u, 25u, 274877906u)

/* Clock frequency 70 MHz
 *
 * Factor is 70000000/1000000000
 *         = 7/100
 * 100 = 10*10, so there 10 is the first and last divisor.
*/
#define OS_NsToTicks_70000000(ns)               OS_DivMulDiv((ns), 10u, 7u, 10u)
#define OS_TicksToNs_70000000(tx)               OS_SatMulDivMul((tx), 10u, 7u, 10u, OS_NsToTicks_70000000(0xffffffffu))

/* Clock frequency 75 MHz
 *
 * Factor is 75000000/1000000000
 *         = 75/1000
 *         = 3/40
 * 40 = 4*10, and 4>3, so 4 is the first division factor
*/
#define OS_NsToTicks_75000000(ns)	OS_DivMulDiv((ns), 4u, 3u, 10u)
#define OS_TicksToNs_75000000(tx)	OS_SatMulDivMul((tx), 10u, 3u, 4u, 322122547u)

/* Clock frequency 80 MHz
 *
 * Factor is 80000000/1000000000
 *         = 80/1000
 *         = 2/25
 * 25 = 5*5 and 5>2, so 5 is the first division factor
*/
#define OS_NsToTicks_80000000(ns)	OS_DivMulDiv((ns), 5u, 2u, 5u)
#define OS_TicksToNs_80000000(tx)	OS_SatMulDivMul((tx), 5u, 2u, 5u, 343597383u)

/* Clock frequency 90 MHz
 *
 * Factor is 90000000/1000000000
 *         = 90/1000
 *         = 9/100
 * 100 = 10*10 and 10>9, so 10 is the first division factor
*/
#define OS_NsToTicks_90000000(ns)	OS_DivMulDiv((ns), 10u, 9u, 10u)
#define OS_TicksToNs_90000000(tx)	OS_SatMulDivMul((tx), 10u, 9u, 10u, 386547056u)

/* Clock frequency 96 MHz
 *
 * Factor is 96000000/1000000000
 *         = 96/1000
 *         = 12/125
 *
 * 125 = 25 * 5 and 25>12, so 25 is the first division factor.
*/
#define OS_NsToTicks_96000000(ns)               OS_DivMulDiv((ns), 25u, 12u, 5u)
#define OS_TicksToNs_96000000(tx)               OS_SatMulDivMul((tx), 5u, 12u, 25u, 412316860u)

/* Clock frequency 100 MHz
 *
 * Factor is 100000000/1000000000
 *         = 1/10
 * No mul-div-mul needed.
*/
#define OS_NsToTicks_100000000(ns)	((ns)/10u)
#define OS_TicksToNs_100000000(tx)	( ((tx) > 429496729u) ? 0xffffffffu : ((tx)*10u) )

/* Clock frequency 110 MHz
 *
 * Factor is 110000000/1000000000
 *         = 11/100
 * 100 = 20*5, and 20>11, so 20 is the first division factor
 *
 * The limit is calculated by hand because the rounding error in the ns-to-ticks macro gives an incorrect limit.
*/
#define OS_NsToTicks_110000000(ns)	OS_DivMulDiv((ns), 20u, 11u, 5u)
#define OS_TicksToNs_110000000(tx)	OS_SatMulDivMul((tx), 5u, 11u, 20u, 472446402u)

/* Clock frequency 120 MHz
 *
 * Factor is 120000000/1000000000
 *         = 12/100
 *         = 3/25
 * 25 = 5*5, and 5>3, so 5 is the first division factor.
*/
#define OS_NsToTicks_120000000(ns)	OS_DivMulDiv((ns), 5u, 3u, 5u)
#define OS_TicksToNs_120000000(tx)	OS_SatMulDivMul((tx), 5u, 3u, 5u, 515396075u)

/* Clock frequency 160 MHz
 *
 * Factor is 160000000/1000000000
 *         = 16/100
 *         = 4/25
 * 25 = 5*5, and 5>4, so 5 is the first division factor.
*/
#define OS_NsToTicks_160000000(ns)	OS_DivMulDiv((ns), 5u, 4u, 5u)
#define OS_TicksToNs_160000000(tx)	OS_SatMulDivMul((tx), 5u, 4u, 5u, 687194767u)

/* Clock frequency 180 MHz
 *
 * Factor is 180000000/1000000000
 *         = 18/100
 *         = 9/50
 * 50 = 5*10 and 10>9, so 10 is the first division factor
*/
#define OS_NsToTicks_180000000(ns)	OS_DivMulDiv((ns), 10u, 9u, 5u)
#define OS_TicksToNs_180000000(tx)	OS_SatMulDivMul((tx), 5u, 9u, 10u, 773094113u)

/* Clock frequency 250 MHz
 *
 * Factor is 250000000/1000000000
 *         = 1/4
 * No mul-div-mul needed.
*/
#define OS_NsToTicks_250000000(ns)              ((ns)/4u)
#define OS_TicksToNs_250000000(tx)              (((tx) > 1073741823u) ? 0xffffffffu : ((tx)*4u))

/* Clock frequency 300 MHz
 *
 * Factor is 300000000/1000000000
 *         = 3/10
 * 10 = 5*2 and 5>3, so 5 is the first division factor
*/
#define OS_NsToTicks_300000000(ns)              OS_DivMulDiv((ns), 5u, 3u, 2u)
#define OS_TicksToNs_300000000(tx)              OS_SatMulDivMul((tx), 2u, 3u, 5u, 1288490188u)

/* Clock frequency 330 MHz
 *
 * Factor is 330000000/1000000000
 *         = 33/100
 * 100 = 50*2 and 50>33, so 50 is the first division factor
*/
#define OS_NsToTicks_330000000(ns)	OS_DivMulDiv((ns), 50u, 33u, 2u)
#define OS_TicksToNs_330000000(tx)	OS_SatMulDivMul((tx), 2u, 33u, 50u, 1417339207u)

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
