/* Os_osek.h - OSEK/VDX API
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_osek.h 17683 2014-02-11 14:43:30Z tojo2507 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.13 (advisory)
 * The # and ## preprocessor operators should not be used.
 *
 * Reason:
 * ## operator must be used to generate an identifier as demanded by specification.
 */

#ifndef __OS_OSEK_H
#define __OS_OSEK_H

#include <Os_defs.h>

#if OS_KERNEL_TYPE==OS_MICROKERNEL
#error "Os_osek.h should not be used in a microkernel environment"
#endif

#include <Os_api.h>
#include <Os_error.h>
#include <Os_removeapi.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Choose a default value for "use fast locking" that is Autosar-conformant.
 * Disabling this doesn't prevent the fast locking functions from being used.
 * It just prevents them from being used as implementations of standard OSEK/Autosar functions.
*/
#ifndef OS_USE_FAST_LOCKING
#define OS_USE_FAST_LOCKING		0
#endif

/*!
 * OSEK/VDX API
 *
 * The OSEK/VDX API is defined in terms of the OS API, which is
 * has a different naming convention and in one or two places is
 * slightly different to facilitate the memory-protection requirements.
 *
 * !LINKSTO Kernel.Autosar.OSEK, 1
*/

/*!
 * OSEK/VDX API
 *
 * These macros define the OSEK/VDX data types in terms of
 * OSEMKP data types.
 *
 * !LINKSTO Kernel.API.StatusType, 1
 * !LINKSTO Kernel.API.TaskManagement.DataTypes, 1
 * !LINKSTO Kernel.API.ResourceManagement.DataTypes, 1
 * !LINKSTO Kernel.API.EventControl.DataTypes, 1
 * !LINKSTO Kernel.API.Alarms.DataTypes, 1
 * !LINKSTO Kernel.API.OSControl.DataTypes, 1
 * !LINKSTO Kernel.API.Hooks.DataTypes, 1
*/
#if (!defined STATUSTYPEDEFINED)
/* Conformant with OSEK Binding Specification.
 * We have to check and set STATUSTYPEDEFINED to be compatible here.
*/
#ifndef OS_ASM
#define STATUSTYPEDEFINED	1
typedef unsigned char StatusType;
#endif
#endif

#if !defined(RTE_TYPE_TickType) /* needed for RTE interaction */
#define RTE_TYPE_TickType
#define TickType			os_tick_t
#endif

#define TaskType			os_taskid_t
#define TaskRefType			os_taskid_t*
#define TaskStateType		os_taskstate_t
#define TaskStateRefType	os_taskstate_t*
#define ResourceType		os_resourceid_t
#define EventMaskType		os_eventmask_t
#define EventMaskRefType	os_eventmask_t*
#define TickRefType			os_tick_t*
#define AlarmBaseType		os_alarmbase_t
#define AlarmBaseRefType	os_alarmbase_t*
#define AlarmType			os_alarmid_t
#define AppModeType			os_appmodeid_t
#define OSServiceIdType		os_serviceid_t

/*!
 * OSEK/VDX API
 *
 * These macros define the OSEK/VDX constructional elements
 *
 * !LINKSTO Kernel.API.TaskManagement.DeclareTask, 1
 * !LINKSTO Kernel.API.InterruptHandling.ISR, 1
 * !LINKSTO Kernel.API.ResourceManagement.DeclareResource, 1
 * !LINKSTO Kernel.API.EventControl.DeclareEvent, 1
 * !LINKSTO Kernel.API.Alarms.DeclareAlarm, 1
 * !LINKSTO Kernel.API.Alarms.AlarmCallback, 1
 *
 * Alarms, resources and events don't need an external declaration, but if we leave
 * the macros empty some compilers complain when working on very strict settings.
 * The previous solution of declaring a function prototype for OS_NullFunction()
 * also causes warnings when the "redundant redeclarations" warning in gcc is turned on.
 * Declaring a unique prototype for a non-existent function is also not desirable in case
 * some intelligent linker decides to warn for functions that are prototyped but don't
 * exist. The solution adopted is to declare a unique data type using typedef. This should be
 * harmless.
*/
#ifdef __cplusplus
#define OS_EXTERN_C extern "C"
#else
#define OS_EXTERN_C
#endif

/* Deviation MISRA-1 */
#define TASK(x)				OS_EXTERN_C void OS_TASK_##x(void)
/* Deviation MISRA-1 */
#define ALARMCALLBACK(x)	OS_EXTERN_C void OS_ACB_##x(void)
/* Deviation MISRA-1 */
#define ISR(x)				OS_EXTERN_C void OS_ISR_##x(void)

#ifndef DeclareTask
/* Deviation MISRA-1 */
#define DeclareTask(x)		OS_EXTERN_C void OS_TASK_##x(void)
#endif
#ifndef DeclareResource
/* Deviation MISRA-1 */
#define DeclareResource(x)	typedef os_int_t os_0##x
#endif
#ifndef DeclareEvent
/* Deviation MISRA-1 */
#define DeclareEvent(x)		typedef os_int_t os_0##x
#endif
#ifndef DeclareAlarm
/* Deviation MISRA-1 */
#define DeclareAlarm(x)		typedef os_int_t os_0##x
#endif


/*!
 * OSEK/VDX API
 *
 * These macros define the OSEK/VDX API values in terms of OS
 * constants.
 *
 * !LINKSTO Kernel.Architecture.ConformanceClasses.ResourcesBCC1, 1
*/
#define SUSPENDED			0
#define READY				1
#define RUNNING				2
#define WAITING				3
#define INVALID_TASK		OS_NULLTASK

/*!
 * OSEK/VDX API
 *
 * These macros define the OSEK/VDX error codes in terms of
 * OS error codes.
 *
 * NOTE 1: The OS must define the correct values for E_OK --- E_OS_VALUE to
 * be OSEK-conformant.
 *
 * NOTE 2: E_OK is defined as 0 here, not OS_E_OK, to try to avoid conflicts in
 * Autosar. If is only defined if not previously defined. There's also a sanity check for it afterwards.
 *
 * !LINKSTO Kernel.API.StatusType, 1
 * !LINKSTO Kernel.API.StatusType.E_OK, 1
*/
#ifndef E_OK
#define E_OK			0
#endif
#define E_OS_ACCESS		OS_E_ACCESS
#define E_OS_CALLLEVEL	OS_E_CALLLEVEL
#define E_OS_CALLEVEL	OS_E_CALLLEVEL	/* Typo in spec? */
#define E_OS_ID			OS_E_ID
#define E_OS_LIMIT		OS_E_LIMIT
#define E_OS_NOFUNC		OS_E_NOFUNC
#define E_OS_RESOURCE	OS_E_RESOURCE
#define E_OS_STATE		OS_E_STATE
#define E_OS_VALUE		OS_E_VALUE

#if ( E_OK != 0 )
#error "E_OK is non-zero. This is not OSEK-conformant"
#endif

/*!
 * OSEK/VDX API
 *
 * These macros define the OSEK/VDX API calls in
 * terms of the OS system-call stubs
 *
 * !LINKSTO Kernel.API.TaskManagement.ActivateTask.API, 1
 * !LINKSTO Kernel.API.TaskManagement.TerminateTask.API, 1
 * !LINKSTO Kernel.API.TaskManagement.ChainTask.API, 1
 * !LINKSTO Kernel.API.TaskManagement.Schedule.API, 1
 * !LINKSTO Kernel.API.TaskManagement.GetTaskID.API, 1
 * !LINKSTO Kernel.API.TaskManagement.GetTaskState.API, 1
 * !LINKSTO Kernel.API.InterruptHandling.EnableAllInterrupts.API, 1
 * !LINKSTO Kernel.API.InterruptHandling.DisableAllInterrupts.API, 1
 * !LINKSTO Kernel.API.InterruptHandling.ResumeAllInterrupts.API, 1
 * !LINKSTO Kernel.API.InterruptHandling.SuspendAllInterrupts.API, 1
 * !LINKSTO Kernel.API.InterruptHandling.ResumeOSInterrupts.API, 1
 * !LINKSTO Kernel.API.InterruptHandling.SuspendOSInterrupts.API, 1
 * !LINKSTO Kernel.API.ResourceManagement.GetResource.API, 1
 * !LINKSTO Kernel.API.ResourceManagement.ReleaseResource.API, 1
 * !LINKSTO Kernel.API.EventControl.SetEvent.API, 1
 * !LINKSTO Kernel.API.EventControl.ClearEvent.API, 1
 * !LINKSTO Kernel.API.EventControl.GetEvent.API, 1
 * !LINKSTO Kernel.API.EventControl.WaitEvent.API, 1
 * !LINKSTO Kernel.API.Alarms.GetAlarmBase.API, 1
 * !LINKSTO Kernel.API.Alarms.GetAlarm.API, 1
 * !LINKSTO Kernel.API.Alarms.SetRelAlarm.API, 1
 * !LINKSTO Kernel.API.Alarms.SetAbsAlarm.API, 1
 * !LINKSTO Kernel.API.Alarms.CancelAlarm.API, 1
 * !LINKSTO Kernel.API.OSControl.GetActiveApplicationMode.API, 1
 * !LINKSTO Kernel.API.OSControl.StartOS.API, 1
 * !LINKSTO Kernel.API.OSControl.ShutdownOS.API, 1
*/
#ifndef OS_EXCLUDE_ACTIVATETASK
#define ActivateTask(t)				OS_UserActivateTask(t)
#endif

#ifndef OS_EXCLUDE_TERMINATETASK
#define TerminateTask()				OS_UserTerminateTask()
#endif

#ifndef OS_EXCLUDE_CHAINTASK
#define ChainTask(t)				OS_UserChainTask(t)
#endif

#ifndef OS_EXCLUDE_SCHEDULE
#define Schedule()					OS_UserSchedule()
#endif

#ifndef OS_EXCLUDE_GETTASKID
#define GetTaskID(tr)				OS_UserGetTaskId(tr)
#endif

#ifndef OS_EXCLUDE_GETTASKSTATE
#define GetTaskState(t,sr)			OS_GetTaskState(t,sr)
#endif

#if OS_USE_FAST_LOCKING

#ifndef OS_EXCLUDE_DISABLEALLINTERRUPTS
#define DisableAllInterrupts()		OS_FastSuspendAllInterrupts()
#endif

#ifndef OS_EXCLUDE_ENABLEALLINTERRUPTS
#define EnableAllInterrupts()		OS_FastResumeAllInterrupts()
#endif

#ifndef OS_EXCLUDE_SUSPENDALLINTERRUPTS
#define SuspendAllInterrupts()		OS_FastSuspendAllInterrupts()
#endif

#ifndef OS_EXCLUDE_RESUMEALLINTERRUPTS
#define ResumeAllInterrupts()		OS_FastResumeAllInterrupts()
#endif

#ifndef OS_EXCLUDE_SUSPENDOSINTERRUPTS
#define SuspendOSInterrupts()		OS_FastSuspendOsInterrupts()
#endif

#ifndef OS_EXCLUDE_RESUMEOSINTERRUPTS
#define ResumeOSInterrupts()		OS_FastResumeOsInterrupts()
#endif

#else

#ifndef OS_EXCLUDE_DISABLEALLINTERRUPTS
#define DisableAllInterrupts()		OS_UserSuspendInterrupts(OS_LOCKTYPE_NONEST)
#endif

#ifndef OS_EXCLUDE_ENABLEALLINTERRUPTS
#define EnableAllInterrupts()		OS_UserResumeInterrupts(OS_LOCKTYPE_NONEST)
#endif

#ifndef OS_EXCLUDE_SUSPENDALLINTERRUPTS
#define SuspendAllInterrupts()		OS_UserSuspendInterrupts(OS_LOCKTYPE_ALL)
#endif

#ifndef OS_EXCLUDE_RESUMEALLINTERRUPTS
#define ResumeAllInterrupts()		OS_UserResumeInterrupts(OS_LOCKTYPE_ALL)
#endif

#ifndef OS_EXCLUDE_SUSPENDOSINTERRUPTS
#define SuspendOSInterrupts()		OS_UserSuspendInterrupts(OS_LOCKTYPE_OS)
#endif

#ifndef OS_EXCLUDE_RESUMEOSINTERRUPTS
#define ResumeOSInterrupts()		OS_UserResumeInterrupts(OS_LOCKTYPE_OS)
#endif

#endif	/* OS_USE_FAST_LOCKING */

#ifndef OS_EXCLUDE_GETRESOURCE
#define GetResource(r)				OS_UserGetResource(r)
#endif

#ifndef OS_EXCLUDE_RELEASERESOURCE
#define ReleaseResource(r)			OS_UserReleaseResource(r)
#endif

#ifndef OS_EXCLUDE_SETEVENT
#define SetEvent(t,e)				OS_UserSetEvent(t,e)
#endif

#ifndef OS_EXCLUDE_CLEAREVENT
#define ClearEvent(e)				OS_UserClearEvent(e)
#endif

#ifndef OS_EXCLUDE_GETEVENT
#define GetEvent(t,er)				OS_UserGetEvent(t,er)
#endif

#ifndef OS_EXCLUDE_WAITEVENT
#define WaitEvent(e)				OS_UserWaitEvent(e)
#endif

#ifndef OS_EXCLUDE_GETALARMBASE
#define GetAlarmBase(a,br)			OS_UserGetAlarmBase(a,br)
#endif

#ifndef OS_EXCLUDE_GETALARM
#define GetAlarm(a,tr)				OS_UserGetAlarm(a,tr)
#endif

#ifndef OS_EXCLUDE_SETRELALARM
#define SetRelAlarm(a,i,c)			OS_UserSetRelAlarm(a,i,c)
#endif

#ifndef OS_EXCLUDE_SETABSALARM
#define SetAbsAlarm(a,i,c)			OS_UserSetAbsAlarm(a,i,c)
#endif

#ifndef OS_EXCLUDE_CANCELALARM
#define CancelAlarm(a)				OS_UserCancelAlarm(a)
#endif

#ifndef OS_EXCLUDE_GETACTIVEAPPLICATIONMODE
#define GetActiveApplicationMode()	OS_UserGetActiveApplicationMode()
#endif

#ifndef OS_EXCLUDE_SHUTDOWNOS
#define ShutdownOS(err)				OS_UserShutdownOs(err)
#endif

#ifdef OS_ARCH_USERSTARTOS
#define StartOS(m)					OS_ArchUserStartOs(m)
#else
#define StartOS(m)					OS_UserStartOs(m)
#endif

/*!
 * OSEK/VDX API
 *
 * These macros define the OSEK/VDX Service IDs in
 * terms of the OS SIDs
 *
 * !LINKSTO Kernel.API.Hooks.Constants, 1
*/
#define OSServiceId_ActivateTask			OS_SID_ActivateTask
#define OSServiceId_TerminateTask			OS_SID_TerminateTask
#define OSServiceId_ChainTask				OS_SID_ChainTask
#define OSServiceId_GetTaskState			OS_SID_GetTaskState
#define OSServiceId_GetTaskID				OS_SID_GetTaskID
#define OSServiceId_Schedule				OS_SID_Schedule
#define OSServiceId_GetResource				OS_SID_GetResource
#define OSServiceId_ReleaseResource			OS_SID_ReleaseResource
#define OSServiceId_SetEvent				OS_SID_SetEvent
#define OSServiceId_ClearEvent				OS_SID_ClearEvent
#define OSServiceId_WaitEvent				OS_SID_WaitEvent
#define OSServiceId_GetEvent				OS_SID_GetEvent
#define OSServiceId_GetAlarm				OS_SID_GetAlarm
#define OSServiceId_GetAlarmBase			OS_SID_GetAlarmBase
#define OSServiceId_SetRelAlarm				OS_SID_SetRelAlarm
#define OSServiceId_SetAbsAlarm				OS_SID_SetAbsAlarm
#define OSServiceId_CancelAlarm				OS_SID_CancelAlarm
#define OSServiceId_SuspendOSInterrupts		OS_SID_SuspendInterrupts
#define OSServiceId_ResumeOSInterrupts		OS_SID_ResumeInterrupts
#define OSServiceId_SuspendAllInterrupts	OS_SID_SuspendInterrupts
#define OSServiceId_ResumeAllInterrupts		OS_SID_ResumeInterrupts
#define OSServiceId_DisableAllInterrupts	OS_SID_SuspendInterrupts
#define OSServiceId_EnableAllInterrupts		OS_SID_ResumeInterrupts
#define OSServiceId_StartOS					OS_SID_StartOs
#define OSServiceId_ShutdownOS				OS_SID_ShutdownOs

/*!
 * OSEK/VDX API
 *
 * The OSErrorGetServiceId() macro returns the service ID of the OSEK
 * service in which the error occurred. The value is not meaningful
 * outside the error hook, and its use outside the error hook might
 * cause a protection fault, depending on the context and configuration.
 *
 * The OSError_SIDXX_PARAMXX() macros return the value of the specified
 * parameter to the service. The return value is only meaningful inside
 * the error hook, and then only if the service ID is that which caused
 * the error.
 *
 * !LINKSTO Kernel.API.Hooks.Macros, 1
*/
#define OSErrorGetServiceId()			(OS_errorStatus.serviceId)

/* CHECK: SAVE
 * CHECK: RULE 306 OFF (Looks nicer)
*/
#define OSError_ActivateTask_TaskID()	((TaskType)			(OS_errorStatus.parameter[0]))
#define OSError_ChainTask_TaskID()		((TaskType)			(OS_errorStatus.parameter[0]))
#define OSError_GetTaskID_TaskID()		((TaskType)			(OS_errorStatus.parameter[0]))
#define OSError_GetTaskState_TaskID()	((TaskType)			(OS_errorStatus.parameter[0]))
#define OSError_GetTaskState_State()	((TaskStateRefType)	(OS_errorStatus.parameter[1]))
#define OSError_GetResource_ResID()		((ResourceType)		(OS_errorStatus.parameter[0]))
#define OSError_ReleaseResource_ResID()	((ResourceType)		(OS_errorStatus.parameter[0]))
#define OSError_SetEvent_TaskID()		((TaskType)			(OS_errorStatus.parameter[0]))
#define OSError_SetEvent_Mask()			((EventMaskType)	(OS_errorStatus.parameter[1]))
#define OSError_ClearEvent_Mask()		((EventMaskType)	(OS_errorStatus.parameter[0]))
#define OSError_WaitEvent_Mask()		((EventMaskType)	(OS_errorStatus.parameter[0]))
#define OSError_GetEvent_TaskID()		((TaskType)			(OS_errorStatus.parameter[0]))
#define OSError_GetEvent_Event()		((EventMaskRefType)	(OS_errorStatus.parameter[1]))
#define OSError_GetAlarm_AlarmID()		((AlarmType)		(OS_errorStatus.parameter[0]))
#define OSError_GetAlarm_Tick()			((TickRefType)		(OS_errorStatus.parameter[1]))
#define OSError_GetAlarmBase_AlarmID()	((AlarmType)		(OS_errorStatus.parameter[0]))
#define OSError_GetAlarmBase_Info()		((AlarmBaseRefType)	(OS_errorStatus.parameter[1]))
#define OSError_SetRelAlarm_AlarmID()	((AlarmType)		(OS_errorStatus.parameter[0]))
#define OSError_SetRelAlarm_increment()	((TickType)			(OS_errorStatus.parameter[1]))
#define OSError_SetRelAlarm_cycle()		((TickType)			(OS_errorStatus.parameter[2]))
#define OSError_SetAbsAlarm_AlarmID()	((AlarmType)		(OS_errorStatus.parameter[0]))
#define OSError_SetAbsAlarm_start()		((TickType)			(OS_errorStatus.parameter[1]))
#define OSError_SetAbsAlarm_cycle()		((TickType)			(OS_errorStatus.parameter[2]))
#define OSError_CancelAlarm_AlarmID()	((AlarmType)		(OS_errorStatus.parameter[0]))
#define OSError_StartOS_Mode()			((AppModeType)		(OS_errorStatus.parameter[0]))
#define OSError_ShutdownOS_Error()		((StatusType)		(OS_errorStatus.parameter[0]))
/* CHECK: RESTORE
*/

#ifndef OS_ASM
/* Library routines to map some OS system calls onto OSEK/VDX APIs
*/
StatusType OS_GetTaskState(TaskType, TaskStateRefType);

#endif

#ifdef __cplusplus
}
#endif

#include <Os_callouts.h>

#endif

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
