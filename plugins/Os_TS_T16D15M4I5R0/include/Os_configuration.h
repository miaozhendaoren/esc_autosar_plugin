/* Os_configuration.h
 *
 * This file provides a defined interface from the kernel and from user
 * code to the generated include file.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_configuration.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.6 (required)
 * #undef shall not be used.
 *
 * Reason:
 * A macro is undefined to simplify matters for the build system and ultimately for the end user.
 */

#ifndef __OS_CONFIGURATION_H
#define __OS_CONFIGURATION_H

/* Include the generated config. header
*/
#include <Os_config.h>

#ifndef OS_IOC_ENABLED
#define OS_IOC_ENABLED 0
#endif

#ifndef OS_ASM
#if (OS_IOC_ENABLED==1)
#include <Ioc_gen.h>
#endif
#endif

/* Include patch header file if indicated.
 * We set the macro OS_PATCH_CONFIG to 1 to indicate to the included file that we
 * are patching the configuration.
 * Afterwards, we undefine the macro; this is a MISRA violation. The alternative
 * would be to have separate patch files and separate control macros for including
 * them, which would complicate matters for the build system and therefore for the user.
*/
/* Deviation MISRA-1 <START> */
#ifdef OS_INCLUDE_PATCHES

#define OS_PATCH_CONFIG	1
#include <Os_config_patches.h>
#undef OS_PATCH_CONFIG

#endif
/* Deviation MISRA-1 <STOP> */
#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
