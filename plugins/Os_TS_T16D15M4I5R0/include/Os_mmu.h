/* Os_mmu.h
 *
 * Header file for MMU support. Includes the architecture-specific MMU file first,
 * then defines some architecture-independent MMU structures etc.
 *
 * The Makefiles must ensure that the OS_ARCH and OS_CPU macros are
 * defined appropriately on the command line.
 *
 * If you get an error "file not found" it probably means that the architecture doesn't
 * provide MMU support.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_mmu.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef __OS_MMU_H
#define __OS_MMU_H

#include <Os_defs.h>
#include <Os_types.h>

/* Include the architecture's MMU file.
 * Most architectures don't have an MMU, and of those that do only very few
 * are actually used. But the list of architectures here should be complete anyway
*/
#if (OS_ARCH==OS_PA)
#include <PA/Os_mmu_PA.h>
#elif (OS_ARCH==OS_TRICORE)
#include <TRICORE/Os_mmu_TRICORE.h>
#elif (OS_ARCH==OS_RH850)
#include <RH850/Os_mmu_RH850.h>
#elif (OS_ARCH==OS_V850)
#include <V850/Os_mmu_V850.h>
#elif (OS_ARCH==OS_NEWARCH)
#include <NEWARCH/Os_mmu_NEWARCH.h>
#elif (OS_ARCH==OS_PIKEOS)
#include <PIKEOS/Os_mmu_PIKEOS.h>
#elif (OS_ARCH==OS_S12X)
#include <S12X/Os_mmu_S12X.h>
#elif (OS_ARCH==OS_XC2000)
#include <XC2000/Os_mmu_XC2000.h>
#elif (OS_ARCH==OS_EASYCAN)
#include <EASYCAN/Os_mmu_EASYCAN.h>
#elif (OS_ARCH==OS_MB91)
#include <MB91/Os_mmu_MB91.h>
#elif (OS_ARCH==OS_R32C)
#include <R32C/Os_mmu_R32C.h>
#elif (OS_ARCH==OS_WINDOWS)
#include <WINDOWS/Os_mmu_WINDOWS.h>
#elif (OS_ARCH==OS_SH2)
#include <SH2/Os_mmu_SH2.h>
#elif (OS_ARCH==OS_SH4)
#include <SH4/Os_mmu_SH4.h>
#elif (OS_ARCH==OS_ARM)
#include <ARM/Os_mmu_ARM.h>
#elif (OS_ARCH==OS_LINUX)
#include <LINUX/Os_mmu_LINUX.h>
#else
#error "Unsupported OS_ARCH defined. Check your Makefiles!"
#endif

/* Structure to hold a page mapping
*/
#ifndef OS_ASM
typedef struct os_pagemap_s os_pagemap_t;

struct os_pagemap_s
{
	os_address_t base;		/* Base address of hunk */
	os_pagesize_t size;		/* Size of hunk */
	os_uint32_t flags;		/* Accessibility etc. (see below) */
};
#endif

/* Bit definitions in the 'flags' field. Some of these are PowerPC-specific - we'll perhaps find
 * better names and/or more flags as other architectures with MMU come along.
*/
#define OS_MAP_PERSISTENT	0x80000000		/* Mapping is persistent (doesn't get chucked out on task switch) */
#define OS_MAP_WRITETHROUGH	0x00100000		/* Pages is write-through */
#define OS_MAP_NONCACHED	0x00080000		/* Page is not cacheable */
#define OS_MAP_COHERENT		0x00040000		/* Memory coherence is required */
#define OS_MAP_GUARDED		0x00020000		/* Guarded (no speculation) */
#define OS_MAP_LITTLEENDIAN	0x00010000		/* Little-endian */
#define OS_MEMPERM_SX		0x00000001		/* Can execute in supervisor mode */
#define OS_MEMPERM_SR		0x00000002		/* Can read in supervisor mode */
#define OS_MEMPERM_SW		0x00000004		/* Can write in supervisor mode */
#define OS_MEMPERM_UX		0x00000010		/* Can execute in user mode */
#define OS_MEMPERM_UR		0x00000020		/* Can read in user mode */
#define OS_MEMPERM_UW		0x00000040		/* Can write in user mode */

/* On machines with MMU support there's a concept of MMU ownership to reduce the amount of switching page
 * tables back and forth. Furthermore, it's also possible that trusted tasks and ISRs need to have their
 * page tables reprogrammed (see the TLB entry reduction code). So the decision whether to restore
 * the interrupted task/ISR's protection is left to the MMU functions.
*/
#define OS_QueryRestoreProtection(isr)	(1)

/* Global data and constants related to MMU support:
 *
 *	- Base, size and limit for .text, RAM and .rodata. Used in CheckXxxMemoryAccess()
 *	- Base and size for heap.
 *	- MMU management variables and constants
*/
#ifndef OS_ASM
extern const os_address_t OS_textBase;
extern const os_size_t OS_textSize;
extern const os_address_t OS_textLimit;

extern const os_address_t OS_ramBase;
extern const os_size_t OS_ramSize;
extern const os_address_t OS_ramLimit;

extern const os_address_t OS_constBase;
extern const os_size_t OS_constSize;
extern const os_address_t OS_constLimit;

extern const os_address_t OS_heapBase;
extern const os_size_t OS_heapSize;

extern os_int_t OS_nFreePageMaps;
extern os_int_t OS_maxObjectMaps;
extern os_int_t OS_nTrustedPageMaps;
extern os_archpagemap_t *OS_trustedPageMaps;
extern const os_size_t OS_pageSize[];

extern os_objecttype_t OS_mmuOwnerType;
extern os_objecttype_t OS_mmuOwnerId;
#endif

/* Functions
*/
#ifndef OS_ASM
/* Architecture-independent support functions
*/
os_result_t OS_MmuTaskMapper(os_taskid_t);
os_result_t OS_MmuIsrMapper(os_isrid_t);
os_result_t OS_MmuAppMapper(os_applicationid_t);

os_int_t OS_MmuOptimise(os_pagemap_t *, os_int_t);
void OS_MmuMapSort(os_pagemap_t *, os_int_t);
void OS_MmuPagify(os_pagemap_t *, os_int_t);
os_int_t OS_MmuMapMerge(os_pagemap_t *, os_int_t);
os_int_t OS_MmuPadRamReadonly(os_pagemap_t *, os_int_t);
os_int_t OS_MmuMapPages(os_pagemap_t *, os_int_t, os_pagemap_t*, os_int_t);
os_int_t OS_MmuPageMap(os_address_t, os_size_t, os_uint32_t, os_pagemap_t*);
os_pagesize_t OS_MmuBestPageSize(os_address_t);

os_archpagemap_t *OS_ArchMmuPageMapTranslate(os_pagemap_t *, os_int_t);

#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
