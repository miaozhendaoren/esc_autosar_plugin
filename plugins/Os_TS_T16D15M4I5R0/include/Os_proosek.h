/* Os_proosek.h - ProOSEK-4.x Compatible Features
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_proosek.h 17683 2014-02-11 14:43:30Z tojo2507 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.13 (advisory)
 * The # and ## preprocessor operators should not be used.
 *
 * Reason:
 * ## operator must be used to generate an identifier as demanded by specification.
 */

#ifndef __OS_PROOSEK_H
#define __OS_PROOSEK_H

#include <Os_osek.h>
#include <Os_removeapi.h>


#ifdef __cplusplus
extern "C" {
#endif

/*!
 * ProOSEK 4.x API
 *
 * Additional data types for ProOSEK 4.x compatibility
*/
#define IsrType		os_isrid_t

/*!
 * ProOSEK 4.x API
 *
 * These macros define the additional features of ProOSEK 4.x in
 * terms of the OS system-call stubs and library calls
 *
 * Note: AdvanceCounter and IAdvanceCounter both map to the same
 * OS API. This is not an error.
 *
 * CHECK: SAVE
 * CHECK: RULE 402 OFF (Legacy API macros)
*/
#ifndef OS_EXCLUDE_GETSTACKINFO
#define stackCheck()				OS_StackCheck()
#define getUsedTaskStack(t)			OS_GetUsedTaskStack(t)
#define getUnusedTaskStack(t)		OS_GetUnusedTaskStack(t)
#define getUsedIsrStack()			OS_GetUsedIsrStack()
#define getUnusedIsrStack()			OS_GetUnusedIsrStack()
#endif

#ifndef OS_EXCLUDE_INCREMENTCOUNTER
#define AdvanceCounter(c)			OS_UserIncrementCounter(c)
#define IAdvanceCounter(c)			OS_UserIncrementCounter(c)
#endif

/* CHECK: RESTORE
*/

/*!
 * ISR1
 *
 * This macro can be used to define category 1 interrupt functions
 * in an architecture-independent way.
 *
 * NOTE: Defining this macro in terms of the standard OSEK ISR() macro
 * does not work. The compiler expands the parameter before expanding
 * ISR(), and the result is OS_ISR_n (where n is the numeric ID of
 * the ISR from os.h
*/
/* Deviation MISRA-1 */
#define ISR1(x)				OS_EXTERN_C void OS_ISR_##x(void)


#ifndef OS_ASM
/* Library routines to map some OS system calls onto ProOSEK APIs
*/
os_size_t OS_GetUsedTaskStack(os_taskid_t t);
os_size_t OS_GetUnusedTaskStack(os_taskid_t t);
os_size_t OS_GetUsedIsrStack(void);
os_size_t OS_GetUnusedIsrStack(void);
#endif

#ifdef __cplusplus
}
#endif

#include <Os_callouts.h>

#endif

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
