/* Os_userkernel.h
 *
 * This file defines the bits of the kernel API that are needed for the user-mode system calls.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_userkernel.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef __OS_USERKERNEL_H
#define __OS_USERKERNEL_H

#include <Os_defs.h>

#ifndef OS_ASM
/* The inKernel flag (or counter)
*/
extern os_uint8_t OS_inKernel;
#endif

#if (OS_KERNEL_TYPE==OS_FUNCTION_CALL)

/* Kernel entry - just increment the inKernel counter
*/
#define OS_EnterKernel()	(OS_inKernel++)

/* Kernel exit is more complex, so it's a function by default
*/
#ifndef OS_ArchLeaveKernel
#define OS_ArchLeaveKernel(e)	OS_LeaveKernel(e)
#endif

#ifndef OS_ASM
os_unsigned_t OS_LeaveKernel(os_unsigned_t);

#ifdef OS_ARCH_LEAVEKERNEL_DECL
OS_ARCH_LEAVEKERNEL_DECL
#endif

#endif

/* A simulated "system-call" means simply entering the kernel, calling the kernel routine,
 * then leaving the kernel. OS_ArchLeaveKernel() returns the value passed as its parameter, which
 * is the return value from the kernel routine. This only works for non-void kernel routines,
 * so there's a special OS_VOIDCALL macro for void routines.
 *
 * it would be much easier if all kernel routines returned something.
*/
#define OS_CALL(typ, call)		((typ)OS_ArchLeaveKernel((OS_EnterKernel(), (os_unsigned_t)(call))))

#define OS_VOIDCALL(call) \
	do {									\
		OS_EnterKernel();					\
		(call);								\
		(void)OS_ArchLeaveKernel(OS_E_OK);	\
	} while(0);

#endif

#ifndef OS_ASM

/* Kernel functions for system services
*/
#if (OS_KERNEL_TYPE != OS_MICROKERNEL)
void OS_KernReturnFromCall(void);
os_result_t OS_KernTerminateTask(void);
os_result_t OS_KernChainTask(os_taskid_t);
os_result_t OS_KernActivateTask(os_taskid_t);
os_result_t OS_KernSchedule(void);
os_result_t OS_KernGetTaskId(os_taskid_t*);
os_result_t OS_KernGetTaskState(os_taskid_t, os_taskid_t*);
os_result_t OS_KernGetResource(os_resourceid_t);
os_result_t OS_KernReleaseResource(os_resourceid_t);
os_result_t OS_KernSetEvent(os_taskid_t, os_eventmask_t);
os_result_t OS_KernClearEvent(os_eventmask_t);
os_result_t OS_KernWaitEvent(os_eventmask_t);
os_result_t OS_KernGetStackInfo(os_taskorisr_t, os_stackinfo_t*);
void OS_KernSuspendInterrupts(os_intlocktype_t);
void OS_KernResumeInterrupts(os_intlocktype_t);
os_applicationid_t OS_KernGetApplicationId(void);
os_applicationid_t OS_KernCheckObjectOwnership(os_objecttype_t, os_objectid_t);
os_boolean_t OS_KernCheckObjectAccess(os_applicationid_t, os_objecttype_t, os_objectid_t);
os_isrid_t OS_KernGetIsrId(void);
os_memoryaccess_t OS_KernCheckIsrMemoryAccess(os_isrid_t, const void*, os_size_t);
os_memoryaccess_t OS_KernCheckTaskMemoryAccess(os_taskid_t, const void*, os_size_t);
os_result_t OS_KernCallTrustedFunction(os_functionid_t, void*);
os_result_t OS_KernDisableInterruptSource(os_isrid_t);
os_result_t OS_KernEnableInterruptSource(os_isrid_t);
#endif

os_result_t OS_KernUnknownSyscall(void);
os_result_t OS_KernSetRelAlarm(os_alarmid_t, os_tick_t, os_tick_t);
os_result_t OS_KernSetAbsAlarm(os_alarmid_t, os_tick_t, os_tick_t);
os_result_t OS_KernCancelAlarm(os_alarmid_t);
os_result_t OS_KernGetAlarm(os_alarmid_t, os_tick_t*);
os_result_t OS_KernGetAlarmBase(os_alarmid_t, os_alarmbase_t*);
os_result_t OS_KernGetEvent(os_taskid_t, os_eventmask_t*);
void OS_KernStartOs(os_appmodeid_t);
void OS_KernShutdownOs(os_result_t);
os_result_t OS_KernIncrementCounter(os_counterid_t);
os_result_t OS_KernTerminateApplication(os_restart_t restart);
os_appmodeid_t OS_KernGetActiveApplicationMode(void);
os_result_t OS_KernStartScheduleTable(os_scheduleid_t, os_tick_t, os_boolean_t);
os_result_t OS_KernStartScheduleTableSynchron(os_scheduleid_t);
os_result_t OS_KernChainScheduleTable(os_scheduleid_t, os_scheduleid_t);
os_result_t OS_KernStopScheduleTable(os_scheduleid_t);
os_result_t OS_KernSyncScheduleTable(os_scheduleid_t, os_tick_t);
os_result_t OS_KernGetScheduleTableStatus(os_scheduleid_t, os_uint8_t*);
os_result_t OS_KernSetScheduleTableAsync(os_scheduleid_t);
os_result_t OS_KernGetCounterValue(os_counterid_t, os_tick_t*);
os_result_t OS_KernGetElapsedCounterValue(os_counterid_t, os_tick_t*, os_tick_t*);

os_result_t OS_KernIocWrite(os_uint32_t, const void * const []);
os_result_t OS_KernIocRead(os_uint32_t, void * const []);
os_result_t OS_KernIocSend(os_uint32_t, const void * const []);
os_result_t OS_KernIocReceive(os_uint32_t, void * const []);
os_result_t OS_KernIocEmptyQueue(os_uint32_t);

#endif

#endif
/* Editor settings: DO NOT DELETE
 * vi:set ts=4:
*/
