/* Os_kernel.h
 *
 * Kernel include file. This file includes all the files that define the
 * kernel data structures.
 *
 * The common data structures, macros etc are no longer defined directly in
 * this file because of the MISRA complaint when the architecture-
 * specific prototypes file is included at the end. The kernel structures
 * can now be found in Os_kernel_task.h, Os_kernel_alarm.h and Os_kernel_app.h
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_kernel.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef __OS_KERNEL_H
#define __OS_KERNEL_H

#include <Os_defs.h>

/* Do not use the CLZ queueing algorithm by default if not externally defined
*/
#ifndef OS_USE_CLZ_QUEUE_ALGORITHM
#define OS_USE_CLZ_QUEUE_ALGORITHM		0
#endif

/* Include Exclude definitions for optimization of library build */
#include <Os_optimise.h>

/* Include the architecture-specific definitions first.
*/
#include <Os_types.h>

#if OS_KERNEL_TYPE == OS_MICROKERNEL

#include <Os_microkernel.h>
#include <Os_arch.h>
#include <Os_kernel_alarm.h>

#else

#include <Os_error.h>
#include <Os_arch.h>
#include <Os_kernel_app.h>
#include <Os_kernel_alarm.h>
#include <Os_kernel_task.h>

#endif

#include <Os_userkernel.h>

#if OS_KERNEL_TYPE != OS_MICROKERNEL
/* These are included last because the function prototypes that are in
 * them will need the types that are defined in this file.
*/
#include <Os_proto_arch.h>
#include <Os_callouts.h>
#endif

/* Include timestamp definitions
*/
#include <Os_timestamp.h>

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
