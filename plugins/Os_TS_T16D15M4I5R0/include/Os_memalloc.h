/* Os_memalloc.h
 *
 * This file defines the structures, constants and function prototypes
 * for the kernel memory-allocation system.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_memalloc.h 17649 2014-02-06 15:35:01Z tojo2507 $
*/
#ifndef __OS_MEMALLOC_H
#define __OS_MEMALLOC_H

#include <Os_types.h>

/* OS_FreeMem is not implemented
*/
#define OS_HAVE_FREEMEM	0

/* OS_MEMLUMP - Minimum allocation unit.
 *
 * Memory blocks are allocated in multiples of this.
 *  - Must be a power of 2
 *  - Must be at least sizeof(os_memblock_t)
 *  - Might be advantageous to use larger value than the minimum (e.g. a cache line).
 *
 * We use a default of 16 here which is best for the PowerPC. The architecture can define it otherwise
 * in Os_types_$(ARCH).h if necessary
*/
#ifndef OS_MEMLUMP
#define OS_MEMLUMP			OS_U(16)
#define OS_MEMLUMP_SHIFT	4
#endif

#define OS_MemBytesToLumps(b)	( ( ((b) + OS_MEMLUMP) - OS_U(1) ) >> OS_MEMLUMP_SHIFT )

/* Memblock structure is used as a header for blocks of memory in the free list.
 * Memblocks always begin on a OS_MEMLUMP-aligned boundary.
 * Memlump structure is used for address calculations.
*/
#ifndef OS_ASM
typedef struct os_memblock_s os_memblock_t;

struct os_memblock_s
{
	os_size_t size;				/* Size of this block, in OS_MEMLUMP */
	os_memblock_t *next;
};

typedef struct os_memlump_s os_memlump_t;

struct os_memlump_s
{
	os_char_t lump[OS_MEMLUMP];
};

extern os_memblock_t OS_memHead;			/* Head of the free list. Its size is 0 */

void *OS_GetMem(os_size_t);					/* Desired size in BYTES! */
os_result_t OS_InitMem(void *, os_size_t);	/* Size in BYTES */

#if OS_HAVE_FREEMEM
os_result_t OS_FreeMem(void *, os_size_t);	/* Size in BYTES, block must be aligned to OS_MEMLUMP */
#else
#define OS_FreeMem(mem, sz)		OS_E_NOFUNC	/* Dummy OS_FreeMem() - does nothing! */
#endif

#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
