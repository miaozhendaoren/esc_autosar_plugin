/* Os_error.h
 *
 * Error handling.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_error.h 17665 2014-02-07 15:15:20Z tojo2507 $
*/
#ifndef __OS_ERROR_H
#define __OS_ERROR_H

#include <Os_types.h>

/*!
 * OS_SID_xxx
 *
 * Service IDs for all system services and other areas that can generate
 * errors.
 *
 * os_serviceid_t is the data type that holds one of these.
 *
 * These OS_SID_ macros are parsed by errdb-to-c.pl, so their values
 * must be in one of two formats:
 *  - a literal decimal number, without parenthesis or type suffix
 *  - a simple expression of the form (sid+number), where sid is a
 *    previously-defined OS_SID_ macro, and number is a literal
 *    decimal number, without parenthesis or type suffix
*/
/* AUTOSAR-defined service ids */
#define OS_SID_GetApplicationId				0
#define OS_SID_GetIsrId						1
#define OS_SID_CallTrustedFunction			2
#define OS_SID_CheckIsrMemoryAccess			3
#define OS_SID_CheckTaskMemoryAccess		4
#define OS_SID_CheckObjectAccess			5
#define OS_SID_CheckObjectOwnership			6
#define OS_SID_StartScheduleTableRel		7
#define OS_SID_StartScheduleTableAbs		8
#define OS_SID_StopScheduleTable			9
#define OS_SID_ChainScheduleTable			10
#define OS_SID_StartScheduleTableSynchron	11
#define OS_SID_SyncScheduleTable			12
#define OS_SID_SetScheduleTableAsync		13
#define OS_SID_GetScheduleTableStatus		14
#define OS_SID_IncrementCounter				15
#define OS_SID_GetCounterValue				16
#define OS_SID_GetElapsedCounterValue		17
#define OS_SID_TerminateApplication			18
#define OS_SID_AllowAccess					19
#define OS_SID_GetApplicationState			20
/* OSEK API and others */
#define OS_SID_UnknownSyscall				21
#define OS_SID_ActivateTask					22
#define OS_SID_TerminateTask				23
#define OS_SID_ChainTask					24
#define OS_SID_Schedule						25
#define OS_SID_GetTaskId					26
#define OS_SID_GetTaskState					27
#define OS_SID_SuspendInterrupts			28
#define OS_SID_ResumeInterrupts				29
#define OS_SID_GetResource					30
#define OS_SID_ReleaseResource				31
#define OS_SID_SetEvent						32
#define OS_SID_ClearEvent					33
#define OS_SID_GetEvent						34
#define OS_SID_WaitEvent					35
#define OS_SID_GetAlarmBase					36
#define OS_SID_GetAlarm						37
#define OS_SID_SetRelAlarm					38
#define OS_SID_SetAbsAlarm					39
#define OS_SID_CancelAlarm					40
#define OS_SID_GetActiveApplicationMode		41
#define OS_SID_StartOs						42
#define OS_SID_ShutdownOs					43
#define OS_SID_GetStackInfo					44
#define OS_SID_DisableInterruptSource		45
#define OS_SID_EnableInterruptSource		46
/* Insert additional system services here and adjust OS_SID_XXX to match */
#define OS_SID_XXX							47
#define OS_SID_Dispatch						(OS_SID_XXX+0)
#define OS_SID_TrapHandler					(OS_SID_XXX+1)
#define OS_SID_IsrHandler					(OS_SID_XXX+2)
#define OS_SID_RunSchedule					(OS_SID_XXX+3)
#define OS_SID_KillAlarm					(OS_SID_XXX+4)
#define OS_SID_TaskReturn					(OS_SID_XXX+5)
#define OS_SID_HookHandler					(OS_SID_XXX+6)
#define OS_SID_ArchTrapHandler				(OS_SID_XXX+7)
#define OS_SID_MemoryManagement				(OS_SID_XXX+8)
/* Insert additional internal functions here and adjust OS_N_SIDS to match */
#define OS_N_SIDS							(OS_SID_XXX+9)

/*!
 * OS_ERROR_xxx
 *
 * Error conditions. Multiple services can use the same error
 * condition, but any one service must use a unique error condition
 * for each error detected.
 *
 * os_error_t is the data type that holds one of these.
 * os_errorresult_t is the data type that holds one of these but must also be capable of holding the same range of
 *                  values as os_result_t because the code might get translated.
*/
#define OS_ERROR_NoError					0u	/* Same as UnknownError */
#define OS_ERROR_UnknownError				0u
#define OS_ERROR_UnknownSystemCall			1u
#define OS_ERROR_InvalidTaskId				2u
#define OS_ERROR_InvalidTaskState			3u
#define OS_ERROR_Quarantined				4u
#define OS_ERROR_MaxActivations				5u
#define OS_ERROR_WriteProtect				6u
#define OS_ERROR_ReadProtect				7u
#define OS_ERROR_ExecuteProtect				8u
#define OS_ERROR_InvalidAlarmId				9u
#define OS_ERROR_InvalidAlarmState			10u
#define OS_ERROR_AlarmNotInUse				11u
#define OS_ERROR_WrongContext				12u
#define OS_ERROR_HoldsResource				13u
#define OS_ERROR_NoEvents					14u
#define OS_ERROR_TaskNotExtended			15u
#define OS_ERROR_TaskNotInQueue				16u
#define OS_ERROR_InvalidCounterId			17u
#define OS_ERROR_CorruptAlarmList			18u
#define OS_ERROR_ParameterOutOfRange		19u
#define OS_ERROR_AlarmInUse					20u
#define OS_ERROR_AlreadyStarted				21u
#define OS_ERROR_InvalidStartMode			22u
#define OS_ERROR_AlarmNotInQueue			23u
#define OS_ERROR_InvalidResourceId			24u
#define OS_ERROR_ResourceInUse				25u
#define OS_ERROR_ResourcePriorityError		26u
#define OS_ERROR_ResourceNestingError		27u
#define OS_ERROR_TaskSuspended				28u
#define OS_ERROR_NestingUnderflow			29u
#define OS_ERROR_NestingOverflow			30u
#define OS_ERROR_NonfatalException			31u
#define OS_ERROR_FatalException				32u
#define OS_ERROR_UnhandledNmi				33u
#define OS_ERROR_UnknownInterrupt			34u
#define OS_ERROR_TaskTimeBudgetExceeded		35u
#define OS_ERROR_IsrTimeBudgetExceeded		36u
#define OS_ERROR_UnknownTimeBudgetExceeded	37u
#define OS_ERROR_Permission					38u
#define OS_ERROR_ImplicitSyncStartRel		39u
#define OS_ERROR_CounterIsHw				40u
#define OS_ERROR_InvalidScheduleId			41u
#define OS_ERROR_NotRunning					42u
#define OS_ERROR_NotStopped					43u
#define OS_ERROR_AlreadyChained				44u
#define OS_ERROR_InvalidObjectType			45u
#define OS_ERROR_InvalidObjectId			46u
#define OS_ERROR_InvalidApplicationId		47u
#define OS_ERROR_InvalidIsrId				48u
#define OS_ERROR_InvalidMemoryRegion		49u
#define OS_ERROR_NotChained					50u
#define OS_ERROR_InvalidFunctionId			51u
#define OS_ERROR_NotSyncable				52u
#define OS_ERROR_NotImplemented				53u
#define OS_ERROR_StackError					54u
#define OS_ERROR_RateLimitExceeded			55u
#define OS_ERROR_InterruptDisabled			56u
#define OS_ERROR_ReturnFromTask				57u
#define OS_ERROR_InsufficientStack			58u
#define OS_ERROR_WatchdogTimeout			59u
#define OS_ERROR_PllLockLost				60u
#define OS_ERROR_ArithmeticTrap				61u
#define OS_ERROR_MemoryProtection			62u
#define OS_ERROR_NotTrusted					63u
#define OS_ERROR_TaskResLockTimeExceeded	64u
#define OS_ERROR_IsrResLockTimeExceeded		65u
#define OS_ERROR_TaskIntLockTimeExceeded	66u
#define OS_ERROR_IsrIntLockTimeExceeded		67u
#define OS_ERROR_IncrementZero				68u
#define OS_ERROR_DifferentCounters			69u
#define OS_ERROR_ScheduleTableNotIdle		70u
#define OS_ERROR_InvalidRestartOption		71u
#define OS_ERROR_TaskAggregateTimeExceeded	72u
#define OS_ERROR_IncorrectKernelNesting		73u
#define OS_ERROR_KernelStackOverflow		74u
#define OS_ERROR_TaskStackOverflow			75u
#define OS_ERROR_IntEException				76u
#define OS_ERROR_ExceptionInKernel			77u
#define OS_ERROR_SysReq						78u
#define OS_ERROR_StackOverflow				79u
#define OS_ERROR_StackUnderflow				80u
#define OS_ERROR_SoftBreak					81u
#define OS_ERROR_UndefinedOpcode			82u
#define OS_ERROR_AccessError				83u
#define OS_ERROR_ProtectionFault			84u
#define OS_ERROR_IllegalOperandAccess		85u
#define OS_ERROR_UnknownException			86u

#define OS_ERROR_UndefinedInstruction		87u
#define OS_ERROR_Overflow					88u
#define OS_ERROR_BrkInstruction				89u
#define OS_ERROR_WdgTimer					90u
#define OS_ERROR_NMI						91u

#define OS_ERROR_RegisterBank				92u	/* register bank overflow */
#define OS_ERROR_DebugInterface				93u

#define OS_ERROR_InsufficientPageMaps		94u
#define OS_ERROR_InsufficientHeap			95u


#define OS_ERROR_TLB_multiple_hit					96u
#define OS_ERROR_Userbreak							97u
#define OS_ERROR_InstructionAddressError			98u
#define OS_ERROR_InstructionTlbMiss					99u
#define OS_ERROR_TlbProtectionViolation				100u
#define OS_ERROR_GeneralIllegalInstruction			101u
#define OS_ERROR_SlotIllegalInstruction				102u
#define OS_ERROR_GeneralFPUDisable					103u
#define OS_ERROR_SlotFPUDisable						104u
#define OS_ERROR_DataAddressErrorRead				105u
#define OS_ERROR_DataAddressErrorWrite				106u
#define OS_ERROR_DataTlbMissRead					107u
#define OS_ERROR_DataTlbMissWrite					108u
#define OS_ERROR_DataTlbReadProtViolation			109u
#define OS_ERROR_DataTlbWriteProtViolation			110u
#define OS_ERROR_FpuException						111u
#define OS_ERROR_InitialPageWrite					112u
#define OS_ERROR_UnconditionalTrap					113u
#define OS_ERROR_PrefetchAbort						114u
#define OS_ERROR_DataAbort							115u
#define OS_ERROR_IllegalSupervisorCall				116u
#define OS_ERROR_IllegalInterrupt					117u

/* error codes for Cortex-M exceptions */
#define OS_ERROR_NonMaskableInterrupt				118u
#define OS_ERROR_HardFault							119u
#define OS_ERROR_MemoryManagement					120u
#define OS_ERROR_BusFault							121u
#define OS_ERROR_UsageFault							122u
#define OS_ERROR_ReservedException_1				123u
#define OS_ERROR_ReservedException_2				124u
#define OS_ERROR_ReservedException_3				125u
#define OS_ERROR_ReservedException_4				126u
#define OS_ERROR_SupervisorCall						127u
#define OS_ERROR_DebugMonitor						128u
#define OS_ERROR_ReservedException_5				129u
#define OS_ERROR_PendingSupervisorCall				130u
#define OS_ERROR_SystemTick							131u

/* error codes for DSPIC traps */
#define OS_ERROR_OscillatorFailureTrap				132u
#define OS_ERROR_StackErrorTrap						133u
#define OS_ERROR_AddressErrorTrap					134u
#define OS_ERROR_MathErrorTrap						135u
#define OS_ERROR_DMACErrorTrap						136u
#define OS_ERROR_GenericHardTrap					137u
#define OS_ERROR_GenericSoftTrap					138u
#define OS_ERROR_UnknownTrap						139u

#define OS_ERROR_SysErr								140u
#define OS_ERROR_HVTrap								141u
#define OS_ERROR_FETrap								142u
#define OS_ERROR_Trap								143u
#define OS_ERROR_ReservedInstruction				144u
#define OS_ERROR_CoprocessorUnusable				145u
#define OS_ERROR_PrivilegedInstruction				146u
#define OS_ERROR_MisalignedAccess					147u
#define OS_ERROR_FEINT								148u


#ifndef OS_ASM
typedef os_uint8_t os_error_t;
typedef os_uint16_t os_errorresult_t;
#endif

/*!
 * OS_ACTION_T
 *
 * Error actions
 *
 * os_erroraction_t is the data type that holds one of these
*/
#define OS_ACTION_MASK			OS_U(0x0f)	/* Mask for the Action field */
#define OS_ACTION_IGNORE		OS_U(0x00)	/* Ignore totally - return OK */
#define OS_ACTION_RETURN		OS_U(0x01)	/* Return result to caller */
#define OS_ACTION_KILL			OS_U(0x02)	/* Kill task or ISR */
#define OS_ACTION_QUARANTINE	OS_U(0x03)	/* Quarantine task or ISR */
#define OS_ACTION_QUARANTINEAPP	OS_U(0x04)	/* Quarantine Application */
#define OS_ACTION_RESTART		OS_U(0x05)	/* Kill and restart application */
#define OS_ACTION_SHUTDOWN		OS_U(0x06)	/* Shut down OS */
#define OS_ACTION_DONOTHING		OS_U(0xfe)	/* "Do nothing". Only for use as return value from ProtectionHook */
#define OS_ACTION_DEFAULT		OS_U(0xff)	/* Use default action. Only for use as return value from ProtectionHook */

#define OS_ActionIsValid(act)	((act)<=OS_ACTION_SHUTDOWN)

#define OS_ACTION_HOOK				OS_U(0xe0)	/* Mask for all autosar hooks */
#define OS_ACTION_HOOK_EB			OS_U(0xf0)	/* Mask for all hooks, including EB extensions */
#define OS_ACTION_NOHOOK			OS_U(0x00)	/* Use no hooks ;-) */
#define OS_ACTION_PROTECTIONHOOK	OS_U(0x80)	/* Call ProtectionHook */
#define OS_ACTION_ERRORHOOK			OS_U(0x40)	/* Call global ErrorHook */
#define OS_ACTION_ERRORHOOK_APP		OS_U(0x20)	/* Call application's ErrorHook */
#define OS_ACTION_ERRORHOOK_EB		OS_U(0x10)	/* Call global ErrorHook, only when EB extension is configured */

#ifndef OS_ASM
typedef os_uint8_t os_erroraction_t;

/* Configuration constant - which error hooks really get called, etc.
*/
extern const os_erroraction_t OS_hookSelection;
#endif

/*!
 * os_errorentry_t
 *
 * This is the data structure that drives the OS error handling
 * mechanism. The fields are in a strange order to optimise the
 * size of the struct, since we'll have rather a lot of them.
*/
#ifndef OS_ASM
typedef struct os_errorentry_s os_errorentry_t;

struct os_errorentry_s
{
	os_result_t			result;		/* result code for hooks and return */
	os_error_t			condition;	/* error condition */
	os_erroraction_t	action;		/* action to take */
};

#endif

/*!
 * os_errorstatus_t
 *
 * This structure is used by the kernel to store all the information
 * about the most recent error. The error hook function can use
 * OS_GetErrorStatus to copy its contents into a user-supplied
 * area.
*/
#define OS_MAXPARAM		3		/* Largest no. of parameters to a service */

#ifndef OS_ASM
typedef struct os_errorstatus_s os_errorstatus_t;

struct os_errorstatus_s
{
	os_paramtype_t parameter[OS_MAXPARAM];		/* Parameters */
	os_result_t result;			/* return value from service */
	os_erroraction_t action;	/* Action to take */
	os_uint8_t calledFrom;		/* INTASK, INCAT1 etc. */
	os_serviceid_t serviceId;	/* OS_SID_xxx */
	os_error_t errorCondition;	/* OS_ERROR_xxx */
};

extern os_errorstatus_t OS_errorStatus;

#endif

/*!
 * os_serviceinfo_t
 *
 * This structure is used when calling OS_Error to pass the service ID and
 * the associated error table to the error handler.
*/
#ifndef OS_ASM
typedef struct os_serviceinfo_s os_serviceinfo_t;

struct os_serviceinfo_s
{
	os_errorentry_t const *errorTable;
	os_serviceid_t sid;
	os_uint8_t nErrors;
};

extern const os_serviceinfo_t OS_svc_ActivateTask;
extern const os_serviceinfo_t OS_svc_IncrementCounter;
extern const os_serviceinfo_t OS_svc_CallTrustedFunction;
extern const os_serviceinfo_t OS_svc_CancelAlarm;
extern const os_serviceinfo_t OS_svc_ChainScheduleTable;
extern const os_serviceinfo_t OS_svc_ChainTask;
extern const os_serviceinfo_t OS_svc_CheckIsrMemoryAccess;
extern const os_serviceinfo_t OS_svc_CheckObjectAccess;
extern const os_serviceinfo_t OS_svc_CheckObjectOwnership;
extern const os_serviceinfo_t OS_svc_CheckTaskMemoryAccess;
extern const os_serviceinfo_t OS_svc_ClearEvent;
extern const os_serviceinfo_t OS_svc_DisableInterruptSource;
extern const os_serviceinfo_t OS_svc_Dispatch;
extern const os_serviceinfo_t OS_svc_EnableInterruptSource;
extern const os_serviceinfo_t OS_svc_GetActiveApplicationMode;
extern const os_serviceinfo_t OS_svc_GetAlarm;
extern const os_serviceinfo_t OS_svc_GetAlarmBase;
extern const os_serviceinfo_t OS_svc_GetApplicationId;
extern const os_serviceinfo_t OS_svc_GetCounterValue;
extern const os_serviceinfo_t OS_svc_GetElapsedCounterValue;
extern const os_serviceinfo_t OS_svc_GetEvent;
extern const os_serviceinfo_t OS_svc_GetIsrId;
extern const os_serviceinfo_t OS_svc_GetResource;
extern const os_serviceinfo_t OS_svc_GetScheduleTableStatus;
extern const os_serviceinfo_t OS_svc_GetStackInfo;
extern const os_serviceinfo_t OS_svc_GetTaskId;
extern const os_serviceinfo_t OS_svc_GetTaskState;
extern const os_serviceinfo_t OS_svc_HookHandler;
extern const os_serviceinfo_t OS_svc_IsrHandler;
extern const os_serviceinfo_t OS_svc_KillAlarm;
extern const os_serviceinfo_t OS_svc_MemoryManagement;
extern const os_serviceinfo_t OS_svc_ReleaseResource;
extern const os_serviceinfo_t OS_svc_ResumeInterrupts;
extern const os_serviceinfo_t OS_svc_RunSchedule;
extern const os_serviceinfo_t OS_svc_Schedule;
extern const os_serviceinfo_t OS_svc_SetAbsAlarm;
extern const os_serviceinfo_t OS_svc_SetEvent;
extern const os_serviceinfo_t OS_svc_SetRelAlarm;
extern const os_serviceinfo_t OS_svc_SetScheduleTableAsync;
extern const os_serviceinfo_t OS_svc_ShutdownOs;
extern const os_serviceinfo_t OS_svc_StartOs;
extern const os_serviceinfo_t OS_svc_StartScheduleTableRel;
extern const os_serviceinfo_t OS_svc_StartScheduleTableAbs;
extern const os_serviceinfo_t OS_svc_StartScheduleTableSynchron;
extern const os_serviceinfo_t OS_svc_StopScheduleTable;
extern const os_serviceinfo_t OS_svc_SuspendInterrupts;
extern const os_serviceinfo_t OS_svc_SyncScheduleTable;
extern const os_serviceinfo_t OS_svc_TaskReturn;
extern const os_serviceinfo_t OS_svc_TerminateApplication;
extern const os_serviceinfo_t OS_svc_TerminateTask;
extern const os_serviceinfo_t OS_svc_TrapHandler;
extern const os_serviceinfo_t OS_svc_UnknownSID;
extern const os_serviceinfo_t OS_svc_UnknownSyscall;
extern const os_serviceinfo_t OS_svc_WaitEvent;

extern const os_serviceinfo_t OS_svc_ArchTrapHandler;

#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
