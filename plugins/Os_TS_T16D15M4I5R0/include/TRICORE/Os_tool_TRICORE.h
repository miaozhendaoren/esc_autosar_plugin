/* Os_tool_TRICORE.h
 *
 * This file includes the appropriate Os_tool_TRICORE_xxx.h include file
 * depending on the chosen architecture.
 *
 * The Makefiles must ensure that the OS_ARCH and OS_CPU macros are
 * defined appropriately on the command line.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_tool_TRICORE.h 19537 2014-10-29 11:08:03Z thdr9337 $
*/
#ifndef __OS_TOOL_TRICORE_H
#define __OS_TOOL_TRICORE_H

#include <Os_defs.h>
#include <TRICORE/Os_TRICORE_cpu.h>

#ifndef EB_STATIC_CHECK

#if (OS_TOOL==OS_gnu)
#include <TRICORE/Os_tool_TRICORE_gnu.h>
#elif (OS_TOOL==OS_tasking)
#include <TRICORE/Os_tool_TRICORE_tasking.h>
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

#else /* EB_STATIC_CHECK */

/* dummy prototypes of assembler functions for static checker */
extern void OS_DEBUG_BREAK(void);
extern void OS_DSYNC(void);
extern os_uint32_t OS_MFCR(os_uint32_t);
extern void OS_MTCR(os_uint32_t, os_uint32_t);
extern void * OS_MFRA(void);
extern void * OS_MFSP(void);
extern void OS_MTSP(void *);
extern void OS_MTRA(os_uint32_t);
extern void OS_MTD4(os_uint32_t);
extern void OS_DISABLE(void);
extern void OS_ENABLE(void);
extern void OS_RSLCX(void);
extern void OS_RFJL(void);
extern void OS_RFE(void);
extern os_int_t OS_CLZ(os_uint32_t);
extern void OS_ReadbackVolatile(os_uint32_t);

#define OS_PARAM_UNUSED(p)			do {} while (0)

#endif /* EB_STATIC_CHECK */

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
