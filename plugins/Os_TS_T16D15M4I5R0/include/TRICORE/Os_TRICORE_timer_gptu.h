/* Os_TRICORE_timer_gptu.h - Tricore GPTU driver
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_TRICORE_timer_gptu.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#ifndef __OS_TRICORE_TIMER_GPTU_H
#define __OS_TRICORE_TIMER_GPTU_H

#include <Os_kernel.h>
#include <TRICORE/Os_TRICORE_cpu.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef OS_ASM
void TRICORE_GptuTimerStart(const os_hwt_t *, os_uint32_t, os_uint8_t);
void TRICORE_GptuTimerStop(const os_hwt_t *);
void TRICORE_GptuTimerReload(const os_hwt_t *, os_uint32_t);
os_uint32_t TRICORE_GptuTimerRead(const os_hwt_t *, os_uint8_t);
#endif

/*!
 * Driver macros for using GPTU as an execution-time timer.
 *
 * The macros TRICORE_ET_GPTU_MAJOR and TRICORE_ET_GPTU_MINOR can be defined
 * externally if required. The default is to use GPTU0 timer 0.
*/
#ifdef TRICORE_ET_GPTU_MAJOR
#if (TRICORE_ET_GPTU_MAJOR==0)
#ifndef gptu0
#error "This Tricore has no GPTU0: check your configuration!"
#endif
#elif (TRICORE_ET_GPTU_MAJOR==1)
#ifndef gptu1
#error "This Tricore has no GPTU1: check your configuration!"
#endif
#else
#error "Invalid value for TRICORE_ET_GPTU_MAJOR: check your configuration!"
#endif
#else
#define TRICORE_ET_GPTU_MAJOR 0
#endif

#ifdef TRICORE_ET_GPTU_MINOR
#if !((TRICORE_ET_GPTU_MINOR==0) || (TRICORE_ET_GPTU_MINOR==1))
#error "Invalid value for TRICORE_ET_GPTU_MINOR: check your configuration!"
#endif
#else
#define TRICORE_ET_GPTU_MINOR 0
#endif

#if TRICORE_ET_GPTU_MINOR == 0
#define TRICORE_ET_CMCON_MASK	OS_STM_CMCON0
#define TRICORE_ET_CMCON_VAL	(32u - 1u)
#define TRICORE_ET_STM_MASK		OS_STM_CMP0
#define TRICORE_ET_RST_FLAG		OS_STM_CMP0IRR
#define TRICORE_ET_SRN			OS_stm.stm_src0
#else
#define TRICORE_ET_CMCON_MASK	OS_STM_CMCON1
#define TRICORE_ET_CMCON_VAL	((32u - 1u)<<16)
#define TRICORE_ET_STM_MASK		OS_STM_CMP1
#define TRICORE_ET_RST_FLAG		OS_STM_CMP1IRR
#define TRICORE_ET_SRN			OS_stm.stm_src1
#endif

#define OS_InitExecTimingTimer() \
	do {		\
	} while(0)

#define OS_SetExecTimingInterrupt(ticks) \
	do {		\
	} while(0)

#define OS_ResetExecTimingInterrupt() \
	do {		\
	} while(0)

#ifdef __cplusplus
}
#endif

#endif
/* Editor settings: DO NOT DELETE!
 * vi:set ts=4:
*/
