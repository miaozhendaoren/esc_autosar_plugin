/* Os_TRICORE_cpu.h - Tricore CPU header file
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_TRICORE_cpu.h 19525 2014-10-29 09:12:57Z thdr9337 $
*/

#ifndef __OS_TRICORE_CPU_H
#define __OS_TRICORE_CPU_H

#include <TRICORE/Os_defs_TRICORE.h>

/* Include the appropriate header file for the derivative.
 *
 * CAVEAT:
 * The presence here of an include directive for a particular derivative
 * does not imply support for that derivative, nor does it imply a
 * commitment or intention to support that derivative in the future.
*/
#if (OS_CPU == OS_TC10GP)
#include <TRICORE/TC10GP/Os_tc10gp.h>
#elif (OS_CPU == OS_TC1775)
#include <TRICORE/TC1775/Os_tc1775.h>
#elif (OS_CPU == OS_TC1766)
#include <TRICORE/TC1766/Os_tc1766.h>
#elif (OS_CPU == OS_TC1796)
#include <TRICORE/TC1796/Os_tc1796.h>
#elif (OS_CPU == OS_TC1920)
#include <TRICORE/TC1920/Os_tc1920.h>
#elif (OS_CPU == OS_TC1797)
#include <TRICORE/TC1797/Os_tc1797.h>
#elif (OS_CPU == OS_TC1767)
#include <TRICORE/TC1767/Os_tc1767.h>
#elif (OS_CPU == OS_TC1387)
#include <TRICORE/TC1387/Os_tc1387.h>
#elif (OS_CPU == OS_TC1782)
#include <TRICORE/TC1782/Os_tc1782.h>
#elif (OS_CPU == OS_TC1798)
#include <TRICORE/TC1798/Os_tc1798.h>
#elif (OS_CPU == OS_TC1791)
#include <TRICORE/TC1791/Os_tc1791.h>
#elif (OS_CPU == OS_TC1793)
#include <TRICORE/TC1793/Os_tc1793.h>
#elif (OS_CPU == OS_TC27XFPGA)
#include <TRICORE/TC27XFPGA/Os_tc27xfpga.h>
#elif (OS_CPU == OS_TC2D5)
#include <TRICORE/TC2D5/Os_tc2d5.h>
#elif (OS_CPU == OS_TC277)
#include <TRICORE/TC277/Os_tc277.h>
#elif (OS_CPU == OS_TC23XL)
#include <TRICORE/TC23XL/Os_tc23xl.h>
#elif (OS_CPU == OS_TC22XL)
#include <TRICORE/TC22XL/Os_tc22xl.h>
#else
#error "OS_CPU is not properly defined. Check your makefiles!"
#endif


/* Fallbacks if a specific derivative doesn't set an expected macro */

/* fallback for arbitration cycles is the number provided by the generator */
#ifndef OS_ARBCYCLES
#define OS_ARBCYCLES OS_MINARBCYCLES
#endif

/* Fallback for start adress of physical address space */
#ifndef OS_PHYS_ADDR_SPACE_START
#define OS_PHYS_ADDR_SPACE_START	OS_U(0x80000000)
#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
