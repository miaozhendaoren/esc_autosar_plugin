/* Os_types_TC277.h - Properties and types of TC277
 *
 * This file contains the public properties and types that are TC277
 * specific.
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_types_TC277.h 18196 2014-05-22 10:42:39Z stpo8218 $
*/

#ifndef OS_TYPES_TC277_H
#define OS_TYPES_TC277_H

/* This derivative has an internal timestamp timer. */
#define OS_HAS_TB_INTERNAL 1

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
