/* Os_TRICORE_timer_stm.h - Tricore STM driver
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_TRICORE_timer_stm.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#ifndef __OS_TRICORE_TIMER_STM_H
#define __OS_TRICORE_TIMER_STM_H

#include <Os_kernel.h>
#include <TRICORE/Os_TRICORE_cpu.h>

#ifndef OS_TRICORE_BF_00
#error "Include error: Silicon bug workarounds have not been defined"
#endif

#ifdef __cplusplus
extern "C" {
#endif



#ifndef OS_ASM
extern const os_hwtdrv_t OS_stmDriver;

void OS_StmInit(const os_hwt_t *);
os_timervalue_t OS_StmRead(const os_hwt_t *);
os_boolean_t OS_StmStart(const os_hwt_t *, os_timervalue_t, os_tick_t);
void OS_StmStop(const os_hwt_t *);

#endif

#ifdef __cplusplus
}
#endif

#endif
/* Editor settings: DO NOT DELETE
 * vi:set ts=4:
*/
