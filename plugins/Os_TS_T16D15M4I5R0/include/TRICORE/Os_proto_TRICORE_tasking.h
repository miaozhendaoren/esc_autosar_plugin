/* Os_proto_TRICORE_tasking.h - for Tasking toolchain
 *
 * This file contains some code specific to the Tasking compiler
 * that can only be declared AFTER all the OS_ type declarations.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_proto_TRICORE_tasking.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#ifndef __OS_PROTO_TRICORE_TASKING_H
#define __OS_PROTO_TRICORE_TASKING_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef OS_ASM

/* OS_IntEnable() sets the ICR to the "enable" state (IE=1, CCPN=0)
 * and returns the previous state.
*/
static inline os_intstatus_t OS_IntEnable(void)
{	register os_uint32_t macroTmpOldIcr = OS_MFCR(OS_ICR);
	OS_MTCR(OS_ICR, ((macroTmpOldIcr & ~OS_ICR_CCPN) | OS_ICR_IE));
	return (os_intstatus_t)macroTmpOldIcr;
}

/* OS_IntDisable() sets the ICR to the "disable" state (from the
 * constant OS_kernDisableLevel) and returns the previous state.
*/
static inline os_intstatus_t OS_IntDisable(void)
{	register os_uint32_t macroTmpOldIcr = OS_MFCR(OS_ICR);
	OS_MTCR(OS_ICR, ((macroTmpOldIcr & ~(OS_ICR_IE | OS_ICR_CCPN)) | OS_kernDisableLevel));
	return (os_intstatus_t)macroTmpOldIcr;
}

/* OS_IntDisableConditional() sets the ICR to the "disable" state (from the
 * constant OS_kernDisableLevel) provided that by doing so the level does not get reduced,
 *  and returns the previous state.
*/
static inline os_intstatus_t OS_IntDisableConditional(void)
{	register os_uint32_t macroTmpOldIcr = OS_MFCR(OS_ICR);
	if ( (macroTmpOldIcr & OS_ICR_CCPN) < (OS_kernDisableLevel & OS_ICR_CCPN) )
	{
		OS_MTCR(OS_ICR, ((macroTmpOldIcr & ~(OS_ICR_IE | OS_ICR_CCPN)) | OS_kernDisableLevel));
	}
	return (os_intstatus_t)macroTmpOldIcr;
}

/* OS_IntDisableAll() sets the ICR to the "disable" state (from the
 * constant OS_intDisableLevelAll) and returns the previous state.
*/
static inline os_intstatus_t OS_IntDisableAll(void)
{	register os_uint32_t macroTmpOldIcr = OS_MFCR(OS_ICR);
	OS_MTCR(OS_ICR, ((macroTmpOldIcr & ~(OS_ICR_IE | OS_ICR_CCPN)) | OS_intDisableLevelAll));
	return (os_intstatus_t)macroTmpOldIcr;
}

/* OS_IntRestore() restores a previously-saved IE/CCPN status.
 * No value is returned.
*/
static inline void OS_IntRestore(os_intstatus_t os_p)
{
	register os_uint32_t macroTmpIcr = (os_p) & (OS_ICR_IE | OS_ICR_CCPN);
	OS_MTCR(OS_ICR, ((OS_MFCR(OS_ICR) & ~(OS_ICR_IE | OS_ICR_CCPN)) | macroTmpIcr));
}

#endif


#ifdef __cplusplus
}
#endif

#endif /* OS_PROTO_TRICORE_TASKING_H */

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
