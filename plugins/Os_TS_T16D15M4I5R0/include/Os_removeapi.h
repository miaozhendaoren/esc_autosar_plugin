/* Os_removeapi.h - Api remove feature header
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_removeapi.h 18692 2014-09-02 10:31:08Z mist8519 $
*/

/*
 * !LINKSTO Kernel.Feature.APIRemoval, 1
*/

#ifndef __OS_REMOVEAPI_H
#define __OS_REMOVEAPI_H

/*  Optimization */
#ifdef OS_USE_API_REMOVAL
#include <os-remove-api.h>

#ifdef OS_EXCLUDE_ACTIVATETASK
#define OS_K_ACTIVATETASK				&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_TERMINATETASK
#define OS_K_TERMINATETASK				&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_CHAINTASK
#define OS_K_CHAINTASK					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_SCHEDULE
#define OS_K_SCHEDULE					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETTASKID
#define OS_K_GETTASKID					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETTASKSTATE
#define OS_K_GETTASKSTATE				&OS_KernUnknownSyscall
#endif

#if defined(OS_EXCLUDE_DISABLEALLINTERRUPTS) && \
	defined(OS_EXCLUDE_SUSPENDALLINTERRUPTS) && \
	defined(OS_EXCLUDE_SUSPENDOSINTERRUPTS)
#define OS_K_SUSPENDINTERRUPTS			&OS_KernUnknownSyscall
#endif

#if defined(OS_EXCLUDE_ENABLEALLINTERRUPTS) && \
	defined(OS_EXCLUDE_RESUMEALLINTERRUPTS) && \
	defined(OS_EXCLUDE_RESUMEOSINTERRUPTS)
#define OS_K_RESUMEINTERRUPTS			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETRESOURCE
#define OS_K_GETRESOURCE				&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_RELEASERESOURCE
#define OS_K_RELEASERESOURCE			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_SETEVENT
#define OS_K_SETEVENT					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_CLEAREVENT
#define OS_K_CLEAREVENT					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETEVENT
#define OS_K_GETEVENT					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_WAITEVENT
#define OS_K_WAITEVENT					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETALARMBASE
#define OS_K_GETALARMBASE				&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETALARM
#define OS_K_GETALARM					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_SETRELALARM
#define OS_K_SETRELALARM				&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_SETABSALARM
#define OS_K_SETABSALARM				&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_CANCELALARM
#define OS_K_CANCELALARM				&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETACTIVEAPPLICATIONMODE
#define OS_K_GETACTIVEAPPLICATIONMODE	&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_SHUTDOWNOS
#define OS_K_SHUTDOWNOS					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_INCREMENTCOUNTER
#define OS_K_INCREMENTCOUNTER			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETCOUNTERVALUE
#define OS_K_GETCOUNTERVALUE			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETELAPSEDCOUNTERVALUE
#define OS_K_GETELAPSEDCOUNTERVALUE		&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETSTACKINFO
#define OS_K_GETSTACKINFO				&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_STARTSCHEDULETABLE
#define OS_K_STARTSCHEDULETABLE			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_STARTSCHEDULETABLESYNCHRON
#define OS_K_STARTSCHEDULETABLESYNCHRON	&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_NEXTSCHEDULETABLE
#define OS_K_CHAINSCHEDULETABLE			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_STOPSCHEDULETABLE
#define OS_K_STOPSCHEDULETABLE			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_SYNCSCHEDULETABLE
#define OS_K_SYNCSCHEDULETABLE			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_SETSCHEDULETABLEASYNC
#define OS_K_SETSCHEDULETABLEASYNC		&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETSCHEDULETABLESTATUS
#define OS_K_GETSCHEDULETABLESTATUS		&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_TERMINATEAPPLICATION
#define OS_K_TERMINATEAPPLICATION		&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETAPPLICATIONID
#define OS_K_GETAPPLICATIONID			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_GETISRID
#define OS_K_GETISRID					&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_CALLTRUSTEDFUNCTION
#define OS_K_CALLTRUSTEDFUNCTION		&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_CHECKISRMEMORYACCESS
#define OS_K_CHECKISRMEMORYACCESS		&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_CHECKTASKMEMORYACCESS
#define OS_K_CHECKTASKMEMORYACCESS		&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_CHECKOBJECTACCESS
#define OS_K_CHECKOBJECTACCESS			&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_CHECKOBJECTOWNERSHIP
#define OS_K_CHECKOBJECTOWNERSHIP		&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_DISABLEINTERRUPTSOURCE
#define OS_K_DISABLEINTERRUPTSOURCE		&OS_KernUnknownSyscall
#endif

#ifdef OS_EXCLUDE_ENABLEINTERRUPTSOURCE
#define OS_K_ENABLEINTERRUPTSOURCE		&OS_KernUnknownSyscall
#endif

#endif /* OS_USE_API_REMOVAL */

#endif /* __OS_REMOVEAPI_H */
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
