/* Os_mklib.h
 *
 * This file provides prototypes of the library functions that are available
 * in a microkernel environment.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_mklib.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_MKLIB_H
#define OS_MKLIB_H

#include <Os_defs.h>

#if (OS_KERNEL_TYPE!=OS_MICROKERNEL)
#error "Os_mklib.h should only be used in a microkernel environment"
#endif

#include <Os_types.h>

#ifndef OS_ASM
os_result_t OS_GetAlarmBase(os_alarmid_t, os_alarmbase_t*);
#endif


#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
