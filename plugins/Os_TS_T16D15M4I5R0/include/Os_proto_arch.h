/* Os_proto_arch.h
 *
 * This file includes the appropriate architecture prototypes file depending
 * on the chosen architecture.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_proto_arch.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef __OS_PROTO_ARCH_H
#define __OS_PROTO_ARCH_H

#if (OS_ARCH==OS_PA)
#include <PA/Os_proto_PA.h>
#elif (OS_ARCH==OS_TRICORE)
#include <TRICORE/Os_proto_TRICORE.h>
#elif (OS_ARCH==OS_RH850)
#include <RH850/Os_proto_RH850.h>
#elif (OS_ARCH==OS_V850)
#include <V850/Os_proto_V850.h>
#elif (OS_ARCH==OS_NEWARCH)
#include <NEWARCH/Os_proto_NEWARCH.h>
#elif (OS_ARCH==OS_PIKEOS)
#include <PIKEOS/Os_proto_PIKEOS.h>
#elif (OS_ARCH==OS_S12X)
#include <S12X/Os_proto_S12X.h>
#elif (OS_ARCH==OS_XC2000)
#include <XC2000/Os_proto_XC2000.h>
#elif (OS_ARCH==OS_EASYCAN)
#include <EASYCAN/Os_proto_EASYCAN.h>
#elif (OS_ARCH==OS_MB91)
#include <MB91/Os_proto_MB91.h>
#elif (OS_ARCH==OS_R32C)
#include <R32C/Os_proto_R32C.h>
#elif (OS_ARCH==OS_WINDOWS)
#include <WINDOWS/Os_proto_WINDOWS.h>
#elif (OS_ARCH==OS_SH2)
#include <SH2/Os_proto_SH2.h>
#elif (OS_ARCH==OS_SH4)
#include <SH4/Os_proto_SH4.h>
#elif (OS_ARCH==OS_ARM)
#include <ARM/Os_proto_ARM.h>
#elif (OS_ARCH==OS_LINUX)
#include <LINUX/Os_proto_LINUX.h>
#elif (OS_ARCH==OS_DSPIC33)
#include <DSPIC33/Os_proto_DSPIC33.h>
#else
#error "Unsupported OS_ARCH defined. Check your Makefiles!"
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
