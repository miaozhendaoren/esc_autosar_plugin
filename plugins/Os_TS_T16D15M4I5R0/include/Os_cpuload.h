/* Os_cpuload.h
 *
 * Defines macros, data types and function prototypes for measuring CPU load (user-visible part)
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_cpuload.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef __OS_CPULOAD_H
#define __OS_CPULOAD_H

#ifndef OS_ASM

#include <Os_types.h>

#if (OS_KERNEL_TYPE==OS_FUNCTION_CALL)

#include <Os_userkernel.h>

#define OS_UserGetCpuLoad(getMax)	OS_CALL(os_uint8_t, OS_KernGetCpuLoad(getMax))
#define OS_UserResetPeakCpuLoad()	OS_VOIDCALL(OS_KernResetPeakCpuLoad())

#else /* OS_KERNEL_TYPE */

/* Userland functions in case it's necessary to call the CPU load API from a non-trusted
 * application. The system-call overhead is counted as part of the load.
 *
 * !LINKSTO Kernel.Feature.CpuLoadMeasurement.API.GetMaxCpuLoad, 1
*/
os_uint8_t OS_UserGetCpuLoad(os_boolean_t);
void OS_UserResetPeakCpuLoad(void);

#endif /* OS_KERNEL_TYPE */

/* Kernel functions. These are mapped to the SALSA API via Os_salsa.h
*/
os_uint8_t OS_KernGetCpuLoad(os_boolean_t);	/* Parameter: if true: get peak, false: get current */
void OS_KernResetPeakCpuLoad(void);

#define OS_GetCpuLoad(getMax)	OS_KernGetCpuLoad(getMax)
#define OS_ResetPeakCpuLoad()	OS_KernResetPeakCpuLoad()

#endif /* OS_ASM */

#endif /* __OS_CPULOAD_H */
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
