/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined IPDUM_H)
#define IPDUM_H

/*
 *  MISRA-1) Deviated Rule: 19.6 (required)
 *   '#undef' shall not be used.
 *
 *  Reason:
 *  The macros IPDUM_NO_PBCFG_REQUIRED, IPDUM_NO_CFG_REQUIRED
 *  are used for optimized compilation time of the modules using the IpduM module.
 *  To avoid that this macro is used by other modules accidentally
 *  a #undef is used here.
 *
 */

/*==================[inclusions]============================================*/
#include <IpduM_Api.h>
#include <IpduM_Cbk.h>

/* Exclude post-build-time configuration include file if requested to do so */
#if (!defined IPDUM_NO_PBCFG_REQUIRED) && (!defined IPDUM_NO_CFG_REQUIRED)
#include <IpduM_PBcfg.h>
#endif /* (!defined IPDUM_NO_PBCFG_REQUIRED) && (!defined IPDUM_NO_CFG_REQUIRED) */

/* Deviation MISRA-1 */
#undef IPDUM_NO_PBCFG_REQUIRED
/* Deviation MISRA-1 */
#undef IPDUM_NO_CFG_REQUIRED


/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( IPDUM_H ) */
/*==================[end of file]===========================================*/
