/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined IPDUM_COMCALLOUT_CFG_H)
#define IPDUM_COMCALLOUT_CFG_H

[!AUTOSPACING!]
/*==================[includes]==============================================*/

#include <TSAutosar.h>              /* global configuration */

[!INCLUDE "IpduM_Macros.m"!]

/*==================[macros]================================================*/

/* Network ordering of Global EcuId */
#define IPDUM_NETWORK_BIG_ENDIAN        STD_OFF

[!IF "node:exists(IpduMRequestMessageConfiguration)"!]
#define IPDUM_SERVICE_MESSAGES          STD_ON

/* Number of service message */
#define IPDUM_MAP_INFO_LENGTH           [!"num:i(count(IpduMRequestMessageConfiguration/IpduMRequestMessageMapInfo/*))"!]U
/* Defines the byte position of the data field in the received message */
#define IPDUM_RX_CALLOUT_BYTEPOS        [!"IpduMRequestMessageConfiguration/IpduMRequestMessageIdBytePos"!]U
/* Defines the length of the data field */
#define IPDUM_RX_CALLOUT_LENGTH         [!"IpduMRequestMessageConfiguration/IpduMRequestMessageIdLength"!]U

/* Table for mapping of global EcuId to HandleId for requesting PDU */
#define IPDUM_MAP_INFO_CFG \
  { \
[!LOOP "IpduMRequestMessageConfiguration/IpduMRequestMessageMapInfo/*"!]
    [!CALL "GetComHandleId","IpduMRequestMessagePduRef"="IpduMRequestedMessagePduRef"!]
    { [!"IpduMRequestedMessageId"!]U, [!"$PduID"!]U }, \
[!ENDLOOP!]
  }
[!ELSE!]
#define IPDUM_SERVICE_MESSAGES          STD_OFF
[!ENDIF!]

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( IPDUM_COMCALLOUT_CFG_H ) */
/*==================[end of file]===========================================*/
