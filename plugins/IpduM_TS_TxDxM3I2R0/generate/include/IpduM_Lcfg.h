/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined IPDUM_LCFG_H)
#define IPDUM_LCFG_H

[!AUTOSPACING!]
/*==================[includes]==============================================*/

#include <TSAutosar.h>              /* global configuration */
#include <IpduM_Types.h>            /* Module types */

[!INCLUDE "IpduM_Macros.m"!]
[!INCLUDE "IpduM_checks.m"!]
/*==================[macros]================================================*/
[!SELECT "IpduMConfig/*[1]"!]

/**
 * Size of internal IpduM data in units of bytes (static memory
 * allocation) - Memory required by post-build configuration must be smaller
 * than this constant
 */[!//
[!IF "node:exists(../../IpduMGeneral/IpduMDataMemSize)"!]
#define IPDUM_DATA_MEM_SIZE [!"../../IpduMGeneral/IpduMDataMemSize"!]U
[!ELSE!]
  [!IF "node:exists(IpduMTxPathway/*)"!]
    [!VAR "TxPathWayTotalCount" = "num:i(count(IpduMTxPathway/*))"!]
    [!IF "$IpduMDynamicPartQueue = 'true'"!]
      [!VAR "TxQueueTotalEntries" = "num:i(sum(IpduMTxPathway/*/IpduMTxRequest/IpduMQueueSize))"!]
    [!ELSE!]
      [!VAR "TxQueueTotalEntries" = "num:i(0)"!]
    [!ENDIF!]
    [!IF "$IpduMZeroCopy = 'true'"!]
      [!VAR "TxTotalBufferSize" = "num:i(0)"!]
    [!ELSE!]
      [!VAR "TxTotalBufferSize" = "num:i(sum(node:refs(./IpduMTxPathway/*/IpduMTxRequest/IpduMOutgoingPduRef)/PduLength))"!]
  [!ENDIF!][!//
#define IPDUM_DATA_MEM_SIZE \
    ((((uint32)sizeof(IpduM_TxDataType))*[!"num:i(count(as:modconf('IpduM')[1]/IpduMConfig/*[1]/IpduMTxPathway/*))"!]U) + (((uint32)sizeof(IpduM_QueueEntryType))*[!"$TxQueueTotalEntries"!]U )+ [!"$TxTotalBufferSize"!]U)
  [!ELSE!]
#define IPDUM_DATA_MEM_SIZE 1U
  [!ENDIF!]
[!ENDIF!]

[!ENDSELECT!]

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( IPDUM_LCFG_H ) */
/*==================[end of file]===========================================*/
