/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined IPDUM_CFG_H)
#define IPDUM_CFG_H

[!INCLUDE "IpduM_checks.m"!]

/* This file contains all target independent public configuration declarations
 * for the AUTOSAR module IpduM. */

[!AUTOSPACING!]
[!//
[!VAR "Spaces31" = "'                               '"!]
[!//

/*==================[inclusions]============================================*/
#include <ComStack_Types.h>

/*==================[macros]================================================*/
/*------------------[General]-----------------------------------------------*/

[!INDENT "0"!]
#if (defined IPDUM_AUTOSAR_VERSION)
#error IPDUM_AUTOSAR_VERSION already defined
#endif
/** \brief The respective AUTOSAR version */
 #define IPDUM_AUTOSAR_VERSION         30
[!ENDINDENT!]

[!INDENT "0"!]
#if (defined IPDUM_DEV_ERROR_DETECT)
#error IPDUM_DEV_ERROR_DETECT already defined
#endif
/** \brief Switch, indicating if development error detection is activated for
 ** IPDUM */
[!IF "$IpduMDevErrorDetect = 'true'"!][!/*
*/!]  #define IPDUM_DEV_ERROR_DETECT         STD_ON
[!ELSE!][!/*
*/!]  #define IPDUM_DEV_ERROR_DETECT         STD_OFF
[!ENDIF!][!/*
*/!][!ENDINDENT!]

[!INDENT "0"!]
#if (defined IPDUM_VERSION_INFO_API)
#error IPDUM_VERSION_INFO_API already defined
#endif
/** \brief Switch, indicating if the version information API is enabled or
 ** disabled for IPDUM */
[!IF "$IpduMVersionInfoApi = 'true'"!][!/*
 */!]  #define IPDUM_VERSION_INFO_API         STD_ON
[!ELSE!][!/*
 */!]  #define IPDUM_VERSION_INFO_API         STD_OFF
[!ENDIF!][!/*
 */!][!ENDINDENT!]

[!INDENT "0"!]
#if (defined IPDUM_STATIC_PART_EXISTS)
#error IPDUM_STATIC_PART_EXISTS already defined
#endif
/** \brief Switch, indicating if a static part exists or not in an IPDU.
 ** This is to allow optimizations in case the IpduM will never
 ** be used with a static part.
 **
 ** \note This is a pre-compile option. If this is set to False then it
 ** will not be possible to add static parts after compilation.
 **/
[!IF "$IpduMStaticPartExists = 'true'"!]
  #define IPDUM_STATIC_PART_EXISTS         STD_ON
[!ELSE!]
  #define IPDUM_STATIC_PART_EXISTS         STD_OFF
[!ENDIF!]
[!ENDINDENT!]

[!INDENT "0"!]
#if (defined IPDUM_ZERO_COPY)
#error IPDUM_ZERO_COPY already defined
#endif
/** \brief This indicates do not allocate memory for data in the IpduM.
**  Only pointers for static and dynamic parts will be passed.
**
** \note Zero copy is possible only in some circumstances.
 **/
[!IF "$IpduMZeroCopy = 'true'"!][!/*
 */!]  #define IPDUM_ZERO_COPY         STD_ON
[!ELSE!][!/*
 */!]  #define IPDUM_ZERO_COPY         STD_OFF
[!ENDIF!][!/*
 */!][!ENDINDENT!]

[!INDENT "0"!]
#if (defined IPDUM_BYTE_COPY)
#error IPDUM_BYTE_COPY already defined
#endif
/** \brief Switch, indicating use of byte-wise copy routines. Only possible if static and
**  dynamic parts are already byte-aligned.
**/
[!IF "$IpduMByteCopy = 'true'"!]
  #define IPDUM_BYTE_COPY         STD_ON
[!ELSE!]
  #define IPDUM_BYTE_COPY         STD_OFF
[!ENDIF!]
[!ENDINDENT!]

[!INDENT "0"!]
#if (defined IPDUM_DYNAMIC_PART_QUEUE)
#error IPDUM_DYNAMIC_PART_QUEUE already defined
#endif
/** \brief This specifies if queuing is enabled for dynamic PDUs.
**/
[!IF "$IpduMDynamicPartQueue = 'true'"!][!/*
*/!]  #define IPDUM_DYNAMIC_PART_QUEUE         STD_ON
[!ELSE!][!/*
*/!]  #define IPDUM_DYNAMIC_PART_QUEUE         STD_OFF
[!ENDIF!][!/*
*/!][!ENDINDENT!]

/** \brief This specifies the number of bytes on stack used for the buffer
**  in RxIndication
**/
#define IPDUM_RECEIVE_STACK_SIZE 254U

/** \brief This specifies the number of bytes for the SDU buffer in the
**  transmission queue
**/
[!IF "node:exists(node:refs(./IpduMConfig/*[1]/IpduMTxPathway/*/IpduMTxRequest/IpduMOutgoingPduRef)/PduLength) = 'true'"!][!/*
*/!]#define IPDUM_TX_SDU_SIZE [!"num:i(num:max(node:refs(./IpduMConfig/*[1]/IpduMTxPathway/*/IpduMTxRequest/IpduMOutgoingPduRef)/PduLength))"!]U
[!ELSE!][!/*
*/!]#define IPDUM_TX_SDU_SIZE 1U
[!ENDIF!][!/*
*/!]
[!INDENT "0"!]
#if (defined IPDUM_AUTOMATIC_SELECTOR)
#error IPDUM_AUTOMATIC_SELECTOR already defined
#endif
/** \brief This specifies if automatic selector setting is enabled for transmit
**  PDUs.
**/
[!IF "$IpduMTxAutomaticSelector = 'true'"!][!/*
*/!]  #define IPDUM_AUTOMATIC_SELECTOR         STD_ON
[!ELSE!][!/*
*/!]  #define IPDUM_AUTOMATIC_SELECTOR         STD_OFF
[!ENDIF!][!/*
*/!][!ENDINDENT!]




/*------------------[Defensive programming]---------------------------------*/
[!SELECT "IpduMDefensiveProgramming"!][!//

#if (defined IPDUM_DEFENSIVE_PROGRAMMING_ENABLED)
#error IPDUM_DEFENSIVE_PROGRAMMING_ENABLED is already defined
#endif
/** \brief Defensive programming usage
 **
 ** En- or disables the usage of the defensive programming */
#define IPDUM_DEFENSIVE_PROGRAMMING_ENABLED   [!//
[!IF "(../IpduMGeneral/IpduMDevErrorDetect  = 'true') and (IpduMDefProgEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined IPDUM_PRECONDITION_ASSERT_ENABLED)
#error IPDUM_PRECONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Precondition assertion usage
 **
 ** En- or disables the usage of precondition assertion checks */
#define IPDUM_PRECONDITION_ASSERT_ENABLED     [!//
[!IF "(../IpduMGeneral/IpduMDevErrorDetect  = 'true') and (IpduMDefProgEnabled = 'true') and (IpduMPrecondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined IPDUM_POSTCONDITION_ASSERT_ENABLED)
#error IPDUM_POSTCONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Postcondition assertion usage
 **
 ** En- or disables the usage of postcondition assertion checks */
#define IPDUM_POSTCONDITION_ASSERT_ENABLED    [!//
[!IF "(../IpduMGeneral/IpduMDevErrorDetect  = 'true') and (IpduMDefProgEnabled = 'true') and (IpduMPostcondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined IPDUM_UNREACHABLE_CODE_ASSERT_ENABLED)
#error IPDUM_UNREACHABLE_CODE_ASSERT_ENABLED is already defined
#endif
/** \brief Unreachable code assertion usage
 **
 ** En- or disables the usage of unreachable code assertion checks */
#define IPDUM_UNREACHABLE_CODE_ASSERT_ENABLED [!//
[!IF "(../IpduMGeneral/IpduMDevErrorDetect  = 'true') and (IpduMDefProgEnabled = 'true') and (IpduMUnreachAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined IPDUM_INVARIANT_ASSERT_ENABLED)
#error IPDUM_INVARIANT_ASSERT_ENABLED is already defined
#endif
/** \brief Invariant assertion usage
 **
 ** En- or disables the usage of invariant assertion checks */
#define IPDUM_INVARIANT_ASSERT_ENABLED        [!//
[!IF "(../IpduMGeneral/IpduMDevErrorDetect  = 'true') and (IpduMDefProgEnabled = 'true') and (IpduMInvariantAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined IPDUM_STATIC_ASSERT_ENABLED)
#error IPDUM_STATIC_ASSERT_ENABLED is already defined
#endif
/** \brief Static assertion usage
 **
 ** En- or disables the usage of static assertion checks */
#define IPDUM_STATIC_ASSERT_ENABLED           [!//
[!IF "(../IpduMGeneral/IpduMDevErrorDetect  = 'true') and (IpduMDefProgEnabled = 'true') and (IpduMStaticAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDSELECT!][!//


/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( IPDUM_CFG_H ) */
/*==================[end of file]===========================================*/
