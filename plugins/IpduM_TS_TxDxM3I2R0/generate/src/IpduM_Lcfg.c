/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * MISRA-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 11.4 (advisory)
 * A cast should not be performed between a pointer to object type and a
 * different pointer to object type
 *
 * Reason:
 * For post build configuration this type cast is needed.
 */

/*==================[inclusions]============================================*/

#include <TSAutosar.h>           /* EB specific standard types */
#include <IpduM_Lcfg.h>          /* Generated configuration */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

#define IPDUM_DATA_MEM_SIZE_32BIT_WORDS \
    (((IPDUM_DATA_MEM_SIZE + (uint32)sizeof(uint32)) - 1U) / (uint32)sizeof(uint32))

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/* start data section declaration */
#define IPDUM_START_SEC_CONST_32
#include <MemMap.h>

/**
 * Constant with size of post-build RAM in units of bytes
 */
/* !LINKSTO IPDUM.ASR40.IPDUM075,1 */
CONST(uint32, IPDUM_CONST) IpduM_gDataMemSize = IPDUM_DATA_MEM_SIZE;

/* stop data section declaration */
#define IPDUM_STOP_SEC_CONST_32
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/* start data section declaration */
#define IPDUM_START_SEC_VAR_NO_INIT_32
#include <MemMap.h>

/**
 * Internal memory statically allocated upon link-time. - Size depends on
 * post build configuration => memory requirements of post build configuration
 * must be smaller than IPDUM_DATA_MEM_SIZE in order to avoid buffer
 * overflows. - Memory is 32-bit aligned in oder to enable 32 Bit accesses
 * to memory
 *
 * Note: This array itself is private. - Access to this array is done via
 *       IpduM_gDataMemPtr.
 */
static VAR(uint32, IPDUM_VAR_NOINIT) IpduM_DataMem[IPDUM_DATA_MEM_SIZE_32BIT_WORDS];

/* stop data section declaration */
#define IPDUM_STOP_SEC_VAR_NO_INIT_32
#include <MemMap.h>


/*==================[internal data]=========================================*/

/*==================[external constants]====================================*/

/* start data section declaration */
#define IPDUM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/**
 * Exported pointer to post build memory which is statically allocated upon link-time
 *
 * Although the memory itself (i.e., the start address of the memory) is 32 bit aligned, a uint8
 * pointer is used for access to facilitate accesses as byte array with byte offsets as array indices
 */
/* !LINKSTO IPDUM.ASR40.IPDUM075,1 */
/* Deviation MISRA-1 */
CONSTP2VAR(uint8, IPDUM_CONST, IPDUM_VAR_NOINIT) IpduM_gDataMemPtr = (P2VAR(uint8, IPDUM_CONST, IPDUM_VAR_NOINIT)) IpduM_DataMem;

/* stop data section declaration */
#define IPDUM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
