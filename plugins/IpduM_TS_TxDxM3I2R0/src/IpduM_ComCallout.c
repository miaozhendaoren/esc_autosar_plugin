/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]============================================*/
#include <IpduM_Trace.h>
#include <ComStack_Types.h>
#include <TSCompiler.h>

#include <IpduM_Int.h>
#include <IpduM_ComCallout_Cfg.h>
#include <IpduM_ComCallout.h>

#if (IPDUM_DEV_ERROR_DETECT==STD_ON)
#include <Det.h>
#endif

#if (IPDUM_SERVICE_MESSAGES==STD_ON)
#include <Com.h>           /* !LINKSTO IPDUM.ASR40.IPDUM148_6,1 */
#endif

#if (IPDUM_SERVICE_MESSAGES==STD_ON)
/*==================[macros]================================================*/
#if (IPDUM_NETWORK_BIG_ENDIAN==STD_ON)

#if (IPDUM_RX_CALLOUT_LENGTH==1U)
#define IPDUM_GET_MESSAGEID(src,pos) \
  ((src)[(pos)])
#elif (IPDUM_RX_CALLOUT_LENGTH==2U)
#define IPDUM_GET_MESSAGEID(src,pos) \
  ((IpduM_ServiceMessageIdType)(((IpduM_ServiceMessageIdType)((src)[(pos)])) << 8U) | \
                                (src)[(pos)+1U])
#else
#define IPDUM_GET_MESSAGEID(src,pos) \
  ((IpduM_ServiceMessageIdType)(((IpduM_ServiceMessageIdType)((src)[(pos)]))    << 24U) | \
   (IpduM_ServiceMessageIdType)(((IpduM_ServiceMessageIdType)((src)[(pos)+1U])) << 16U) | \
   (IpduM_ServiceMessageIdType)(((IpduM_ServiceMessageIdType)((src)[(pos)+2U])) << 8U)  | \
   (src)[(pos)+3U])
#endif

#else /* (IPDUM_NETWORK_BIG_ENDIAN==STD_OFF) */

#if (IPDUM_RX_CALLOUT_LENGTH==1U)
#define IPDUM_GET_MESSAGEID(src,pos) \
  ((src)[(pos)])
#elif (IPDUM_RX_CALLOUT_LENGTH==2U)
#define IPDUM_GET_MESSAGEID(src,pos) \
  (((src)[(pos)]) | \
   (IpduM_ServiceMessageIdType)(((IpduM_ServiceMessageIdType)((src)[(pos)+1U])) << 8U))
#else
#define IPDUM_GET_MESSAGEID(src,pos) \
  ((src)[(pos)] | \
   (IpduM_ServiceMessageIdType)(((IpduM_ServiceMessageIdType)((src)[(pos)+1U])) << 8U)  | \
   (IpduM_ServiceMessageIdType)(((IpduM_ServiceMessageIdType)((src)[(pos)+2U])) << 16U) | \
   (IpduM_ServiceMessageIdType)(((IpduM_ServiceMessageIdType)((src)[(pos)+3U])) << 24U))
#endif

#endif

/*==================[type definitions]======================================*/

#if (IPDUM_RX_CALLOUT_LENGTH==1)
typedef uint8 IpduM_ServiceMessageIdType;
#elif (IPDUM_RX_CALLOUT_LENGTH==2)
typedef uint16 IpduM_ServiceMessageIdType;
#else
typedef uint32 IpduM_ServiceMessageIdType;
#endif

typedef struct
{
  IpduM_ServiceMessageIdType ServiceMessageId;
  PduIdType ComTxPduId;
} IpduM_MapInfoType;

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/
#define IPDUM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

CONST(IpduM_MapInfoType, IPDUM_CONST) IpduM_MapInfo[IPDUM_MAP_INFO_LENGTH] = IPDUM_MAP_INFO_CFG;

#define IPDUM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/
#define IPDUM_START_SEC_CODE
#include <MemMap.h>

FUNC(boolean, IPDUM_CODE) IpduM_ProcessRequestPdu
(
  PduIdType PdumRxPduId,
  P2CONST(uint8, AUTOMATIC, IPDUM_APPL_DATA) SduDataPtr
)
{
  IpduM_ServiceMessageIdType ServiceMessageId;
  uint16 i;

  DBG_IPDUM_PROCESSREQUESTPDU_ENTRY(PdumRxPduId,SduDataPtr);

  TS_PARAM_UNUSED(PdumRxPduId);

  /* Extract service message */
  ServiceMessageId = (IpduM_ServiceMessageIdType)(IPDUM_GET_MESSAGEID(SduDataPtr,
                               IPDUM_RX_CALLOUT_BYTEPOS));

  /* Search for Com PduId */
  i=0;
  while (i<(uint16)(IPDUM_MAP_INFO_LENGTH))
  {
    if (IpduM_MapInfo[i].ServiceMessageId==ServiceMessageId)
  {
    /* Call Com */
      Com_TriggerIPDUSend(IpduM_MapInfo[i].ComTxPduId);

      break;
  }
    i++;
}


  DBG_IPDUM_PROCESSREQUESTPDU_EXIT(FALSE,PdumRxPduId,SduDataPtr);
  return FALSE;
}

#define IPDUM_STOP_SEC_CODE
#include <MemMap.h>

#endif /* #if (IPDUM_SERVICE_MESSAGES==STD_OFF) */

/* Avoid empty translation unit according to ISO C90 */
TS_PREVENTEMPTYTRANSLATIONUNIT

/*==================[end of file]===========================================*/
