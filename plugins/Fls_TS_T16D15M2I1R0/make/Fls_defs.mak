# \file
#
# \brief AUTOSAR Fls
#
# This file contains the implementation of the AUTOSAR
# module Fls.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

Fls_CORE_PATH      := $(SSC_ROOT)\Fls_$(Fls_VARIANT)
Fls_OUTPUT_PATH    := $(AUTOSAR_BASE_OUTPUT_PATH)

Fls_GEN_FILES      += $(Fls_OUTPUT_PATH)\include\Fls_17_Pmu_Cfg.h
Fls_GEN_FILES      += $(Fls_OUTPUT_PATH)\src\Fls_17_Pmu_PBCfg.c

TRESOS_GEN_FILES   += $(Fls_GEN_FILES)

CC_INCLUDE_PATH    += $(Fls_CORE_PATH)\include
CC_INCLUDE_PATH    += $(Fls_OUTPUT_PATH)\include

