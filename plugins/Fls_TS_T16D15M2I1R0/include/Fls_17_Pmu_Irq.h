
#ifndef PMU_17_PMU_IRQ_H
#define PMU_17_PMU_IRQ_H

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Std_Types.h"                                                        

#ifdef OS_KERNEL_TYPE                                                           
#include <Os.h>        /* OS interrupt services */                              
#endif


/*******************************************************************************
**                      Public Macro Definitions                              **
*******************************************************************************/

/************************* interrupt CATEGORY *********************************/

#ifdef IRQ_CAT1
#if (IRQ_CAT1 != 1)
#error IRQ_CAT1 already defined with a wrong value
#endif
#else
#define IRQ_CAT1                    (1)
#endif

#ifdef IRQ_CAT23
#if (IRQ_CAT23 != 2)
#error IRQ_CAT23 already defined with a wrong value
#endif
#else
#define IRQ_CAT23                    (2)
#endif



/* The name of the ISRs shall be the same name than the ISR       *
 * functions provided by Infineon                                 */

/*************************PMU0SR0_ISR*********************************/          

#ifdef PMU0SR0_ISR
#define PMU0SR0_EXIST        STD_ON
#define IRQ_PMU0_SR0_PRIO    PMU0SR0_ISR_ISR_LEVEL
#define IRQ_PMU0_SR0_CAT     PMU0SR0_ISR_ISR_CATEGORY
#else
#define PMU0SR0_EXIST        STD_OFF
#endif

/*************************PMU0SR1_ISR*********************************/          

#ifdef PMU0SR1_ISR
#define PMU0SR1_EXIST        STD_ON
#define IRQ_PMU0_SR1_PRIO    PMU0SR1_ISR_ISR_LEVEL
#define IRQ_PMU0_SR1_CAT     PMU0SR1_ISR_ISR_CATEGORY
#else
#define PMU0SR1_EXIST        STD_OFF
#endif

#endif /* #ifndef PMU_17_PMU_IRQ_H */
