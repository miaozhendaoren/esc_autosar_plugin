# \file
#
# \brief AUTOSAR CanIf
#
# This file contains the implementation of the AUTOSAR
# module CanIf.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

##############################################################################
# DEFINITIONS


##############################################################################
# REGISTRY

LIBRARIES_TO_BUILD   += CanIf_src

ifndef TS_CANIF_COMPILE_WITH_POSTBUILD
TS_CANIF_COMPILE_WITH_POSTBUILD := TRUE
endif

ifndef TS_BUILD_POST_BUILD_BINARY
TS_BUILD_POST_BUILD_BINARY := FALSE
endif

CanIf_lib_FILES      +=

CanIf_src_FILES      += \
    $(CanIf_CORE_PATH)\src\CanIf.c \
    $(CanIf_OUTPUT_PATH)\src\CanIf_Lcfg.c \
    $(CanIf_OUTPUT_PATH)\src\CanIf_Cfg.c

ifeq ($(TS_CANIF_COMPILE_WITH_POSTBUILD),TRUE)
# Compile with postbuild
CanIf_src_FILES += $(CanIf_OUTPUT_PATH)\src\CanIf_PBcfg.c
endif

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
CanIf_src_FILES :=
endif

# Fill the list with post build configuration files needed to build the post build binary.
CanIf_pbconfig_FILES := $(CanIf_OUTPUT_PATH)/src/CanIf_PBcfg.c

CC_FILES_TO_BUILD    +=
CPP_FILES_TO_BUILD   +=
ASM_FILES_TO_BUILD   +=


MAKE_CLEAN_RULES     +=
MAKE_GENERATE_RULES  +=
MAKE_COMPILE_RULES   +=
#MAKE_DEBUG_RULES    +=
MAKE_CONFIG_RULES    +=
#MAKE_ADD_RULES      +=

define defineCanIfLibOutputPATH
$(1)_OBJ_OUTPUT_PATH += $(CanIf_lib_LIB_OUTPUT_PATH)
endef

$(foreach SRC,$(basename $(notdir $(subst \,/,$(CanIf_lib_FILES)))),$(eval $(call defineCanIfLibOutputPATH,$(SRC))))

##############################################################################
# DEPENDENCIES (only for assembler files)
#

##############################################################################
# RULES
