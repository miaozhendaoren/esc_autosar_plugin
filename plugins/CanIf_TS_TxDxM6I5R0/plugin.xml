<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.2"?>
<plugin>

  <!--
    ** \brief AUTOSAR CanIf_TS_TxDxM6I5R0 plugin
    ** \project AUTOSAR Standard Core
    ** \file plugin.xdm
    **
    ** Copyright 2010 by Elektrobit Automotive GmbH
    ** All rights exclusively reserved for Elektrobit Automotive GmbH,
    ** unless expressly agreed to otherwise.
  -->


  <extension point="dreisoft.tresos.launcher2.plugin.module"
             id="CanIf_TS_TxDxM6I5R0_ModuleId"
             name="CanIf_TS_TxDxM6I5R0 Module">

    <module id="CanIf_TS_TxDxM6I5R0"
            label="CanIf"
            mandatory="false"
            allowMultiple="false"
            description="ACG-CANStack: AUTOSAR CanIf module"
            copyright="(c) 2005-2013 Elektrobit Automotive GmbH"
            swVersionMajor="6"
            swVersionMinor="5"
            swVersionPatch="1"
            swVersionSuffix="B84203"
            specVersionMajor="5"
            specVersionMinor="0"
            specVersionPatch="0"
            specVersionSuffix="0000"
            relVersionMajor="4"
            relVersionMinor="0"
            relVersionPatch="3"
            categoryType="CanIf"
            categoryLayer="Communication HW Abstraction"
            categoryCategory="CAN"
            categoryComponent="ECUC">


      <cluster name="Can Stack"/>
      


    </module>
  </extension>


  <extension point="dreisoft.tresos.launcher2.plugin.configuration"
             id="CanIf_TS_TxDxM6I5R0_ConfigId"
             name="CanIf_TS_TxDxM6I5R0 Configuration">
    <configuration moduleId="CanIf_TS_TxDxM6I5R0">
      <schema>
        <manager class="dreisoft.tresos.autosar2.resourcehandling.AutosarSchemaManager"/>
        <resource value="config/CanIf.xdm" type="xdm" id="res_default"/>

        <!-- register the recommended configuration CanIfRecConfigurationDflt -->
        <resource value="config_ext/CanIfRecConfigurationDflt.xdm" type="xdm" id="res_CanIfRecConfigurationDflt"/>
        <recconfig
          name="CanIfRecConfigurationDflt"
          default="true"
          description="Adds CanNm, CanTp and PduR as upper layer modules"
          resourceId="res_CanIfRecConfigurationDflt"
          path="ASPath:/TS_TxDxM6I5R0/CanIfRecConfigurationDflt"/>
      </schema>

      <data>
        <manager class="dreisoft.tresos.autosar2.resourcehandling.AutosarConfigManager"/>
        <schemaNode path="ASPath:/TS_TxDxM6I5R0/CanIf"/>
      </data>

      <editor id="CanIf_TS_TxDxM6I5R0_EditorId"
              label="Default"
              tooltip="ACG-CANStack: AUTOSAR CanIf module">
        <class class="dreisoft.tresos.launcher2.editor.GenericConfigEditor">
          <parameter name="schema" value="ASPath:/TS_TxDxM6I5R0/CanIf"/>
          <parameter name="title" value="CanIf"/>
          <parameter name="noTabs" value="false"/>
          <parameter name="noLinks" value="true"/>
          <parameter name="groupLinks" value="false"/>
          <parameter name="groupContainers" value="false"/>
          <parameter name="groupTables" value="true"/>
          <parameter name="optionalGeneralTab" value="true"/>
        </class>
      </editor>
    </configuration>
  </extension>




    <extension point="dreisoft.tresos.comimporter.api.plugin.comtransformer">
    <comtransformer
           id="ComTransformer_CanIf_TS_TxDxM6I5R0"
           moduleId="CanIf_TS_TxDxM6I5R0">
      <transformer
             class="dreisoft.tresos.comimporter.api.transformer.asr40.CanIfTransformer">
      </transformer>
    </comtransformer>
  </extension>




  
  <extension point="dreisoft.tresos.launcher2.plugin.generator"
              id="CanIf_TS_TxDxM6I5R0_GeneratorId"
              name="CanIf_TS_TxDxM6I5R0 Generator">
    <generator moduleId="CanIf_TS_TxDxM6I5R0"
               class="dreisoft.tresos.launcher2.generator.TemplateBasedCodeGenerator">

      <!-- common template path parameters -->
      <parameter name="templates"
                 mode="generate,verify" value="generate"/>

      <!-- swcd modes and template path parameters -->
      <parameter name="mode_type"
                 mode="generate_swcd" value="generate"/>

      <parameter name="mode_type"
                 mode="verify_swcd" value="verify"/>

      <parameter name="templates"
                 mode="generate_swcd,verify_swcd" value="generate_swcd"/>




    </generator>
  </extension>




    <extension point="dreisoft.tresos.guidedconfig.api.plugin.pushservice">
    <pushoperation
        id="SvcAsReq_CanIf_TS_TxDxM6I5R0"
        name="CanIf service needs requests">
      <operationclass plugin="SvcAsLib" class="eb.tresos.svclib.api.pushservice.SvcRequestOperation">
        <parameter name="module" value="CanIf" />
      </operationclass>
      <event>
        <and>
          <with variable="class">
            <equals value="eb.tresos.svclib.api.event.SvcRequestsEvent" />
          </with>
          <with variable="svc.type">
            <equals value="request" />
          </with>
          <with variable="ecuConfig.moduleId.CanIf_TS_TxDxM6I5R0">
            <equals value="true" />
          </with>
          <or>
            <with variable="ecuConfig.relVersion">
              <equals value="4.0" />
            </with>
            <with variable="ecuConfig.relVersion">
              <equals value="0.0" />
            </with>
          </or>
        </and>
      </event>
    </pushoperation>
  </extension>



  
  <extension point="dreisoft.tresos.autosar2.api.plugin.ecuresource">
    <ecuresource
      target="*"
      derivate="*"
      moduleId="CanIf_TS_TxDxM6I5R0"
      file="resources/AdjacentLayer.properties">
    </ecuresource>
  </extension>



<!-- register common ComStack modules for Can Stack cluster -->
  <extension point="dreisoft.tresos.launcher2.api.plugin.modulecluster">
    <modulecluster>
      <default>
        <cluster name="Can Stack"/>
        <module type="Com"/>
        <module type="EcuC"/>
        <module type="PduR"/>
        <module type="IpduM"/>
        <module type="IPduMplex"/>
      </default>
    </modulecluster>
  </extension>

<!-- handle id calculator for CanIfRxPduId -->
  <extension
    point="dreisoft.tresos.guidedconfig.api.plugin.pushservice">
    <pushoperation id="CanIfCanRxPduId_CanIf_TS_TxDxM6I5R0"
    name="CanIf/CanIfCanRxPduId"
    desc="">
        <operationclass class="dreisoft.tresos.guidedconfig.api.pushservice.operations.handleid.SimpleHandleIdPushOperation">
            <parameter name="moduleId" value="CanIf_TS_TxDxM6I5R0"/>
            <parameter name="listPath" value="/AUTOSAR/TOP-LEVEL-PACKAGES/TS_TxDxM6I5R0/ELEMENTS/CanIf/CanIfInitCfg/CanIfInitCfg/CanIfRxPduCfg/"/>
            <parameter name="idPath" value="CanIfRxPduId"/>
            <parameter name="zbcGroups" value="true"/>
        </operationclass>
        <event>
            <and>
                <with variable="class">
                    <equals value="dreisoft.tresos.guidedconfig.api.pushservice.operations.handleid.HandleIdEvent"/>
                </with>
                <with variable="moduleId.CanIf_TS_TxDxM6I5R0">
                   <equals value="true"/>
                </with>
                <with variable="relVersion">
          <equals value="4.0"/>
          </with>
            </and>
        </event>
    </pushoperation>
  </extension>

<!-- handle id calculator for CanIfTxPduId -->
  <extension
    point="dreisoft.tresos.guidedconfig.api.plugin.pushservice">
    <pushoperation id="CanIfTxPduId_CanIf_TS_TxDxM6I5R0"
    name="CanIf/CanIfTxPduId"
    desc="">
        <operationclass class="dreisoft.tresos.guidedconfig.api.pushservice.operations.handleid.SimpleHandleIdPushOperation">
            <parameter name="moduleId" value="CanIf_TS_TxDxM6I5R0"/>
            <parameter name="listPath" value="/AUTOSAR/TOP-LEVEL-PACKAGES/TS_TxDxM6I5R0/ELEMENTS/CanIf/CanIfInitCfg/CanIfInitCfg/CanIfTxPduCfg/"/>
            <parameter name="idPath" value="CanIfTxPduId"/>
            <parameter name="groupBy" value="CanIfTxPduType"/>
            <parameter name="sortOrder" value="STATIC,DYNAMIC"/>
            <parameter name="zbcGroups" value="false"/>
        </operationclass>
        <event>
            <and>
                <with variable="class">
                    <equals value="dreisoft.tresos.guidedconfig.api.pushservice.operations.handleid.HandleIdEvent"/>
                </with>
                <with variable="moduleId.CanIf_TS_TxDxM6I5R0">
                   <equals value="true"/>
                </with>
                <with variable="relVersion">
          <equals value="4.0"/>
          </with>
            </and>
        </event>
    </pushoperation>
  </extension>

</plugin>
