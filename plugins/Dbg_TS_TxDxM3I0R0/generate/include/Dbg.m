[!/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!MACRO "DBG_RES_PREDEF_DID_GROUP_COUNT", "M_resGroupName"!][!//
[!VAR "M_numberOfFindings" = "0"!][!//
[!FOR "M_index"="1" TO "count(ecu:list($M_resGroupName))"!][!//
[!VAR "M_didTypeName" = "ecu:list($M_resGroupName)[num:i($M_index)]"!][!//
[!VAR "M_numberOfFindings" = "$M_numberOfFindings + count(./DbgPredefinedDID/*[DbgPredefinedDIDName=$M_didTypeName])"!][!//
[!ENDFOR!][!//
[!"num:i($M_numberOfFindings)"!]
[!ENDMACRO!][!//
