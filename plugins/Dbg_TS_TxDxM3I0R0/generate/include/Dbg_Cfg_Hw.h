/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_CFG_HW_H)
#define DBG_CFG_HW_H

/*==================[includes]==============================================*/
[!CODE!]
[!AUTOSPACING!]
[!SELECT "as:modconf('Dbg')[1]"!]
[!INCLUDE "Dbg.m"!]

/*==================[version check]=========================================*/

/*==================[macros]================================================*/

[!IF "node:exists(./DbgTcpIp)"!]
/* We can't use STD_ON / STD_OFF here, as these defines may not be available
 * in the target specific sources. */
#define DBG_TCPIP_SUPPORT                    1
#define DBG_TCPIP_PORT                       [!"node:value(./DbgTcpIp/DbgTcpPort)"!]
[!ELSE!]
#define DBG_TCPIP_SUPPORT                    0
[!ENDIF!]

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

[!ENDSELECT!]
[!ENDCODE!]
/** @} doxygen end group definition */
#endif /* if !defined( DBG_CFG_HW_H ) */
/*==================[end of file]===========================================*/
