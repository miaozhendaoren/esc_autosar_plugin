/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
*/

/*==================[inclusions]=============================================*/
#include <TSAutosar.h>                /* EB specific standard types */

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/
#define DBG_START_SEC_CONST_8BIT
/* Deviation MISRA-1 */
#include <MemMap.h>

CONST(uint8,DBG_CONST) Dbg_GenericStateHookId = { 1 };

#define DBG_STOP_SEC_CONST_8BIT
/* Deviation MISRA-1 */
#include <MemMap.h>
/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

/*==================[end of file]============================================*/
