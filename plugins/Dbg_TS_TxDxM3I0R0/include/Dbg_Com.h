/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_COM_H)
#define DBG_COM_H

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 8.12 (required)
 *    When an array is declared with external linkage, its size shall be
 *    stated explicitely or defined implicitely by initialisation.
 *
 *    Reason:
 *    Value is only calculated in .c file and not available here.
 *
 *  MISRA-2) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
 * !LINKSTO Dbg.FileStructure.Dbg233,1
*/

/*==================[inclusions]============================================*/
/*
 * !LINKSTO Dbg.API.Dbg096,1
 */
#include <TSAutosar.h>                        /* EB specific standard types */
#include <Std_Types.h>                            /* AUTOSAR standard types */
#include <Dbg_Types.h>
#include <Dbg_Cbk.h>                      /* callbacks provided by Com part */

#if (DBG_COM_VIA_TP_PDUR==STD_ON)
#include <PduR_Dbg.h>
#endif

/*==================[version check]=========================================*/

/*==================[macros]================================================*/
/** \brief TP1ST frame flag
 **
 ** This flag is needed in the TP implementation for first frame selection on
 ** segmented data.
 */
#define DBG_COM_TP_FIRST_FRAME   0x10U

#define DBG_COM_TP_FLG_SET_TP1ST 0x01U
#define DBG_COM_TP_FLG_DEL_TP1ST 0x02U

#define DBG_COM_TP_ADD_CHUNK     1U

#define DBG_COM_TP_DOUBLE_BUFFER 2U
/*==================[type definitions]======================================*/

#if (DBG_COM_VIA_TP_PDUR==STD_ON)
typedef struct
{
    PduIdType txId;
    PduIdType rxId;
}Dbg_ComTpPduRConfigType;

typedef struct
{
    Dbg_DIDsizeType remainingSize;
    Dbg_DIDsizeType currentSize;
    Dbg_DIDsizeType offset;
    const Dbg_DIDsizeType chunkBufferSize;
    const Dbg_BufferPtrType  chunkBuffer;
    P2VAR(uint8,AUTOMATIC,DBG_APPL_DATA) data;
    uint8 flags;
}Dbg_ComTpStateType;
#endif /* #if (DBG_COM_VIA_TP_PDUR==STD_ON) */

/*==================[external function declarations]========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-2 */
#include <MemMap.h>

/** \brief ECU API: Tp implemented service used by Dbg Com part to send single DID frame
 **
 ** This function is used by the Dbg Com part to send a single DID frame.
 ** Function "must be" or "is" implemented by the Dbg specific transport protocol
 ** using other lower layer (PduR, TCP, serial line) for comminucation.
 ** The DID frame data is consistent between the Dbg_Transmit() call
 ** and the corresponding Dbg_TxConfirmation() of the Tp.
 ** This function must implement segmentation, Tp flag, syncronization
 ** with received host requests and ACK sending.
 **
 ** Deviation: In difference to the AUTOSAR SWS the Dbg Com implementation
 **            of the function supports a return value.
 **
 ** \param[in] size size of the DID frame
 ** \param[in] buffer pointer to the DID frame data
 ** \return status of the lower layer
 ** \retval E_OK transmit start was okay
 ** \retval E_NOT_OK start of transmission was not okay, there will be
 **         no Dbg_Confirmation() according with this request.
 **
 ** \ServiceID{DBG_API_TRANSMIT}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType,DBG_CODE) Dbg_Transmit(uint16 size,P2VAR(uint8,AUTOMATIC,DBG_APPL_DATA) buffer);

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-2 */
#include <MemMap.h>

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/
#define DBG_START_SEC_CONST_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>

#if (DBG_COM_VIA_TP_PDUR==STD_ON)
extern CONST(Dbg_ComTpPduRConfigType,DBG_CONST) Dbg_ComTpPduRConfig;
#endif /* #if (DBG_COM_VIA_TP_PDUR==STD_ON) */

#define DBG_STOP_SEC_CONST_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>
/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/
#define DBG_START_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>

#if (DBG_COM_VIA_TP_PDUR==STD_ON)
/* Deviation MISRA-1 */
extern VAR(uint8,DBG_VAR) Dbg_ComTpBuffer[];
extern VAR(Dbg_ComTpStateType,DBG_VAR) Dbg_ComTpState[DBG_COM_TP_DOUBLE_BUFFER];
#endif /* #if (DBG_COM_VIA_TP_PDUR==STD_ON) */

#define DBG_STOP_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( DBG_COM_H ) */
/*==================[end of file]===========================================*/
