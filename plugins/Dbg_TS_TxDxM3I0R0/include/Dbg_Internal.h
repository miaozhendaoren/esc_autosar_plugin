/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_INTERNAL_H)
#define DBG_INTERNAL_H

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 8.12 (required)
 *    When an array is declared with external linkage, its size shall be
 *    stated explicitely or defined implicitely by initialisation.
 *
 *    Reason:
 *    Value is only calculated in .c file and not available here.
 *
 *  MISRA-2) Deviated Rule: 19.13 (advisory)
 *    The # and ## preprocessor operators should not be used.
 *
 *    Reason:
 *    Simplier code and better readable code.
 *
 *  MISRA-3) Deviated Rule: 19.7 (advisory)
 *  A function should be used in preference to a function-like macro.
 *
 *    Reason:
 *    Three reasons for the usage of function-like macros:
 *    - Performance
 *    - Readability
 *    - Possibility to switch Dbg functionality "completely off"
 *
 *  MISRA-4) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
*/

/*==================[inclusions]============================================*/
/*
 * !LINKSTO Dbg.API.Dbg096,1
 */
#include <TSAutosar.h>     /* EB specific standard types */
#include <Std_Types.h>     /* AUTOSAR standard types */
#include <Dbg_Types.h>
#include <Dbg_Cfg.h>
#include <Dbg_Core.h>               /* core specific defines */
#include <Dbg_Com.h>                /* com specific defines */
#if (DBG_DEV_ERROR_DETECT == STD_ON)
#include <Det.h>
#endif
#if (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON)
#include <Os.h>
#endif

/*==================[macros]================================================*/

/*------------------[DEV ERROR]---------------------------------------------*/
#if (DBG_DEV_ERROR_DETECT == STD_ON)
#define DBG_INSTANCE_ID_DET 0

/*
 * DBG_DET_REPORT_ERROR()
 *
 * In development mode, this macro reports the indicated error to the
 * Development error tracer (Det). The parameters are:
 *  a -- API id
 *  e -- error code
*/
/* Deviation MISRA-3 */
#define DBG_DET_REPORT_ERROR(a,e) \
    Det_ReportError(DBG_MODULE_ID, DBG_INSTANCE_ID_DET, (uint8)(a), (e))

#endif
#define DBG_PREDEF_DID_START_NR 235U

/* Deviation MISRA-3 */
#define DBG_DID_TO_INDEX(a) ((a) >= DBG_PREDEF_DID_START_NR ? (Dbg_DIDtype)(((a)-DBG_PREDEF_DID_START_NR)+Dbg_Config.numStdDID) : (a))
/* Deviation MISRA-3 */
#define DBG_DID_IN_RANGE(a) (DBG_IS_STD_DID(a) || DBG_IS_PREDEF_DID(a))
/* Deviation MISRA-3 */
#define DBG_IS_STD_DID(a)   ((a) < Dbg_Config.numStdDID)
/* Deviation MISRA-3 */
#define DBG_IS_PREDEF_DID(a)   ((a) >= DBG_PREDEF_DID_START_NR)
/* use this macro only in DBG_DET_* else it will not optimized away
 * if DET is disabled */
/* Deviation MISRA-3 */
#define DBG_DET_DID_TO_API_ID(did) (Dbg_PredefDidToApiId[((did)-DBG_PREDEF_DID_START_NR)])

/* returns true if global state merged with local state results in an active DID */
/* Deviation MISRA-3 */
#define DBG_IS_DID_ACTIVE(DID) \
    ((Dbg_StateType)( (Dbg_StateType)( \
         (Dbg_StateType)( (Dbg_StateType)(Dbg_Config.DIDstatePtr[DBG_DID_TO_INDEX(DID)] & Dbg_State) & DBG_STATE_MASK ) | \
         (Dbg_StateType)( Dbg_State & (Dbg_StateType)(~DBG_STATE_MASK) ) \
      ) & DBG_STATE_ACTIVE \
    )!=0U?TRUE:FALSE)

/* Deviation MISRA-2 */
/* Deviation MISRA-3 */
#define DBG_STR(a,b) a##b
#define DBG_SIZEOF(a) ((uint8)sizeof(a))

/** \brief static configuration flags used for library
 **
 ** This are static const configuration flags. Not changable at runtime.
 ** Used for checking host requests. For example: enable of time stamp
 ** at runtime does not make sense if complete disabled in user configuration
 */
#define DBG_CFG_FLAG_INIT                          0U
#define DBG_CFG_FLAG_BUFFER_OVERWRITE              1U
#define DBG_CFG_FLAG_TX_BUFFER_ON_REQ              2U
#define DBG_CFG_FLAG_NO_TIMESTAMP                  4U
#define DBG_CFG_FLAG_EMULATE_TIME                  8U
#define DBG_CFG_FLAG_NO_BUFFER                    16U
#define DBG_CFG_FLAG_PROCESSID                    32U

#ifndef DBG_STATIC
#define DBG_STATIC STATIC
#endif /* ifndef DBG_STATIC */

/*------------------[re-entrance]-------------------------------------------*/
/* Deviation MISRA-3 */
#define Dbg_Lock(is)             *(is)=TS_IntDisable()
/* Deviation MISRA-3 */
#define Dbg_Unlock(is)           TS_IntRestore(is)
#if (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON)
/* Deviation MISRA-3 */
#define Dbg_NonTrustedLock(is)   do { TS_PARAM_UNUSED(is); SuspendAllInterrupts(); } while(0)
/* Deviation MISRA-3 */
#define Dbg_NonTrustedUnLock(is) do { TS_PARAM_UNUSED(is); ResumeAllInterrupts(); } while(0)
#else /* #if (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON) */
/* Deviation MISRA-3 */
#define Dbg_NonTrustedLock(is)   *(is)=TS_IntDisable()
/* Deviation MISRA-3 */
#define Dbg_NonTrustedUnLock(is) TS_IntRestore(is)
#endif /* #if (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON) */

/** \brief perform an atomic save and modification
 **
 ** This macro is used by core part for the re-entrance (atomic) access.
 ** \p current will be saved in \p save first to make a copy if the current
 ** lock state. Than \p current will be set to the \p modifyValue.
 **
 ** \param[inout] save save location of current lock state
 ** \param[inout] current is the current lock state
 ** \param[in] modifyValue new current lock state
 */
/* Deviation MISRA-3 */
#define DBG_CRIT_SAVE_MODIFY(save,current,modifyValue) \
    do{ \
        Dbg_IntStatusType intStatus; \
        Dbg_Lock(&intStatus); \
        (save)=(current); \
        (current)=(modifyValue); \
        Dbg_Unlock(intStatus); \
    }while(0)

/*------------------[memory protection]-------------------------------------*/
#if (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON)
/* Write data to buffer */
/* Deviation MISRA-3 */
#define DBG_CORE_MEM_WRITE(a) \
  (void)CallTrustedFunction(Dbg_CoreMemWrite, (TrustedFunctionParameterRefType)(a))
/* Read data from buffer */
/* Deviation MISRA-3 */
#define DBG_CORE_MEM_READ(a) \
  (void)CallTrustedFunction(Dbg_CoreMemRead, (TrustedFunctionParameterRefType)(a))
#else
/* Write data to buffer */
/* Deviation MISRA-3 */
#define DBG_CORE_MEM_WRITE(a) \
  Dbg_CoreMemWrite((P2VAR(void, AUTOMATIC, DBG_APPL_DATA))(a))
/* Read data from buffer */
/* Deviation MISRA-3 */
#define DBG_CORE_MEM_READ(a) \
  Dbg_CoreMemRead((P2VAR(void, AUTOMATIC, DBG_APPL_DATA))(a))
#endif

/*------------------[static predef DID sizes]-------------------------------*/
/** \brief convert DID predef DID name to size
 **
 ** \param[in] name name of the predef DID
 ** \return size of the named DID
 */
/* Deviation MISRA-3 */
#define DBG_PREDEF_DID_SIZE(name)   DBG_STR(DBG_PREDEF_DID_SIZE_,name)
/** \brief static sizes of predefined DID's
 **   - used in generated include file
 **   - needed to save size of predef DID in address size pair array
 */
#define DBG_PREDEF_DID_SIZE_235U     (DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_236U     (0u)
#define DBG_PREDEF_DID_SIZE_237U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_238U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_239U     (DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_240U     (DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_241U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_242U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_243U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_244U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_245U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_246U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_247U     (DBG_SIZEOF(uint16))
#define DBG_PREDEF_DID_SIZE_248U     (DBG_SIZEOF(uint16))
#define DBG_PREDEF_DID_SIZE_249U     (DBG_SIZEOF(uint16))
#define DBG_PREDEF_DID_SIZE_250U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_251U     (0u)
#define DBG_PREDEF_DID_SIZE_252U     (0u)
#define DBG_PREDEF_DID_SIZE_253U     (DBG_SIZEOF(uint16)+DBG_SIZEOF(uint8)+DBG_SIZEOF(uint8))
#define DBG_PREDEF_DID_SIZE_254U     (0u)
#define DBG_PREDEF_DID_SIZE_255U     (0u)

/*------------------[Simple value sizes]------------------------------------*/
/** \brief static sizes of simple value tracing
 **   - used in generated include file
 **   - needed to save size of predef DID in address size pair array
 */
#define DBG_SIMPLE_VALUE_SIZE_UI32  (4u)

/*------------------[architecture independent CONVERT]----------------------*/
/** \brief convert uintXX value to char array
 **
 ** Macro is used for architecture independent convert of
 ** values bigger uint8 to the uint8 based transmit buffer. Dbg module
 ** shall not do any transformation
 **
 ** \param[inout] uint8Ptr pointer to the buffer
 ** \param[in] value uintXX value to be saved in uint8Ptr memory
 **
 ** !LINKSTO Dbg.Assumptions.Dbg004,1
 */
/* Deviation MISRA-3 */
#define DBG_CONVERT_SHORT(uint8Ptr,value) \
    do{ \
        uint16 m_tempValue = (value); \
        (uint8Ptr)[0] = ((uint8*)&(m_tempValue))[0]; \
        (uint8Ptr)[1] = ((uint8*)&(m_tempValue))[1]; \
    }while(0)

/* Deviation MISRA-3 */
#define DBG_CONVERT_LONG(uint8Ptr,value) \
    do{ \
        uint32 m_tempValue = (value); \
        (uint8Ptr)[0] = ((uint8*)&(m_tempValue))[0]; \
        (uint8Ptr)[1] = ((uint8*)&(m_tempValue))[1]; \
        (uint8Ptr)[2] = ((uint8*)&(m_tempValue))[2]; \
        (uint8Ptr)[3] = ((uint8*)&(m_tempValue))[3]; \
    }while(0)

/*==================[type definitions]======================================*/
typedef TS_IntStatusType Dbg_IntStatusType;

/*==================[external function declarations]========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-4 */
#include <MemMap.h>

extern FUNC(void,DBG_CODE) Dbg_CoreTimestampInit(void);
extern FUNC(void,DBG_CODE) Dbg_CoreTimestampDeInit(void);
extern FUNC(void,DBG_CODE) Dbg_Fdx_Init(void);
extern FUNC(void,DBG_CODE) Dbg_Trace_Status
(
  uint8 EcuState,
  P2VAR(uint8, AUTOMATIC, DBG_VAR) DidStateId,
  uint8 DidStateLen
);

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-4 */
#include <MemMap.h>
/*==================[internal function declarations]========================*/
/*==================[external constants]====================================*/
#define DBG_START_SEC_CONST_UNSPECIFIED
/* Deviation MISRA-4 */
#include <MemMap.h>

extern CONST(Dbg_ConfigType,DBG_CONST) Dbg_Config;

/* Deviation MISRA-1 */
extern CONST(uint8,DBG_CONST) Dbg_PredefDidToApiId[];

#define DBG_STOP_SEC_CONST_UNSPECIFIED
/* Deviation MISRA-4 */
#include <MemMap.h>

/*==================[internal constants]====================================*/
/*==================[external data]=========================================*/
#define DBG_START_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-4 */
#include <MemMap.h>

extern VAR(Dbg_StateType,DBG_VAR) Dbg_State;
extern VAR(boolean,DBG_VAR) Dbg_isInit;

extern VAR(Dbg_BufferLockState,DBG_VAR) directBufferLockState;

#define DBG_STOP_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-4 */
#include <MemMap.h>
/*==================[internal data]=========================================*/
/*==================[external function definitions]=========================*/
/*==================[internal function definitions]=========================*/


#endif /* if !defined( DBG_INTERNAL_H ) */
/*==================[end of file]===========================================*/
