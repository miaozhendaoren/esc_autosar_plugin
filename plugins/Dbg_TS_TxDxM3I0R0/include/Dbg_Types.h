/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_TYPES_H)
#define DBG_TYPES_H

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/
typedef uint32 Dbg_DIDaddrType;
typedef uint8  Dbg_DIDsizeType;
typedef uint8  Dbg_NumType;
typedef uint8  Dbg_BufferType;
typedef uint16 Dbg_BufferSizeType;
typedef P2VAR(Dbg_BufferType,AUTOMATIC,DBG_APPL_DATA) Dbg_BufferPtrType;
typedef uint8  Dbg_StateType;
/*
 * !LINKSTO Dbg.Functional.General.Dbg018,1
 */
typedef uint8  Dbg_DIDtype;

typedef struct
{
    Dbg_DIDaddrType addr;
    Dbg_DIDsizeType size;
}Dbg_AddrSizePairType;

typedef struct
{
    Dbg_NumType numIndex;
    Dbg_StateType stateFlags;
    P2CONST(Dbg_AddrSizePairType,AUTOMATIC,AUTOMATIC) tablePtr;
}Dbg_DIDConfigType;

typedef struct
{
    Dbg_NumType numStaticDID;
    Dbg_NumType numDynamicDID;
    Dbg_NumType numPredefDID;
    Dbg_NumType numStdDID;
    Dbg_NumType numDID;
    Dbg_StateType defaultState;  /* default state used before Dbg_Init() */
    Dbg_StateType initState;     /* state set by Dbg_Init() */
    Dbg_StateType cfgFlags;
    Dbg_NumType sizeOfTimestamp;
    uint16      sizeOfDirectBuffer;
    uint16      sizeOfRingBuffer;
    P2CONST(Dbg_DIDConfigType,AUTOMATIC,DBG_APPL_CONST) DIDcfgPtr;
    P2VAR(Dbg_StateType,AUTOMATIC,DBG_APPL_DATA) DIDstatePtr;
}Dbg_ConfigType;


/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( DBG_TYPES_H ) */
/*==================[end of file]============================================*/
