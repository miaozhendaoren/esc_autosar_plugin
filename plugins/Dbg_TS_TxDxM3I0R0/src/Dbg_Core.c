/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/**
 ** This file contains the CORE implementation of the AUTOSAR module Dbg.
 ** It is designed like mem alloc and free.
 **
 ** General description:
 **     The complete Dbg module buffer handling is implemented in this file.
 **     The function in this file can be used like a mem alloc and free.
 **     If the user needs DID specific space write lock request can be performed.
 **     If the DID is active and there is buffer available the user
 **     gets a pointer to store data for this DID.
 **     The frame header handling is done in this core module and must not
 **     be performed by the user.
 **     The read functionality shall be used by the Com part. A valid pointer will
 **     be returned if there is data in the buffer. Priorisation, set
 **     of frame flags (like overflow) will be done by Core module.
 **     The function returned pointer (referenced memory) is valid until a
 **     Unlock is done.
 **
 ** Use cases:
 **     WRITE (used by API functions, cyclic DID collection, ...)
 **     -----
 **        if( currentPtr = WriteLock(DID) != NULL_PTR )
 **        {
 **            currentPtr[0] = data1;
 **            ...
 **            WriteUnlock(currentPtr);
 **        }
 **
 **     READ (used by Com part)
 **     ----
 **     size;
 **     if( currentPtr = ReadLock(&size) != NULL_PTR )
 **     {
 **         PduR_Send(currentPtr,size)
 **         ...
 **         ReadUnlock(currentPtr);
 **     }
 **
 ** Ring Buffer Management: (and deviation from the SWS)
 **     - read ptr == write ptr if buffer is empty
 **     - one byte size at the start of each Dbg frame
 **     - fragmentation at:
 **           - end of ring buffer array -> reason: linear space for the user
 **           - between write and read ptr
 **                     -> reason: avoid that read ptr == write ptr
 **                     -> implemented via: request size+1 but reserve size
 ** */

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 11.4 (advisory)
 *    A cast should not be performed between a pointer to object
 *    type and a different pointer to object type
 *
 *    Reason:
 *    Target independent conversion of data received from an
 *    external source. The destination is uint8, thus no alignment
 *    problems will occur.
 *
 *  MISRA-2) Deviated Rule: 16.7 (advisory)
 *    A pointer parameter in a function prototype should be declared
 *    as pointer to const if the pointer is not used to modify the
 *    addressed object.
 *
 *    Reason:
 *    - The pointer acts as abstraction layer due to the optional
 *      "trustedness" of the function. The addressed object is modified.
 *
 *  MISRA-3) Deviated Rule: 19.7 (advisory)
 *  A function should be used in preference to a function-like macro.
 *
 *    Reason:
 *    Three reasons for the usage of function-like macros:
 *    - Performance
 *    - Readability
 *    - Possibility to switch Dbg functionality "completely off"
 *
 *  MISRA-4) Deviated Rule: 11.3 (advisory)
 *    A cast should not be performed between a pointer type and an
 *    integral type.
 *
 *    Reason:
 *    AUTOSAR NULL_PTR macro.
 *
 *  MISRA-5) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
 *  MISRA-6) Deviated Rule: Rule 16.10 (required)
 *    If a function returns error information, then that error information shall
 *    be tested.
 *
 *    Reason:
 *    Function GetTaskID always return E_OK.
 *
*/

/*==================[inclusions]============================================*/
#include <TSMem.h>
#include <Dbg_Cfg.h>
#include <Dbg_Cfg_Internal.h>
#include <Dbg_Internal.h>

/*==================[version check]=========================================*/

/*==================[macros]================================================*/
/*
 * !LINKSTO Dbg.API.Dbg096,1
 */
#if (DBG_CFG_TIMESTAMP==STD_ON)
#if (DBG_TIMESTAMP_VIA_OS_TIMESTAMP==STD_ON)
#include <Os.h>
#include <Os_kernel.h>
/* Deviation MISRA-3 */
#define DBG_GET_POST_INIT_TIME(a) \
    do{ \
        os_timestamp_t m_time; \
        OS_GetTimeStamp(&m_time); \
        (a)=(uint32)m_time.tsLo; \
    }while(0)
#elif (DBG_TIMESTAMP_VIA_OS==STD_ON)
#include <os.h>
/* Deviation MISRA-3 */
#define DBG_GET_POST_INIT_TIME(a) \
    do{ \
        TickType m_time; \
        GetCounterValue(DBG_TIMESTAMP_CHANNEL,(TickType*)&m_time); \
        (a)=(uint32)m_time; \
    }while(0)
#elif (DBG_TIMESTAMP_VIA_GPT==STD_ON)
#include <Gpt.h>
/* Deviation MISRA-3 */
#define DBG_GET_POST_INIT_TIME(a) \
    do{ \
        (a)=(uint32)Gpt_GetTimeElapsed(DBG_TIMESTAMP_CHANNEL); \
    }while(0)
#elif (DBG_TIMESTAMP_EMU==STD_ON)
/* no Os or Gpt ref configured */
/* Deviation MISRA-3 */
#define DBG_GET_POST_INIT_TIME(a) do{(a)=globalTime++;}while(0)
#else
/* no Gpt or Os ref and no emulation seletcted */
/* Deviation MISRA-3 */
#define DBG_GET_POST_INIT_TIME(a) do{(a)=0;}while(0)
#endif
#else
/* timestamp configuration so i always return 0 */
/* Deviation MISRA-3 */
#define DBG_GET_POST_INIT_TIME(a) do{(a)=0;}while(0)
#endif

#if ((DBG_PROCESSID_SUPPORT == STD_ON) || (DBG_MEMORY_PROTECTION_SUPPORT == STD_ON))
#include <Os.h>
#if (DBG_PROCESSID_SUPPORT == STD_ON)
#include <Os_configuration.h>
#endif
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-5 */
#include <MemMap.h>

DBG_STATIC FUNC(Dbg_StateType, DBG_CODE) Dbg_CoreGetRecordStateAndSize
(
  Dbg_DIDtype DID,
  Dbg_DIDsizeType *headerSize,
  Dbg_DIDsizeType *dataSize
);

DBG_STATIC FUNC(Dbg_BufferPtrType,DBG_CODE) Dbg_CoreGetRingSpace(Dbg_DIDsizeType size);

DBG_STATIC FUNC(uint32,DBG_CODE) Dbg_CoreGetTimestamp(void);

DBG_STATIC FUNC(Dbg_BufferPtrType, DBG_CODE) Dbg_CoreMemWriteLock(Dbg_DIDtype DID,Dbg_DIDsizeType *size);
DBG_STATIC FUNC(Dbg_BufferPtrType, DBG_CODE) Dbg_CoreMemReadLock(Dbg_DIDsizeType *size);

DBG_STATIC FUNC(boolean, DBG_CODE) Dbg_CoreMemWriteUnlock(Dbg_BufferPtrType bufferPtr);
DBG_STATIC FUNC(boolean, DBG_CODE) Dbg_CoreMemReadUnlock(Dbg_BufferPtrType bufferPtr);

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-5 */
#include <MemMap.h>
/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/
#define DBG_START_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-5 */
#include <MemMap.h>

/* lock states */
VAR(Dbg_BufferLockState, DBG_VAR) directBufferLockState = DBG_BUFFER_EMPTY;

DBG_STATIC Dbg_BufferLockState ringReadBufferLockState = DBG_BUFFER_EMPTY;
DBG_STATIC Dbg_BufferLockState ringWriteBufferLockState = DBG_BUFFER_EMPTY;

/* ring buffer pointer
 * !LINKSTO Dbg.Functional.Buffering.Dbg037,1,
 * !        Dbg.Functional.Buffering.Dbg038,1
 */
DBG_STATIC Dbg_BufferPtrType Dbg_RbReadPointer=&Dbg_RingBuffer[0];
DBG_STATIC Dbg_BufferPtrType Dbg_RbWritePointer=&Dbg_RingBuffer[0];
/* frame loss flag */
DBG_STATIC boolean Dbg_BufferOverflow = FALSE;
DBG_STATIC boolean Dbg_BufferClear = FALSE;

#define DBG_STOP_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-5 */
#include <MemMap.h>

#if (DBG_TIMESTAMP_EMU==STD_ON)

#define DBG_START_SEC_VAR_32BIT
/* Deviation MISRA-5 */
#include <MemMap.h>

DBG_STATIC uint32 globalTime=0;

#define DBG_STOP_SEC_VAR_32BIT
/* Deviation MISRA-5 */
#include <MemMap.h>

#endif
/*==================[external function definitions]=========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-5 */
#include <MemMap.h>

#if (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON)
FUNC(void, DBG_CODE) TRUSTED_Dbg_CoreMemWrite
(
  TrustedFunctionIndexType FunctionIndex,
  TrustedFunctionParameterRefType Params
)
#else
FUNC(void, DBG_CODE) Dbg_CoreMemWrite
(
  /* Deviation MISRA-2 */
  P2VAR(void, AUTOMATIC, DBG_APPL_DATA) Params
)
#endif
{
  Dbg_BufferPtrType currentPtr;
  Dbg_DIDsizeType currentSize = 0;
  P2VAR(Dbg_BufferInfoWriteType, AUTOMATIC, DBG_APPL_DATA) BufferInfo =
    (P2VAR(Dbg_BufferInfoWriteType, AUTOMATIC, DBG_APPL_DATA))Params;

#if (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON)
  TS_PARAM_UNUSED(FunctionIndex);
#endif

  currentPtr = Dbg_CoreMemWriteLock(BufferInfo->DID,&currentSize);

  /* Deviation MISRA-4 */
  if( currentPtr != NULL_PTR )
  {
    /* copy data */
    TS_MemCpy(currentPtr, BufferInfo->Src, currentSize);

    if (!Dbg_CoreMemWriteUnlock(currentPtr))
    {
#if (DBG_DEV_ERROR_DETECT == STD_ON)
      /* the return value can be ignored as the specification states
       * that the only possible return value is E_OK */
      (void)DBG_DET_REPORT_ERROR(DBG_API_I_CORE_MEM_WRITE,DBG_E_I_REENTRANCE);
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
    }
  }
}

/*--------------------------------------------------------------------------*/
#if (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON)
FUNC(void, DBG_CODE) TRUSTED_Dbg_CoreMemRead
(
  TrustedFunctionIndexType FunctionIndex,
  TrustedFunctionParameterRefType Params
)
#else
FUNC(void, DBG_CODE) Dbg_CoreMemRead
(
  /* Deviation MISRA-2 */
  P2VAR(void, AUTOMATIC, DBG_APPL_DATA) Params
)
#endif
{
  uint8 MaxSize;
  Dbg_BufferPtrType currentPtr;
  P2VAR(Dbg_BufferInfoReadType, AUTOMATIC, DBG_APPL_DATA) BufferInfo =
    (P2VAR(Dbg_BufferInfoReadType, AUTOMATIC, DBG_APPL_DATA))Params;
  MaxSize = BufferInfo->Size;

#if (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON)
  TS_PARAM_UNUSED(FunctionIndex);
#endif

  currentPtr = Dbg_CoreMemReadLock(&BufferInfo->Size);

  if (BufferInfo->Size <= MaxSize)
  {
    /* Deviation MISRA-4 */
    if( currentPtr != NULL_PTR )
    {
      /* copy data */
      TS_MemCpy(BufferInfo->Dst, currentPtr, BufferInfo->Size);

      (void)Dbg_CoreMemReadUnlock(currentPtr);
    }
  }
#if (DBG_DEV_ERROR_DETECT == STD_ON)
  else
  {
    /* If an error occurred, don't transfer data */
    BufferInfo->Size = 0U;

    /* the return value can be ignored as the specification states
     * that the only possible return value is E_OK */
    (void)DBG_DET_REPORT_ERROR(DBG_API_I_CORE_MEM_READ,DBG_E_I_MEM_SIZE);
  }
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
}

/*--------------------------------------------------------------------------*/
/** \brief get DID frame size and state
 **
 ** The current state is defined by global Dbg module state merged
 ** together with the DID state. Global state overwrites DID state.
 ** Example: If global Dbg state ACTIVE is not set (means Dbg module disabled)
 **          but DID state ACTIVE is set the returned flag ACTIVE is not set.
 **
 ** The returned sizes will be 0 if the DID is not active or invalid.
 **
 ** \param[in] DID           DID number
 ** \param[out] headerSize   size of the DID frame header dependend on time stamp state
 ** \param[out] dataSize     size of the DID data.
 ** \return    current DID state (merged together with global Dbg state)
 ** \retval    DBG_STATE_RELEVANT  bits within this mask
 **/
DBG_STATIC FUNC(Dbg_StateType, DBG_CODE) Dbg_CoreGetRecordStateAndSize
(
  Dbg_DIDtype DID,
  Dbg_DIDsizeType *headerSize,
  Dbg_DIDsizeType *dataSize
)
{
   Dbg_NumType i;
   Dbg_DIDtype index;
   P2CONST(Dbg_DIDConfigType,AUTOMATIC,DBG_APPL_CONST) currentDID;
   Dbg_StateType currentState=DBG_STATE_INIT;
   *headerSize = 0;
   *dataSize = 0;

   index = DBG_DID_TO_INDEX(DID);

   currentDID = &Dbg_Config.DIDcfgPtr[index];

   DBG_STATE_GET_OVERAL(Dbg_Config.DIDstatePtr[index],Dbg_State,currentState);

   /* ring buffer managment record size byte */
   *headerSize += 1;

   /* DID byte and flag byte*/
   *headerSize += 2;
   if( (currentState & (Dbg_StateType)DBG_STATE_TIMESTAMP)!=0U )
   {
      *headerSize += Dbg_Config.sizeOfTimestamp;
   }

#if (DBG_PROCESSID_SUPPORT==STD_ON)
   if (DID != (Dbg_DIDtype)DBG_DID_STATUS)
   {
     *headerSize += DBG_PROCESSID_SIZE;
   }
#endif

   for(i=0;i<currentDID->numIndex;i++)
   {
     *dataSize += currentDID->tablePtr[i].size;
   }

   return currentState;
}
/*--------------------------------------------------------------------------*/
/** \brief check and get ring buffer location
 **
 ** This function allocates requested size of memory [in bytes] in the ring buffer.
 ** If there is no space left due to read lock or overwrite disabled the function
 ** return a NULL ptr.
 **
 ** \param[in] size           requested size
 ** \return   pointer to ring buffer
 ** \retval    NULL_PTR  no buffer available
 ** \retval    !NULL_PTR  buffer with requested size free at returned position
 **/
DBG_STATIC FUNC(Dbg_BufferPtrType,DBG_CODE) Dbg_CoreGetRingSpace(Dbg_DIDsizeType size)
{
    Dbg_BufferLockState savedLockState;
    /* Deviation MISRA-4 */
    Dbg_BufferPtrType currentPtr = NULL_PTR;
    /* Deviation MISRA-4 */
    Dbg_BufferPtrType fallBackPtr = NULL_PTR;
    boolean skip = FALSE;

    /* Deviation MISRA-4 */
    while( (currentPtr == NULL_PTR) && (!skip) )
    {
        /* force cleanup because both pointer can point somewhere to the ring
         * but the size to the end is too small for the request */
        if( Dbg_RbWritePointer == Dbg_RbReadPointer )
        {
          Dbg_RbWritePointer = &Dbg_RingBuffer[0];
          Dbg_RbReadPointer = &Dbg_RingBuffer[0];
        }

        if( Dbg_RbWritePointer >= Dbg_RbReadPointer )
        {
            if( &Dbg_RbWritePointer[size+1] > &Dbg_RingBuffer[Dbg_Config.sizeOfRingBuffer] )
            {
                Dbg_RbWritePointer[0] = size;
                /* set fallback ptr because it could be that:
                 *   - read ptr is still on first position
                 *   - read ptr locked
                 *   - buffer overwrite disabled */
                fallBackPtr = &Dbg_RbWritePointer[0];
                /* go to the start of the ring */
                Dbg_RbWritePointer = &Dbg_RingBuffer[0];
            }
            else
            {
                /* enough space to write needed size */
                currentPtr = Dbg_RbWritePointer;
            }
        }
        /* Deviation MISRA-4 */
        if( currentPtr == NULL_PTR )
        {
            if( &Dbg_RbWritePointer[size+1] > Dbg_RbReadPointer )
            {

                if( (Dbg_Config.cfgFlags & DBG_CFG_FLAG_BUFFER_OVERWRITE)==0U )
                {
                    /* buffer full */
                    skip = TRUE;
                }
                else
                {
                    DBG_CRIT_SAVE_MODIFY(savedLockState,ringReadBufferLockState,DBG_BUFFER_LOCKED);
                    if( savedLockState >= DBG_BUFFER_LOCKED )
                    {
                        /* can not overwrite old frames because current transmission
                         * is in progress */
                        ringReadBufferLockState = savedLockState;
                        skip = TRUE;
                    }
                    else
                    {
                        /*
                         * !LINKSTO Dbg.Functional.Buffering.Dbg044,1
                         */
                        Dbg_DIDsizeType rbSize;
                        rbSize =  Dbg_RbReadPointer[0];
                        if( &Dbg_RbReadPointer[rbSize+1] > &Dbg_RingBuffer[Dbg_Config.sizeOfRingBuffer] )
                        {
                            Dbg_RbReadPointer = &Dbg_RingBuffer[0];
                        }
                        Dbg_RbReadPointer = &Dbg_RbReadPointer[rbSize];
                        Dbg_BufferOverflow = TRUE;
                        /* unlock the read pointer */
                        ringReadBufferLockState = DBG_BUFFER_FULL;
                    }
                    /* do the loop again */
                }
                /* the write ptr has wrapped from the end to the start of the ring
                 * but overwrite is not possioble */
                /* Deviation MISRA-4 */
                if( skip && (fallBackPtr!=NULL_PTR) )
                {
                    Dbg_RbWritePointer = fallBackPtr;
                }
            }
            else
            {
                currentPtr = Dbg_RbWritePointer;
            }
        }
    }
    return currentPtr;
}
/*--------------------------------------------------------------------------*/
/** \brief handle write requested memory allocation
 **
 ** This function allocates memory for the requested DID and returns a valid
 ** pointer if buffer space is available. The found memory location is locked
 ** for the user until an explicit WriteUnlock() is done.
 **
 ** Reasons
 **     - DID inactive
 **     - Dbg module inactive
 **     - direct send DID but directTxbuffer currently in use (locked)
 **     - buffered DID but no ring buffer space (overwrite disabled)
 **     - buffered DID, ring full but transmission active at read ptr (overwrite enabled)
 **
 ** \param[in] DID           DID number
 ** \return   pointer to DID data space.
 ** \retval    NULL_PTR  no buffer available
 ** \retval    !NULL_PTR  buffer for DID data can be used
 **/
DBG_STATIC FUNC(Dbg_BufferPtrType, DBG_CODE) Dbg_CoreMemWriteLock(Dbg_DIDtype DID,Dbg_DIDsizeType *size)
{
    Dbg_DIDsizeType headerSize,dataSize;
    Dbg_StateType   recordState;
    Dbg_BufferLockState savedLockState;
    /* Deviation MISRA-4 */
    Dbg_BufferPtrType currentPtr = NULL_PTR;

    headerSize = 0;
    dataSize = 0;
    recordState = Dbg_CoreGetRecordStateAndSize(DID,&headerSize,&dataSize);

   /**************************************************
    * header size is zero if DID, Dbg module not active
    * or DID number out of range
    **************************************************/
    if( headerSize > 0)
    {
           /**************************************************
            * direct transmission of DID
            **************************************************/
           if( (recordState & DBG_STATE_BUFFERED)==0U )
           {
              if (DBG_SIZE_OF_DIRECT_BUFFER >= (headerSize+dataSize))
              {
                DBG_CRIT_SAVE_MODIFY(savedLockState,directBufferLockState,DBG_BUFFER_WRITE_LOCKED);
                if ( ((savedLockState<DBG_BUFFER_LOCKED) &&
                     ((recordState & (Dbg_StateType)DBG_STATE_DIRECT_TX)!=0U)) &&
                     (savedLockState == DBG_BUFFER_EMPTY) )
                 {
                    /* own atomic buffer for direct transmit, so
                     * you can use always the start at this point */
                    currentPtr = &Dbg_DirectTxBuffer[0];
                 }
                 else
                 {
                   /* restore the lock state */
                   directBufferLockState = savedLockState;
                   Dbg_BufferOverflow = TRUE;
                 }
              }
#if (DBG_DEV_ERROR_DETECT == STD_ON)
              else
              {
                /* DbgDirectBufferSize is configured to small */
                /* the return value can be ignored as the specification states
                 * that the only possible return value is E_OK */
                (void)DBG_DET_REPORT_ERROR(DBG_API_I_CORE_MEM_WRITE_LOCK,DBG_E_I_MEM_SIZE);
              }
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
           }
           /**************************************************
            * buffered transmission of DID
            **************************************************/
           else
           {
               DBG_CRIT_SAVE_MODIFY(savedLockState,ringWriteBufferLockState,DBG_BUFFER_WRITE_LOCKED);
               if ( (savedLockState<DBG_BUFFER_LOCKED) &&
                    ((recordState & (Dbg_StateType)DBG_STATE_BUFFERED)!=0U))
               {
                  /* lock search within the buffer */
                  currentPtr=Dbg_CoreGetRingSpace(headerSize+dataSize);
                  /* Deviation MISRA-4 */
                  if(currentPtr == NULL_PTR)
                  {
                      /* unlock if no space available, else it remains locked */
                      ringWriteBufferLockState = savedLockState;
                      Dbg_BufferOverflow = TRUE;
                  }
                  else
                  {
                    /* increase internal buffer entry counter */
                    Dbg_BufferCount++;
                  }
               }
           }
           /**************************************************
            * prepare frame header
            **************************************************/
           /* Deviation MISRA-4 */
           if( currentPtr != NULL_PTR )
           {
             currentPtr[0] = headerSize+dataSize; /* core mem managment */
             currentPtr[1] = DID;
             currentPtr[2] = DBG_MASK_FRAME_STATE(recordState);
             if( Dbg_BufferOverflow )
             {
                 currentPtr[2] |= 0x08U;
                 Dbg_BufferOverflow = FALSE;
             }
             /* timestamp */
             if( (recordState & (Dbg_StateType)DBG_STATE_TIMESTAMP)!=0U )
             {
                uint32 currentTimeStamp = Dbg_CoreGetTimestamp();
                if(Dbg_Config.sizeOfTimestamp > 2)
                {
                    /* Deviation MISRA-1 */
                    DBG_CONVERT_LONG(&currentPtr[3],currentTimeStamp);
                }
                else
                {
                    /* Deviation MISRA-1 */
                    DBG_CONVERT_SHORT(&currentPtr[3],(uint16)currentTimeStamp);
                }
             }
             /* process id */
#if (DBG_PROCESSID_SUPPORT==STD_ON)
             {
               if (DID != (Dbg_DIDtype)DBG_DID_STATUS)
               {
                 Dbg_ProcessIdType processId = (Dbg_ProcessIdType)GetISRID();

                 if (processId != (Dbg_ProcessIdType)INVALID_ISR)
                 {
                   processId += OS_NTASKS;
                 }
                 else
                 {
                   /* Deviation MISRA-3 */
                   (void)GetTaskID((TaskRefType)&processId);
                 }

#if (DBG_PROCESSID_SIZE==1)
                 currentPtr[headerSize-DBG_PROCESSID_SIZE] = processId;
#elif (DBG_PROCESSID_SIZE==2)
                 /* Deviation MISRA-1 */
                 DBG_CONVERT_SHORT(&currentPtr[headerSize-DBG_PROCESSID_SIZE], processId);
#else
                 /* Deviation MISRA-1 */
                 DBG_CONVERT_LONG(&currentPtr[headerSize-DBG_PROCESSID_SIZE], processId);
#endif
               }
             }
#endif

             /* core user shall not handle the frame header, so this
              * function returns pointer to the data space */
             currentPtr = &currentPtr[headerSize];

             if( dataSize == 0 )
             {
                /* force mem unlock right now because DID has no user data */
                if( !Dbg_CoreMemWriteUnlock(currentPtr) )
                {
#if (DBG_DEV_ERROR_DETECT == STD_ON)
                    /* the return value can be ignored as the specification
                     * states that the only possible return value is E_OK */
                    (void)DBG_DET_REPORT_ERROR(DBG_API_I_CORE_MEM_WRITE_LOCK,DBG_E_I_REENTRANCE);
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
                }
                /* no data needed from user */
                /* Deviation MISRA-4 */
                currentPtr = NULL_PTR;
             }
           }
    }

    *size = dataSize;
    return currentPtr;
}
/*--------------------------------------------------------------------------*/
/** \brief unlock the previous write locked memory
 **
 ** This function must be used by the user if the frame data is stored at
 ** the allocated location.
 ** After this call the direct, buffered transmission or new
 ** WriteLock() on the same buffer (direct/ring) are possible.
 ** The pointer will be consumed/destroyed.
 **
 ** \param[in] bufferPtr pointer to the allocated frame data
 ** \retval    true pointer is direct or buffered space
 ** \retval    false pointer mismatch (out of range)
 **/

/* FIXME: possible check that pointer points to the locked frame */
DBG_STATIC FUNC(boolean, DBG_CODE) Dbg_CoreMemWriteUnlock(Dbg_BufferPtrType bufferPtr)
{
    Dbg_BufferLockState savedLockState;
    boolean result = FALSE;

    /* which buffer */
    if( (bufferPtr >= &Dbg_DirectTxBuffer[0]) &&
        (bufferPtr <  &Dbg_DirectTxBuffer[Dbg_Config.sizeOfDirectBuffer]) )
    {
        DBG_CRIT_SAVE_MODIFY(savedLockState,directBufferLockState,DBG_BUFFER_LOCKED);
        if( savedLockState == DBG_BUFFER_WRITE_LOCKED )
        {
            directBufferLockState = DBG_BUFFER_FULL;
            result = TRUE;
        }
        else
        {
            directBufferLockState = savedLockState;
        }
    }
    else
    {
        /* ring buffer */
        if ( (bufferPtr >= &Dbg_RingBuffer[0]) &&
             (bufferPtr <  &Dbg_RingBuffer[Dbg_Config.sizeOfRingBuffer]) )
        {
             /* go to common locked state
              *  - always locked against further write access */
            DBG_CRIT_SAVE_MODIFY(savedLockState,ringWriteBufferLockState,DBG_BUFFER_LOCKED);

            if( savedLockState == DBG_BUFFER_WRITE_LOCKED )
            {
                /* user always gets a pointer with enough linear space
                 * for his data, so ring wrap arpund handling is not needed */
                Dbg_RbWritePointer = &Dbg_RbWritePointer[Dbg_RbWritePointer[0]];
                ringWriteBufferLockState = DBG_BUFFER_FULL;
                /* data available but it is no direct data */
                result = TRUE;
            }
            else
            {
                /* restore the lock state */
                ringWriteBufferLockState = savedLockState;

                /* FIXME: (bad point) should never occur, else the
                 * return value of the core function must be used because the memory
                 * could not be freed in higher prior content */
            }
            /* buffer clear supported for ring buffer only */
            if( Dbg_BufferClear )
            {
                Dbg_ClearBuffer();
            }
        }
        else
        {
#if (DBG_DEV_ERROR_DETECT == STD_ON)
            /* the return value can be ignored as the specification states
             * that the only possible return value is E_OK */
            (void)DBG_DET_REPORT_ERROR(DBG_API_I_CORE_MEM_WRITE_UNLOCK,DBG_E_I_MEM_FREE);
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
        }
    }
    return result;
}

/*--------------------------------------------------------------------------*/
/** \brief get priorised next frame (locked)
 **
 ** This function returns a pointer to the next frame.
 ** Direct send prior to ring buffered frames. If a DID is not marked
 ** as direct send it is automatically buffered.
 ** ReadUnlock() must be called if the caller has finished
 ** acccess to the frame data. The pointer/memory will be invalid.
 **
 ** Frame structure:
 **      [DID|flags|timestamp|   data    ]
 **
 ** \param[out] size of the frame
 ** \return pointer to the start of the frame starting with DID number
 ** \retval    NULL_PTR no frame in the buffer
 ** \retval    !NULL_PTR frame
 **/
DBG_STATIC FUNC(Dbg_BufferPtrType,DBG_CODE) Dbg_CoreMemReadLock(Dbg_DIDsizeType *size)
{
    Dbg_BufferLockState savedLockState;
    /* Deviation MISRA-4 */
    Dbg_BufferPtrType currentPtr = NULL_PTR;

    *size=1; /* compenaste -1 at the and to correct buffer managment byte */

    DBG_CRIT_SAVE_MODIFY(savedLockState,directBufferLockState,DBG_BUFFER_LOCKED);
    /* direct buffer has higher prio than ring buffer */
    if( savedLockState == DBG_BUFFER_FULL )
    {
        /* lock before use */
        directBufferLockState = DBG_BUFFER_READ_LOCKED;
        *size=Dbg_DirectTxBuffer[0];
        currentPtr = &Dbg_DirectTxBuffer[1];
    }
    else
    {
        directBufferLockState = savedLockState;
        if( Dbg_RbWritePointer != Dbg_RbReadPointer )
        {
            DBG_CRIT_SAVE_MODIFY(savedLockState,ringReadBufferLockState,DBG_BUFFER_LOCKED);
            if( savedLockState < DBG_BUFFER_LOCKED )
            {
                ringReadBufferLockState = DBG_BUFFER_READ_LOCKED;
                *size = Dbg_RbReadPointer[0];
                /* dummy frame at the end */
                if( &Dbg_RbReadPointer[(*size)+1] > &Dbg_RingBuffer[Dbg_Config.sizeOfRingBuffer] )
                {
                    Dbg_RbReadPointer = &Dbg_RingBuffer[0];
                }
                currentPtr = &Dbg_RbReadPointer[1];
                /* decrease entry counter */
                Dbg_BufferCount--;
            }
            else
            {
                ringReadBufferLockState = savedLockState;
            }
        }
    }
    *size -=1;

    return currentPtr;
}
/*--------------------------------------------------------------------------*/
/** \brief unlock the current read frame
 **
 ** This function consumes the current read locked pointer.
 **
 ** \param[in] bufferPtr
 ** \return pointer to the start of the frame starting with DID number
 ** \retval    true pointer correct consumed
 ** \retval    false failure
 **/
DBG_STATIC FUNC(boolean,DBG_CODE) Dbg_CoreMemReadUnlock(Dbg_BufferPtrType bufferPtr)
{
    Dbg_BufferLockState savedLockState;
    boolean result = FALSE;

    /* which buffer */
    if( (bufferPtr >= &Dbg_DirectTxBuffer[0]) &&
        (bufferPtr <  &Dbg_DirectTxBuffer[Dbg_Config.sizeOfDirectBuffer]) )

    {
        DBG_CRIT_SAVE_MODIFY(savedLockState,directBufferLockState,DBG_BUFFER_LOCKED);
        if( savedLockState == DBG_BUFFER_READ_LOCKED )
        {
            directBufferLockState = DBG_BUFFER_EMPTY;
            result = TRUE;
        }
        else
        {
            directBufferLockState = savedLockState;
        }
    }
    else if ( (bufferPtr >= &Dbg_RingBuffer[0]) &&
              (bufferPtr <  &Dbg_RingBuffer[Dbg_Config.sizeOfRingBuffer]) )

    {
        DBG_CRIT_SAVE_MODIFY(savedLockState,ringReadBufferLockState,DBG_BUFFER_LOCKED);
        if( savedLockState == DBG_BUFFER_READ_LOCKED )
        {
            /* user always gets a pointer with enough linear space
             * for his data, so ring wrap arpund handling is not needed */
            Dbg_RbReadPointer = &Dbg_RbReadPointer[Dbg_RbReadPointer[0]];
            ringReadBufferLockState = DBG_BUFFER_EMPTY;
            result = TRUE;
        }
        else
        {
            ringReadBufferLockState = savedLockState;
        }
        /* buffer clear supported for ring buffer only */
        if( Dbg_BufferClear )
        {
            Dbg_ClearBuffer();
        }
    }
    else
    {
#if (DBG_DEV_ERROR_DETECT == STD_ON)
        /* the return value can be ignored as the specification states
         * that the only possible return value is E_OK */
        (void)DBG_DET_REPORT_ERROR(DBG_API_I_CORE_MEM_READ_UNLOCK,DBG_E_I_MEM_FREE);
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
    }
    return result;
}
/*--------------------------------------------------------------------------*/
/** \brief clear the ring buffer
 **
 ** This function tried to clear the ring buffer. How to do this
 ** depends on the current read and/or write lock state.
 ** If the core is read locked and write locked at the same time
 ** the request will be synchronized with the next unlock function use.
 **/
FUNC(void,DBG_CODE) Dbg_ClearBuffer(void)
{
    Dbg_IntStatusType intStatus;
    Dbg_Lock(&intStatus);

    Dbg_BufferClear = FALSE;

    if( (ringReadBufferLockState < DBG_BUFFER_LOCKED) &&
        (ringWriteBufferLockState < DBG_BUFFER_LOCKED)   )
    {
        /* read and write not locked */
        Dbg_RbWritePointer = Dbg_RbReadPointer;
        /* reset entry counter */
        Dbg_BufferCount = 0;
        Dbg_BufferOverflow = FALSE;
    }else
    if( (ringReadBufferLockState < DBG_BUFFER_LOCKED) &&
        (ringWriteBufferLockState >= DBG_BUFFER_LOCKED)   )
    {
        /* write locked, i can manipulate the read pointer */
        Dbg_RbReadPointer = Dbg_RbWritePointer;
        /* reset entry counter */
        Dbg_BufferCount = 0;
        Dbg_BufferOverflow = FALSE;
    }
    else
    if( (ringReadBufferLockState >= DBG_BUFFER_LOCKED) &&
        (ringWriteBufferLockState < DBG_BUFFER_LOCKED)   )
    {
        /* read locked, i can manipulate the write pointer */
        Dbg_RbWritePointer = &Dbg_RbReadPointer[Dbg_RbReadPointer[0]];
        /* reset entry counter */
        Dbg_BufferCount = 0;
        Dbg_BufferOverflow = FALSE;
    }
    else
    {
        /* save request because both (read and write) pointer are currently
         * in use */
         Dbg_BufferClear = TRUE;
    }

    Dbg_Unlock(intStatus);
}

/*--------------------------------------------------------------------------*/
FUNC(void,DBG_CODE) Dbg_CoreTimestampInit(void)
{
#if (DBG_TIMESTAMP_VIA_GPT==STD_ON)
    Gpt_StartTimer(DBG_TIMESTAMP_CHANNEL,DBG_TIMESTAMP_GPT_MAX_VAL);
#endif
}
/*--------------------------------------------------------------------------*/
FUNC(void,DBG_CODE) Dbg_CoreTimestampDeInit(void)
{
#if (DBG_TIMESTAMP_VIA_GPT==STD_ON)
    Gpt_StopTimer(DBG_TIMESTAMP_CHANNEL);
#endif
#if (DBG_TIMESTAMP_EMU==STD_ON)
    globalTime=0;
#endif
}
/*==================[internal function definitions]=========================*/
DBG_STATIC FUNC(uint32,DBG_CODE) Dbg_CoreGetTimestamp(void)
{

    uint32 tempTime=0;
    if( !Dbg_isInit )
    {
        /* only emulated time available before Dbg_Init() */
#if (DBG_TIMESTAMP_EMU==STD_ON)
        tempTime=globalTime++;
#endif
    }
    else
    {
        DBG_GET_POST_INIT_TIME(tempTime);
    }
    return tempTime;
}
#define DBG_STOP_SEC_CODE
/* Deviation MISRA-5 */
#include <MemMap.h>

/*==================[end of file]===========================================*/
