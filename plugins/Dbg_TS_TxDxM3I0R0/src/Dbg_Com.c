/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/**
 ** This file contains the COM implementation of the AUTOSAR module Dbg.
 ** Tasks:
 **    Transmissions
 **      - calls transport layer specific function Dbg_Transmit()
 **      - event triggered by the core part every time new data are available
 **        in core buffers
 **      - empty the core buffer according current transmission state
 **      - event triggered by Tx confirmation
 **      - error handling (last request remains active and will be
 **        used again after new core trigger) on lower layer problems
 **    Reception (host request)
 **      - process host received data
 **      - call API function, change Com state or use Core part functions
 **          to perform the requested functionallity
 **    Transmission states:
 **    - next N send request
 **    - continous send
 **    - send stopped
 **
 ** Using:
 **    - core functions
 **    - API functions
 **    - lower TP layer Dbg_Transmit()
 ** Callbacks:
 **    Dbg_Confirmation() and Dbg_Indication()
 ** */

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 17.4 (required)
 *    Array indexing shall be the only allowed form of pointer arithmetic.
 *
 *    Reason:
 *    Optimization for buffer handling.
 *
 *  MISRA-2) Deviated Rule: 11.3 (advisory)
 *    A cast should not be performed between a pointer type and an
 *    integral type.
 *
 *    Reason:
 *    AUTOSAR NULL_PTR macro.
 *
 *  MISRA-3) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
 *  MISRA-4) Deviated Rule: 6.3 (advisory)
 *    'typedefs' that indicate size and signedness should be used
 *    in place of the basic types.
 *
 *    Reason:
 *    Function prototype and arguments are defined by the library
 *    supplier.
 *
*/

/*==================[inclusions]============================================*/
#include <Dbg_Cfg.h>
#include <Dbg_Cfg_Internal.h>
#include <Dbg.h>
#include <Dbg_Internal.h>
#include <Dbg_Cbk.h>
#if (DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON)
#include <t32fdx.h>
#endif
#if (DBG_TCPIP_SUPPORT==1)
#include <Dbg_TcpIp.h>
#endif
/*==================[version check]=========================================*/

/*==================[macros]================================================*/
#define DBG_CONTINOUS_SENDING 2U

/* two times the size of data to send */
#define FDXTEST_BUFSIZE (2U*DBG_TX_BUFFER_SIZE)

#define DBG_TX_NORM_DID 0U
#define DBG_TX_ACK_DID 1U
/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-3 */
#include <MemMap.h>

#if (DBG_COM_VIA_TP_PDUR==STD_ON)
DBG_STATIC FUNC(Std_ReturnType,DBG_CODE) Dbg_ComTpSendNext(uint8 stateIndex);
#endif /* #if (DBG_COM_VIA_TP_PDUR==STD_ON) */

#if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF)
static FUNC(void,DBG_CODE) Dbg_GetStatus(sint32 txCounter);
#endif /* #if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF) */

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-3 */
#include <MemMap.h>
/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/
#define DBG_START_SEC_VAR_8BIT
/* Deviation MISRA-3 */
#include <MemMap.h>

#if (DBG_COM_VIA_TP_PDUR==STD_ON)
DBG_STATIC uint8 Dbg_currentIdx=DBG_TX_NORM_DID;
#endif

#if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF)
DBG_STATIC uint8 Dbg_txBuffer[DBG_TX_BUFFER_SIZE];
#endif

#define DBG_STOP_SEC_VAR_8BIT
/* Deviation MISRA-3 */
#include <MemMap.h>

#define DBG_START_SEC_VAR_32BIT
/* Deviation MISRA-3 */
#include <MemMap.h>

/* -1 -> continous
 *  0 -> stopped
 * >0 -> n frames to send */
sint32 Dbg_txCounter = 0;

#define DBG_STOP_SEC_VAR_32BIT
/* Deviation MISRA-3 */
#include <MemMap.h>

#define DBG_START_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-3 */
#include <MemMap.h>

#if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF)
/* FIXME: optimization, core module can handle direct and ring buffer
 *        separated. so it could be possible to implement
 *        one PduR id for direct transmission and an other for ring
 *        buffer transmission */
/* Deviation MISRA-2 */
DBG_STATIC Dbg_BufferPtrType Dbg_currentTxPtr = NULL_PTR;
/* used for re-transmission detection */
DBG_STATIC boolean Dbg_lastFrameError = FALSE;
#endif

#define DBG_STOP_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-3 */
#include <MemMap.h>

#if (DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON)
/* Deviation MISRA-4 */
T32_Fdx_DefineChannel(FdxTestSendBuffer,FDXTEST_BUFSIZE);
#endif /* #if (DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON) */

/*==================[external function definitions]=========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-3 */
#include <MemMap.h>

#if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF)
/*--------------------------------------------------------------------------*/
FUNC(Std_ReturnType,DBG_CODE) Dbg_Indication(P2CONST(uint8,AUTOMATIC,AUTOMATIC) Buffer)
{
    Std_ReturnType result = E_OK;
    uint8 command = Buffer[0];
    /* don't process receiving commands without Dbg_Init(),
     * done by the ECU startup */

#if (DBG_DEV_ERROR_DETECT == STD_ON)
    if(!Dbg_isInit)
    {
        /* the return value can be ignored as the specification states
         * that the only possible return value is E_OK */
        (void)DBG_DET_REPORT_ERROR(DBG_API_INDICATION,DBG_E_I_NO_INIT);
        result = E_NOT_OK;
    }
    else
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
    {
        switch(command)
        {

            case 255U: /* transparent write access */
                /* not yet implemented */
                result = E_NOT_OK;
                break;
            case 254U: /* transparent read access */
                result = E_NOT_OK; /* not yet implemented */
                break;

            case 253U: /* global DID collection on/off */
                Dbg_EnableDidCollection(Buffer[1]);
                break;
            case 252U: /* individual DID collection on/off */
                Dbg_ActivateDid(Buffer[1],Buffer[2]);
                break;
            case 251U: /* global timestamp on/off */
                Dbg_UseLocalTimestampActivation(Buffer[1]);
                break;
            case 250U: /* individual timestamp on/off */
                Dbg_ActivateTimestamp(Buffer[1],Buffer[2]);
                break;
            case 249U: /* individual DID buffering on/off */
                Dbg_ActivateDidBuffering(Buffer[1],Buffer[2]);
                break;
            case 248U: /* clear buffer */
                Dbg_ClearBuffer();
                break;
            case 247U: /* send next n entries */
                Dbg_SendNextEntries(Buffer[1]);
                break;
            case 246U: /* send contionously */
                Dbg_StartContinousSend();
                break;
            case 245U: /* stop send */
                Dbg_StopSend();
                break;
            case 244U: /* set data collection tick */
                /* not yet implemented */
                result = E_NOT_OK;
                break;
            /* specification leak -> predefined DID can not be collected manually
               because collected data is missing */
            case 243U:
            case 242U:
            case 241U:
            case 240U:
            case 239U:
            case 238U:
            case 237U:
            case 236U:
                result = E_NOT_OK;
                break;
#if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF)
            case 37U: /* get status of configuration */
                Dbg_GetStatus(Dbg_txCounter);
                break;
#endif
            default: /* collect DID (no problem if not configured) */
                Dbg_CollectDid(command);
                break;
        }
    }
    return result;
}
/*--------------------------------------------------------------------------*/
/*
 * !LINKSTO Dbg.Functional.Communication.Dbg088,1
 */
FUNC(void,DBG_CODE) Dbg_Confirmation(Std_ReturnType success)
{
    /* Deviation MISRA-2 */
    if( (Dbg_currentTxPtr != NULL_PTR) || (!Dbg_isInit) )
    {
        if( success == E_OK )
        {
            if( Dbg_txCounter >= 1 )
            {
                Dbg_txCounter--;
            }
            /* Deviation MISRA-2 */
            Dbg_currentTxPtr = NULL_PTR;
            Dbg_lastFrameError = FALSE;
        }
        else
        {
           /* force re-transmission on next Com part activation */
           Dbg_lastFrameError = TRUE;
        }
    }
    else
    {
#if (DBG_DEV_ERROR_DETECT == STD_ON)
        /* the return value can be ignored as the specification states
         * that the only possible return value is E_OK */
        (void)DBG_DET_REPORT_ERROR(DBG_API_CONFIRMATION,DBG_E_I_CONFIRM);
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
    }
}
#endif /* #if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF) */

#if (DBG_COM_VIA_TP_PDUR==STD_ON)
/*--------------------------------------------------------------------------*/
/*
 * !LINKSTO Dbg.Functional.Communication.Dbg085,1
 */
FUNC(Std_ReturnType,DBG_CODE) Dbg_Transmit
(
  uint16 size,
  P2VAR(uint8,AUTOMATIC,DBG_APPL_DATA) buffer
)
{
    Dbg_IntStatusType intStatus;
    Std_ReturnType result = E_NOT_OK;
    uint8 tempIdx;

    Dbg_ComTpState[DBG_TX_NORM_DID].data = buffer;
    Dbg_ComTpState[DBG_TX_NORM_DID].offset = 2;
    /* Deviation MISRA-1 */
    Dbg_ComTpState[DBG_TX_NORM_DID].chunkBuffer[0] = *(Dbg_ComTpState[DBG_TX_NORM_DID].data);
    Dbg_ComTpState[DBG_TX_NORM_DID].data++;
    /* Deviation MISRA-1 */
    Dbg_ComTpState[DBG_TX_NORM_DID].chunkBuffer[1] = *(Dbg_ComTpState[DBG_TX_NORM_DID].data);
    Dbg_ComTpState[DBG_TX_NORM_DID].data++;
    /* SET_TP1ST will be processed prior to DEL_ */
    Dbg_ComTpState[DBG_TX_NORM_DID].flags = (DBG_COM_TP_FLG_SET_TP1ST|DBG_COM_TP_FLG_DEL_TP1ST);

    Dbg_NonTrustedLock(&intStatus);
    Dbg_ComTpState[DBG_TX_NORM_DID].remainingSize = (Dbg_DIDsizeType)(size-2);
    tempIdx=Dbg_currentIdx;
    Dbg_NonTrustedUnLock(intStatus);

    if(tempIdx == DBG_TX_NORM_DID)
    {
        result = Dbg_ComTpSendNext(DBG_TX_NORM_DID);
    }
    else
    {
        result=E_OK;
    }
    return result;
}
/*--------------------------------------------------------------------------*/
FUNC(void, DBG_CODE) Dbg_RxIndication
(
    PduIdType DbgRxPduId,
    P2VAR(PduInfoType, AUTOMATIC, DBG_APPL_DATA) PduInfoPtr
)
{
    TS_PARAM_UNUSED(DbgRxPduId);

    if(Dbg_Indication(PduInfoPtr->SduDataPtr) == E_OK)
    {
        /* prepare ACK request
         * FIXME: could be optimized because it can be done in Init() and
         *        will be fix for the whole runtime */

        Dbg_ComTpState[DBG_TX_ACK_DID].currentSize = 1U;
        Dbg_ComTpState[DBG_TX_ACK_DID].offset = Dbg_ComTpState[DBG_TX_ACK_DID].chunkBufferSize;
        Dbg_ComTpState[DBG_TX_ACK_DID].chunkBuffer[0] = 0xFFU;

        Dbg_ComTpState[DBG_TX_ACK_DID].remainingSize = 1U;

        /* check if there is an transmission running */

        if(Dbg_ComTpState[DBG_TX_NORM_DID].remainingSize == 0U)
        {
            Dbg_currentIdx=DBG_TX_ACK_DID;
            /* no error handling for internal ack */
            (void)Dbg_ComTpSendNext(Dbg_currentIdx);
        }
    }
}
/*--------------------------------------------------------------------------*/
FUNC(void, DBG_CODE) Dbg_TxConfirmation
(
    PduIdType DbgTxPduId
)
{
    Std_ReturnType result=E_OK;
    boolean wasAck=FALSE;

    TS_PARAM_UNUSED(DbgTxPduId);

    Dbg_ComTpState[Dbg_currentIdx].remainingSize -= Dbg_ComTpState[Dbg_currentIdx].currentSize;

    /* switch back to default idx=0
     *   - use case 3. -> size was changed to zero by the line above
     */
    if(Dbg_currentIdx==DBG_TX_ACK_DID)
    {
        wasAck=TRUE;
        Dbg_currentIdx=DBG_TX_NORM_DID;
    }

    /* use case 3. -> no affect
     * use case 2. -> handle queued ack */
    if(Dbg_ComTpState[DBG_TX_ACK_DID].remainingSize > 0)
    {
        Dbg_currentIdx=DBG_TX_ACK_DID;
        /* no error handling for internal TP ack */
        (void)Dbg_ComTpSendNext(Dbg_currentIdx);
    }
    else if( Dbg_ComTpState[DBG_TX_NORM_DID].remainingSize > 0 )
    {
        /* note: use case 4. FLG_SET_TP1ST could be still pending
         *       then the DEL flag will be scheduled next round
         *       because the frame was not started in Dbg_ComSend() */
        result = Dbg_ComTpSendNext(Dbg_currentIdx);
    }
    else
    {

    }

    /* 1. if Dbg_ComTpSendNext() returns with error
     *    Dbg_ComTpState[DBG_TX_NORM_DID].remainingSize should be still > 0
     * 2. do not call CmTxCconfirmation for ack requested by RxIndication() */
    if( (!wasAck) && (Dbg_ComTpState[DBG_TX_NORM_DID].remainingSize == 0))
    {
        Dbg_Confirmation(E_OK);
    }

    if( result != E_OK )
    {
        Dbg_Confirmation(E_NOT_OK);
    }
}
/*--------------------------------------------------------------------------*/
FUNC(void, DBG_CODE) Dbg_TriggerTransmit
(
    PduIdType DbgTxPduId,
    P2VAR(PduInfoType, AUTOMATIC, DBG_APPL_DATA) PduInfoPtr
)
{
    uint8 i;

    TS_PARAM_UNUSED(DbgTxPduId);

    for(i=0;i<(Dbg_ComTpState[Dbg_currentIdx].currentSize+Dbg_ComTpState[Dbg_currentIdx].offset);i++)
    {
        PduInfoPtr->SduDataPtr[i]=Dbg_ComTpState[Dbg_currentIdx].chunkBuffer[i];
    }
}
#endif /* #if (DBG_COM_VIA_TP_PDUR==STD_ON) */

#if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF)
/*--------------------------------------------------------------------------*/
FUNC(void,DBG_CODE) Dbg_SendNextEntries(uint8 NrOfDids)
{
    Dbg_txCounter = (sint32)NrOfDids;

    if (Dbg_txCounter == 0)
    {
      Dbg_txCounter=(sint32)Dbg_BufferCount;
    }
}
/*--------------------------------------------------------------------------*/
FUNC(void,DBG_CODE) Dbg_StartContinousSend(void)
{
    Dbg_txCounter = -1;
}
/*--------------------------------------------------------------------------*/
FUNC(void,DBG_CODE) Dbg_StopSend(void)
{
    Dbg_txCounter = 0;
}
/*--------------------------------------------------------------------------*/
/* not used/changed functions
 *   - Dbg_TransmitSegmentedData -> ring buffer handling segmented
 */
FUNC(void,DBG_CODE) Dbg_MainFunction(void)
{
    if( Dbg_isInit )
    {
      /* static
       * Reason: Dbg_lastFrameError handling needs the last size */
      DBG_STATIC Dbg_DIDsizeType lastsize = 0U;

      /* start a new transmission */
      /* Deviation MISRA-2 */
      if( Dbg_currentTxPtr == NULL_PTR )
      {
          if( (Dbg_txCounter != 0) || (directBufferLockState == DBG_BUFFER_FULL) )
          {
              Dbg_BufferInfoReadType BufferInfo;
              BufferInfo.Size = DBG_TX_BUFFER_SIZE;
              BufferInfo.Dst = &Dbg_txBuffer[0];

              /* read data from buffer */
              DBG_CORE_MEM_READ(&BufferInfo);

              /* new data available? */
              if (BufferInfo.Size > 0U)
              {
                lastsize = BufferInfo.Size;
                Dbg_currentTxPtr = &Dbg_txBuffer[0];
              }
          }
          /* Deviation MISRA-2 */
          if( (Dbg_currentTxPtr != NULL_PTR) && (lastsize>0U))
          {
              Dbg_lastFrameError = FALSE; /* avoid problems on TxConfirmation bypass */
              if( Dbg_Transmit(lastsize,Dbg_currentTxPtr) != E_OK )
              {
                  /* error, no tx confirmation
                   *   re-transmission on next Com part activation because
                   * currenTxPtr remains active */
                  Dbg_lastFrameError = TRUE;
              }
          }
      /* Deviation MISRA-2 */
      } else if( (Dbg_currentTxPtr != NULL_PTR) && Dbg_lastFrameError )
      {
          /* retransmit current read locked frame */
          Dbg_lastFrameError = FALSE;
          if( Dbg_Transmit(lastsize,Dbg_currentTxPtr) != E_OK )
          {
              /* error, no tx confirmation
               *   re-transmission on next Com part activation because
               * currenTxPtr remains active */
              Dbg_lastFrameError = TRUE;
          }
      }
      else
      {
          /* nothing to current transmission in progress */
          lastsize = 110; /* debugging breakpoint */
      }
    }
}
#endif /* #if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF) */
/*--------------------------------------------------------------------------*/
/*
 ** This function provides the implementation of Dbg_Activate(). This function
 ** can be used to:
 **    - switch DID specific time stamp on/off
 **    - switch DID  on/off
 **    - switch DID storage between ring buffer or direct buffer
 **    - enable/disable global DID collection
 **    - enable/disable global time stamp.
*/
FUNC(void, DBG_CODE) Dbg_Activate(uint8 Did, boolean status,Dbg_activationType activation)
{
#if (DBG_DEV_ERROR_DETECT == STD_ON)
    /* not initisalized yet */
    if( !Dbg_isInit )
    {
        /* use enum type value of activation for API Id */
        /* the return value can be ignored as the specification states
         * that the only possible return value is E_OK */
        (void)DBG_DET_REPORT_ERROR(activation,DBG_E_I_NO_INIT);
    }else
    if( !DBG_DID_IN_RANGE(Did) )
    {
        /* use enum type value of activation for API Id */
        /* the return value can be ignored as the specification states
         * that the only possible return value is E_OK */
        (void)DBG_DET_REPORT_ERROR(activation,DBG_E_INVALID_DID);
    }
    else
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
    {
        Dbg_DIDtype index;
        Dbg_IntStatusType intStatus;
#if (DBG_DEV_ERROR_DETECT == STD_ON)
        boolean triggerDet=FALSE; /* used to force DET call after critical section */
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
        P2VAR(Dbg_StateType,AUTOMATIC,AUTOMATIC) statePtr = &Dbg_State;
        index = DBG_DID_TO_INDEX(Did);

        /* critical enter */
        Dbg_NonTrustedLock(&intStatus);

        switch(activation)
        {
            case DBG_DID_ACTIVE:
                statePtr = &Dbg_Config.DIDstatePtr[index];
                if(status)
                {
                    *statePtr |=DBG_STATE_ACTIVE;
                }
                else
                {
                    *statePtr &= (Dbg_StateType)~DBG_STATE_ACTIVE;
                }
                break;
            case DBG_GLOBAL_ACTIVE:
                if(status)
                {
                    *statePtr |=DBG_STATE_ACTIVE;
                }
                else
                {
                    *statePtr &= (Dbg_StateType)~DBG_STATE_ACTIVE;
                }
                break;
            case DBG_DID_TIMESTAMP_ACTIVE:
                statePtr = &Dbg_Config.DIDstatePtr[index];
                if((Dbg_Config.cfgFlags&DBG_CFG_FLAG_NO_TIMESTAMP)==0U)
                {
                    if(status)
                    {
                        *statePtr |= DBG_STATE_TIMESTAMP;
                    }
                    else
                    {
                        *statePtr &=(Dbg_StateType)~DBG_STATE_TIMESTAMP;
                    }
                }
                else
                {
#if (DBG_DEV_ERROR_DETECT == STD_ON)
                    triggerDet = TRUE;
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
                }
                break;
            case DBG_GLOBAL_TIMESTAMP_ACTIVE:
                if((Dbg_Config.cfgFlags&DBG_CFG_FLAG_NO_TIMESTAMP)==0U)
                {
                    if(status)
                    {
                        *statePtr |= DBG_STATE_TIMESTAMP;
                    }
                    else
                    {
                        *statePtr &=(Dbg_StateType)~DBG_STATE_TIMESTAMP;
                    }
                }
                else
                {
#if (DBG_DEV_ERROR_DETECT == STD_ON)
                    triggerDet = TRUE;
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
                }
                break;
            case DBG_DID_BUFFERING_ACTIVE:
                if((Dbg_Config.cfgFlags&DBG_CFG_FLAG_NO_BUFFER)==0U)
                {
                    statePtr = &Dbg_Config.DIDstatePtr[index];
                    if(status)
                    {
                        *statePtr |= DBG_STATE_BUFFERED;
                    }
                    else
                    {
                        *statePtr &=(Dbg_StateType)~DBG_STATE_BUFFERED;
                    }
                }
                else
                {
#if (DBG_DEV_ERROR_DETECT == STD_ON)
                    triggerDet = TRUE;
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
                }
                break;
            default:
                break;
        }
        /* critical leave */
        Dbg_NonTrustedUnLock(intStatus);

#if (DBG_DEV_ERROR_DETECT == STD_ON)
        if( triggerDet )
        {
            /* use enum type value of activation for API Id */
            /* the return value can be ignored as the specification states
             * that the only possible return value is E_OK */
            (void)DBG_DET_REPORT_ERROR(activation,DBG_E_I_CFG);
        }
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
    }
}
/*--------------------------------------------------------------------------*/
#if (DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON)
FUNC(void, DBG_CODE) Dbg_Fdx_Init(void)
{
    T32_Fdx_InitChannel(FdxTestSendBuffer);
    T32_Fdx_EnableChannel(FdxTestSendBuffer);
}
#endif /* if (DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON) */

#if ((DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON) || (DBG_TCPIP_SUPPORT==1))
/*--------------------------------------------------------------------------*/
/*
 * !LINKSTO Dbg.Functional.Communication.Dbg085,1
 */
FUNC(Std_ReturnType,DBG_CODE) Dbg_Transmit(uint16 size,P2VAR(uint8,AUTOMATIC,DBG_APPL_DATA) buffer)
{
  Std_ReturnType result = E_NOT_OK;


#if (DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON)
  /*
   * !LINKSTO Dbg.Assumptions.Dbg005,1
   */
  if (T32_Fdx_Send(&FdxTestSendBuffer, buffer, size) != -1)
  {
    result = E_OK;
  }
#elif (DBG_TCPIP_SUPPORT==1)
  /* If Dbg_TcpClient==0, no client is currently connected. 
     If Dbg_TcpServer==0, starting the server has failed. */
  if((Dbg_TcpClientFlag != 0U) && (Dbg_TcpServerFlag != 0U))
  {
    /*
     * !LINKSTO Dbg.Assuptions.Dbg005,1
     */
    if(Dbg_TcpTransmit(&buffer[0], size) != -1)
    {
      result = E_OK;
    }
  }
#endif /* if (DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON) */

  if( result == E_OK )
  {
      if( Dbg_txCounter >= 1 )
      {
          Dbg_txCounter--;
      }
      /* Deviation MISRA-2 */
      Dbg_currentTxPtr = NULL_PTR;
      Dbg_lastFrameError = FALSE;
  }
  else
  {
     /* force re-transmission on next Com part activation */
     Dbg_lastFrameError = TRUE;
  }

  return result;
}
#endif /* if ((DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON) || (DBG_TCPIP_SUPPORT==1)) */

/*==================[internal function definitions]=========================*/
#if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF)
DBG_STATIC FUNC(void,DBG_CODE) Dbg_GetStatus(sint32 txCounter)
{
  uint8 i,j=0;
  uint8 EcuState;
  uint8 temp;
  uint8 DidStateId[DBG_PREDEF_DID_STATUS_SIZE]={0};

  /* Get global state */
  EcuState = DBG_MASK_FRAME_STATE((uint8)(Dbg_State>>1));

  /* Set continous sending enabled/disabled */
  if (txCounter==-1)
  {
    EcuState |= (uint8)DBG_CONTINOUS_SENDING;     /* Continous -> set bit 1 */
  }
  else
  {
    EcuState &= (uint8)(~DBG_CONTINOUS_SENDING);  /* Not continous -> clear bit 1 */
  }

  for(i=0; i<(DBG_NUM_DID);i++)
  {
    if ((Dbg_Config.DIDcfgPtr[i].stateFlags & (Dbg_StateType)DBG_S_STATE_CONFIGURED) != 0U)
    {
      /* Ignore bit 0,1 */
      temp = (uint8)((uint8)(Dbg_Config.DIDstatePtr[i]>>1U)&0xFU);
      /* shift to high or low nibble */
      temp = (uint8)(temp << (uint8)(((j&1U)!=0U)?0U:4U));
      DidStateId[j>>1] |= temp;
      j++;
    }
  }

  Dbg_Trace_Status(EcuState, DidStateId, DBG_PREDEF_DID_STATUS_SIZE);
}
#endif
/*--------------------------------------------------------------------------*/
#if (DBG_COM_VIA_TP_PDUR==STD_ON)
DBG_STATIC FUNC(Std_ReturnType,DBG_CODE) Dbg_ComTpSendNext(uint8 stateIndex)
{
    Dbg_DIDsizeType size;
    PduInfoType currentTxInfo;
    Std_ReturnType result = E_NOT_OK;
    if( Dbg_ComTpState[stateIndex].offset >= Dbg_ComTpState[stateIndex].chunkBufferSize )
    {
        /* possible check that currentSize == remainingSize */
        size = Dbg_ComTpState[stateIndex].currentSize;
    }
    else
    {
        /* copy data to chunk buffer according to current settings */
        Dbg_DIDsizeType i;
        size = Dbg_ComTpState[stateIndex].remainingSize + Dbg_ComTpState[stateIndex].offset;

        if( size > Dbg_ComTpState[stateIndex].chunkBufferSize )
        {
            size = Dbg_ComTpState[stateIndex].chunkBufferSize;
        }

        if( (Dbg_ComTpState[stateIndex].flags & (uint8)DBG_COM_TP_FLG_SET_TP1ST)!=0U )
        {
            Dbg_ComTpState[stateIndex].chunkBuffer[1] |= DBG_COM_TP_FIRST_FRAME;
            Dbg_ComTpState[stateIndex].flags &= (uint8)(~DBG_COM_TP_FLG_SET_TP1ST);
        } /* else needed for use case 4. -> FLG_SET_TP1ST prior to second flag FLG_DEL_TP1ST */
        else if( (Dbg_ComTpState[stateIndex].flags & (uint8)DBG_COM_TP_FLG_DEL_TP1ST)!=0U)
        {
           Dbg_ComTpState[stateIndex].chunkBuffer[1] &= (Dbg_BufferType)(~DBG_COM_TP_FIRST_FRAME);
           Dbg_ComTpState[stateIndex].flags &= (uint8)(~DBG_COM_TP_FLG_DEL_TP1ST);
        }
        else
        {

        }

        /* fill chunk buffer with data */
        for(i=Dbg_ComTpState[stateIndex].offset;i<size;i++)
        {
            Dbg_ComTpState[stateIndex].chunkBuffer[i]=Dbg_ComTpState[stateIndex].data[0];
            /* Deviation MISRA-1 */
            Dbg_ComTpState[stateIndex].data++;
        }
        /* real used new data size because the chunkBuffer[0 .. offset] was not
         * updated */
        Dbg_ComTpState[stateIndex].currentSize = size - Dbg_ComTpState[stateIndex].offset;
    }

    currentTxInfo.SduLength = size;
    currentTxInfo.SduDataPtr = &Dbg_ComTpState[stateIndex].chunkBuffer[0];
    /*
     * !LINKSTO Dbg.Assuptions.Dbg005,1
     */
    result = PduR_DbgTransmit(Dbg_ComTpPduRConfig.txId,&currentTxInfo);

    return result;
}
#endif

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-3 */
#include <MemMap.h>

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
