/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
*/

#include <Os.h>
#include <Dbg_Cbk.h>
#include <Dbg_TcpIp.h>
#include <Dbg_Cfg_Hw.h>

#if (DBG_TCPIP_SUPPORT == 1)

/*==================[inclusions]============================================*/

/*==================[version check]=========================================*/

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-1 */
#include <MemMap.h>

/*
 * This ISR runs in its own Windows thread. Thus we need a Windows
 * critical section shared with the Dbg_TcpReceiveThread() to
 * prevent concurrent accesses to Dbg_TcpBuffer.
 */  
ISR(Dbg_Rx_Interrupt)
{
  Dbg_EnterCriticalSection();
  Dbg_Indication((P2CONST(uint8,AUTOMATIC,AUTOMATIC))Dbg_TcpBuffer);
  Dbg_LeaveCriticalSection();
}

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-1 */
#include <MemMap.h>
/*==================[internal function definitions]=========================*/

#endif /* (DBG_TCPIP_SUPPORT == 1) */
/*==================[end of file]===========================================*/
