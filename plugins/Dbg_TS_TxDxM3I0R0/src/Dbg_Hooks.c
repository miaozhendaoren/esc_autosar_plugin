/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 11.4 (advisory)
 *    A cast should not be performed between a pointer to object
 *    type and a different pointer to object type
 *
 *    Reason:
 *    Target independent conversion of data received from an
 *    external source. The destination is uint8, thus no alignment
 *    problems will occur.
 *
 *  MISRA-2) Deviated Rule: 11.3 (advisory)
 *    A cast should not be performed between a pointer type and an integral type.
 *
 *    Reason:
 *    Pointers of different type need to be stored in the same structure.
 *    All pointers are converted into uint32, thus no problems are to be
 *    expected.
 *
 *  MISRA-3) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
*/


/*==================[inclusions]============================================*/
#include <TSMem.h>
#include <Dbg_Cfg.h>
#include <Dbg_Cfg_Internal.h>
#include <Dbg_Internal.h>

#if ((DBG_TRACE_OS==STD_ON) || (DBG_MEMORY_PROTECTION_SUPPORT==STD_ON))
#include <Os.h>
#endif

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

#define DBG_START_SEC_CODE
/* Deviation MISRA-3 */
#include <MemMap.h>

FUNC(void, DBG_CODE) Dbg_Trace_U8(uint8 aU8,uint8 DID)
{
  Dbg_BufferInfoWriteType BufferInfo;
  uint8 Buffer[1U];

#if (DBG_DEV_ERROR_DETECT == STD_ON)
  if( (!DBG_IS_PREDEF_DID(DID)) || (DBG_DID_TO_INDEX(DID) >= Dbg_Config.numDID) )
  {
    /* do not use DBG_DET_DID_TO_API_ID because DID is not valid */
    /* the return value can be ignored as the specification states
     * that the only possible return value is E_OK */
    (void)DBG_DET_REPORT_ERROR(DBG_API_I_TRACE_U8,DBG_E_INVALID_DID);
  }
  else
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
  {
    if (DBG_IS_DID_ACTIVE(DID)==TRUE)
    {
      Buffer[0U]=aU8;

      BufferInfo.DID = DID;
      BufferInfo.Src = Buffer;

      DBG_CORE_MEM_WRITE(&BufferInfo);
    }
  }
}
/*--------------------------------------------------------------------------*/
FUNC(void, DBG_CODE) Dbg_Trace_U16_U8_U8
(
  uint16 aU16,
  uint8  bU8,
  uint8  cU8,
  uint8  DID
)
{
  Dbg_BufferInfoWriteType BufferInfo;
  uint8 Buffer[4U];

#if (DBG_DEV_ERROR_DETECT == STD_ON)
  if( (!DBG_IS_PREDEF_DID(DID)) || (DBG_DID_TO_INDEX(DID) >= Dbg_Config.numDID) )
  {
    /* do not use DBG_DET_DID_TO_API_ID because DID is not valid */
    /* the return value can be ignored as the specification states
     * that the only possible return value is E_OK */
    (void)DBG_DET_REPORT_ERROR(DBG_API_I_TRACE_U16_U8_U8,DBG_E_INVALID_DID);
  }
  else
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
  {
    if (DBG_IS_DID_ACTIVE(DID)==TRUE)
    {
      /* Deviation MISRA-1 */
      DBG_CONVERT_SHORT(&Buffer[0U],aU16);
      Buffer[2U]=bU8;
      Buffer[3U]=cU8;

      BufferInfo.DID = DID;
      BufferInfo.Src = Buffer;

      DBG_CORE_MEM_WRITE(&BufferInfo);
    }
  }
}
/*--------------------------------------------------------------------------*/
FUNC(void, DBG_CODE) Dbg_Trace_U16_U8_U8_U8
(
  uint16 aU16,
  uint8  bU8,
  uint8  cU8,
  uint8  dU8,
  uint8  DID
)
{
  Dbg_BufferInfoWriteType BufferInfo;
  uint8 Buffer[5U];

#if (DBG_DEV_ERROR_DETECT == STD_ON)
  if( (!DBG_IS_PREDEF_DID(DID)) || (DBG_DID_TO_INDEX(DID) >= Dbg_Config.numDID) )
  {
    /* do not use DBG_DET_DID_TO_API_ID because DID is not valid */
    /* the return value can be ignored as the specification states
     * that the only possible return value is E_OK */
    (void)DBG_DET_REPORT_ERROR(DBG_API_I_TRACE_U16_U8_U8_U8,DBG_E_INVALID_DID);
  }
  else
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
  {
    if (DBG_IS_DID_ACTIVE(DID)==TRUE)
    {
      /* Deviation MISRA-1 */
      DBG_CONVERT_SHORT(&Buffer[0U],aU16);
      Buffer[2U]=bU8;
      Buffer[3U]=cU8;
      Buffer[4U]=dU8;

      BufferInfo.DID = DID;
      BufferInfo.Src = Buffer;

      DBG_CORE_MEM_WRITE(&BufferInfo);
    }
  }
}
/*--------------------------------------------------------------------------*/
FUNC(void, DBG_CODE) Dbg_Trace_U16_U8_U8_U8_U32
(
  uint16 aU16,
  uint8  bU8,
  uint8  cU8,
  uint8  dU8,
  uint32 eU32,
  uint8  DID
)
{
  Dbg_BufferInfoWriteType BufferInfo;
  uint8 Buffer[9U];

#if (DBG_DEV_ERROR_DETECT == STD_ON)
  if( (!DBG_IS_PREDEF_DID(DID)) || (DBG_DID_TO_INDEX(DID) >= Dbg_Config.numDID) )
  {
    /* do not use DBG_DET_DID_TO_API_ID because DID is not valid */
    /* the return value can be ignored as the specification states
     * that the only possible return value is E_OK */
    (void)DBG_DET_REPORT_ERROR(DBG_API_I_TRACE_U16_U8_U8_U8_U32,DBG_E_INVALID_DID);
  }
  else
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
  {
    if (DBG_IS_DID_ACTIVE(DID)==TRUE)
    {
      /* Deviation MISRA-1 */
      DBG_CONVERT_SHORT(&Buffer[0U],aU16);
      Buffer[2U]=bU8;
      Buffer[3U]=cU8;
      Buffer[4U]=dU8;
      /* Deviation MISRA-1 */
      DBG_CONVERT_LONG(&Buffer[5U],eU32);

      BufferInfo.DID = DID;
      BufferInfo.Src = Buffer;

      DBG_CORE_MEM_WRITE(&BufferInfo);
    }
  }
}
/*--------------------------------------------------------------------------*/
FUNC(void,DBG_CODE) Dbg_Trace_Status
(
  uint8 EcuState,
  P2VAR(uint8, AUTOMATIC, DBG_VAR) DidStateId,
  uint8 DidStateLen
)
{
  Dbg_BufferInfoWriteType BufferInfo;
  uint8 Buffer[DBG_PREDEF_DID_STATUS_SIZE+1U]; /* Did States + Ecu State */
  uint8 i;

  Buffer[0]=EcuState;
  for (i=0; i<(uint8)(DidStateLen); i++)
  {
    Buffer[1+i]=DidStateId[i];
  }

  BufferInfo.DID = DBG_DID_STATUS;
  BufferInfo.Src = Buffer;

  DBG_CORE_MEM_WRITE(&BufferInfo);
}
/*--------------------------------------------------------------------------*/
#if (DBG_TRACE_OS==STD_ON)
FUNC(void,DBG_CODE) PreTaskHook(void)
{
  TaskType currentTaskId;
  if( GetTaskID(&currentTaskId) == E_OK )
  {
    Dbg_PreTaskHook(currentTaskId);
  }
}
/*--------------------------------------------------------------------------*/
FUNC(void,DBG_CODE) PostTaskHook(void)
{
  TaskType currentTaskId;
  if( GetTaskID(&currentTaskId) == E_OK )
  {
    Dbg_PostTaskHook(currentTaskId);
  }
}
#endif

/*--------------------------------------------------------------------------*/
FUNC(void, DBG_CODE) Dbg_CollectDid(uint8 Did)
{
  Dbg_BufferInfoWriteType BufferInfo;
  uint8 Buffer[DBG_MAX_STD_DID_SIZE];

#if (DBG_DEV_ERROR_DETECT == STD_ON)
  if( (!DBG_IS_STD_DID(Did)) || (DBG_DID_TO_INDEX(Did) >= Dbg_Config.numDID) )
  {
    /* do not use DBG_DET_DID_TO_API_ID because DID is not valid */
    /* the return value can be ignored as the specification states
     * that the only possible return value is E_OK */
    (void)DBG_DET_REPORT_ERROR(DBG_API_COLLECT_DID,DBG_E_INVALID_DID);
  }
  else
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
  {
    if (DBG_IS_DID_ACTIVE(Did)==TRUE)
    {
      /* time to collect Did data */
      uint16 k=0;
      Dbg_DIDtype index;
      Dbg_NumType i;
      Dbg_DIDsizeType maxSize=0;
      P2CONST(Dbg_DIDConfigType,AUTOMATIC,DBG_APPL_CONST) currentDid;

      index = DBG_DID_TO_INDEX(Did);
      currentDid = &Dbg_Config.DIDcfgPtr[index];

      /* Calculate maximum size of DID */
      for(i=0;i<currentDid->numIndex;i++)
      {
        maxSize += currentDid->tablePtr[i].size;
      }

      if (((Dbg_DIDsizeType)DBG_MAX_STD_DID_SIZE) >= maxSize)
      {
        for(i=0;i<currentDid->numIndex;i++)
        {
          /* copy data */
          /* Deviation MISRA-2 */
          TS_MemCpy(&Buffer[k], ((Dbg_BufferPtrType)currentDid->tablePtr[i].addr), currentDid->tablePtr[i].size);
          k+=currentDid->tablePtr[i].size;
        }

        BufferInfo.DID = Did;
        BufferInfo.Src = Buffer;

        DBG_CORE_MEM_WRITE(&BufferInfo);
      }
#if (DBG_DEV_ERROR_DETECT == STD_ON)
      else
      {
        /* Configured DbgMaxSize is smaller than this DID */
        /* the return value can be ignored as the specification states
         * that the only possible return value is E_OK */
        (void)DBG_DET_REPORT_ERROR(DBG_API_COLLECT_DID,DBG_E_I_MEM_SIZE);
      }
#endif /* if (DBG_DEV_ERROR_DETECT == STD_ON) */
    }
  }
}

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-3 */
#include <MemMap.h>

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/

