# \file
#
# \brief AUTOSAR Rte
#
# This file contains the implementation of the AUTOSAR
# module Rte.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

Rte_src_FILES      := $(wildcard $(Rte_OUTPUT_PATH)/src/Rte*.c)
Rte_CC_FILES_TO_BUILD := $(Rte_src_FILES)
CC_FILES_TO_BUILD += $(Rte_src_FILES)
