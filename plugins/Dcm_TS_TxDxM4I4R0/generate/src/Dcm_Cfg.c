/**
 * \file
 *
 * \brief AUTOSAR Dcm
 *
 * This file contains the implementation of the AUTOSAR
 * module Dcm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

 /* MISRA-C:2004 Deviation List
  *
  * MISRA-1) Deviated Rule: 12.4 (required)
  *     The right-hand operand of a logical '&&' or '||' operator
  *     shall not contain side effects.
  *
  *     Reason:
  *     Logical operators '&&' and '||' are used here purposefully. The sub-expression shall not be
  *      evaluated if the first expression of a '&&' operation is FALSE. Similarly the
  *      sub-expression shall not be evaluated if the first expression of a '||' operation is TRUE.
  *      There is no side effects.
  *
  * MISRA-2) Deviated Rule: 11.4 (advisory)
  *     A cst shouldnot be performed between a pointer to object type and a
  *     different pointer to object type.
  *
  *     Reason:
  *     A cast to unsigned is necessary here to copy data bit by bit and
  *     there is no alignment issues.
  */

/*==================[inclusions]=================================================================*/
[!//
[!AUTOSPACING!]
[!INDENT "0"!]
#include <ComStack_Types.h>                                            /* AUTOSAR standard types */
#include <Dcm_Cfg.h>                                           /* Generated configuration header */
#include <Dcm_API_Cfg.h>                                  /*  Configurable function declarations */
#include <Dcm_Hsm.h>                                                        /* Dcm State Machine */
#include <Dcm_Int.h>                                       /* Module internal header static part */
#include <Dcm_DspExternal.h>                          /* Callbacks provided by Central Diag SwC  */
[!IF "((count(DcmConfigSet/*/DcmProcessingConditions/DcmModeCondition/*[node:exists(DcmBswModeRef)])) > 0) or (as:modconf('Dcm')[1]/DcmConfigSet/*[1]/DcmGeneral/DcmModeDeclarationSupport = 'true')"!]
#include <SchM_Dcm.h>
[!ENDIF!]
[!INCLUDE "Dcm.m"!][!//
/*==================[macros]=====================================================================*/
[!/*

EcuC References of Dcm Rx PDUs
*/!][!VAR "DcmRxPduCount" = "num:integer( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolRx/*/DcmDslProtocolRxPduRef))"!][!/*

EcuC References of Dcm Tx PDUs
*/!][!VAR "DcmTxPduCount" = "num:integer( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*))"!]  [!/* Default Tx connection */!][!/*

Activation period of main function
*/!][!VAR "DcmTaskTime" = "DcmConfigSet/*[1]/DcmGeneral/DcmTaskTime"!][!/*

Indicates whether Routine Control VARIABLE_LENGTH signal is expressed in bytes or bits
*/!][!VAR "DcmRoutineVarLenInBytes" = "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutineVariableLengthInBytes"!][!//
/*==================[type definitions]===========================================================*/

/*==================[external function declarations]=============================================*/

/*==================[internal function declarations]=============================================*/
#define DCM_START_SEC_CODE
#include <MemMap.h>

[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!]
  [!IF "DcmDspRoutineUsed = 'true'"!]
  [!/*------------Internal declarations of user functions for the servic0x31 ----------------*/!][!//
    STATIC FUNC(Std_ReturnType, DCM_CODE)Dcm_[!"node:name(.)"!]_Start(
      Dcm_OpStatusType OpStatus,
      P2VAR(Dcm_MsgContextType, AUTOMATIC, DCM_APPL_DATA)pMsgContext,
      P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);

    [!IF "DcmDspStopRoutineSupported = 'true'"!]
      STATIC FUNC(Std_ReturnType, DCM_CODE)Dcm_[!"node:name(.)"!]_Stop(
        Dcm_OpStatusType OpStatus,
        P2VAR(Dcm_MsgContextType, AUTOMATIC, DCM_APPL_DATA)pMsgContext,
        P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
    [!ENDIF!]

    [!IF "DcmDspRequestResultsRoutineSupported = 'true'"!]
      STATIC FUNC(Std_ReturnType, DCM_CODE)Dcm_[!"node:name(.)"!]_RequestResults(
        Dcm_OpStatusType OpStatus,
        P2VAR(Dcm_MsgContextType, AUTOMATIC, DCM_APPL_DATA)pMsgContext,
        P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
    [!ENDIF!]
  [!ENDIF!]
[!ENDLOOP!]

[!LOOP "DcmConfigSet/*/DcmProcessingConditions/DcmModeCondition/*"!]
/** \brief Mode condition function declaration for [!"name(.)"!] */
STATIC FUNC(boolean, DCM_CODE) ModeCondition_[!"name(.)"!]_Result(void);
[!ENDLOOP!]

#define DCM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal constants]=========================================================*/

#define DCM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSubService/*/DcmDsdSubServiceSessionLevelRef/*) > 0"!]
  /* Array of session types for all Subservices */
  STATIC CONST(Dcm_SesCtrlType,DCM_CONST) Dcm_SubSidTabSesLevels[[!"num:i(count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSubService/*/DcmDsdSubServiceSessionLevelRef/*))"!]] =
  {
    [!LOOP "node:order(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*, 'DcmDsdSidTabId')"!][!//
      [!LOOP "node:order(DcmDsdService/*, 'DcmDsdSidTabServiceId')"!][!//
        [!LOOP "node:order(DcmDsdSubService/*, 'DcmDsdSubServiceId')"!][!//
          [!LOOP "DcmDsdSubServiceSessionLevelRef/*"!][!//
            [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSessionLevel, 2)"!]U, /* [!"name(node:ref(.))"!] of [!"name(../../../..)"!] referred by its subservice [!"name(../..)"!] */
          [!ENDLOOP!][!//
        [!ENDLOOP!][!//
      [!ENDLOOP!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]

[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSubService/*/DcmDsdSubServiceSecurityLevelRef/*) > 0"!]
  /* Array of security types for all Subservices */
  STATIC CONST(Dcm_SecLevelType,DCM_CONST) Dcm_SubSidTabSecLevels[[!"num:i(count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSubService/*/DcmDsdSubServiceSecurityLevelRef/*))"!]] =
  {
    [!LOOP "node:order(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*, 'DcmDsdSidTabId')"!][!//
      [!LOOP "node:order(DcmDsdService/*, 'DcmDsdSidTabServiceId')"!][!//
        [!LOOP "node:order(DcmDsdSubService/*, 'DcmDsdSubServiceId')"!][!//
          [!LOOP "DcmDsdSubServiceSecurityLevelRef/*"!][!//
            [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSecurityLevel, 2)"!]U, /* [!"name(node:ref(.))"!] of [!"name(../../../..)"!] referred by its subservice [!"name(../..)"!] */
          [!ENDLOOP!][!//
        [!ENDLOOP!][!//
      [!ENDLOOP!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]

[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSidTabSessionLevelRef/*) > 0"!]
  /* Array of session types for all services */
  STATIC CONST(Dcm_SesCtrlType,DCM_CONST) Dcm_SidTabSesLevels[[!"num:i(count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSidTabSessionLevelRef/*))"!]] =
  {
    [!LOOP "node:order(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*, 'DcmDsdSidTabId')"!][!//
      [!LOOP "node:order(DcmDsdService/*, 'DcmDsdSidTabServiceId')"!][!//
        [!LOOP "DcmDsdSidTabSessionLevelRef/*"!][!//
          [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSessionLevel, 2)"!]U, /* [!"name(node:ref(.))"!] referred to by [!"name(../..)"!] */
        [!ENDLOOP!][!//
      [!ENDLOOP!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSidTabSecurityLevelRef/*) > 0"!]
  /* Array of security types for all services */
  STATIC CONST(Dcm_SecLevelType, DCM_CONST) Dcm_SidTabSecLevels[[!"num:i(count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSidTabSecurityLevelRef/*))"!]] =
  {
    [!LOOP "node:order(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*, 'DcmDsdSidTabId')"!][!//
      [!LOOP "node:order(DcmDsdService/*, 'DcmDsdSidTabServiceId')"!][!//
        [!LOOP "DcmDsdSidTabSecurityLevelRef/*"!][!//
          [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSecurityLevel, 2)"!]U, /* [!"name(node:ref(.))"!] referred to by [!"name(../..)"!] */
        [!ENDLOOP!][!//
      [!ENDLOOP!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
[!VAR "SubSesLevelPos" = "0"!][!//
[!VAR "SubSecLevelPos" = "0"!][!//

[!LOOP "node:order(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*, 'DcmDsdSidTabId')"!][!//
  [!LOOP "node:order(DcmDsdService/*, 'DcmDsdSidTabServiceId')"!][!//
  [!//
    [!IF "count(./DcmDsdSubService/*) > 0 and ./DcmDsdSidTabSubfuncAvail = 'true'"!]
      /* Array of Subservices for Service '[!"./@name"!]' */
      STATIC CONST(Dcm_SubSidTabEntryConfigType, DCM_CONST) Dcm_SidTabEntryConfig[!"../../DcmDsdSidTabId"!]_SubSidTabEntryConfig[!"DcmDsdSidTabServiceId"!][[!"num:integer( count(./DcmDsdSubService/*))"!]] =
      {
         [!LOOP "node:order(DcmDsdSubService/*, 'DcmDsdSubServiceId')"!][!//

          [!VAR "NumSubSesLevels" = "num:integer( count( DcmDsdSubServiceSessionLevelRef/*))"!][!//
          [!VAR "NumSubSecLevels" = "num:integer( count( DcmDsdSubServiceSecurityLevelRef/*))"!][!//
          [!WS "2"!]{
          [!INDENT "4"!]
            [!IF "$NumSubSesLevels > 0"!]
              &( Dcm_SubSidTabSesLevels[[!"num:integer( $SubSesLevelPos)"!]]), /* Pointer to subservice sessions */
            [!ELSE!]
              NULL_PTR,  /* Pointer to subservice sessions */
            [!ENDIF!]
            [!IF "$NumSubSecLevels > 0"!]
              &( Dcm_SubSidTabSecLevels[[!"num:integer( $SubSecLevelPos)"!]]), /* Pointer to subservice security levels */
            [!ELSE!]
              NULL_PTR,/* Pointer to subservice security levels */
            [!ENDIF!]
            [!IF "node:exists(DcmDsdSubServiceFnc)"!]
              &[!"DcmDsdSubServiceFnc"!],  /* Pointer to Subservice function */
              NULL_PTR,
            [!ELSE!][!//
              NULL_PTR,
              &[!CALL "DCM_SUB_SERVICE_HANDLER_NAME"!],  /* Pointer to Subservice function */
            [!ENDIF!]
            [!IF "node:exists(DcmDsdSubServiceModeRuleRef)"!]
              &ModeRule_[!"name(as:ref(DcmDsdSubServiceModeRuleRef))"!]_Result, /* Sub-service Mode rule */
            [!ELSE!]
              NULL_PTR,
            [!ENDIF!]
            [!"num:inttohex( DcmDsdSubServiceId, 2)"!]U, /* SubServiceId */
            [!"$NumSubSesLevels"!]U, /* Number of sub service session levels*/
            [!"$NumSubSecLevels"!]U, /* Number of sub service security levels */
          [!ENDINDENT!]
          [!WS "2"!]},
          [!VAR "SubSesLevelPos" = "$SubSesLevelPos + $NumSubSesLevels"!][!//
          [!VAR "SubSecLevelPos" = "$SubSecLevelPos + $NumSubSecLevels"!][!//
        [!ENDLOOP!]
      };
    [!ENDIF!]
  [!ENDLOOP!]
[!ENDLOOP!]
[!//
[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
[!VAR "SesLevelPos" = "0"!][!//
[!VAR "SecLevelPos" = "0"!][!//
[!LOOP "node:order(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*, 'DcmDsdSidTabId')"!][!//
[!//
  /* Array of services for Service table '[!"name( .)"!]' (id [!"DcmDsdSidTabId"!]) */
  STATIC CONST(Dcm_SidTabEntryConfigType, DCM_CONST) Dcm_SidTabEntryConfig[!"DcmDsdSidTabId"!][[!"num:integer( count(DcmDsdService/*))"!]] =
    {
      [!LOOP "node:order(DcmDsdService/*, 'DcmDsdSidTabServiceId')"!][!//
        [!VAR "NumSesLevels" = "num:integer( count( DcmDsdSidTabSessionLevelRef/*))"!][!//
        [!VAR "NumSecLevels" = "num:integer( count( DcmDsdSidTabSecurityLevelRef/*))"!][!//
        [!WS "2"!]{
        [!INDENT "4"!]
          [!IF "$NumSesLevels > 0"!]
            &( Dcm_SidTabSesLevels[[!"num:integer( $SesLevelPos)"!]]), /* Pointer to sessions */
          [!ELSE!]
            NULL_PTR,
          [!ENDIF!]
          [!IF "$NumSecLevels > 0"!]
            &( Dcm_SidTabSecLevels[[!"num:integer( $SecLevelPos)"!]]),/* Pointer to security levels */
          [!ELSE!]
            NULL_PTR,
          [!ENDIF!]
          [!IF "count(./DcmDsdSubService/*) > 0 and ./DcmDsdSidTabSubfuncAvail = 'true'"!]
            (Dcm_SidTabEntryConfig[!"../../DcmDsdSidTabId"!]_SubSidTabEntryConfig[!"DcmDsdSidTabServiceId"!]), /*Pointer to subservices array. */
          [!ELSE!]
            NULL_PTR, /*Pointer to subservices array. */
          [!ENDIF!]
          [!IF "not(node:exists(DcmDsdSidTabFnc))"!]
            [!IF "(DcmDsdSidTabServiceId != 16) and (DcmDsdSidTabServiceId != 20)
              and (DcmDsdSidTabServiceId != 25) and (DcmDsdSidTabServiceId != 35) and (DcmDsdSidTabServiceId != 39) and (DcmDsdSidTabServiceId != 47) and (DcmDsdSidTabServiceId != 61) and
              (DcmDsdSidTabServiceId != 49) and (DcmDsdSidTabServiceId != 62) and (DcmDsdSidTabServiceId != 34) and (DcmDsdSidTabServiceId != 46) and (DcmDsdSidTabServiceId != 17) and (DcmDsdSidTabServiceId != 52) and (DcmDsdSidTabServiceId != 53) and (DcmDsdSidTabServiceId != 54) and (DcmDsdSidTabServiceId != 40) and (DcmDsdSidTabServiceId != 55) and (DcmDsdSidTabServiceId != 133) and (DcmDsdSidTabServiceId != 135) and (DcmDsdSidTabServiceId != 134)"!][!//
            [!ERROR "This service is not implementd in Dsp."!][!//
            [!ELSE!][!//
              NULL_PTR,
              &[!CALL "DCM_SERVICE_HANDLER_NAME"!],  /* Pointer to service function */
            [!ENDIF!]
          [!ELSE!][!//
            &[!"DcmDsdSidTabFnc"!], /* Pointer to service function */
            NULL_PTR,
          [!ENDIF!][!//
          [!IF "node:exists(DcmDsdSidTabModeRuleRef)"!]
            &ModeRule_[!"name(as:ref(DcmDsdSidTabModeRuleRef))"!]_Result, /* Service Mode rule */
          [!ELSE!]
            NULL_PTR,
          [!ENDIF!][!//
          [!"num:inttohex( DcmDsdSidTabServiceId, 2)"!]U, /* ServiceId */
          [!"$NumSesLevels"!]U, /* Number of sessions */
          [!"$NumSecLevels"!]U, /* Number of security levels */
          [!"num:integer( count(./DcmDsdSubService/*))"!]U, /*Number of subservices */
          [!"text:toupper( DcmDsdSidTabSubfuncAvail)"!], /* Subfunction available */
          [!"text:toupper(DcmAsyncServiceExecution)"!] /* AsyncServiceExecution available */
        [!ENDINDENT!]
        [!WS "2"!]},
        [!VAR "SesLevelPos" = "$SesLevelPos + $NumSesLevels"!][!//
        [!VAR "SecLevelPos" = "$SecLevelPos + $NumSecLevels"!][!//
      [!ENDLOOP!][!//
    };
[!ENDLOOP!][!//

[!/*---------------To get number of read session levels configured for all Dids-------------*/!][!//
[!VAR "sessioncount" = "0"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
  [!VAR "sessioncount" = "$sessioncount + count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidRead/DcmDspDidReadSessionRef/*)"!][!//
[!ENDLOOP!][!//
[!//
[!IF "$sessioncount > 0"!]
  /* Array of session types for Read Did services */
  STATIC CONST(Dcm_SesCtrlType,DCM_CONST) Dcm_ReadDidSesTypes[[!"num:i($sessioncount)"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
      [!SELECT "node:ref(DcmDspDidInfoRef)"!][!//
        [!LOOP "DcmDspDidAccess/DcmDspDidRead/DcmDspDidReadSessionRef/*"!][!//
          [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSessionLevel, 2)"!]U,
        [!ENDLOOP!][!//
      [!ENDSELECT!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]

[!/*---------------To get number of read security levels configured for all Dids------------*/!][!//
[!VAR "securitycount" = "0"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
  [!VAR "securitycount" = "$securitycount + count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidRead/DcmDspDidReadSecurityLevelRef/*)"!][!//
[!ENDLOOP!][!//

[!IF "$securitycount > 0"!]
  /* Array of session types for Read Did services */
  STATIC CONST(Dcm_SesCtrlType,DCM_CONST) Dcm_ReadDidSecTypes[[!"num:i($securitycount)"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
      [!SELECT "node:ref(DcmDspDidInfoRef)"!][!//
        [!LOOP "DcmDspDidAccess/DcmDspDidRead/DcmDspDidReadSecurityLevelRef/*"!][!//
          [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSecurityLevel, 2)"!]U,
        [!ENDLOOP!][!//
      [!ENDSELECT!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]
[!/*---------------To get number of write session levels configured for all Dids------------*/!][!//
[!VAR "sessioncount" = "0"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
  [!VAR "sessioncount" = "$sessioncount + count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidWrite/DcmDspDidWriteSessionRef/*)"!][!//
[!ENDLOOP!][!//
[!//
[!IF "$sessioncount > 0"!]
  /* Array of session types for Read Did services */
  STATIC CONST(Dcm_SesCtrlType,DCM_CONST) Dcm_WriteDidSesTypes[[!"num:i($sessioncount)"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
      [!SELECT "node:ref(DcmDspDidInfoRef)"!][!//
        [!LOOP "DcmDspDidAccess/DcmDspDidWrite/DcmDspDidWriteSessionRef/*"!][!//
          [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSessionLevel, 2)"!]U,
        [!ENDLOOP!][!//
      [!ENDSELECT!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]
[!/*---------------To get number of write security levels configured for all Dids------------*/!][!//
[!VAR "securitycount" = "0"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
  [!VAR "securitycount" = "$securitycount + count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidWrite/DcmDspDidWriteSecurityLevelRef/*)"!][!//
[!ENDLOOP!][!//
[!//
[!IF "$securitycount > 0"!]
  /* Array of session types for Read Did services */
  STATIC CONST(Dcm_SesCtrlType,DCM_CONST) Dcm_WriteDidSecTypes[[!"num:i($securitycount)"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
      [!SELECT "node:ref(DcmDspDidInfoRef)"!][!//
        [!LOOP "DcmDspDidAccess/DcmDspDidWrite/DcmDspDidWriteSecurityLevelRef/*"!][!//
          [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSecurityLevel, 2)"!]U,
        [!ENDLOOP!][!//
      [!ENDSELECT!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]

[!/*-------------To get number of Control session levels configured for all Dids------------*/!][!//
[!VAR "sessioncount" = "0"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
  [!VAR "sessioncount" = "$sessioncount + count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl/DcmDspDidControlSessionRef/*)"!][!//
[!ENDLOOP!][!//

[!IF "$sessioncount > 0"!]
/* Array of session types for IOControl Did services */
STATIC CONST(Dcm_SesCtrlType,DCM_CONST) Dcm_ControlDidSesLevels[[!"num:i($sessioncount)"!]] =
{
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
    [!SELECT "node:ref(DcmDspDidInfoRef)"!][!//
      [!LOOP "DcmDspDidAccess/DcmDspDidControl/DcmDspDidControlSessionRef/*"!][!//
        [!"num:inttohex(node:ref(.)/DcmDspSessionLevel, 2)"!]U,
      [!ENDLOOP!][!//
    [!ENDSELECT!][!//
  [!ENDLOOP!][!//
};
[!ENDIF!]

[!/*------------To get number of control security levels configured for all Dids------------*/!][!//
[!VAR "securitycount" = "0"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
  [!VAR "securitycount" = "$securitycount + count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl/DcmDspDidControlSecurityLevelRef/*)"!][!//
[!ENDLOOP!][!//

[!IF "$securitycount > 0"!]
/* Array of session types for IOControl Did services */
STATIC CONST(Dcm_SesCtrlType,DCM_CONST) Dcm_ControlDidSecLevels[[!"num:i($securitycount)"!]] =
{
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
    [!SELECT "node:ref(DcmDspDidInfoRef)"!][!//
      [!LOOP "DcmDspDidAccess/DcmDspDidControl/DcmDspDidControlSecurityLevelRef/*"!][!//
        [!"num:inttohex(node:ref(.)/DcmDspSecurityLevel, 2)"!]U,
      [!ENDLOOP!][!//
    [!ENDSELECT!][!//
  [!ENDLOOP!][!//
};
[!ENDIF!]

[!/*---------------Security Access Levels configured for read access to memory locations------------*/!][!//
[!VAR "readMemSeccount" = "count(DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*/DcmDspReadMemoryRangeInfo/*/DcmDspReadMemoryRangeSecurityLevelRef/*)"!][!//
[!//
[!IF "$readMemSeccount > 0"!]
  STATIC CONST(Dcm_SecLevelType,DCM_CONST) Dcm_ReadMemSecLevels[[!"num:i($readMemSeccount)"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*/DcmDspReadMemoryRangeInfo/*"!][!//
        [!LOOP "./DcmDspReadMemoryRangeSecurityLevelRef/*"!][!//
          [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSecurityLevel, 2)"!]U,
        [!ENDLOOP!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]

[!/*-------------Security Access Levels configured for Write access to memory locations--*/!][!//
[!VAR "writeMemSeccount" = "count(DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*/DcmDspWriteMemoryRangeInfo/*/DcmDspWriteMemoryRangeSecurityLevelRef/*)"!][!//
[!//
[!IF "$writeMemSeccount > 0"!]
  STATIC CONST(Dcm_SecLevelType,DCM_CONST) Dcm_WriteMemSecLevels[[!"num:i($writeMemSeccount)"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*/DcmDspWriteMemoryRangeInfo/*"!][!//
        [!LOOP "./DcmDspWriteMemoryRangeSecurityLevelRef/*"!][!//
          [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSecurityLevel, 2)"!]U,
        [!ENDLOOP!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!]


[!IF "count(DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']/DcmDspDidSignal/*) > 0"!][!//
  /* Array of signals configured Did services */
  STATIC CONST(Dcm_DidSignalType,DCM_CONST) Dcm_DidSignals[[!"num:i(count(DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']/DcmDspDidSignal/*))"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!][!//
      [!VAR "signaloverlap" = "0"!][!//
      [!VAR "did" = "node:name(.)"!]
      [!LOOP "node:order(DcmDspDidSignal/* , 'DcmDspDidDataPos')"!][!//
        [!IF "$signaloverlap > num:i(DcmDspDidDataPos)"!][!//
          [!ERROR!]
            Signals configured for the Did [!"$did"!] are overlapped.
          [!ENDERROR!]
        [!ENDIF!]
        [!WS "2"!]{ [!"num:i(DcmDspDidDataPos)"!]U, [!"as:ref(DcmDspDidDataRef)/@index"!]U }, /* signal [!"./@index"!] of Did [!"$did"!] */
        [!VAR "signaloverlap" = "num:i(DcmDspDidDataPos) + node:ref(DcmDspDidDataRef)/DcmDspDataSize"!][!//
      [!ENDLOOP!][!//
    [!ENDLOOP!][!//
  };
[!ENDIF!][!//
[!VAR "sessioncount" = "0"!][!//
[!VAR "RoutineInfoList"="'#'"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
  [!IF "not(contains($RoutineInfoList, concat('#', name(node:ref(DcmDspRoutineInfoRef)), '#')))"!]
    [!VAR "sessioncount" = "$sessioncount + count(node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineAuthorization/DcmDspRoutineSessionRef/*)"!][!//
    [!VAR "RoutineInfoList" = "concat($RoutineInfoList, name(node:ref(DcmDspRoutineInfoRef)), '#')"!]
  [!ENDIF!]
[!ENDLOOP!][!//
[!//
[!VAR "RoutineCount" = "0"!]
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
  [!IF "DcmDspRoutineUsed = 'true'"!]
    [!VAR "RoutineCount" = "$RoutineCount + 1"!]
  [!ENDIF!]
[!ENDLOOP!]

[!IF "$RoutineCount > 0"!]
[!IF "count(node:refs(DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*/DcmDspRoutineInfoRef)/DcmDspRoutineAuthorization/DcmDspRoutineSessionRef/*) > 0"!]
[!VAR "RoutineInfoList"="'#'"!][!//
  /* Array of session types for all routines */
  STATIC CONST(Dcm_SesCtrlType,DCM_CONST) Dcm_RoutineSesTypes[[!"num:i($sessioncount)"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
      [!IF "not(contains($RoutineInfoList, concat('#', name(node:ref(DcmDspRoutineInfoRef)), '#')))"!]
        [!SELECT "node:ref(DcmDspRoutineInfoRef)"!][!//
          [!LOOP "DcmDspRoutineAuthorization/DcmDspRoutineSessionRef/*"!][!//
            [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSessionLevel, 2)"!]U,
          [!ENDLOOP!][!//
        [!ENDSELECT!][!//
        [!VAR "RoutineInfoList" = "concat($RoutineInfoList, name(node:ref(DcmDspRoutineInfoRef)), '#')"!]
      [!ENDIF!]
    [!ENDLOOP!][!//
  };
[!ENDIF!]
[!//
[!VAR "seclevelList"="'#'"!][!//
[!VAR "seclevelcount" = "0"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
  [!IF "not(contains($seclevelList, concat('#', name(node:ref(DcmDspRoutineInfoRef)), '#')))"!]
    [!VAR "seclevelcount" = "$seclevelcount + count(node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineAuthorization/DcmDspRoutineSecurityLevelRef/*)"!][!//
    [!VAR "seclevelList" = "concat($seclevelList, name(node:ref(DcmDspRoutineInfoRef)), '#')"!]
  [!ENDIF!]
[!ENDLOOP!][!//

[!IF "count(node:refs(DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*/DcmDspRoutineInfoRef)/DcmDspRoutineAuthorization/DcmDspRoutineSecurityLevelRef/*) > 0"!]
[!VAR "seclevelList"="'#'"!][!//
  /* Array of security types for all routines */
  STATIC CONST(Dcm_SecLevelType,DCM_CONST) Dcm_RoutineSecTypes[[!"num:i($seclevelcount)"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
      [!IF "not(contains($seclevelList, concat('#', name(node:ref(DcmDspRoutineInfoRef)), '#')))"!]
        [!VAR "seclevelcount" = "$seclevelcount + 1"!][!//
        [!SELECT "node:ref(DcmDspRoutineInfoRef)"!][!//
          [!LOOP "DcmDspRoutineAuthorization/DcmDspRoutineSecurityLevelRef/*"!][!//
            [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspSecurityLevel, 2)"!]U,
          [!ENDLOOP!][!//
        [!ENDSELECT!][!//
        [!VAR "seclevelList" = "concat($seclevelList, name(node:ref(DcmDspRoutineInfoRef)), '#')"!]
      [!ENDIF!]
    [!ENDLOOP!][!//
  };
[!ENDIF!]

  /* Configuration parameters for 'Routine Control' service */
  CONST(Dcm_DspRoutineServicesType, DCM_CONST) Dcm_DspRoutineConfig[DCM_NUM_ROUTINES] =
  {
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
    [!IF "DcmDspRoutineUsed = 'true'"!]
    [!WS "2"!]{
      [!INDENT "4"!]
        [!IF "DcmDspRequestResultsRoutineSupported = 'true'"!][!//
          &Dcm_[!"node:name(.)"!]_RequestResults, /* Pointer to request service handler function */
        [!ELSE!][!//
          NULL_PTR,
        [!ENDIF!][!//
        &Dcm_[!"node:name(.)"!]_Start, /* Pointer to start service handler function */
        [!IF "DcmDspStopRoutineSupported = 'true'"!]
          &Dcm_[!"node:name(.)"!]_Stop, /* Pointer to stop service handler function */
        [!ELSE!][!//
          NULL_PTR,
        [!ENDIF!][!//
        [!VAR "currentconfig" = "name(as:ref(DcmDspRoutineInfoRef))"!][!//
        [!VAR "routineinfoindex" = "0"!][!//
        [!VAR "routineInfoRefList"="'#'"!][!//
        [!LOOP "../*"!][!//
          [!IF "name(as:ref(DcmDspRoutineInfoRef)) = $currentconfig"!]
            [!BREAK!]
          [!ENDIF!]
          [!IF "not(contains($routineInfoRefList, concat('#', name(node:ref(DcmDspRoutineInfoRef)), '#')))"!]
            [!VAR "routineinfoindex" = "$routineinfoindex + 1"!][!//
            [!VAR "routineInfoRefList" = "concat($routineInfoRefList, name(node:ref(DcmDspRoutineInfoRef)), '#')"!]
          [!ENDIF!]
        [!ENDLOOP!][!//
        &Dcm_DspRoutineInfoConfig[[!"num:integer($routineinfoindex)"!]],
        [!"DcmDspRoutineIdentifier"!]U, /* RID Identifier */
        [!"text:toupper(DcmDspRoutineFixedLength)"!],
        [!"text:toupper(DcmDspRoutineUsePort)"!]
      [!ENDINDENT!]
    [!WS "2"!]},
    [!ENDIF!][!//
  [!ENDLOOP!][!//
  };
[!ENDIF!]

[!IF "$RoutineCount > 0"!]
  /* Contains the configuration (parameters) for Routine's Info */
[!VAR "routineinfocount" = "0"!][!//
[!VAR "routineInfoRefList"="'#'"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
  [!IF "not(contains($routineInfoRefList, concat('#', name(node:ref(DcmDspRoutineInfoRef)), '#')))"!]
    [!VAR "routineinfocount" = "$routineinfocount + 1"!][!//
    [!VAR "routineInfoRefList" = "concat($routineInfoRefList, name(node:ref(DcmDspRoutineInfoRef)), '#')"!]
  [!ENDIF!]
[!ENDLOOP!][!//
[!//

[!VAR "routineInfoRefList"="'#'"!][!//
/* Contains the configuration (parameters) for Routine's Info */
[!VAR "SesLevelPos" = "0"!][!//
[!VAR "SecLevelPos" = "0"!][!//
CONST(Dcm_DspRoutineServicesInfoType, DCM_CONST) Dcm_DspRoutineInfoConfig[[!"num:integer( $routineinfocount)"!]] =
{
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!][!//
    [!IF "not(contains($routineInfoRefList, concat('#', name(node:ref(DcmDspRoutineInfoRef)), '#')))"!]
      [!SELECT "node:ref(DcmDspRoutineInfoRef)"!][!//
      [!WS "2"!]{
        [!INDENT "4"!]
        [!VAR "NumSecLevels" = "num:integer(count(DcmDspRoutineAuthorization/DcmDspRoutineSecurityLevelRef/*))"!][!//
        [!VAR "NumSesLevels" = "num:integer(count(DcmDspRoutineAuthorization/DcmDspRoutineSessionRef/*))"!][!//
         [!IF "$NumSecLevels > 0"!][!//
           &(Dcm_RoutineSecTypes[[!"num:integer( $SecLevelPos)"!]]), /* Pointer to security levels */
         [!ELSE!][!//
           NULL_PTR,
         [!ENDIF!]
         [!IF "$NumSesLevels > 0"!][!//
           &(Dcm_RoutineSesTypes[[!"num:integer( $SesLevelPos)"!]]), /* Pointer to sessions */
         [!ELSE!][!//
           NULL_PTR,
         [!ENDIF!]
         [!"$NumSesLevels"!]U, /* Number of sessions */
         [!"$NumSecLevels"!]U /* Number of security levels */
        [!VAR "SesLevelPos" = "$SesLevelPos + $NumSesLevels"!][!//
        [!VAR "SecLevelPos" = "$SecLevelPos + $NumSecLevels"!][!//
        [!ENDINDENT!]
      [!WS "2"!]},
      [!ENDSELECT!][!//
      [!VAR "routineInfoRefList" = "concat($routineInfoRefList, name(node:ref(DcmDspRoutineInfoRef)), '#')"!]
    [!ENDIF!]
  [!ENDLOOP!][!//
};
[!ENDIF!]

[!IF "count( DcmConfigSet/*[1]/DcmDsp/DcmDspSecurity/DcmDspSecurityRow/*) > 0"!]
  /* Array of configuration information for diagnostic security levels */
  STATIC CONST(Dcm_SecTabEntryConfigType, DCM_CONST) Dcm_SecTabEntryConfig[[!"num:integer( count( DcmConfigSet/*[1]/DcmDsp/DcmDspSecurity/DcmDspSecurityRow/*))"!]] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspSecurity/DcmDspSecurityRow/*"!][!//
      [!WS "2"!]{
      [!INDENT "4"!]
        [!"num:integer(( DcmDspSecurityDelayTime) div $DcmTaskTime)"!]U, /* Delay time */
        [!/*---GetSeedFnc------*/!][!//
        [!IF "DcmDspSecurityADRSize > 0"!][!//
          [!IF "DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_FUNC' or DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_FNC'"!][!//
            [!IF "node:exists(DcmDspSecurityGetSeedFnc)"!][!//
              &[!"DcmDspSecurityGetSeedFnc"!],
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
            [!ELSE!][!//
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
            [!ENDIF!][!//
          [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FUNC' or DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FNC'"!][!//
            [!IF "node:exists(DcmDspSecurityGetSeedFnc)"!][!//
              NULL_PTR,
              &[!"DcmDspSecurityGetSeedFnc"!],
              NULL_PTR,
              NULL_PTR,
            [!ELSE!][!//
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
            [!ENDIF!][!//
          [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_CLIENT_SERVER'"!]
            &Rte_Call_SecurityAccess_[!"node:name(.)"!]_GetSeed,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
          [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_CLIENT_SERVER'"!]
            NULL_PTR,
            &Rte_Call_SecurityAccess_[!"node:name(.)"!]_GetSeed,
            NULL_PTR,
            NULL_PTR,
          [!ELSE!][!//
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!][!//
        [!ELSE!][!//
          [!IF "DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_FUNC' or DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_FNC'"!][!//
            [!IF "node:exists(DcmDspSecurityGetSeedFnc)"!][!//
              NULL_PTR,
              NULL_PTR,
              &[!"DcmDspSecurityGetSeedFnc"!],
              NULL_PTR,
            [!ELSE!][!//
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
            [!ENDIF!][!//
          [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FUNC' or DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FNC'"!][!//
            [!IF "node:exists(DcmDspSecurityGetSeedFnc)"!][!//
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
              &[!"DcmDspSecurityGetSeedFnc"!],
            [!ELSE!][!//
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
              NULL_PTR,
            [!ENDIF!][!//
          [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_CLIENT_SERVER'"!]
            NULL_PTR,
            NULL_PTR,
            &Rte_Call_SecurityAccess_[!"node:name(.)"!]_GetSeed,
            NULL_PTR,
          [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_CLIENT_SERVER'"!]
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            &Rte_Call_SecurityAccess_[!"node:name(.)"!]_GetSeed,
          [!ELSE!][!//
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!][!//
        [!ENDIF!][!//
        [!/*---CompareKeyFnc------*/!][!//
        [!IF "DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_FUNC' or DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_FNC'"!][!//
          [!IF "node:exists(DcmDspSecurityCompareKeyFnc)"!][!//
            &[!"DcmDspSecurityCompareKeyFnc"!],
            NULL_PTR,
          [!ELSE!][!//
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!][!//
        [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FUNC' or DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FNC'"!][!//
          [!IF "node:exists(DcmDspSecurityCompareKeyFnc)"!][!//
            NULL_PTR,
            &[!"DcmDspSecurityCompareKeyFnc"!],
          [!ELSE!][!//
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!][!//
        [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_CLIENT_SERVER'"!]
          &Rte_Call_SecurityAccess_[!"node:name(.)"!]_CompareKey,
          NULL_PTR,
        [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_CLIENT_SERVER'"!]
          NULL_PTR,
          &Rte_Call_SecurityAccess_[!"node:name(.)"!]_CompareKey,
        [!ELSE!][!//
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!][!//
        [!/*---SecAccessInterface------*/!][!//
        [!IF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FNC'"!]
          DCM_USE_SECURITY_ASYNCH_FUNC,
        [!ELSEIF "DcmDspSecurityUsePort = 'USE_SECURITY_SYNCH_FNC'"!]
          DCM_USE_SECURITY_SYNCH_FUNC,
        [!ELSE!]
          DCM_[!"DcmDspSecurityUsePort"!],
        [!ENDIF!]
        [!"DcmDspSecurityLevel"!]U, /* Security level */
        [!"DcmDspSecuritySeedSize"!]U, /* Seed size */
        [!"DcmDspSecurityKeySize"!]U, /* Key size */
        [!"DcmDspSecurityNumAttDelay"!]U, /* Attempts until delay */
        [!IF "node:exists(DcmDspSecurityADRSize)"!]
          [!"DcmDspSecurityADRSize"!]U, /* ADR Size */
        [!ELSE!]
          0U,
        [!ENDIF!]
      [!ENDINDENT!]
      [!WS "2"!]},
    [!ENDLOOP!][!//
  };
[!ENDIF!]

#if (DCM_MODEDECLARATION_SUPPORT == STD_ON)
#if (DCM_NUM_COMCONTROL_ALL_CHANNEL > 0U)
  /* Array of configuration information for Com Control All Channels */
  CONST(Dcm_ComControlAllChannelType, DCM_CONST) Dcm_ComControlAllChannel[DCM_NUM_COMCONTROL_ALL_CHANNEL] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspComControl/DcmDspComControlAllChannel/*"!]
      [!WS "2"!][!"as:ref(DcmDspAllComMChannelRef)/ComMChannelId"!]U, /* Network Id */
    [!ENDLOOP!]
  };
#endif

#if (DCM_NUM_COMCONTROL_SPECIFIC_CHANNEL > 0U)
  /* Array of configuration information for Com Control All Channels */
  CONST(Dcm_ComControlSpecificChannelType, DCM_CONST) Dcm_ComControlSpecificChannel[DCM_NUM_COMCONTROL_SPECIFIC_CHANNEL] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspComControl/DcmDspComControlSpecificChannel/*"!][!//
    [!WS "2"!]{
      [!INDENT "4"!]
      [!"as:ref(DcmDspSpecificComMChannelRef)/ComMChannelId"!]U, /* Network Id */
      [!"DcmDspSubnetNumber"!]U, /* Subnet Number which controls the specific ComMChannel */
    [!ENDINDENT!]
    [!WS "2"!]},
    [!ENDLOOP!][!//
  };
#endif

#if (DCM_COMCONTROL_LOOKUP_SIZE > 0U)
  /* Array of ComMChannels and SchM Switch pointers for CommunicationControl */
  CONST(Dcm_ComControlLookupType, DCM_CONST) Dcm_ComControlLookUpTable[DCM_COMCONTROL_LOOKUP_SIZE] =
  {
    [!VAR "ChannelList"="'#'"!][!//
    [!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolRx/*"!][!//
      [!IF "not( contains( $ChannelList, concat( '#', ./DcmDslProtocolRxChannelId,'#')))"!]
      [!WS "2"!]{
      [!INDENT "4"!]
        [!"DcmDslProtocolRxChannelId"!]U, /* Network Id */
        &SchM_Switch_Dcm_DcmCommunicationControl_[!"name(as:ref(DcmDslProtocolRxComMChannelRef))"!], /* pointer to SchM Switch for DcmCommunicationControl */
      [!ENDINDENT!]
      [!WS "2"!]},
        [!VAR "ChannelList" = "concat( $ChannelList,'#',./DcmDslProtocolRxChannelId,'#')"!]
      [!ENDIF!][!//
    [!ENDLOOP!][!//

    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspComControl/DcmDspComControlAllChannel/*"!]
      [!IF "not(contains($ChannelList, concat('#', as:ref(DcmDspAllComMChannelRef)/ComMChannelId, '#')))"!]
      [!WS "2"!]{
      [!INDENT "4"!]
        [!"as:ref(DcmDspAllComMChannelRef)/ComMChannelId"!]U, /* Network Id */
        &SchM_Switch_Dcm_DcmCommunicationControl_[!"name(as:ref(./DcmDspAllComMChannelRef))"!], /* pointer to SchM Switch for DcmCommunicationControl */
      [!ENDINDENT!]
      [!WS "2"!]},
        [!VAR "ChannelList" = "concat($ChannelList, as:ref(DcmDspAllComMChannelRef)/ComMChannelId, '#')"!]
      [!ENDIF!]
    [!ENDLOOP!]

    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspComControl/DcmDspComControlSpecificChannel/*"!]
      [!IF "not(contains($ChannelList, concat('#', as:ref(DcmDspSpecificComMChannelRef)/ComMChannelId, '#')))"!]
      [!WS "2"!]{
      [!INDENT "4"!]
        [!"as:ref(DcmDspSpecificComMChannelRef)/ComMChannelId"!]U, /* Network Id */
        &SchM_Switch_Dcm_DcmCommunicationControl_[!"name(as:ref(./DcmDspSpecificComMChannelRef))"!], /* pointer to SchM Switch for DcmCommunicationControl */
      [!ENDINDENT!]
      [!WS "2"!]},
        [!VAR "ChannelList" = "concat($ChannelList, as:ref(DcmDspSpecificComMChannelRef)/ComMChannelId, '#')"!]
      [!ENDIF!]
    [!ENDLOOP!][!//
  };
#endif /* #if (DCM_COMCONTROL_LOOKUP_SIZE > 0U) */
#endif /* #if (DCM_MODEDECLARATION_SUPPORT == STD_ON) */

[!/*------------------------------------------------------------------*/!][!//
[!//
#define DCM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

#if DCM_DID_REF_COUNT > 0
#define DCM_START_SEC_CONST_16
#include <MemMap.h>

/* Array for Dids which is referred from other Dids. */
STATIC CONST(uint16,DCM_CONST) Dcm_DidRefers[DCM_DID_REF_COUNT] =
{
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']/DcmDspDidRef/*"!][!//
    [!WS "2"!][!"num:inttohex(node:ref(.)/DcmDspDidIdentifier)"!]U,
  [!ENDLOOP!][!//
};
#define DCM_STOP_SEC_CONST_16
#include <MemMap.h>
#endif

/*==================[internal data]==============================================================*/

#define DCM_START_SEC_VAR_NO_INIT_8
#include <MemMap.h>

/* Allocate memory of buffers configured for the Dcm module */
[!/* Verify and increase the buffer size with the numbers of extra bytes required for routine control service */!]
[!VAR "buffersRequiredExraByte" = "':'"!][!//
[!VAR "extraBufferSpaceForRoutineControl" = "0"!][!//
[!VAR "currenttxbuffername" = "''"!][!//
[!VAR "currentrxbuffername" = "''"!][!//

[!/* Loop through all the routines configured */!]
[!LOOP "  DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!]

  [!/* Verify if any start in or stop in or requestresults in signals are variable length and not byte aligned */!]
  [!IF "(count(node:ref(DcmDspRoutineInfoRef)/DcmDspStartRoutineIn/DcmDspStartRoutineInSignal/*[DcmDspRoutineSignalType = 'VARIABLE_LENGTH' and ((DcmDspRoutineSignalPos mod 8) != 0)]) > 0)  or (count(node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineStopIn/DcmDspRoutineStopInSignal/*[DcmDspRoutineSignalType = 'VARIABLE_LENGTH' and ((DcmDspRoutineSignalPos mod 8) != 0)]) > 0 ) or (count(node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineRequestResIn/DcmDspRoutineRequestResInSignal/*[DcmDspRoutineSignalType = 'VARIABLE_LENGTH' and ((DcmDspRoutineSignalPos mod 8) != 0)]) > 0)"!]
    [!/* Loop through all the protocols configured */!]
    [!LOOP "../../../DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*"!][!//
      [!/* Check that 0x31 service is enabled and using internal service handler */!]
      [!IF "count(node:ref(DcmDslProtocolSIDTable)/DcmDsdService/*[(not(node:exists(DcmDsdSidTabFnc))) and DcmDsdSidTabServiceId = num:hextoint('0x31')]) > 0 "!]
        [!/* Loop through all Rx channels configured */!]
        [!LOOP "DcmDslConnection/*/DcmDslProtocolRx/*"!][!//
          [!/* Extract the Rxbuffer name of this protocol */!]
          [!VAR "currentrxbuffername" = "node:name(node:ref(DcmDslProtocolRxBufferID))"!][!//
          [!/* Add into buffersRequiredExraByte list */!]
          [!IF "not( contains( $buffersRequiredExraByte, concat( ':', $currentrxbuffername, ':')))"!]
            [!VAR "buffersRequiredExraByte" = "concat( $buffersRequiredExraByte, $currentrxbuffername, ':')"!][!//
            [!/* Increment extraBufferSpaceForRoutineControl whenever new buffer is added to the list */!]
            [!VAR "extraBufferSpaceForRoutineControl" = "$extraBufferSpaceForRoutineControl +1"!][!//
          [!ENDIF!][!//
        [!ENDLOOP!] [!/* End of Rx channels loop */!]
      [!ENDIF!][!//
    [!ENDLOOP!] [!/* End of protocols loop */!]
  [!ENDIF!][!//

  [!/* Verify if any start out or stop out or routine results out signals are variable length and byte aligned */!]
  [!IF "count(node:ref(DcmDspRoutineInfoRef)/DcmDspStartRoutineOut/DcmDspStartRoutineOutSignal/*[DcmDspRoutineSignalType = 'VARIABLE_LENGTH' and ((DcmDspRoutineSignalPos mod 8) != 0)]) > 0  or count(node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineStopOut/DcmDspRoutineStopOutSignal/*[DcmDspRoutineSignalType = 'VARIABLE_LENGTH' and ((DcmDspRoutineSignalPos mod 8) != 0)]) > 0 or count(node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineRequestResOut/DcmDspRoutineRequestResOutSignal/*[DcmDspRoutineSignalType = 'VARIABLE_LENGTH' and ((DcmDspRoutineSignalPos mod 8) != 0)]) > 0"!]
    [!/* Loop through all the protocols configured */!]
    [!LOOP "../../../DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*"!][!//
      [!/* Check that 0x31 service is enabled and using internal service handler */!]
      [!IF "count(node:ref(DcmDslProtocolSIDTable)/DcmDsdService/*[(not(node:exists(DcmDsdSidTabFnc))) and DcmDsdSidTabServiceId = num:hextoint('0x31')]) > 0 "!]
        [!/* Extract the Txbuffer name of this protocol */!]
        [!VAR "currenttxbuffername" = "node:name(node:ref(DcmDslProtocolTxBufferID))"!][!//
        [!/* Add into buffersRequiredExraByte list */!]
        [!IF "not( contains( $buffersRequiredExraByte, concat( ':', $currenttxbuffername, ':')))"!]
          [!VAR "buffersRequiredExraByte" = "concat( $buffersRequiredExraByte, $currenttxbuffername, ':')"!][!//
          [!/* Increment extraBufferSpaceForRoutineControl whenever new buffer is added to the list */!]
          [!VAR "extraBufferSpaceForRoutineControl" = "$extraBufferSpaceForRoutineControl +1"!][!//
        [!ENDIF!][!//
      [!ENDIF!][!//
    [!ENDLOOP!] [!/* End of protocols loop */!]
  [!ENDIF!][!//

[!ENDLOOP!] [!/* End of routines loop */!]

STATIC uint8 Dcm_Buffer[[!"num:integer( sum( DcmConfigSet/*[1]/DcmDsl/DcmDslBuffer/*/DcmDslBufferSize) + $extraBufferSpaceForRoutineControl)"!]];

STATIC uint8 Dcm_NRCBuffer[[!"num:integer( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*) * 3)"!]];

[!VAR "CommonBufferCount"="count(DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*[DcmDslProtocolTxBufferID =  DcmDslConnection/*/DcmDslProtocolRx/*/DcmDslProtocolRxBufferID])"!][!//
[!IF "$CommonBufferCount > 0"!]
[!IF "count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = num:hextoint('0x31')]) > 0 "!]
/* Defining global buffer for Routine control service   */
  [!VAR "SignalLength" = "0"!][!//
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutineInfo/*//DcmDspRoutineSignalType[. = 'VARIABLE_LENGTH']"!][!//
    [!IF "(../DcmDspRoutineSignalLength > $SignalLength) and ((node:name(../../.) = 'DcmDspRoutineStartOutSignal') or (node:name(../../.) = 'DcmDspRoutineStopOutSignal') or (node:name(../../.) = 'DcmDspRoutineRequestResOutSignal'))"!] 
      [!VAR "SignalLength" = "../DcmDspRoutineSignalLength"!][!//
    [!ENDIF!]
  [!ENDLOOP!][!//
[!IF "$SignalLength > 0"!]
STATIC uint8 Dcm_RoutineControlBuffer[[!"num:i(($SignalLength + 7) div 8)"!]];
[!ENDIF!]
[!ENDIF!][!//
[!ENDIF!][!//
#define DCM_STOP_SEC_VAR_NO_INIT_8
#include <MemMap.h>

/*==================[external constants]=========================================================*/
[!/*ChannelID holds the different 'DcmDslProtocolRxChannelId' configured. This will use for the
generation of the array Dcm_ChannelId[] and also for genarating the index in Dcm_RxPduIdConfig*/!][!//
[!VAR "ChannelID" =  "' '"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolRx/*"!][!//
  [!IF "not( contains( $ChannelID, concat( ' ', ./DcmDslProtocolRxChannelId,' ')))"!][!//
    [!VAR "ChannelID" = "concat( $ChannelID,' ',./DcmDslProtocolRxChannelId,' ')"!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//
#define DCM_START_SEC_CONST_8
#include <MemMap.h>
/* Array holding the channel identifiers configured */[!//
CONST(NetworkHandleType,DCM_CONST) Dcm_ChannelId[DCM_NUM_CHANNELID] =
{
[!FOR "i" = "1" TO "count(text:split($ChannelID))"!]
  [!WS "2"!][!"text:split($ChannelID)[position() = $i]"!]U,
[!ENDFOR!]
};
#define DCM_STOP_SEC_CONST_8
#include <MemMap.h>

#define DCM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief Configuration structure */
CONST(Dcm_ConfigType, DCM_CONST) [!"name(DcmConfigSet/*[1])"!] = {0U};

[!/*------------------------------------------------------------------*/!][!//

[!IF "node:exists(DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspAddressAndLengthFormatIdentifier)"!][!//
  /* Array of configuration information of all Supported AddressAndLengthFormatIdentifiers */
  CONST(uint8, DCM_CONST) Dcm_SupportedAddAndLenFormatIds[DCM_NUM_ADDR_LENGTH_FORMAT_IDENTIFIER] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspAddressAndLengthFormatIdentifier/DcmDspSupportedAddressAndLengthFormatIdentifier/*"!][!//
      [!WS "2"!][!"num:inttohex(.)"!]U,
    [!ENDLOOP!][!//
  };
[!ENDIF!]

/* Array of configuration information for diagnostic sessions */
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspSession/*"!]
  CONST(Dcm_SesTabEntryConfigType, DCM_CONST) Dcm_SesTabEntryConfig[!"./@index"!][[!"num:integer( count(./DcmDspSessionRow/*))"!]] =
  {
  [!LOOP "./DcmDspSessionRow/*"!][!//
    [!WS "2"!]{
      [!INDENT "4"!]
        [!"num:integer(floor((DcmDspSessionP2StarServerMax) div $DcmTaskTime))"!]U,/*P2StarMaxTime*/
        [!"DcmDspSessionForBoot"!],/*specifies whether this session allows to jump to Bootloader */
        [!"num:integer(floor(DcmDspSessionP2ServerMax div $DcmTaskTime))"!]U, /*P2MaxTime*/
        [!"DcmDspSessionLevel"!]U,/* Session type */
        [!IF "../../../../../DcmGeneral/DcmModeDeclarationSupport = 'true'"!]
          RTE_MODE_DcmDiagnosticSessionControl_[!"name(.)"!] /* Session type generate by Rte */
        [!ENDIF!]
      [!ENDINDENT!]
    [!WS "2"!]},
  [!ENDLOOP!][!//
  };
[!ENDLOOP!]

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
/* Holds configuration information for diagnostic sessions */
CONST(Dcm_SesTabConfigType, DCM_CONST) Dcm_SesTabConfig[DCM_NUM_PROTOCOL] =
{
  [!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*"!]
    [!WS "2"!]{
      [!INDENT "4"!]
        Dcm_SesTabEntryConfig[!"as:ref(DcmDslProtocolSessionRef)/@index"!], /* Pointer to session configurations */
        [!"num:integer( count(as:ref(DcmDslProtocolSessionRef)/*/*))"!]U, /* Number of configured sessions */
        [!VAR "Indx" = "(as:ref(DcmDslProtocolSessionRef)/@index)"!]
        [!LOOP "../../../../DcmDsp/DcmDspSession/*/DcmDspSessionRow/*"!]
          [!IF "(./DcmDspSessionLevel = 1) and (../../@index = $Indx)"!]
            [!"./@index"!]U /* Index of default session */
          [!ENDIF!]
        [!ENDLOOP!]
      [!ENDINDENT!]
    [!WS "2"!]},
  [!ENDLOOP!]
};

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//

/* Holds configuration information for security levels */
CONST(Dcm_SecTabConfigType, DCM_CONST) Dcm_SecTabConfig =
{
  [!IF "count( DcmConfigSet/*[1]/DcmDsp/DcmDspSecurity/DcmDspSecurityRow/*) > 0"!]
    [!WS "2"!]Dcm_SecTabEntryConfig,/* Pointer to session configurations */
  [!ELSE!]
    [!WS "2"!]NULL_PTR,
  [!ENDIF!]
  [!WS "2"!][!"num:integer( count( DcmConfigSet/*[1]/DcmDsp/DcmDspSecurity/DcmDspSecurityRow/*))"!]U /* Number of configured security levels */
};

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//

/* Initialize global array holding the configuration for each buffer */
CONST(Dcm_BufferConfigType, DCM_CONST) Dcm_BufferConfig[[!"num:integer( count(DcmConfigSet/*[1]/DcmDsl/DcmDslBuffer/*))"!]] =
{
  [!VAR "BufferPos" = "0"!][!//
  [!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslBuffer/*"!][!//
    [!WS "2"!]{
      [!INDENT "4"!]
        &Dcm_Buffer[[!"num:integer( $BufferPos)"!]], /* Pointer to buffer */
        &Dcm_BufferInfo[[!"@index"!]], /* Pointer to buffer status information */
        [!IF "( contains( $buffersRequiredExraByte, concat( ':', node:name(.), ':')))"!]
          [!VAR "BufferPos" = "$BufferPos + DcmDslBufferSize + 1"!][!//
          [!"num:integer(DcmDslBufferSize)"!]U /* Size of buffer - Used extra 1 byte for 0x31 service */
        [!ELSE!]
          [!VAR "BufferPos" = "$BufferPos + DcmDslBufferSize "!][!//
          [!"num:integer(DcmDslBufferSize)"!]U /* Size of buffer*/
        [!ENDIF!][!//
      [!ENDINDENT!]
    [!WS "2"!]},
  [!ENDLOOP!]
};

/* Initialize global array holding the configuration for each NRC buffer */
CONST(Dcm_BufferConfigType, DCM_CONST) Dcm_NRCBufferConfig[[!"num:integer( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*))"!]] =
{
  [!VAR "BufferPos" = "0"!][!//
  [!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*"!][!//
    [!WS "2"!]{
      [!INDENT "4"!]
        &Dcm_NRCBuffer[[!"num:integer( $BufferPos)"!]], /* Pointer to buffer */
        &Dcm_NRCBufferInfo[[!"@index"!]], /* Pointer to buffer status information */
        3U /* Size of buffer*/
      [!ENDINDENT!]
    [!WS "2"!]},[!/*
    */!][!VAR "BufferPos" = "$BufferPos + 3"!][!/*
    */!]
  [!ENDLOOP!]
};

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//

/* Initialize global array holding the configuration for the service Id table */
CONST(Dcm_SidTabConfigType, DCM_CONST) Dcm_SidTabConfig[[!"num:integer( count( DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*))"!]] =
{
  [!FOR "SidTabId"="0" TO "count( DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*) - 1"!][!//
    [!SELECT "DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*[DcmDsdSidTabId = $SidTabId]"!][!//
    [!WS "2"!]{
      [!WS "4"!]Dcm_SidTabEntryConfig[!"$SidTabId"!], /* Pointer to array of service table entries */
      [!WS "4"!][!"num:integer(count(DcmDsdService/*))"!]U /* Number of service entries within this table */
    [!WS "2"!]},
    [!ENDSELECT!][!//
  [!ENDFOR!][!//
};

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//

/* Initialize global array holding the protocol configuration */
CONST(Dcm_HsmConfigType, DCM_CONST) Dcm_HsmConfig[[!"num:integer( count( DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*))"!]] =
{
  [!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*"!][!//
  [!WS "2"!]{
    [!INDENT "4"!]
      [!IF "node:exists(DcmDslProtocolTransType)"!][!//
        DCM_ROE_[!"DcmDslProtocolTransType"!], /* Protocol transmission type */
      [!ELSE!][!//
        DCM_ROE_NOTYPE, /* Protocol transmission type */
      [!ENDIF!][!//
      [!"num:integer(floor(DcmTimStrP2ServerAdjust div $DcmTaskTime))"!]U, /*P2ServerAdjustTime*/
      [!"num:integer(floor(DcmTimStrP2StarServerAdjust div $DcmTaskTime))"!]U, /*P2StarServerAdjustTime */      [!//
      [!"DcmDslProtocolID"!], /* Protocol type */
      [!"DcmDslProtocolPriority"!]U, /* Priority */
      /* !LINKSTO Dcm.LinktoServiceTable.ProtocolInit,1 */
      [!"num:integer( as:ref( DcmDslProtocolSIDTable)/DcmDsdSidTabId)"!]U, /* Dcm_SidTabConfigType */
      [!"num:integer( as:ref( DcmDslProtocolTxBufferID)/@index)"!]U, /* Tx buffer Id */
      [!"text:toupper(DcmDslProtocolEndiannessConvEnabled)"!], /* endianness Conversion */
      [!"text:toupper(DcmSendRespPendOnTransToBoot)"!] /* Send RP OnTransToBoot */
    [!ENDINDENT!]
  [!WS "2"!]},
[!ENDLOOP!][!//
};
[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//

/* Initialize global array holding the protocol configuration for each Rx Pdu Id */
CONST(Dcm_RxPduIdConfigType, DCM_CONST) Dcm_RxPduIdConfig[DCM_NUM_RX_PDU_ID] =
{
  [!FOR "RxPduId" = "0" TO "$DcmRxPduCount - 1"!][!/*
    */!][!SELECT "DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*/DcmDslProtocolRx/*[DcmDslProtocolRxPduId = $RxPduId]"!][!/*
      */!][!FOR "i" = "1" TO "count(text:split($ChannelID))"!][!/*
          */!][!IF "DcmDslProtocolRxChannelId = text:split($ChannelID)[position() = $i]"!][!BREAK!][!ENDIF!][!/*
      */!][!ENDFOR!][!//
    [!VAR "ChannelID_Index" = "num:i($i - 1)"!][!//
    [!VAR "BufferId" = "num:integer( as:ref( DcmDslProtocolRxBufferID)/@index)"!][!//
    [!VAR "ConnId" = "-1"!]
    [!VAR "ConnId_i" = "0"!]
    [!LOOP "node:order(as:modconf('Dcm')[1]/DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*[.='DcmDslMainConnection'],'DcmDslProtocolTx/DcmDslTxConfirmationPduId')"!]
      [!IF "count(./DcmDslProtocolRx/*/DcmDslProtocolRxPduId[.=number($RxPduId)]) > 0"!]
        [!VAR "ConnId" = "$ConnId_i"!]
        [!BREAK!]
      [!ENDIF!]
      [!VAR "ConnId_i" = "$ConnId_i+1"!]
    [!ENDLOOP!]
  [!WS "2"!]{
    [!INDENT "4"!]
      [!"../../DcmDslProtocolRxTesterSourceAddr"!]U, /* Tester source address configured per protocol */
      [!"DcmDslProtocolRxAddrType"!], /* Addressing Type for this Pdu Id */
      [!"$BufferId"!]U, /* ID of buffer used for this Rx Pdu Id */
      [!"num:integer( ../../../../@index)"!]U, /* ID of protocol for this Rx Pdu Id */
      [!"num:i($ConnId)"!]U, /* ID of connection for this Rx Pdu Id */
      [!"$ChannelID_Index"!]U /* Index of Channel Identifier in Dcm_ChannelId[] */
    [!ENDINDENT!]
  [!WS "2"!]},
    [!ENDSELECT!][!//
  [!ENDFOR!][!//
};

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//

/* Initialize global array holding the TxPduId and TxConfirmationPduId configuration for Main connection configuration **/
CONST(Dcm_TxPduIdConfigType, DCM_CONST) Dcm_TxPduIds[DCM_NUM_TX_PDU_ID] =
{
  [!LOOP "node:order(as:modconf('Dcm')[1]/DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*[.='DcmDslMainConnection'],'DcmDslProtocolTx/DcmDslTxConfirmationPduId')"!][!//
    [!INDENT "2"!]
    {
      [!INDENT "4"!]
        [!"asc:getPduId( 'PduR','ForUpperLayer','Tp','Tx', asc:getPdus('PduR','ForUpperLayer','Tp','Tx',1,./DcmDslProtocolTx/DcmDslProtocolTxPduRef  )[1]) "!]U,
        [!"./DcmDslProtocolTx/DcmDslTxConfirmationPduId"!]U,
      [!ENDINDENT!]
    },
    [!ENDINDENT!]
  [!ENDLOOP!]
};

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
[!//
[!LOOP "DcmConfigSet/*/DcmDsp/DcmDspDid/*"!]
[!/*-Check for the service 0x22 .If a DataConfigured as USE_DATA_SYNCH_FUNC or USE_DATA_ASYNCH_FUNC
     or USE_DATA_SYNCH_FNC or USE_DATA_ASYNCH_FNC, its readdata and conditioncheck functions must be
     configured provided if it is linked to a
   signal with read access-----*/!][!//
  [!IF "node:exists(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidRead) = 'true'"!]
    [!LOOP "DcmDspDidSignal/*"!]
      [!SELECT "node:ref(DcmDspDidDataRef)"!][!//
        [!IF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC') and (not(node:exists(DcmDspDataConditionCheckReadFnc)))"!]
        [!ERROR!]
          Configure Conditioncheck operation for Data configuration [!"node:name(.)"!]
          This Data is configured as '[!"node:value(DcmDspDataUsePort)"!]' and is mapped to a signal which has read access.
        [!ENDERROR!]
        [!ENDIF!]
        [!IF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC') and (not(node:exists(DcmDspDataReadFnc)))"!]
          [!ERROR!]
            Configure ReadData operation for Data configuration [!"node:name(.)"!]
            This Data is configured as '[!"node:value(DcmDspDataUsePort)"!]' and is mapped to
            a signal which has read access.
          [!ENDERROR!]
        [!ENDIF!]
      [!ENDSELECT!]
    [!ENDLOOP!]
  [!ENDIF!]
[!/*-1) Check for the service 0x2E .If a DataConfigured as USE_DATA_SYNCH_FUNC or USE_DATA_ASYNCH_FUNC
        or USE_DATA_SYNCH_FNC or USE_DATA_ASYNCH_FNC, its writedata functions must be configured provided
        if it is linked to a signal with write access.
   -2) If the fixed lenth is false only one signal can be configured for a Did which has write
    access ---*/!][!//
  [!IF "node:exists(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidWrite) = 'true'"!]
    [!VAR "numsignals" = "count(DcmDspDidSignal/*)"!]
    [!LOOP "DcmDspDidSignal/*"!]
      [!SELECT "node:ref(DcmDspDidDataRef)"!][!//
        [!IF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC') and (not(node:exists(DcmDspDataWriteFnc)))"!]
        [!ERROR!]
            Configure WriteData operation for Data configuration [!"node:name(.)"!]
            This Data is configured as '[!"node:value(DcmDspDataUsePort)"!]' and is mapped to
            a signal which has write access.
          [!ENDERROR!]
        [!ENDIF!]
        [!IF "($numsignals > 1) and (node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'false')"!]
          [!ERROR!]
            /* !LINKSTO Dcm620,2 */
            Fixed length is false for the data [!"node:name(.)"!]. It is mapped to a Did with
            more than one signals.
          [!ENDERROR!]
        [!ENDIF!]
      [!ENDSELECT!]
    [!ENDLOOP!]
  [!ENDIF!]
  [!/* Check for the service 0x2F. If the fixed lenth is false only one signal can be configured
         for a Did which has Control access ---*/!][!//
  [!IF "node:exists(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl) = 'true'"!]
    [!VAR "numsignals" = "count(DcmDspDidSignal/*)"!]
    [!LOOP "DcmDspDidSignal/*"!]
      [!SELECT "node:ref(DcmDspDidDataRef)"!][!//
        [!IF "($numsignals > 1) and (node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'false')"!]
          [!ERROR!]
            /* !LINKSTO Dcm620,2 */
            Fixed length is false for the data [!"node:name(.)"!]. It is mapped to a Did with
            more than one signal.
          [!ENDERROR!]
        [!ENDIF!]
      [!ENDSELECT!]
    [!ENDLOOP!]
  [!ENDIF!]
[!ENDLOOP!][!//
[!//

[!/*------------------------------------------------------------------*/!][!//
[!IF "count(DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']) > 0"!]
  [!VAR "ReadSesPos" = "0"!][!//
  [!VAR "ReadSecPos" = "0"!][!//
  [!VAR "WriteSesPos" = "0"!][!//
  [!VAR "WriteSecPos" = "0"!][!//
  [!VAR "ControlSesPos" = "0"!][!//
  [!VAR "ControlSecPos" = "0"!][!//
  [!VAR "RefPos" = "num:integer(0)"!][!//
  [!VAR "SignalPos" = "num:integer(0)"!][!//
  CONST(Dcm_DidServicesType,DCM_CONST) Dcm_DidTable[DCM_NUM_DID] =
{
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*[DcmDspDidUsed = 'true']"!]
    [!VAR "ReadSesTypes" = "num:integer(count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidRead/DcmDspDidReadSessionRef/*))"!][!//
    [!VAR "ReadSecTypes" = "num:integer(count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidRead/DcmDspDidReadSecurityLevelRef/*))"!][!//
    [!VAR "WriteSesTypes" = "num:integer(count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidWrite/DcmDspDidWriteSessionRef/*))"!][!//
    [!VAR "WriteSecTypes" = "num:integer(count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidWrite/DcmDspDidWriteSecurityLevelRef/*))"!][!//
    [!VAR "ControlSesTypes" = "num:integer(count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl/DcmDspDidControlSessionRef/*))"!][!//
    [!VAR "ControlSecTypes" = "num:integer(count(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl/DcmDspDidControlSecurityLevelRef/*))"!][!//
    [!VAR "Refs" = "num:integer(count(DcmDspDidRef/*))"!][!//
    [!VAR "NumSignals" = "num:integer(count(DcmDspDidSignal/*))"!][!//
    [!WS "2"!]{
      [!INDENT "4"!]
        /* Configurations for read access*/
        [!IF "node:exists(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidRead) = 'true'"!]
        {
          [!INDENT "6"!]
            [!IF "$ReadSecTypes > 0"!]
              &( Dcm_ReadDidSecTypes[[!"num:integer( $ReadSecPos)"!]]),
            [!ELSE!]
              NULL_PTR,
            [!ENDIF!]
            [!IF "$ReadSesTypes > 0"!]
              &( Dcm_ReadDidSesTypes[[!"num:integer( $ReadSesPos)"!]]),
            [!ELSE!]
              NULL_PTR,
            [!ENDIF!]
            [!"$ReadSecTypes"!],
            [!"$ReadSesTypes"!],
            TRUE
          [!ENDINDENT!]
        },
        [!ELSE!]
          {NULL_PTR, NULL_PTR ,0,0,FALSE},
        [!ENDIF!]
        /* Configurations for write access*/
        [!IF "node:exists(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidWrite) = 'true'"!]
        {
          [!INDENT "6"!]
            [!IF "$WriteSecTypes > 0"!]
              &( Dcm_WriteDidSecTypes[[!"num:integer( $WriteSecPos)"!]]),
            [!ELSE!]
              NULL_PTR,
            [!ENDIF!]
            [!IF "$WriteSesTypes > 0"!]
              &( Dcm_WriteDidSesTypes[[!"num:integer( $WriteSesPos)"!]]),
            [!ELSE!]
              NULL_PTR,
            [!ENDIF!]
            [!"$WriteSecTypes"!],
            [!"$WriteSesTypes"!],
            TRUE
          [!ENDINDENT!]
        },
        [!ELSE!]
          {NULL_PTR, NULL_PTR ,0,0,FALSE},
        [!ENDIF!]
        /* Configurations for Control access*/
        [!IF "node:exists(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl) = 'true'"!]
        {
         [!INDENT "6"!]
          [!IF "$ControlSecTypes > 0"!]
            &( Dcm_ControlDidSecLevels[[!"num:integer( $ControlSecPos)"!]]),
           [!ELSE!]
             NULL_PTR,
           [!ENDIF!]
          [!IF "$ControlSesTypes > 0"!]
            &( Dcm_ControlDidSesLevels[[!"num:integer( $ControlSesPos)"!]]),
           [!ELSE!]
             NULL_PTR,
           [!ENDIF!]
          [!"$ControlSecTypes"!],
          [!"$ControlSesTypes"!],
          TRUE
          [!ENDINDENT!]
        },
        [!ELSE!]
          {NULL_PTR, NULL_PTR, 0, 0, FALSE},
        [!ENDIF!]
        [!IF "$Refs > 0"!]
          &( Dcm_DidRefers[[!"num:integer($RefPos)"!]]),
        [!ELSE!]
          NULL_PTR,
        [!ENDIF!]
        [!IF "$NumSignals > 0"!]
          &( Dcm_DidSignals[[!"num:integer($SignalPos)"!]]),
        [!ELSE!]
          NULL_PTR,
        [!ENDIF!]
        [!IF "node:exists(DcmDspDidExtRoe)"!]
          [!IF "node:exists(DcmDspDidExtRoe/DcmDspDidRoeActivateFnc)"!]
            &[!"DcmDspDidExtRoe/DcmDspDidRoeActivateFnc"!],
          [!ELSE!]
            &Rte_Call_ROEServices_[!"node:name(.)"!]_DcmDspDidExtRoe_ActivateEvent,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
        [!ENDIF!]
        [!"$NumSignals"!]U,
        [!"num:inttohex(DcmDspDidIdentifier)"!]U,
        [!"$Refs"!]U,
        [!/*---RoeEventId------*/!][!//
        [!IF "node:exists(DcmDspDidExtRoe)"!][!//
          [!"DcmDspDidExtRoe/DcmDspDidRoeEventId"!]U,
        [!ELSE!][!//
          0xFFU,
        [!ENDIF!]
        [!/*---ExtRoeSupport------*/!][!//
        [!IF "node:exists(DcmDspDidExtRoe) = 'true'"!]
          TRUE,
        [!ELSE!][!//
          FALSE,
        [!ENDIF!]
          [!/*---FreezeCurrentState------*/!][!//
        [!IF "(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl/DcmDspDidFreezeCurrentState = 'true')"!]
          TRUE,
        [!ELSE!]
          FALSE,
        [!ENDIF!]
        [!/*---ResetToDefault------*/!][!//
        [!IF "(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl/DcmDspDidResetToDefault = 'true')"!]
          TRUE,
        [!ELSE!]
          FALSE,
        [!ENDIF!]
        [!/*---ShortTermAdjustment------*/!][!//
        [!IF "(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl/DcmDspDidShortTermAdjustment = 'true')"!]
          TRUE,
        [!ELSE!]
          FALSE,
        [!ENDIF!]
        [!/*---ReturnControlToEcu------*/!][!//
        [!IF "(node:ref(DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl/DcmDspDidReturnControlToEcu = 'true')"!]
          TRUE
        [!ELSE!]
          FALSE
        [!ENDIF!]
      [!ENDINDENT!]
    [!WS "2"!]},
    [!VAR "ReadSesPos" = "$ReadSesPos + $ReadSesTypes"!][!//
    [!VAR "ReadSecPos" = "$ReadSecPos + $ReadSecTypes"!][!//
    [!VAR "WriteSesPos" = "$WriteSesPos + $WriteSesTypes"!][!//
    [!VAR "WriteSecPos" = "$WriteSecPos + $WriteSecTypes"!][!//
    [!VAR "ControlSesPos" = "$ControlSesPos + $ControlSesTypes"!][!//
    [!VAR "ControlSecPos" = "$ControlSecPos + $ControlSecTypes"!][!//
    [!VAR "RefPos" = "num:integer($RefPos + $Refs)"!][!//
    [!VAR "SignalPos" = "num:integer($SignalPos + $NumSignals)"!][!//
  [!ENDLOOP!]
};
[!ENDIF!]
[!/*------------------------------------------------------------------*/!][!//
[!/*--------To check one signal data has reference to different DidInfo's in in multiple DID's
            which have different combination of IOControl parameter enables-------------*/!][!//
[!LOOP "DcmConfigSet/*/DcmDsp/DcmDspDid/*"!]
[!LOOP "DcmDspDidSignal/*"!]
    [!VAR "data1" = "./DcmDspDidDataRef"!]
    [!VAR "data2" = "../../DcmDspDidInfoRef"!]
    [!LOOP "../../../*"!]
      [!LOOP "./DcmDspDidSignal/*"!]
        [!IF "$data1 = ./DcmDspDidDataRef"!]
          [!IF "((node:exists(as:ref($data2)//DcmDspDidFreezeCurrentState)) and
                 (node:exists(as:ref(../../DcmDspDidInfoRef)//DcmDspDidFreezeCurrentState)) and
                 (as:ref($data2)//DcmDspDidFreezeCurrentState != as:ref(../../DcmDspDidInfoRef)//DcmDspDidFreezeCurrentState))
                or
                ((node:exists(as:ref($data2)//DcmDspDidResetToDefault)) and
                 (node:exists(as:ref(../../DcmDspDidInfoRef)//DcmDspDidResetToDefault)) and
                 (as:ref($data2)//DcmDspDidResetToDefault != as:ref(../../DcmDspDidInfoRef)//DcmDspDidResetToDefault))
                or
                ((node:exists(as:ref($data2)//DcmDspDidReturnControlToEcu)) and
                 (node:exists(as:ref(../../DcmDspDidInfoRef)//DcmDspDidReturnControlToEcu)) and
                 (as:ref($data2)//DcmDspDidReturnControlToEcu != as:ref(../../DcmDspDidInfoRef)//DcmDspDidReturnControlToEcu))
                or
                ((node:exists(as:ref($data2)//DcmDspDidShortTermAdjustment)) and
                (node:exists(as:ref(../../DcmDspDidInfoRef)//DcmDspDidShortTermAdjustment)) and
                (as:ref($data2)//DcmDspDidShortTermAdjustment != as:ref(../../DcmDspDidInfoRef)//DcmDspDidShortTermAdjustment))"!]
                  [!ERROR!]
                    Same signal data [!"$data1"!] is configured for many DID's which have reference to different DID info's
                  [!ENDERROR!]
          [!ENDIF!]
        [!ENDIF!]
      [!ENDLOOP!]
    [!ENDLOOP!]
[!ENDLOOP!]
[!ENDLOOP!]
[!/*------------------------------------------------------------------*/!][!//

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
[!IF "count(DcmConfigSet/*/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*/DcmDspReadMemoryRangeInfo/*) > 0"!]
  [!VAR "ReadMemSecPos" = "0"!][!//
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*"!]
    /** \brief Provides the range of memory address allowed for reading */
    CONST(Dcm_MemoryRangeInfoType,DCM_CONST) Dcm_ReadMemoryRangeInfoTable[!"./@index"!][[!"num:i(count(DcmDspReadMemoryRangeInfo/*))"!]] =
    {
    [!LOOP "DcmDspReadMemoryRangeInfo/*"!]
      [!VAR "ReadMemSecLevels" = "num:integer(count(node:refs(DcmDspReadMemoryRangeSecurityLevelRef/*)))"!][!//
      [!WS "2"!]{
        [!INDENT "4"!]
          [!/*---MemoryRangeLow------*/!][!//
          [!"DcmDspReadMemoryRangeLow"!]U,
          [!/*---MemoryRangeHigh------*/!][!//
          [!"DcmDspReadMemoryRangeHigh"!]U,
          /* Configurations for Read Memory access*/
          [!IF "node:exists(node:refs(DcmDspReadMemoryRangeSecurityLevelRef/*))"!]
            [!INDENT "6"!]
              [!IF "$ReadMemSecLevels > 0"!]
                &(Dcm_ReadMemSecLevels[[!"num:integer( $ReadMemSecPos)"!]]),
              [!ELSE!]
                NULL_PTR,
              [!ENDIF!]
                [!"$ReadMemSecLevels"!]U,
            [!ENDINDENT!]
          [!ELSE!]
            NULL_PTR, 0,
          [!ENDIF!]
        [!ENDINDENT!]
      [!WS "2"!]},
      [!VAR "ReadMemSecPos" = "$ReadMemSecPos + $ReadMemSecLevels"!][!//
    [!ENDLOOP!]
    };
  [!ENDLOOP!]
[!ENDIF!]

[!IF "count(DcmConfigSet/*/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*/DcmDspWriteMemoryRangeInfo/*) > 0"!]
  [!VAR "WriteMemSecPos" = "0"!][!//
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*"!]
    /** \brief Provides the range of memory address allowed for writing */
    CONST(Dcm_MemoryRangeInfoType,DCM_CONST) Dcm_WriteMemoryAddressRangeTable[!"./@index"!][[!"num:i(count(DcmDspWriteMemoryRangeInfo/*))"!]] =
    {
    [!LOOP "DcmDspWriteMemoryRangeInfo/*"!]
      [!VAR "NumSecLevels" = "num:integer(count(node:refs(DcmDspWriteMemoryRangeSecurityLevelRef/*)))"!][!//
      [!WS "2"!]{
        [!INDENT "4"!]
          [!/*---MemoryRangeLow------*/!][!//
          [!"DcmDspWriteMemoryRangeLow"!]U,
          [!/*---MemoryRangeHigh------*/!][!//
          [!"DcmDspWriteMemoryRangeHigh"!]U,
          /* Configurations for Write Memory access*/
          [!IF "node:exists(node:refs(DcmDspWriteMemoryRangeSecurityLevelRef/*))"!]
            [!IF "$NumSecLevels > 0"!]
              &(Dcm_WriteMemSecLevels[[!"num:integer( $WriteMemSecPos)"!]]),
            [!ELSE!]
              NULL_PTR,
            [!ENDIF!]
              [!"$NumSecLevels"!]U,
          [!ELSE!]
            NULL_PTR, 0,
          [!ENDIF!]
        [!ENDINDENT!]
      [!WS "2"!]},
      [!VAR "WriteMemSecPos" = "$WriteMemSecPos + $NumSecLevels"!][!//
    [!ENDLOOP!]
    };
  [!ENDLOOP!]
[!ENDIF!]

[!IF "count(DcmConfigSet/*/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*) > 0"!]
  /** \brief Provides the configuration of memory access requested through diagnostic services: WriteMemoryByAddress and readMemoryByAddress*/
  CONST(Dcm_MemoryIdInfoType, DCM_CONST) Dcm_MemoryIdInfoAndIdValueTable[DCM_NUM_MEMORY_IDINFO] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspMemory/DcmDspMemoryIdInfo/*"!]
      [!WS "2"!]{
        [!INDENT "4"!]
          [!IF "count(./DcmDspWriteMemoryRangeInfo/*) > 0"!]
            [!WS "2"!]Dcm_WriteMemoryAddressRangeTable[!"./@index"!], /* Pointer to Write memory range info configurations */
          [!ELSE!]
            NULL_PTR,
          [!ENDIF!]
          [!IF "count(./DcmDspReadMemoryRangeInfo/*) > 0"!]
            [!WS "2"!]Dcm_ReadMemoryRangeInfoTable[!"./@index"!], /* Pointer to Read memory range info configurations */
          [!ELSE!]
            NULL_PTR,
          [!ENDIF!]
          [!"num:i(count(./DcmDspWriteMemoryRangeInfo/*))"!]U, /*Number of write RangeInfo in id info */
          [!"num:i(count(./DcmDspReadMemoryRangeInfo/*))"!]U, /*Number of read RangeInfos configured for this 'MemoryId' */
          [!/*---MemoryIdValue------*/!][!//
          [!IF "node:exists(DcmDspMemoryIdValue)"!]
            [!"DcmDspMemoryIdValue"!]U
          [!ELSE!]
            0U
          [!ENDIF!]
        [!ENDINDENT!]
      [!WS "2"!]},
    [!ENDLOOP!]
  };
[!ENDIF!]

[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
[!IF "count(DcmConfigSet/*[1]/DcmDsp/DcmDspData/*) > 0"!]
CONST(Dcm_DidDataType,DCM_CONST) Dcm_DidDataTable[DCM_NUM_DID_DATA] =
{
  [!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspData/*"!]
    [!VAR "ReadAccess" = "'FALSE'"!]
    [!VAR "WriteAccess" = "'FALSE'"!]
    [!VAR "ControlAccess" = "'FALSE'"!]
    [!VAR "Data" = "node:path(.)"!]
    [!/* Check that DspData element is refered in a Did, which has the read access*/!]
    [!LOOP "../../DcmDspDid/*/DcmDspDidSignal/*[(node:path(node:ref(DcmDspDidDataRef)) = $Data)]"!]
      [!IF "node:exists(node:ref(../../DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidRead) = 'true'"!]
        [!VAR "ReadAccess" = "'TRUE'"!]
      [!BREAK!]
      [!ENDIF!]
    [!ENDLOOP!]
    [!/* Check that DspData element is refered in a Did, which has the write access*/!]
    [!LOOP "../../DcmDspDid/*/DcmDspDidSignal/*[(node:path(node:ref(DcmDspDidDataRef)) = $Data)]"!]
      [!IF "node:exists(node:ref(../../DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidWrite) = 'true'"!]
        [!VAR "WriteAccess" = "'TRUE'"!]
      [!BREAK!]
      [!ENDIF!]
    [!ENDLOOP!]
    [!/* Check that DspData element is refered in a Did, which has the Control access*/!]
    [!LOOP "../../DcmDspDid/*/DcmDspDidSignal/*[(node:path(node:ref(DcmDspDidDataRef)) = $Data)]"!]
      [!IF "node:exists(node:ref(../../DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl) = 'true'"!]
        [!VAR "ControlAccess" = "'TRUE'"!]
      [!BREAK!]
      [!ENDIF!]
    [!ENDLOOP!]

  [!WS "2"!]{
    [!INDENT "4"!]
      [!/*---ConditionCheckRead------*/!][!//
      [!IF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
        [!IF "node:exists(DcmDspDataConditionCheckReadFnc)"!][!//
          &[!"DcmDspDataConditionCheckReadFnc"!],
          NULL_PTR,
        [!ELSE!][!//
        NULL_PTR,
        NULL_PTR,
      [!ENDIF!][!//
      [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
        [!IF "node:exists(DcmDspDataConditionCheckReadFnc)"!][!//
          NULL_PTR,
          &[!"DcmDspDataConditionCheckReadFnc"!],
        [!ELSE!][!//
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!][!//
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_CLIENT_SERVER') and ($ReadAccess = 'TRUE')"!]
        &Rte_Call_DataServices_[!"node:name(.)"!]_ConditionCheckRead,
        NULL_PTR,
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_ASYNCH_CLIENT_SERVER') and ($ReadAccess = 'TRUE')"!]
        NULL_PTR,
        &Rte_Call_DataServices_[!"node:name(.)"!]_ConditionCheckRead,
      [!ELSE!][!//
        NULL_PTR,
        NULL_PTR,
      [!ENDIF!][!//
      [!/*---ReadDataLength------*/!][!//
      [!IF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
        [!IF "node:exists(DcmDspDataReadDataLengthFnc)"!][!//
          &[!"DcmDspDataReadDataLengthFnc"!],
        [!ELSE!][!//
          NULL_PTR,
        [!ENDIF!][!//
      [!ELSEIF "((DcmDspDataUsePort = 'USE_DATA_SYNCH_CLIENT_SERVER')or(DcmDspDataUsePort = 'USE_DATA_ASYNCH_CLIENT_SERVER')) and (node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'false') and ($ReadAccess = 'TRUE')"!]
        &Rte_Call_DataServices_[!"node:name(.)"!]_ReadDataLength,
      [!ELSE!][!//
        NULL_PTR,
      [!ENDIF!][!//
      [!/*---ReadDataFnc------*/!][!//
      [!IF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
        [!IF "node:exists(DcmDspDataReadFnc)"!][!//
          &[!"DcmDspDataReadFnc"!],
        [!ELSE!][!//
          NULL_PTR,
        [!ENDIF!][!//
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_CLIENT_SERVER') and ($ReadAccess = 'TRUE')"!]
        &Rte_Call_DataServices_[!"node:name(.)"!]_ReadData,
      [!ELSE!][!//
        NULL_PTR,
      [!ENDIF!][!//
      [!/*---CallReadData ------*/!][!//
      [!IF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
        [!IF "node:exists(DcmDspDataReadFnc)"!][!//
          &[!"DcmDspDataReadFnc"!],
        [!ELSE!][!//
          NULL_PTR,
        [!ENDIF!][!//
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_ASYNCH_CLIENT_SERVER') and ($ReadAccess = 'TRUE')"!][!//
        &Rte_Call_DataServices_[!"node:name(.)"!]_ReadData,
      [!ELSE!][!//
        NULL_PTR,
      [!ENDIF!][!//
      [!/*---WriteFixedData ------*/!][!//
      [!IF "node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true'"!][!//
        [!IF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
          [!IF "node:exists(DcmDspDataWriteFnc)"!][!//
            &[!"DcmDspDataWriteFnc"!],
          [!ELSE!][!//
            NULL_PTR,
        [!ENDIF!][!//
        [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_CLIENT_SERVER') and ($WriteAccess = 'TRUE')"!]
          &Rte_Call_DataServices_[!"node:name(.)"!]_WriteData,
        [!ELSE!][!//
          NULL_PTR,
        [!ENDIF!][!//
      [!ELSE!][!//
        NULL_PTR,
      [!ENDIF!][!//
      [!/*--- WriteVariableData------*/!][!//
      [!IF "node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'false'"!][!//
        [!IF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
          [!IF "node:exists(DcmDspDataWriteFnc)"!][!//
            &[!"DcmDspDataWriteFnc"!],
          [!ELSE!][!//
            NULL_PTR,
          [!ENDIF!][!//
        [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_CLIENT_SERVER') and ($WriteAccess = 'TRUE')"!]
          &Rte_Call_DataServices_[!"node:name(.)"!]_WriteData,
        [!ELSE!]
          NULL_PTR,
        [!ENDIF!][!//
      [!ELSE!][!//
        NULL_PTR,
      [!ENDIF!][!//
      [!/*---CallWriteFixedLength, CallWriteVarLength , ResultWriteData------*/!][!//
      [!IF "(DcmDspDataUsePort = 'USE_DATA_ASYNCH_CLIENT_SERVER')  and ($WriteAccess = 'TRUE')"!][!//
        [!IF "node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true'"!][!//
          &Rte_Call_DataServices_[!"node:name(.)"!]_WriteData,
          NULL_PTR,
        [!ELSE!][!//
          NULL_PTR,
          &Rte_Call_DataServices_[!"node:name(.)"!]_WriteData,
        [!ENDIF!][!//
        [!/*--- Rte_Result_DataServices_[!"node:name(.)"!]_WriteData, -----*/!]
        NULL_PTR,
      [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!]
        [!IF "node:exists(DcmDspDataWriteFnc)"!][!//
          [!IF "node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true'"!][!//
            &[!"DcmDspDataWriteFnc"!],
            NULL_PTR,
          [!ELSE!][!//
            NULL_PTR,
            &[!"DcmDspDataWriteFnc"!],
          [!ENDIF!][!//
        [!ELSE!][!//
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!][!//
        [!/*--- Rte_Result_DataServices_[!"node:name(.)"!]_WriteData, -----*/!]
        NULL_PTR,
      [!ELSE!][!//
        NULL_PTR,
        NULL_PTR,
        NULL_PTR,
      [!ENDIF!][!//
      [!/*---FreezeCurrentState------*/!][!//
      [!IF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "(node:exists(as:ref(./DcmDspDidDataRef)/DcmDspDataFreezeCurrentStateFnc)) and (as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidFreezeCurrentState = 'true')"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            &[!"./DcmDspDataFreezeCurrentStateFnc"!],
            NULL_PTR,
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
      [!VAR "flag" = "'FALSE'"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
         [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "(node:exists(as:ref(./DcmDspDidDataRef)/DcmDspDataFreezeCurrentStateFnc)) and (as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidFreezeCurrentState = 'true')"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            NULL_PTR,
            &[!"./DcmDspDataFreezeCurrentStateFnc"!],
          [!ELSE!]
             NULL_PTR,
             NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_CLIENT_SERVER') and ($ControlAccess = 'TRUE')"!]
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidFreezeCurrentState = 'true'"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            &Rte_Call_DataServices_[!"$dataname"!]_FreezeCurrentState,
            NULL_PTR,
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_ASYNCH_CLIENT_SERVER') and ($ControlAccess = 'TRUE')"!]
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
         [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidFreezeCurrentState = 'true'"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            NULL_PTR,
            &Rte_Call_DataServices_[!"$dataname"!]_FreezeCurrentState,
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSE!][!//
          NULL_PTR,
          NULL_PTR,
      [!ENDIF!][!//
      [!/*---ShortTermAdjustment------*/!][!//
      [!IF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "(node:exists(as:ref(./DcmDspDidDataRef)/DcmDspDataShortTermAdjustmentFnc)) and (as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidShortTermAdjustment = 'true')"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            [!IF "(node:ref(./DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true') = 'TRUE'"!]
              &[!"./DcmDspDataShortTermAdjustmentFnc"!],
              NULL_PTR,
            [!ELSE!]
              NULL_PTR,
              &[!"./DcmDspDataShortTermAdjustmentFnc"!],
            [!ENDIF!]
              NULL_PTR,
              NULL_PTR,
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "(node:exists(as:ref(./DcmDspDidDataRef)/DcmDspDataShortTermAdjustmentFnc)) and (as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidShortTermAdjustment = 'true')"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            NULL_PTR,
            NULL_PTR,
              [!IF "(node:ref(./DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true') = 'TRUE'"!]
              &[!"./DcmDspDataShortTermAdjustmentFnc"!],
              NULL_PTR,
            [!ELSE!]
              NULL_PTR,
              &[!"./DcmDspDataShortTermAdjustmentFnc"!],
            [!ENDIF!]
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_CLIENT_SERVER') and ($ControlAccess = 'TRUE')"!]
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
         [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidShortTermAdjustment = 'true'"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            [!IF "(node:ref(./DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true') = 'TRUE'"!]
              &Rte_Call_DataServices_[!"$dataname"!]_ShortTermAdjustment,
              NULL_PTR,
            [!ELSE!]
              NULL_PTR,
              &Rte_Call_DataServices_[!"$dataname"!]_ShortTermAdjustment,
            [!ENDIF!]
            NULL_PTR,
            NULL_PTR,
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_ASYNCH_CLIENT_SERVER') and ($ControlAccess = 'TRUE')"!]
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidShortTermAdjustment = 'true'"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            NULL_PTR,
            NULL_PTR,
            [!IF "(node:ref(./DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true') = 'TRUE'"!]
              &Rte_Call_DataServices_[!"$dataname"!]_ShortTermAdjustment,
              NULL_PTR,
            [!ELSE!]
               NULL_PTR,
               &Rte_Call_DataServices_[!"$dataname"!]_ShortTermAdjustment,
            [!ENDIF!]
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!][!//
          NULL_PTR,
          NULL_PTR,
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!][!//
      [!ELSE!][!//
        NULL_PTR,
        NULL_PTR,
        NULL_PTR,
        NULL_PTR,
      [!ENDIF!][!//
      [!/*---ResetToDefault------*/!][!//
      [!IF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "(node:exists(as:ref(./DcmDspDidDataRef)/DcmDspDataResetToDefaultFnc)) and (as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidResetToDefault = 'true')"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            &[!"./DcmDspDataResetToDefaultFnc"!],
            NULL_PTR,
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
              [!IF "(node:exists(as:ref(./DcmDspDidDataRef)/DcmDspDataResetToDefaultFnc)) and (as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidResetToDefault = 'true')"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            NULL_PTR,
            &[!"./DcmDspDataResetToDefaultFnc"!],
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_CLIENT_SERVER') and ($ControlAccess = 'TRUE')"!]
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidResetToDefault = 'true'"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            &Rte_Call_DataServices_[!"$dataname"!]_ResetToDefault,
            NULL_PTR,
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_ASYNCH_CLIENT_SERVER') and ($ControlAccess = 'TRUE')"!]
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!VAR "isPresent" = "'FALSE'"!][!//
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "as:ref(../../DcmDspDidInfoRef)/*/DcmDspDidControl/DcmDspDidResetToDefault = 'true'"!]
              [!VAR "isPresent"="'TRUE'"!]
            [!ENDIF!]
          [!ENDSELECT!][!//
          [!IF "$isPresent = 'TRUE'"!]
            NULL_PTR,
            &Rte_Call_DataServices_[!"$dataname"!]_ResetToDefault,
          [!ELSE!]
            NULL_PTR,
            NULL_PTR,
          [!ENDIF!]
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSE!][!//
        NULL_PTR,
        NULL_PTR,
      [!ENDIF!][!//
      [!/*---ReturnControlToEcu------*/!][!//
      [!IF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
      [!VAR "dataArray" = "':'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "not( contains( $dataArray, concat( ':', $dataname, ':')))"!]
              [!IF "(node:exists(as:ref(./DcmDspDidDataRef)/DcmDspDataReturnControlToEcuFnc))"!]
                &[!"as:ref(./DcmDspDidDataRef)/DcmDspDataReturnControlToEcuFnc"!],
                NULL_PTR,
              [!ELSE!]
                NULL_PTR,
                NULL_PTR,
              [!ENDIF!]
              [!VAR "dataArray" = "concat( $dataArray, $dataname, ':')"!][!//
            [!ENDIF!]
          [!ENDSELECT!][!//
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!VAR "dataArray" = "':'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "not( contains( $dataArray, concat( ':', $dataname, ':')))"!]
              [!IF "(node:exists(as:ref(./DcmDspDidDataRef)/DcmDspDataReturnControlToEcuFnc))"!]
                NULL_PTR,
                &[!"as:ref(./DcmDspDidDataRef)/DcmDspDataReturnControlToEcuFnc"!],
              [!ELSE!]
                NULL_PTR,
                NULL_PTR,
              [!ENDIF!]
              [!VAR "dataArray" = "concat( $dataArray, $dataname, ':')"!][!//
            [!ENDIF!]
          [!ENDSELECT!][!//
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_SYNCH_CLIENT_SERVER') and ($ControlAccess = 'TRUE')"!]
      [!VAR "dataArray" = "':'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "not( contains( $dataArray, concat( ':', $dataname, ':')))"!]
              &Rte_Call_DataServices_[!"$dataname"!]_ReturnControlToECU,
              NULL_PTR,
              [!VAR "dataArray" = "concat( $dataArray, $dataname, ':')"!][!//
            [!ENDIF!]
          [!ENDSELECT!][!//
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSEIF "(DcmDspDataUsePort = 'USE_DATA_ASYNCH_CLIENT_SERVER') and ($ControlAccess = 'TRUE')"!]
      [!VAR "dataArray" = "':'"!][!//
      [!VAR "dataname" = "node:name(.)"!][!//
        [!IF "count(../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]) > 0"!]
          [!SELECT "../../DcmDspDid/*/DcmDspDidSignal/*[node:name(as:ref(./DcmDspDidDataRef)) = $dataname]"!][!//
            [!IF "not( contains( $dataArray, concat( ':', $dataname, ':')))"!]
              NULL_PTR,
              &Rte_Call_DataServices_[!"$dataname"!]_ReturnControlToECU,
              [!VAR "dataArray" = "concat( $dataArray, $dataname, ':')"!][!//
            [!ENDIF!]
          [!ENDSELECT!][!//
        [!ELSE!]
          NULL_PTR,
          NULL_PTR,
        [!ENDIF!]
      [!ELSE!][!//
        NULL_PTR,
        NULL_PTR,
      [!ENDIF!][!//
      [!/*---DataType------*/!][!//
      [!IF "node:exists(DcmDspDataType)"!][!//
        DCM_TYPE_[!"DcmDspDataType"!],
      /* !LINKSTO Dcm.EB.Config.DcmDspDataType.Default,1 */
      [!ELSE!]
        DCM_TYPE_UINT8,
      [!ENDIF!]
      [!/*---AccessInterface------*/!][!//
      [!IF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!]
        DCM_USE_DATA_ASYNCH_FUNC,
      [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!]
        DCM_USE_DATA_SYNCH_FUNC,
      [!ELSE!]
        DCM_[!"DcmDspDataUsePort"!],
      [!ENDIF!]

      [!/*---DataLength------*/!][!//
      [!"DcmDspDataSize"!],
      [!/*---NvRamBlockId------*/!][!//
      [!IF "node:exists(DcmDspDataBlockIdRef)"!][!//
        [!"node:ref(DcmDspDataBlockIdRef)/NvMNvramBlockIdentifier"!]U,
      [!ELSE!][!//
        0U,
      [!ENDIF!][!//
      [!/*---FixedLenth------*/!][!//
      [!IF "node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true'"!]
        TRUE
      [!ELSE!][!//
        FALSE
      [!ENDIF!]
    [!ENDINDENT!]
  [!WS "2"!]},
  [!ENDLOOP!]
};
[!ENDIF!][!//
[!//
[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
[!IF "num:i(count(DcmConfigSet/*[1]/DcmDsl/DcmDslServiceRequestManufacturerNotification/*)) != 0"!]
  /** \brief RTE function pointer table for Manufacturer Notification */
  CONST(Dcm_RteRequestNotificationType, DCM_CONST)
  [!WS "2"!]Dcm_RteManufacturerNotificationTable[DCM_RTE_TABLE_REQUEST_MANUFACTURER_SIZE] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslServiceRequestManufacturerNotification/*"!]
      [!WS "2"!]{
        [!WS "4"!][!"concat('&Rte_Call_ServiceRequestNotification_', name(.), '_Indication')"!],
        [!WS "4"!][!"concat('&Rte_Call_ServiceRequestNotification_', name(.), '_Confirmation')"!]
      [!WS "2"!]},
    [!ENDLOOP!]
  };
[!ENDIF!]
[!//
[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
[!IF "num:i(count(DcmConfigSet/*[1]/DcmDsl/DcmDslServiceRequestSupplierNotification/*)) != 0"!]
  /** \brief RTE function pointer table for Supplier Notification */
  CONST(Dcm_RteRequestNotificationType, DCM_CONST)
  [!WS "2"!]Dcm_RteSupplierNotificationTable[DCM_RTE_TABLE_REQUEST_SUPPLIER_SIZE] =
  {
    [!LOOP "DcmConfigSet/*[1]/DcmDsl/DcmDslServiceRequestSupplierNotification/*"!]
      {[!WS "2"!]
        [!WS "4"!][!"concat('&Rte_Call_ServiceRequestNotification_', name(.), '_Indication')"!],
        [!WS "4"!][!"concat('&Rte_Call_ServiceRequestNotification_', name(.), '_Confirmation')"!]
      [!WS "2"!]},
    [!ENDLOOP!]
  };
[!ENDIF!]
[!//
[!/*------------------------------------------------------------------*/!][!//
[!/*------------------------------------------------------------------*/!][!//
[!IF "count(./DcmConfigSet/*/DcmDsp/DcmDspRoe/DcmDspRoePreconfEvent/*) > 0"!]
  [!IF "(count(DcmConfigSet/*[1]/DcmDsl/DcmDslProtocol/DcmDslProtocolRow/*/DcmDslConnection/*[. = 'DcmDslResponseOnEvent']) > 0 and count(DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*[DcmDsdSidTabServiceId = '134']) > 0)"!]
    CONST(Dcm_RoeEventPreConfigType,DCM_CONST) Dcm_RoeEventPreConfiguration[DCM_NUM_PRECONF_EVENT] =
    {
      [!LOOP "./DcmConfigSet/*/DcmDsp/DcmDspRoe/DcmDspRoePreconfEvent/*"!]
        {
          [!VAR "DidFound" = "'FALSE'"!]
          [!VAR "did" = "as:ref(DcmDspRoePreconfEventDetails/DcmDspRoePreconfOnChangeOfDataIdentifierDIDRef)/DcmDspDidIdentifier"!]
          [!INDENT "4"!]
            [!/*---RoeEventWindowTime------*/!][!//
            [!"DcmDspRoePreconfEventWindowTime"!]U,
            [!/*---RoeEventInitialStatus------*/!][!//
            [!"DcmDspRoeInitialEventStatus"!],
            [!LOOP "as:ref(DcmDspRoePreconfServiceToRespondToRef)/DcmDspRoeReadDataByIdentifier/DcmDspRoeReadDataByIdentifierDIDRef/*"!]
              [!IF "$did = node:ref(.)/DcmDspDidIdentifier"!]
                [!/*---DataIdentifier------*/!][!//
                [!"num:inttohex($did)"!]U,
                [!VAR "DidFound" = "'TRUE'"!]
              [!BREAK!]
              [!ENDIF!]
            [!ENDLOOP!]
            [!IF "$DidFound = 'FALSE'"!]
              [!ERROR!]
                Matching ServiceToRespondTo for DID [!"$did"!] not found.
              [!ENDERROR!]
            [!ENDIF!]
            [!IF "../*/DcmDspRoeInitialEventStatus = 'DCM_ROE_ACTIVE'"!]
              [!"num:i(as:ref(../../DcmDspRoePreconfRxChannelRef)/DcmDslProtocolRxPduId)"!]U,  
            [!ELSE!]
              [!"num:i(0)"!]U,
            [!ENDIF!]
          [!ENDINDENT!]
        },
      [!ENDLOOP!]
    };
  [!ENDIF!]
[!ENDIF!]

[!VAR "BswModeDecGpList" = "'#'"!]
[!VAR "SwcModeDecGpList" = "'#'"!]
[!LOOP "DcmConfigSet/*/DcmProcessingConditions/DcmModeCondition/*"!]
/** \brief Mode condition function for [!"name(.)"!] */
STATIC FUNC(boolean, DCM_CODE) ModeCondition_[!"name(.)"!]_Result(void)
{
  [!INDENT "2"!]
    boolean result = FALSE;
    [!IF "DcmConditionType = 'DCM_EQUALS' "!]
      [!VAR "condition" = "'=='"!]
      [!VAR "check" = "'matches'"!]
    [!ELSE!]
      [!VAR "condition" = "'!='"!]
      [!VAR "check" = "'does not match'"!]
    [!ENDIF!]
    [!IF "node:exists(DcmBswModeRef)"!]
      [!VAR "ModeDecGp" = "text:split(./DcmBswModeRef/TARGET, '/')[last()-1]"!]
      /* Bsw mode (provided by Dcm or any other mode manager) */
      Rte_ModeType_[!"$ModeDecGp"!] mode = [!//
        RTE_MODE_[!"$ModeDecGp"!]_[!"text:split(./DcmBswModeRef/TARGET, '/')[last()]"!];

      [!IF "not(contains($BswModeDecGpList, concat('#', $ModeDecGp, '#')))"!]
        /* check if mode returned by the Bsw module [!"$check"!] with that configured in DcmBswModeRef */
        if(SchM_Mode_Dcm_DcmModeRuleRequiredMode_[!"@index"!]_[!"$ModeDecGp"!]() [!"$condition"!]
        [!VAR "BswModeDecGpList" = "concat($BswModeDecGpList, $ModeDecGp, '#')"!]
      [!ELSE!]
        [!/* loop to find the index of mode condition having the same Bsw mode declaration */!]
        [!LOOP "../*"!]
          [!IF "$ModeDecGp = text:split(./DcmBswModeRef/TARGET, '/')[last()-1]"!]
            [!VAR "idx" = "@index"!]
            [!BREAK!]
          [!ENDIF!]
        [!ENDLOOP!]
        /* check if mode returned by the Bsw module [!"$check"!] with that configured in DcmBswModeRef */
        if(SchM_Mode_Dcm_DcmModeRuleRequiredMode_[!"$idx"!]_[!"$ModeDecGp"!]() [!"$condition"!]
      [!ENDIF!]
      [!WS "4"!]mode)
    [!ELSEIF "node:exists(DcmSwcModeRef)"!]
      [!VAR "SwcModeDecGp" = "text:split(./DcmSwcModeRef/TARGET, '/')[last()-1]"!]
      /* Swc mode (provided by Swc which is the mode manager) */
      Rte_ModeType_[!"$SwcModeDecGp"!] mode = [!//
        RTE_MODE_[!"$SwcModeDecGp"!]_[!"text:split(./DcmSwcModeRef/TARGET, '/')[last()]"!];
      [!IF "not(contains($SwcModeDecGpList, concat('#', $SwcModeDecGp, '#')))"!]
        /* check if mode returned by the Swc [!"$check"!] with that configured in DcmSwcModeRef */
        if(Rte_Mode_Dcm_R_SwcMode_[!"@index"!]_[!"$SwcModeDecGp"!]_DcmModeRuleRequiredModeGp_[!"$SwcModeDecGp"!]() [!"$condition"!]
        [!VAR "SwcModeDecGpList" = "concat($SwcModeDecGpList, $SwcModeDecGp, '#')"!]
      [!ELSE!]
        [!/* loop to find the index of mode condition having the same Swc mode declaration */!]
        [!LOOP "../*"!]
          [!IF "$SwcModeDecGp = text:split(./DcmSwcModeRef/TARGET, '/')[last()-1]"!]
            [!VAR "Swcidx" = "@index"!]
            [!BREAK!]
          [!ENDIF!]
        [!ENDLOOP!]
        /* check if mode returned by the Swc [!"$check"!] with that configured in DcmSwcModeRef */
        if(Rte_Mode_Dcm_R_SwcMode_[!"$Swcidx"!]_[!"$SwcModeDecGp"!]_DcmModeRuleRequiredModeGp_[!"$SwcModeDecGp"!]() [!"$condition"!]
      [!ENDIF!]
      [!WS "4"!]mode)
    [!ENDIF!]
      {
      [!WS "2"!]result = TRUE;
      }

    return result;
  [!ENDINDENT!]
}
[!ENDLOOP!]

#define DCM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external data]==============================================================*/

/*==================[external function definitions]==============================================*/
#define DCM_START_SEC_CODE
#include <MemMap.h>

[!IF "(DcmConfigSet/*[1]/DcmGeneral/DcmModeDeclarationSupport = 'false')"!]
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspSession/*/DcmDspSessionRow/*"!][!//
  [!VAR "StrName"="''"!]
  [!VAR "StrTemp"="substring-after(name(.),'DCM_')"!]
  [!IF "$StrTemp = ''"!][!VAR "StrTemp"="name(.)"!][!ENDIF!]
  [!LOOP "text:split($StrTemp, '_')"!]
    [!VAR "StrName"="concat($StrName, concat(substring(., 1,1), text:tolower(substring(., 2))))"!]
  [!ENDLOOP!]
[!//
  [!IF "not(contains($StrName,'AllSessionLevel'))"!]
    FUNC(Std_ReturnType, DCM_CODE) Dcm_[!"$StrName"!]ModeEntry(void)
    {
    [!INDENT "2"!]
      /* get information */
      CONSTP2VAR(Dcm_HsmInfoType, AUTOMATIC, DCM_VAR) curHsmInfo = &DCM_HSM_INFO(Dcm_CurProtocolId);
      if (curHsmInfo->msgContext.idContext == DCM_SID_SESSION_CONTROL)
      {
        [!WS "2"!]curHsmInfo->confirmedDiagSession = [!"./@name"!];
      }
      return E_OK;
    [!ENDINDENT!]
    }
  [!ENDIF!]
[!ENDLOOP!][!//
[!ENDIF!]

[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!]
  [!IF "DcmDspRoutineUsed = 'true'"!]
/* !LINKSTO Dcm.EB.GenerateRoutineControlStart,1 */
    [!CALL "ROUTINECONTROL_GENERATE",
       "operation"="'startRoutine'"!]
    [!IF "DcmDspStopRoutineSupported = 'true'"!]
      /* !LINKSTO Dcm.EB.Config.DcmDspStopRoutineSupported.DcmDspRoutineUsePort,1 */
      [!CALL "ROUTINECONTROL_GENERATE",
         "operation"="'stopRoutine'"!]
    [!ENDIF!]
    [!IF "DcmDspRequestResultsRoutineSupported = 'true'"!]
      [!CALL "ROUTINECONTROL_GENERATE",
         "operation"="'requestRoutineResults'"!]
    [!ENDIF!]
  [!ENDIF!]
[!ENDLOOP!]

[!/* Verification that Routine Control signals are not overlapping */!]
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutineInfo/*"!]
  [!LOOP "node:order(DcmDspRoutineRequestResOut/DcmDspRoutineRequestResOutSignal/* , 'DcmDspRoutineSignalPos')"!]
    [!VAR "POSN" = "DcmDspRoutineSignalPos"!]
    [!VAR "LENGTH" = "DcmDspRoutineSignalLength"!]
    [!VAR "nodeposn" = "node:pos(.) + 1"!]
    [!VAR "Name" = "node:name(.)"!]
    [!LOOP "../*[position() != $nodeposn]"!]
      [!IF "(($POSN >= DcmDspRoutineSignalPos) and ($POSN <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))
        or ((($POSN + $LENGTH -1) >= DcmDspRoutineSignalPos) and (($POSN + $LENGTH -1) <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))"!]
        [!ERROR!]
          signal [!"node:name(.)"!] overlaps with signal [!"$Name"!]
          Routine Information : '[!"node:name(../../..)"!]'
          Configuration       : '[!"node:name(../..)"!]'
         [!ENDERROR!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDLOOP!]

  [!LOOP "node:order(DcmDspRoutineRequestResIn/DcmDspRoutineRequestResInSignal/* , 'DcmDspRoutineSignalPos')"!]
    [!VAR "POSN" = "DcmDspRoutineSignalPos"!]
    [!VAR "LENGTH" = "DcmDspRoutineSignalLength"!]
    [!VAR "nodeposn" = "node:pos(.) + 1"!]
    [!VAR "Name" = "node:name(.)"!]
    [!LOOP "../*[position() != $nodeposn]"!]
      [!IF "(($POSN >= DcmDspRoutineSignalPos) and ($POSN <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))
        or ((($POSN + $LENGTH -1) >= DcmDspRoutineSignalPos) and (($POSN + $LENGTH -1) <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))"!]
        [!ERROR!]
          signal [!"node:name(.)"!] overlaps with signal [!"$Name"!]
          Routine Information : '[!"node:name(../../..)"!]'
          Configuration       : '[!"node:name(../..)"!]'
         [!ENDERROR!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDLOOP!]

  [!LOOP "node:order(DcmDspRoutineStopIn/DcmDspRoutineStopInSignal/* , 'DcmDspRoutineSignalPos')"!]
    [!VAR "POSN" = "DcmDspRoutineSignalPos"!]
    [!VAR "LENGTH" = "DcmDspRoutineSignalLength"!]
    [!VAR "nodeposn" = "node:pos(.) + 1"!]
    [!VAR "Name" = "node:name(.)"!]
    [!LOOP "../*[position() != $nodeposn]"!]
      [!IF "(($POSN >= DcmDspRoutineSignalPos) and ($POSN <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))
      or ((($POSN + $LENGTH -1) >= DcmDspRoutineSignalPos) and (($POSN + $LENGTH -1) <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))"!]
        [!ERROR!]
          signal [!"node:name(.)"!] overlaps with signal [!"$Name"!]
          Routine Information : '[!"node:name(../../..)"!]'
          Configuration       : '[!"node:name(../..)"!]'
        [!ENDERROR!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDLOOP!]

  [!LOOP "node:order(DcmDspRoutineStopOut/DcmDspRoutineStopOutSignal/* , 'DcmDspRoutineSignalPos')"!]
    [!VAR "POSN" = "DcmDspRoutineSignalPos"!]
    [!VAR "LENGTH" = "DcmDspRoutineSignalLength"!]
    [!VAR "nodeposn" = "node:pos(.) + 1"!]
    [!VAR "Name" = "node:name(.)"!]
    [!LOOP "../*[position() != $nodeposn]"!]
      [!IF "(($POSN >= DcmDspRoutineSignalPos) and ($POSN <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))
      or ((($POSN + $LENGTH -1) >= DcmDspRoutineSignalPos) and (($POSN + $LENGTH -1) <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))"!]
        [!ERROR!]
          signal [!"node:name(.)"!] overlaps with signal [!"$Name"!]
          Routine Information : '[!"node:name(../../..)"!]'
          Configuration       : '[!"node:name(../..)"!]'
        [!ENDERROR!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDLOOP!]

  [!LOOP "node:order(DcmDspStartRoutineIn/DcmDspStartRoutineInSignal/* , 'DcmDspRoutineSignalPos')"!]
    [!VAR "POSN" = "DcmDspRoutineSignalPos"!]
    [!VAR "LENGTH" = "DcmDspRoutineSignalLength"!]
    [!VAR "nodeposn" = "node:pos(.) + 1"!]
    [!VAR "Name" = "node:name(.)"!]
    [!LOOP "../*[position() != $nodeposn]"!]
      [!IF "(($POSN >= DcmDspRoutineSignalPos) and ($POSN <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))
      or ((($POSN + $LENGTH -1) >= DcmDspRoutineSignalPos) and (($POSN + $LENGTH -1) <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))"!]
        [!ERROR!]
          signal [!"node:name(.)"!] overlaps with signal [!"$Name"!]
          Routine Information : '[!"node:name(../../..)"!]'
          Configuration       : '[!"node:name(../..)"!]'
        [!ENDERROR!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDLOOP!]

  [!LOOP "node:order(DcmDspStartRoutineOut/DcmDspStartRoutineOutSignal/* , 'DcmDspRoutineSignalPos')"!]
    [!VAR "POSN" = "DcmDspRoutineSignalPos"!]
    [!VAR "LENGTH" = "DcmDspRoutineSignalLength"!]
    [!VAR "nodeposn" = "node:pos(.) + 1"!]
    [!VAR "Name" = "node:name(.)"!]
    [!LOOP "../*[position() != $nodeposn]"!]
      [!IF "(($POSN >= DcmDspRoutineSignalPos) and ($POSN <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))
      or ((($POSN + $LENGTH -1) >= DcmDspRoutineSignalPos) and (($POSN + $LENGTH -1) <= (DcmDspRoutineSignalPos + DcmDspRoutineSignalLength -1)))"!]
        [!ERROR!]
          signal [!"node:name(.)"!] overlaps with signal [!"$Name"!]
          Routine Information : '[!"node:name(../../..)"!]'
          Configuration       : '[!"node:name(../..)"!]'
        [!ENDERROR!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDLOOP!]
[!ENDLOOP!]

[!ENDINDENT!]
#define DCM_STOP_SEC_CODE
#include <MemMap.h>
/*==================[internal function definitions]==============================================*/

[!LOOP "DcmConfigSet/*/DcmProcessingConditions/DcmModeRule/*"!]
/** \brief Mode rule function for [!"name(.)"!] */
FUNC(boolean, DCM_CODE) ModeRule_[!"name(.)"!]_Result(P2VAR(uint8, AUTOMATIC, DCM_VAR) Nrc)
{
  [!INDENT "2"!]
    uint8 calcNrc = 0U;
    [!IF "DcmLogicalOperator = 'DCM_AND'"!]
      boolean result = TRUE;
      [!VAR "operator" = "'&&'"!]
    [!ENDIF!]
    [!IF "DcmLogicalOperator = 'DCM_OR'"!]
      boolean result = FALSE;
      [!VAR "operator" = "'||'"!]
    [!ENDIF!]
    /* Get result of all referenced MRs/MCs */
    /* Deviation MISRA-1  <START> */
    result =
    [!LOOP "DcmArgumentRef/*"!]
      [!IF "'DcmModeCondition' = name(as:ref(.)/..)"!]
        (ModeCondition_[!"name(as:ref(.))"!]_Result()) [!"$operator"!]
      [!ELSE!][!//
        (ModeRule_[!"name(as:ref(.))"!]_Result(&calcNrc)) [!"$operator"!]
      [!ENDIF!][!//
    [!ENDLOOP!][!//
       result;
  /* Deviation MISRA-1  <STOP> */

  if (result == FALSE)
  {
  [!INDENT "4"!]
    /* result is a failure */
    if( calcNrc != 0U )
    {
      [!WS "2"!]/* referenced MR which failed has an NRC */
      [!WS "2"!]*Nrc = calcNrc;
    }
    else
    {
    [!IF "node:exists(DcmModeRuleNrcValue)"!]
      [!WS "2"!]/* referenced MR which failed does NOT have an NRC or an MC failed so use
      [!WS "3"!]   configured NRC for the master MR */
      [!WS "2"!]*Nrc = [!"DcmModeRuleNrcValue"!]U;
    [!ELSE!]
      [!WS "2"!]/* referenced MR which failed does NOT have an NRC or an MC failed and also
       [!WS "3"!]  no configured NRC for the master MR so use DCM_E_CONDITIONSNOTCORRECT as the NRC */
      [!WS "2"!]*Nrc = 0x22U;
    [!ENDIF!]
    }
    [!ENDINDENT!]
  }
  else
  {
  [!WS "2"!]/* result is TRUE so no NRC to be sent */
  [!WS "2"!]*Nrc = 0U;
  }

  return result;
  [!ENDINDENT!]
}
[!ENDLOOP!]

/*==================[end of file]================================================================*/
