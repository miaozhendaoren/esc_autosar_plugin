/**
 * \file
 *
 * \brief AUTOSAR Dcm
 *
 * This file contains the implementation of the AUTOSAR
 * module Dcm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DCM_API_CFG_H)
#define DCM_API_CFG_H

/*==================[includes]===================================================================*/

[!AUTOSPACING!]
#include <Dcm_Types.h>
/*==================[macros]=====================================================================*/

[!/*--------------- Macros for user-implemented service handler IDs -------------*/!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*"!][!//
[!IF "node:exists(DcmDsdSidTabFnc)"!][!//
#define DCM_USERSVC_[!"text:toupper(num:inttohex(DcmDsdSidTabServiceId, 2))"!]
[!ENDIF!][!//
[!ENDLOOP!][!//
[!//

[!/*--------------- Macros for user-implemented subservice handler IDs -------------*/!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSubService/*"!][!//
[!IF "node:exists(DcmDsdSubServiceFnc)"!][!//
#define DCM_USERSSVC_[!"text:toupper(num:inttohex(../../DcmDsdSidTabServiceId, 2))"!]_[!"text:toupper(num:inttohex(DcmDsdSubServiceId, 2))"!]
[!ENDIF!][!//
[!ENDLOOP!][!//
[!//
/*==================[type definitions]===========================================================*/

/*==================[external function declarations]=============================================*/
#define DCM_START_SEC_CALLOUT_CODE
#include <MemMap.h>

[!INDENT "0"!][!//
[!VAR "userfnc" = "':'"!][!//
[!/*---------------Extern declarations of user functions for the services 0x27 -------------*/!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspSecurity/DcmDspSecurityRow/*"!][!//
[!//
  [!IF "node:exists(DcmDspSecurityGetSeedFnc)"!][!//
  [!/*--Extern declarations of user functions for 0x27 if DcmDspSecurityADRSize is present ---*/!][!//
    [!IF "DcmDspSecurityADRSize > 0"!][!//
      [!IF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FUNC' or DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FNC'"!][!//
        [!IF "not( contains( $userfnc, concat( ':', ./DcmDspSecurityGetSeedFnc, ':')))"!]
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspSecurityGetSeedFnc"!](
          [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_VAR) SecurityAccessDataRecord,
          [!WS "4"!]Dcm_OpStatusType OpStatus,
          [!WS "4"!]P2VAR(uint8, AUTOMATIC, DCM_VAR) Seed,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
          [!VAR "userfnc" = "concat( $userfnc, ./DcmDspSecurityGetSeedFnc, ':')"!][!//
        [!ENDIF!][!//
      [!ELSE!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspSecurityGetSeedFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspSecurityGetSeedFnc"!](
        [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_VAR) SecurityAccessDataRecord,
        [!WS "4"!]P2VAR(uint8, AUTOMATIC, DCM_VAR) Seed,
        [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspSecurityGetSeedFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ENDIF!][!//
    [!/* Extern declarations of user functions for 0x27 if DcmDspSecurityADRSize is not present */!]
    [!ELSE!][!//
      [!IF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FUNC' or DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FNC'"!][!//
        [!IF "not( contains( $userfnc, concat( ':', ./DcmDspSecurityGetSeedFnc, ':')))"!]
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspSecurityGetSeedFnc"!](
          [!WS "4"!]Dcm_OpStatusType OpStatus,
          [!WS "4"!]P2VAR(uint8, AUTOMATIC, DCM_VAR) Seed,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
          [!VAR "userfnc" = "concat( $userfnc, ./DcmDspSecurityGetSeedFnc, ':')"!][!//
        [!ENDIF!]
      [!ELSE!][!//
        [!IF "not( contains( $userfnc, concat( ':', ./DcmDspSecurityGetSeedFnc, ':')))"!]
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspSecurityGetSeedFnc"!](
          [!WS "4"!]P2VAR(uint8, AUTOMATIC, DCM_VAR) Seed,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
          [!VAR "userfnc" = "concat( $userfnc, ./DcmDspSecurityGetSeedFnc, ':')"!][!//
        [!ENDIF!][!//
      [!ENDIF!][!//
    [!ENDIF!]
  [!ENDIF!]

  [!IF "node:exists(DcmDspSecurityCompareKeyFnc)"!][!//
    [!IF "DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FUNC' or DcmDspSecurityUsePort = 'USE_SECURITY_ASYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspSecurityCompareKeyFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspSecurityCompareKeyFnc"!](
        [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_VAR) Key, Dcm_OpStatusType OpStatus);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspSecurityCompareKeyFnc, ':')"!][!//
      [!ENDIF!]
    [!ELSE!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspSecurityCompareKeyFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspSecurityCompareKeyFnc"!](
        [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_VAR) Key);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspSecurityCompareKeyFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//

[!VAR "userfnc" = "':'"!][!//
[!/*-----------Extern declarations of user functions for the services 0x22 and 0x2E --------*/!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspData/*"!][!//
  [!IF "node:exists(DcmDspDataConditionCheckReadFnc)"!][!//
    [!IF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataConditionCheckReadFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataConditionCheckReadFnc"!](
        [!WS "4"!]Dcm_OpStatusType OpStatus,
        [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataConditionCheckReadFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ELSE!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataConditionCheckReadFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataConditionCheckReadFnc"!](
        [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataConditionCheckReadFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
  [!IF "node:exists(DcmDspDataReadDataLengthFnc)"!][!//
    [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataReadDataLengthFnc, ':')))"!]
      extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataReadDataLengthFnc"!](
      [!WS "4"!]P2VAR(uint16, AUTOMATIC, DCM_VAR) DataLength);
      [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataReadDataLengthFnc, ':')"!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
  [!IF "node:exists(DcmDspDataReadFnc)"!][!//
    [!IF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataReadFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataReadFnc"!](
        [!WS "4"!]Dcm_OpStatusType OpStatus,
        [!WS "4"!]P2VAR(uint8, AUTOMATIC, DCM_VAR) Data);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataReadFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ELSE!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataReadFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataReadFnc"!](
        [!WS "4"!]P2VAR(uint8, AUTOMATIC, DCM_VAR) Data);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataReadFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
  [!IF "node:exists(DcmDspDataWriteFnc)"!][!//
    [!IF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataWriteFnc, ':')))"!][!//
        [!IF "node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true'"!]
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataWriteFnc"!](
          [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_CONST) Data,
          [!WS "4"!]Dcm_OpStatusType OpStatus,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ELSE!][!//
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataWriteFnc"!](
          [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_CONST) Data,
          [!WS "4"!]uint16 DataLength,
          [!WS "4"!]Dcm_OpStatusType OpStatus,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ENDIF!][!//
          [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataWriteFnc, ':')"!]
      [!ENDIF!][!//
    [!ELSE!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataWriteFnc, ':')))"!]
        [!IF "node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true'"!][!//
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataWriteFnc"!](
          [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_CONST) Data,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ELSE!][!//
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataWriteFnc"!](
          [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_CONST) Data,
          [!WS "4"!]uint16 DataLength,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ENDIF!][!//
          [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataWriteFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//

[!VAR "userfnc" = "':'"!][!//
[!/*-----------Extern declarations of user functions for the services 0x2F --------*/!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspData/*"!][!//

  [!IF "node:exists(DcmDspDataFreezeCurrentStateFnc)"!][!//
    [!IF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataFreezeCurrentStateFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataFreezeCurrentStateFnc"!](
        [!WS "4"!]Dcm_OpStatusType opStatus,
        [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataFreezeCurrentStateFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataFreezeCurrentStateFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataFreezeCurrentStateFnc"!](
        [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataFreezeCurrentStateFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ELSE!]
    [!ENDIF!][!//
  [!ENDIF!][!//

  [!IF "node:exists(DcmDspDataShortTermAdjustmentFnc)"!][!//
    [!IF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataShortTermAdjustmentFnc, ':')))"!]
        [!IF "node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true'"!][!//
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataShortTermAdjustmentFnc"!](
          [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_CONST) Data,
          Dcm_OpStatusType opStatus,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ELSE!]
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataShortTermAdjustmentFnc"!](
          [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_CONST) Data,
          [!WS "4"!]uint16 DataLength,
          [!WS "4"!]Dcm_OpStatusType opStatus,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ENDIF!]
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataShortTermAdjustmentFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataShortTermAdjustmentFnc, ':')))"!]
        [!IF "node:ref(DcmDspDataInfoRef)/DcmDspDataFixedLength = 'true'"!][!//
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataShortTermAdjustmentFnc"!](
          [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_CONST) Data,
          [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ELSE!]
          extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataShortTermAdjustmentFnc"!](
          [!WS "4"!]P2CONST(uint8, AUTOMATIC, DCM_CONST) Data,
          [!WS "4"!]uint16 DataLength,
          P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ENDIF!]
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataShortTermAdjustmentFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ELSE!]
    [!ENDIF!][!//
  [!ENDIF!][!//

  [!IF "node:exists(DcmDspDataResetToDefaultFnc)"!][!//
    [!IF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataResetToDefaultFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataResetToDefaultFnc"!](
        [!WS "4"!]Dcm_OpStatusType opStatus,
        [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataResetToDefaultFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataResetToDefaultFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataResetToDefaultFnc"!](
        [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataResetToDefaultFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ELSE!]
    [!ENDIF!][!//
  [!ENDIF!][!//

  [!IF "node:exists(DcmDspDataReturnControlToEcuFnc)"!][!//
    [!IF "DcmDspDataUsePort = 'USE_DATA_ASYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_ASYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataReturnControlToEcuFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataReturnControlToEcuFnc"!](
        [!WS "4"!]Dcm_OpStatusType opStatus,
        [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataReturnControlToEcuFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ELSEIF "DcmDspDataUsePort = 'USE_DATA_SYNCH_FUNC' or DcmDspDataUsePort = 'USE_DATA_SYNCH_FNC'"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDataReturnControlToEcuFnc, ':')))"!]
        extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDataReturnControlToEcuFnc"!](
        [!WS "4"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDataReturnControlToEcuFnc, ':')"!][!//
      [!ENDIF!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//

[!VAR "userfnc" = "':'"!][!//
[!/*------------Extern declarations of user functions for the services 0x86 -------*/!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspDid/*"!][!//
[!//
  [!IF "node:exists(DcmDspDidExtRoe/DcmDspDidRoeActivateFnc)"!][!//
    [!IF "not( contains( $userfnc, concat( ':', ./DcmDspDidExtRoe/DcmDspDidRoeActivateFnc, ':')))"!][!//
      extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"DcmDspDidExtRoe/DcmDspDidRoeActivateFnc"!](
      [!WS "4"!]uint8 RoeEventId, Dcm_RoeStateType RoeState);
      [!VAR "userfnc" = "concat( $userfnc, ./DcmDspDidExtRoe/DcmDspDidRoeActivateFnc, ':')"!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
[!//
[!ENDLOOP!][!//
[!//
[!LOOP "DcmConfigSet/*[1]/DcmDsp/DcmDspRoutine/*"!]
  [!IF "DcmDspRoutineUsed = 'true'"!]
    [!IF "DcmDspRoutineUsePort = 'false'"!]
    [!/*------------Extern declarations of user functions for the servic0x31 ----------------*/!][!//
      [!VAR "inVarCount"="0"!][!//
      [!VAR "inParamListStart"="''"!][!//
      [!LOOP "node:ref(DcmDspRoutineInfoRef)/DcmDspStartRoutineIn/DcmDspStartRoutineInSignal/*"!][!//
        [!VAR "inParamListStart"="concat($inParamListStart, text:tolower(DcmDspRoutineSignalType),' inParam', num:i($inVarCount), ', ')"!][!//
        [!VAR "inVarCount"="$inVarCount + 1"!][!//
      [!ENDLOOP!][!//
      [!VAR "inParamListStart"="text:replaceAll( $inParamListStart, 'variable_length', 'P2CONST(uint8, AUTOMATIC, DCM_VAR)')"!]
      [!VAR "outVarCount"="0"!][!//
      [!VAR "outParamListStart"="''"!][!//
      [!LOOP "node:ref(DcmDspRoutineInfoRef)/DcmDspStartRoutineOut/DcmDspStartRoutineOutSignal/*"!][!//
        [!VAR "outParamListStart"="concat($outParamListStart, 'P2VAR(', text:tolower(DcmDspRoutineSignalType),', AUTOMATIC, DCM_VAR)', ' outParam', num:i($outVarCount), ', ')"!][!//
        [!VAR "outVarCount"="$outVarCount + 1"!][!//
      [!ENDLOOP!][!//
      [!VAR "outParamListStart"="text:replaceAll( $outParamListStart, 'variable_length', 'uint8 ')"!][!//
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspStartRoutineFnc, ':')))"!] 
      [!IF "DcmDspRoutineFixedLength = 'true'"!]
          extern Std_ReturnType [!"DcmDspStartRoutineFnc"!]([!"$inParamListStart"!]Dcm_OpStatusType OpStatus, [!"$outParamListStart"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ELSE!]
          extern Std_ReturnType [!"DcmDspStartRoutineFnc"!]([!"$inParamListStart"!]Dcm_OpStatusType OpStatus, [!"$outParamListStart"!]P2VAR(uint16, AUTOMATIC, DCM_VAR)currentDataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
        [!ENDIF!][!//
      [!ENDIF!][!//
      [!VAR "userfnc" = "concat( $userfnc, ./DcmDspStartRoutineFnc, ':')"!][!// 
      [!VAR "inVarCount"="0"!][!//
      [!VAR "inParamListStop"="''"!][!//
      [!LOOP "node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineStopIn/DcmDspRoutineStopInSignal/*"!][!//
        [!VAR "inParamListStop"="concat($inParamListStop, text:tolower(DcmDspRoutineSignalType),' inParam', num:i($inVarCount), ', ')"!][!//
        [!VAR "inVarCount"="$inVarCount + 1"!][!//
      [!ENDLOOP!][!//
      [!VAR "inParamListStop"="text:replaceAll( $inParamListStop, 'variable_length', 'P2CONST(uint8, AUTOMATIC, DCM_VAR)')"!][!//
      [!VAR "outVarCount"="0"!][!//
      [!VAR "outParamListStop"="''"!][!//
      [!LOOP "node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineStopOut/DcmDspRoutineStopOutSignal/*"!][!//
        [!VAR "outParamListStop"="concat($outParamListStop, 'P2VAR(', text:tolower(DcmDspRoutineSignalType),', AUTOMATIC, DCM_VAR)', ' outParam', num:i($outVarCount), ', ')"!][!//
        [!VAR "outVarCount"="$outVarCount + 1"!][!//
      [!ENDLOOP!][!//
      [!VAR "outParamListStop"="text:replaceAll( $outParamListStop, 'variable_length', 'uint8 ')"!]
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspStopRoutineFnc, ':')))"!] 
      [!IF "node:exists(DcmDspStopRoutineFnc)"!]
          [!IF "DcmDspRoutineFixedLength = 'true'"!]
            extern Std_ReturnType [!"DcmDspStopRoutineFnc"!]([!"$inParamListStop"!]Dcm_OpStatusType OpStatus, [!"$outParamListStop"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
          [!ELSE!]
            extern Std_ReturnType [!"DcmDspStopRoutineFnc"!]([!"$inParamListStop"!]Dcm_OpStatusType OpStatus, [!"$outParamListStop"!]P2VAR(uint16, AUTOMATIC, DCM_VAR) currentDataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
          [!ENDIF!][!//
        [!ENDIF!][!//
      [!ENDIF!][!//
      [!VAR "userfnc" = "concat( $userfnc, ./DcmDspStopRoutineFnc, ':')"!][!// 
      [!VAR "inVarCount"="0"!][!//
      [!VAR "inParamListRequestRes"="''"!][!//
      [!LOOP "node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineRequestResIn/DcmDspRoutineRequestResInSignal/*"!][!//
        [!VAR "inParamListRequestRes"="concat($inParamListRequestRes, text:tolower(DcmDspRoutineSignalType),' inParam', num:i($inVarCount), ', ')"!][!//
        [!VAR "inVarCount"="$inVarCount + 1"!][!//
      [!ENDLOOP!][!//
      [!VAR "inParamListRequestRes"="text:replaceAll( $inParamListRequestRes, 'variable_length', 'P2CONST(uint8, AUTOMATIC, DCM_VAR)')"!][!//
      [!VAR "outVarCount"="0"!][!//
      [!VAR "outParamListRequestRes"="''"!][!//
      [!LOOP "node:ref(DcmDspRoutineInfoRef)/DcmDspRoutineRequestResOut/DcmDspRoutineRequestResOutSignal/*"!][!//
        [!VAR "outParamListRequestRes"="concat($outParamListRequestRes, 'P2VAR(', text:tolower(DcmDspRoutineSignalType),', AUTOMATIC, DCM_VAR)', ' outParam', num:i($outVarCount), ', ')"!][!//
        [!VAR "outVarCount"="$outVarCount + 1"!][!//
      [!ENDLOOP!][!//
      [!VAR "outParamListRequestRes"="text:replaceAll( $outParamListRequestRes, 'variable_length', 'uint8 ')"!]
      [!IF "not( contains( $userfnc, concat( ':', ./DcmDspRequestResultsRoutineFnc, ':')))"!] 
      [!IF "node:exists(DcmDspRequestResultsRoutineFnc)"!]
          [!IF "DcmDspRoutineFixedLength = 'true'"!]
            extern Std_ReturnType [!"DcmDspRequestResultsRoutineFnc"!]([!"$inParamListRequestRes"!]Dcm_OpStatusType OpStatus, [!"$outParamListRequestRes"!]P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
          [!ELSE!]
            extern Std_ReturnType [!"DcmDspRequestResultsRoutineFnc"!]([!"$inParamListRequestRes"!]Dcm_OpStatusType OpStatus, [!"$outParamListRequestRes"!]P2VAR(uint16, AUTOMATIC, DCM_VAR) currentDataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR) ErrorCode);
          [!ENDIF!][!//
        [!ENDIF!][!//
    [!ENDIF!][!//
        [!VAR "userfnc" = "concat( $userfnc, ./DcmDspRequestResultsRoutineFnc, ':')"!][!// 
    [!ENDIF!]
  [!ENDIF!]
[!ENDLOOP!]

[!/*------------External declarations of user service functions ----------------*/!][!//
[!VAR "SidFuncList" = "':'"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*"!][!//
[!IF "node:exists(DcmDsdSidTabFnc)"!]
[!IF "not( contains( $SidFuncList, concat( ':', ./DcmDsdSidTabFnc, ':')))"!][!//
extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"./DcmDsdSidTabFnc"!](
  Dcm_OpStatusType OpStatus,
  P2VAR(Dcm_MsgContextType, AUTOMATIC, DCM_APPL_DATA) pMsgContext);

[!VAR "SidFuncList" = "concat( $SidFuncList, ./DcmDsdSidTabFnc, ':')"!][!//
[!ENDIF!][!//
[!ENDIF!][!//
[!ENDLOOP!][!//

[!/*------------External declarations of user subservice functions ----------------*/!][!//
[!VAR "SubidFuncList" = "':'"!][!//
[!LOOP "DcmConfigSet/*[1]/DcmDsd/DcmDsdServiceTable/*/DcmDsdService/*/DcmDsdSubService/*"!][!//
[!IF "node:exists(DcmDsdSubServiceFnc)"!]
[!IF "not( contains( $SubidFuncList, concat( ':', ./DcmDsdSubServiceFnc, ':')))"!][!//
extern FUNC(Std_ReturnType, DCM_APPL_CODE) [!"./DcmDsdSubServiceFnc"!](
  Dcm_OpStatusType OpStatus,
  P2VAR(Dcm_MsgContextType, AUTOMATIC, DCM_APPL_DATA) pMsgContext);

[!VAR "SubidFuncList" = "concat( $SubidFuncList, ./DcmDsdSubServiceFnc, ':')"!][!//
[!ENDIF!][!//
[!ENDIF!][!//
[!ENDLOOP!][!//
[!//

[!ENDINDENT!]
#define DCM_STOP_SEC_CALLOUT_CODE
#include <MemMap.h>
/*==================[internal function declarations]=============================================*/

/*==================[external constants]=========================================================*/

/*==================[internal data]==============================================================*/

/*==================[external function definitions]==============================================*/

/*==================[internal function definitions]==============================================*/

#endif /* if !defined( DCM_API_CFG_H ) */
/*==================[end of file]================================================================*/
