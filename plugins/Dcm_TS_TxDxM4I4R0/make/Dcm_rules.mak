# \file
#
# \brief AUTOSAR Dcm
#
# This file contains the implementation of the AUTOSAR
# module Dcm.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

#################################################################
# REGISTRY

Dcm_src_FILES          := \
 $(Dcm_CORE_PATH)\src\Dcm_Hsm.c \
 $(Dcm_CORE_PATH)\src\Dcm.c \
 $(Dcm_CORE_PATH)\src\Dcm_HsmDcmFnct.c \
 $(Dcm_CORE_PATH)\src\Dcm_HsmDcmData.c \
 $(Dcm_CORE_PATH)\src\Dcm_Cbk.c \
 $(Dcm_CORE_PATH)\src\Dcm_Dsp.c \
 $(Dcm_CORE_PATH)\src\Dcm_Dsd.c \
 $(Dcm_CORE_PATH)\src\Dcm_Dsl.c \
 $(Dcm_OUTPUT_PATH)\src\Dcm_Cfg.c

LIBRARIES_TO_BUILD     += Dcm_src

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES
