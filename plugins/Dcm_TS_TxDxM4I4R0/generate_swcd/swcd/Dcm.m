[!/**
 * \file
 *
 * \brief AUTOSAR Dcm
 *
 * This file contains the implementation of the AUTOSAR
 * module Dcm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!//
[!/*=== The code checks if a certain DspData element has read or write access ===*/!][!//
[!MACRO "DspDataElement_Read_Write_ControlAccess"!][!/*
*/!]   [!VAR "ReadAccess" = "'FALSE'"!] [!/*
*/!]   [!VAR "WriteAccess" = "'FALSE'"!] [!/*
*/!]   [!VAR "ControlAccess" = "'FALSE'"!] [!/*
*/!]   [!VAR "Data" = "node:path(.)"!]  [!/*
*/!][!/* Check that DspData element is refered in a Did, which has the read access*/!][!/*
*/!] [!LOOP "../../DcmDspDid/*/DcmDspDidSignal/*[(node:path(node:ref(DcmDspDidDataRef)) = $Data)]"!][!/*
*/!]    [!IF "node:exists(node:ref(../../DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidRead) = 'true'"!][!/*
*/!]       [!VAR "ReadAccess" = "'TRUE'"!][!/*
*/!]       [!BREAK!][!/*
*/!]    [!ENDIF!][!/*
*/!] [!ENDLOOP!][!/*
*/!][!/* Check that DspData element is refered in a Did, which has the write access*/!][!/*
*/!] [!LOOP "../../DcmDspDid/*/DcmDspDidSignal/*[(node:path(node:ref(DcmDspDidDataRef)) = $Data)]"!][!/*
*/!]    [!IF "node:exists(node:ref(../../DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidWrite) = 'true'"!][!/*
*/!]      [!VAR "WriteAccess" = "'TRUE'"!][!/*
*/!]      [!BREAK!][!/*
*/!]    [!ENDIF!][!/*
*/!] [!ENDLOOP!][!/*
*/!][!/* Check that DspData element is refered in a Did, which has the control access*/!][!/*
*/!] [!LOOP "../../DcmDspDid/*/DcmDspDidSignal/*[(node:path(node:ref(DcmDspDidDataRef)) = $Data)]"!][!/*
*/!]    [!IF "node:exists(node:ref(../../DcmDspDidInfoRef)/DcmDspDidAccess/DcmDspDidControl) = 'true'"!][!/*
*/!]      [!VAR "ControlAccess" = "'TRUE'"!][!/*
*/!]      [!BREAK!][!/*
*/!]    [!ENDIF!][!/*
*/!] [!ENDLOOP!][!/*
 
*/!][!ENDMACRO!]

