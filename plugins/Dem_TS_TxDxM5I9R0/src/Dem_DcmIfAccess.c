/**
 * \file
 *
 * \brief AUTOSAR Dem
 *
 * This file contains the implementation of the AUTOSAR
 * module Dem.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* This file contains definitions for interface functions provided by Dem to
 * the Dcm for clearing and control of DTC setting. */

/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 12.4 (required)
 *    "The right-hand operand of a logical '&&' or '||' operator shall not
 *    contain side effects."
 *
 *    Reason:
 *    Call to getter functions which do not modify any global state.
 */

/*==================[inclusions]============================================*/
/* !LINKSTO dsn.Dem.IncludeStr,1 */

#include <TSAutosar.h>                        /* EB specific standard types */
#include <Std_Types.h>                            /* AUTOSAR standard types */

#include <SchM_Dem.h>             /* for the locking of the exclusive areas */

#include <Dem_Int.h>             /* Module public and internal declarations */
#include <Dem_Trace.h>                        /* Dbg related macros for Dem */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

#define DEM_START_SEC_CODE
#include <MemMap.h>

/** \brief Function to convert an 24-bit DTC-group-value into the
 ** corresponding DTC-group-index
 **
 ** \param[in] DTCGroup
 ** \param[out] Idx
 **
 ** \return Boolean value indication success
 ** \retval TRUE  could find corresponding DTC-group-index
 ** \retval FALSE wrong DTC-group-value
 **
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 **
 */
STATIC FUNC(boolean, DEM_CODE) Dem_GetIdxOfDTCGroup(
  Dem_DTCGroupType                      DTCGroup,
  CONSTP2VAR(uint8, AUTOMATIC, DEM_VAR) Idx);

/** \brief Process clearing of all event status related to a DTC or DTC group
 **
 ** \param[in]  DTC
 ** \param[in]  DTCOrigin
 ** \param[in]  DtcGroupRequest  TRUE, in case a group of DTC is requested
 ** \param[in]  DtcGroupIndex    Internal group index of requested DTC group
 ** \param[out] NvDataModified   if any DTC status was changed
 **
 ** \return Updated result of ClearDTC process
 */
STATIC FUNC(Dem_ReturnClearDTCType, DEM_CODE) Dem_ClearDTCStatus(
  Dem_DTCType                               DTC,
  Dem_DTCOriginType                         DTCOrigin,
  boolean                                   DtcGroupRequest,
  uint8                                     DtcGroupIndex,
  CONSTP2VAR(boolean, AUTOMATIC, AUTOMATIC) NvDataModified);

/** \brief Process clearing of all event entries related to a DTC or DTC group
 **
 ** \param[in]  DTC
 ** \param[in]  DTCOrigin
 ** \param[in]  DtcGroupRequest  TRUE, in case a group of DTC is requested
 ** \param[in]  DtcGroupIndex    Internal group index of requested DTC group
 ** \param[out] NvDataModified   if any DTC entry was cleared
 **
 ** \return Updated result of ClearDTC process
 **/
STATIC FUNC(Dem_ReturnClearDTCType, DEM_CODE) Dem_ClearDTCEntries(
  Dem_DTCType                               DTC,
  Dem_DTCOriginType                         DTCOrigin,
  boolean                                   DtcGroupRequest,
  uint8                                     DtcGroupIndex,
  CONSTP2VAR(boolean, AUTOMATIC, AUTOMATIC) NvDataModified);

#define DEM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

#define DEM_START_SEC_CODE
#include <MemMap.h>

/*------------------[Dem_ClearDTC]------------------------------------------*/

FUNC(Dem_ReturnClearDTCType, DEM_CODE) Dem_ClearDTC(
  Dem_DTCType       DTC,
  Dem_DTCFormatType DTCFormat,
  Dem_DTCOriginType DTCOrigin)
  /* DTCFormat parameter is ignored since there is no OBD support:
     Implementation is scheduled for future releases. */
{
  Dem_ReturnClearDTCType result = DEM_CLEAR_OK;

  DBG_DEM_CLEARDTC_ENTRY(DTC, DTCFormat, DTCOrigin);

#if (DEM_DEV_ERROR_DETECT == STD_ON)
  if (Dem_InitializationState != DEM_INITIALIZED)
  {
    DEM_REPORT_ERROR(DEM_SID_ClearDTC, DEM_E_UNINIT);
    result = DEM_CLEAR_FAILED;
  }
  else if (DTC > (Dem_DTCType) 0xFFFFFFU)
  {
    DEM_REPORT_ERROR(DEM_SID_ClearDTC, DEM_E_PARAM_DATA);
    result = DEM_CLEAR_WRONG_DTC;
  }
  else if ( (DTCFormat != DEM_DTC_FORMAT_OBD) &&
            (DTCFormat != DEM_DTC_FORMAT_UDS)
          )
  {
    DEM_REPORT_ERROR(DEM_SID_ClearDTC, DEM_E_PARAM_DATA);
    result = DEM_CLEAR_FAILED;
  }
  else if ( ( ( (DTCOrigin != DEM_DTC_ORIGIN_PRIMARY_MEMORY) &&
                (DTCOrigin != DEM_DTC_ORIGIN_SECONDARY_MEMORY)
              ) &&
              (DTCOrigin != DEM_DTC_ORIGIN_MIRROR_MEMORY)
            ) &&
            (DTCOrigin != DEM_DTC_ORIGIN_PERMANENT_MEMORY)
          )
  {
    DEM_REPORT_ERROR(DEM_SID_ClearDTC, DEM_E_PARAM_DATA);
    result = DEM_CLEAR_WRONG_DTCORIGIN;
  }
  else
#endif /* DEM_DEV_ERROR_DETECT */
  {
    const uint8 OriginIdx = (uint8)(DTCOrigin - DEM_DTC_ORIGIN_PRIMARY_MEMORY);
    uint8 groupIdx;

    boolean DtcGroupRequest = FALSE;

#if (DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH)
    /* check if an async ClearDTC was started */
    if (DEM_CLEARDTC_STATE_RUNNING == Dem_ClearDtcAsyncStatus.RunState)
    {
      /* report result PENDING */
      result = DEM_CLEAR_PENDING;
    }
    /* CHECK: NOPARSE */
    else if (DEM_CLEARDTC_STATE_CANCELLED == Dem_ClearDtcAsyncStatus.RunState)
    { /* !LINKSTO Dem629,1 */
      /* report result FAILED */
      result = DEM_CLEAR_FAILED;
      /* Dem_DcmCancelOperation is able to cancel ClearDTC immediately */
      DEM_UNREACHABLE_CODE_ASSERT(DEM_SID_ClearDTC);
    }
    /* CHECK: PARSE */
    else if (DEM_CLEARDTC_STATE_FAILED == Dem_ClearDtcAsyncStatus.RunState)
    {
      /* set the status back to IDLE */
      Dem_ClearDtcAsyncStatus.RunState = DEM_CLEARDTC_STATE_IDLE;
      /* report result FAILED */
      result = DEM_CLEAR_FAILED;
    }
    else if (DEM_CLEARDTC_STATE_FINISHED == Dem_ClearDtcAsyncStatus.RunState)
    { /* !LINKSTO Dem572,1 */
      /* set the status back to IDLE */
      Dem_ClearDtcAsyncStatus.RunState = DEM_CLEARDTC_STATE_IDLE;
      /* report result OK */
      result = DEM_CLEAR_OK;
    }
    else /* RunState: DEM_CLEARDTC_STATE_IDLE */
#endif /* DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH */
    {
      boolean EvStNvDataModified = FALSE;
      boolean EvMemNvDataModified = FALSE;

      /* check if requested DTC origin is supported */
      if (Dem_SizeEventMem[OriginIdx] == (Dem_SizeEvMemEntryType) 0U)
      {
        result = DEM_CLEAR_WRONG_DTCORIGIN;
      }
      /* check if DTC matches a group Id */
      else if ((Dem_DTCGroupType)DTC == DEM_DTC_GROUP_ALL_DTCS)
      {
        /* groupIdx is not modified if 'DTCGroup == DEM_DTC_GROUP_ALL_DTCS',
         * and is not allowed to be used in this case */
        DtcGroupRequest = TRUE;
      }
      else if (Dem_GetIdxOfDTCGroup((Dem_DTCGroupType) DTC, &groupIdx) !=
               FALSE)
      {
        DtcGroupRequest = TRUE;
      }
      else if (DTCFormat == DEM_DTC_FORMAT_OBD)
      {
        result = DEM_CLEAR_WRONG_DTC;
      }
      else
      {
        /* DtcGroupRequest remains FALSE */
      }

      /* first step, clear event status:
       * - if all preconditions are met, execute clearing routines */
      if (result == DEM_CLEAR_OK)
      {
        result = Dem_ClearDTCStatus(
          DTC, DTCOrigin, DtcGroupRequest, groupIdx, &EvStNvDataModified);
      }

      /* second step, clear event entry
       * - if the DTC status was cleared successfully, also remove the entries */
      if (result == DEM_CLEAR_OK)
      {
        result = Dem_ClearDTCEntries(
          DTC, DTCOrigin, DtcGroupRequest, groupIdx, &EvMemNvDataModified);
      }

#if (DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH)
      if ((EvStNvDataModified == TRUE) || (EvMemNvDataModified == TRUE))
      {
        Dem_ClearDtcAsyncStatus.RunState = DEM_CLEARDTC_STATE_RUNNING;

        /* !LINKSTO Dem572,1 */
        /* set result to PENDING */
        result = DEM_CLEAR_PENDING;
      }
#endif

#if (DEM_USE_IMMEDIATE_NV_STORAGE == STD_ON)
      /* at this point, result is always CLEAR_OK or CLEAR_PENDING */
      /* depending on DTC behavior:
       * - !LINKSTO Dem571,1
       *     if set to NONVOLATILE_TRIGGER, trigger NvM and return OK
       * - !LINKSTO Dem572,1
       *     if set to NONVOLATILE_FINISH, trigger NvM and return PENDING
       * - !LINKSTO Dem570,1
       *     if set to VOLATILE, trigger NvM (for immediate storage events)
       *                         and return OK */
      Dem_TriggerNvMWrite();
#endif
    }
  }

  DBG_DEM_CLEARDTC_EXIT(result, DTC, DTCFormat, DTCOrigin);
  return result;
}

/*------------------[Dem_DisableDTCSetting]---------------------------------*/

FUNC(Dem_ReturnControlDTCSettingType, DEM_CODE) Dem_DisableDTCSetting(
  Dem_DTCGroupType DTCGroup,
  Dem_DTCKindType  DTCKind)
  /* DTCKind parameter is ignored:
     The current implementation only supports non OBD-relevant ECUs.
     Implementation is scheduled for future releases. */
{
  Dem_ReturnControlDTCSettingType result = DEM_CONTROL_DTC_SETTING_N_OK;

  DBG_DEM_DISABLEDTCSETTING_ENTRY(DTCGroup, DTCKind);

#if (DEM_DEV_ERROR_DETECT == STD_ON)
  if (Dem_InitializationState != DEM_INITIALIZED)
  {
    DEM_REPORT_ERROR(DEM_SID_DisableDTCSetting, DEM_E_UNINIT);
  }
  else if ( (DTCKind != DEM_DTC_KIND_ALL_DTCS) &&
            (DTCKind != DEM_DTC_KIND_EMISSION_REL_DTCS)
          )
  {
    DEM_REPORT_ERROR(DEM_SID_DisableDTCSetting, DEM_E_PARAM_DATA);
  }
  else
#else /* DEM_DEV_ERROR_DETECT */
  TS_PARAM_UNUSED(DTCKind);
#endif /* DEM_DEV_ERROR_DETECT */
  {
    if (DTCGroup == DEM_DTC_GROUP_ALL_DTCS)
    {
      /* disable DTC setting for all groups beside DEM_DTCGRP_IDX_NO_DTC */
      Dem_DTCSettingAllowed = (uint32)1U << DEM_DTCGRP_IDX_NO_DTC;

      result = DEM_CONTROL_DTC_SETTING_OK;
    }
    else
    {
      uint8 GroupIdx;

      if (Dem_GetIdxOfDTCGroup(DTCGroup, &GroupIdx) == TRUE)
      {
        /* disable DTC setting for requested group index */
        DEM_ATOMIC_CLR_BIT_32(Dem_DTCSettingAllowed, GroupIdx);

        result = DEM_CONTROL_DTC_SETTING_OK;
      }
      else /* invalid or not configured group */
      {
        result = DEM_CONTROL_DTC_WRONG_DTCGROUP;
      }
    }
  }

  DBG_DEM_DISABLEDTCSETTING_EXIT(result, DTCGroup, DTCKind);
  return result;
}

/*------------------[Dem_EnableDTCSetting]----------------------------------*/

FUNC(Dem_ReturnControlDTCSettingType, DEM_CODE) Dem_EnableDTCSetting(
  Dem_DTCGroupType DTCGroup,
  Dem_DTCKindType  DTCKind)
  /* DTCKind parameter is ignored:
     The current implementation only supports non OBD-relevant ECUs.
     Implementation is scheduled for future releases. */
{
  Dem_ReturnControlDTCSettingType result = DEM_CONTROL_DTC_SETTING_N_OK;

  DBG_DEM_ENABLEDTCSETTING_ENTRY(DTCGroup, DTCKind);

#if (DEM_DEV_ERROR_DETECT == STD_ON)
  if (Dem_InitializationState != DEM_INITIALIZED)
  {
    DEM_REPORT_ERROR(DEM_SID_EnableDTCSetting, DEM_E_UNINIT);
  }
  else if ( (DTCKind != DEM_DTC_KIND_ALL_DTCS) &&
            (DTCKind != DEM_DTC_KIND_EMISSION_REL_DTCS)
          )
  {
    DEM_REPORT_ERROR(DEM_SID_EnableDTCSetting, DEM_E_PARAM_DATA);
  }
  else
#else /* DEM_DEV_ERROR_DETECT */
  TS_PARAM_UNUSED(DTCKind);
#endif /* DEM_DEV_ERROR_DETECT */
  {
    if (DTCGroup == DEM_DTC_GROUP_ALL_DTCS)
    {
      /* enable DTC setting for all groups */
      DEM_ATOMIC_ASSIGN_32(Dem_DTCSettingAllowed,
                           DEM_BITFIELD_VALUE_ALL_GROUPS);

      result = DEM_CONTROL_DTC_SETTING_OK;
    }
    else
    {
      uint8 GroupIdx;

      if (Dem_GetIdxOfDTCGroup(DTCGroup, &GroupIdx) == TRUE)
      {
        /* enable DTC setting for requested group index */
        DEM_ATOMIC_SET_BIT_32(Dem_DTCSettingAllowed, GroupIdx);

        result = DEM_CONTROL_DTC_SETTING_OK;
      }
      else /* invalid or not configured group */
      {
        result = DEM_CONTROL_DTC_WRONG_DTCGROUP;
      }
    }
  }

  DBG_DEM_ENABLEDTCSETTING_EXIT(result, DTCGroup, DTCKind);
  return result;
}

/*------------------[Dem_DcmCancelOperation]--------------------------------*/

FUNC(void, DEM_CODE) Dem_DcmCancelOperation(void)
{
  DBG_DEM_DCMCANCELOPERATION_ENTRY();

#if (DEM_DEV_ERROR_DETECT == STD_ON)
  if (Dem_InitializationState != DEM_INITIALIZED)
  {
    DEM_REPORT_ERROR(DEM_SID_DcmCancelOperation, DEM_E_UNINIT);
  }
  else
#endif
  {
#if (DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH)
    /* check if asynchronous ClearDTC request is running */
    if (DEM_CLEARDTC_STATE_RUNNING == Dem_ClearDtcAsyncStatus.RunState)
    {
      /* immediate storage mechanism can handle data management in the
       * background including subsequent ClearDTC requests and event status
       * reports -> simply ClearDtcAsyncStatus back to IDLE */
      Dem_ClearDtcAsyncStatus.RunState = DEM_CLEARDTC_STATE_IDLE;
    }
#endif
  }

  /* Dem_Set<...>Filter() APIs will cancel previouse filter automatically,
     when called again by Dcm */

  DBG_DEM_DCMCANCELOPERATION_EXIT();
}

/*------------------[Dem_ClearEventStatus]-----------------------------------*/

FUNC(boolean, DEM_CODE) Dem_ClearEventStatus(Dem_EventIdType EventId)
{
  boolean EventStatusChanged = FALSE;
#if (DEM_USE_CB_CLEAR_EVENT_ALLOWED == STD_ON)
  boolean Allowed;
  Std_ReturnType RetVal;

  DBG_DEM_CLEAREVENTSTATUS_ENTRY(EventId);

  RetVal = Dem_CbClearEventAllowed(EventId, &Allowed);

  if ( (RetVal != E_OK) || ( Allowed == TRUE) )
#else
  DBG_DEM_CLEAREVENTSTATUS_ENTRY(EventId);
#endif /* (DEM_USE_CB_CLEAR_EVENT_ALLOWED == STD_ON) */
  {
    const Dem_DTCStatusMaskType DTCStatusMask =
      (DEM_DTCSTATUSBIT_TNCSLC | DEM_DTCSTATUSBIT_TNCTOC);
    Dem_DTCStatusMaskType OldDTCStatusMask;

    /* get old status mask */
    DEM_ATOMIC_ASSIGN_8(OldDTCStatusMask, DEM_DTC_STATUS_MASKS[EventId]);

    /* reset status mask for event */
    DEM_ATOMIC_ASSIGN_8(DEM_DTC_STATUS_MASKS[EventId], DTCStatusMask);

#if (DEM_MAX_NUMBER_PRESTORED_FF > 0U)
    /* release prestored freeze frame */
    if (Dem_GbiEnablePrestorage(EventId) == TRUE)
    {
      /* mark prestored freeze frame as unused */
      DEM_ATOMIC_CLR_BIT_IN_ARRAY_8(
        Dem_PFFEntryValid, Dem_GetPFFIndex(EventId));
    }
#endif

    /* reset debouncing counters for event (Dem343) */
    Dem_DebounceFctPtrTable[Dem_GbiDebounceAlgo(EventId)].ResetFct(EventId);

    /* report event change by callback */
    if (OldDTCStatusMask != DTCStatusMask)
    {
#if ( (DEM_USE_CB_TRIG_ON_EVST == STD_ON) || \
      (DEM_USE_CB_TRIG_ON_DTCST == STD_ON) )
      /* notify application that event status has changed */
      Dem_CbTriggerOnEventStatus(EventId, OldDTCStatusMask, DTCStatusMask);
#endif

#if (DEM_USE_CB_INIT_MONITOR == STD_ON)
      /* Dem009: inform application about clearance */
      Dem_CbInitMonitorForEvent(EventId, DEM_INIT_MONITOR_CLEAR);
#endif

      EventStatusChanged = TRUE;
    }
  }

  DBG_DEM_CLEAREVENTSTATUS_EXIT(EventStatusChanged, EventId);
  return EventStatusChanged;
}

/*==================[internal function definitions]=========================*/

STATIC FUNC(Dem_ReturnClearDTCType, DEM_CODE) Dem_ClearDTCStatus(
  Dem_DTCType                               DTC,
  Dem_DTCOriginType                         DTCOrigin,
  boolean                                   DtcGroupRequest,
  uint8                                     DtcGroupIndex,
  CONSTP2VAR(boolean, AUTOMATIC, AUTOMATIC) NvDataModified)
{
  boolean EventMemoryModified = FALSE;
  Dem_ReturnClearDTCType result = DEM_CLEAR_OK;
  Dem_EventIdType EventId;

  DBG_DEM_CLEARDTCSTATUS_ENTRY(
    DTC, DTCOrigin, DtcGroupRequest, DtcGroupIndex, NvDataModified);

  /* Note reg. nonuse of critical section:
   * - Dem_ClearEventStatus triggers several callbacks which must not be
   *   protected.
   * - DTC status bits are modified using atomic assignment.
   */

  /* search all event Ids for requested event */
  for (EventId = 1U; EventId <= DEM_MAX_EVENTID; ++EventId)
  {
    const uint8 groupIdxOfEvt = Dem_GbiDTCGroupIdx(EventId);
    const Dem_DTCOriginType originOfEvt = Dem_GbiDTCOrigin(EventId);

    /* Deviation MISRA-1 <+9> */
    if ( /* single DTC: requested DTC and respective origin */
         ( (FALSE == DtcGroupRequest) &&
           (Dem_GbiDTC(EventId) == DTC) &&
           (originOfEvt == DTCOrigin) ) ||
         /* group of DTC: requested group and respective origin only */
         ( ( ( ((Dem_DTCGroupType) DTC == DEM_DTC_GROUP_ALL_DTCS) &&
               (groupIdxOfEvt != DEM_DTCGRP_IDX_NO_DTC) ) ||
             (DtcGroupIndex == groupIdxOfEvt) ) &&
           (originOfEvt == DTCOrigin) ) )
    {
#if (DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH)
      /* clear DTC depends on clear event allowed callback */
      if (Dem_ClearEventStatus(EventId) == TRUE)
      {
        /* event status was modified: mark block as modified to trigger
         * a ClearDtc run state transition to RUNNING */
        EventMemoryModified = TRUE;
        /* mark event status block to be written */
        Dem_ClearDtcAsyncStatus.EventStatusWrite = DEM_NVM_PREPARE;
      }
#else
      (void)Dem_ClearEventStatus(EventId);
#endif

      if (FALSE == DtcGroupRequest)
      {
        /* event for single DTC found: clear OK, stop searching */
        /* result remains DEM_CLEAR_OK */
        break;
      } /* else: group of DTC -> continue searching */
    } /* else: neither DTC nor group is matching */
  }

  /* check if a single DTC has been found and deleted */
  if ( (FALSE == DtcGroupRequest) && (EventId > DEM_MAX_EVENTID) )
  {
    /* single DTC not found, wrong DTC */
    result = DEM_CLEAR_WRONG_DTC;
  }

  *NvDataModified = EventMemoryModified;

  DBG_DEM_CLEARDTCSTATUS_EXIT(
    result, DTC, DTCOrigin, DtcGroupRequest, DtcGroupIndex, NvDataModified);
  return result;
}

STATIC FUNC(Dem_ReturnClearDTCType, DEM_CODE) Dem_ClearDTCEntries(
  Dem_DTCType                               DTC,
  Dem_DTCOriginType                         DTCOrigin,
  boolean                                   DtcGroupRequest,
  uint8                                     DtcGroupIndex,
  CONSTP2VAR(boolean, AUTOMATIC, AUTOMATIC) NvDataModified)
{
  boolean EventMemoryChanged = FALSE;
  Dem_ReturnClearDTCType result = DEM_CLEAR_OK;
  Dem_SizeEvMemEntryType EventEntryIdx;
  Dem_EventIdType EventId;

  const uint8 OriginIdx = (uint8)(DTCOrigin - DEM_DTC_ORIGIN_PRIMARY_MEMORY);
  const Dem_EventMemoryEntryPtrType EventMem = Dem_EventMem[OriginIdx];
  const Dem_SizeEvMemEntryType sizeEventMem = Dem_SizeEventMem[OriginIdx];

#if (DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH)
  boolean clearedEntriesPending = FALSE;
#endif

#if (DEM_USE_IMMEDIATE_NV_STORAGE == STD_ON)
  Dem_NvMGatePrepStatusType NvMGateStatus;
#endif

  DBG_DEM_CLEARDTCENTRIES_ENTRY(
    DTC, DTCOrigin, DtcGroupRequest, DtcGroupIndex, NvDataModified);

#if (DEM_USE_IMMEDIATE_NV_STORAGE == STD_ON)
  /* try to lock the gate entry */
  NvMGateStatus = Dem_BeginPrepareGateEntry(OriginIdx);
#endif

  /*
   * ENTER critical section to protect the event memory
   *       call-context: Dcm (Task)
   */
  SchM_Enter_Dem_SCHM_DEM_EXCLUSIVE_AREA_0();

  /* search for entry with given DTC */
  for (EventEntryIdx = 0U; EventEntryIdx < sizeEventMem; ++EventEntryIdx)
  {
    EventId = EventMem[EventEntryIdx].EventId;

    if (EventId != DEM_EVENT_ID_INVALID)
    {
      const uint8 groupIdxOfEvt = Dem_GbiDTCGroupIdx(EventId);
      const Dem_DTCOriginType originOfEvt = Dem_GbiDTCOrigin(EventId);

      /* Deviation MISRA-1 <+7> */
      if ( /* single DTC */
           ( (FALSE == DtcGroupRequest) && (Dem_GbiDTC(EventId) == DTC) ) ||
           /* group of DTC: requested group and respective origin only */
           ( ( ( ((Dem_DTCGroupType) DTC == DEM_DTC_GROUP_ALL_DTCS) &&
                 (groupIdxOfEvt != DEM_DTCGRP_IDX_NO_DTC) ) ||
               (DtcGroupIndex == groupIdxOfEvt) ) &&
             (originOfEvt == DTCOrigin) ) )
      {
        /* if event clearing was allowed, the mask now is TNCSLC | TNCTOC */
        if ( DEM_DTC_STATUS_MASKS[EventId] ==
               (DEM_DTCSTATUSBIT_TNCSLC | DEM_DTCSTATUSBIT_TNCTOC) )
        {
          Dem_ClearEventEntry(&EventMem[EventEntryIdx]);

#if (DEM_USE_IMMEDIATE_NV_STORAGE == STD_ON)
#if (DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH)
          clearedEntriesPending = TRUE;
#endif
          /* event memory entry has been deleted: trigger immediate storage */
          Dem_CheckForImmediateEntryStorage(
            EventId,
            &Dem_EventMem[OriginIdx][EventEntryIdx],
            EventEntryIdx,
#if (DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_VOLATILE)
            DEM_NVM_NO_FORCE, /* CLRRESP_VOLATILE: updated on shutdown */
#else
            DEM_NVM_FORCE, /* CLRRESP_NONVOLATILE_<...>: write immediately */
#endif
            &NvMGateStatus);
#endif /* DEM_USE_IMMEDIATE_NV_STORAGE == STD_ON */

          if (FALSE == DtcGroupRequest)
          {
            /* event for single DTC found: clear OK, stop searching */
            /* result remains DEM_CLEAR_OK */
            break;
          } /* else: group of DTC -> continue searching */
        } /* else: clear event not allowed */
      } /* else: neither DTC nor group is matching */
    } /* else: EventId is INVALID */

#if (DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH)
    /* check if entry is scheduled to be cleared earlier */
    if (EventMem[EventEntryIdx].EntryStatus == DEM_ENTRY_CLEAR_IMMEDIATELY)
    {
      clearedEntriesPending = TRUE;
    }
#endif
  }

  /*
   * LEAVE critical section
   */
  SchM_Exit_Dem_SCHM_DEM_EXCLUSIVE_AREA_0();

  if (DEM_DTC_GROUP_ALL_DTCS == (Dem_DTCGroupType)DTC)
  {
    DEM_ATOMIC_CLR_BIT_8(DEM_OVFIND_FLAGS, OriginIdx);
  }

  if (DtcGroupRequest != FALSE)
  {
    result = DEM_CLEAR_OK;
  }

#if (DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH)
  if ( (DEM_CLEAR_OK == result) && (clearedEntriesPending != FALSE) )
  {
    Dem_ClearDtcAsyncStatus.OriginIdx = OriginIdx;
    Dem_ClearDtcAsyncStatus.EventMemWrite = DEM_NVM_WAITING;

    EventMemoryChanged = TRUE;
  }
#endif /* DEM_CLEAR_DTC_BEHAVIOR == DEM_CLRRESP_NONVOLATILE_FINISH */

#if (DEM_USE_IMMEDIATE_NV_STORAGE == STD_ON)
  /* unlock the gate entry */
  Dem_EndPrepareGateEntry(OriginIdx, NvMGateStatus);
#endif /* DEM_USE_IMMEDIATE_NV_STORAGE == STD_ON */

  *NvDataModified = EventMemoryChanged;

  DBG_DEM_CLEARDTCENTRIES_EXIT(
    result, DTC, DTCOrigin, DtcGroupRequest, DtcGroupIndex, NvDataModified);
  return result;
}

STATIC FUNC(boolean, DEM_CODE) Dem_GetIdxOfDTCGroup(
  Dem_DTCGroupType                      DTCGroup,
  CONSTP2VAR(uint8, AUTOMATIC, DEM_VAR) Idx)
{
  boolean result;
  uint8_least index;

  DBG_DEM_GETIDXOFDTCGROUP_ENTRY(DTCGroup, Idx);

  /* for all DTCGroups */
  for (index = 0U; index < DEM_NUM_DTC_GROUPS; index++)
  {
    if (DTCGroup == Dem_DTCGroups[index])
    {
      break;
    }
  }

  if (index < DEM_NUM_DTC_GROUPS)
  {
    *Idx   = (uint8)index;
    result = TRUE;
  }
  else
  {
    *Idx   = DEM_DTCGRP_IDX_INVALID;
    result = FALSE;
  }

  DBG_DEM_GETIDXOFDTCGROUP_EXIT(result, DTCGroup, Idx);
  return result;
}

#define DEM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]===========================================*/
