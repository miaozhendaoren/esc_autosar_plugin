/**
 * \file
 *
 * \brief AUTOSAR Dem
 *
 * This file contains the implementation of the AUTOSAR
 * module Dem.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!AUTOSPACING!]

/*==================[inclusions]============================================*/

#include <Std_Types.h>                            /* AUTOSAR standard types */
#include <TSAutosar.h>     /* EB specific standard types, TS_PARAM_UNUSED() */

[!IF "(count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) or (DemGeneral/DemClearDTCBehavior != 'DEM_CLRRESP_VOLATILE') or node:exists(DemGeneral/DemCalloutDynamicDTCFnc)"!]
#include <Dem_Cfg.h>                             /* declaration of callouts */
[!ENDIF!]

[!IF "(count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) or (DemGeneral/DemClearDTCBehavior != 'DEM_CLRRESP_VOLATILE')"!]
/* Implementation hint:
 * include necessary Memory-Stack header-files: NvM.h, Ea/Fee.h, Eep/Fls.h */
[!ENDIF!]

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

[!IF "(count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) or (DemGeneral/DemClearDTCBehavior != 'DEM_CLRRESP_VOLATILE') or node:exists(DemGeneral/DemCalloutDynamicDTCFnc)"!]

#define DEM_START_SEC_CODE
#include <MemMap.h>

/*------------------[Main callouts]-----------------------------------------*/

[!IF "(count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) or (DemGeneral/DemClearDTCBehavior != 'DEM_CLRRESP_VOLATILE')"!]

FUNC(void, DEM_APPL_CODE) [!"DemGeneral/DemCallbackMemStackMainFuncTrigger"!](const NvM_BlockIdType BlockId)
{
  TS_PARAM_UNUSED(BlockId);

  /* Implement the routine to trigger memory stack main functions
   * with in this callout function.
   *
   *  Implementation hint:
   *
   *  FUNC(void, DEM_APPL_CODE) Appl_TriggerMemoryStack(const NvM_BlockIdType BlockId)
   *  {
   *    NvM_RequestResultType Result;
   *
   *    do
   *    {
   *      NvM_MainFunction();
   *      Ea_MainFunction(); //Fee_MainFunction();
   *      Eep_MainFunction(); //Fls_MainFunction();
   *
   *      NvM_GetErrorStatus(BlockId, &Result);
   *    }
   *    while (Result != NVM_REQ_OK);
   *  }
   */
}

[!ENDIF!]

[!IF "node:exists(DemGeneral/DemCalloutDynamicDTCFnc)"!]
/* With Side Allocation feature enabled, implement this function to fetch a dynamic DTC value.
 *
 * FUNC(uint32, DEM_APPL_CODE) [!"DemGeneral/DemCalloutDynamicDTCFnc"!](const Dem_EventIdType EventId, uint32 DTC)
 * {
 *    Implementation hint:
 *
 *    * calculate or look up new DTC based on EventId and/or original DTC value *
 *
 *    * return the DTC to be used for this event Id *
 *    return DTC;
 * }
 */
[!ENDIF!]

#define DEM_STOP_SEC_CODE
#include <MemMap.h>

[!ENDIF!]

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
