/**
 * \file
 *
 * \brief AUTOSAR Dem
 *
 * This file contains the implementation of the AUTOSAR
 * module Dem.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DEM_INT_CFG_H)
#define DEM_INT_CFG_H

/* This file contains the all module internal configuration declarations for
 * the AUTOSAR module Dem that are target independent. */

[!AUTOSPACING!]
[!INCLUDE "Dem_Include.m"!]
[!//
/*==================[includes]==============================================*/
/* !LINKSTO dsn.Dem.IncludeStr,1 */

/*------------------[callback header file inclusions]-----------------------*/

[!LOOP "DemGeneral/DemHeaderFileInclusion/*"!]
  [!WS "0"!]#include <[!"."!]>
[!ENDLOOP!]

/*==================[macros]================================================*/

/*------------------[General]-----------------------------------------------*/

#if (defined DEM_USE_AGING)
#error DEM_USE_AGING already defined
#endif
/** \brief Macro for Enabling/Disabling aging functionality */
[!IF "count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemAgingAllowed = 'true']) > 0"!]
  [!WS "0"!]#define DEM_USE_AGING   STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_USE_AGING   STD_OFF
[!ENDIF!]

#if (defined DEM_USE_INT_VAL_AREA)
#error DEM_USE_INT_VAL_AREA already defined
#endif
/** \brief Macro for Enabling/Disabling internal value area
 **
 ** In the current implementation the existance of the internal value area
 ** depends on aging only. */
#define DEM_USE_INT_VAL_AREA   DEM_USE_AGING

#if (defined DEM_USE_MAPPED_INT_DATA_ELEMENTS)
#error DEM_USE_MAPPED_INT_DATA_ELEMENTS already defined
#endif
/** \brief Macro for Enabling/Disabling internal data elements mapping */
[!/* note: internal data elements can not be configured for FFs */!][!//
[!IF "count(node:refs(node:refs(DemGeneral/DemExtendedDataClass/*/DemExtendedDataRecordClassRef/*)/DemDataElementClassRef/*)[. = 'DemInternalDataElementClass']) > 0"!]
  [!WS "0"!]#define DEM_USE_MAPPED_INT_DATA_ELEMENTS    STD_ON
[!ELSE!][!//
  [!WS "0"!]#define DEM_USE_MAPPED_INT_DATA_ELEMENTS    STD_OFF
[!ENDIF!]




/*------------------[Defensive programming]---------------------------------*/
[!SELECT "DemDefensiveProgramming"!][!//

#if (defined DEM_DEFENSIVE_PROGRAMMING_ENABLED)
#error DEM_DEFENSIVE_PROGRAMMING_ENABLED is already defined
#endif
/** \brief Defensive programming usage
 **
 ** En- or disables the usage of the defensive programming */
#define DEM_DEFENSIVE_PROGRAMMING_ENABLED   [!//
[!IF "(../DemGeneral/DemDevErrorDetect  = 'true') and (DemDefProgEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DEM_PRECONDITION_ASSERT_ENABLED)
#error DEM_PRECONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Precondition assertion usage
 **
 ** En- or disables the usage of precondition assertion checks */
#define DEM_PRECONDITION_ASSERT_ENABLED     [!//
[!IF "(../DemGeneral/DemDevErrorDetect  = 'true') and (DemDefProgEnabled = 'true') and (DemPrecondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DEM_POSTCONDITION_ASSERT_ENABLED)
#error DEM_POSTCONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Postcondition assertion usage
 **
 ** En- or disables the usage of postcondition assertion checks */
#define DEM_POSTCONDITION_ASSERT_ENABLED    [!//
[!IF "(../DemGeneral/DemDevErrorDetect  = 'true') and (DemDefProgEnabled = 'true') and (DemPostcondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DEM_UNREACHABLE_CODE_ASSERT_ENABLED)
#error DEM_UNREACHABLE_CODE_ASSERT_ENABLED is already defined
#endif
/** \brief Unreachable code assertion usage
 **
 ** En- or disables the usage of unreachable code assertion checks */
#define DEM_UNREACHABLE_CODE_ASSERT_ENABLED [!//
[!IF "(../DemGeneral/DemDevErrorDetect  = 'true') and (DemDefProgEnabled = 'true') and (DemUnreachAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DEM_INVARIANT_ASSERT_ENABLED)
#error DEM_INVARIANT_ASSERT_ENABLED is already defined
#endif
/** \brief Invariant assertion usage
 **
 ** En- or disables the usage of invariant assertion checks */
#define DEM_INVARIANT_ASSERT_ENABLED        [!//
[!IF "(../DemGeneral/DemDevErrorDetect  = 'true') and (DemDefProgEnabled = 'true') and (DemInvariantAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined DEM_STATIC_ASSERT_ENABLED)
#error DEM_STATIC_ASSERT_ENABLED is already defined
#endif
/** \brief Static assertion usage
 **
 ** En- or disables the usage of static assertion checks */
#define DEM_STATIC_ASSERT_ENABLED           [!//
[!IF "(../DemGeneral/DemDevErrorDetect  = 'true') and (DemDefProgEnabled = 'true') and (DemStaticAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDSELECT!][!//


/*------------------[Callback macros]---------------------------------------*/
[!VAR "GenericCtr" = "count(DemConfigSet/*/DemEventParameter/*/DemCallbackInitMForE)"!]

#if (defined DEM_CB_TABLE_INIT_MONITOR_SIZE)
#error DEM_CB_TABLE_INIT_MONITOR_SIZE already defined
#endif
/** \brief Size of the generic callback function pointer table for InitMonitor */
#define DEM_CB_TABLE_INIT_MONITOR_SIZE [!"num:integer($GenericCtr)"!]U

#if (defined DEM_LOOKUP_TABLE_INIT_MONITOR_SIZE)
#error DEM_LOOKUP_TABLE_INIT_MONITOR_SIZE already defined
#endif
/** \brief Size of the generic lookup table for InitMonitor */
#define DEM_LOOKUP_TABLE_INIT_MONITOR_SIZE [!"num:integer($GenericCtr)"!]U

#if (defined DEM_USE_CB_INIT_MONITOR)
#error DEM_USE_CB_INIT_MONITOR already defined
#endif
/** \brief Macro for Enabling/Disabling init monitor callback functionality */
#if (DEM_CB_TABLE_INIT_MONITOR_SIZE > 0U)
#define DEM_USE_CB_INIT_MONITOR STD_ON
#else
#define DEM_USE_CB_INIT_MONITOR STD_OFF
#endif

[!VAR "CbNameList" = "'#'"!]
[!VAR "CCbCtr" = "0"!]
[!VAR "CLookupCtr" = "0"!]
[!VAR "RteCtr" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackEventStatusChanged/*"!]
  [!IF "not(node:exists(DemCallbackEventStatusChangedFnc))"!]
    [!VAR "RteCtr" = "$RteCtr + 1"!]
  [!ELSE!]
    [!VAR "CLookupCtr" = "$CLookupCtr + 1"!]
    [!IF "not(contains($CbNameList, concat('#', DemCallbackEventStatusChangedFnc, '#')))"!]
      [!VAR "CCbCtr" = "$CCbCtr + 1"!]
      [!VAR "CbNameList" = "concat($CbNameList, DemCallbackEventStatusChangedFnc, '#')"!]
    [!ENDIF!]
  [!ENDIF!]
[!ENDLOOP!]

#if (defined DEM_C_CALLBACK_TABLE_TRIG_ON_EVST_SIZE)
#error DEM_C_CALLBACK_TABLE_TRIG_ON_EVST_SIZE already defined
#endif
/** \brief Size of the C-callback function pointer table for TriggerOnEventStatus */
#define DEM_C_CALLBACK_TABLE_TRIG_ON_EVST_SIZE [!"num:integer($CCbCtr)"!]U

#if (defined DEM_RTE_CALLBACK_TABLE_TRIG_ON_EVST_SIZE)
#error DEM_RTE_CALLBACK_TABLE_TRIG_ON_EVST_SIZE already defined
#endif
/** \brief Size of the RTE-callback function pointer table for TriggerOnEventStatus */
#define DEM_RTE_CALLBACK_TABLE_TRIG_ON_EVST_SIZE [!"num:integer($RteCtr)"!]U

#if (defined DEM_C_LOOKUP_TABLE_TRIG_ON_EVST_SIZE)
#error DEM_C_LOOKUP_TABLE_TRIG_ON_EVST_SIZE already defined
#endif
/** \brief Size of the C-lookup table for TriggerOnEventStatus */
#define DEM_C_LOOKUP_TABLE_TRIG_ON_EVST_SIZE [!"num:integer($CLookupCtr)"!]U

#if (defined DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVST_SIZE)
#error DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVST_SIZE already defined
#endif
/** \brief Size of the RTE-lookup table for TriggerOnEventStatus */
#define DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVST_SIZE [!"num:integer($RteCtr)"!]U

#if (defined DEM_USE_CB_TRIG_ON_EVST)
#error DEM_USE_CB_TRIG_ON_EVST already defined
#endif
/** \brief Macro for Enabling/Disabling trigger on event status callback
 ** functionality */
[!IF "(($RteCtr > 0) or ($CCbCtr > 0) or (DemGeneral/DemTriggerFiMReports = 'true'))"!]
#define DEM_USE_CB_TRIG_ON_EVST STD_ON
[!ELSE!]
#define DEM_USE_CB_TRIG_ON_EVST STD_OFF
[!ENDIF!]

[!VAR "GenericCtr" = "count(DemGeneral/DemCallbackDTCStatusChanged/*)"!]

#if (defined DEM_CB_TABLE_TRIG_ON_DTCST_SIZE)
#error DEM_CB_TABLE_TRIG_ON_DTCST_SIZE already defined
#endif
/** \brief Size of the C-callback function pointer table for TriggerOnDTCEventStatus */
#define DEM_CB_TABLE_TRIG_ON_DTCST_SIZE [!"num:integer($GenericCtr)"!]U

#if (defined DEM_USE_CB_TRIG_ON_DTCST)
#error DEM_USE_CB_TRIG_ON_EVST already defined
#endif
/** \brief Macro for Enabling/Disabling trigger on DTC event status callback
 ** functionality */
[!IF "(($GenericCtr > 0) or (DemGeneral/DemTriggerDcmReports = 'true'))"!]
#define DEM_USE_CB_TRIG_ON_DTCST STD_ON
[!ELSE!]
#define DEM_USE_CB_TRIG_ON_DTCST STD_OFF
[!ENDIF!]

[!VAR "CCbCtr" = "0"!]
[!VAR "CLookupCtr" = "0"!]
[!VAR "RteCtr" = "0"!]
[!VAR "CbNameList" = "'#'"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackEventDataChanged"!]
  [!IF "not(node:exists(DemCallbackEventDataChangedFnc))"!]
    [!VAR "RteCtr" = "$RteCtr + 1"!]
  [!ELSE!]
    [!VAR "CLookupCtr" = "$CLookupCtr + 1"!]
    [!IF "not(contains($CbNameList, concat('#', DemCallbackEventDataChangedFnc, '#')))"!]
      [!VAR "CCbCtr" = "$CCbCtr + 1"!]
      [!VAR "CbNameList" = "concat($CbNameList, DemCallbackEventDataChangedFnc, '#')"!]
    [!ENDIF!]
  [!ENDIF!]
[!ENDLOOP!]

#if (defined DEM_C_CB_TABLE_TRIG_ON_EVDAT_SIZE )
#error DEM_C_CB_TABLE_TRIG_ON_EVDAT_SIZE  already defined
#endif
/** \brief Size of the C-callback function pointer table for TriggerOnEventDataChanged */
#define DEM_C_CB_TABLE_TRIG_ON_EVDAT_SIZE  [!"num:integer($CCbCtr)"!]U

#if (defined DEM_RTE_CB_TABLE_TRIG_ON_EVDAT_SIZE)
#error DEM_RTE_CB_TABLE_TRIG_ON_EVDAT_SIZE already defined
#endif
/** \brief Size of the RTE-callback function pointer table for TriggerOnEventDataChanged */
#define DEM_RTE_CB_TABLE_TRIG_ON_EVDAT_SIZE [!"num:integer($RteCtr)"!]U

#if (defined DEM_C_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE)
#error DEM_C_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE already defined
#endif
/** \brief Size of the C-lookup table for TriggerOnEventDataChanged */
#define DEM_C_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE [!"num:integer($CLookupCtr)"!]U

#if (defined DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE)
#error DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE already defined
#endif
/** \brief Size of the RTE-lookup table for TriggerOnEventDataChanged */
#define DEM_RTE_LOOKUP_TABLE_TRIG_ON_EVDAT_SIZE [!"num:integer($RteCtr)"!]U

#if (defined DEM_USE_CB_TRIG_ON_EVDAT)
#error DEM_USE_CB_TRIG_ON_EVDAT already defined
#endif
/** \brief Macro for Enabling/Disabling trigger on event data changed callback functionality */
#if ( (DEM_RTE_CB_TABLE_TRIG_ON_EVDAT_SIZE > 0U) || \
      (DEM_C_CB_TABLE_TRIG_ON_EVDAT_SIZE > 0U) )
#define DEM_USE_CB_TRIG_ON_EVDAT STD_ON
#else
#define DEM_USE_CB_TRIG_ON_EVDAT STD_OFF
#endif

[!VAR "CCbCtr" = "0"!]
[!VAR "CLookupCtr" = "0"!]
[!VAR "RteCtr" = "0"!]
[!VAR "CbNameList" = "'#'"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemCallbackClearEventAllowed"!]
  [!IF "not(node:exists(DemCallbackClearEventAllowedFnc))"!]
    [!VAR "RteCtr" = "$RteCtr + 1"!]
  [!ELSE!]
    [!VAR "CLookupCtr" = "$CLookupCtr + 1"!]
    [!IF "not(contains($CbNameList, concat('#', DemCallbackClearEventAllowedFnc, '#')))"!]
      [!VAR "CCbCtr" = "$CCbCtr + 1"!]
      [!VAR "CbNameList" = "concat($CbNameList, DemCallbackClearEventAllowedFnc, '#')"!]
    [!ENDIF!]
  [!ENDIF!]
[!ENDLOOP!]

#if (defined DEM_C_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE )
#error DEM_C_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE  already defined
#endif
/** \brief Size of the C-callback function pointer table for ClearEventAllowed */
#define DEM_C_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE  [!"num:integer($CCbCtr)"!]U

#if (defined DEM_RTE_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE)
#error DEM_RTE_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE already defined
#endif
/** \brief Size of the RTE-callback function pointer table for ClearEventAllowed */
#define DEM_RTE_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE [!"num:integer($RteCtr)"!]U

#if (defined DEM_C_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE)
#error DEM_C_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE already defined
#endif
/** \brief Size of the C-lookup table for ClearEventAllowed */
#define DEM_C_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE [!"num:integer($CLookupCtr)"!]U

#if (defined DEM_RTE_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE)
#error DEM_RTE_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE already defined
#endif
/** \brief Size of the RTE-lookup table for ClearEventAllowed */
#define DEM_RTE_LOOKUP_TABLE_CLEAR_EVENT_ALLOWED_SIZE [!"num:integer($RteCtr)"!]U

#if (defined DEM_USE_CB_CLEAR_EVENT_ALLOWED)
#error DEM_USE_CB_CLEAR_EVENT_ALLOWED already defined
#endif
/** \brief Macro for Enabling/Disabling ClearEventAllowed callback functionality */
#if ( (DEM_RTE_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE > 0U) || \
      (DEM_C_CB_TABLE_CLEAR_EVENT_ALLOWED_SIZE > 0U) )
#define DEM_USE_CB_CLEAR_EVENT_ALLOWED STD_ON
#else
#define DEM_USE_CB_CLEAR_EVENT_ALLOWED STD_OFF
#endif

[!VAR "GenericCtr" = "count(DemConfigSet/*/DemEventParameter/*/DemEventClass/DemDebounceAlgorithmClass[. = 'DemDebounceMonitorInternal']/DemCallbackGetFDC)"!]

#if (defined DEM_CB_TABLE_GET_FDC_SIZE)
#error DEM_CB_TABLE_GET_FDC_SIZE already defined
#endif
/** \brief Size of the generic callback function pointer table for GetFaultDetectionCounter */
#define DEM_CB_TABLE_GET_FDC_SIZE [!"num:integer($GenericCtr)"!]U

#if (defined DEM_LOOKUP_TABLE_GET_FDC_SIZE)
#error DEM_LOOKUP_TABLE_GET_FDC_SIZE already defined
#endif
/** \brief Size of the generic lookup table for GetFaultDetectionCounter */
#define DEM_LOOKUP_TABLE_GET_FDC_SIZE [!"num:integer($GenericCtr)"!]U

#if (defined DEM_USE_CB_GET_FDC)
#error DEM_USE_CB_GET_FDC already defined
#endif
/** \brief Macro for Enabling/Disabling fault detection counter callback
 ** functionality */
#if (DEM_CB_TABLE_GET_FDC_SIZE > 0U)
#define DEM_USE_CB_GET_FDC STD_ON
#else
#define DEM_USE_CB_GET_FDC STD_OFF
#endif

/*------------------[DTC groups configuration]------------------------------*/

/* Symbolic names of internal DTC group indices */
[!VAR "GroupIdx" = "0"!]
[!LOOP "node:order(DemGeneral/DemGroupOfDTC/*, './DemGroupDTCs')"!]
  [!VAR "GroupName" = "substring(name(.), 15)"!]
  [!INDENT "0"!]

    #if (defined DEM_DTCGRP_IDX_[!"$GroupName"!])
    #error DEM_DTCGRP_IDX_[!"$GroupName"!] already defined
    #endif
    /** \brief Symbolic name of DTC group [!"$GroupName"!] index */
    #define DEM_DTCGRP_IDX_[!"$GroupName"!][!CALL "Indent", "Length" = "31 - string-length($GroupName)"!] [!"num:integer($GroupIdx)"!]U
    [!VAR "GroupIdx" = "$GroupIdx + 1"!]
  [!ENDINDENT!]
[!ENDLOOP!]

#if (defined DEM_DTCGRP_IDX_NO_DTC)
#error DEM_DTCGRP_IDX_NO_DTC already defined
#endif
/** \brief Symbolic name of DTC group index for events without a DTC */
#define DEM_DTCGRP_IDX_NO_DTC  (DEM_DTCGRP_IDX_[!"$GroupName"!] + 1U)

#if (defined DEM_DTCGRP_IDX_INVALID)
#error DEM_DTCGRP_IDX_INVALID already defined
#endif
/** \brief Symbolic name of invalid DTC group index */
#define DEM_DTCGRP_IDX_INVALID  (DEM_DTCGRP_IDX_NO_DTC + 1U)

#if (defined DEM_NUM_DTC_GROUPS)
#error DEM_NUM_DTC_GROUPS already defined
#endif
/** \brief Number of DTC groups */
#define DEM_NUM_DTC_GROUPS [!"num:integer(count(DemGeneral/DemGroupOfDTC/*))"!]U

/*------------------[Freeze frame configuration]----------------------------*/
[!//
[!VAR "MaxFFSize" = "0"!]
[!LOOP "DemGeneral/DemFreezeFrameClass/*"!]
  [!VAR "FFSize" = "0"!]
  [!LOOP "DemDidClassRef/*"!]
    [!LOOP "as:ref(.)/DemDidDataElementClassRef/*"!]
      [!VAR "FFSize" = "$FFSize + as:ref(.)/DemDataElementDataSize"!]
    [!ENDLOOP!]
  [!ENDLOOP!]
  [!WS "0"!]/* Size of the freeze frame class [!"name(.)"!] in bytes: [!"num:integer($FFSize)"!] */
  [!IF "$FFSize > 255"!]
    [!ERROR!]The size of freeze frame class [!"node:name(.)"!] is greater than 255 ([!"num:integer($FFSize)"!]), but the API Dem_GetEventFreezeFrameData() cannot handle this size. Remove DataIds from the freeze frame class.[!ENDERROR!]
  [!ENDIF!]
  [!IF "$MaxFFSize < $FFSize"!]
    [!VAR "MaxFFSize" = "$FFSize"!]
  [!ENDIF!]
[!ENDLOOP!]

#if (defined DEM_MAXSIZE_FFCLS)
#error DEM_MAXSIZE_FFCLS already defined
#endif
/** \brief Maximum size of a freeze frame class in bytes */
#define DEM_MAXSIZE_FFCLS [!"num:integer($MaxFFSize)"!]U

/* Symblic names of internal freeze frame class indices */
[!LOOP "DemGeneral/DemFreezeFrameClass/*"!]
  [!INDENT "0"!]
    #if (defined DEM_FFCLS_IDX_[!"name(.)"!])
    #error DEM_FFCLS_IDX_[!"name(.)"!] already defined
    #endif
    /** \brief Symbolic name of freeze frame class [!"name(.)"!] index */
    #define DEM_FFCLS_IDX_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"num:integer(@index)"!]U
  [!ENDINDENT!]
[!ENDLOOP!]
[!//
[!VAR "NumFFCls" = "count(DemGeneral/DemFreezeFrameClass/*)"!]
[!IF "count(DemConfigSet/*/DemEventParameter/*[not(node:exists(DemFreezeFrameClassRef))]) > 0"!]
  [!INDENT "0"!]

    #if (defined DEM_FFCLS_NULL_IDX)
    #error DEM_FFCLS_NULL_IDX already defined
    #endif
    /** \brief Symbolic name of freeze frame class null index */
    #define DEM_FFCLS_NULL_IDX                             [!"num:integer($NumFFCls)"!]U
    [!VAR "NumFFCls" = "$NumFFCls + 1"!]
  [!ENDINDENT!]
[!ENDIF!]


#if (defined DEM_NUM_FFCLS)
#error DEM_NUM_FFCLS already defined
#endif
/** \brief Number of different freeze frame classes */
#define DEM_NUM_FFCLS [!"num:integer($NumFFCls)"!]U

#if (defined DEM_NUM_FFSEGS)
#error DEM_NUM_FFSEGS already defined
#endif
/** \brief Number of different freeze frame segments */
#define DEM_NUM_FFSEGS [!"num:integer(count(DemGeneral/DemDidClass/*))"!]U

/*------------------[Freeze frame record number configuration]--------------*/
[!//
[!IF "DemGeneral/DemTypeOfFreezeFrameRecordNumeration = 'DEM_FF_RECNUM_CONFIGURED' and count(DemConfigSet/*/DemEventParameter/*[node:exists(DemFreezeFrameClassRef)]) > 0"!]

/* Symblic names of internal freeze frame record number class indices */
  [!INDENT "0"!]
    [!LOOP "DemGeneral/DemFreezeFrameRecNumClass/*"!]
      #if (defined DEM_FFRECNUMCLS_IDX_[!"name(.)"!])
      #error DEM_FFRECNUMCLS_IDX_[!"name(.)"!] already defined
      #endif
      /** \brief Symbolic name of freeze frame record number class [!"name(.)"!] index */
      #define DEM_FFRECNUMCLS_IDX_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"num:integer(@index)"!]U
    [!ENDLOOP!]

    #if (defined DEM_NUM_FFRECNUMCLS)
    #error DEM_NUM_FFRECNUMCLS already defined
    #endif
    /** \brief Number of different freeze frame record number classes */
    #define DEM_NUM_FFRECNUMCLS [!"num:integer(count(DemGeneral/DemFreezeFrameRecNumClass/*))"!]U
  [!ENDINDENT!]
[!ENDIF!]

/*------------------[Prestored freeze frame configuration]------------------*/

#if (defined DEM_PFF_ZERO_END)
#error DEM_PFF_ZERO_END already defined
#endif
/** \brief Zero value to be added to each size, whose involved patterns could
 ** be none */
#define DEM_PFF_ZERO_END                  0U

#if (defined DEM_SIZE_PFF_ENTRY_DATA)
#error DEM_SIZE_PFF_ENTRY_DATA already defined
#endif
/** \brief Size of dynamic PFF entry data
 **
 ** All FF sizes of events with enabled prestorage are added.
 **/
#define DEM_SIZE_PFF_ENTRY_DATA \
  ([!//
[!LOOP "DemConfigSet/*/DemEventParameter/*[DemEventClass/DemFFPrestorageSupported = 'true']"!]
  [!VAR "FFSize" = "0"!]
  [!LOOP "as:ref(DemFreezeFrameClassRef)/DemDidClassRef/*"!]
    [!LOOP "as:ref(.)/DemDidDataElementClassRef/*"!]
      [!VAR "FFSize" = "$FFSize + as:ref(.)/DemDataElementDataSize"!]
    [!ENDLOOP!]
  [!ENDLOOP!]
  [!WS "0"!][!"num:integer($FFSize)"!]U + [!//
[!ENDLOOP!]
DEM_PFF_ZERO_END)

/*------------------[Extended data configuration]---------------------------*/
[!//
[!VAR "MaxEDSize" = "0"!]
[!VAR "MaxEDStartByte" = "0"!]
[!LOOP "DemGeneral/DemExtendedDataClass/*"!]
  [!VAR "EDSize" = "0"!]
  [!LOOP "DemExtendedDataRecordClassRef/*"!]
    [!IF "node:islast()"!][!/* last element is omitted for start byte calculation */!][!//
      [!IF "$MaxEDStartByte < $EDSize"!]
        [!VAR "MaxEDStartByte" = "$EDSize"!]
      [!ENDIF!]
    [!ENDIF!]
    [!LOOP "as:ref(.)/DemDataElementClassRef/*"!]
      [!VAR "EDSize" = "$EDSize + as:ref(.)/DemDataElementDataSize"!]
    [!ENDLOOP!]
  [!ENDLOOP!]
  [!WS "0"!]/* Size of the extended data class [!"name(.)"!] in bytes: [!"num:integer($EDSize)"!] */
  [!IF "$EDSize > 255"!]
    [!ERROR!]The size of extended data class [!"node:name(.)"!] is greater than 255 ([!"num:integer($EDSize)"!]), but the API Dem_GetEventExtendedDataRecord() cannot handle this size. Remove extended data record from the extended data class.[!ENDERROR!]
  [!ENDIF!]
  [!IF "$MaxEDSize < $EDSize"!]
    [!VAR "MaxEDSize" = "$EDSize"!]
  [!ENDIF!]
[!ENDLOOP!]

#if (defined DEM_MAXSIZE_EDCLS)
#error DEM_MAXSIZE_EDCLS already defined
#endif
/** \brief Maximum size of a extended data class in bytes */
#define DEM_MAXSIZE_EDCLS [!"num:integer($MaxEDSize)"!]U

/* Symbolic names of internal extended data class indices */
[!LOOP "DemGeneral/DemExtendedDataClass/*"!]
  [!INDENT "0"!]

    #if (defined DEM_EDCLS_IDX_[!"name(.)"!])
    #error DEM_EDCLS_IDX_[!"name(.)"!] already defined
    #endif
    /** \brief Symbolic name of extended data class [!"name(.)"!] index */
    #define DEM_EDCLS_IDX_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"num:integer(@index)"!]U
  [!ENDINDENT!]
[!ENDLOOP!]
[!//
[!VAR "NumEDCls" = "count(DemGeneral/DemExtendedDataClass/*)"!]
[!IF "count(DemConfigSet/*/DemEventParameter/*[not(node:exists(DemExtendedDataClassRef))]) > 0"!]
  [!INDENT "0"!]

    #if (defined DEM_EDCLS_NULL_IDX)
    #error DEM_EDCLS_NULL_IDX already defined
    #endif
    /** \brief Symbolic name of extended data class null index */
    #define DEM_EDCLS_NULL_IDX                             [!"num:integer($NumEDCls)"!]U
  [!ENDINDENT!]
  [!VAR "NumEDCls" = "$NumEDCls + 1"!]
[!ENDIF!]

#if (defined DEM_NUM_EDCLS)
#error DEM_NUM_EDCLS already defined
#endif
/** \brief Number of different extended data classes */
#define DEM_NUM_EDCLS [!"num:integer($NumEDCls)"!]U

#if (defined DEM_NUM_EDSEGS)
#error DEM_NUM_EDSEGS already defined
#endif
/** \brief Number of different extended data segments */
#define DEM_NUM_EDSEGS [!"num:integer(count(DemGeneral/DemExtendedDataRecordClass/*))"!]U

/*------------------[Data elements configuration]---------------------------*/

#if (defined DEM_NUM_EXT_CS_DATAELEMENTS)
#error DEM_NUM_EXT_CS_DATAELEMENTS already defined
#endif
/** \brief Number of different data elements of type DemExternalCSDataElementClass */
#define DEM_NUM_EXT_CS_DATAELEMENTS [!"num:integer(count(DemGeneral/DemDataElementClass/*[. = 'DemExternalCSDataElementClass']))"!]U

#if (defined DEM_INT_VAL_IDX_OCCCTR)
#error DEM_INT_VAL_IDX_OCCCTR already defined
#endif
/** \brief Macro for internal data element 'Occurence counter' */
#define DEM_INT_VAL_IDX_OCCCTR    DEM_NUM_EXT_CS_DATAELEMENTS

#if (defined DEM_INT_VAL_IDX_AGECTR)
#error DEM_INT_VAL_IDX_AGECTR already defined
#endif
/** \brief Macro for internal data element 'Aging counter' */
#define DEM_INT_VAL_IDX_AGECTR    (DEM_NUM_EXT_CS_DATAELEMENTS + 1U)

#if (defined DEM_INT_VAL_IDX_OVFIND)
#error DEM_INT_VAL_IDX_OVFIND already defined
#endif
/** \brief Macro for internal data element 'Overflow indication' */
#define DEM_INT_VAL_IDX_OVFIND    (DEM_NUM_EXT_CS_DATAELEMENTS + 2U)

#if (defined DEM_INT_VAL_IDX_EVSIGNIF)
#error DEM_INT_VAL_IDX_EVSIGNIF already defined
#endif
/** \brief Macro for internal data element 'Event significance' */
#define DEM_INT_VAL_IDX_EVSIGNIF  (DEM_NUM_EXT_CS_DATAELEMENTS + 3U)

/*------------------[Events configuration]----------------------------------*/
[!//
[!MACRO "LOG", "PNumber"!]
  [!INDENT "0"!]
    [!IF "$PNumber < 0"!]
      0[!ELSEIF "$PNumber < 2"!]
      1[!ELSEIF "$PNumber < 4"!]
      2[!ELSEIF "$PNumber < 8"!]
      3[!ELSEIF "$PNumber < 16"!]
      4[!ELSEIF "$PNumber < 32"!]
      5[!ELSEIF "$PNumber < 64"!]
      6[!ELSEIF "$PNumber < 128"!]
      7[!ELSEIF "$PNumber < 256"!]
      8[!ELSEIF "$PNumber < 512"!]
      9[!ELSEIF "$PNumber < 1024"!]
      10[!ELSEIF "$PNumber < 2048"!]
      11[!ELSE!]
      [!ERROR "Unexpected condition in DEM 'LOG' macro. Given number is too big!"!]
    [!ENDIF!]
  [!ENDINDENT!]
[!ENDMACRO!]

/* Static value width in bits used for Dem_EventDescType */

#if (defined DEM_UDS_DTC_WIDTH)
#error DEM_UDS_DTC_WIDTH already defined
#endif
/** \brief Number of bits to hold the value of DTC */
#define DEM_UDS_DTC_WIDTH              24U

#if (defined DEM_DTCORIGIN_WIDTH)
#error DEM_DTCORIGIN_WIDTH already defined
#endif
/** \brief Number of bits to hold the value of DTC origin */
#define DEM_DTCORIGIN_WIDTH            2U

#if (defined DEM_DTCGROUP_WIDTH)
#error DEM_DTCGROUP_WIDTH already defined
#endif
/** \brief Number of bits to hold the value of DTC group */
#define DEM_DTCGROUP_WIDTH             5U

#if (defined DEM_IMMEDIATESTORAGE_WIDTH)
#error DEM_IMMEDIATESTORAGE_WIDTH already defined
#endif
/** \brief Number of bits to hold the status of Immediate storage */
#define DEM_IMMEDIATESTORAGE_WIDTH     1U

#if (defined DEM_DTCSEVERITY_WIDTH)
#error DEM_DTCSEVERITY_WIDTH already defined
#endif
/** \brief Number of bits to hold the value of DTC severity */
#define DEM_DTCSEVERITY_WIDTH          2U

#if (defined DEM_ENABLEPRESTORAGE_WIDTH)
#error DEM_ENABLEPRESTORAGE_WIDTH already defined
#endif
/** \brief Number of bits to hold the status of Prestorage */
#define DEM_ENABLEPRESTORAGE_WIDTH     1U

#if (defined DEM_DEBOUNCEALGO_WIDTH)
#error DEM_DEBOUNCEALGO_WIDTH already defined
#endif
/** \brief Number of bits to hold the value of Debounce algorithm */
#define DEM_DEBOUNCEALGO_WIDTH         2U

/* Configurational value width in bits used for Dem_EventDescType */

#if (defined DEM_FFCLASSIDX_WIDTH)
#error DEM_FFCLASSIDX_WIDTH already defined
#endif
/** \brief Number of bits to hold the value of Freeze frame class index */
#define DEM_FFCLASSIDX_WIDTH[!"'           '"!][!CALL "LOG", "PNumber" = "count(DemGeneral/DemFreezeFrameClass/*)"!]U

#if (defined DEM_FFRECINFO_WIDTH)
#error DEM_FFRECINFO_WIDTH already defined
#endif
[!VAR "FFRecInfo" = "0"!]
[!INDENT "0"!]
  [!IF "DemGeneral/DemTypeOfFreezeFrameRecordNumeration = 'DEM_FF_RECNUM_CALCULATED'"!]
    [!LOOP "DemConfigSet/*/DemEventParameter/*"!]
      [!IF "$FFRecInfo < DemMaxNumberFreezeFrameRecords"!]
        [!VAR "FFRecInfo" = "DemMaxNumberFreezeFrameRecords"!]
      [!ENDIF!]
    [!ENDLOOP!]
    /** \brief Number of bits to hold the value of maximum number of freeze frames */
  [!ELSE!]
    [!VAR "FFRecInfo" = "count(DemGeneral/DemFreezeFrameRecNumClass/*)"!]
    /** \brief Number of bits to hold the value of Freeze frame record numeration class index */
  [!ENDIF!]
[!ENDINDENT!]
#define DEM_FFRECINFO_WIDTH[!"'            '"!][!CALL "LOG", "PNumber" = "num:integer($FFRecInfo)"!]U

#if (defined DEM_EDCLASSIDX_WIDTH)
#error DEM_EDCLASSIDX_WIDTH already defined
#endif
/** \brief Number of bits to hold the value of Extended data class index */
#define DEM_EDCLASSIDX_WIDTH[!"'           '"!][!CALL "LOG", "PNumber" = "count(DemGeneral/DemExtendedDataClass/*)"!]U

#if (defined DEM_OPCYCLEIDX_WIDTH)
#error DEM_OPCYCLEIDX_WIDTH already defined
#endif
[!VAR "CycNameList" = "'#'"!]
[!VAR "OpCycCtr"  = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass/DemOperationCycleRef"!]
  [!IF "not(contains($CycNameList, concat('#', name(as:ref(.)), '#')))"!]
    [!VAR "OpCycCtr" = "$OpCycCtr + 1"!]
    [!VAR "CycNameList" = "concat($CycNameList, name(as:ref(.)), '#')"!]
  [!ENDIF!]
[!ENDLOOP!]
/** \brief Number of bits to hold the value of Operation cycele index */
#define DEM_OPCYCLEIDX_WIDTH[!"'           '"!][!CALL "LOG", "PNumber" = "num:integer($OpCycCtr)"!]U

#if (defined DEM_AGINGCYCLES_WIDTH)
#error DEM_AGINGCYCLES_WIDTH already defined
#endif
[!VAR "MaxAgeCyc" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass[DemAgingAllowed = 'true']"!]
  [!IF "$MaxAgeCyc < DemAgingCycleCounterThreshold"!]
    [!VAR "MaxAgeCyc" = "DemAgingCycleCounterThreshold"!]
  [!ENDIF!]
[!ENDLOOP!]
/** \brief Number of bits to hold the value of number of Aging cycles */
#define DEM_AGINGCYCLES_WIDTH[!"'          '"!][!CALL "LOG", "PNumber" = "$MaxAgeCyc"!]U

#if (defined DEM_EVENTKIND_WIDTH)
#error DEM_EVENTKIND_WIDTH already defined
#endif
/** \brief Number of bits to hold the value of Event kind */
#define DEM_EVENTKIND_WIDTH            1U

#if (defined DEM_EVENTFAILURECLASSIDX_WIDTH)
#error DEM_EVENTFAILURECLASSIDX_WIDTH already defined
#endif
[!/* create unique list of all configured failure cycle counter cycles and thresholds */!][!//
[!VAR "FailureCycleCfgList" = "'#'"!]
[!VAR "EventFailureClassIdx" = "0"!]
[!LOOP "(DemConfigSet/*/DemEventParameter/*[node:exists(./DemEventClass/DemEventFailureCycleRef)])"!]
  [!VAR "EventFailureCycle" = "node:name(as:ref(DemEventClass/DemEventFailureCycleRef))"!]
  [!VAR "EventFailureThreshold" = "node:value(DemEventClass/DemEventFailureCycleCounterThreshold)"!]
  [!IF "not(contains($FailureCycleCfgList, concat('#', $EventFailureCycle, '*', $EventFailureThreshold, '#')))"!]
[!/* add non-existing cycle counter cycle and threshold to the list */!][!//
    [!VAR "FailureCycleCfgList" = "concat($FailureCycleCfgList, $EventFailureCycle, '*', $EventFailureThreshold, '#')"!]
[!/* increment counter of unique combinations */!][!//
    [!VAR "EventFailureClassIdx" = "$EventFailureClassIdx + 1"!]
  [!ENDIF!]
[!ENDLOOP!]
/** \brief Number of bits to hold the value of Event failure class index */
#define DEM_EVENTFAILURECLASSIDX_WIDTH[!"' '"!][!CALL "LOG", "PNumber" = "$EventFailureClassIdx"!]U

#if (defined DEM_ENCONDIDX_WIDTH)
#error DEM_ENCONDIDX_WIDTH already defined
#endif
[!VAR "NumEnCond" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass[node:exists(DemEnableConditionGroupRef)]"!]
  [!VAR "NumEnCond" = "$NumEnCond + count(as:ref(DemEnableConditionGroupRef)/DemEnableConditionRef/*)"!]
[!ENDLOOP!]
/** \brief Number of bits to hold the value of Enable condition index */
#define DEM_ENCONDIDX_WIDTH[!"'            '"!][!CALL "LOG", "PNumber" = "$NumEnCond"!]U

#if (defined DEM_NUMENCOND_WIDTH)
#error DEM_NUMENCOND_WIDTH already defined
#endif
[!VAR "MaxNumEnCondLinks" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass[node:exists(DemEnableConditionGroupRef)]"!]
  [!IF "$MaxNumEnCondLinks < count(as:ref(DemEnableConditionGroupRef)/DemEnableConditionRef/*)"!]
    [!VAR "MaxNumEnCondLinks" = "count(as:ref(DemEnableConditionGroupRef)/DemEnableConditionRef/*)"!]
  [!ENDIF!]
[!ENDLOOP!]
/** \brief Number of bits to hold the value of number of enable conditions */
#define DEM_NUMENCOND_WIDTH[!"'            '"!][!CALL "LOG", "PNumber" = "$MaxNumEnCondLinks"!]U

#if (defined DEM_INDICATORUSED_WIDTH)
#error DEM_INDICATORUSED_WIDTH already defined
#endif
/** \brief Number of bits to hold the status of Indicators */
#define DEM_INDICATORUSED_WIDTH        1U

#if (defined DEM_PRIORITY_WIDTH)
#error DEM_PRIORITY_WIDTH already defined
#endif
[!VAR "NumPriorities" = "0"!]
[!IF "DemGeneral/DemEventDisplacementSupport = 'true'"!]
[!/* create unique list of all configured priority values */!][!//
  [!VAR "PriorityValueList" = "'#'"!]
  [!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass"!]
    [!IF "not(contains($PriorityValueList, concat('#', DemEventPriority, '#')))"!]
      [!VAR "PriorityValueList" = "concat($PriorityValueList, DemEventPriority, '#')"!]
    [!ENDIF!]
  [!ENDLOOP!]
[!/* count number of different priorities used */!][!//
  [!VAR "NumPriorities" = "count(text:split($PriorityValueList, '#'))"!]
[!ENDIF!]
[!/* negative number will lead to WIDTH of zero */!][!//
/** \brief Number of bits to hold the value of Internal priority */
#define DEM_PRIORITY_WIDTH[!"'             '"!][!CALL "LOG", "PNumber" = "$NumPriorities - 1"!]U

#if (defined DEM_DEBOUNCEIDX_WIDTH)
#error DEM_DEBOUNCEIDX_WIDTH already defined
#endif
[!VAR "MaxDebIdx" = "count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceCounterBased'])"!]
[!VAR "MaxTimeDebIdx" = "count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceTimeBase'])"!]
[!IF "$MaxDebIdx < $MaxTimeDebIdx"!]
  [!VAR "MaxDebIdx" = "$MaxTimeDebIdx"!]
[!ENDIF!]
[!VAR "MaxFreqDebIdx" = "count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceFrequencyBased'])"!]
[!IF "$MaxDebIdx < $MaxFreqDebIdx"!]
  [!VAR "MaxDebIdx" = "$MaxFreqDebIdx"!]
[!ENDIF!]
/** \brief Number of bits to hold the value of Debounce index */
#define DEM_DEBOUNCEIDX_WIDTH[!"'          '"!][!CALL "LOG", "PNumber" = "$MaxDebIdx"!]U

#if (defined DEM_DTCFUNCTIONALUNIT_WIDTH)
#error DEM_DTCFUNCTIONALUNIT_WIDTH already defined
#endif
[!VAR "MaxDTCFuncUnit" = "0"!]
[!LOOP "DemConfigSet/*/DemDTCClass/*"!]
  [!IF "$MaxDTCFuncUnit < DemDTCFunctionalUnit"!]
    [!VAR "MaxDTCFuncUnit" = "DemDTCFunctionalUnit"!]
  [!ENDIF!]
[!ENDLOOP!]
/** \brief Number of bits to hold the value of DTC functional unit */
#define DEM_DTCFUNCTIONALUNIT_WIDTH[!"'    '"!][!CALL "LOG", "PNumber" = "$MaxDTCFuncUnit"!]U

#if (defined DEM_AGINGCYCLEIDX_WIDTH)
#error DEM_AGINGCYCLEIDX_WIDTH already defined
#endif
[!VAR "CycNameList" = "'#'"!]
[!VAR "AgeCycCtr" = "1"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass/DemAgingCycleRef"!]
  [!IF "not(contains($CycNameList, concat('#', name(as:ref(.)), '#')))"!]
    [!VAR "AgeCycCtr" = "$AgeCycCtr + 1"!]
    [!VAR "CycNameList" = "concat($CycNameList, name(as:ref(.)), '#')"!]
  [!ENDIF!]
[!ENDLOOP!]
/** \brief Number of bits to hold the value of relative Aging cycle index
 *
 * The invalid value :: DEM_NUM_AGINGCYCLES is also included in the width. */
#define DEM_AGINGCYCLEIDX_WIDTH[!"'        '"!][!CALL "LOG", "PNumber" = "num:integer($AgeCycCtr)"!]U

/*------------------[Warning indicators configuration]----------------------*/

[!VAR "HealList"="'#'"!]
[!VAR "HealCtr"="0"!][!/* number of unique healing cycle and threshold pair */!][!//
[!LOOP "DemConfigSet/*[1]/DemEventParameter/*"!]
  [!LOOP "./DemEventClass/DemIndicatorAttribute/*"!]
    [!IF "not(contains($HealList, concat('#', node:name(as:ref(./DemIndicatorHealingCycleRef)),'*',node:value(./DemIndicatorHealingCycleCounterThreshold), '#')))"!]
        [!VAR "HealList" = "concat($HealList, node:name(as:ref(./DemIndicatorHealingCycleRef)),'*',node:value(./DemIndicatorHealingCycleCounterThreshold), '#')"!]
    [!VAR "HealCtr" = "$HealCtr + 1"!]
    [!ENDIF!]
  [!ENDLOOP!]
[!ENDLOOP!]
#if (defined DEM_NUM_HEALINGCYC_COUNTER_INFO_ELEMENTS)
#error DEM_NUM_HEALINGCYC_COUNTER_INFO_ELEMENTS already defined
#endif
/** \brief Number of healing cycle-counter information elements */
#define DEM_NUM_HEALINGCYC_COUNTER_INFO_ELEMENTS [!"num:integer($HealCtr)"!]

[!VAR "FailList"="'#'"!]
[!VAR "FailCtr"="0"!][!/* number of unique failure cycle and threshold pair */!][!//
[!LOOP "DemConfigSet/*[1]/DemEventParameter/*"!]
  [!LOOP "./DemEventClass/DemIndicatorAttribute/*"!]
    [!IF "./DemIndicatorFailureCycleSource ='DEM_FAILURE_CYCLE_INDICATOR'"!]
      [!VAR "FailureCycle" = "node:name(as:ref(./DemIndicatorFailureCycleRef))"!]
      [!VAR "FailureThreshold" = "node:value(./DemIndicatorFailureCycleCounterThreshold)"!]
    [!ELSE!]
      [!VAR "FailureCycle" = "node:name(as:ref(../../DemEventFailureCycleRef))"!]
      [!VAR "FailureThreshold" = "node:value(../../DemEventFailureCycleCounterThreshold)"!]
    [!ENDIF!]
    [!IF "not(contains($FailList, concat('#', $FailureCycle,'*', $FailureThreshold, '#')))"!]
        [!VAR "FailList" = "concat($FailList, $FailureCycle,'*', $FailureThreshold, '#')"!]
    [!VAR "FailCtr" = "$FailCtr + 1"!]
    [!ENDIF!]
  [!ENDLOOP!]
[!ENDLOOP!]
#if (defined DEM_NUM_FAILURECYC_COUNTER_INFO_ELEMENTS)
#error DEM_NUM_FAILURECYC_COUNTER_INFO_ELEMENTS already defined
#endif
/** \brief Number of failure cycle-counter information elements */
#define DEM_NUM_FAILURECYC_COUNTER_INFO_ELEMENTS [!"num:integer($FailCtr)"!]

/*------------------[Enable conditions configuration]-----------------------*/

#if (defined DEM_NUM_ENCOND_LINKS)
#error DEM_NUM_ENCOND_LINKS already defined
#endif
/** \brief Number of enable condition links */
[!VAR "NumEnCond" = "0"!]
[!LOOP "DemConfigSet/*/DemEventParameter/*/DemEventClass[node:exists(DemEnableConditionGroupRef)]"!]
  [!VAR "NumEnCond" = "$NumEnCond + count(as:ref(DemEnableConditionGroupRef)/DemEnableConditionRef/*)"!]
[!ENDLOOP!]
#define DEM_NUM_ENCOND_LINKS   [!"num:integer($NumEnCond)"!]U

/*------------------[Debouncing configuration]------------------------------*/

#if (defined DEM_NUM_DEBOUNCE_COUNTER)
#error DEM_NUM_DEBOUNCE_COUNTER already defined
#endif
/** \brief Number of counter based debounced events */
#define DEM_NUM_DEBOUNCE_COUNTER   [!"num:integer(count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceCounterBased']))"!]U

#if (defined DEM_NUM_DEBOUNCE_TIME)
#error DEM_NUM_DEBOUNCE_TIME already defined
#endif
/** \brief Number of counter based debounced events */
#define DEM_NUM_DEBOUNCE_TIME      [!"num:integer(count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceTimeBase']))"!]U

#if (defined DEM_NUM_DEBOUNCE_FREQUENCY)
#error DEM_NUM_DEBOUNCE_FREQUENCY already defined
#endif
/** \brief Number of counter based debounced events */
#define DEM_NUM_DEBOUNCE_FREQUENCY [!"num:integer(count(DemConfigSet/*/DemEventParameter/*[DemEventClass/DemDebounceAlgorithmClass = 'DemDebounceFrequencyBased']))"!]U

/*------------------[Fault confirmation configuration]----------------------*/

#if (defined DEM_NO_FAULT_CONFIRMATION)
#error DEM_NO_FAULT_CONFIRMATION already defined
#endif
/** \brief Symbolic name for failure class index if default configuration is used */
#define DEM_NO_FAULT_CONFIRMATION [!"num:integer($EventFailureClassIdx)"!]U

/*------------------[Immediate storage configuration]-----------------------*/

[!IF "(count(DemConfigSet/*/DemEventParameter/*[node:refexists(DemDTCClassRef) and node:value(as:ref(DemDTCClassRef)/DemImmediateNvStorage) = 'true']) > 0) or (DemGeneral/DemClearDTCBehavior != 'DEM_CLRRESP_VOLATILE')"!]
#if (defined DEM_STORAGEWAITNVMREADY)
#error DEM_STORAGEWAITNVMREADY already defined
#endif
#define DEM_STORAGEWAITNVMREADY(BlockId) [!"DemGeneral/DemCallbackMemStackMainFuncTrigger"!](BlockId);
[!ENDIF!]

/*------------------[Dynamic DTC configuration]-----------------------------*/

[!IF "node:exists(DemGeneral/DemCalloutDynamicDTCFnc)"!]
#if (defined DEM_GET_DYNAMIC_DTC)
#error DEM_GET_DYNAMIC_DTC already defined
#endif
#define DEM_GET_DYNAMIC_DTC(EvId, DTC) [!"DemGeneral/DemCalloutDynamicDTCFnc"!](EvId, DTC);
[!ENDIF!]

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( DEM_INT_CFG_H ) */
/*==================[end of file]===========================================*/