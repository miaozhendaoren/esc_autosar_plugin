/**
 * \file
 *
 * \brief AUTOSAR Dem
 *
 * This file contains the implementation of the AUTOSAR
 * module Dem.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DEM_INT_ERR_ID_H)
#define DEM_INT_ERR_ID_H

/* This file contains the configuration declarations of the
 * BSW Event ID symbols for AUTOSAR module Dem. */

[!AUTOSPACING!]
[!INCLUDE "Dem_Include.m"!]
[!//
/*==================[inclusions]============================================*/

/*==================[macros]================================================*/

/*------------------[Events configuration]----------------------------------*/

/* Symbolic names of configured BSW event IDs */
[!LOOP "node:order(DemConfigSet/*/DemEventParameter/*[DemEventKind = 'DEM_EVENT_KIND_BSW'], 'DemEventId')"!]
  [!INDENT "0"!]

    #if (defined DemConf_DemEventParameter_[!"name(.)"!])
    #error DemConf_DemEventParameter_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value */
    #define DemConf_DemEventParameter_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"DemEventId"!]U

    #if (!defined DEM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
    #if (defined [!"name(.)"!])
    #error [!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
    #define [!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"DemEventId"!]U

    #if (defined Dem_[!"name(.)"!])
    #error Dem_[!"name(.)"!] already defined
    #endif
    /** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) **/
    #define Dem_[!"name(.)"!][!CALL "Indent", "Length" = "31 - string-length(name(.))"!] [!"DemEventId"!]U
    #endif /* !defined DEM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
  [!ENDINDENT!]
[!ENDLOOP!]

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( DEM_INT_ERR_ID_H ) */
/*==================[end of file]===========================================*/
