/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2014)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Adc_Irq.c $                                                **
**                                                                           **
**  $CC VERSION : \main\dev_tc23x\5 $                                        **
**                                                                           **
**  $DATE       : 2014-06-25 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains the interrupt frames for the ADC Module.**
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: Yes                                     **
**                                                                           **
******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Std_Types.h"
#include "Adc_Irq.h"
#include "Mcal.h"
#include "Adc.h"

/*******************************************************************************
**                      Private Macros Definitions                            **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/


/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/


/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/


/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/


/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/
#define ADC_START_SEC_CODE
#include "MemMap.h"

/******************************************************************************
** Syntax : void OS_ISR_ADC0SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC0SR0_EXIST == STD_ON)
#if((IRQ_ADC0_SR0_PRIO > 0) || (IRQ_ADC0_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC0_SR0_PRIO > 0) && (IRQ_ADC0_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC0SR0_ISR (void)
#elif IRQ_ADC0_SR0_CAT == IRQ_CAT23
ISR(ADC0SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC0 SRN0 is used by request source 0 of ADC0 */
#if (ADC0_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC0);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC0SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC0SR1_EXIST == STD_ON)
#if((IRQ_ADC0_SR1_PRIO > 0) || (IRQ_ADC0_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC0_SR1_PRIO > 0) && (IRQ_ADC0_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC0SR1_ISR (void)
#elif IRQ_ADC0_SR1_CAT == IRQ_CAT23
ISR(ADC0SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC0 SRN1 is used by request source 1 of ADC0 */
#if (ADC0_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC0);
#endif
}
#endif

#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC1SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC1SR0_EXIST == STD_ON)
#if((IRQ_ADC1_SR0_PRIO > 0) || (IRQ_ADC1_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC1_SR0_PRIO > 0) && (IRQ_ADC1_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC1SR0_ISR (void)
#elif IRQ_ADC1_SR0_CAT == IRQ_CAT23
ISR(ADC1SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC1 SRN0 is used by request source 0 of ADC1 */
#if (ADC1_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC1);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC1SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC1SR1_EXIST == STD_ON)
#if((IRQ_ADC1_SR1_PRIO > 0) || (IRQ_ADC1_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC1_SR1_PRIO > 0) && (IRQ_ADC1_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC1SR1_ISR (void)
#elif IRQ_ADC1_SR1_CAT == IRQ_CAT23
ISR(ADC1SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC1 SRN1 is used by request source 1 of ADC1 */
#if (ADC1_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC1);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC2SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC2SR0_EXIST == STD_ON)
#if((IRQ_ADC2_SR0_PRIO > 0) || (IRQ_ADC2_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC2_SR0_PRIO > 0) && (IRQ_ADC2_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC2SR0_ISR (void)
#elif IRQ_ADC2_SR0_CAT == IRQ_CAT23
ISR(ADC2SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC2 SRN0 is used by request source 0 of ADC2 */
#if (ADC2_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC2);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC2SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC2SR1_EXIST == STD_ON)
#if((IRQ_ADC2_SR1_PRIO > 0) || (IRQ_ADC2_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC2_SR1_PRIO > 0) && (IRQ_ADC2_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC2SR1_ISR (void)
#elif IRQ_ADC2_SR1_CAT == IRQ_CAT23
ISR(ADC2SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC2 SRN1 is used by request source 1 of ADC2 */
#if (ADC2_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC2);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC3SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC3SR0_EXIST == STD_ON)
#if((IRQ_ADC3_SR0_PRIO > 0) || (IRQ_ADC3_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC3_SR0_PRIO > 0) && (IRQ_ADC3_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC3SR0_ISR (void)
#elif IRQ_ADC3_SR0_CAT == IRQ_CAT23
ISR(ADC3SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC3 SRN0 is used by request source 0 of ADC3 */
#if (ADC3_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC3);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC3SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC3SR1_EXIST == STD_ON)
#if((IRQ_ADC3_SR1_PRIO > 0) || (IRQ_ADC3_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC3_SR1_PRIO > 0) && (IRQ_ADC3_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC3SR1_ISR (void)
#elif IRQ_ADC3_SR1_CAT == IRQ_CAT23
ISR(ADC3SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC3 SRN1 is used by request source 1 of ADC3 */
#if (ADC3_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC3);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC4SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC4SR0_EXIST == STD_ON)
#if((IRQ_ADC4_SR0_PRIO > 0) || (IRQ_ADC4_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC4_SR0_PRIO > 0) && (IRQ_ADC4_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC4SR0_ISR (void)
#elif IRQ_ADC4_SR0_CAT == IRQ_CAT23
ISR(ADC4SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC4 SRN0 is used by request source 0 of ADC4 */
#if (ADC4_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC4);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC4SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC4SR1_EXIST == STD_ON)
#if((IRQ_ADC4_SR1_PRIO > 0) || (IRQ_ADC4_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC4_SR1_PRIO > 0) && (IRQ_ADC4_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC4SR1_ISR (void)
#elif IRQ_ADC4_SR1_CAT == IRQ_CAT23
ISR(ADC4SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC4 SRN1 is used by request source 1 of ADC4 */
#if (ADC4_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC4);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC5SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC5SR0_EXIST == STD_ON)
#if((IRQ_ADC5_SR0_PRIO > 0) || (IRQ_ADC5_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC5_SR0_PRIO > 0) && (IRQ_ADC5_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC5SR0_ISR (void)
#elif IRQ_ADC5_SR0_CAT == IRQ_CAT23
ISR(ADC5SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC5 SRN0 is used by request source 0 of ADC5 */
#if (ADC5_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC5);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC5SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC5SR1_EXIST == STD_ON)
#if((IRQ_ADC5_SR1_PRIO > 0) || (IRQ_ADC5_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC5_SR1_PRIO > 0) && (IRQ_ADC5_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC5SR1_ISR (void)
#elif IRQ_ADC5_SR1_CAT == IRQ_CAT23
ISR(ADC5SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC5 SRN1 is used by request source 1 of ADC5 */
#if (ADC5_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC5);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC6SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC6SR0_EXIST == STD_ON)
#if((IRQ_ADC6_SR0_PRIO > 0) || (IRQ_ADC6_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC6_SR0_PRIO > 0) && (IRQ_ADC6_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC6SR0_ISR (void)
#elif IRQ_ADC6_SR0_CAT == IRQ_CAT23
ISR(ADC6SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC6 SRN0 is used by request source 0 of ADC6 */
#if (ADC6_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC6);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC6SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC6SR1_EXIST == STD_ON)
#if((IRQ_ADC6_SR1_PRIO > 0) || (IRQ_ADC6_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC6_SR1_PRIO > 0) && (IRQ_ADC6_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC6SR1_ISR (void)
#elif IRQ_ADC6_SR1_CAT == IRQ_CAT23
ISR(ADC6SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC6 SRN1 is used by request source 1 of ADC6 */
#if (ADC6_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC6);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC7SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC7SR0_EXIST == STD_ON)
#if((IRQ_ADC7_SR0_PRIO > 0) || (IRQ_ADC7_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC7_SR0_PRIO > 0) && (IRQ_ADC7_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC7SR0_ISR (void)
#elif IRQ_ADC7_SR0_CAT == IRQ_CAT23
ISR(ADC7SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC7 SRN0 is used by request source 0 of ADC7 */
#if (ADC7_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC7);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC7SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC7SR1_EXIST == STD_ON)
#if((IRQ_ADC7_SR1_PRIO > 0) || (IRQ_ADC7_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC7_SR1_PRIO > 0) && (IRQ_ADC7_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC7SR1_ISR (void)
#elif IRQ_ADC7_SR1_CAT == IRQ_CAT23
ISR(ADC7SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC7 SRN1 is used by request source 1 of ADC7 */
#if (ADC7_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC7);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC8SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC8SR0_EXIST == STD_ON)
#if((IRQ_ADC8_SR0_PRIO > 0) || (IRQ_ADC8_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC8_SR0_PRIO > 0) && (IRQ_ADC8_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC8SR0_ISR (void)
#elif IRQ_ADC8_SR0_CAT == IRQ_CAT23
ISR(ADC8SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC8 SRN0 is used by request source 0 of ADC8 */
#if (ADC8_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC8);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC8SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC8SR1_EXIST == STD_ON)
#if((IRQ_ADC8_SR1_PRIO > 0) || (IRQ_ADC8_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC8_SR1_PRIO > 0) && (IRQ_ADC8_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC8SR1_ISR (void)
#elif IRQ_ADC8_SR1_CAT == IRQ_CAT23
ISR(ADC8SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC8 SRN1 is used by request source 1 of ADC8 */
#if (ADC8_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC8);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC9SR0_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC9SR0_EXIST == STD_ON)
#if((IRQ_ADC9_SR0_PRIO > 0) || (IRQ_ADC9_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC9_SR0_PRIO > 0) && (IRQ_ADC9_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC9SR0_ISR (void)
#elif IRQ_ADC9_SR0_CAT == IRQ_CAT23
ISR(ADC9SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC9 SRN0 is used by request source 0 of ADC9 */
#if (ADC9_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC9);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC9SR1_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC9SR1_EXIST == STD_ON)
#if((IRQ_ADC9_SR1_PRIO > 0) || (IRQ_ADC9_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC9_SR1_PRIO > 0) && (IRQ_ADC9_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC9SR1_ISR (void)
#elif IRQ_ADC9_SR1_CAT == IRQ_CAT23
ISR(ADC9SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC9 SRN1 is used by request source 1 of ADC9 */
#if (ADC9_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC9);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC10SR0_ISR(void)                                   **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC10SR0_EXIST == STD_ON)
#if((IRQ_ADC10_SR0_PRIO > 0) || (IRQ_ADC10_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADC10_SR0_PRIO > 0) && (IRQ_ADC10_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADC10SR0_ISR (void)
#elif IRQ_ADC10_SR0_CAT == IRQ_CAT23
ISR(ADC10SR0_ISR)
#endif
{
  
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC10 SRN0 is used by request source 0 of ADC10 */
#if (ADC10_REQSRC0 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0AdcRS0(ADC_HWUNIT_ADC10);
#endif

}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC10SR1_ISR(void)                                   **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC10SR1_EXIST == STD_ON)
#if((IRQ_ADC10_SR1_PRIO > 0) || (IRQ_ADC10_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADC10_SR1_PRIO > 0) && (IRQ_ADC10_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADC10SR1_ISR (void)
#elif IRQ_ADC10_SR1_CAT == IRQ_CAT23
ISR(ADC10SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* ADC10 SRN1 is used by request source 1 of ADC10 */
#if (ADC10_REQSRC1 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1AdcRS1(ADC_HWUNIT_ADC10);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADCCG0SR0_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADCCG0SRO_EXIST == STD_ON)
#if((IRQ_ADCCG0_SR0_PRIO > 0) || (IRQ_ADCCG0_SR0_CAT == IRQ_CAT23))
#if((IRQ_ADCCG0_SR0_PRIO > 0) && (IRQ_ADCCG0_SR0_CAT == IRQ_CAT1))
void OS_ISR_ADCCG0SR0_ISR (void)
#elif IRQ_ADCCG0_SR0_CAT == IRQ_CAT23
ISR(ADCCG0SR0_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* CG0 SRN0 is used by request source 2 of All ADC kernels */
#if ( (ADC0_REQSRC2 == ADC_REQSRC_USED) || (ADC1_REQSRC2 == ADC_REQSRC_USED)||\
      (ADC2_REQSRC2 == ADC_REQSRC_USED) || (ADC3_REQSRC2 == ADC_REQSRC_USED)||\
      (ADC4_REQSRC2 == ADC_REQSRC_USED) || (ADC5_REQSRC2 == ADC_REQSRC_USED)||\
      (ADC6_REQSRC2 == ADC_REQSRC_USED) || (ADC7_REQSRC2 == ADC_REQSRC_USED)||\
      (ADC8_REQSRC2 == ADC_REQSRC_USED) || (ADC9_REQSRC2 == ADC_REQSRC_USED)||\
      (ADC10_REQSRC2 == ADC_REQSRC_USED) )
  /* Call Adc Interrupt function*/
  Adc_IsrSrn0CG0AdcRS2();
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADCCG0SR1_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADCCG0SR1_EXIST == STD_ON)
#if((IRQ_ADCCG0_SR1_PRIO > 0) || (IRQ_ADCCG0_SR1_CAT == IRQ_CAT23))
#if((IRQ_ADCCG0_SR1_PRIO > 0) && (IRQ_ADCCG0_SR1_CAT == IRQ_CAT1))
void OS_ISR_ADCCG0SR1_ISR (void)
#elif IRQ_ADCCG0_SR1_CAT == IRQ_CAT23
ISR(ADCCG0SR1_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* CG0 SRN1 is used by request source 3 of ADC0 */
#if (ADC0_REQSRC3 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn1CG0AdcRS3(ADC_HWUNIT_ADC0);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADCCG0SR2_ISR(void)                                  **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADCCG0SR2_EXIST == STD_ON)
#if((IRQ_ADCCG0_SR2_PRIO > 0) || (IRQ_ADCCG0_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADCCG0_SR2_PRIO > 0) && (IRQ_ADCCG0_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADCCG0SR2_ISR (void)
#elif IRQ_ADCCG0_SR2_CAT == IRQ_CAT23
ISR(ADCCG0SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

/* CG0 SRN2 is used by request source 3 of ADC1 */
#if (ADC1_REQSRC3 == ADC_REQSRC_USED)
  /* Call Adc Interrupt function*/
  Adc_IsrSrn2CG0AdcRS3(ADC_HWUNIT_ADC1);
#endif
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC0SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC0SR2_EXIST == STD_ON)
#if((IRQ_ADC0_SR2_PRIO > 0) || (IRQ_ADC0_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC0_SR2_PRIO > 0) && (IRQ_ADC0_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC0SR2_ISR (void)
#elif IRQ_ADC0_SR2_CAT == IRQ_CAT23
ISR(ADC0SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC0 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC0);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC1SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC1SR2_EXIST == STD_ON)
#if((IRQ_ADC1_SR2_PRIO > 0) || (IRQ_ADC1_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC1_SR2_PRIO > 0) && (IRQ_ADC1_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC1SR2_ISR (void)
#elif IRQ_ADC1_SR2_CAT == IRQ_CAT23
ISR(ADC1SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC1 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC1);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC2SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC2SR2_EXIST == STD_ON)
#if((IRQ_ADC2_SR2_PRIO > 0) || (IRQ_ADC2_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC2_SR2_PRIO > 0) && (IRQ_ADC2_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC2SR2_ISR (void)
#elif IRQ_ADC2_SR2_CAT == IRQ_CAT23
ISR(ADC2SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC2 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC2);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC3SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC3SR2_EXIST == STD_ON)
#if((IRQ_ADC3_SR2_PRIO > 0) || (IRQ_ADC3_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC3_SR2_PRIO > 0) && (IRQ_ADC3_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC3SR2_ISR (void)
#elif IRQ_ADC3_SR2_CAT == IRQ_CAT23
ISR(ADC3SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC3 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC3);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC4SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC4SR2_EXIST == STD_ON)
#if((IRQ_ADC4_SR2_PRIO > 0) || (IRQ_ADC4_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC4_SR2_PRIO > 0) && (IRQ_ADC4_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC4SR2_ISR (void)
#elif IRQ_ADC4_SR2_CAT == IRQ_CAT23
ISR(ADC4SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC4 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC4);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC5SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC5SR2_EXIST == STD_ON)
#if((IRQ_ADC5_SR2_PRIO > 0) || (IRQ_ADC5_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC5_SR2_PRIO > 0) && (IRQ_ADC5_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC5SR2_ISR (void)
#elif IRQ_ADC5_SR2_CAT == IRQ_CAT23
ISR(ADC5SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC5 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC5);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif 

/******************************************************************************
** Syntax : void OS_ISR_ADC6SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC6SR2_EXIST == STD_ON)
#if((IRQ_ADC6_SR2_PRIO > 0) || (IRQ_ADC6_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC6_SR2_PRIO > 0) && (IRQ_ADC6_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC6SR2_ISR (void)
#elif IRQ_ADC6_SR2_CAT == IRQ_CAT23
ISR(ADC6SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC6 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC6);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC7SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC7SR2_EXIST == STD_ON)
#if((IRQ_ADC7_SR2_PRIO > 0) || (IRQ_ADC7_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC7_SR2_PRIO > 0) && (IRQ_ADC7_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC7SR2_ISR (void)
#elif IRQ_ADC7_SR2_CAT == IRQ_CAT23
ISR(ADC7SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC7 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC7);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC8SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC8SR2_EXIST == STD_ON)
#if((IRQ_ADC8_SR2_PRIO > 0) || (IRQ_ADC8_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC8_SR2_PRIO > 0) && (IRQ_ADC8_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC8SR2_ISR (void)
#elif IRQ_ADC8_SR2_CAT == IRQ_CAT23
ISR(ADC8SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC8 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC8);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_ADC9SR2_ISR(void)                                    **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC9SR2_EXIST == STD_ON)
#if((IRQ_ADC9_SR2_PRIO > 0) || (IRQ_ADC9_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC9_SR2_PRIO > 0) && (IRQ_ADC9_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC9SR2_ISR (void)
#elif IRQ_ADC9_SR2_CAT == IRQ_CAT23
ISR(ADC9SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC9 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC9);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif
#endif /* (IRQ_ADC9_EXIST == STD_ON) */

/******************************************************************************
** Syntax : void OS_ISR_ADC10SR2_ISR(void)                                   **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       non reentrant                                           **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description :     Service on ADC Request source conversion complete       **
**                   service request                                         **
**                                                                           **
******************************************************************************/
#if (IRQ_ADC10SR2_EXIST == STD_ON)
#if((IRQ_ADC10_SR2_PRIO > 0) || (IRQ_ADC10_SR2_CAT == IRQ_CAT23))
#if((IRQ_ADC10_SR2_PRIO > 0) && (IRQ_ADC10_SR2_CAT == IRQ_CAT1))
void OS_ISR_ADC10SR2_ISR (void)
#elif IRQ_ADC10_SR2_CAT == IRQ_CAT23
ISR(ADC10SR2_ISR)
#endif
{
     
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

#if (ADC_ENABLE_LIMIT_CHECK == STD_ON)
/* ADC10 SRN2 is used by Channel Event for Limit check */
/* Call Adc Interrupt function*/
  Adc_IsrSrn2AdcChEvnt(ADC_HWUNIT_ADC10);
#endif /* (ADC_ENABLE_LIMIT_CHECK == STD_ON) */
}
#endif

#endif

#define ADC_STOP_SEC_CODE
#include "MemMap.h"

