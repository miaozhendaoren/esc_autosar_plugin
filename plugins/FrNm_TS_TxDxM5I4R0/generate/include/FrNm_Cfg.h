/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/* !LINKSTO FRNM369,1 */

/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 8.8 (required)
 *     An external object or function shall be declared in one
 *     and only one file.
 *
 *     Reason:
 *     The files SchM_FrNm.h and FrNm_Cfg.h contain declarations of
 *     FrNm_MainFunction_x. This is due to the issue reported in
 *     http://www.autosar.org/bugzilla/show_bug.cgi?id=53853
 *     and should be fixed in a future version of the Rte.
 */

[!AUTOSPACING!][!//
[!INCLUDE "FrNm_Checks.m"!][!//
#if (!defined FRNM_CFG_H)
#define FRNM_CFG_H

/*==================[includes]==============================================*/

#include <Std_Types.h>    /* AUTOSAR standard types     */
#include <TSAutosar.h>    /* EB specific standard types */

/*==================[macros]================================================*/

[!SELECT "FrNmGlobalConfig/FrNmGlobalProperties"!][!//
/** \brief Macro switch for version info API usage */
#define FRNM_VERSION_INFO_API            [!//
[!IF "FrNmVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Switch for DET usage */
#define FRNM_DEV_ERROR_DETECT            [!//
[!IF "FrNmDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_MAIN_ACROSS_FR_CYCLE            [!//
[!IF "FrNmMainAcrossFrCycle = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDSELECT!][!//

[!SELECT "FrNmGlobalConfig/FrNmGlobalFeatures"!][!//
#define FRNM_STATE_CHANGE_INDICATION_ENABLED    [!//
[!IF "FrNmStateChangeIndicationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_PASSIVE_MODE_ENABLED        [!//
[!IF "FrNmPassiveModeEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_PDU_RX_INDICATION_ENABLED   [!//
[!IF "FrNmPduRxIndicationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_USER_DATA_ENABLED           [!//
[!IF "FrNmUserDataEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_REMOTE_SLEEP_IND_ENABLED    [!//
[!IF "FrNmRemoteSleepIndicationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!IF "FrNmPassiveModeEnabled = 'true'"!]
/* FrNm0040_Conf : If passive mode is enabled, then FrNmNodeDetectionEnabled is false.*/
#define FRNM_NODE_DETECTION_ENABLED STD_OFF

/* Value of FrNmSourceNodeIdentifierEnabled is same as FrNmNodeDetectionEnabled.*/
#define FRNM_SOURCE_NODE_IDENTIFIER_ENABLED  STD_OFF

/*FrNm0041_Conf : FrNmControlBitVectorEnabled has dependency to FrNmNodeDetectionEnabled */
#define FRNM_CONTROL_BIT_VECTOR_ENABLED STD_OFF
[!ELSE!]
#define FRNM_NODE_DETECTION_ENABLED      [!//
[!IF "FrNmNodeDetectionEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_SOURCE_NODE_IDENTIFIER_ENABLED   [!//
[!IF "FrNmSourceNodeIdentifierEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_CONTROL_BIT_VECTOR_ENABLED      [!//
[!IF "FrNmControlBitVectorEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDIF!]
#define FRNM_BUS_SYNCHRONIZATION_ENABLED [!//
[!IF "FrNmBusSynchronizationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_REPEAT_MSG_BIT_ENABLED      [!//
[!IF "FrNmRepeatMessageBitEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]



#define FRNM_DUAL_CHANNEL_PDU_ENABLE      [!//
[!IF "FrNmDualChannelPduEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_COM_USER_DATA_SUPPORT      [!//
[!IF "FrNmComUserDataSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_CYCLE_COUNTER_EMULATION      [!//
[!IF "FrNmCycleCounterEmulation = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_HW_VOTE_ENABLE      [!//
[!IF "FrNmHwVoteEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_REMOTE_SLEEP_INDICATION_ENABLED      [!//
[!IF "FrNmRemoteSleepIndicationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_COORDINATOR_SYNC_SUPPORT  [!//
[!IF "FrNmCoordinatorSyncSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** FrNmScheduleVariant2VoteBitValue **/
[!IF "node:exists(FrNmScheduleVariant2VoteBitValue)"!]
#define FRNM_SCHEDULE_VARIANT_2_FORCE_VOTE_BIT      STD_ON
#define FRNM_SCHEDULE_VARIANT_2_VOTE_BIT_VALUE      [!//
  [!IF "num:i(FrNmScheduleVariant2VoteBitValue) = 0"!][!/*
  */!]0x00U[!//
  [!ELSE!][!/*
  */!]0x80U[!//
  [!ENDIF!]
[!ELSE!]
#define FRNM_SCHEDULE_VARIANT_2_FORCE_VOTE_BIT      STD_OFF
[!ENDIF!]
[!ENDSELECT!][!//

#define  FRNM_CONTROL_BIT_VECTOR_ACTIVE   [!//
[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers[FrNmControlBitVectorActive = 'true'])) != 0"!][!//
STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRNM_SYNCHRONIZATIONPOINT_ENABLED   [!//
[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers[FrNmSynchronizationPointEnabled = 'true'])) != 0"!][!//
STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#define FRNM_REPEAT_MESSAGE_BIT_ACTIVE  [!//
[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers[FrNmRepeatMessageBitActive = 'true'])) != 0"!][!//
STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#define FRNM_NUMBER_OF_CLUSTERS   [!//
[!"num:i(FrNmGlobalConfig/FrNmGlobalConstants/FrNmNumberOfClusters)"!]U


/** \brief Maximum number of Active channels */
#define FRNM_NUMBER_OF_CHANNELS               [!//
[!"num:i(count(FrNmChannelConfig/*/FrNmChannel/*))"!]U

/* Defines depends on whether Rx Pdus used are only for vote or for vote && data*/
#define FRNM_ENABLE_NM_RXVOTE          [!//
[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmRxPdu/*[FrNmRxPduContainsVote = 'true' and FrNmRxPduContainsData='false'])) !=0"!][!//
STD_ON[!ELSE!]STD_OFF[!ENDIF!] [!//

#define FRNM_ENABLE_NM_RXDATA          [!//
[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmRxPdu/*[FrNmRxPduContainsVote = 'false' and FrNmRxPduContainsData='true'])) !=0"!][!//
STD_ON[!ELSE!]STD_OFF[!ENDIF!] [!//

#define FRNM_ENABLE_NM_RXMIXED         [!//
[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmRxPdu/*[FrNmRxPduContainsVote = 'true' and FrNmRxPduContainsData = 'true'])) !=0"!][!//
STD_ON[!ELSE!]STD_OFF[!ENDIF!] [!//

/* Defines depends on whether Tx Pdus used are only for vote or for vote && data*/
#define FRNM_ENABLE_NMVOTE          [!//
[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsVote = 'true' and FrNmTxPduContainsData='false'])) !=0"!][!//
STD_ON[!ELSE!]STD_OFF[!ENDIF!] [!//

#define FRNM_ENABLE_NMDATA          [!//
[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsVote = 'false' and FrNmTxPduContainsData='true'])) !=0"!][!//
STD_ON[!ELSE!]STD_OFF[!ENDIF!] [!//

#define FRNM_ENABLE_NMMIXED         [!//
[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*[FrNmTxPduContainsVote = 'true' and FrNmTxPduContainsData = 'true'])) !=0"!][!//
STD_ON[!ELSE!]STD_OFF[!ENDIF!] [!//

#define FRNM_NUMBER_OF_RX_PDUS   [!//
[!"num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmRxPdu/*))"!]U

#define FRNM_NUMBER_OF_TX_PDUS   [!//
[!"num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*))"!]U

[!IF "num:i(count(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*)) !=0"!]
/* Maximum size of associated Pdus. This size is used to define local buffer when sending data */
#define FRNM_TX_BUFFER_SIZE  [!//
[!"num:max(node:refs(FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmTxPdu/*/FrNmTxPduRef)/PduLength)"!]U
[!ENDIF!]

[!LOOP "FrNmChannelConfig/*/FrNmChannel/*/*/FrNmRxPdu/*"!][!//

#if (defined FrNmConf_[!"name(..)"!]_[!"name(.)"!])
#error FrNmConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif

/** \brief Symbolic name of FrNmRxPdu with Id "[!"FrNmRxPduId"!]" for
 *  the channel [!"name(../../..)"!] */
#define FrNmConf_[!"name(..)"!]_[!"name(.)"!]   [!"FrNmRxPduId"!]U

#if (!defined FRNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value without prefix
 * (AUTOSAR version <= 3.1 rev4) [!"FrNmRxPduId"!] */
#define [!"name(.)"!]                     [!"FrNmRxPduId"!]U

#if (defined FrNm_[!"name(.)"!])
#error FrNm_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define FrNm_[!"name(.)"!]   [!"FrNmRxPduId"!]U
#endif /* FRNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]


[!LOOP "FrNmChannelConfig/*/FrNmChannel/*/*/FrNmTxPdu/*"!][!//

#if (defined FrNmConf_[!"name(..)"!]_[!"name(.)"!])
#error FrNmConf_[!"name(..)"!]_[!"name(.)"!] already defined
#endif

/** \brief Symbolic name of FrNmRxPdu with Id "[!"FrNmTxConfirmationPduId"!]"
 *  for the channel [!"name(../../..)"!] */
#define FrNmConf_[!"name(..)"!]_[!"name(.)"!]   [!"FrNmTxConfirmationPduId"!]U

#if (!defined FRNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value without prefix
 * (AUTOSAR version <= 3.1 rev4) [!"FrNmTxConfirmationPduId"!] */
#define [!"name(.)"!]                     [!"FrNmTxConfirmationPduId"!]U

#if (defined FrNm_[!"name(.)"!])
#error FrNm_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define FrNm_[!"name(.)"!]   [!"FrNmTxConfirmationPduId"!]U
#endif /* FRNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]

[!LOOP "FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmUserDataTxPdu"!][!//
[!CALL "GetSymbolName","ShortNameRef"="'.'"!][!//

#if (defined [!"$SymbolName"!])
#error [!"$SymbolName"!] already defined
#endif

/** \brief Symbolic name of FrNmTxUserDataPduId with Id
 * "[!"FrNmTxUserDataPduId"!]" for the channel [!"name(../..)"!]
 */
#define [!"$SymbolName"!]   [!"FrNmTxUserDataPduId"!]U

#if (!defined FRNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(../..)"!]_FrNmUserDataTxPdu)
#error [!"name(../..)"!]_FrNmUserDataTxPdu already defined
#endif

/** \brief Export symbolic name value without prefix
 * (AUTOSAR version <= 3.1 rev4) [!"FrNmTxUserDataPduId"!]
 */
#define [!"name(../..)"!]_FrNmUserDataTxPdu           [!"FrNmTxUserDataPduId"!]U

#if (defined FrNm_[!"name(../..)"!]_FrNmUserDataTxPdu)
#error FrNm_[!"name(../..)"!]_FrNmUserDataTxPdu already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2)
 */
#define FrNm_[!"name(../..)"!]_FrNmUserDataTxPdu   [!"FrNmTxUserDataPduId"!]U
#endif /* FRNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]

[!LOOP "FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers/FrNmUserDataRxPdu"!][!//
[!CALL "GetSymbolName","ShortNameRef"="'.'"!][!//

#if (defined [!"$SymbolName"!])
#error [!"$SymbolName"!] already defined
#endif

/** \brief Symbolic name of FrNmRxUserDataPduId with Id
 * "[!"FrNmRxUserDataPduId"!]" for the channel [!"name(../..)"!]
 */
#define [!"$SymbolName"!]   [!"FrNmRxUserDataPduId"!]U

#if (!defined FRNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(../..)"!]_FrNmUserDataRxPdu)
#error [!"name(../..)"!]_FrNmUserDataRxPdu already defined
#endif

/** \brief Export symbolic name value without prefix
 * (AUTOSAR version <= 3.1 rev4) [!"FrNmRxUserDataPduId"!]
 */
#define [!"name(../..)"!]_FrNmUserDataRxPdu           [!"FrNmRxUserDataPduId"!]U

#if (defined FrNm_[!"name(../..)"!]_FrNmUserDataRxPdu)
#error FrNm_[!"name(../..)"!]_FrNmUserDataRxPdu already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2)
 */
#define FrNm_[!"name(../..)"!]_FrNmUserDataRxPdu   [!"FrNmRxUserDataPduId"!]U
#endif /* FRNM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]


/*------------------[PN Extensions]-------------------------------------*/
[!SELECT "FrNmGlobalConfig"!]
[!IF "count(../FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers[FrNmPnEnabled ='true']) > 0"!]
/** FrNmPnInfoLength **/
#define FRNM_PN_INFO_LENGTH     [!//
              [!"FrNmGlobalFeatures/FrNmPnInfo/FrNmPnInfoLength"!]U

#define FRNM_PN_EIRA_TIMER_SIZE  (FRNM_PN_INFO_LENGTH * 8U)

#define FRNM_PN_INFO_OFFSET     [!//
              [!"FrNmGlobalFeatures/FrNmPnInfo/FrNmPnInfoOffset"!]U

/** FrNmPnEiraCalcEnabled **/
#define FRNM_PN_EIRA_CALC_ENABLED             [!//
[!IF "FrNmGlobalFeatures/FrNmPnEiraCalcEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** FrNmPnResetTime **/
#define FRNM_PN_RESET_TIME [!//
    [!"num:i(((FrNmGlobalFeatures/FrNmPnResetTime) * 1000) div ((num:min(../FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelTiming/FrNmMainFunctionPeriod)) * 1000))"!]U

[!/* get Eira PduId by EcuC reference from PduR, existence is checked in xdm */!][!//
[!IF "FrNmGlobalFeatures/FrNmPnEiraCalcEnabled = 'true'"!][!//

#define FRNM_PN_EIRA_PDUID  [!//
    [!"as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/PduRRoutingPath/*/PduRSrcPdu[PduRSrcPduRef=node:current()/FrNmGlobalFeatures/FrNmPnEiraRxNSduRef
]/PduRSourcePduHandleId"!]U/* FrNmPnEiraRx Id*/
[!ENDIF!]

#define FRNM_PN_ENABLED  STD_ON
[!ELSE!]
#define FRNM_PN_ENABLED  STD_OFF
#define FRNM_PN_EIRA_CALC_ENABLED STD_OFF
[!ENDIF!]

[!ENDSELECT!][!//

[!IF "FrNmGlobalConfig/FrNmGlobalFeatures/FrNmPnEiraCalcEnabled = 'true'"!]
  /** Calculation of Eira Timer size from Configured PN bits **/
  [!VAR "EiraTimerSize" = "0"!][!//
  [!LOOP "FrNmGlobalConfig/FrNmGlobalFeatures/FrNmPnInfo/FrNmPnFilterMaskByte/*"!][!//
    [!FOR "BitPos" = "0" TO "7"!][!//
      [!IF "bit:getbit(node:value(./FrNmPnFilterMaskByteValue), $BitPos) = 'true' "!][!//Checking set PN bits of each FrNmPnFilterMaskByteValue
        [!VAR "EiraTimerSize" = "$EiraTimerSize + 1"!][!//Timer size calculation for each PN bit
      [!ENDIF!][!//
    [!ENDFOR!][!//
  [!ENDLOOP!][!//
#define FRNM_EIRA_TIMER_SIZE  [!"num:i($EiraTimerSize)"!]U
[!ENDIF!]

/*------------------[Hsm configuration]-------------------------------------*/

#if (FRNM_NUMBER_OF_CHANNELS > 1U)
#define FRNM_HSM_INST_MULTI_ENABLED STD_ON
#else
#define FRNM_HSM_INST_MULTI_ENABLED STD_OFF
#endif

/*==================[type definitions]======================================*/

/** \brief Type for the module config
 **
 ** This type describes the config data of the module.
 **
 ** This FrNm implementation does not support link or post build time
 ** configuration.  Therefore the argument of the init function is ignored and
 ** this data type is defined only for compatibility. */
typedef uint8 FrNm_ConfigType;

/*==================[external function declarations]========================*/
#define FRNM_START_SEC_CODE
#include <MemMap.h>
[!FOR "I"="0" TO "num:i(FrNmGlobalConfig/FrNmGlobalConstants/FrNmNumberOfClusters)-1"!]
  /* Deviation MISRA-1 */
  FUNC(void, FRNM_CODE) FrNm_MainFunction_[!"num:i($I)"!](void);

[!ENDFOR!]
#define FRNM_STOP_SEC_CODE
#include <MemMap.h>
/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)

#define FRNM_START_SEC_CONFIG_DATA_8
#include <MemMap.h>
/* Mapping between PN Index and EIRA timer array */
extern CONST(uint8, FRNM_CONST) FrNm_EiraTimerMap[FRNM_PN_EIRA_TIMER_SIZE];
#define FRNM_STOP_SEC_CONFIG_DATA_8
#include <MemMap.h>

#endif
/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/



/*==================[internal function definitions]=========================*/

#endif /* if !defined( FRNM_CFG_H ) */
/*==================[end of file]===========================================*/
