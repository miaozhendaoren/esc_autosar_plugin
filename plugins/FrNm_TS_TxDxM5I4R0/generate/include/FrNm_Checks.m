[!/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!/* These checks were originally present as INVALID checks in FrNm.xdm.
   * Since these inter-module checks involve parameters from different
   * configuration classes, it's no more possible to keep them in FrNm.xdm.
   * Refer ASCFRNM-588 for more details.
   */!]

[!/* *** multiple inclusion protection *** */!]
[!IF "not(var:defined('FRNM_CHECKS_M'))"!]
[!VAR "FRNM_CHECKS_M" = "'true'"!]

[!NOCODE!][!//

[!/* === Inter-module checks between FrNm and FrIf, EcuC or PduR === */!]

[!/* === General checks === */!]

[!/* === Check if channel handles are zero-based and unique === */!]
[!/* === This check has been moved from FrNmChannelHandle in FrNm.xdm.m4 === */!]
[!IF "not(node:isconsecutive(node:refs(FrNmChannelConfig/*/FrNmChannel/*/
          FrNmChannelIdentifiers/FrNmChannelHandle)/FrIfClstIdx, 0))"!]
  [!ERROR!]"The referenced FrIf Cluster Ids are not zero-based consecutive!"
  [!ENDERROR!]
[!ENDIF!]

[!/* === Ensure that the routing path of the EIRA Rx NSdu is configured in PduR === */!]
[!/* === This check has been moved from FrNmPnEiraRxNSduRef in FrNm.xdm.m4 === */!]
[!SELECT "as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures"!]
  [!IF "((FrNmPnEiraCalcEnabled = 'true') and
         (not(node:exists(as:modconf('PduR')[1]/PduRRoutingTables/*/
          PduRRoutingTable/*/PduRRoutingPath/*/PduRSrcPdu[PduRSrcPduRef =
          node:current()/FrNmPnEiraRxNSduRef]/PduRSourcePduHandleId))))"!]
    [!ERROR!]"The routing path of the EIRA Rx NSdu (FrNmPnEiraRxNSduRef) is not configured in PduR."
    [!ENDERROR!]
  [!ENDIF!]
[!ENDSELECT!]

[!/* === Perform the following checks for each channel configured === */!]
[!/* === Following checks have been moved from FrNmRxPduRef in FrNm.xdm.m4 === */!]
[!LOOP "FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers"!]

  [!/* === Checks for Rx Pdus === */!]
  [!LOOP "FrNmRxPdu/*"!]

    [!/* === Ensure that Rx Pdu length is in range === */!]
    <!-- /* !LINKSTO FRNM154,1 */ -->
    [!IF "((node:ref(FrNmRxPduRef)/PduLength < 1) or
           (node:ref(FrNmRxPduRef)/PduLength > 8))"!]
      [!ERROR!]"Length of the Rx Pdu [!"node:name(.)"!] in channel '[!"node:name(../../..)"!]' is out of range (1-8)."
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that Rx Pdu is large enough to hold Node Identifier === */!]
    [!IF "((FrNmRxPduContainsData = 'true' ) and
           (as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/
            FrNmSourceNodeIdentifierEnabled = 'true') and
           (node:ref(FrNmRxPduRef)/PduLength < 2))"!]
      [!ERROR!]"'FrNmSourceNodeIdentifierEnabled' is set to true and 'FrNmRxPduContainsData' is enabled for channel [!"node:name(../../..)"!], but the Rx Pdu '[!"node:name(.)"!]' is too small (< 2) to hold the Node Identifier!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that Rx Pdu Data Length matches with the Tx Pdu Data Length === */!]
    [!IF "((FrNmRxPduContainsData = 'true') and
            (node:exists(../../FrNmTxPdu/*[FrNmTxPduContainsData = 'true'])) and
            (node:ref(FrNmRxPduRef)/PduLength !=
            node:ref(../../FrNmTxPdu/*[FrNmTxPduContainsData = 'true']/FrNmTxPduRef)/PduLength))"!]
      [!ERROR!]"Rx Data Pdu Length for channel '[!"node:name(../../..)"!]' does not match with the Tx Pdu Data Length!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that Rx Pdu length is consistent for both Rx Pdus === */!]
    [!IF "num:max(node:refs(../*/FrNmRxPduRef)/PduLength) !=
          num:i(node:ref(FrNmRxPduRef)/PduLength)"!]
      [!ERROR!]"The lengths of the two Rx Pdus (FrNmRxPdu) in channel '[!"node:name(../../..)"!]' are not equal."
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that Rx Pdu is large enough to hold PN Info === */!]
    [!IF "((count(as:modconf('FrNm')[1]/FrNmChannelConfig/*/FrNmChannel/*/FrNmChannelIdentifiers[FrNmPnEnabled ='true']) > 0) and
           (node:ref(FrNmRxPduRef)/PduLength <
           (as:modconf('FrNm')[1]/FrNmGlobalConfig/
            FrNmGlobalFeatures/FrNmPnInfo/FrNmPnInfoLength +
            as:modconf('FrNm')[1]/FrNmGlobalConfig/
            FrNmGlobalFeatures/FrNmPnInfo/FrNmPnInfoOffset)))"!]
      [!ERROR!]"Partial networking is enabled for channel '[!"node:name(../../..)"!]', but length of Rx Pdu '[!"node:name(.)"!]' is less than (FrNmPnInfoLength + FrNmPnInfoOffset)."
      [!ENDERROR!]
    [!ENDIF!]
  [!ENDLOOP!]

  [!/* === Checks for Tx Pdus === */!]
  [!/* === Following checks have been moved from FrNmTxPduRef in FrNm.xdm.m4 === */!]
  [!LOOP "FrNmTxPdu/*"!]

    [!/* === Ensure that Tx Pdu length is not 0 === */!]
    [!IF "(node:ref(FrNmTxPduRef)/PduLength = 0)"!]
      [!ERROR!]"The length of the Tx Pdu [!"node:name(.)"!] in channel '[!"node:name(../../..)"!]' is 0!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that Tx Pdu is not larger than Rx Pdus === */!]
    <!-- /* !LINKSTO FRNM154,1 */ -->
    [!IF "(num:max(node:refs(../../FrNmRxPdu/*/FrNmRxPduRef)/PduLength) <
           num:i(node:ref(FrNmTxPduRef)/PduLength))"!]
      [!ERROR!]"The length of the Tx Pdu '[!"node:name(.)"!]' is greater than the length of at least one Rx Pdu in channel [!"node:name(../../..)"!]."
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that Tx Pdu is large enough to hold Node Identifier === */!]
    [!IF "((FrNmTxPduContainsData = 'true' ) and
           (as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/
            FrNmSourceNodeIdentifierEnabled = 'true') and
           (node:ref(FrNmTxPduRef)/PduLength < 2))"!]
      [!ERROR!]"'FrNmSourceNodeIdentifierEnabled' is set to true and 'FrNmTxPduContainsData' is enabled for channel '[!"node:name(../../..)"!]', but the Tx Pdu '[!"node:name(.)"!]' is too small (< 2) to hold the Node Identifier!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that exactly one Tx Pdu is configured in FrIf === */!]
    [!IF "count(as:modconf('FrIf')[1]/FrIfConfig/*/FrIfPdu/*/FrIfPduDirection[
          FrIfTxPduRef = node:current()/FrNmTxPduRef]) != 1"!]
      [!ERROR!]"No unique Tx Pdu is configured in FrIf via 'FrIfTxPduRef' for the Tx Pdu [!"node:name(.)"!] in channel [!"node:name(../../..)"!]."
      [!ENDERROR!]
    [!ENDIF!]

  [!ENDLOOP!]

  [!/* === Checks for User Data Tx Pdus === */!]
  [!/* === Following checks have been moved from FrNmTxUserDataPduRef in FrNm.xdm.m4 === */!]
  [!SELECT "FrNmUserDataTxPdu"!]

    [!/* === Ensure that FrNmUserDataTxPdu is disabled when User Data Length is 0 === */!]
    [!/* !LINKSTO FrNm.ComUserDataSupport.UserDataLength.NoUserData,1 */!]
    [!IF "(node:refvalid(../FrNmTxPdu/*[FrNmTxPduContainsData = 'true']/FrNmTxPduRef)) and
           not(((as:modconf('FrNm')/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmSourceNodeIdentifierEnabled = 'true') and
                (as:ref(../FrNmTxPdu/*[FrNmTxPduContainsData = 'true']/FrNmTxPduRef)/PduLength > 2)) or
                ((as:modconf('FrNm')/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmSourceNodeIdentifierEnabled = 'false') and
                 (as:ref(../FrNmTxPdu/*[FrNmTxPduContainsData = 'true']/FrNmTxPduRef)/PduLength > 1)))"!]
      [!ERROR!]"Tx User Data Length of '[!"node:name(../FrNmTxPdu)"!]' is 0 for channel '[!"node:name(../..)"!]', so parameter 'FrNmTxUserDataPdu' cannot be enabled!"
      [!ENDERROR!]
    [!ENDIF!]


    [!/* === Ensure that Tx Pdu length is not 0 === */!]
    [!IF "(node:ref(FrNmTxUserDataPduRef)/PduLength = 0)"!]
      [!ERROR!]"Length of User Data Tx Pdu [!"node:name(.)"!] for channel '[!"node:name(../..)"!]' is 0!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that Length of User Data Tx Pdu is equal to Tx Pdu Length - Header Length === */!]
    [!/* !LINKSTO FrNm.ComUserDataSupport.UserDataLength.Error,1, FRNM155,1 */!]
    [!/* === When Source Node Identifier is enabled === */!]
    [!IF "(as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmComUserDataSupport = 'true') and
          (as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmSourceNodeIdentifierEnabled = 'true') and
          ((as:ref(../FrNmTxPdu/*[FrNmTxPduContainsData = 'true']/FrNmTxPduRef)/PduLength) !=
          ((as:ref(FrNmTxUserDataPduRef)/PduLength) + 2))"!]
      [!ERROR!]"'FrNmSourceNodeIdentifierEnabled' is set to 'true' but length of User Data Tx Pdu for channel '[!"node:name(../..)"!]' is not equal to Tx Pdu Length - 2!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === When Source Node Identifier is disabled === */!]
    [!IF "(as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmComUserDataSupport = 'true') and
          (as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmSourceNodeIdentifierEnabled = 'false') and
          ((as:ref(../FrNmTxPdu/*[FrNmTxPduContainsData = 'true']/FrNmTxPduRef)/PduLength) !=
          ((as:ref(FrNmTxUserDataPduRef)/PduLength) + 1))"!]
      [!ERROR!]"'FrNmSourceNodeIdentifierEnabled' is set to 'false' but length of User Data Tx Pdu for channel '[!"node:name(../..)"!]' is not equal to Tx Pdu Length - 1!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that User Data Tx Pdu is not larger than Tx Pdus === */!]
    [!/* !LINKSTO FrNm.ComUserDataSupport.UserDataLength,1 */!]
    [!/* === When Source Node Identifier is enabled === */!]
    [!/* === When Source Node Identifier is enabled === */!]
    [!IF "(as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/
           FrNmSourceNodeIdentifierEnabled = 'true')"!]
      [!IF "((num:max(node:refs(../FrNmTxPdu/*/FrNmTxPduRef)/PduLength)) <
             (num:i(node:ref(FrNmTxUserDataPduRef)/PduLength + 2)))"!]
        [!ERROR!]"'FrNmSourceNodeIdentifierEnabled' is set to 'true' but User Data Tx Pdu (FrNmTxUserDataPduRef) for channel '[!"node:name(../..)"!]' is not smaller (by at least 2) than one or more FrNmTxPdus."
        [!ENDERROR!]
      [!ENDIF!]
    [!/* === When Source Node Identifier is disabled === */!]
    [!ELSEIF "((num:max(node:refs(../FrNmTxPdu/*/FrNmTxPduRef)/PduLength)) <
               (num:i(node:ref(FrNmTxUserDataPduRef)/PduLength + 1)))"!]
      [!ERROR!]"Length of User Data Tx Pdu 'FrNmTxUserDataPduRef' for channel '[!"node:name(../..)"!]' is not smaller (by at least 1) than one or more FrNmTxPdus."
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that the routing path of User Data Tx Pdu
             is configured in PduR ===*/!]
    [!IF "not(node:exists(as:modconf('PduR')[1]/PduRRoutingTables/*/
              PduRRoutingTable/*/PduRRoutingPath/*/PduRDestPdu/*[PduRDestPduRef =
              node:current()/FrNmTxUserDataPduRef]/PduRDestPduHandleId))"!]
      [!ERROR!]"The routing path of User Data Tx Pdu (FrNmTxUserDataPdu) for channel '[!"node:name(../..)"!]' is not configured in PduR!"
      [!ENDERROR!]
    [!ENDIF!]

  [!ENDSELECT!]

  [!/* === Checks for User Data Rx Pdus === */!]
  [!/* === Following checks have been moved from FrNmRxUserDataPduRef in FrNm.xdm.m4 === */!]
  [!SELECT "FrNmUserDataRxPdu"!]

    [!/* === Ensure that FrNmUserDataRxPdu is disabled when User Data Length is 0 === */!]
    [!/* !LINKSTO FrNm.ComUserDataSupport.UserDataLength.NoUserData,1 */!]
    [!IF "(node:refvalid(../FrNmRxPdu/*[FrNmRxPduContainsData = 'true']/FrNmRxPduRef)) and
           not(((as:modconf('FrNm')/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmSourceNodeIdentifierEnabled = 'true') and
                (as:ref(../FrNmRxPdu/*[FrNmRxPduContainsData = 'true']/FrNmRxPduRef)/PduLength > 2)) or
                ((as:modconf('FrNm')/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmSourceNodeIdentifierEnabled = 'false') and
                 (as:ref(../FrNmRxPdu/*[FrNmRxPduContainsData = 'true']/FrNmRxPduRef)/PduLength > 1)))"!]
      [!ERROR!]"Rx User Data Length of '[!"node:name(../FrNmRxPdu)"!]' is 0 for channel '[!"node:name(../..)"!]', so parameter 'FrNmRxUserDataPdu' cannot be enabled!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that Rx Pdu length is not 0 === */!]
    [!IF "(node:ref(FrNmRxUserDataPduRef)/PduLength = 0)"!]
      [!ERROR!]"Length of User Data Rx Pdu 'FrNmRxUserDataPduRef' for channel '[!"node:name(../..)"!]' is 0!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that User Data Rx Pdu is not larger than Rx Pdus === */!]
    [!/* !LINKSTO FrNm.ComUserDataSupport.UserDataLength,1 */!]
    [!/* === When Source Node Identifier is enabled === */!]
    [!IF "(as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/
           FrNmSourceNodeIdentifierEnabled = 'true')"!]
      [!IF "((num:max(node:refs(../FrNmRxPdu/*/FrNmRxPduRef)/PduLength)) <
             (num:i(node:ref(FrNmRxUserDataPduRef)/PduLength + 2)))"!]
        [!ERROR!]"'FrNmSourceNodeIdentifierEnabled' is set to 'true' but User Data Rx Pdu (FrNmRxUserDataPduRef) for channel '[!"node:name(../..)"!]' is not smaller (by at least 2) than one or more FrNmRxPdus."
        [!ENDERROR!]
      [!ENDIF!]
    [!/* === When Source Node Identifier is disabled === */!]
    [!ELSEIF "((num:max(node:refs(../FrNmRxPdu/*/FrNmRxPduRef)/PduLength)) <
               (num:i(node:ref(FrNmRxUserDataPduRef)/PduLength + 1)))"!]
      [!ERROR!]"User Data Rx Pdu 'FrNmRxUserDataPduRef' for channel '[!"node:name(../..)"!]' is not smaller (by at least 1) than one or more FrNmRxPdus."
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that Length of User Data Rx Pdu is equal to Rx Pdu Length - Header Length === */!]
    [!/* !LINKSTO FrNm.ComUserDataSupport.UserDataLength.Error,1 */!]
    [!/* === When Source Node Identifier is enabled === */!]
    [!IF "(as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmComUserDataSupport = 'true') and
          (as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmSourceNodeIdentifierEnabled = 'true') and
          ((as:ref(../FrNmRxPdu/*[FrNmRxPduContainsData = 'true']/FrNmRxPduRef)/PduLength) !=
          ((as:ref(FrNmRxUserDataPduRef)/PduLength) + 2))"!]
      [!ERROR!]"'FrNmSourceNodeIdentifierEnabled' is set to 'true' but length of User Data Rx Pdu for channel '[!"node:name(../..)"!]' is not equal to Rx Pdu Length - 2!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === When Source Node Identifier is disabled === */!]
    [!IF "(as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmComUserDataSupport = 'true') and
          (as:modconf('FrNm')[1]/FrNmGlobalConfig/FrNmGlobalFeatures/FrNmSourceNodeIdentifierEnabled = 'false') and
          ((as:ref(../FrNmRxPdu/*[FrNmRxPduContainsData = 'true']/FrNmRxPduRef)/PduLength) !=
          ((as:ref(FrNmRxUserDataPduRef)/PduLength) + 1))"!]
      [!ERROR!]"'FrNmSourceNodeIdentifierEnabled' is set to 'false' but length of User Data Rx Pdu for channel '[!"node:name(../..)"!]' is not equal to Rx Pdu Length - 1!"
      [!ENDERROR!]
    [!ENDIF!]

    [!/* === Ensure that the routing path of User Data Rx Pdu
             is configured in PduR ===*/!]
    [!/* !LINKSTO FrNm.ComUserDataSupport.PduR_FrNmUserDataRxId,1 */!]
    [!IF "not(node:exists(as:modconf('PduR')[1]/PduRRoutingTables/*/
              PduRRoutingTable/*/PduRRoutingPath/*/PduRSrcPdu[PduRSrcPduRef =
              node:current()/FrNmRxUserDataPduRef]/PduRSourcePduHandleId))"!]
      [!ERROR!]"The routing path of User Data Rx Pdu (FrNmUserDataRxPdu) for channel '[!"node:name(../..)"!]' is not configured in PduR!"
      [!ENDERROR!]
    [!ENDIF!]

  [!ENDSELECT!]

[!ENDLOOP!]

[!ENDNOCODE!][!//

[!ENDIF!]

[!/*
*************************************************************************
* MACRO to get the string for the symbolic name value.
* "ShortNameRef" must reference the container holding the short name.
*************************************************************************
*/!]
[!NOCODE!][!//
[!MACRO "GetSymbolName","ShortNameRef"!][!//
  [!VAR "SymbolName" = "asc:getShortName($ShortNameRef)"!][!//
  [!IF "$SymbolName = ''"!][!//
    [!VAR "SymbolName" = "concat('FrNmConf_',name(../..),'_',name(.))"!][!//
  [!ELSE!][!//
    [!VAR "SymbolName" = "concat('FrNmConf_',name(.),'_',$SymbolName)"!][!//
  [!ENDIF!][!//
[!ENDMACRO!][!//
[!ENDNOCODE!]