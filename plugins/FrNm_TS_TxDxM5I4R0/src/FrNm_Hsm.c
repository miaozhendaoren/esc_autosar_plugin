/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* This file contains the implementation of the hierarchical state machine
 * driver. */

/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 19.10 (required)
 *     Parameter instance shall be enclosed in parentheses.
 *
 *     Reason:
 *     The macro either makes use of the string concatenation of the C
 *     language which does not work with substrings in parentheses or it is
 *     used in function parameter declarations and definitions where the
 *     number of parentheses matter.
 *
 *
 *  MISRA-2) Deviated Rule: 20.9 (required)
 *     The input/output library <stdio.h> shall not be used in production code.
 *
 *     Reason:
 *     Printf(), which is defined inside library <stdio.h> is used in this file,
 *     for the debugging purpose only.
 *
 *  MISRA-3) Deviated Rule: 13.5 (required)
 *     The three expressions of a for statement shall be concerned only with
 *     loop control.
 *
 *     Reason:
 *     Violations of rule 13.5 is false positive, rearranging the condition
 *     check would lead towards unexpected result.
 */

/*==================[inclusions]============================================*/

#include <FrNm_Trace.h>
#include <Std_Types.h>          /* AUTOSAR standard types */

#include <SchM_FrNm.h>           /* SchM symbols for crit. sections */

#include <FrNm_Hsm.h>            /* public API of FrNm_Hsm.c */

/* design debugging output */
#if (FRNM_HSM_TRACE == STD_ON)
/* CHECK: RULE 501 OFF (macro needed in std headers) */
#define __NO_ISOCEXT
#include <stdlib.h>
#include <stdio.h>
/* CHECK: RULE 501 ON */
#endif

/*==================[macros]================================================*/

#if (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
/* Deviation MISRA-1 */
#define FRNM_HSM_DEL_FROM_QUEUE(a,b,c) FrNm_HsmDelFromQueue(a,b,c)
/* Deviation MISRA-1 */
#define FRNM_HSM_TRAN(a,b,c)           FrNm_HsmTran(a,b,c)
/* Deviation MISRA-1 */
#define FRNM_HSM_INIT_SUBSTATES(a,b)   FrNm_HsmInitSubstates(a,b)
/* Deviation MISRA-1 */
#define FRNM_HSM_FIND_EV_TO_DISPATCH(a,b,c,d) FrNm_HsmFindEvToDispatch(a,b,c,d)
/* Deviation MISRA-1 */
#define FRNM_HSM_DISPATCH_EVENT(a,b,c) FrNm_HsmDispatchEvent(a,b,c)

/** \brief Access entry in event queue of a specific hsm instance */
#define FRNM_EV_QUEUE_ENTRY(sc,instIdx,entryIdx) \
  ((sc)->events[((instIdx)*((sc)->numEvents))+(entryIdx)])
#define FRNM_EV_COUNTER_ENTRY(sc,instIdx,entryIdx) \
  ((sc)->evCounters[((instIdx)*((sc)->numEvents))+(entryIdx)])
#else
/* Deviation MISRA-1 */
#define FRNM_HSM_DEL_FROM_QUEUE(a,b,c) FrNm_HsmDelFromQueue(a,c)
/* Deviation MISRA-1 */
#define FRNM_HSM_TRAN(a,b,c)           FrNm_HsmTran(a,c)
#define FRNM_HSM_INIT_SUBSTATES(a,b)   FrNm_HsmInitSubstates(a)
/* Deviation MISRA-1 */
#define FRNM_HSM_FIND_EV_TO_DISPATCH(a,b,c,d) FrNm_HsmFindEvToDispatch(a,c,d)
/* Deviation MISRA-1 */
#define FRNM_HSM_DISPATCH_EVENT(a,b,c) FrNm_HsmDispatchEvent(a,c)
#define FRNM_EV_QUEUE_ENTRY(sc,instIdx,entryIdx) \
  ((sc)->events[(entryIdx)])
#define FRNM_EV_COUNTER_ENTRY(sc,instIdx,entryIdx) \
  ((sc)->evCounters[(entryIdx)])
#endif

#if (FRNM_HSM_TRACE == STD_ON)
#define FRNM_HSM_PRINT1(a)                      \
  do                                            \
  {                                             \
    if (FrNm_HsmTraceEnabled == TRUE)           \
    {                                           \
      /* Deviation MISRA-1 */                   \
      (void)printf("HsmTrace: " a);             \
    }                                           \
  } while (0U)
#define FRNM_HSM_PRINT2(a,b)                    \
  do                                            \
  {                                             \
    if (FrNm_HsmTraceEnabled == TRUE)           \
    {                                           \
      /* Deviation MISRA-1 */                   \
      (void)printf("HsmTrace: " a,(b));         \
    }                                           \
  } while (0U)
#define FRNM_HSM_PRINT3(a,b,c)                  \
  do                                            \
  {                                             \
    if (FrNm_HsmTraceEnabled == TRUE)           \
    {                                           \
      /* Deviation MISRA-1 */                   \
      (void)printf("HsmTrace: " a,(b),(c));     \
    }                                           \
  } while (0U)
#define FRNM_HSM_PRINT4(a,b,c,d)                \
  do                                            \
  {                                             \
    if (FrNm_HsmTraceEnabled == TRUE)           \
    {                                           \
      /* Deviation MISRA-1 */                   \
      (void)printf("HsmTrace: " a,(b),(c),(d)); \
    }                                           \
  } while (0U)
#define FRNM_HSM_PRINT5(a,b,c,d,e)              \
  do                                            \
  {                                             \
    if (FrNm_HsmTraceEnabled == TRUE)           \
    {                                           \
      /* Deviation MISRA-1 */                   \
      (void)printf("HsmTrace: " a,(b),(c),(d),(e));   \
    }                                           \
  } while (0U)
#else
#define FRNM_HSM_PRINT1(a)          /* nothing */
#define FRNM_HSM_PRINT2(a,b)        /* nothing */
#define FRNM_HSM_PRINT3(a,b,c)      /* nothing */
#define FRNM_HSM_PRINT4(a,b,c,d)    /* nothing */
#define FRNM_HSM_PRINT5(a,b,c,d,e)  /* nothing */
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

#define FRNM_START_SEC_CODE
#include <MemMap.h>

/** \brief Delete entry from event queue
 ** \param sc pointer to state chart
 ** \param instIdx index of state machine instance to work on
 ** \param queueEntryNo entry in queue to delete
 **
 ** This function must be called inside of a critical section which prevents
 ** the concurrent access to the queue.
 **/
STATIC FUNC(void, FRNM_CODE) FRNM_HSM_DEL_FROM_QUEUE(
  const FrNm_HsmStatechartPtrType sc,
  const uint8                    instIdx,
  const uint8                    queueEntryNo);

/** \brief Execute a transition
 ** \param sc pointer to state chart
 ** \param instIdx index of state machine instance to work on
 ** \param tran Pointer to the transition description struct */
STATIC FUNC(void, FRNM_CODE) FRNM_HSM_TRAN(
  const FrNm_HsmStatechartPtrType                      sc,
  const uint8                                         instIdx,
  CONSTP2CONST(FrNm_HsmTranType, AUTOMATIC, FRNM_CONST) tran);

/** \brief Initialize all substates for the current state
 ** \param sc pointer to state chart to work on
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, FRNM_CODE) FRNM_HSM_INIT_SUBSTATES(
  const FrNm_HsmStatechartPtrType sc,
  const uint8                    instIdx);

/** \brief Look for dispatchable event in event queue
 ** \param sc pointer to state chart to work on
 ** \param instIdx index of state machine instance to work on
 ** \param event identified event from queue
 ** \param queueEntryNo index in queue for identified event
 ** \return look up status
 ** \retval TRUE an event is found in the queue and it is not deferred
 ** and not ignored
 ** \retval FALSE no event is not found or one which is deferred or one
 ** which is ignored in the current state
 **
 ** Events which are marked to be ignored in the current state are deleted
 ** from the queue within this function. */
STATIC FUNC(boolean, FRNM_CODE) FRNM_HSM_FIND_EV_TO_DISPATCH(
  const FrNm_HsmStatechartPtrType                     sc,
  const uint8                                        instIdx,
  CONSTP2VAR(FrNm_HsmEventType, AUTOMATIC, AUTOMATIC) event,
  CONSTP2VAR(uint8, AUTOMATIC, AUTOMATIC)            queueEntryNo);

/** \brief Dispatch event from event queue
 ** \param sc pointer to state chart to work on
 ** \param instIdx index of state machine instance to work on
 ** \param event identified event from queue
 ** \return Transition fired indicator
 ** \retval TRUE the event dispatching resulted in an fired transition
 ** \retval FALSE else */
STATIC FUNC(boolean, FRNM_CODE) FRNM_HSM_DISPATCH_EVENT(
  const FrNm_HsmStatechartPtrType sc,
  const uint8                    instIdx,
  const FrNm_HsmEventType         event);

#define FRNM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

#if (FRNM_HSM_TRACE == STD_ON)

#define FRNM_START_SEC_VAR_INIT_BOOLEAN
#include <MemMap.h>

STATIC VAR(boolean, FRNM_VAR) FrNm_HsmTraceEnabled = FALSE;

#define FRNM_STOP_SEC_VAR_INIT_BOOLEAN
#include <MemMap.h>

#endif

/*==================[external function definitions]=========================*/

#define FRNM_START_SEC_CODE
#include <MemMap.h>

/*------------------ init functions ----------------------------------------*/

#if (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
FUNC(void, FRNM_CODE) FrNm_HsmInit(const FrNm_HsmStatechartPtrType sc)
{
  uint8 instIdx;
  for (instIdx = 0U; instIdx < sc->numInstances; ++instIdx)
  {
    FRNM_HSMINITINST(sc, instIdx);
  }
}
/* else: function is implemented as macro and maps to FrNm_HsmInitInst() */
#endif

FUNC(void, FRNM_CODE) FRNM_HSMINITINST(
  const FrNm_HsmStatechartPtrType sc,
  const uint8                    instIdx)
{
  CONSTP2VAR(FrNm_HsmInstType, AUTOMATIC, FRNM_VAR_NOINIT) inst
    = &FRNM_INST_ACCESS(sc->instances, instIdx);
  uint8 i;

  FrNm_HsmStateIdxType oldState;

  /* Deviation MISRA-2 */
  FRNM_HSM_PRINT3("%s %d Init\n", sc->hsmName, FRNM_INST(instIdx));

  /* init all fields because the struct is placed in a NOINIT memory
   * section */
  oldState = inst->stateId;
  inst->stateId  = sc->topStateId;
  inst->sourceId = FRNM_HSM_SIDX_INVALID;

  for (i = 0U; i < sc->numEvents; ++i)
  {
    FRNM_EV_QUEUE_ENTRY(sc, instIdx, i) = 0U;
    FRNM_EV_COUNTER_ENTRY(sc, instIdx, i) = 0U;
  }
  inst->evQueueFill       = 0U;
  inst->evQueueInsertEvNo = 0U;

  /* Deviation MISRA-2 */
  FRNM_HSM_PRINT4(
    "%s %d %s\n",
    sc->hsmName, FRNM_INST(instIdx), sc->actionNames[inst->stateId]);

  /* enter top state */
  sc->actions[sc->states[inst->stateId].entryFnIdx](FRNM_PL_SF(instIdx));

  /* init all substates */
  FRNM_HSM_INIT_SUBSTATES(sc, instIdx);
  DBG_FRNM_GENERIC_GRP((sc->stateChartId), (instIdx), (oldState), (inst->stateId));
}

/*------------------ emit functions ----------------------------------------*/

FUNC(void, FRNM_CODE) FRNM_HSMEMITINST(
  const FrNm_HsmStatechartPtrType sc,
  const uint8                    instIdx,
  const FrNm_HsmEventType         event)
{
  CONSTP2VAR(FrNm_HsmInstType, AUTOMATIC, FRNM_VAR_NOINIT) inst
    = &FRNM_INST_ACCESS(sc->instances, instIdx);


  SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
  /* in case the event has already been queued don't add it again to the queue*/
  if ( FRNM_EV_COUNTER_ENTRY(sc, instIdx, event) < 1U )
  {
    FRNM_EV_QUEUE_ENTRY(sc, instIdx, inst->evQueueFill) = event;
    inst->evQueueFill++;
    /* leave evQueueInsertEvNo unchanged, this pointer will be updated on
     * insertion or event dispatching */
    ++FRNM_EV_COUNTER_ENTRY(sc, instIdx, event);

    /* Deviation MISRA-2 */
    FRNM_HSM_PRINT4(
      "%s %d event %s enqueued\n",
      sc->hsmName, FRNM_INST(instIdx), sc->eventNames[event]);
  }

  SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
}

/*------------------ main functions ----------------------------------------*/

FUNC(boolean, FRNM_CODE) FRNM_HSMMAININST(
  const FrNm_HsmStatechartPtrType sc,
  const uint8                    instIdx)
{
  CONSTP2VAR(FrNm_HsmInstType, AUTOMATIC, FRNM_VAR_NOINIT) inst
    = &FRNM_INST_ACCESS(sc->instances,instIdx);
  /* return value, flag indicating if at least one transition fired */
  boolean          transitionFired = FALSE;
  FrNm_HsmEventType event;
  uint8            queueEntryNo;
  boolean          eventPending;

  /* Deviation MISRA-2 */
  FRNM_HSM_PRINT3(
    "%s %d Main\n",
    sc->hsmName, FRNM_INST(instIdx));

  /* reset event insertion pointer to an invalid value, this will be set to a
   * valid location if the first event is being dispatched */
  inst->evQueueInsertEvNo = 0U;

  /* check if there are is at least one pending event */
  eventPending
    = FRNM_HSM_FIND_EV_TO_DISPATCH(sc, instIdx, &event, &queueEntryNo);

  do
  {
    /* dispatch all pending events from event queue */
    while (TRUE == eventPending)
    {
      const boolean retVal = FRNM_HSM_DISPATCH_EVENT(sc, instIdx, event);
      transitionFired = (retVal || transitionFired);

      /* delete dispatched event from queue */
      SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
      FRNM_HSM_DEL_FROM_QUEUE(sc, instIdx, queueEntryNo);
      SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

      eventPending
        = FRNM_HSM_FIND_EV_TO_DISPATCH(sc, instIdx, &event, &queueEntryNo);
    }

    /* transitions perfomed, execute do actions of leaf and all parent
     * states */
    {
      FrNm_HsmStateIdxType stateId = inst->stateId;
      do
      {
        CONSTP2CONST(FrNm_HsmStateType, AUTOMATIC, FRNM_CONST) state
          = &sc->states[stateId];

        if (FRNM_HSM_AIDX_INVALID != state->doFnIdx)
        {
          /* Deviation MISRA-2 */
          FRNM_HSM_PRINT4(
            "%s %d %s\n",
            sc->hsmName, FRNM_INST(instIdx), sc->actionNames[state->doFnIdx]);
          sc->actions[state->doFnIdx](FRNM_PL_SF(instIdx));
        }

        stateId = state->parentId; /* goto parent state */
      } while (FRNM_HSM_SIDX_INVALID != stateId);
    }

    /* the do actions may have emitted another event, recheck ev. queue */
    eventPending
      = FRNM_HSM_FIND_EV_TO_DISPATCH(sc, instIdx, &event, &queueEntryNo);
  } while (TRUE == eventPending);

  return transitionFired;
}

#if (FRNM_HSM_TRACE == STD_ON)
FUNC(void, FRNM_CODE) FrNm_HsmSetTracing(const boolean newValue)
{
  FrNm_HsmTraceEnabled = newValue;
}
#endif

#define FRNM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function definitions]=========================*/

#define FRNM_START_SEC_CODE
#include <MemMap.h>

STATIC FUNC(void, FRNM_CODE) FRNM_HSM_DEL_FROM_QUEUE(
  const FrNm_HsmStatechartPtrType sc,
  const uint8                    instIdx,
  const uint8                    queueEntryNo)
{
  /* delete event no queueEntryNo from queue */
  CONSTP2VAR(FrNm_HsmInstType, AUTOMATIC, FRNM_VAR_NOINIT) inst
    = &FRNM_INST_ACCESS(sc->instances, instIdx);
  uint8 j;

  /* reduce the occurrence counter of the respective event */
  FrNm_HsmEventType event = FRNM_EV_QUEUE_ENTRY(sc, instIdx, queueEntryNo);
  --FRNM_EV_COUNTER_ENTRY(sc, instIdx, event);

  /* Deviation MISRA-3 */
  for (j = queueEntryNo; (j + 1U) < inst->evQueueFill; ++j)
  {
    FRNM_EV_QUEUE_ENTRY(sc, instIdx, j)
      = FRNM_EV_QUEUE_ENTRY(sc, instIdx, j + 1U);
  }
  --inst->evQueueFill;
  if (queueEntryNo < inst->evQueueInsertEvNo)
  {
    /* if this was an event emitted to self also decrement the insertion
     * pointer  */
    --inst->evQueueInsertEvNo;
  }
  /* no need to check for queueEntryNo >= inst->evQueueInsertEvNo here.  This
   * will be done before dispatching the next event from the queue */
}

STATIC FUNC(void, FRNM_CODE) FRNM_HSM_TRAN(
  const FrNm_HsmStatechartPtrType                      sc,
  const uint8                                         instIdx,
  CONSTP2CONST(FrNm_HsmTranType, AUTOMATIC, FRNM_CONST) tran)
{
  P2CONST(FrNm_HsmStateType, AUTOMATIC, FRNM_CONST) state;
  CONSTP2VAR(FrNm_HsmInstType, AUTOMATIC, FRNM_VAR_NOINIT) inst
      = &FRNM_INST_ACCESS(sc->instances, instIdx);

  FrNm_HsmStateIdxType oldState = inst->stateId;

  /* perform simplified transition for internal transitions */
  if (tran->target == FRNM_HSM_SIDX_INVALID)
  {
    /* Deviation MISRA-2 */
    FRNM_HSM_PRINT4(
      "%s %d %s (internal transition)\n",
      sc->hsmName, FRNM_INST(instIdx), sc->actionNames[tran->steps[0U]]);
    sc->actions[tran->steps[0U]](FRNM_PL_SF(instIdx));
  }
  else /* external transitions */
  {
    uint8_least i;

    /* "source" may be a superstate of "state".  exit all states from "state"
     * until "source" is reached */
    while (inst->stateId != inst->sourceId)
    {
      state = &sc->states[inst->stateId];

      if (state->exitFnIdx != FRNM_HSM_AIDX_INVALID)
      {
        /* Deviation MISRA-2 */
        FRNM_HSM_PRINT4(
          "%s %d %s\n",
          sc->hsmName, FRNM_INST(instIdx), sc->actionNames[state->exitFnIdx]);
        sc->actions[state->exitFnIdx](FRNM_PL_SF(instIdx));
      }

      inst->stateId = state->parentId;
    }

    /* execute exit/transiton/entry actions on transition path */
    for (i = 0U; i < tran->numSteps; ++i)
    {
      /* Deviation MISRA-2 */
      FRNM_HSM_PRINT4(
        "%s %d %s\n",
        sc->hsmName, FRNM_INST(instIdx), sc->actionNames[tran->steps[i]]);
      sc->actions[tran->steps[i]](FRNM_PL_SF(instIdx));
    }

    /* set the new current state and perform init steps of substates */
    inst->stateId = tran->target; /* current state is last entered state */
    FRNM_HSM_INIT_SUBSTATES(sc, instIdx);

    /* Deviation MISRA-2 */
    FRNM_HSM_PRINT4(
      "%s %d transition to state %s finished\n",
      sc->hsmName, FRNM_INST(instIdx), sc->stateNames[inst->stateId]);
  }

  DBG_FRNM_GENERIC_GRP((sc->stateChartId), (instIdx), (oldState), (inst->stateId));
}

STATIC FUNC(void, FRNM_CODE) FRNM_HSM_INIT_SUBSTATES(
  const FrNm_HsmStatechartPtrType sc,
  const uint8                    instIdx)
{
  CONSTP2VAR(FrNm_HsmInstType, AUTOMATIC, FRNM_VAR_NOINIT) inst
    = &FRNM_INST_ACCESS(sc->instances,instIdx);

  FrNm_HsmStateIdxType stateIdx = sc->states[inst->stateId].initId;

  while (stateIdx != FRNM_HSM_SIDX_INVALID) /* init state if necessary */
  {
    P2CONST(FrNm_HsmStateType, AUTOMATIC, FRNM_CONST) state
      = &sc->states[stateIdx];

    inst->stateId = stateIdx; /* set new current state */
    /* enter the substate */
    /* Deviation MISRA-2 */
    FRNM_HSM_PRINT4(
      "%s %d %s\n",
      sc->hsmName, FRNM_INST(instIdx), sc->actionNames[state->entryFnIdx]);
    sc->actions[state->entryFnIdx](FRNM_PL_SF(instIdx));

    /* get next init state */
    stateIdx = state->initId;
  }
}

STATIC FUNC(boolean, FRNM_CODE) FRNM_HSM_FIND_EV_TO_DISPATCH(
  const FrNm_HsmStatechartPtrType                     sc,
  const uint8                                        instIdx,
  CONSTP2VAR(FrNm_HsmEventType, AUTOMATIC, AUTOMATIC) event,
  CONSTP2VAR(uint8, AUTOMATIC, AUTOMATIC)            queueEntryNo)
{
    CONSTP2VAR(FrNm_HsmInstType, AUTOMATIC, FRNM_VAR_NOINIT) inst
      = &FRNM_INST_ACCESS(sc->instances,instIdx);
    CONSTP2CONST(FrNm_HsmStateType, AUTOMATIC, FRNM_CONST) state
      = &sc->states[inst->stateId];
    boolean          foundEventToDispatch = FALSE;
    FrNm_HsmEventType ev                   = FRNM_HSM_EVENT_INVALID;
    uint8            entryNo              = 0U;

    /* lock event queue access */
    SchM_Enter_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();
    while ((entryNo < inst->evQueueFill)
           && (FALSE == foundEventToDispatch))
    {
      FrNm_HsmEventMaskType eventMask;
      ev        = FRNM_EV_QUEUE_ENTRY(sc, instIdx, entryNo);
      eventMask = (FrNm_HsmEventMaskType)1UL << ev;

      if ((eventMask & state->ignoredEventMask) != 0U)
      {
        /* Deviation MISRA-2 */
        FRNM_HSM_PRINT4(
          "%s %d event %s ignored\n",
          sc->hsmName, FRNM_INST(instIdx), sc->eventNames[ev]);

        /* delete event no 'entryNo' from queue */
        FRNM_HSM_DEL_FROM_QUEUE(sc, instIdx, entryNo);
        /* do not increment entryNo */
      }
      /*
       * There are no deffered events in the statemachine so
       * this branch corresponds to action events.
       * That is, eventMask & state->actionEventMask will yield
       * a value different than 0.
       */
      else
      {
        /*  if the insertion location is still unset (or pointing to an
         *  invalid location) then set the insertion location to just after
         *  the current event to be dispatched.
         *  Note: the insertion location is used only when events are emitted
         *  to self.
         */
        inst->evQueueInsertEvNo = entryNo + 1U;
        /* leave while loop and dispatch event no entryNo */
        foundEventToDispatch = TRUE;
      }
    }
    SchM_Exit_FrNm_SCHM_FRNM_EXCLUSIVE_AREA_0();

    *event        = ev;
    *queueEntryNo = entryNo;
    return foundEventToDispatch;
}

STATIC FUNC(boolean, FRNM_CODE) FRNM_HSM_DISPATCH_EVENT(
  const FrNm_HsmStatechartPtrType sc,
  const uint8                    instIdx,
  const FrNm_HsmEventType         event)
{
  CONSTP2VAR(FrNm_HsmInstType, AUTOMATIC, FRNM_VAR_NOINIT) inst
    = &FRNM_INST_ACCESS(sc->instances,instIdx);
  P2CONST(FrNm_HsmTranType, AUTOMATIC, FRNM_CONST) trans = NULL_PTR;
  /* return value, flag indicating if at least one transition fired */
  boolean transitionFired = FALSE;
  /* flag for the trigger search algorithm */
  boolean triggerFound    = FALSE;

  /* *** search for transition for non-deferred and non-ignored event and
   * fire the transition if found *** */

  /* Deviation MISRA-2 */
  FRNM_HSM_PRINT5(
    "%s %d dispatching event %s in state %s\n",
    sc->hsmName, FRNM_INST(instIdx), sc->eventNames[event],
    sc->stateNames[inst->stateId]);

  /* try current leaf state as src of transitions */
  inst->sourceId = inst->stateId;

  /* start searching for a triggering event */
  do
  {
    uint8_least i;          /* index in transition array of state */

    CONSTP2CONST(FrNm_HsmStateType, AUTOMATIC, FRNM_CONST) state
      = &sc->states[inst->sourceId];

    /* check all possible transitions of state */
    for (i = 0U; (i < state->numTrans) && (FALSE == triggerFound); ++i)
    {
      trans = &state->trans[i]; /* test next transition */

      if (event == trans->event)
      {
        /* evaluate guard condition */
        boolean guardCond = TRUE;

        if (trans->guardFnIdx != FRNM_HSM_GIDX_INVALID)
        {
          guardCond = sc->guards[trans->guardFnIdx](FRNM_PL_SF(instIdx));
          /* Deviation MISRA-2 */
          FRNM_HSM_PRINT5(
            "%s %d %s evaluates to %s\n",
            sc->hsmName, FRNM_INST(instIdx),
            sc->guardNames[trans->guardFnIdx], (guardCond?"TRUE":"FALSE"));
        }

        if (TRUE == guardCond)
        {
          /* exit for loop and middle do-while loop in any case because we
           * have found at least one enabled transition for this event. */
          triggerFound    = TRUE;

          /* remember that some real event processing e.g. a state machine
           * action happened. In this case a subsequent call of this or other
           * dispatching functions may be necessary */
          transitionFired = TRUE;

          /* event+guard is OK: transition action is perfomed later after
           * loops are left and stack usage is lower */
        }
        /* else: if guard is false, then treat it as if the related
         * transition is not existing in the state chart model, "Practical
         * Statecharts", 2.ed, sect. 2.3.15  */
      }
    }

    if (FALSE == triggerFound)
    {
      /* we did not find the trigger in this state: try parent state as next
       * source state */
      inst->sourceId = state->parentId;
    }
  } while ((FALSE == triggerFound)
           && (FRNM_HSM_SIDX_INVALID != inst->sourceId));

  /* a triggering event was found and transition is ready to be executed or
   * the top state is reached the queued event must be ignored
   *
   * we have to check if the top state is not yet reached at runtime because
   * there may still be non-ignored and not handled events due to false
   * guard conditions */

  if (TRUE == triggerFound)
  {
    FRNM_HSM_TRAN(sc, instIdx, trans); /* execute the transition */
  }
  else
  {
    /* Deviation MISRA-2 */
    FRNM_HSM_PRINT4(
      "%s %d event %s ignored at top state\n",
      sc->hsmName, FRNM_INST(instIdx), sc->eventNames[event]);
  }

  return transitionFired;
}

#define FRNM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]===========================================*/
