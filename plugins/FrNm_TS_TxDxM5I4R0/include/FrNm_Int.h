/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#ifndef FRNM_INT_H
#define FRNM_INT_H

/*==================[inclusions]=============================================*/
#include <NmStack_Types.h>      /* Nm specific types */
#include <FrNm_Types.h>
#include <FrNm.h>              /* Types and decl. from public API */
#include <FrNm_Hsm.h>
#include <FrNm_Cfg.h>          /* Module configuration */
#include <ComStack_Types.h>     /* Include for PduInfoType */

/*==================[macros]================================================*/

/* Service IDs */
#define FRNM_INIT_ID                        0x00U
#define FRNM_PASSIVESTARTUP_ID              0x01U
#define FRNM_NETWORKREQUEST_ID              0x02U
#define FRNM_NETWORKRELEASE_ID              0x03U
#define FRNM_ENABLECOMMUNICATION_ID         0x05U
#define FRNM_SETUSERDATA_ID                 0x06U
#define FRNM_GETUSERDATA_ID                 0x07U
#define FRNM_GETPDUDATA_ID                  0x08U
#define FRNM_REPEATMESSAGEREQUEST_ID        0x09U
#define FRNM_GETNODEIDENTIFIER_ID           0x0aU
#define FRNM_GETLOCALNODEIDENTIFIER_ID      0x0bU
#define FRNM_DISABLECOMMUNICATION_ID        0x0cU
#define FRNM_REQUESTBUSSYNCHRONIZATION_ID   0xc0U
#define FRNM_CHECKREMOTESLEEPINDICATION_ID  0x0dU
#define FRNM_GETSTATE_ID                    0x0eU
#define FRNM_GETVERSIONINFO_ID              0x0fU
#define FRNM_RXINDICATION_ID                0x42U
#define FRNM_TRIGGERTRANSMIT_ID             0x41U
#define FRNM_TXCONFIRMATION_ID              0x40U
#define FRNM_MAINFUNCTION_ID                0xf0U

/* Pdu schedule variants */

#define  FRNM_PDU_SCHEDULE_VARIANT_1  0x01U
#define  FRNM_PDU_SCHEDULE_VARIANT_2  0x02U
#define  FRNM_PDU_SCHEDULE_VARIANT_3  0x03U
#define  FRNM_PDU_SCHEDULE_VARIANT_4  0x04U
#define  FRNM_PDU_SCHEDULE_VARIANT_5  0x05U
#define  FRNM_PDU_SCHEDULE_VARIANT_6  0x06U
#define  FRNM_PDU_SCHEDULE_VARIANT_7  0x07U

/* Bit mask for Pdu schedule variants */
#define  FRNM_PDU_SCHEDULE_VARIANT_MASK 0x07U

/* Bit mask for PN info bit (CRI) */
#define  FRNM_CBV_PNINFOBIT_MASK    0x40U

/* Bit mask for Sleep Ready bit */
#define  FRNM_CBV_SLEEPREADYBIT_MASK 0x08U

/* Bit mask for Active wakeUp bit */
#define  FRNM_CBV_ACTIVEWAKEUPBIT   0x10U

/* Bit masks for ChannelProperty */
/* Bit mask for control bit vector */
#define FRNM_CONTROL_BIT_VECTOR        0x08U
/* Bit mask for repeat message bit */
#define FRNM_REPEAT_MESSAGE_BIT        0x10U
/* Bit mask for synchronization bit */
#define FRNM_SYNCH_POINT_ENABLED       0x20U
/* Bit mask for vote inhibition bit */
#define FRNM_VOTE_INHIBITION_ENABLED   0x40U
/* Bit mask for data bit */
#define FRNM_NM_DATA_ENABLED           0x80U

/*Rx Pdu types*/
#define FRNM_VOTE  0x00U
#define FRNM_DATA  0x01U
#define FRNM_MIXED 0x02U

/* Flex Ray schedule cycles used to define voting cycle or data cycle.*/
#define FRNM_CYCLE_VALUE_1  1U
#define FRNM_CYCLE_VALUE_2  2U
#define FRNM_CYCLE_VALUE_4  4U
#define FRNM_CYCLE_VALUE_8  8U
#define FRNM_CYCLE_VALUE_16 16U
#define FRNM_CYCLE_VALUE_32 32U
#define FRNM_CYCLE_VALUE_64 64U

/* FRNM_RESERVED_BYTES says how many bytes are reserved in FrNm data buffer
 * which can not be used for the user data.
 * First byte of the data buffer is reserved for Nm vote and/or Control Bit vector.
 * If FRNM_SOURCE_NODE_IDENTIFIER_ENABLED = STD_OFF, this means FrNm doesn't
 * send nodeId of the channel while sending Nm data. i.e second byte is not reserved.
 * If FRNM_SOURCE_NODE_IDENTIFIER_ENABLED = STD_ON, second byte also is reserved.
 */

#if (FRNM_SOURCE_NODE_IDENTIFIER_ENABLED == STD_OFF)
#define FRNM_RESERVED_BYTES 0x01U
#else
#define FRNM_RESERVED_BYTES 0x02U
#endif /* FRNM_SOURCE_NODE_IDENTIFIER_ENABLED */

/* Value for repeat message bit in the Pdu byte 0 */
#define FRNM_PDU_REPEAT_MSG_BIT 0x01U

/* Value for vote bit in the Pdu byte 0 */
#define FRNM_PDU_VOTE_BIT 0x80U

/** \brief Macro to create a bitmask with single bit set at specified position
 ** (BitPos)
 **
 ** \param[in] BitPos - Bit position */
#define FRNM_BITPOS2MASK(BitPos) ((uint8)(1U << (BitPos)))


/* Internal State */
/* The numbers describe bit positions */
#define FRNM_NETWORK_REQUEST_BIT           1U
#define FRNM_NETWORK_PASSIVE_START_BIT     2U
#define FRNM_STARTUP_ERROR_BIT             3U
#define FRNM_REPEAT_MSG_REQUEST_BIT        4U
#define FRNM_UNI_TIMEOUT_BIT               5U
#define FRNM_REMOTE_SLEEP_IND_BIT          6U
#define FRNM_NODE_DETECTION_LOCK_BIT       7U


/** \brief Bitmasks for internal states upated in ChanStatus */
#define FRNM_NETWORK_REQUESTED            \
   FRNM_BITPOS2MASK(FRNM_NETWORK_REQUEST_BIT)
#define FRNM_NETWORK_PASSIVE_START            \
   FRNM_BITPOS2MASK(FRNM_NETWORK_PASSIVE_START_BIT)
#define FRNM_NETWORK_STARTED  ((uint8)(FRNM_NETWORK_REQUESTED | FRNM_NETWORK_PASSIVE_START))
#define FRNM_STARTUP_ERROR            \
   FRNM_BITPOS2MASK(FRNM_STARTUP_ERROR_BIT)
#define FRNM_REPEAT_MSG_REQUESTED            \
   FRNM_BITPOS2MASK(FRNM_REPEAT_MSG_REQUEST_BIT)
#define FRNM_UNI_TIMEOUT_PASSED            \
   FRNM_BITPOS2MASK(FRNM_UNI_TIMEOUT_BIT)
#define FRNM_REMOTE_SLEEP_INDICATION            \
   FRNM_BITPOS2MASK(FRNM_REMOTE_SLEEP_IND_BIT)

#define FRNM_NODE_DETECTION_LOCK            \
   FRNM_BITPOS2MASK(FRNM_NODE_DETECTION_LOCK_BIT)


/* Defines for initialisation status */
#define FRNM_UNINIT 0U
#define FRNM_INIT   1U


#if (FRNM_HSM_INST_MULTI_ENABLED == STD_ON)
#define FRNM_CHANNEL_STATUS(a)        FrNm_ChanStatus[(a)]
#define FRNM_CHANNEL_CONFIG(a)        FrNm_ChanConfig[(a)]
#define FRNM_CHANNELTIME_CONFIG(a)    FrNm_ChanConfig[(a)].ChannelTiming
#define FRNM_CHANNELID_CONFIG(a)      FrNm_ChanConfig[(a)].ChannelIdentifiers
#else
#define FRNM_CHANNEL_STATUS(a)        FrNm_ChanStatus[0]
#define FRNM_CHANNEL_CONFIG(a)        FrNm_ChanConfig[0]
#define FRNM_CHANNELTIME_CONFIG(a)    FrNm_ChanConfig[0].ChannelTiming
#define FRNM_CHANNELID_CONFIG(a)      FrNm_ChanConfig[0].ChannelIdentifiers
#endif

/* Value for Invalid Index for Eira timer array */
#define FRNM_EIRA_INVALID_INDEX 0xFFU

/*==================[type definitions]=======================================*/

/** \brief Definition of the FrNm_TimeType */
typedef uint16 FrNm_TimeType;

/** \brief Definition of the FrNm_NodeIdType */
typedef uint8 FrNm_NodeIdType;

/** \brief Type for the channel state
 **
 ** This type stores the dynamic data of a FrNm channel; e.g. timers
 ** and states. */
typedef struct
{
  Nm_StateType     CurState;        /**< Current state of a cluster */
  FrNm_TimeType    UniversalTimer;  /**< Timer for repeat message time and remote sleep time */
  FrNm_TimeType    TimeoutTimer;    /**< Timer for detecting transmisson timeout */
  /* !LINKSTO FRNM309,1 */
  FrNm_TimeType    ReadySleepTimer; /**< Timer for ready sleep time */
  uint8            ActCycle;        /**< Current FlexRay cycle read from FrIf */
  uint8            ChanStatus;      /**< See  bitmasks for internal States.
                                         This variable is not protected against concurrent access
                                         and must only be accessed from within the main function
                                         context */
  boolean          ComEnabled;      /**< Store communication control request from upper layer.
                                         This variable is accessed concurrently between
                                         FrNm_Mainfunction context and other API's.
                                         Set to FALSE in FrNm_Mainfunction context when bus
                                         synchronization is lost. In this case, FrNm will go to
                                         either Bus Sleep State or Synchronous State. But set back
                                         to TRUE while entering 'NetworkMode'.
                                         Disable or Enable API's can set this variable only if
                                         FrNm is already in 'NetworkMode'. We do not expect
                                         synchronization error when transmission is disabled.
                                         Therefore, protection against concurrent access of this
                                         variable is not required.*/
#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
  uint8             PnInfoEira[FRNM_PN_INFO_LENGTH];
#endif
} FrNm_ChanStatusType;

/** \brief Definition of the FrNm_UserDataConfigType */
#if (FRNM_COM_USER_DATA_SUPPORT ==STD_ON)
typedef struct
{
  PduIdType      ComUserTxPduId;      /**< Tx PDU for Com user data defined
                                           by EcuC */
  PduIdType      ComUserTxConfPduId;  /**< Tx Confirmation PduId for Com
                                           user data defined by EcuC */
  PduIdType      ComUserRxPduId;      /**< Rx PDU for Com user data defined
                                           by EcuC */
  PduLengthType  PduLength;           /**< PDU length defined by EcuC */
  boolean        ComUserTxPduEnabled; /**< Flag indicates whether Tx PDU
                                           for Com user data configured */
  boolean        ComUserRxPduEnabled; /**< Flag indicates whether Rx PDU
                                           for Com user data configured */
}FrNm_UserDataConfigType;
#endif

#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
/** \brief Definition of FrNm_PnStatusType */
typedef struct
{
  FrNm_TimeType     EiraTimer[FRNM_EIRA_TIMER_SIZE];
  uint8             EiraValue[FRNM_PN_INFO_LENGTH];
}FrNm_PnStatusType;
#endif

/*==================[internal function declarations]=========================*/
/*==================[internal constants]=====================================*/
/*==================[external data]==========================================*/

#define FRNM_START_SEC_VAR_INIT_8
#include <MemMap.h>

/** \brief Intialization information of FrNm module */
extern VAR(uint8, FRNM_VAR) FrNm_InitStatus;

#define FRNM_STOP_SEC_VAR_INIT_8
#include <MemMap.h>

#define FRNM_START_SEC_VAR_FAST_NO_INIT_UNSPECIFIED
#include <MemMap.h>

/** \brief Declaration of the extern variable FrNm_ChanStatus */
extern VAR(FrNm_ChanStatusType, FRNM_VAR_FAST)
   FrNm_ChanStatus[FRNM_NUMBER_OF_CHANNELS];\

#define FRNM_STOP_SEC_VAR_FAST_NO_INIT_UNSPECIFIED
#include <MemMap.h>

#define FRNM_START_SEC_VAR_INIT_BOOLEAN
#include <MemMap.h>

extern VAR(boolean, FRNM_VAR_FAST) FrNm_SetActiveWakeUpFlag;

#define FRNM_STOP_SEC_VAR_INIT_BOOLEAN
#include <MemMap.h>

/*==================[external constants]=====================================*/
#define  FRNM_START_SEC_CONST_8
#include <MemMap.h>

#if (FRNM_PN_ENABLED == STD_ON)
extern CONST(uint8, FRNM_CONST)FrNm_PnFilterMask[FRNM_PN_INFO_LENGTH];
#endif
#define  FRNM_STOP_SEC_CONST_8
#include <MemMap.h>

#define FRNM_START_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

#if (FRNM_COM_USER_DATA_SUPPORT ==STD_ON)
extern CONST(FrNm_UserDataConfigType, FRNM_CONST) FrNm_ComUserDataInfo[FRNM_NUMBER_OF_CHANNELS];
#endif

/** \brief Configuration for FrNm clusters
*/
extern CONST(FrNm_ChnConfigType, FRNM_CONST) FrNm_ChanConfig[FRNM_NUMBER_OF_CHANNELS];

/** \brief Array with first Controller for each FrNm cluster
*/
extern CONST(uint8, FRNM_CONST) FrNm_CtrlIdx[FRNM_NUMBER_OF_CHANNELS];

/** \brief RxPdu to FrNmChannelHandle / Cluster mapping.
*/
extern CONST (FrNm_PduType, FRNM_CONST) FrNm_RxPduMap[FRNM_NUMBER_OF_RX_PDUS];

#if ((FRNM_ENABLE_NMDATA == STD_ON) \
   ||(FRNM_ENABLE_NMMIXED == STD_ON) \
   ||(FRNM_ENABLE_NMVOTE == STD_ON))
/** \brief TxPdu to FrNmChannelHandle / Cluster mapping.
*/
extern CONST (FrNm_TxPduType, FRNM_CONST) FrNm_TxPduMap[FRNM_NUMBER_OF_TX_PDUS];
#endif

#define FRNM_STOP_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

/*==================[external function declarations]=========================*/
#define FRNM_START_SEC_CODE
#include <MemMap.h>
extern FUNC(void, FRNM_CODE) FrNm_MainFunction
(
  const uint8 instIdx
);

#if (FRNM_PN_EIRA_CALC_ENABLED == STD_ON)
extern FUNC(void, FRNM_CODE) FrNm_HandlePnTimers
(
  void
);
#endif

#if ((FRNM_ENABLE_NMVOTE == STD_ON) || (FRNM_ENABLE_NMMIXED == STD_ON))
/** \brief Handle transmission of Nm vote with or without Nm data.
 ** \param instIdx Index to the channel clusters.
 */
FUNC(void, FRNM_CODE) FrNm_HandleVoteCycle
(
  FRNM_PDL_SF(const uint8 instIdx)
);
#endif
#if (FRNM_ENABLE_NMDATA == STD_ON)
/** \brief Handle transmission of Nm data.
 ** \param instIdx Index to the channel clusters.
 */
FUNC(void, FRNM_CODE) FrNm_HandleDataCycle
(
  FRNM_PDL_SF(const uint8 instIdx)
);
#endif

/** \brief Handle start up error and global time error.
 ** \param instIdx Index to the channel clusters.
 */
FUNC(void, FRNM_CODE) FrNm_HandleSynchError
(
  FRNM_PDL_SF(const uint8 instIdx)
);
#define FRNM_STOP_SEC_CODE
#include <MemMap.h>
#endif /* (!defined FRNM_INT_H) */

/*==================[end of file]============================================*/
