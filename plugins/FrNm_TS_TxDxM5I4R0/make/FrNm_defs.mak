# \file
#
# \brief AUTOSAR FrNm
#
# This file contains the implementation of the AUTOSAR
# module FrNm.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

FrNm_CORE_PATH      := $(SSC_ROOT)\FrNm_$(FrNm_VARIANT)

FrNm_OUTPUT_PATH    := $(AUTOSAR_BASE_OUTPUT_PATH)

#################################################################
# REGISTRY
SSC_PLUGINS             += FrNm
FrNm_DEPENDENT_PLUGINS := base_make tresos
FrNm_VERSION           := 2.00.00
FrNm_DESCRIPTION       := FrNm Basic Software Makefile PlugIn for Autosar
CC_INCLUDE_PATH         += \
   $(FrNm_CORE_PATH)\include \
   $(FrNm_OUTPUT_PATH)\include
