/**
 * \file
 *
 * \brief AUTOSAR FrIf
 *
 * This file contains the implementation of the AUTOSAR
 * module FrIf.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 11.3 (advisory)
 * A cast should not be performed between a pointer type and an integral type.
 *
 * Reason:
 * In order to compare a pointer against an invalid value (NULL_PTR) a conversion of an integral
 * into an pointer is required.
 *
 * MISRA-2) Deviated Rule: 19.6 (required)
 * '#undef' shall not be used.
 *
 * Reason:
 * In order to simplify Service ID usage over a singel macro and putting two services into this
 * single source code file, the service ID macro is redefined (undefined and newly defined).
 *
 */

/******************************************************************************
 Include Section
******************************************************************************/

#include <FrIf_Trace.h>
#include <FrIf_Priv.h>
#include <SchM_FrIf.h>

/******************************************************************************
 Local Macros
******************************************************************************/

/* Deviation MISRA-2 */
#undef FRIF_API_ID
#define FRIF_API_ID (0x12U)    /* API identifier */

#if (TS_MERGED_COMPILE == STD_OFF)
#define FRIF_START_SEC_CODE
#include <MemMap.h>  /* !LINKSTO FrIf.ASR40.FrIf05088,1 */
#endif /* TS_MERGED_COMPILE */

/**
 * \brief  Transmit Service
 *
 * \param FrIf_TxPduId (in)         Id of Pdu to be transmitted.
 * \param FrIf_PduInfoPtr (in)      Description of Pdu content to be transmitted.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */

FUNC(Std_ReturnType,FRIF_CODE) FrIf_Transmit
    (
        PduIdType FrIf_TxPduId,
        P2CONST(PduInfoType,AUTOMATIC,FRIF_APPL_DATA) FrIf_PduInfoPtr
    )
{
    Std_ReturnType RetValue = E_NOT_OK;

    DBG_FRIF_TRANSMIT_ENTRY(FrIf_TxPduId,FrIf_PduInfoPtr);

/* check if development error detection is enabled */
#if (FRIF_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if(FrIf_ModuleInitialized == FALSE)
    {
        /* Report to DET */
        FrIf_Det_ReportError(FRIF_E_NOT_INITIALIZED);
    }

    else if(FrIf_TxPduId >= FrIf_RootConfigPtr->FrIf_TxPduNum)
    {
        /* Report to DET */
        FrIf_Det_ReportError(FRIF_E_INV_TXPDUID);
    }

    else
#endif  /* FRIF_DEV_ERROR_DETECT */
    {
        uint8 State = (uint8) FRIF_STATE_OFFLINE;

#if (FRIF_SINGLE_CLST_OPT_ENABLE == STD_OFF)
        /* check if at least a single cluster is ONLINE */
        uint8_least FrIf_ClstIdx;
        for( (FrIf_ClstIdx = 0U); 
             ((FrIf_ClstIdx < FrIf_RootConfigPtr->FrIf_ClstNum) && (State == (uint8)FRIF_STATE_OFFLINE)); 
             (FrIf_ClstIdx++))
#endif
        {
            /* Read FrIf_State in an atomic fashion */
            TS_AtomicAssign8(State,FrIf_ClstRuntimeData[FRIF_CLSTIDX].FrIf_State);
        }

        if(State == (uint8)FRIF_STATE_ONLINE)
        {
#if ((FRIF_IMMEDIATE_TX_ENABLE == STD_ON) || \
     (FRIF_TRANSMIT_QUEUE_ENABLE ==  STD_ON))
            /* get pointer to Pdu configuration */
            CONSTP2CONST(FrIf_TxPduType,AUTOMATIC,FRIF_APPL_CONST)
                PduPtr = &FRIF_GET_CONFIG_ADDR(FrIf_TxPduType,
                            FrIf_RootConfigPtr->FrIf_TxPduRef)[FrIf_TxPduId];
#endif /* FRIF_IMMEDIATE_TX_ENABLE, FRIF_TRANSMIT_QUEUE_ENABLE */

#if (FRIF_IMMEDIATE_TX_ENABLE == STD_ON)

            if((PduPtr->TxType_TxCntLimit_CtrlIdx & FRIF_TXTYPE_MASK)
               != FRIF_TXTYPE_DECOUPLED)
            {

#if (FRIF_SINGLE_CTRL_OPT_ENABLE == STD_OFF)
                const uint8 FrIf_CtrlIdx =
                    (uint8)((PduPtr->TxType_TxCntLimit_CtrlIdx) & FRIF_CTRLIDX_MASK);
#endif /* FRIF_SINGLE_CTRL_OPT_ENABLE */

#if FRIF_DEV_ERROR_DETECT == STD_ON

                /* Deviation MISRA-1 */
                if (NULL_PTR == FrIf_PduInfoPtr)
                {
                    /* Report to DET */
                    FrIf_Det_ReportError(FRIF_E_INV_POINTER);
                }
            
                /* Deviation MISRA-1 */
                else if (NULL_PTR == FrIf_PduInfoPtr->SduDataPtr)
                {
                    /* Report to DET */
                    FrIf_Det_ReportError(FRIF_E_INV_POINTER);
                }

                else
#endif /* FRIF_DEV_ERROR_DETECT */
                {
                    const Std_ReturnType RetCode =
                        FrIf_Call_Fr(FRIF_FR_INSTANCE_IDX,TransmitTxLPdu)
                               (FRIF_FR_CTRLIDX,
                               PduPtr->DynLenEnable_LPduIdx,
                               FrIf_PduInfoPtr->SduDataPtr,
                               (uint8)FrIf_PduInfoPtr->SduLength);

                    if(E_OK == RetCode)
                    {
                        /* If Pdu requires tx confirmation then pending bit must be set */
                        if(FrIf_TxPduId >= FrIf_RootConfigPtr->FrIf_TxPduNoConfNum )
                        {
                            /* if a transmission confirmation accesses this data at the same time,
                             * it will be corrupted!
                             */
                            /* That's why this critical area is protected */
                            SchM_Enter_FrIf_SCHM_FRIF_TX_ADMINISTRATION();

                            ((P2VAR(uint8,AUTOMATIC,FRIF_VAR))(P2VAR(void,AUTOMATIC,FRIF_VAR))FrIf_Mem)[FrIf_TxPduId] 
                              |= FRIF_PENDINGBIT_SETBITMASK;

                            /* Leave critical area */
                            SchM_Exit_FrIf_SCHM_FRIF_TX_ADMINISTRATION();
                        }
                        RetValue = E_OK;
                    }
                }
            }
            /* this is a decoupled Pdu */
            else
#endif /* FRIF_IMMEDIATE_TX_ENABLE */
            {
                /* for decoupled transmission, FrIf_PduInfoPtr is not used */
                TS_PARAM_UNUSED(FrIf_PduInfoPtr);
                
                /**
                 * just increment the TxCounter but the counter should not
                 * exceed MaxCounter.
                 * The MCG ensures that the transmit counter values are placed in the beginning
                 * of the FrIf memory w.r.t the tx-pdu id.
                 **/

                SchM_Enter_FrIf_SCHM_FRIF_TX_ADMINISTRATION();

                if((((P2VAR(uint8,AUTOMATIC,FRIF_VAR))(P2VAR(void,AUTOMATIC,FRIF_VAR))FrIf_Mem)[FrIf_TxPduId] & FRIF_TXCNT_MASK)
                            < FRIF_GET_CNTLIMIT(PduPtr->TxType_TxCntLimit_CtrlIdx))
                {

                    /* increment transmission request counter */
                    ((P2VAR(uint8,AUTOMATIC,FRIF_VAR))(P2VAR(void,AUTOMATIC,FRIF_VAR))FrIf_Mem)[FrIf_TxPduId]++;

                    RetValue = E_OK;
                }
                
                SchM_Exit_FrIf_SCHM_FRIF_TX_ADMINISTRATION();
            }
        }
    }

    DBG_FRIF_TRANSMIT_EXIT(RetValue,FrIf_TxPduId,FrIf_PduInfoPtr);
    return RetValue;
}


/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FRIF_STOP_SEC_CODE
#include <MemMap.h>  /* !LINKSTO FrIf.ASR40.FrIf05088,1 */
#endif /* TS_MERGED_COMPILE */ 

