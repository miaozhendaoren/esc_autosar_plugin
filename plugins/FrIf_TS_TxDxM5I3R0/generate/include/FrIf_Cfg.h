/**
 * \file
 *
 * \brief AUTOSAR FrIf
 *
 * This file contains the implementation of the AUTOSAR
 * module FrIf.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!CODE!]
#ifndef _FRIF_CFG_H_
#define _FRIF_CFG_H_

/*
 * Include Section
 */

#include <Std_Types.h>      /* definitions of STD_ON and STD_OFF */
#include <TSAutosar.h>      /* definitions of TS_PROD_ERR_* */
/*
 * Global Macros
 */

[!SELECT "FrIfGeneral"!]
/* standard SWS pre-compile time configuration parameters */
#define FRIF_DEV_ERROR_DETECT [!IF "FrIfDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_VERSION_INFO_API [!IF "FrIfVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRIF_GETNMVECTOR_API_ENABLE [!IF "FrIfGetNmVectorSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETCHANNELSTATUS_API_ENABLE [!IF "FrIfGetChannelStatusSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETCLOCKCORRECTION_API_ENABLE [!IF "FrIfGetClockCorrectionSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETSYNCFRAMELIST_API_ENABLE [!IF "FrIfGetSyncFrameListSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_RECONFIGLPDU_API_ENABLE [!IF "FrIfReconfigLPduSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_DISABLELPDU_API_ENABLE [!IF "FrIfDisableLPduSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETWAKEUPRXSTATUS_API_ENABLE [!IF "FrIfGetWakeupRxStatusSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_ALLSLOTS_API_ENABLE [!IF "FrIfAllSlotsSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRIF_CANCELTRANSMIT_API_ENABLE [!IF "FrIfCancelTransmitSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_ENABLETRANSCEIVERBRANCH_API_ENABLE [!IF "FrIfEnableTransceiverBranchSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_DISABLETRANSCEIVERBRANCH_API_ENABLE [!IF "FrIfDisableTransceiverBranchSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETNUMOFSTARTUPFRAMES_API_ENABLE [!IF "FrIfGetNumOfStartupFramesSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETTRANSCEIVERERROR_API_ENABLE [!IF "FrIfGetTransceiverErrorSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_READCCCONFIG_API_ENABLE [!IF "FrIfReadCCConfigApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GLOBALRXMAXLOOP [!"FrIfGlobalRxMaxLoop"!]U

[!IF "node:exists(FrIfUnusedBitValue)"!]
#define FRIF_TX_FRAME_DEFAULT_VALUE_ENABLE STD_ON
#define FRIF_TX_FRAME_DEFAULT_VALUE [!IF "FrIfUnusedBitValue = 0"!]0x00U[!ELSE!]0xFFU[!ENDIF!]
[!ELSE!]
#define FRIF_TX_FRAME_DEFAULT_VALUE_ENABLE STD_OFF
#define FRIF_TX_FRAME_DEFAULT_VALUE 0x0U
[!ENDIF!]

/** \brief Switch for DEM to DET reporting for production error FRIF_PROD_ERR_HANDLING_JLE_SYNC */
#define FRIF_PROD_ERR_HANDLING_JLE_SYNC   [!//
[!IF "ReportToDem/FrIfJoblistSyncReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrIfJoblistSyncReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrIfJoblistSyncReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRIF_PROD_ERR_HANDLING_JLE_SYNC */
#define FRIF_E_DEMTODET_JLE_SYNC          [!"ReportToDem/FrIfJoblistSyncReportToDemDetErrorId"!]U
[!ENDIF!][!//

/** \brief Switch for DEM to DET reporting for production error FRIF_PROD_ERR_HANDLING_NIT_CH_A */
#define FRIF_PROD_ERR_HANDLING_NIT_CH_A   [!//
[!IF "ReportToDem/FrIfNITStatusAReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrIfNITStatusAReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrIfNITStatusAReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRIF_PROD_ERR_HANDLING_NIT_CH_A */
#define FRIF_E_DEMTODET_NIT_CH_A          [!"ReportToDem/FrIfNITStatusAReportToDemDetErrorId"!]U
[!ENDIF!][!//

/** \brief Switch for DEM to DET reporting for production error FRIF_PROD_ERR_HANDLING_NIT_CH_B */
#define FRIF_PROD_ERR_HANDLING_NIT_CH_B   [!//
[!IF "ReportToDem/FrIfNITStatusBReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrIfNITStatusBReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrIfNITStatusBReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRIF_PROD_ERR_HANDLING_NIT_CH_B */
#define FRIF_E_DEMTODET_NIT_CH_B          [!"ReportToDem/FrIfNITStatusBReportToDemDetErrorId"!]U
[!ENDIF!][!//

/** \brief Switch for DEM to DET reporting for production error FRIF_PROD_ERR_HANDLING_SW_CH_A */
#define FRIF_PROD_ERR_HANDLING_SW_CH_A   [!//
[!IF "ReportToDem/FrIfSymbolWindowStatusAReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrIfSymbolWindowStatusAReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrIfSymbolWindowStatusAReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRIF_PROD_ERR_HANDLING_SW_CH_A */
#define FRIF_E_DEMTODET_SW_CH_A          [!"ReportToDem/FrIfSymbolWindowStatusAReportToDemDetErrorId"!]U
[!ENDIF!][!//

/** \brief Switch for DEM to DET reporting for production error FRIF_PROD_ERR_HANDLING_SW_CH_B */
#define FRIF_PROD_ERR_HANDLING_SW_CH_B   [!//
[!IF "ReportToDem/FrIfSymbolWindowStatusBReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrIfSymbolWindowStatusBReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrIfSymbolWindowStatusBReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRIF_PROD_ERR_HANDLING_SW_CH_B */
#define FRIF_E_DEMTODET_SW_CH_B          [!"ReportToDem/FrIfSymbolWindowStatusBReportToDemDetErrorId"!]U
[!ENDIF!][!//

/** \brief Switch for DEM to DET reporting for production error FRIF_PROD_ERR_HANDLING_ACS_CH_A */
#define FRIF_PROD_ERR_HANDLING_ACS_CH_A   [!//
[!IF "ReportToDem/FrIfAggregatedStatusAReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrIfAggregatedStatusAReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrIfAggregatedStatusAReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRIF_PROD_ERR_HANDLING_ACS_CH_A */
#define FRIF_E_DEMTODET_ACS_CH_A          [!"ReportToDem/FrIfAggregatedStatusAReportToDemDetErrorId"!]U
[!ENDIF!][!//

/** \brief Switch for DEM to DET reporting for production error FRIF_PROD_ERR_HANDLING_ACS_CH_B */
#define FRIF_PROD_ERR_HANDLING_ACS_CH_B   [!//
[!IF "ReportToDem/FrIfAggregatedStatusBReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrIfAggregatedStatusBReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrIfAggregatedStatusBReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRIF_PROD_ERR_HANDLING_ACS_CH_B */
#define FRIF_E_DEMTODET_ACS_CH_B          [!"ReportToDem/FrIfAggregatedStatusBReportToDemDetErrorId"!]U
[!ENDIF!][!//
[!ENDSELECT!]

[!SELECT "FrIfGeneral/VendorSpecific"!]
#define FRIF_RELATIVETIMER_API_ENABLE [!IF "RelativeTimerApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_SETEXTSYNC_API_ENABLE [!IF "SetExtSyncApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_MTS_API_ENABLE [!IF "MtsApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_WAKEUP_API_ENABLE [!IF "WakeupApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETIRQSTATUS_API_ENABLE [!IF "GetIrqStatusApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_DISABLEIRQ_API_ENABLE [!IF "DisableIrqApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_ALLOWCOLDSTART_API_ENABLE [!IF "AllowColdstartApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETCONTROLLERERRORSTATUS_API_ENABLE [!IF "GetCtrlErrorStatusApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_EXT_IRQ_SERVICES_API_ENABLE [!IF "ExtIRQServicesApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETSYNCSTATE_API_ENABLE [!IF "GetSyncStateApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRIF_GETTRANSCEIVERMODE_API_ENABLE [!IF "GetTransceiverModeApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_GETTRANSCEIVERWUREASON_API_ENABLE [!IF "GetTransceiverWUReasonApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_WAKEUPCONTROL_API_ENABLE [!IF "WakeupControlApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_CHECKWAKEUPBYTRANSCEIVER_API_ENABLE [!IF "CheckWakeupByTransceiverApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRIF_REPORT_TO_DET_ENABLE [!IF "ReportToDetEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_SINGLE_CLST_OPT_ENABLE [!IF "SingleClstOptEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_SINGLE_CTRL_OPT_ENABLE [!IF "SingleCtrlOptEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRIF_FR_INTEGRATION_ENABLE [!IF "FrIntegrationEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_SINGLE_FR_OPT_ENABLE [!IF "SingleFrOptEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!IF "SingleFrOptEnable = 'true'"!]
[!IF "not(node:refvalid(../../FrIfConfig/*[1]/FrIfCluster/*/FrIfController/*[1]/FrIfFrCtrlRef))"!]
[!ERROR!]Fr controller referenced by FrIfFrCtrlRef of FrIfController [!"node:path(../FrIfConfig/*[1]/FrIfCluster/*/FrIfController/*[1])"!] is not valid[!ENDERROR!]
[!ENDIF!]
[!SELECT "as:ref(../../FrIfConfig/*[1]/FrIfCluster/*/FrIfController/*[1]/FrIfFrCtrlRef)/../../../../CommonPublishedInformation"!]
[!VAR "FrApiPostfix"="''"!]
[!IF "node:exists(VendorApiInfix) and (VendorApiInfix != '')"!]
[!VAR "FrApiPostfix"!]_[!"VendorId"!]_[!"VendorApiInfix"!][!ENDVAR!]
[!ENDIF!]
#define FRIF_SINGLE_FR_PREFIX Fr[!"$FrApiPostfix"!]
#include <Fr[!"$FrApiPostfix"!].h>
[!ENDSELECT!]
[!ENDIF!]


#define FRIF_SINGLE_FRTRCV_OPT_ENABLE [!IF "SingleFrTrcvOptEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!IF "SingleFrTrcvOptEnable = 'true'"!]
[!VAR "FrTrcvApiPostfix"="''"!]
[!IF "node:exists(../../FrIfConfig/*[1]/FrIfCluster/*/FrIfController/*/FrIfTransceiver/*[1])"!]
[!IF "not(node:refvalid(../../FrIfConfig/*[1]/FrIfCluster/*/FrIfController/*/FrIfTransceiver/*[1]/FrIfFrTrcvChannelRef))"!]
[!ERROR!]FrTrcv transceiver referenced by FrIfFrTrcvChannelRef of FrIfTransceiver [!"node:path(../FrIfConfig/*[1]/FrIfCluster/*/FrIfController/*/FrIfTransceiver/*[1])"!] is not valid[!ENDERROR!]
[!ENDIF!]
[!SELECT "as:ref(../../FrIfConfig/*[1]/FrIfCluster/*/FrIfController/*/FrIfTransceiver/*[1]/FrIfFrTrcvChannelRef)/../../CommonPublishedInformation"!]
[!IF "node:exists(VendorApiInfix) and (VendorApiInfix != '')"!]
[!VAR "FrTrcvApiPostfix"!]_[!"VendorId"!]_[!"VendorApiInfix"!][!ENDVAR!]
[!ENDIF!]
[!ENDSELECT!]
[!ENDIF!]
#define FRIF_SINGLE_FRTRCV_PREFIX FrTrcv[!"$FrTrcvApiPostfix"!]
#include <FrTrcv[!"$FrTrcvApiPostfix"!].h>
#include <FrTrcv[!"$FrTrcvApiPostfix"!]_Cbk.h>
[!ENDIF!]

#define FRIF_JOBLIST_IRQMUX_ENABLE [!IF "JoblistIRQMuxEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_JOBLIST_IRQDELAYCHECK_ENABLE [!IF "JoblistIRQDelayCheckEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_JOBLIST_PREPARE_LPDU_ENABLE [!IF "JoblistPrepareLPduEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRIF_DYNAMIC_PAYLOAD_LENGTH_ENABLE [!IF "DynamicPayloadLengthEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_IMMEDIATE_TX_ENABLE [!IF "ImmediateTxEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_TRANSMIT_QUEUE_ENABLE [!IF "TransmitQueueEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRIF_DECOUPLED_RX_ENABLE [!IF "DecoupledRxEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRIF_EXTENDED_RXFIFO_ENABLE [!IF "ExtendedRxFIFOEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRIF_PBCFGM_SUPPORT_ENABLED [!IF "node:contains(node:refs(as:modconf('PbcfgM')/PbcfgMBswModules/*/PbcfgMBswModuleRef), as:modconf('FrIf')) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!IF "node:contains(node:refs(as:modconf('PbcfgM')/PbcfgMBswModules/*/PbcfgMBswModuleRef), as:modconf('FrIf')) = 'true'"!]
#define FRIF_RELOCATABLE_CFG_ENABLE [!IF "as:modconf('PbcfgM')/PbcfgMGeneral/PbcfgMRelocatableCfgEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!ELSE!]
#define FRIF_RELOCATABLE_CFG_ENABLE [!IF "FrIfRelocatablePbcfgEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!ENDIF!]

/** \brief Link time verification value */
#define FRIF_PUBLIC_INFO_SIGNATURE [!"asc:getConfigSignature(node:difference(as:modconf('FrIf')[1]/CommonPublishedInformation//*[not(child::*) and (node:configclass() = 'PublishedInformation')], as:modconf('FrIf')[1]/CommonPublishedInformation/Release))"!]U

/** \brief Compile time verification value */
#define FRIF_CFG_SIGNATURE [!"asc:getConfigSignature(as:modconf('FrIf')[1]//*[not(child::*) and (node:configclass() = 'PreCompile') ])"!]U

/** \brief Link time verification value */
#define FRIF_LCFG_SIGNATURE [!"asc:getConfigSignature(as:modconf('FrIf')[1]//*[not(child::*) and (node:configclass() = 'Link') ])"!]U

#endif /* _FRIF_CFG_H_ */

/*==================[end of file]============================================*/
[!ENDSELECT!]
[!ENDCODE!][!//

