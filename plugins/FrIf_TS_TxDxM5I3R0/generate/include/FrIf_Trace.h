/**
 * \file
 *
 * \brief AUTOSAR FrIf
 *
 * This file contains the implementation of the AUTOSAR
 * module FrIf.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined FRIF_TRACE_H)
#define FRIF_TRACE_H
/*==================[inclusions]============================================*/

[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>

[!ENDIF!]
/*==================[macros]================================================*/

#ifndef DBG_FRIF_ABORTCOMMUNICATION_ENTRY
/** \brief Entry point of function FrIf_AbortCommunication() */
#define DBG_FRIF_ABORTCOMMUNICATION_ENTRY(a)
#endif

#ifndef DBG_FRIF_ABORTCOMMUNICATION_EXIT
/** \brief Exit point of function FrIf_AbortCommunication() */
#define DBG_FRIF_ABORTCOMMUNICATION_EXIT(a,b)
#endif

#ifndef DBG_FRIF_ACKABSOLUTETIMERIRQ_ENTRY
/** \brief Entry point of function FrIf_AckAbsoluteTimerIRQ() */
#define DBG_FRIF_ACKABSOLUTETIMERIRQ_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_ACKABSOLUTETIMERIRQ_EXIT
/** \brief Exit point of function FrIf_AckAbsoluteTimerIRQ() */
#define DBG_FRIF_ACKABSOLUTETIMERIRQ_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_ACKIRQ_ENTRY
/** \brief Entry point of function FrIf_AckIRQ() */
#define DBG_FRIF_ACKIRQ_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_ACKIRQ_EXIT
/** \brief Exit point of function FrIf_AckIRQ() */
#define DBG_FRIF_ACKIRQ_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_ACKRELATIVETIMERIRQ_ENTRY
/** \brief Entry point of function FrIf_AckRelativeTimerIRQ() */
#define DBG_FRIF_ACKRELATIVETIMERIRQ_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_ACKRELATIVETIMERIRQ_EXIT
/** \brief Exit point of function FrIf_AckRelativeTimerIRQ() */
#define DBG_FRIF_ACKRELATIVETIMERIRQ_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_ALLOWCOLDSTART_ENTRY
/** \brief Entry point of function FrIf_AllowColdstart() */
#define DBG_FRIF_ALLOWCOLDSTART_ENTRY(a)
#endif

#ifndef DBG_FRIF_ALLOWCOLDSTART_EXIT
/** \brief Exit point of function FrIf_AllowColdstart() */
#define DBG_FRIF_ALLOWCOLDSTART_EXIT(a,b)
#endif

#ifndef DBG_FRIF_ALLSLOTS_ENTRY
/** \brief Entry point of function FrIf_AllSlots() */
#define DBG_FRIF_ALLSLOTS_ENTRY(a)
#endif

#ifndef DBG_FRIF_ALLSLOTS_EXIT
/** \brief Exit point of function FrIf_AllSlots() */
#define DBG_FRIF_ALLSLOTS_EXIT(a,b)
#endif

#ifndef DBG_FRIF_CANCELABSOLUTETIMER_ENTRY
/** \brief Entry point of function FrIf_CancelAbsoluteTimer() */
#define DBG_FRIF_CANCELABSOLUTETIMER_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CANCELABSOLUTETIMER_EXIT
/** \brief Exit point of function FrIf_CancelAbsoluteTimer() */
#define DBG_FRIF_CANCELABSOLUTETIMER_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_CANCELRELATIVETIMER_ENTRY
/** \brief Entry point of function FrIf_CancelRelativeTimer() */
#define DBG_FRIF_CANCELRELATIVETIMER_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CANCELRELATIVETIMER_EXIT
/** \brief Exit point of function FrIf_CancelRelativeTimer() */
#define DBG_FRIF_CANCELRELATIVETIMER_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_CHECKMTS_ENTRY
/** \brief Entry point of function FrIf_CheckMTS() */
#define DBG_FRIF_CHECKMTS_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_CHECKMTS_EXIT
/** \brief Exit point of function FrIf_CheckMTS() */
#define DBG_FRIF_CHECKMTS_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_CHECKWAKEUPBYTRANSCEIVER_ENTRY
/** \brief Entry point of function FrIf_CheckWakeupByTransceiver() */
#define DBG_FRIF_CHECKWAKEUPBYTRANSCEIVER_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CHECKWAKEUPBYTRANSCEIVER_EXIT
/** \brief Exit point of function FrIf_CheckWakeupByTransceiver() */
#define DBG_FRIF_CHECKWAKEUPBYTRANSCEIVER_EXIT(a,b)
#endif

#ifndef DBG_FRIF_CLEARTRANSCEIVERWAKEUP_ENTRY
/** \brief Entry point of function FrIf_ClearTransceiverWakeup() */
#define DBG_FRIF_CLEARTRANSCEIVERWAKEUP_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CLEARTRANSCEIVERWAKEUP_EXIT
/** \brief Exit point of function FrIf_ClearTransceiverWakeup() */
#define DBG_FRIF_CLEARTRANSCEIVERWAKEUP_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_CONTROLLERINIT_ENTRY
/** \brief Entry point of function FrIf_ControllerInit() */
#define DBG_FRIF_CONTROLLERINIT_ENTRY(a)
#endif

#ifndef DBG_FRIF_CONTROLLERINIT_EXIT
/** \brief Exit point of function FrIf_ControllerInit() */
#define DBG_FRIF_CONTROLLERINIT_EXIT(a,b)
#endif

#ifndef DBG_FRIF_DISABLEABSOLUTETIMERIRQ_ENTRY
/** \brief Entry point of function FrIf_DisableAbsoluteTimerIRQ() */
#define DBG_FRIF_DISABLEABSOLUTETIMERIRQ_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_DISABLEABSOLUTETIMERIRQ_EXIT
/** \brief Exit point of function FrIf_DisableAbsoluteTimerIRQ() */
#define DBG_FRIF_DISABLEABSOLUTETIMERIRQ_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_DISABLEIRQ_ENTRY
/** \brief Entry point of function FrIf_DisableIRQ() */
#define DBG_FRIF_DISABLEIRQ_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_DISABLEIRQ_EXIT
/** \brief Exit point of function FrIf_DisableIRQ() */
#define DBG_FRIF_DISABLEIRQ_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_DISABLELPDU_ENTRY
/** \brief Entry point of function FrIf_DisableLPdu() */
#define DBG_FRIF_DISABLELPDU_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_DISABLELPDU_EXIT
/** \brief Exit point of function FrIf_DisableLPdu() */
#define DBG_FRIF_DISABLELPDU_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_DISABLERELATIVETIMERIRQ_ENTRY
/** \brief Entry point of function FrIf_DisableRelativeTimerIRQ() */
#define DBG_FRIF_DISABLERELATIVETIMERIRQ_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_DISABLERELATIVETIMERIRQ_EXIT
/** \brief Exit point of function FrIf_DisableRelativeTimerIRQ() */
#define DBG_FRIF_DISABLERELATIVETIMERIRQ_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_DISABLETRANSCEIVERBRANCH_ENTRY
/** \brief Entry point of function FrIf_DisableTransceiverBranch() */
#define DBG_FRIF_DISABLETRANSCEIVERBRANCH_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_DISABLETRANSCEIVERBRANCH_EXIT
/** \brief Exit point of function FrIf_DisableTransceiverBranch() */
#define DBG_FRIF_DISABLETRANSCEIVERBRANCH_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_ENABLEABSOLUTETIMERIRQ_ENTRY
/** \brief Entry point of function FrIf_EnableAbsoluteTimerIRQ() */
#define DBG_FRIF_ENABLEABSOLUTETIMERIRQ_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_ENABLEABSOLUTETIMERIRQ_EXIT
/** \brief Exit point of function FrIf_EnableAbsoluteTimerIRQ() */
#define DBG_FRIF_ENABLEABSOLUTETIMERIRQ_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_ENABLEIRQ_ENTRY
/** \brief Entry point of function FrIf_EnableIRQ() */
#define DBG_FRIF_ENABLEIRQ_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_ENABLEIRQ_EXIT
/** \brief Exit point of function FrIf_EnableIRQ() */
#define DBG_FRIF_ENABLEIRQ_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_ENABLERELATIVETIMERIRQ_ENTRY
/** \brief Entry point of function FrIf_EnableRelativeTimerIRQ() */
#define DBG_FRIF_ENABLERELATIVETIMERIRQ_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_ENABLERELATIVETIMERIRQ_EXIT
/** \brief Exit point of function FrIf_EnableRelativeTimerIRQ() */
#define DBG_FRIF_ENABLERELATIVETIMERIRQ_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_ENABLETRANSCEIVERBRANCH_ENTRY
/** \brief Entry point of function FrIf_EnableTransceiverBranch() */
#define DBG_FRIF_ENABLETRANSCEIVERBRANCH_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_ENABLETRANSCEIVERBRANCH_EXIT
/** \brief Exit point of function FrIf_EnableTransceiverBranch() */
#define DBG_FRIF_ENABLETRANSCEIVERBRANCH_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETABSOLUTETIMERIRQSTATUS_ENTRY
/** \brief Entry point of function FrIf_GetAbsoluteTimerIRQStatus() */
#define DBG_FRIF_GETABSOLUTETIMERIRQSTATUS_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_GETABSOLUTETIMERIRQSTATUS_EXIT
/** \brief Exit point of function FrIf_GetAbsoluteTimerIRQStatus() */
#define DBG_FRIF_GETABSOLUTETIMERIRQSTATUS_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETCHANNELSTATUS_ENTRY
/** \brief Entry point of function FrIf_GetChannelStatus() */
#define DBG_FRIF_GETCHANNELSTATUS_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_GETCHANNELSTATUS_EXIT
/** \brief Exit point of function FrIf_GetChannelStatus() */
#define DBG_FRIF_GETCHANNELSTATUS_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETCLOCKCORRECTION_ENTRY
/** \brief Entry point of function FrIf_GetClockCorrection() */
#define DBG_FRIF_GETCLOCKCORRECTION_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_GETCLOCKCORRECTION_EXIT
/** \brief Exit point of function FrIf_GetClockCorrection() */
#define DBG_FRIF_GETCLOCKCORRECTION_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETCONTROLLERERRORSTATUS_ENTRY
/** \brief Entry point of function FrIf_GetControllerErrorStatus() */
#define DBG_FRIF_GETCONTROLLERERRORSTATUS_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_GETCONTROLLERERRORSTATUS_EXIT
/** \brief Exit point of function FrIf_GetControllerErrorStatus() */
#define DBG_FRIF_GETCONTROLLERERRORSTATUS_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_GETCYCLELENGTH_ENTRY
/** \brief Entry point of function FrIf_GetCycleLength() */
#define DBG_FRIF_GETCYCLELENGTH_ENTRY(a)
#endif

#ifndef DBG_FRIF_GETCYCLELENGTH_EXIT
/** \brief Exit point of function FrIf_GetCycleLength() */
#define DBG_FRIF_GETCYCLELENGTH_EXIT(a,b)
#endif

#ifndef DBG_FRIF_GETGLOBALTIME_ENTRY
/** \brief Entry point of function FrIf_GetGlobalTime() */
#define DBG_FRIF_GETGLOBALTIME_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_GETGLOBALTIME_EXIT
/** \brief Exit point of function FrIf_GetGlobalTime() */
#define DBG_FRIF_GETGLOBALTIME_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETIRQSTATUS_ENTRY
/** \brief Entry point of function FrIf_GetIRQStatus() */
#define DBG_FRIF_GETIRQSTATUS_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_GETIRQSTATUS_EXIT
/** \brief Exit point of function FrIf_GetIRQStatus() */
#define DBG_FRIF_GETIRQSTATUS_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETNMVECTOR_ENTRY
/** \brief Entry point of function FrIf_GetNmVector() */
#define DBG_FRIF_GETNMVECTOR_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_GETNMVECTOR_EXIT
/** \brief Exit point of function FrIf_GetNmVector() */
#define DBG_FRIF_GETNMVECTOR_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_GETNUMOFSTARTUPFRAMES_ENTRY
/** \brief Entry point of function FrIf_GetNumOfStartupFrames() */
#define DBG_FRIF_GETNUMOFSTARTUPFRAMES_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_GETNUMOFSTARTUPFRAMES_EXIT
/** \brief Exit point of function FrIf_GetNumOfStartupFrames() */
#define DBG_FRIF_GETNUMOFSTARTUPFRAMES_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_GETPOCSTATUS_ENTRY
/** \brief Entry point of function FrIf_GetPOCStatus() */
#define DBG_FRIF_GETPOCSTATUS_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_GETPOCSTATUS_EXIT
/** \brief Exit point of function FrIf_GetPOCStatus() */
#define DBG_FRIF_GETPOCSTATUS_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_GETRELATIVETIMERIRQSTATUS_ENTRY
/** \brief Entry point of function FrIf_GetRelativeTimerIRQStatus() */
#define DBG_FRIF_GETRELATIVETIMERIRQSTATUS_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_GETRELATIVETIMERIRQSTATUS_EXIT
/** \brief Exit point of function FrIf_GetRelativeTimerIRQStatus() */
#define DBG_FRIF_GETRELATIVETIMERIRQSTATUS_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETSYNCFRAMELIST_ENTRY
/** \brief Entry point of function FrIf_GetSyncFrameList() */
#define DBG_FRIF_GETSYNCFRAMELIST_ENTRY(a,b,c,d,e,f)
#endif

#ifndef DBG_FRIF_GETSYNCFRAMELIST_EXIT
/** \brief Exit point of function FrIf_GetSyncFrameList() */
#define DBG_FRIF_GETSYNCFRAMELIST_EXIT(a,b,c,d,e,f,g)
#endif

#ifndef DBG_FRIF_GETSYNCSTATE_ENTRY
/** \brief Entry point of function FrIf_GetSyncState() */
#define DBG_FRIF_GETSYNCSTATE_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_GETSYNCSTATE_EXIT
/** \brief Exit point of function FrIf_GetSyncState() */
#define DBG_FRIF_GETSYNCSTATE_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_GETTRANSCEIVERERROR_ENTRY
/** \brief Entry point of function FrIf_GetTransceiverError() */
#define DBG_FRIF_GETTRANSCEIVERERROR_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETTRANSCEIVERERROR_EXIT
/** \brief Exit point of function FrIf_GetTransceiverError() */
#define DBG_FRIF_GETTRANSCEIVERERROR_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FRIF_GETTRANSCEIVERMODE_ENTRY
/** \brief Entry point of function FrIf_GetTransceiverMode() */
#define DBG_FRIF_GETTRANSCEIVERMODE_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_GETTRANSCEIVERMODE_EXIT
/** \brief Exit point of function FrIf_GetTransceiverMode() */
#define DBG_FRIF_GETTRANSCEIVERMODE_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETTRANSCEIVERWUREASON_ENTRY
/** \brief Entry point of function FrIf_GetTransceiverWUReason() */
#define DBG_FRIF_GETTRANSCEIVERWUREASON_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_GETTRANSCEIVERWUREASON_EXIT
/** \brief Exit point of function FrIf_GetTransceiverWUReason() */
#define DBG_FRIF_GETTRANSCEIVERWUREASON_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_GETVERSIONINFO_ENTRY
/** \brief Entry point of function FrIf_GetVersionInfo() */
#define DBG_FRIF_GETVERSIONINFO_ENTRY(a)
#endif

#ifndef DBG_FRIF_GETVERSIONINFO_EXIT
/** \brief Exit point of function FrIf_GetVersionInfo() */
#define DBG_FRIF_GETVERSIONINFO_EXIT(a)
#endif

#ifndef DBG_FRIF_GETWAKEUPRXSTATUS_ENTRY
/** \brief Entry point of function FrIf_GetWakeupRxStatus() */
#define DBG_FRIF_GETWAKEUPRXSTATUS_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_GETWAKEUPRXSTATUS_EXIT
/** \brief Exit point of function FrIf_GetWakeupRxStatus() */
#define DBG_FRIF_GETWAKEUPRXSTATUS_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_HALTCOMMUNICATION_ENTRY
/** \brief Entry point of function FrIf_HaltCommunication() */
#define DBG_FRIF_HALTCOMMUNICATION_ENTRY(a)
#endif

#ifndef DBG_FRIF_HALTCOMMUNICATION_EXIT
/** \brief Exit point of function FrIf_HaltCommunication() */
#define DBG_FRIF_HALTCOMMUNICATION_EXIT(a,b)
#endif

#ifndef DBG_FRIF_ISVALIDCONFIG_ENTRY
/** \brief Entry point of function FrIf_IsValidConfig() */
#define DBG_FRIF_ISVALIDCONFIG_ENTRY(a)
#endif

#ifndef DBG_FRIF_ISVALIDCONFIG_EXIT
/** \brief Exit point of function FrIf_IsValidConfig() */
#define DBG_FRIF_ISVALIDCONFIG_EXIT(a,b)
#endif

#ifndef DBG_FRIF_INIT_ENTRY
/** \brief Entry point of function FrIf_Init() */
#define DBG_FRIF_INIT_ENTRY(a)
#endif

#ifndef DBG_FRIF_MODULEINITIALIZED
/** \brief Change of FrIf_ModuleInitialized */
#define DBG_FRIF_MODULEINITIALIZED(a,b)
#endif

#ifndef DBG_FRIF_INIT_EXIT
/** \brief Exit point of function FrIf_Init() */
#define DBG_FRIF_INIT_EXIT(a)
#endif

#ifndef DBG_FRIF_PREPAREFRAME_ENTRY
/** \brief Entry point of function FrIf_PrepareFrame() */
#define DBG_FRIF_PREPAREFRAME_ENTRY(a)
#endif

#ifndef DBG_FRIF_PREPAREFRAME_EXIT
/** \brief Exit point of function FrIf_PrepareFrame() */
#define DBG_FRIF_PREPAREFRAME_EXIT(a)
#endif

#ifndef DBG_FRIF_RECEIVEINDICATION_ENTRY
/** \brief Entry point of function FrIf_ReceiveIndication() */
#define DBG_FRIF_RECEIVEINDICATION_ENTRY(a)
#endif

#ifndef DBG_FRIF_RECEIVEINDICATION_EXIT
/** \brief Exit point of function FrIf_ReceiveIndication() */
#define DBG_FRIF_RECEIVEINDICATION_EXIT(a)
#endif

#ifndef DBG_FRIF_RECEIVEANDSTORE_ENTRY
/** \brief Entry point of function FrIf_ReceiveAndStore() */
#define DBG_FRIF_RECEIVEANDSTORE_ENTRY(a)
#endif

#ifndef DBG_FRIF_RECEIVEANDSTORE_EXIT
/** \brief Exit point of function FrIf_ReceiveAndStore() */
#define DBG_FRIF_RECEIVEANDSTORE_EXIT(a)
#endif

#ifndef DBG_FRIF_TXCONFIRMATION_ENTRY
/** \brief Entry point of function FrIf_TxConfirmation() */
#define DBG_FRIF_TXCONFIRMATION_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_TXCONFIRMATION_EXIT
/** \brief Exit point of function FrIf_TxConfirmation() */
#define DBG_FRIF_TXCONFIRMATION_EXIT(a,b)
#endif

#ifndef DBG_FRIF_RECEIVEANDINDICATEFRAME_ENTRY
/** \brief Entry point of function FrIf_ReceiveAndIndicateFrame() */
#define DBG_FRIF_RECEIVEANDINDICATEFRAME_ENTRY(a)
#endif

#ifndef DBG_FRIF_RECEIVEANDINDICATEFRAME_EXIT
/** \brief Exit point of function FrIf_ReceiveAndIndicateFrame() */
#define DBG_FRIF_RECEIVEANDINDICATEFRAME_EXIT(a)
#endif

#ifndef DBG_FRIF_RECEIVEANDINDICATESIMPLEFRAME_ENTRY
/** \brief Entry point of function FrIf_ReceiveAndIndicateSimpleFrame() */
#define DBG_FRIF_RECEIVEANDINDICATESIMPLEFRAME_ENTRY(a)
#endif

#ifndef DBG_FRIF_RECEIVEANDINDICATESIMPLEFRAME_EXIT
/** \brief Exit point of function FrIf_ReceiveAndIndicateSimpleFrame() */
#define DBG_FRIF_RECEIVEANDINDICATESIMPLEFRAME_EXIT(a)
#endif

#ifndef DBG_FRIF_TRANSMITFRAME_ENTRY
/** \brief Entry point of function FrIf_TransmitFrame() */
#define DBG_FRIF_TRANSMITFRAME_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_TRANSMITFRAME_EXIT
/** \brief Exit point of function FrIf_TransmitFrame() */
#define DBG_FRIF_TRANSMITFRAME_EXIT(a,b)
#endif

#ifndef DBG_FRIF_TRANSMITSIMPLEFRAME_ENTRY
/** \brief Entry point of function FrIf_TransmitSimpleFrame() */
#define DBG_FRIF_TRANSMITSIMPLEFRAME_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_TRANSMITSIMPLEFRAME_EXIT
/** \brief Exit point of function FrIf_TransmitSimpleFrame() */
#define DBG_FRIF_TRANSMITSIMPLEFRAME_EXIT(a,b)
#endif

#ifndef DBG_FRIF_LOOKUPRXFIFOENTRY_ENTRY
/** \brief Entry point of function FrIf_LookUpRxFIFOEntry() */
#define DBG_FRIF_LOOKUPRXFIFOENTRY_ENTRY(a,b,c,d,e)
#endif

#ifndef DBG_FRIF_LOOKUPRXFIFOENTRY_EXIT
/** \brief Exit point of function FrIf_LookUpRxFIFOEntry() */
#define DBG_FRIF_LOOKUPRXFIFOENTRY_EXIT(a,b,c,d,e,f)
#endif

#ifndef DBG_FRIF_CONSUMERXFIFO_ENTRY
/** \brief Entry point of function FrIf_ConsumeRxFIFO() */
#define DBG_FRIF_CONSUMERXFIFO_ENTRY(a)
#endif

#ifndef DBG_FRIF_CONSUMERXFIFO_EXIT
/** \brief Exit point of function FrIf_ConsumeRxFIFO() */
#define DBG_FRIF_CONSUMERXFIFO_EXIT(a)
#endif

#ifndef DBG_FRIF_INTERNDISPATCHCOMOPS_ENTRY
/** \brief Entry point of function FrIf_InternDispatchComOps() */
#define DBG_FRIF_INTERNDISPATCHCOMOPS_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_INTERNDISPATCHCOMOPS_EXIT
/** \brief Exit point of function FrIf_InternDispatchComOps() */
#define DBG_FRIF_INTERNDISPATCHCOMOPS_EXIT(a,b)
#endif

#ifndef DBG_FRIF_DISPATCHCOMOPS_ENTRY
/** \brief Entry point of function FrIf_DispatchComOps() */
#define DBG_FRIF_DISPATCHCOMOPS_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_DISPATCHCOMOPS_EXIT
/** \brief Exit point of function FrIf_DispatchComOps() */
#define DBG_FRIF_DISPATCHCOMOPS_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_JOBLISTEXEC_ENTRY
/** \brief Entry point of function FrIf_JobListExec() */
#define DBG_FRIF_JOBLISTEXEC_ENTRY(a)
#endif

#ifndef DBG_FRIF_JOBLISTEXEC_EXIT
/** \brief Exit point of function FrIf_JobListExec() */
#define DBG_FRIF_JOBLISTEXEC_EXIT(a)
#endif

#ifndef DBG_FRIF_GETMACROTICKSPERCYCLE_ENTRY
/** \brief Entry point of function FrIf_GetMacroticksPerCycle() */
#define DBG_FRIF_GETMACROTICKSPERCYCLE_ENTRY(a)
#endif

#ifndef DBG_FRIF_GETMACROTICKSPERCYCLE_EXIT
/** \brief Exit point of function FrIf_GetMacroticksPerCycle() */
#define DBG_FRIF_GETMACROTICKSPERCYCLE_EXIT(a,b)
#endif

#ifndef DBG_FRIF_READCCCONFIG_ENTRY
/** \brief Entry point of function FrIf_ReadCCConfig() */
#define DBG_FRIF_READCCCONFIG_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_READCCCONFIG_EXIT
/** \brief Exit point of function FrIf_ReadCCConfig() */
#define DBG_FRIF_READCCCONFIG_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_RECONFIGLPDU_ENTRY
/** \brief Entry point of function FrIf_ReconfigLPdu() */
#define DBG_FRIF_RECONFIGLPDU_ENTRY(a,b,c,d,e,f,g,h)
#endif

#ifndef DBG_FRIF_RECONFIGLPDU_EXIT
/** \brief Exit point of function FrIf_ReconfigLPdu() */
#define DBG_FRIF_RECONFIGLPDU_EXIT(a,b,c,d,e,f,g,h,i)
#endif

#ifndef DBG_FRIF_SENDMTS_ENTRY
/** \brief Entry point of function FrIf_SendMTS() */
#define DBG_FRIF_SENDMTS_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_SENDMTS_EXIT
/** \brief Exit point of function FrIf_SendMTS() */
#define DBG_FRIF_SENDMTS_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_STOPMTS_ENTRY
/** \brief Entry point of function FrIf_StopMTS() */
#define DBG_FRIF_STOPMTS_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_STOPMTS_EXIT
/** \brief Exit point of function FrIf_StopMTS() */
#define DBG_FRIF_STOPMTS_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_SENDWUP_ENTRY
/** \brief Entry point of function FrIf_SendWUP() */
#define DBG_FRIF_SENDWUP_ENTRY(a)
#endif

#ifndef DBG_FRIF_SENDWUP_EXIT
/** \brief Exit point of function FrIf_SendWUP() */
#define DBG_FRIF_SENDWUP_EXIT(a,b)
#endif

#ifndef DBG_FRIF_SETABSOLUTETIMER_ENTRY
/** \brief Entry point of function FrIf_SetAbsoluteTimer() */
#define DBG_FRIF_SETABSOLUTETIMER_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRIF_SETABSOLUTETIMER_EXIT
/** \brief Exit point of function FrIf_SetAbsoluteTimer() */
#define DBG_FRIF_SETABSOLUTETIMER_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FRIF_SETEXTSYNC_ENTRY
/** \brief Entry point of function FrIf_SetExtSync() */
#define DBG_FRIF_SETEXTSYNC_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_SETEXTSYNC_EXIT
/** \brief Exit point of function FrIf_SetExtSync() */
#define DBG_FRIF_SETEXTSYNC_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_SETRELATIVETIMER_ENTRY
/** \brief Entry point of function FrIf_SetRelativeTimer() */
#define DBG_FRIF_SETRELATIVETIMER_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_SETRELATIVETIMER_EXIT
/** \brief Exit point of function FrIf_SetRelativeTimer() */
#define DBG_FRIF_SETRELATIVETIMER_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_SETTRANSCEIVERMODE_ENTRY
/** \brief Entry point of function FrIf_SetTransceiverMode() */
#define DBG_FRIF_SETTRANSCEIVERMODE_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_SETTRANSCEIVERMODE_EXIT
/** \brief Exit point of function FrIf_SetTransceiverMode() */
#define DBG_FRIF_SETTRANSCEIVERMODE_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRIF_SETWAKEUPCHANNEL_ENTRY
/** \brief Entry point of function FrIf_SetWakeupChannel() */
#define DBG_FRIF_SETWAKEUPCHANNEL_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_SETWAKEUPCHANNEL_EXIT
/** \brief Exit point of function FrIf_SetWakeupChannel() */
#define DBG_FRIF_SETWAKEUPCHANNEL_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_STARTCOMMUNICATION_ENTRY
/** \brief Entry point of function FrIf_StartCommunication() */
#define DBG_FRIF_STARTCOMMUNICATION_ENTRY(a)
#endif

#ifndef DBG_FRIF_STARTCOMMUNICATION_EXIT
/** \brief Exit point of function FrIf_StartCommunication() */
#define DBG_FRIF_STARTCOMMUNICATION_EXIT(a,b)
#endif

#ifndef DBG_FRIF_RESETTXFLAGS_ENTRY
/** \brief Entry point of function FrIf_ResetTxFlags() */
#define DBG_FRIF_RESETTXFLAGS_ENTRY(a)
#endif

#ifndef DBG_FRIF_RESETTXFLAGS_EXIT
/** \brief Exit point of function FrIf_ResetTxFlags() */
#define DBG_FRIF_RESETTXFLAGS_EXIT(a)
#endif

#ifndef DBG_FRIF_RESETRXFLAGS_ENTRY
/** \brief Entry point of function FrIf_ResetRxFlags() */
#define DBG_FRIF_RESETRXFLAGS_ENTRY(a)
#endif

#ifndef DBG_FRIF_RESETRXFLAGS_EXIT
/** \brief Exit point of function FrIf_ResetRxFlags() */
#define DBG_FRIF_RESETRXFLAGS_EXIT(a)
#endif

#ifndef DBG_FRIF_SYNCJOBLIST_ENTRY
/** \brief Entry point of function FrIf_SyncJobList() */
#define DBG_FRIF_SYNCJOBLIST_ENTRY(a,b,c)
#endif

#ifndef DBG_FRIF_SYNCJOBLIST_EXIT
/** \brief Exit point of function FrIf_SyncJobList() */
#define DBG_FRIF_SYNCJOBLIST_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_MAINFUNCTION_ENTRY
/** \brief Entry point of function FrIf_MainFunction() */
#define DBG_FRIF_MAINFUNCTION_ENTRY(a)
#endif

#ifndef DBG_FRIF_MAINFUNCTION_EXIT
/** \brief Exit point of function FrIf_MainFunction() */
#define DBG_FRIF_MAINFUNCTION_EXIT(a)
#endif

#ifndef DBG_FRIF_REPORTCHANNELSTATUS_ENTRY
/** \brief Entry point of function FrIf_ReportChannelStatus() */
#define DBG_FRIF_REPORTCHANNELSTATUS_ENTRY(a)
#endif

#ifndef DBG_FRIF_REPORTCHANNELSTATUS_EXIT
/** \brief Exit point of function FrIf_ReportChannelStatus() */
#define DBG_FRIF_REPORTCHANNELSTATUS_EXIT(a)
#endif

#ifndef DBG_FRIF_GETSTATE_ENTRY
/** \brief Entry point of function FrIf_GetState() */
#define DBG_FRIF_GETSTATE_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_GETSTATE_EXIT
/** \brief Exit point of function FrIf_GetState() */
#define DBG_FRIF_GETSTATE_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_SETSTATE_ENTRY
/** \brief Entry point of function FrIf_SetState() */
#define DBG_FRIF_SETSTATE_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_SETSTATE_EXIT
/** \brief Exit point of function FrIf_SetState() */
#define DBG_FRIF_SETSTATE_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_GETMACROTICKDURATION_ENTRY
/** \brief Entry point of function FrIf_GetMacrotickDuration() */
#define DBG_FRIF_GETMACROTICKDURATION_ENTRY(a)
#endif

#ifndef DBG_FRIF_GETMACROTICKDURATION_EXIT
/** \brief Exit point of function FrIf_GetMacrotickDuration() */
#define DBG_FRIF_GETMACROTICKDURATION_EXIT(a,b)
#endif

#ifndef DBG_FRIF_TRANSMIT_ENTRY
/** \brief Entry point of function FrIf_Transmit() */
#define DBG_FRIF_TRANSMIT_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_TRANSMIT_EXIT
/** \brief Exit point of function FrIf_Transmit() */
#define DBG_FRIF_TRANSMIT_EXIT(a,b,c)
#endif

#ifndef DBG_FRIF_SYNCSTATE_GRP
#define DBG_FRIF_SYNCSTATE_GRP(a,b,c)
#endif

#ifndef DBG_FRIF_STATE_GRP
#define DBG_FRIF_STATE_GRP(a,b,c)
#endif

#ifndef DBG_FRIF_CHECKREPORT_SW_CHA_ENTRY
/** \brief Entry point of function FrIf_CheckReport_SW_ChA() */
#define DBG_FRIF_CHECKREPORT_SW_CHA_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_SW_CHA_EXIT
/** \brief Exit point of function FrIf_CheckReport_SW_ChA() */
#define DBG_FRIF_CHECKREPORT_SW_CHA_EXIT(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_SW_CHB_ENTRY
/** \brief Entry point of function FrIf_CheckReport_SW_ChB() */
#define DBG_FRIF_CHECKREPORT_SW_CHB_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_SW_CHB_EXIT
/** \brief Exit point of function FrIf_CheckReport_SW_ChB() */
#define DBG_FRIF_CHECKREPORT_SW_CHB_EXIT(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_ACS_CHA_ENTRY
/** \brief Entry point of function FrIf_CheckReport_ACS_ChA() */
#define DBG_FRIF_CHECKREPORT_ACS_CHA_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_ACS_CHA_EXIT
/** \brief Exit point of function FrIf_CheckReport_ACS_ChA() */
#define DBG_FRIF_CHECKREPORT_ACS_CHA_EXIT(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_ACS_CHB_ENTRY
/** \brief Entry point of function FrIf_CheckReport_ACS_ChB() */
#define DBG_FRIF_CHECKREPORT_ACS_CHB_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_ACS_CHB_EXIT
/** \brief Exit point of function FrIf_CheckReport_ACS_ChB() */
#define DBG_FRIF_CHECKREPORT_ACS_CHB_EXIT(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_NIT_CHA_ENTRY
/** \brief Entry point of function FrIf_CheckReport_NIT_ChA() */
#define DBG_FRIF_CHECKREPORT_NIT_CHA_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_NIT_CHA_EXIT
/** \brief Exit point of function FrIf_CheckReport_NIT_ChA() */
#define DBG_FRIF_CHECKREPORT_NIT_CHA_EXIT(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_NIT_CHB_ENTRY
/** \brief Entry point of function FrIf_CheckReport_NIT_ChB() */
#define DBG_FRIF_CHECKREPORT_NIT_CHB_ENTRY(a,b)
#endif

#ifndef DBG_FRIF_CHECKREPORT_NIT_CHB_EXIT
/** \brief Exit point of function FrIf_CheckReport_NIT_ChB() */
#define DBG_FRIF_CHECKREPORT_NIT_CHB_EXIT(a,b)
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined FRIF_TRACE_H) */
/*==================[end of file]===========================================*/
