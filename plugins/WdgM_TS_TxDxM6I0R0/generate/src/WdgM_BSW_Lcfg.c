/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 19.6 (required)
 * #undef shall not be used
 *
 * Reason:
 * The macro names used for memory mapping are different between
 * AUTOSAR 3.x and AUTOSAR 4.x. For sake of compatibility between
 * these worlds, both macros are defined.
 * Since only one of them will be undefined within the MemMap.h file,
 * all macros are undefined after the memory section ends.
 * This avoids possible redundant macro definitions.
 */
[!INCLUDE "../include/WdgM_Cfg.m"!][!//
[!INCLUDE "../include/WdgM_Macros.m"!][!//
[!INCLUDE "../include/WdgM_Checks.m"!][!//
[!CODE!][!//

/*==================[inclusions]=================================================================*/

#include <Std_Types.h>                                                         /* standard types */
/* !LINKSTO WDGM.EB.ASR32.WDGM027,1 */
#include <WdgM_Lcfg.h>                               /* Module internal BSW declarations and API */
#include <WdgIf.h>                                                             /* WdgIf_ModeType */

/*==================[macros]=====================================================================*/

[!AUTOSPACING!][!//
[!//

/*==================[type definitions]===========================================================*/

/*==================[internal function declarations]=============================================*/

/*==================[internal data]==============================================================*/

/*==================[external data]==============================================================*/

/* !LINKSTO WDGM.EB.TIMEPM.WDGM020304,1 */
#if (WDGM_EB_PARTITIONING_ENABLED == STD_ON)
#define WDGM_START_SEC_SHARED_VAR_NO_INIT_UNSPECIFIED
#else
#define WDGM_START_SEC_VAR_NOINIT_UNSPECIFIED
#define WDGM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#endif
#include <MemMap.h>

/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable3,1,WDGM.EB.DesignDecision.InternalVariable5,1,
 WDGM.EB.DesignDecision.InternalVariable8,1 */
[!LOOP "WdgMGeneral/WdgMSupervisedEntity/*"!][!//
/* Checkpoint individual run-time data for each SE */
VAR(WdgM_EB_CPDataType,WDGM_VAR_NOINIT) WdgM_EB_CPData_[!"@name"!][[!"num:i(count(WdgMCheckpoint/*))"!]];
[!ENDLOOP!][!//

#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
#if (WDGM_EB_GRAPH_NUM > 0)
/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable6,1,WDGM.EB.DesignDecision.InternalVariable7,1 */
VAR(WdgM_EB_GraphDataType,WDGM_VAR_NOINIT) WdgM_EB_GraphData[WDGM_EB_GRAPH_NUM];
#else
VAR(WdgM_EB_GraphDataType,WDGM_VAR_NOINIT) WdgM_EB_GraphData[1];
#endif
#endif

#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
#if (WDGM_EB_DM_NUM > 0)
/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable4,1,WDGM.EB.DesignDecision.InternalVariable9,1 */
VAR(WdgM_EB_DMDataType,WDGM_VAR_NOINIT) WdgM_EB_DMData[WDGM_EB_DM_NUM];
#else
VAR(WdgM_EB_DMDataType,WDGM_VAR_NOINIT) WdgM_EB_DMData[1];
#endif
#endif

/* !LINKSTO WDGM.EB.TIMEPM.WDGM020304,1 */
#if (WDGM_EB_PARTITIONING_ENABLED == STD_ON)
#define WDGM_STOP_SEC_SHARED_VAR_NO_INIT_UNSPECIFIED
#else
#define WDGM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#define WDGM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#endif
#include <MemMap.h>
/* Deviation MISRA-1 <+4> */
#undef WDGM_START_SEC_VAR_NOINIT_UNSPECIFIED
#undef WDGM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#undef WDGM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#undef WDGM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED

/*==================[internal constants]=========================================================*/

[!IF "node:exists(WdgMGeneral/WdgMCallerIds) and (count(WdgMGeneral/WdgMCallerIds/WdgMCallerId/*) > 0)"!][!//
/*------------------[Allowed Caller IDs]------------------------------------*/

#define WDGM_START_SEC_CONST_16BIT
#define WDGM_START_SEC_CONST_16
#include <MemMap.h>

CONST(uint16, WDGM_CONST) WdgM_EB_CallerIds[WDGM_EB_CALLERIDS_NUM] =
{
  [!LOOP "WdgMGeneral/WdgMCallerIds/WdgMCallerId/*"!][!//
  [!"node:value(.)"!]U,
  [!ENDLOOP!][!// WdgMCallerIds /*
};

#define WDGM_STOP_SEC_CONST_16BIT
#define WDGM_STOP_SEC_CONST_16
#include <MemMap.h>
/* Deviation MISRA-1 <+4> */
#undef WDGM_START_SEC_CONST_16BIT
#undef WDGM_START_SEC_CONST_16
#undef WDGM_STOP_SEC_CONST_16BIT
#undef WDGM_STOP_SEC_CONST_16
[!ENDIF!][!//

#define WDGM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*------------------[trigger and watchdog configuration]--------------------*/

[!LOOP "node:order(WdgMConfigSet/*[1]/WdgMMode/*, 'WdgMModeId')"!][!//
[!IF "count(WdgMTrigger/*) > 0"!][!//
/* trigger configuration for mode [!"name(.)"!] */
STATIC CONST(WdgM_EB_TriggerCfgType,WDGM_CONST) WdgM_EB_TrigCfg_M[!"WdgMModeId"!][[!"num:i(count(WdgMTrigger/*))"!]] =
{
  [!LOOP "WdgMTrigger/*"!][!//
  { /* [!"@name"!] */
    [!"WdgMWatchdogMode"!], /* watchdog trigger mode */
    [!"WdgMTriggerConditionValue"!]U, /* TriggerCondition */
    [!"as:ref(as:ref(WdgMTriggerWatchdogRef)/WdgMWatchdogDeviceRef)/WdgIfDeviceIndex"!]U /* watchdog device index */
  },
  [!ENDLOOP!][!// WdgM triggers /*
};
[!ELSE!][!//
/* no triggers configured for mode [!"name(.)"!] */
[!ENDIF!][!//

[!ENDLOOP!][!// WdgM modes /*

/*------------------[SE configuration]--------------------------------------*/

[!VAR "BaseIntGraphId" = "0"!][!//
[!VAR "DMId" = "0"!][!//
[!LOOP "WdgMGeneral/WdgMSupervisedEntity/*"!][!//
/*------------------Internal configuration data for SE: [!"@name"!] */

[!VAR "SEName" = "@name"!][!//
[!VAR "ExtSEId" = "num:i(WdgMSupervisedEntityId)"!][!//
[!VAR "IntSEId" = "@index"!][!//
[!LOOP "WdgMCheckpoint/*"!][!//
[!VAR "CPName" = "@name"!][!//
[!VAR "IntCPId" = "WdgMCheckpointId"!][!//
[!VAR "BaseExtGraphId" = "0"!][!//
[!LOOP "node:order(../../../../../WdgMConfigSet/*[1]/WdgMMode/*, 'WdgMModeId')"!][!//
[!VAR "IntModeId" = "WdgMModeId"!][!//
[!LOOP "WdgMAliveSupervision/*[(num:i(as:ref(WdgMAliveSupervisionCheckpointRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMAliveSupervisionCheckpointRef)/../../WdgMSupervisedEntityId) = $ExtSEId)]"!][!//
/* alive supervision of CP [!"$CPName"!] in mode [!"$IntModeId"!] */
STATIC CONST(WdgM_EB_CPAliveCfgType,WDGM_CONST) WdgM_EB_CPAliveCfg_[!"$IntSEId"!]_[!"$IntCPId"!]_[!"$IntModeId"!] =
{
  [!"num:i(WdgMExpectedAliveIndications)"!]U, /* ExpextedAliveIndication */
  [!"num:i(WdgMSupervisionReferenceCycle)"!]U, /* Amount of supervision reference cycles */
  [!"num:i(WdgMMaxMargin)"!]U, /* max margin */
  [!"num:i(WdgMMinMargin)"!]U /* min margin */
};

[!ENDLOOP!][!//
[!/*
Generate Configuration for external supervision graph
=====================================================
*/!][!//
[!/* Check if checkpoint is a destination node of some external transition
*/!][!//
[!VAR "NumConfiguredPredecessors" = "num:i(count(WdgMExternalLogicalSupervision/*/WdgMExternalTransition/*[(num:i(as:ref(WdgMExternalTransitionDestRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMExternalTransitionDestRef)/../../WdgMSupervisedEntityId) = $ExtSEId)]))"!][!//
[!IF "$NumConfiguredPredecessors > 0"!][!//
[!VAR "ExtGraphIdOffset" = "num:i(WdgMExternalLogicalSupervision/*/WdgMExternalTransition/*[(num:i(as:ref(WdgMExternalTransitionDestRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMExternalTransitionDestRef)/../../WdgMSupervisedEntityId) = $ExtSEId)][1]/../../@index)"!][!//
/* predecessor points of CP [!"$CPName"!] in mode [!"$IntModeId"!] */
STATIC CONST(WdgM_EB_CPType,WDGM_CONST) WdgM_EB_CPPred_[!"$IntSEId"!]_[!"$IntCPId"!]_[!"$IntModeId"!][[!"$NumConfiguredPredecessors"!]] =
{
  [!LOOP "WdgMExternalLogicalSupervision/*/WdgMExternalTransition/*[(num:i(as:ref(WdgMExternalTransitionDestRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMExternalTransitionDestRef)/../../WdgMSupervisedEntityId) = $ExtSEId)]"!][!//
  [!VAR "PredecessorSEID" = "num:i(as:ref(WdgMExternalTransitionSourceRef)/../../@index)"!][!//
  [!VAR "PredecessorCPID" = "num:i(as:ref(WdgMExternalTransitionSourceRef)/WdgMCheckpointId)"!][!//
  {
    [!"$PredecessorSEID"!]U, /* Internal SEId of predecessor 0 */
    [!"$PredecessorCPID"!]U /* CPId of predecessor 0 */
  },
  [!ENDLOOP!][!//
};

/* External logical supervision of CP [!"$CPName"!] in mode [!"$IntModeId"!] */
STATIC CONST(WdgM_EB_CPLogicalCfgType,WDGM_CONST) WdgM_EB_CPExtLogicalCfg_[!"$IntSEId"!]_[!"$IntCPId"!]_[!"$IntModeId"!] =
{
  WdgM_EB_CPPred_[!"$IntSEId"!]_[!"$IntCPId"!]_[!"$IntModeId"!], /* pointer to predecessor CPs */
  [!"num:i($BaseExtGraphId+$ExtGraphIdOffset)"!]U, /* graph id */
  [!"$NumConfiguredPredecessors"!]U, /* number of predecessors */
  [!IF "count(WdgMExternalLogicalSupervision/*/WdgMExternalCheckpointFinalRef/*[(num:i(node:ref(.)/WdgMCheckpointId) = $IntCPId) and ((num:i(node:ref(.)/../../WdgMSupervisedEntityId)) = $ExtSEId)]) > 0"!]  TRUE[!ELSE!]  FALSE[!ENDIF!] /* isFinal */
};

[!ELSE!][!//
[!/* Check if the checkpoint is an initial node of some graph and not already treated before
*/!][!//
[!LOOP "WdgMExternalLogicalSupervision/*/WdgMExternalCheckpointInitialRef/*[(num:i(as:ref(.)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(.)/../../WdgMSupervisedEntityId) = $ExtSEId)]"!][!//
[!VAR "ExtGraphIdOffset" = "num:i(../../@index)"!][!//
/* External logical supervision of CP [!"$CPName"!] in mode [!"$IntModeId"!] */
STATIC CONST(WdgM_EB_CPLogicalCfgType,WDGM_CONST) WdgM_EB_CPExtLogicalCfg_[!"$IntSEId"!]_[!"$IntCPId"!]_[!"$IntModeId"!] =
{
  NULL_PTR, /* initial node has no predecessor CPs */
  [!"num:i($BaseExtGraphId+$ExtGraphIdOffset)"!]U, /* graph id */
  0U, /* number of predecessors */
  FALSE /* isFinal */
};

[!ENDLOOP!][!//
[!ENDIF!][!//
[!/* continuously increment base graph id
*/!][!//
[!VAR "BaseExtGraphId" = "$BaseExtGraphId + num:i(count(WdgMExternalLogicalSupervision/*))"!][!//
[!ENDLOOP!][!/* WdgM modes
*/!][!//
[!/*
Generate Configuration for internal supervision graph
=====================================================
Check if internal supervision is configured at all for the CPs of actual SE
*/!][!//
[!VAR "IsInternalLogicalSupervisionConfigured" = "count(../../WdgMInternalTransition/*[((num:i(as:ref(WdgMInternalTransitionDestRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMInternalTransitionDestRef)/../../WdgMSupervisedEntityId) = $ExtSEId)) or ((num:i(as:ref(WdgMInternalTransitionSourceRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMInternalTransitionSourceRef)/../../WdgMSupervisedEntityId) = $ExtSEId))]) > 0"!][!//
[!IF "$IsInternalLogicalSupervisionConfigured"!][!//
[!VAR "NumConfiguredPredecessors" = "num:i(count(../../WdgMInternalTransition/*[(num:i(as:ref(WdgMInternalTransitionDestRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMInternalTransitionDestRef)/../../WdgMSupervisedEntityId) = $ExtSEId)]))"!][!//
[!IF "$NumConfiguredPredecessors > 0"!][!//
/* predecessor points of CP [!"$CPName"!] */
STATIC CONST(WdgM_EB_CPType,WDGM_CONST) WdgM_EB_CPPred_[!"$IntSEId"!]_[!"$IntCPId"!][[!"$NumConfiguredPredecessors"!]] =
{
  [!LOOP "../../WdgMInternalTransition/*[(num:i(as:ref(WdgMInternalTransitionDestRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMInternalTransitionDestRef)/../../WdgMSupervisedEntityId) = $ExtSEId)]"!][!//
  [!VAR "PredecessorSEID" = "num:i(as:ref(WdgMInternalTransitionSourceRef)/../../@index)"!][!//
  [!VAR "PredecessorCPID" = "num:i(as:ref(WdgMInternalTransitionSourceRef)/WdgMCheckpointId)"!][!//
  {
    [!"$PredecessorSEID"!]U, /* Internal SEId of predecessor 0 */
    [!"$PredecessorCPID"!]U /* CPId of predecessor 0 */
  },
  [!ENDLOOP!][!//
};

/* Internal logical supervision of [!"$CPName"!] */
STATIC CONST(WdgM_EB_CPLogicalCfgType,WDGM_CONST) WdgM_EB_CPIntLogicalCfg_[!"$IntSEId"!]_[!"$IntCPId"!] =
{
  WdgM_EB_CPPred_[!"$IntSEId"!]_[!"$IntCPId"!], /* pointer to predecessor CPs */
  [!"num:i($BaseExtGraphId+$BaseIntGraphId)"!]U, /* graph id */
  [!"$NumConfiguredPredecessors"!]U, /* number of predecessors */
  [!IF "count(../../WdgMInternallCheckpointFinalRef/*[(num:i(node:ref(.)/WdgMCheckpointId) = $IntCPId) and ((num:i(node:ref(.)/../../WdgMSupervisedEntityId)) = $ExtSEId)]) > 0"!]  TRUE[!ELSE!]  FALSE[!ENDIF!] /* isFinal */
};

[!ELSEIF "node:refvalid(../../WdgMInternalCheckpointInitialRef) and (num:i(node:ref(../../WdgMInternalCheckpointInitialRef)/WdgMCheckpointId) = $IntCPId) and (num:i(node:ref(../../WdgMInternalCheckpointInitialRef)/../../WdgMSupervisedEntityId) = $ExtSEId)"!][!//
/* predecessor points of CP [!"$CPName"!] */
STATIC CONST(WdgM_EB_CPType,WDGM_CONST) WdgM_EB_CPPred_[!"$IntSEId"!]_[!"$IntCPId"!][1] =
{
  {
    WDGM_EB_DUMMY_SEID, /* SEId of predecessor 0 */
    WDGM_EB_DUMMY_CPID /* CPId of predecessor 0 */
  }
};

/* Internal logical supervision of [!"$CPName"!] */
STATIC CONST(WdgM_EB_CPLogicalCfgType,WDGM_CONST) WdgM_EB_CPIntLogicalCfg_[!"$IntSEId"!]_[!"$IntCPId"!] =
{
  WdgM_EB_CPPred_[!"$IntSEId"!]_[!"$IntCPId"!], /* pointer to predecessor CPs */
  [!"num:i($BaseExtGraphId+$BaseIntGraphId)"!]U, /* graph id */
  0U, /* number of predecessors */
  FALSE /* isFinal */
};

[!ENDIF!][!//
[!ENDIF!][!//
[!ENDLOOP!][!// Checkpoints
[!/*
Generate Configuration for deadline supervision
===============================================
*/!][!//
[!LOOP "node:order(../../../WdgMConfigSet/*[1]/WdgMMode/*, 'WdgMModeId')"!][!//
[!VAR "IntModeId" = "WdgMModeId"!][!//
[!VAR "ListOfGeneratedCPIds" = "''"!][!//
[!FOR "MaxSimpleSortLoop" = "0" TO "count(../../../../WdgMGeneral/WdgMSupervisedEntity/*[WdgMSupervisedEntityId = $ExtSEId]/WdgMCheckpoint/*)"!]
[!VAR "IsContinueSimpleSortLoop" = "'false'"!][!//
[!LOOP "node:order(../../../../WdgMGeneral/WdgMSupervisedEntity/*[WdgMSupervisedEntityId = $ExtSEId]/WdgMCheckpoint/*, 'WdgMCheckpointId')"!][!//
[!VAR "CPName" = "@name"!][!//
[!VAR "IntCPId" = "WdgMCheckpointId"!][!//
[!SELECT "../../../../../WdgMConfigSet/*[1]/WdgMMode/*[WdgMModeId = $IntModeId]"!][!//
[!IF "not(text:contains( text:split($ListOfGeneratedCPIds), $IntCPId))"!][!//
[!VAR "IsDeadlineSupervisionStartConfiguration" = "count(WdgMDeadlineSupervision/*[((num:i(as:ref(WdgMDeadlineStartRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMDeadlineStartRef)/../../WdgMSupervisedEntityId) = $ExtSEId))]) = 1"!][!//
[!IF "$IsDeadlineSupervisionStartConfiguration"!][!//
  [!VAR "DeadlineSupervisionStartConfigurationDMMax" = "WdgMDeadlineSupervision/*[((num:i(as:ref(WdgMDeadlineStartRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMDeadlineStartRef)/../../WdgMSupervisedEntityId) = $ExtSEId))][1]/WdgMDeadlineMax"!][!//
  [!VAR "DeadlineSupervisionStartConfigurationDMMin" = "WdgMDeadlineSupervision/*[((num:i(as:ref(WdgMDeadlineStartRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMDeadlineStartRef)/../../WdgMSupervisedEntityId) = $ExtSEId))][1]/WdgMDeadlineMin"!][!//
  [!VAR "DeadlineSupervisionStartConfigurationDMStopRef" = "WdgMDeadlineSupervision/*[((num:i(as:ref(WdgMDeadlineStartRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMDeadlineStartRef)/../../WdgMSupervisedEntityId) = $ExtSEId))][1]/WdgMDeadlineStopRef"!][!//
  [!VAR "IsPrevDeadlineSupervisionConfiguration" = "count(WdgMDeadlineSupervision/*[((num:i(as:ref(WdgMDeadlineStopRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMDeadlineStopRef)/../../WdgMSupervisedEntityId) = $ExtSEId))]) > 0"!][!//
  [!VAR "DoGenerateCPId" = "'true'"!][!//
  [!IF "$IsPrevDeadlineSupervisionConfiguration"!][!//
    [!VAR "PrevDeadlineSupervisionConfiguration" = "WdgMDeadlineSupervision/*[((num:i(as:ref(WdgMDeadlineStopRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMDeadlineStopRef)/../../WdgMSupervisedEntityId) = $ExtSEId))][1]/WdgMDeadlineStartRef"!][!//
    [!VAR "PrevDeadlineSupervisionIntCPId" = "num:i(as:ref($PrevDeadlineSupervisionConfiguration)/WdgMCheckpointId)"!][!//
    [!VAR "PrevDeadlineSupervisionIntSEId" = "num:i(as:ref($PrevDeadlineSupervisionConfiguration)/../../@index)"!][!//
    [!VAR "IsPrevDeadlineSupervisionConfigurationNotGenerated" = "not(text:contains( text:split($ListOfGeneratedCPIds), $PrevDeadlineSupervisionIntCPId))"!][!//
    [!IF "$IsPrevDeadlineSupervisionConfigurationNotGenerated"!][!//
      [!VAR "IsContinueSimpleSortLoop" = "'true'"!][!//
      [!VAR "DoGenerateCPId" = "'false'"!][!//
    [!ENDIF!][!//
  [!ENDIF!][!//
  [!IF "$DoGenerateCPId = 'true'"!][!//
    [!VAR "ListOfGeneratedCPIds" = "concat($ListOfGeneratedCPIds, ' ', $IntCPId)"!][!//
/* deadline configuration for start CP [!"$CPName"!] in mode [!"$IntModeId"!] */
STATIC CONST(WdgM_EB_CPDeadlineCfgType,WDGM_CONST) WdgM_EB_CPDeadlineCfg_[!"$IntSEId"!]_[!"$IntCPId"!]_[!"$IntModeId"!] =
{
  [!"num:i(round(num:f(num:div($DeadlineSupervisionStartConfigurationDMMax, $TimeGranularity))))"!]U, /* maximum Deadline in granularity of [!"num:i(num:mul($TimeGranularity, 1000000))"!]us */
  [!"num:i(round(num:f(num:div($DeadlineSupervisionStartConfigurationDMMin, $TimeGranularity))))"!]U, /* minimum Deadline in granularity of [!"num:i(num:mul($TimeGranularity, 1000000))"!]us */
  [!"num:i(num:div(num:i(num:i(round(num:f(num:div($DeadlineSupervisionStartConfigurationDMMax, $TimeGranularity)))) + $MainFunctionCycleTime - 1), $MainFunctionCycleTime))"!]U, /* maximum allowed main function calls until deadline violation */
  [!IF "$IsPrevDeadlineSupervisionConfiguration"!][!//
  &(WdgM_EB_CPDeadlineCfg_[!"$PrevDeadlineSupervisionIntSEId"!]_[!"$PrevDeadlineSupervisionIntCPId"!]_[!"$IntModeId"!]),
  [!ELSE!][!//
  NULL_PTR,
  [!ENDIF!][!//
  [!"num:i($DMId)"!]U, /* internal Deadline Monitoring ID for runtime data */
  [!"num:i($IntCPId)"!]U, /* Internal Checkpoint id of start checkpoint */
  [!"num:i(as:ref($DeadlineSupervisionStartConfigurationDMStopRef)/WdgMCheckpointId)"!]U, /* Internal Checkpoint id of stop checkpoint */
};
[!VAR "DMId" = "num:i($DMId + 1)"!][!//

  [!ENDIF!][!// DoGenerateCPId
[!ENDIF!][!// $IsDeadlineSupervisionStartConfiguration
[!ENDIF!][!// not(text:contains( text:split($ListOfGeneratedCPIds), $IntCPId))
[!ENDSELECT!][!//
[!ENDLOOP!][!// Checkpoints
[!IF "$IsContinueSimpleSortLoop = 'false'"!][!//
  [!BREAK!][!//
[!ENDIF!][!//
[!ENDFOR!][!// MaxSimpleSortLoop: Ensures that all Checkpoints are generated in worst-case
[!ENDLOOP!][!// WdgMModes

/* checkpoint configuration of SE [!"$SEName"!] */
STATIC CONST(WdgM_EB_CPCfgType,WDGM_CONST) WdgM_EB_CPCfg_[!"$SEName"!][[!"num:i(count(WdgMCheckpoint/*))"!]] =
{
[!LOOP "WdgMCheckpoint/*"!][!//
[!VAR "IntCPId" = "WdgMCheckpointId"!][!//
[!VAR "IntSupervExists" = "WdgMCheckpointId"!][!//
[!VAR "IsInternalLogicalSupervisionConfigured" = "count(../../WdgMInternalTransition/*[((num:i(as:ref(WdgMInternalTransitionDestRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMInternalTransitionDestRef)/../../WdgMSupervisedEntityId) = $ExtSEId)) or ((num:i(as:ref(WdgMInternalTransitionSourceRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMInternalTransitionSourceRef)/../../WdgMSupervisedEntityId) = $ExtSEId))]) > 0"!][!//
  {
    /* CP [!"@name"!]*/
    {
      /* alive supervision per WdgM mode */
      [!LOOP "node:order(../../../../../WdgMConfigSet/*[1]/WdgMMode/*, 'WdgMModeId')"!][!//
      [!VAR "IntModeId" = "WdgMModeId"!][!//
      [!IF "count(WdgMAliveSupervision/*[(num:i(as:ref(WdgMAliveSupervisionCheckpointRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMAliveSupervisionCheckpointRef)/../../WdgMSupervisedEntityId) = $ExtSEId)]) = 1"!][!//
      &(WdgM_EB_CPAliveCfg_[!"$IntSEId"!]_[!"$IntCPId"!]_[!"$IntModeId"!]),
      [!ELSE!][!//
      NULL_PTR,
      [!ENDIF!][!//
      [!ENDLOOP!][!// WdgMMode
    },
#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
    /* deadline monitoring per WdgM mode */
    {
      [!LOOP "node:order(../../../../../WdgMConfigSet/*[1]/WdgMMode/*, 'WdgMModeId')"!][!//
        [!VAR "IntModeId" = "WdgMModeId"!][!//
        [!VAR "IsDeadlineSupervisionStartConfiguration" = "count(WdgMDeadlineSupervision/*[(num:i(as:ref(WdgMDeadlineStartRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMDeadlineStartRef)/../../WdgMSupervisedEntityId) = $ExtSEId)]) > 0"!][!//
        [!VAR "IsDeadlineSupervisionStopConfiguration" = "count(WdgMDeadlineSupervision/*[(num:i(as:ref(WdgMDeadlineStopRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMDeadlineStopRef)/../../WdgMSupervisedEntityId) = $ExtSEId)]) > 0"!][!//
        [!IF "($IsDeadlineSupervisionStartConfiguration = 'true') or ($IsDeadlineSupervisionStopConfiguration = 'true')"!][!//
          [!IF "$IsDeadlineSupervisionStartConfiguration"!]
            [!VAR "IntStartCPId" = "$IntCPId"!][!//
            [!VAR "IntStartSEId" = "$ExtSEId"!][!//
          [!ELSE!][!//
            [!VAR "DeadlineSupervisionConfiguration" = "WdgMDeadlineSupervision/*[(num:i(as:ref(WdgMDeadlineStopRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMDeadlineStopRef)/../../WdgMSupervisedEntityId) = $ExtSEId)]/WdgMDeadlineStartRef"!][!//
            [!VAR "IntStartCPId" = "num:i(as:ref($DeadlineSupervisionConfiguration)/WdgMCheckpointId)"!][!//
            [!VAR "IntStartSEId" = "num:i(as:ref($DeadlineSupervisionConfiguration)/../../WdgMSupervisedEntityId)"!][!//
          [!ENDIF!][!//
      &(WdgM_EB_CPDeadlineCfg_[!"$IntStartSEId"!]_[!"$IntStartCPId"!]_[!"$IntModeId"!]),
        [!ELSE!][!//
      NULL_PTR,
        [!ENDIF!][!//
      [!ENDLOOP!][!// WdgMMode
    },
#endif
#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
    /* external / internal logical supervision per WdgM mode */
    {
      [!LOOP "node:order(../../../../../WdgMConfigSet/*[1]/WdgMMode/*, 'WdgMModeId')"!][!//
        [!VAR "IntModeId" = "WdgMModeId"!][!//
        [!VAR "IsExternalLogicalSupervisionConfigured" = "count(WdgMExternalLogicalSupervision/*/WdgMExternalTransition/*[((num:i(as:ref(WdgMExternalTransitionDestRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMExternalTransitionDestRef)/../../WdgMSupervisedEntityId) = $ExtSEId)) or ((num:i(as:ref(WdgMExternalTransitionSourceRef)/WdgMCheckpointId) = $IntCPId) and (num:i(as:ref(WdgMExternalTransitionSourceRef)/../../WdgMSupervisedEntityId) = $ExtSEId))]) > 0"!][!//
        [!VAR "SupervisedEntityIsReferencedInMode" = "count(WdgMLocalStatusParams/*[(num:i(as:ref(WdgMLocalStatusSupervisedEntityRef)/WdgMSupervisedEntityId)) = $ExtSEId]) > 0"!][!//
        [!IF "$IsExternalLogicalSupervisionConfigured"!][!//
      &(WdgM_EB_CPExtLogicalCfg_[!"$IntSEId"!]_[!"$IntCPId"!]_[!"$IntModeId"!]),
        [!ELSEIF "($IsInternalLogicalSupervisionConfigured = 'true') and ($SupervisedEntityIsReferencedInMode = 'true')"!][!//
      &(WdgM_EB_CPIntLogicalCfg_[!"$IntSEId"!]_[!"$IntCPId"!]),
        [!ELSE!][!//
      NULL_PTR,
        [!ENDIF!][!//
      [!ENDLOOP!][!// WdgMMode
    },
#endif
  },
[!ENDLOOP!][!// Checkpoints
};

[!/* Supervised Enitity contains an internal graph => increment counter for graph id */!][!//
[!IF "count(WdgMInternalTransition/*) > 0"!][!//
[!VAR "BaseIntGraphId" = "num:i($BaseIntGraphId + 1)"!][!//
[!ENDIF!][!//
[!//
[!ENDLOOP!][!// Supervised Entities
#define WDGM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>


/*==================[external constants]=========================================================*/

#define WDGM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
CONST(WdgM_EB_GraphIdType,WDGM_CONST) WdgM_EB_GraphDataSize = WDGM_EB_GRAPH_NUM;
#endif

#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
CONST(WdgM_EB_DMIdType,WDGM_CONST) WdgM_EB_DMDataSize = WDGM_EB_DM_NUM;
#endif

CONST(WdgM_ModeType,WDGM_CONST) WdgM_EB_InitialModeId =
    WdgMConf_WdgMMode_[!"name(as:ref(WdgMConfigSet/*[1]/WdgMInitialMode))"!];

[!IF "$SleepModeEnabled"!][!//
CONST(WdgM_ModeType,WDGM_CONST) WdgM_EB_DeInitModeId =
    WdgMConf_WdgMMode_[!"name(as:ref(WdgMConfigSet/*[1]/WdgMSleepMode))"!];

[!ENDIF!][!//
/*------------------[mode configuration]------------------------------------*/

CONST(WdgM_EB_ModeCfgType, WDGM_CONST) WdgM_EB_ModeCfg[WDGM_EB_MODE_NUM] =
{
  [!LOOP "node:order(WdgMConfigSet/*[1]/WdgMMode/*, 'WdgMModeId')"!]
  { /* mode [!"name(.)"!] (id [!"WdgMModeId"!]) */
    [!CALL "getModeSpecificLCM", "modeRef" = "as:path(.)"!]U, /* least common multiple of all reference cycle values */
    [!IF "count(WdgMTrigger/*) > 0"!]
    WdgM_EB_TrigCfg_M[!"WdgMModeId"!], /* trigger configuration */
    [!ELSE!]
    NULL_PTR, /* no triggers in this mode */
    [!ENDIF!]
    [!"WdgMExpiredSupervisionCycleTol"!]U, /* number of supervision cycles in stats EXPIRED */
    [!"num:i(count(WdgMTrigger/*))"!]U /* number of watchdogs (triggers) */
  },
  [!ENDLOOP!][!// WdgMMode/*
};

CONST(WdgM_ModeType,WDGM_CONST) WdgM_EB_ModeCfgSize = WDGM_EB_MODE_NUM;

/* -----------------[SE configuration]--------------------- */

CONST(WdgM_EB_SECfgType,WDGM_CONST) WdgM_EB_SECfg[WDGM_EB_SE_NUM] =
{
  [!LOOP "WdgMGeneral/WdgMSupervisedEntity/*"!]
  [!VAR "ExtSEId" = "num:i(WdgMSupervisedEntityId)"!]
  { /* SE [!"@name"!] (internal SEId: [!"@index"!]) */
    WdgM_EB_CPCfg_[!"@name"!], /* pointer to the array of checkpoints */
    WdgM_EB_CPData_[!"@name"!], /* pointer to the run-time data of the CPs */
    WdgMConf_WdgMSupervisedEntity_[!"@name"!], /* external SEId */
    [!"num:i(count(WdgMCheckpoint/*))"!]U, /* number of CPs in this SE */
    [!IF "WdgMErrorRecoveryEnabled = 'true'"!]TRUE[!ELSE!]FALSE[!ENDIF!], /* Support for Error Recovery */
    /* number of tolerated reference cycles which are allowed to fail in each WdgM mode*/
    {
      [!LOOP "node:order(../../../WdgMConfigSet/*[1]/WdgMMode/*, 'WdgMModeId')"!]
      [!IF "count(WdgMLocalStatusParams/*[as:ref(WdgMLocalStatusSupervisedEntityRef)/WdgMSupervisedEntityId = $ExtSEId]) > 0"!]
      [!"WdgMLocalStatusParams/*[as:ref(WdgMLocalStatusSupervisedEntityRef)/WdgMSupervisedEntityId = $ExtSEId]/WdgMFailedAliveSupervisionRefCycleTol"!]U, /* WdgM mode [!"@name"!] */
      [!ELSE!]
      WDGM_EB_DUMMY_FAILEDREFCYCLETOL, /* WdgM mode [!"@name"!] */
      [!ENDIF!]
      [!ENDLOOP!][!// /*
    }
  },
  [!ENDLOOP!][!// /*
};

/* Dummy module configuration structure*/
CONST(WdgM_ConfigType, WDGM_APPL_CONST) WDGM_CONFIG_NAME = 0;

#define WDGM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external function definitions]==============================================*/

/*==================[internal function definitions]==============================================*/

/*==================[end of file]================================================================*/
[!ENDCODE!][!//
