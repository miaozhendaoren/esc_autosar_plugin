[!// Info if ASR32 or ASR40 Service APIs are enabled in ASR31 generation mode
[!IF "WdgMGeneral/WdgMRteUsage = 'true'"!][!//
  [!IF "WdgMGeneral/WdgMServiceAPI/WdgMEnableASR32ServiceAPI = 'true'"!][!//
    [!ERROR!]Generation of ASR32 Service APIs in ASR31 generation mode is not supported.[!ENDERROR!][!//
  [!ENDIF!][!//
  [!IF "WdgMGeneral/WdgMServiceAPI/WdgMEnableASR40ServiceAPI = 'true'"!][!//
    [!ERROR!]Generation of ASR40 Service APIs in ASR31 generation mode is not supported.[!ENDERROR!][!//
  [!ENDIF!][!//
  [!IF "WdgMGeneral/WdgMServiceAPI/WdgMDefaultASRServiceAPI != 'AUTOSAR_32'"!][!//
    [!ERROR!]The parameter WdgMDefaultASRServiceAPI must be set to AUTOSAR_32 in ASR31 generation mode.[!ENDERROR!][!//
  [!ENDIF!][!//
[!ENDIF!][!//

