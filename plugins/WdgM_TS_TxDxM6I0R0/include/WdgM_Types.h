/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if (!defined WDGM_TYPES_H)
#define WDGM_TYPES_H

/*==================[inclusions]=================================================================*/

/* !LINKSTO WDGM.EB.ASR32.WDGM021,1 */
#include <WdgM_BSW_Types.h>                     /* Types required in own module's implementation */

/*==================[macros]=====================================================================*/

/*==================[type definitions]===========================================================*/

/* !LINKSTO WDGM.EB.ASR32.WDGM117,1 */
#ifndef RTE_TYPE_WdgM_SupervisedEntityIdType
#define RTE_TYPE_WdgM_SupervisedEntityIdType
/* !LINKSTO WDGM.EB.ASR32.WDGM105,1 */
/** \brief Supervised Entity Id Type
 *
 * Provide AUTOSAR 4.0 SupervisedEntityIdType as default to other BSW modules
 */
typedef WdgM_ASR40_SupervisedEntityIdType WdgM_SupervisedEntityIdType;
#endif

/* !LINKSTO WDGM.EB.ASR32.WDGM118,1 */
#ifndef RTE_TYPE_WdgM_CheckpointIdType
#define RTE_TYPE_WdgM_CheckpointIdType
/* !LINKSTO WDGM.EB.ASR32.WDGM105,1 */
/** \brief Checkpoint Id Type
 *
 * Provide AUTOSAR 4.0 CheckpointIdType as default to other BSW modules
 */
typedef WdgM_ASR40_CheckpointIdType WdgM_CheckpointIdType;
#endif


/*==================[external function declarations]=============================================*/

/*==================[internal function declarations]=============================================*/

/*==================[external constants]=========================================================*/

/*==================[internal constants]=========================================================*/

/*==================[external data]==============================================================*/

/*==================[internal data]==============================================================*/

/*==================[external function definitions]==============================================*/

/*==================[internal function definitions]==============================================*/

#endif /* if !defined( WDGM_TYPES_H ) */
/*==================[end of file]================================================================*/
