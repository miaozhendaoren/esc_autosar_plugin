/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if (!defined WDGM_INT_H)
#define WDGM_INT_H

/*==================[inclusions]=================================================================*/

#include <WdgM_BSW_Types.h>                                                  /* WdgM's BSW types */

/*==================[macros]=====================================================================*/

/*------------------------[The WdgM Instance Id]-----------------------------*/

#if (defined WDGM_INSTANCE_ID)
#error WDGM_INSTANCE_ID is already defined
#endif
/* \brief instance id, used for DET */
#define WDGM_INSTANCE_ID     0U




/*------------------------[Defensive programming]----------------------------*/

#if (defined WDGM_PRECONDITION_ASSERT)
#error WDGM_PRECONDITION_ASSERT is already defined
#endif
#if (WDGM_PRECONDITION_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define WDGM_PRECONDITION_ASSERT(Condition, ApiId) \
  DET_PRECONDITION_ASSERT((Condition), WDGM_MODULE_ID, WDGM_INSTANCE_ID, (ApiId))
#else
#define WDGM_PRECONDITION_ASSERT(Condition, ApiId)
#endif

#if (defined WDGM_POSTCONDITION_ASSERT)
#error WDGM_POSTCONDITION_ASSERT is already defined
#endif
#if (WDGM_POSTCONDITION_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define WDGM_POSTCONDITION_ASSERT(Condition, ApiId) \
  DET_POSTCONDITION_ASSERT((Condition), WDGM_MODULE_ID, WDGM_INSTANCE_ID, (ApiId))
#else
#define WDGM_POSTCONDITION_ASSERT(Condition, ApiId)
#endif

#if (defined WDGM_INVARIANT_ASSERT)
#error WDGM_INVARIANT_ASSERT is already defined
#endif
#if (WDGM_INVARIANT_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define WDGM_INVARIANT_ASSERT(Condition, ApiId) \
  DET_INVARIANT_ASSERT((Condition), WDGM_MODULE_ID, WDGM_INSTANCE_ID, (ApiId))
#else
#define WDGM_INVARIANT_ASSERT(Condition, ApiId)
#endif

#if (defined WDGM_STATIC_ASSERT)
# error WDGM_STATIC_ASSERT is already defined
#endif
#if (WDGM_STATIC_ASSERT_ENABLED == STD_ON)
/** \brief Report an static assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated */
# define WDGM_STATIC_ASSERT(expr) DET_STATIC_ASSERT(expr)
#else
# define WDGM_STATIC_ASSERT(expr)
#endif

#if (defined WDGM_UNREACHABLE_CODE_ASSERT)
#error WDGM_UNREACHABLE_CODE_ASSERT is already defined
#endif
#if (WDGM_UNREACHABLE_CODE_ASSERT_ENABLED == STD_ON)
/** \brief Report an unreachable code assertion violation to Det
 **
 ** \param[in] ApiId Service ID of the API function */
#define WDGM_UNREACHABLE_CODE_ASSERT(ApiId) \
  DET_UNREACHABLE_CODE_ASSERT(WDGM_MODULE_ID, WDGM_INSTANCE_ID, (ApiId))
#else
#define WDGM_UNREACHABLE_CODE_ASSERT(ApiId)
#endif

#if (defined WDGM_INTERNAL_API_ID)
#error WDGM_INTERNAL_API_ID is already defined
#endif
/** \brief API ID of module internal functions to be used in assertions */
#define WDGM_INTERNAL_API_ID DET_INTERNAL_API_ID


/*==================[type definitions]===========================================================*/

/*==================[external function declarations]=============================================*/

#define WDGM_START_SEC_CODE
#include <MemMap.h>

#if (WDGM_EB_INCLUDE_RTE == STD_ON)

#if (WDGM_EB_DEFAULT_ASR_SERVICE_API != WDGM_EB_SERVICE_API_NONE)
/** \brief Signal individual SE mode change via RTE
 *
 * The function receives SE Id and status from the WdgM point of view and has to
 * translate them into the values expected by the RTE. The reported values depend
 * on the AUTOSAR service API selected as default.
 *
 * The return value of the RTE call is ignored on purpose because the WdgM
 * does not have a strategy to handle failing SW-C calls.
 *
 * \param[in] SEID  supervised entity whose status has changed
 * \param[in] LocalStatus  new alive supervision status */
FUNC(void, WDGM_CODE) WdgM_RteIndividualModeSwitch (
  WdgM_ASR40_SupervisedEntityIdType SEID,
  WdgM_LocalStatusType              LocalStatus
);

/** \brief Signal global mode change via RTE
 *
 * The function receives the status from the WdgM point of view and  has to
 * translate them into the values expected by the RTE. The reported values depend
 * on the AUTOSAR service API selected as default.
 *
 * The return value of the RTE call is ignored on purpose because the WdgM
 * does not have a strategy to handle failing SW-C calls.
 *
 * \param[in] GlobalStatus  new global alive supervision status */
FUNC(void, WDGM_CODE) WdgM_RteGlobalModeSwitch (
  WdgM_GlobalStatusType GlobalStatus
);
#endif /* (WDGM_EB_DEFAULT_ASR_SERVICE_API != WDGM_EB_SERVICE_API_NONE) */

#if (WDGM_EB_ENABLE_ASR32_SERVICE_API == STD_ON)
/** \brief Signal individual SE mode change via RTE
 *
 * The function receives SE Id and status from the WdgM point of view and has to
 * translate them into the AUTOSAR 3.2 values expected by the RTE.
 *
 * The return value of the RTE call is ignored on purpose because the WdgM
 * does not have a strategy to handle failing SW-C calls.
 *
 * \param[in] SEID  supervised entity whose status has changed
 * \param[in] LocalStatus  new alive supervision status
 *
 * \note This function is called from WdgM_MainFunction and thus uses the
 * AUTOSAR 4.0 argument types. The mapping to the corresponding
 * AUTOSAR 3.2 types is done in the function definition. */
FUNC(void, WDGM_CODE) WdgM_ASR32_RteIndividualModeSwitch (
  WdgM_ASR40_SupervisedEntityIdType SEID,
  WdgM_LocalStatusType              LocalStatus
);

/** \brief Signal global mode change via RTE
 *
 * The function receives the status from the WdgM point of view and  has to
 * translate them into the AUTOSAR 3.2 values expected by the RTE.
 *
 * The return value of the RTE call is ignored on purpose because the WdgM
 * does not have a strategy to handle failing SW-C calls.
 *
 * \param[in] GlobalStatus  new global alive supervision status */
FUNC(void, WDGM_CODE) WdgM_ASR32_RteGlobalModeSwitch (
  WdgM_GlobalStatusType GlobalStatus
);
#endif /* (WDGM_EB_ENABLE_ASR32_SERVICE_API == STD_ON) */

#if (WDGM_EB_ENABLE_ASR40_SERVICE_API == STD_ON)
/** \brief Signal individual SE mode change via RTE
 *
 * The function receives SE Id and status from the WdgM point of view and has to
 * translate them into the AUTOSAR 4.0 values expected by the RTE.
 *
 * The return value of the RTE call is ignored on purpose because the WdgM
 * does not have a strategy to handle failing SW-C calls.
 *
 * \param[in] SEID  supervised entity whose status has changed
 * \param[in] LocalStatus  new alive supervision status */
FUNC(void, WDGM_CODE) WdgM_ASR40_RteIndividualModeSwitch (
  WdgM_ASR40_SupervisedEntityIdType SEID,
  WdgM_LocalStatusType              LocalStatus
);

/** \brief Signal global mode change via RTE
 *
 * The function receives the status from the WdgM point of view and  has to
 * translate them into the AUTOSAR 4.0 values expected by the RTE.
 *
 * The return value of the RTE call is ignored on purpose because the WdgM
 * does not have a strategy to handle failing SW-C calls.
 *
 * \param[in] GlobalStatus  new global alive supervision status */
FUNC(void, WDGM_CODE) WdgM_ASR40_RteGlobalModeSwitch (
  WdgM_GlobalStatusType GlobalStatus
);
#endif /* (WDGM_EB_ENABLE_ASR40_SERVICE_API == STD_ON) */
#endif /* (WDGM_EB_INCLUDE_RTE == STD_ON) */

#define WDGM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]=========================================================*/

/*==================[external data]==============================================================*/

#endif /* if !defined( WDGM_INT_H ) */
/*==================[end of file]================================================================*/
