/**
 * \file
 *
 * \brief AUTOSAR Fee
 *
 * This file contains the implementation of the AUTOSAR
 * module Fee.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]=============================================*/
/* !LINKSTO FEE084,1 */
#include <Std_Types.h>    /* Autosar standard type definitions */
#include <Fee_Int.h>      /* Fee internal header file */
#include <Fee_Cfg.h>      /* Fee module configuration header */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

#define FEE_START_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

[!VAR "VirPageSize" = "FeeGeneral/FeeVirtualPageSize"!][!//
[!VAR "Index" = "0"!][!//
CONST(Fee_BlockConfiguration_t, FEE_CONST)Fee_BlockCfg[FEE_NO_OF_CONFIGURED_BLOCKS] =
{
[!VAR "TotalCount" = "count(FeeBlockConfiguration/*)"!][!//
[!LOOP "node:order(FeeBlockConfiguration/*,'node:value(FeeBlockNumber)')"!][!//
[!VAR "Index" = "($Index + 1)"!][!//
  {
    [!"FeeBlockSize"!]U,
    [!"num:integer(FeeBlockSize + 2 + ($VirPageSize - ((FeeBlockSize + 2) mod $VirPageSize)) mod $VirPageSize)"!]U,
    [!"FeeBlockNumber"!]U,
    [!IF "FeeImmediateData='true'"!]TRUE[!ELSE!]FALSE[!ENDIF!]
  }[!IF "$Index = $TotalCount"!][!ELSE!],[!ENDIF!]
[!ENDLOOP!][!//
};

#define FEE_STOP_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*===========================================================================*/

/*==================[end of file Fee_Cfg.c]===========================================*/

