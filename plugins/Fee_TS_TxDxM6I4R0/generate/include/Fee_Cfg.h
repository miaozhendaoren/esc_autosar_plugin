#ifndef FEE_CFG_H
#define FEE_CFG_H
/**
 * \file
 *
 * \brief AUTOSAR Fee
 *
 * This file contains the implementation of the AUTOSAR
 * module Fee.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

[!AUTOSPACING!]
/*==================[inclusions]============================================*/
/* !LINKSTO FEE084,1, FEE002,1 */
#include <MemIf_Types.h>
/* !LINKSTO FEE084,1, FEE002,1 */
#include <Fls.h>
/* !LINKSTO FEE002,1 */
[!IF "((node:exists(FeeGeneral/FeeNvmJobEndNotification)) and
(FeeGeneral/FeeNvmJobEndNotification != '') and (FeeGeneral/FeeNvmJobEndNotification != 'NULL_PTR'))
or ((node:exists(FeeGeneral/FeeNvmJobErrorNotification)) and 
(FeeGeneral/FeeNvmJobErrorNotification != '') and (FeeGeneral/FeeNvmJobErrorNotification != 'NULL_PTR'))"!]
#include <NvM_Cbk.h>  /* NvM callback interface */
[!ENDIF!]
[!INCLUDE "Fee_Checks.m"!]
/*==================[macros]================================================*/

/** \brief It represent the number of blocks configured by the user */
#define FEE_NO_OF_CONFIGURED_BLOCKS        [!"num:integer(count(FeeBlockConfiguration/*))"!]U

[!SELECT "FeeGeneral"!]

/** \brief Represents the size of internal buffer for writing block data and
 ** reading block infos */
#define FEE_BUFFER_SIZE                    [!//
[!IF "node:exists(FeeBufferSize)"!][!//
[!"FeeBufferSize"!]U
[!ELSE!][!//
[!"num:i(num:max(text:split(concat($Headersize,' ',$LargestBlockSize))))"!]U
[!ENDIF!][!//

/** \brief Virtual page size */
#define FEE_VIRTUAL_PAGE_SIZE              [!"FeeVirtualPageSize"!]U

/** \brief Size of virtual page aligned block info */
#define FEE_BLOCKINFO_ALIGNED_SIZE         [!"$AlignedBlockInfoSize"!]U

/** \brief Number of bytes used by header fields
 **  when aligned to virtual page */
#define FEE_SECTION_STATUS_ALIGNED_SIZE    [!"$Headerfieldsize"!]U

/** \brief Size of the 3 fields of header */
#define FEE_HEADER_SIZE                    [!"$Headersize"!]U

/** \brief Offset of active field from header start address */
#define FEE_ACTIVEFIELD_INDEX              0U

/** \brief Offset of full field from header start address */
#define FEE_FULLFIELD_INDEX                [!"$Headerfieldsize"!]U

/** \brief Offset of copied field from header start address */
#define FEE_COPIEDFIELD_INDEX              [!"num:integer(2 * $Headerfieldsize)"!]U

/** \brief Nvm Job end notification function */
#define FEE_NVM_JOB_END_NOTIFICATION() [!//
[!IF "node:exists(FeeNvmJobEndNotification) and (FeeNvmJobEndNotification != '') and (FeeNvmJobEndNotification != 'NULL_PTR')"!]
  [!"FeeNvmJobEndNotification"!]()[!ELSE!] do {} while (0)
[!ENDIF!]

/** \brief Nvm Job error notification function */
#define FEE_NVM_JOB_ERROR_NOTIFICATION() [!//
[!IF "node:exists(FeeNvmJobErrorNotification) and (FeeNvmJobErrorNotification != '') and (FeeNvmJobErrorNotification != 'NULL_PTR')"!]
  [!"FeeNvmJobErrorNotification"!]()[!ELSE!] do {} while (0)
[!ENDIF!]

/** \brief Version info on\off compiler switch */
#define FEE_VERSION_INFO_API               [!//
[!IF "FeeVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Polling Mode on\off compiler switch */
#define FEE_POLLING_MODE                   [!//
[!IF "FeePollingMode = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Development error detect on\off compiler switch */
#define FEE_DEV_ERROR_DETECT               [!//
[!IF "FeeDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Set mode on\off compiler switch */
#define FEE_SET_MODE_SUPPORTED               [!//
[!IF "FeeSetModeSupported = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief InstanceId of this module instance */
#define FEE_INSTANCE_ID                    [!"FeeIndex"!]U

/** \brief Macro to call Fls_GetJobResult API if available and
 ** update internal flash job result variable.
 **
 ** This macro is dummy in case polling mode is not supported,
 ** but in that case internal flash job result variable gets updated
 ** via call back functions. */
#define FEE_GET_FLASH_JOB_RESULT()  \
[!IF "FeePollingMode = 'true'"!]
  (Fee_Gv.FeeFlashJobResult = Fls_GetJobResult())
[!ENDIF!]
[!ENDSELECT!]

[!SELECT "as:modconf('Fls')[1]/FlsGeneral"!]
/** \brief Macro to show if Fls_Cancel API is available */
#define FEE_FLS_CANCEL_API                 [!//
[!IF "FlsCancelApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Macro to call Fls_Cancel API, if available */
#define FEE_CANCEL_FLASHJOB() \
  do\
  {\
[!IF "FlsCancelApi = 'true'"!]
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_CANCELED;\
    Fls_Cancel();\
[!ENDIF!]
  }while(0)

/** \brief Macro to call Fls_SetMode API, if available */
#define FEE_FLS_SETMODE(Mode) \
[!IF "FlsSetModeApi = 'true'"!]
  (Fls_SetMode((Mode)))
[!ELSE!][!//
  (TS_PARAM_UNUSED(Mode))
[!ENDIF!]
[!ENDSELECT!]

[!SELECT "as:modconf('Fls')[1]/FlsPublishedInformation"!]
/** \brief Erase value of flash */
#define FEE_FLASH_ERASE_VALUE              [!"num:inttohex(FlsErasedValue, 2)"!]U

/** \brief Value to indicate Erased section */
#define FEE_SECTION_STATUS_ERASED          [!"num:inttohex(FlsErasedValue, 2)"!][!"substring-after(num:inttohex(FlsErasedValue, 2),'0x')"!]U
[!ENDSELECT!]

/** \brief The total flash size
 **
 ** This parameter defines the total flash size. */
#define FEE_TOTAL_FLASH_SIZE               [!"num:inttohex($TotalFlashSize)"!]U

/** \brief Section 0 start address
 **
 ** This parameter defines the start address of section 0. */
#define FEE_SECTION0_START_ADDRESS         0U

/** \brief Section 1 start address
 **
 ** This parameter defines the start address of section 1. */
#define FEE_SECTION1_START_ADDRESS         [!"num:inttohex($Section0size)"!]U

/** \brief Section 0 end address
 **
 ** This parameter defines the end address of section 0. */
#define FEE_SECTION0_END_ADDRESS           [!"num:inttohex($Section0size - 1)"!]UL

/** \brief Section 1 end address
 **
 ** This parameter defines the end address of section 1. */
#define FEE_SECTION1_END_ADDRESS           [!"num:inttohex($TotalFlashSize - 1)"!]UL

/** \brief Section 0 size
 **
 ** This parameter defines the size of section 0. */
#define FEE_SECTION0_SIZE                  [!"num:inttohex($Section0size)"!]UL

/** \brief Section 1 size
 **
 ** This parameter defines the size of section 1. */
#define FEE_SECTION1_SIZE                  [!"num:inttohex($Section1size)"!]UL

/** \brief The address at which the headers are written in section 0 */
#define FEE_HEADER0_ADDRESS                [!"num:inttohex($Section0size - $Headersize)"!]U

/** \brief The address at which the headers are written in section 1 */
#define FEE_HEADER1_ADDRESS                [!"num:inttohex($TotalFlashSize - $Headersize)"!]U


/* -----------[Symbolic names for the Fee blocks]--------------------------- */

[!LOOP "FeeBlockConfiguration/*"!][!//
/** \brief Symbolic name for the FEE Block number [!"FeeBlockNumber"!]" */
#define FeeConf_FeeBlockConfiguration_[!"name(.)"!]   [!"FeeBlockNumber"!]U

#if (!defined FEE_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]   [!"FeeBlockNumber"!]U
/** \brief Export symbolic name value with module abbreviation as prefix 
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define Fee_[!"@name"!] [!"FeeBlockNumber"!]U
#endif /* !defined FEE_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//




/*------------------[Defensive programming]---------------------------------*/
[!SELECT "FeeDefensiveProgramming"!][!//

#if (defined FEE_DEFENSIVE_PROGRAMMING_ENABLED)
#error FEE_DEFENSIVE_PROGRAMMING_ENABLED is already defined
#endif
/** \brief Defensive programming usage
 **
 ** En- or disables the usage of the defensive programming */
#define FEE_DEFENSIVE_PROGRAMMING_ENABLED   [!//
[!IF "(../FeeGeneral/FeeDevErrorDetect  = 'true') and (FeeDefProgEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FEE_PRECONDITION_ASSERT_ENABLED)
#error FEE_PRECONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Precondition assertion usage
 **
 ** En- or disables the usage of precondition assertion checks */
#define FEE_PRECONDITION_ASSERT_ENABLED     [!//
[!IF "(../FeeGeneral/FeeDevErrorDetect  = 'true') and (FeeDefProgEnabled = 'true') and (FeePrecondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FEE_POSTCONDITION_ASSERT_ENABLED)
#error FEE_POSTCONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Postcondition assertion usage
 **
 ** En- or disables the usage of postcondition assertion checks */
#define FEE_POSTCONDITION_ASSERT_ENABLED    [!//
[!IF "(../FeeGeneral/FeeDevErrorDetect  = 'true') and (FeeDefProgEnabled = 'true') and (FeePostcondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FEE_UNREACHABLE_CODE_ASSERT_ENABLED)
#error FEE_UNREACHABLE_CODE_ASSERT_ENABLED is already defined
#endif
/** \brief Unreachable code assertion usage
 **
 ** En- or disables the usage of unreachable code assertion checks */
#define FEE_UNREACHABLE_CODE_ASSERT_ENABLED [!//
[!IF "(../FeeGeneral/FeeDevErrorDetect  = 'true') and (FeeDefProgEnabled = 'true') and (FeeUnreachAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FEE_INVARIANT_ASSERT_ENABLED)
#error FEE_INVARIANT_ASSERT_ENABLED is already defined
#endif
/** \brief Invariant assertion usage
 **
 ** En- or disables the usage of invariant assertion checks */
#define FEE_INVARIANT_ASSERT_ENABLED        [!//
[!IF "(../FeeGeneral/FeeDevErrorDetect  = 'true') and (FeeDefProgEnabled = 'true') and (FeeInvariantAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FEE_STATIC_ASSERT_ENABLED)
#error FEE_STATIC_ASSERT_ENABLED is already defined
#endif
/** \brief Static assertion usage
 **
 ** En- or disables the usage of static assertion checks */
#define FEE_STATIC_ASSERT_ENABLED           [!//
[!IF "(../FeeGeneral/FeeDevErrorDetect  = 'true') and (FeeDefProgEnabled = 'true') and (FeeStaticAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDSELECT!][!//


/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
#endif /* #ifndef FEE_CFG_H */
