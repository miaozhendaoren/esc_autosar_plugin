/**
 * \file
 *
 * \brief AUTOSAR Fee
 *
 * This file contains the implementation of the AUTOSAR
 * module Fee.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 12.5 (required)
 *    "The operands of a logical && or || shall be primary-expressions."
 *
 *    Reason: Misra has an exception that whenever an expression consists of
 *     either a sequence of only logical && or a sequence of only logical ||,
 *     extra parentheses are not required
 *
 *  MISRA-2) Deviated Rule: 10.1 (required)
 *    "The value of an expression of integer type shall not be implicitly
 *     converted to a different underlying type if:
 *     a) it is not a conversion to a wider integer type of the same signedness, or
 *     b) the expression is complex, or
 *     c) the expression is not constant and is a function argument, or
 *     d) the expression is not constant and is a return expression."
 *
 *    Reason: The portion of code is necessary and has no side effects.
 */

/*==================[inclusions]=============================================*/

#include <Fee_Trace.h>
#include <TSMem.h>            /* Memory Routines */
/* !LINKSTO FEE002,1 */
#include <Fee.h>              /* External API declarations  */
/* !LINKSTO FEE002,1 */
#include <Fee_Cbk.h>          /* Job end and job error notification declarations  */
#include <Fee_Int.h>          /* Internal defines and declarations */

#if (FEE_DEV_ERROR_DETECT == STD_ON)
/* !LINKSTO FEE002,1 */
#include <Det.h>              /* Det public header file  */
#endif

/*==================[macros]=================================================*/

/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined FEE_VENDOR_ID) /* configuration check */
#error FEE_VENDOR_ID must be defined
#endif

#if (FEE_VENDOR_ID != 1U) /* vendor check */
#error FEE_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined FEE_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error FEE_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FEE_AR_RELEASE_MAJOR_VERSION != 4U)
#error FEE_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined FEE_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error FEE_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FEE_AR_RELEASE_MINOR_VERSION != 0U )
#error FEE_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined FEE_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error FEE_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (FEE_AR_RELEASE_REVISION_VERSION != 3U )
#error FEE_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined FEE_SW_MAJOR_VERSION) /* configuration check */
#error FEE_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FEE_SW_MAJOR_VERSION != 6U)
#error FEE_SW_MAJOR_VERSION wrong (!= 6U)
#endif

#if (!defined FEE_SW_MINOR_VERSION) /* configuration check */
#error FEE_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FEE_SW_MINOR_VERSION < 4U)
#error FEE_SW_MINOR_VERSION wrong (< 4U)
#endif

#if (!defined FEE_SW_PATCH_VERSION) /* configuration check */
#error FEE_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (FEE_SW_PATCH_VERSION < 2U)
#error FEE_SW_PATCH_VERSION wrong (< 2U)
#endif

/*------------------------[Development error reporting]----------------------*/

#if (defined FEE_DET_REPORT_ERROR)
#error FEE_DET_REPORT_ERROR already defined
#endif

/* define various macros used for development error reporting,
 * if development error detection is enabled */
#if (FEE_DEV_ERROR_DETECT == STD_ON)
/** \brief Report to the Det
 **
 ** \param[in] ApiId the service ID of the API function
 ** \param[in] ErrorId the ErrorID to be reported */
#define FEE_DET_REPORT_ERROR(ApiId, ErrorId)\
  ((void)Det_ReportError(FEE_MODULE_ID, FEE_INSTANCE_ID, (ApiId), (ErrorId)))
#endif




/*------------------------[Defensive programming]----------------------------*/

#if (defined FEE_PRECONDITION_ASSERT)
#error FEE_PRECONDITION_ASSERT is already defined
#endif
#if (FEE_PRECONDITION_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define FEE_PRECONDITION_ASSERT(Condition, ApiId) \
  DET_PRECONDITION_ASSERT((Condition), FEE_MODULE_ID, FEE_INSTANCE_ID, (ApiId))
#else
#define FEE_PRECONDITION_ASSERT(Condition, ApiId)
#endif

#if (defined FEE_POSTCONDITION_ASSERT)
#error FEE_POSTCONDITION_ASSERT is already defined
#endif
#if (FEE_POSTCONDITION_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define FEE_POSTCONDITION_ASSERT(Condition, ApiId) \
  DET_POSTCONDITION_ASSERT((Condition), FEE_MODULE_ID, FEE_INSTANCE_ID, (ApiId))
#else
#define FEE_POSTCONDITION_ASSERT(Condition, ApiId)
#endif

#if (defined FEE_INVARIANT_ASSERT)
#error FEE_INVARIANT_ASSERT is already defined
#endif
#if (FEE_INVARIANT_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define FEE_INVARIANT_ASSERT(Condition, ApiId) \
  DET_INVARIANT_ASSERT((Condition), FEE_MODULE_ID, FEE_INSTANCE_ID, (ApiId))
#else
#define FEE_INVARIANT_ASSERT(Condition, ApiId)
#endif

#if (defined FEE_STATIC_ASSERT)
# error FEE_STATIC_ASSERT is already defined
#endif
#if (FEE_STATIC_ASSERT_ENABLED == STD_ON)
/** \brief Report an static assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated */
# define FEE_STATIC_ASSERT(expr) DET_STATIC_ASSERT(expr)
#else
# define FEE_STATIC_ASSERT(expr)
#endif

#if (defined FEE_UNREACHABLE_CODE_ASSERT)
#error FEE_UNREACHABLE_CODE_ASSERT is already defined
#endif
#if (FEE_UNREACHABLE_CODE_ASSERT_ENABLED == STD_ON)
/** \brief Report an unreachable code assertion violation to Det
 **
 ** \param[in] ApiId Service ID of the API function */
#define FEE_UNREACHABLE_CODE_ASSERT(ApiId) \
  DET_UNREACHABLE_CODE_ASSERT(FEE_MODULE_ID, FEE_INSTANCE_ID, (ApiId))
#else
#define FEE_UNREACHABLE_CODE_ASSERT(ApiId)
#endif

#if (defined FEE_INTERNAL_API_ID)
#error FEE_INTERNAL_API_ID is already defined
#endif
/** \brief API ID of module internal functions to be used in assertions */
#define FEE_INTERNAL_API_ID DET_INTERNAL_API_ID


/*==================[type definitions]=======================================*/

/** \brief prototype of function pointer */
typedef struct
{
  void (*onEntry)(void); /**< \brief Function to be executed on state entry */
  void (*fn)(void); /**< \brief Function to be executed during state activity */

} Fee_StateDescType;

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

#define FEE_START_SEC_CODE
#include <MemMap.h>

/** \brief Function to start the processing of invalidate/write job */
STATIC FUNC(void, FEE_CODE) Fee_ProcessWriteOrInvalidateJob(void);

/** \brief Function to start the processing of invalidate job */
STATIC FUNC(void, FEE_CODE) Fee_ProcessInvalidateJob(void);

/** \brief Function to calculate the free space for Switch Section */
STATIC FUNC(Fls_LengthType, FEE_CODE) Fee_CalculateSpaceforSS(void);

/** \brief Function to set the section status depending upon the headers
 *
 * \param[in] Section the section to set the status for */
STATIC FUNC(void, FEE_CODE) Fee_SetSectionStatus(const uint8 Section);

/** \brief Function to fill the cache after each Fls_Read of block infos */
STATIC FUNC(boolean, FEE_CODE) Fee_FillCache(void);

/** \brief  Function to initiate the job for the next state after filling the cache */
STATIC FUNC(void, FEE_CODE) Fee_NextStateOfFillCache(void);

/** \brief This function validates the checksum field of the block info
 *
 * \param[in] BufferIndex index of the checksum in the buffer
 * \param[in] Checksum the checksum value
 * \revtal E_OK checksum is valid.
 * \retval E_NOT_OK checksum is invalid.
 */
STATIC FUNC(Std_ReturnType, FEE_CODE) Fee_ValidateChecksum
  (const uint16 BufferIndex, const uint8 Checksum);

/** \brief  Function to check whether all the bytes of the block info are in
 *          erased state
 *
 * \param[in] Index block info index in the buffer
 * \revtal TRUE erased byte is present.
 * \retval FALSE erased byte is not present.
 */
STATIC FUNC(boolean, FEE_CODE) Fee_CheckBlockInfoErased(const uint16 Index);

/** \brief This function calls the flash erase job to erase the active section
 *
 *  \param[in] Length data length to be erased
 */
STATIC FUNC(void, FEE_CODE) Fee_EraseFlash(const Fls_LengthType Length);

/** \brief Function to start copy of one block */
STATIC FUNC(void, FEE_CODE) Fee_InitiateBlockSwitch(void);

/** \brief Function to check space availability to copy next block during switch
 *
 * \revtal TRUE free space is available to copy.
 * \retval FALSE free space is not available to copy.
 */
STATIC FUNC(boolean, FEE_CODE) Fee_ChkFreeSpaceForCopy(void);

/** \brief Function to handle the flash failure during switch section */
STATIC FUNC(void, FEE_CODE) Fee_SwitchSectionFailure(void);

/** \brief State function to start jobs when Fee is in idle state */
STATIC FUNC(void, FEE_CODE) Fee_SfIdleState(void);

/** \brief Function to initiate Fee job */
STATIC FUNC(void, FEE_CODE) Fee_InitiateJob(void);

/** \brief Function to check if sufficient space available to write in the
 *         current active section
 *
 * \revtal TRUE free space is available to write.
 * \retval FALSE free space is not available to write.
 */
STATIC FUNC(boolean, FEE_CODE)Fee_ChkFreeSpace(void);

/** \brief Function to get block index from configuration table
 *
 *  \param[in] BlockNumber requested block number
 */
STATIC FUNC(uint16, FEE_CODE) Fee_SearchConfigTable(const uint16 BlockNumber);

/** \brief Function to calculate the checksum of the given data in the buffer
 *
 * \param[in] Buffer the buffer to calculate the checksum
 * \param[in] DataLength length of the buffer
 */
STATIC FUNC(uint8, FEE_CODE) Fee_CalculateChecksum
(
  CONSTP2VAR(uint8, FEE_CONST, FEE_APPL_DATA)Buffer,
  const Fls_LengthType DataLength
);

/** \brief Function to fill the Fee buffer with block info
 *         including block number, block size, copy/write status and checksum
 *
 * \param[in] BlockNumber block number
 * \param[in] BlockSize block length
 * \param[in] CopyWriteStatus copy/write status
 */
STATIC FUNC(void, FEE_CODE) Fee_FillBlockInfo
(
  const uint16 BlockNumber,
  const uint16 BlockSize,
  const uint8 CopyWriteStatus
);

/** \brief Function to write the requested status in the section header
 *
 * \param[in] FieldIndex field index of the section
 * \param[in] StatusValue section status
 * \param[in] TargetSection section to be updated
 */
STATIC FUNC(void, FEE_CODE) Fee_WriteSectionHeader
(
  const uint8 FieldIndex,
  const uint16 StatusValue,
  const uint8 TargetSection

);

/** \brief Function to read data from flash into Fee buffer
 *
 * \param[in] SourceAddress flash source address
 * \param[in] Length data length
 */
STATIC FUNC(void, FEE_CODE) Fee_FlsRead
(
  const Fls_AddressType SourceAddress,
  const Fls_LengthType Length
);

/** \brief Function to set state after failure  */
STATIC FUNC(void, FEE_CODE) Fee_SetStatesOnFailure(void);

/** \brief Function to start startup 2  */
STATIC FUNC(void, FEE_CODE) Fee_StartInit2(void);

/** \brief Function to initiate Fee read  */
STATIC FUNC(void, FEE_CODE) Fee_InitiateRead(void);

/** \brief State entry function to initiate the flash write of block info
 *         to invalidate a block.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInvalidate(void);

/** \brief State function to invalidate a block both in the cache
 *         and in flash memory
 */
STATIC FUNC(void, FEE_CODE) Fee_SfInvalidate(void);

/** \brief State entry function to initiate the read of Section0 header from flash */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitReadHeader0(void);

/** \brief State function to set the section status for Section0 after the
 *         Section Header is read from flash.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfInitReadHeader0(void);

/** \brief State entry function to initiate the read of Section1 header from flash */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitReadHeader1(void);

/** \brief State function to set the section status for Section1 after the
 *         Section Header is read from flash.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfInitReadHeader1(void);

/** \brief State entry function to initiate the erase of flash section/s depending on the
 *         section status
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitErase(void);

/** \brief State function to check the status of flash section erase operation.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfInitErase(void);

/** \brief State entry function to initiate the read of block infos to fill the cache.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitFillCache(void);

/** \brief State function to check the status of reading block infos to fill the cache.
 *         It checks whether more block infos are present in the current section
 *         and initiate the read operation.
 *         It also checks if the other section also need to be read.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfInitFillCache(void);

/** \brief State entry function to read the block number of the last copied block
 *         to check the consistency of the block.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitChkLastBlock(void);

/** \brief State function to check the consistency of the last copied block
 *         to decide whether this block need to be copied again.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfInitChkLastBlock(void);

/** \brief State entry function to initiate writing the block info
 *         at the next location in flash memory.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryWriteBlockInfo(void);

/** \brief State function to check if writing the block info was successful
 *         and trigger the transition to write block data or update
 *         the block data address if write job was canceled.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfWriteBlockInfo(void);

/** \brief State entry function to initiate writing the block data
 *         at the next location in flash memory.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryWriteBlockData(void);

/** \brief State function to check if writing of block data was successful
 *         and resume a pending switch section or erase operation.
 *         If no operation is pending, state is set to idle.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfWriteBlockData(void);

/** \brief State entry function to initiate the flash write to mark the section
 *         as Active.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryWriteSectionActive(void);

/** \brief State function to check if the flash write to mark the section
 *         as Active was successful. It initializes the switch section
 *         operation if the header write is not during startup.
 *         Otherwise it starts the queued job which triggered the
 *         header write.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfWriteSectionActive(void);

/** \brief State entry function to initiate the flash write to mark the section
 *         as Full.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryWriteSectionFull(void);

/** \brief State function to check if the flash write to mark the section
 *         as Full was successful. If the job is not yet cancelled,
 *         it writes the active status and make the inactive section active.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfWriteSectionFull(void);

/** \brief State entry function to initiate the flash read for the block number using
 *         the address in cache.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryReadBlockNumber(void);

/** \brief State function to check if reading the block number from flash was
 *         successful. If the block is consistent, next state is set to
 *         FEE_READ_BLOCKDATA. Otherwise, the block is reported as
 *         MEMIF_BLOCK_INCONSISTENT by calling Nvm job error notification if
 *         configured.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfReadBlockNumber(void);

/** \brief State entry function to initiate the flash read for the block data using
 *         the address in cache.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryReadBlockData(void);

/** \brief State function to check if reading the block data from flash was
 *         successful. If successful, the block result is set as
 *         MEMIF_JOB_OK and Nvm job end notification is called if configured.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfReadBlockData(void);

/** \brief State function to process section switch operation.
 *         It gets the next block to be copied and check whether space is available
 *         to copy the block. If space is available the block is copied by calling
 *         Fee_InitiateBlockSwitch(). Otherwise, the block is marked as invalid in
 *         cache and next block which fits in available space is searched.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfSectionSwitching(void);

/** \brief State entry function to initiate reading block data from flash
 *         during section switch operation.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSReadData(void);

/** \brief State function to check the status of reading block data from flash
 *         during section switch operation.
 *         If an immediate job has been requested, the ongoing flash read is canceled
 *         and the write job is initiated. Otherwise, the block id at the end of data
 *         is checked for consistency and if the block is inconsistent, cache is update
 *         with inconsistent address, and next state is set to FEE_SS_COPY_INFO.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfSSReadData(void);

/** \brief State entry function to prepare the buffer and write the block info to flash
 *         during section switch operation. If the flash write is accepted,
 *         the Info Address is updated.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSCopyInfo(void);

/** \brief State function to check the status of writing the block info to flash.
 *         If the block is marked as inconsistent in cache, the data block need
 *         not be copied. Otherwise the data block is copied by setting
 *         the next state to FEE_SS_COPY_DATA.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfSSCopyInfo(void);

/** \brief State entry function to prepare the buffer and write the block data to flash
 *         during section switch operation. If the flash write is accepted,
 *         the Data Address is updated.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSCopyData(void);

/** \brief State function to check the status of writing the block data to flash.
 *         If copy of block data was successful, cache is updated and next block
 *         is taken for copying by setting the next state to FEE_SECTION_SWITCHING.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfSSCopyData(void);

/** \brief State entry function to write the section header as Copied
 *         during section switch operation.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSWriteCopied(void);

/** \brief State function to check the status of marking the section header as
 *         Copied during section switch operation. If the write was successful,
 *         Fee next state is set to FEE_SS_ERASE_SECTION to erase the section.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfSSWriteCopied(void);

/** \brief State entry function to initiate flash erase */
STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSEraseSection(void);

/** \brief State function to check if the flash erase was successful.
 *         If an immediate write is in queue the erase job is cancelled and
 *         write job is started. Otherwise, if the flash erase was successful,
 *         any pending job is executed or Fee goes to idle state.
 */
STATIC FUNC(void, FEE_CODE) Fee_SfSSEraseSection(void);

#define FEE_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]=========================================*/

#define FEE_START_SEC_VAR_INIT_UNSPECIFIED
#include <MemMap.h>

VAR(Fee_State_t, FEE_VAR) Fee_State = FEE_UNINIT;
VAR(Fee_State_t, FEE_VAR) Fee_NextState = FEE_STATE_INVALID;

#define FEE_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <MemMap.h>

#define FEE_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>
/* !LINKSTO Fee.GlobalVariables,1 */
VAR(Fee_Gv_t, FEE_VAR_NOINIT) Fee_Gv;

#define FEE_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

/*==================[internal data]==========================================*/

#define FEE_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/* \brief Array of function pointers of main state machine */
STATIC CONST(Fee_StateDescType, FEE_CONST) Fee_States[FEE_NUM_STATES] =
{
  {NULL_PTR, NULL_PTR},
  {NULL_PTR, &Fee_SfIdleState},
  {&Fee_SfOnEntryInitReadHeader0, &Fee_SfInitReadHeader0},
  {&Fee_SfOnEntryInitReadHeader1, &Fee_SfInitReadHeader1},
  {&Fee_SfOnEntryInitErase, &Fee_SfInitErase},
  {&Fee_SfOnEntryInitFillCache, &Fee_SfInitFillCache},
  {&Fee_SfOnEntryInitChkLastBlock, &Fee_SfInitChkLastBlock},
  {&Fee_SfOnEntryReadBlockNumber, &Fee_SfReadBlockNumber},
  {&Fee_SfOnEntryReadBlockData, &Fee_SfReadBlockData},
  {&Fee_SfOnEntryWriteBlockInfo, &Fee_SfWriteBlockInfo},
  {&Fee_SfOnEntryWriteBlockData, &Fee_SfWriteBlockData},
  {&Fee_SfOnEntryWriteSectionActive, &Fee_SfWriteSectionActive},
  {&Fee_SfOnEntryWriteSectionFull, &Fee_SfWriteSectionFull},
  {&Fee_SfOnEntryInvalidate, &Fee_SfInvalidate},
  {NULL_PTR, &Fee_SfSectionSwitching},
  {&Fee_SfOnEntrySSReadData, &Fee_SfSSReadData},
  {&Fee_SfOnEntrySSCopyInfo, &Fee_SfSSCopyInfo},
  {&Fee_SfOnEntrySSCopyData, &Fee_SfSSCopyData},
  {&Fee_SfOnEntrySSWriteCopied, &Fee_SfSSWriteCopied},
  {&Fee_SfOnEntrySSEraseSection, &Fee_SfSSEraseSection}
};

#define FEE_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external function definitions]==========================*/

#define FEE_START_SEC_CODE
#include <MemMap.h>

/*--------------------------[Fee_Init]---------------------------------------*/

FUNC(void, FEE_CODE) Fee_Init
(
  void
)
{
  uint16_least i;

  DBG_FEE_INIT_ENTRY();

  Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
  /* Clear all module globals to initial value */
  Fee_Gv.FeeStartup1Pending = TRUE;
  Fee_Gv.FeeStartup2Pending = FALSE;
  Fee_Gv.FeeCopyPending = FALSE;
  Fee_Gv.FeeErasePending = FALSE;
  Fee_Gv.FeeInterruptSwitch = FALSE;
  Fee_Gv.FeeFillBoth = FALSE;
  Fee_Gv.FeeSectionFull = FALSE;
  Fee_Gv.FeeJobCancelled = FALSE;
  Fee_Gv.FeeFlashFailure = FALSE;
  Fee_Gv.FeeCheckCopyReqd = FALSE;
  Fee_Gv.FeeCopyBlockIndex = 0U;

  Fee_Gv.FeeDelayTimer = 0U;
  Fee_Gv.FeeJob = FEE_NO_JOB;

  Fee_Gv.FeeJobResult = MEMIF_JOB_OK;
  Fee_Gv.FeeFlashJobResult = MEMIF_JOB_OK;
  Fee_Gv.FeeSectionStatus = 0U;
  Fee_Gv.FeeLastCopiedBlockAddress = FEE_INCONSISTENT_ADDRESS;

  /* Update cache entry for every block as invalid, as the
   * blocks are not yet identified and thus not available for read */
  for (i = 0U; i < FEE_NO_OF_CONFIGURED_BLOCKS; ++i)
  {
    Fee_Gv.FeeCache[i] = FEE_INVALID_BLOCKADDRESS;
  }

  /* Initiate Startup1 */
  FEE_TRIGGER_TRANS(FEE_INIT_READ_HEADER0);
  
  DBG_FEE_STATE((Fee_State),(FEE_IDLE));
  Fee_State = FEE_IDLE;

  DBG_FEE_INIT_EXIT();
}

/*---------------------------------[Fee_SetMode]---------------------------------------*/

#if (FEE_SET_MODE_SUPPORTED == STD_ON)

FUNC(void, FEE_CODE) Fee_SetMode(MemIf_ModeType Mode)
{
  DBG_FEE_SETMODE_ENTRY(Mode);
#if (FEE_DEV_ERROR_DETECT == STD_ON)

  /* is the module initialized? */

  if(FEE_UNINIT == Fee_State)
  {
    FEE_DET_REPORT_ERROR(FEE_SETMODE_API_ID, FEE_E_UNINIT);
  }

  /* is the module busy? */
  else if(MEMIF_BUSY == Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_SETMODE_API_ID, FEE_E_BUSY);
  }

  /* is the module busy internal? */
  else if(MEMIF_BUSY_INTERNAL == Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_SETMODE_API_ID, FEE_E_BUSY_INTERNAL);
  }

  /* is the given mode valid? */
  else if((MEMIF_MODE_FAST != Mode) && (MEMIF_MODE_SLOW != Mode))
  {
    FEE_DET_REPORT_ERROR(FEE_SETMODE_API_ID, FEE_E_INVALID_MODE);
  }
  else
#endif

  {
    /* Set the operation mode of the flash driver */
    FEE_FLS_SETMODE(Mode);
  }

  DBG_FEE_SETMODE_EXIT(Mode);
}

#endif
/*------------------------------------[Fee_Read]---------------------------------------*/

FUNC(Std_ReturnType, FEE_CODE) Fee_Read
(
  uint16                                 BlockNumber,
  uint16                                 BlockOffset,
  P2VAR(uint8, AUTOMATIC, FEE_APPL_DATA) DataBufferPtr,
  uint16                                 Length
)
{
  Std_ReturnType RetValue = E_NOT_OK;

  DBG_FEE_READ_ENTRY(BlockNumber,BlockOffset,DataBufferPtr,Length);

#if (FEE_DEV_ERROR_DETECT == STD_ON)
  /* Is the module initialized? */
  if (FEE_UNINIT == Fee_State)
  {
    FEE_DET_REPORT_ERROR(FEE_READ_API_ID, FEE_E_UNINIT);
  }

  /* is the module busy? */
  else if(MEMIF_BUSY == Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_READ_API_ID, FEE_E_BUSY);
  }

  /* is the module internal busy? */
  else if(MEMIF_BUSY_INTERNAL == Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_READ_API_ID, FEE_E_BUSY_INTERNAL);
  }

  /* Is the data buffer valid? */
  else if (NULL_PTR == DataBufferPtr)
  {
    FEE_DET_REPORT_ERROR(FEE_READ_API_ID, FEE_E_INVALID_DATA_PTR);
  }

  else
#endif
  {

    if (MEMIF_IDLE != Fee_Gv.FeeStatus)
    {
      /* return value is already E_NOT_OK */
    }
    else
    {
      /* Get the block index of requested block */
      const uint16 IntBlockNumber = Fee_SearchConfigTable(BlockNumber);

#if (FEE_DEV_ERROR_DETECT == STD_ON)
      /* has the block number been configured in the BlockCfg Table? */
      if (IntBlockNumber >= FEE_NO_OF_CONFIGURED_BLOCKS)
      {
        FEE_DET_REPORT_ERROR(FEE_READ_API_ID, FEE_E_INVALID_BLOCK_NO);
      }

      /* is the block offset valid? */
      else if ( BlockOffset >= Fee_BlockCfg[IntBlockNumber].FeeBlockSize)
      {
        FEE_DET_REPORT_ERROR(FEE_READ_API_ID, FEE_E_INVALID_BLOCK_OFS);
      }

      /* is the block length valid? */
      else if ((Length + BlockOffset) > Fee_BlockCfg[IntBlockNumber].FeeBlockSize)
      {
        FEE_DET_REPORT_ERROR(FEE_READ_API_ID, FEE_E_INVALID_BLOCK_LEN);
      }
      else
#endif
      {

        Fee_Gv.FeeIntBlockNumber = IntBlockNumber;
        Fee_Gv.FeeBlockDataReadBuffPtr = DataBufferPtr;
        Fee_Gv.FeeLength = Length;
        Fee_Gv.FeeBlockReadOffset = BlockOffset;
        Fee_Gv.FeeJob = FEE_READ_JOB;
        Fee_Gv.FeeStatus = MEMIF_BUSY;
        Fee_Gv.FeeJobResult = MEMIF_JOB_PENDING;
        RetValue = E_OK;

        if (TRUE == Fee_Gv.FeeStartup1Pending)
        {
          Fee_Gv.FeeFlashFailure = FALSE;
          /* Initiate Startup1 */
          FEE_TRIGGER_TRANS(FEE_INIT_READ_HEADER0);
        }
        else
        {
          /* Restart the delay timer, if startup or switch internal
             operation time delay is active */
          if (0U < Fee_Gv.FeeDelayTimer)
          {
            Fee_Gv.FeeDelayTimer = FEE_DELAY;
          }
        }
      }
    }
  }


  DBG_FEE_READ_EXIT(RetValue,BlockNumber,BlockOffset,DataBufferPtr,Length);
  return RetValue;
}

/*----------------------------------[Fee_Write]---------------------------------------*/

FUNC(Std_ReturnType, FEE_CODE) Fee_Write
(
  uint16                                 BlockNumber,
  P2CONST(uint8, AUTOMATIC, FEE_APPL_DATA) DataBufferPtr
)
{
  Std_ReturnType ReturnValue = E_NOT_OK;

  /* Get the block index of requested block */
  const uint16 IntBlockNumber = Fee_SearchConfigTable(BlockNumber);

  DBG_FEE_WRITE_ENTRY(BlockNumber,DataBufferPtr);


#if (FEE_DEV_ERROR_DETECT == STD_ON)
  /* is the module initialized? */
  if (FEE_UNINIT == Fee_State)
  {
    FEE_DET_REPORT_ERROR(FEE_WRITE_API_ID, FEE_E_UNINIT);
  }

  /* is the module busy? */
  else if(MEMIF_BUSY == Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_WRITE_API_ID, FEE_E_BUSY);
  }

  /* is the module internal busy? */
  else if((MEMIF_BUSY_INTERNAL == Fee_Gv.FeeStatus) &&
          (TRUE != Fee_BlockCfg[IntBlockNumber].FeeImmediateData))
  {
    FEE_DET_REPORT_ERROR(FEE_WRITE_API_ID, FEE_E_BUSY_INTERNAL);
  }

  /* is the data buffer valid? */
  else if (NULL_PTR == DataBufferPtr)
  {
    FEE_DET_REPORT_ERROR(FEE_WRITE_API_ID, FEE_E_INVALID_DATA_PTR);
  }
  else
#endif
  {

#if (FEE_DEV_ERROR_DETECT == STD_ON)
    /* has the block been configured in the BlockCfg Table? */
    if (IntBlockNumber >= FEE_NO_OF_CONFIGURED_BLOCKS)
    {
      FEE_DET_REPORT_ERROR(FEE_WRITE_API_ID, FEE_E_INVALID_BLOCK_NO);
    }
    else
#endif
    {

      /* If the module is busy processing request or if an internal operation going on
         and the request is not for immediate block, reject the write request */
      if ((MEMIF_BUSY == Fee_Gv.FeeStatus) ||
          ((MEMIF_BUSY_INTERNAL == Fee_Gv.FeeStatus) &&
           (TRUE != Fee_BlockCfg[IntBlockNumber].FeeImmediateData)
           )
          )
      {
        /* Do nothing, return value is already set */
      }
      else
      {
        ReturnValue = E_OK;
        Fee_Gv.FeeJob = FEE_WRITE_JOB;
        Fee_Gv.FeeIntBlockNumber = IntBlockNumber;
        Fee_Gv.FeeBlockDataWriteBuffPtr = DataBufferPtr;
        Fee_Gv.FeeJobResult = MEMIF_JOB_PENDING;
        Fee_Gv.FeeFlashFailure = FALSE;

        if (TRUE == Fee_Gv.FeeStartup1Pending)
        {
          /* Check if startup1 is not currently in progess */
          if(MEMIF_BUSY_INTERNAL != Fee_Gv.FeeStatus)
          {
            /* Initiate Startup1 */
            FEE_TRIGGER_TRANS(FEE_INIT_READ_HEADER0);
          }
        }
        else
        {
          /* Check if sufficient space available to write in the current active section */
          if (TRUE == Fee_ChkFreeSpace())
          {
            /* Set the interrupt switch flag so that immediate write can be started
               if section switching is ongoing.
               If section switching is not ongoing this can still be set as it is cleared
               when the job actually starts (Fee_SfOnEntryWriteBlockInfo) */
            Fee_Gv.FeeInterruptSwitch = TRUE;
          }

          /* Restart the delay timer, if startup or switch internal
             operation time delay is active */
          if (0U < Fee_Gv.FeeDelayTimer)
          {
            Fee_Gv.FeeDelayTimer = FEE_DELAY;
          }
        }

        Fee_Gv.FeeStatus = MEMIF_BUSY;
      }
    }
  }


  DBG_FEE_WRITE_EXIT(ReturnValue,BlockNumber,DataBufferPtr);
  return ReturnValue;
}


/*------------------------------------[Fee_Cancel]---------------------------------------*/

FUNC(void, FEE_CODE) Fee_Cancel
(
  void
)
{
  DBG_FEE_CANCEL_ENTRY();
#if (FEE_DEV_ERROR_DETECT == STD_ON)
  /* is the module initialized? */

  if (FEE_UNINIT == Fee_State)
  {
    FEE_DET_REPORT_ERROR(FEE_CANCEL_API_ID, FEE_E_UNINIT);
  }
  /* Check if the current module status is not MEMIF_BUSY */
  else if (MEMIF_BUSY != Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_CANCEL_API_ID, FEE_E_INVALID_CANCEL);
  }
  else
#endif
  {
    if (MEMIF_BUSY == Fee_Gv.FeeStatus)
    {
      switch (Fee_State)
      {
        case FEE_IDLE: /* job is in the queue, due to some internal management operations */
          Fee_Gv.FeeStatus = MEMIF_IDLE;
          FEE_TRIGGER_TRANS(FEE_IDLE);
          break;

          /* Check whether Write of block data is in process */
        case FEE_WRITE_BLOCK_DATA:
          /* Cancel the flash job if flash cancel API is available
             and set the flash job result to cancelled */
          FEE_CANCEL_FLASHJOB();
          /* Check whether the write job had interrupted an ongoing switch operation */
          if (SECTION_SWITCHING_INPROGRESS())
          {
            Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
          }
          else
          {
            Fee_Gv.FeeStatus = MEMIF_IDLE;
          }
          FEE_TRIGGER_TRANS(FEE_IDLE);
          break;

          /* Check whether read job is in process */
        case FEE_READ_BLOCKNUMBER:  /* fall through */
        case FEE_READ_BLOCKDATA:
          /* Cancel the flash job if flash cancel API is available
             and set the flash job result to cancelled */
          FEE_CANCEL_FLASHJOB();
          Fee_Gv.FeeStatus = MEMIF_IDLE;
          FEE_TRIGGER_TRANS(FEE_IDLE);
          break;

          /* Check whether write of a management info is ongoing (which is not cancellable) */
        case FEE_WRITE_BLOCK_INFO:
          Fee_Gv.FeeCancelledBlock = Fee_Gv.FeeIntBlockNumber;
          Fee_Gv.FeeJobCancelled = TRUE;
          Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
          break;

          /* Check whether write of a management info is ongoing (which is not cancellable) */
        case FEE_WRITE_SECTION_ACTIVE: /* fall through */
        case FEE_WRITE_SECTION_FULL:  /* fall through */
        case FEE_INVALIDATE:
          Fee_Gv.FeeJobCancelled = TRUE;
          Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
          break;

          /* Else the job is in queue, due to some internal management operations */
        default:
          Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
          break;
      }

      /* Unqueue the current job */
      Fee_Gv.FeeJob = FEE_NO_JOB;
      Fee_Gv.FeeJobResult = MEMIF_JOB_CANCELED;

      /* Clear the Interrupt switch flag as the job canceled may be write job */
      Fee_Gv.FeeInterruptSwitch = FALSE;

      /* Call NVM's error notification function */
      FEE_NVM_JOB_ERROR_NOTIFICATION();
    }
  }

  DBG_FEE_CANCEL_EXIT();
}

/*----------------------------------[Fee_GetStatus]---------------------------------------*/

FUNC(MemIf_StatusType, FEE_CODE) Fee_GetStatus
(
  void
)
{
  MemIf_StatusType RetVal;

  DBG_FEE_GETSTATUS_ENTRY();

  /* is the module initialized? */
  if (FEE_UNINIT == Fee_State)
  {
#if (FEE_DEV_ERROR_DETECT == STD_ON)
    FEE_DET_REPORT_ERROR(FEE_GETSTATUS_API_ID, FEE_E_UNINIT);
#endif
    RetVal = MEMIF_UNINIT;
  }
  else
  {
    /* Flash get status is not called, Fee status is returned;
       Reference: EB Refinement EB_FEE090 */
    RetVal = Fee_Gv.FeeStatus;
  }


  DBG_FEE_GETSTATUS_EXIT(RetVal);
  return RetVal;
}

/*----------------------------------[Fee_GetJobResult]---------------------------------------*/

FUNC(MemIf_JobResultType, FEE_CODE) Fee_GetJobResult
(
  void
)
{
  MemIf_JobResultType RetVal;

  DBG_FEE_GETJOBRESULT_ENTRY();

#if (FEE_DEV_ERROR_DETECT == STD_ON)
  /* is the module initialized? */
  if (FEE_UNINIT == Fee_State)
  {
    FEE_DET_REPORT_ERROR(FEE_GETJOBRESULT_API_ID, FEE_E_UNINIT);
    RetVal = MEMIF_JOB_FAILED;
  }
  else
#endif
  {
    /* Flash get job result is not called, Fee job result is returned;
       Reference: EB Refinement EB_FEE091 */
    RetVal = Fee_Gv.FeeJobResult;
  }


  DBG_FEE_GETJOBRESULT_EXIT(RetVal);
  return RetVal;
}

/*----------------------------------[Fee_InvalidateBlock]---------------------------------------*/

FUNC(Std_ReturnType, FEE_CODE) Fee_InvalidateBlock
(
  uint16 BlockNumber
)
{
  Std_ReturnType ReturnValue = E_NOT_OK;

  DBG_FEE_INVALIDATEBLOCK_ENTRY(BlockNumber);

#if (FEE_DEV_ERROR_DETECT == STD_ON)
  /* is the module initialized? */
  if (FEE_UNINIT == Fee_State)
  {
    FEE_DET_REPORT_ERROR(FEE_INVALIDATEBLOCK_API_ID, FEE_E_UNINIT);
  }

  /* is the module busy? */
  else if(MEMIF_BUSY == Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_INVALIDATEBLOCK_API_ID, FEE_E_BUSY);
  }

  /* is the module busy internal? */
  else if(MEMIF_BUSY_INTERNAL == Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_INVALIDATEBLOCK_API_ID, FEE_E_BUSY_INTERNAL);
  }

  else
#endif
  {

    /* Get the block index of requested block */
    const uint16 IntBlockNumber = Fee_SearchConfigTable(BlockNumber);

#if (FEE_DEV_ERROR_DETECT == STD_ON)
    /* Check whether the block has been configured in the BlockCfg Table */
    if (IntBlockNumber >= FEE_NO_OF_CONFIGURED_BLOCKS)
    {
      FEE_DET_REPORT_ERROR(FEE_INVALIDATEBLOCK_API_ID, FEE_E_INVALID_BLOCK_NO);
    }

    else
#endif
    {

      if (MEMIF_IDLE != Fee_Gv.FeeStatus)
      {
        /* Do nothing; return value is already set as E_NOT_OK */
      }
      else
      {
        ReturnValue = E_OK;
        Fee_Gv.FeeIntBlockNumber = IntBlockNumber;
        Fee_Gv.FeeJob = FEE_INVALIDATE_JOB;
        Fee_Gv.FeeStatus = MEMIF_BUSY;
        Fee_Gv.FeeJobResult = MEMIF_JOB_PENDING;
        Fee_Gv.FeeFlashFailure = FALSE;

        if (TRUE == Fee_Gv.FeeStartup1Pending)
        {
          /* Initiate Startup1 */
          FEE_TRIGGER_TRANS(FEE_INIT_READ_HEADER0);
        }
        else
        {
          /* Restart the delay timer, if startup or switch internal
             operation time delay is active */
          if (0U < Fee_Gv.FeeDelayTimer)
          {
            Fee_Gv.FeeDelayTimer = FEE_DELAY;
          }
        }
      }
    }
  }


  DBG_FEE_INVALIDATEBLOCK_EXIT(ReturnValue,BlockNumber);
  return ReturnValue;
}

/*----------------------------------[Fee_GetVersionInfo]---------------------------------------*/

#if (FEE_VERSION_INFO_API == STD_ON)

FUNC(void, FEE_CODE)Fee_GetVersionInfo
(P2VAR(Std_VersionInfoType, AUTOMATIC, FEE_APPL_DATA)VersionInfoPtr)
{
  DBG_FEE_GETVERSIONINFO_ENTRY(VersionInfoPtr);
#if (FEE_DEV_ERROR_DETECT == STD_ON)
  /* is the version pointer valid? */

  if(NULL_PTR == VersionInfoPtr)
  {
    FEE_DET_REPORT_ERROR(FEE_GET_VERSION_INFO_API_ID, FEE_E_INVALID_DATA_PTR);
  }

  else
#endif
  {
    VersionInfoPtr->vendorID = FEE_VENDOR_ID;
    VersionInfoPtr->moduleID = FEE_MODULE_ID;
    VersionInfoPtr->sw_major_version = FEE_SW_MAJOR_VERSION;
    VersionInfoPtr->sw_minor_version = FEE_SW_MINOR_VERSION;
    VersionInfoPtr->sw_patch_version = FEE_SW_PATCH_VERSION;
  }

  DBG_FEE_GETVERSIONINFO_EXIT(VersionInfoPtr);
}

#endif

/*----------------------------------[Fee_EraseImmediateBlock]----------------------------------*/

FUNC(Std_ReturnType, FEE_CODE) Fee_EraseImmediateBlock
(
  uint16 BlockNumber
)
{
  Std_ReturnType ReturnValue = E_NOT_OK;
  uint16 IntBlockNumber;

  DBG_FEE_ERASEIMMEDIATEBLOCK_ENTRY(BlockNumber);
#if (FEE_DEV_ERROR_DETECT == STD_ON)

  /* is the module initialized? */
  if (FEE_UNINIT == Fee_State)
  {
    FEE_DET_REPORT_ERROR(FEE_ERASEIMMEDIATEBLOCK_API_ID, FEE_E_UNINIT);
  }

  /* is the module busy? */
  else if(MEMIF_BUSY == Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_ERASEIMMEDIATEBLOCK_API_ID, FEE_E_BUSY);
  }

  /* is the module busy internal? */
  else if(MEMIF_BUSY_INTERNAL == Fee_Gv.FeeStatus)
  {
    FEE_DET_REPORT_ERROR(FEE_ERASEIMMEDIATEBLOCK_API_ID, FEE_E_BUSY_INTERNAL);
  }

  else
#endif
  {
    IntBlockNumber = Fee_SearchConfigTable(BlockNumber);

#if (FEE_DEV_ERROR_DETECT == STD_ON)

    /* Check whether the block has been configured in the BlockCfg Table and
     * that the block is an immediate block */
    if ((IntBlockNumber >= FEE_NO_OF_CONFIGURED_BLOCKS) ||
        (FALSE == Fee_BlockCfg[IntBlockNumber].FeeImmediateData))
    {
      FEE_DET_REPORT_ERROR(FEE_ERASEIMMEDIATEBLOCK_API_ID, FEE_E_INVALID_BLOCK_NO);
    }

    else
#endif
    {

#if (FEE_DEV_ERROR_DETECT == STD_OFF)
      TS_PARAM_UNUSED(BlockNumber);
#endif

      if (MEMIF_IDLE == Fee_Gv.FeeStatus)
      {
        /* FEE makes sure that there is always space available for
           immediate block write and it is not sure that this API is
           called just before the write of an immediate block. So this
           function does not do any erase function, but invalidates the block.
           The invalidation is done inorder to ensure that, a block read after
           erase immediate will be read as INVALID.
           The job is set as FEE_INVALIDATE_JOB */
        ReturnValue = E_OK;
        Fee_Gv.FeeIntBlockNumber = IntBlockNumber;
        Fee_Gv.FeeJob = FEE_INVALIDATE_JOB;
        Fee_Gv.FeeStatus = MEMIF_BUSY;
        Fee_Gv.FeeJobResult = MEMIF_JOB_PENDING;
        Fee_Gv.FeeFlashFailure = FALSE;

        if (TRUE == Fee_Gv.FeeStartup1Pending)
        {
          /* Initiate Startup1 */
          FEE_TRIGGER_TRANS(FEE_INIT_READ_HEADER0);
        }
        else
        {
          /* Restart the delay timer, if startup or switch internal
             operation time delay is active */
          if (0U < Fee_Gv.FeeDelayTimer)
          {
            Fee_Gv.FeeDelayTimer = FEE_DELAY;
          }
        }
      }
      else
      {
        /* do nothing: the return value is already E_NOT_OK */
      }
    }
  }


  DBG_FEE_ERASEIMMEDIATEBLOCK_EXIT(ReturnValue,BlockNumber);
  return ReturnValue;
}


#if FEE_POLLING_MODE == STD_OFF

/*----------------------------------[Fee_JobEndNotification]-----------------------------------*/

FUNC(void,FEE_CODE)Fee_JobEndNotification(void)
{

  DBG_FEE_JOBENDNOTIFICATION_ENTRY();
  if (Fee_State != FEE_UNINIT)
  {
    /* The last flash job was success */
    if(MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
    {
      Fee_Gv.FeeFlashJobResult = MEMIF_JOB_OK;
    }
  }

  DBG_FEE_JOBENDNOTIFICATION_EXIT();
}
/*----------------------------------[Fee_JobErrorNotification]--------------------------------*/

FUNC(void,FEE_CODE)Fee_JobErrorNotification(void)
{

  DBG_FEE_JOBERRORNOTIFICATION_ENTRY();
  if (Fee_State != FEE_UNINIT)
  {
    /* The last flash job was failure */
    if( MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
    {
      Fee_Gv.FeeFlashJobResult = MEMIF_JOB_FAILED;
    }
  }

  DBG_FEE_JOBERRORNOTIFICATION_EXIT();
}

#endif

/*----------------------------------[Fee_MainFunction]---------------------------------------*/

FUNC(void, FEE_CODE) Fee_MainFunction(void)
{

  DBG_FEE_MAINFUNCTION_ENTRY();
  do
  {
    /* enter the next requested state */
    while (Fee_NextState != FEE_STATE_INVALID)
    {

    DBG_FEE_STATE((Fee_State),(Fee_NextState));
      Fee_State     = Fee_NextState;

      DBG_FEE_NEXTSTATE((Fee_NextState),(FEE_STATE_INVALID));
      Fee_NextState = FEE_STATE_INVALID;
      if (NULL_PTR != Fee_States[Fee_State].onEntry)
      {
        Fee_States[Fee_State].onEntry();
      }
    }

    /* call the state function if available */
    if ((Fee_State != FEE_STATE_INVALID)
      && (Fee_States[Fee_State].fn != NULL_PTR))
    {
      Fee_States[Fee_State].fn();
    }
  } while (Fee_NextState != FEE_STATE_INVALID);

  DBG_FEE_MAINFUNCTION_EXIT();
}

/*==================[internal function definitions]=========================*/

/*--------------------------[Fee_ProcessWriteOrInvalidateJob]----------------*/

STATIC FUNC(void, FEE_CODE) Fee_ProcessWriteOrInvalidateJob(void)
{
  /* If Part2 of initialization is pending */

  DBG_FEE_PROCESSWRITEORINVALIDATEJOB_ENTRY();
  if(TRUE == Fee_Gv.FeeStartup2Pending)
  {
    /* Start startup part2 */
    Fee_StartInit2();
  }
  else
  {
    /* If start up is finished and free space is available in active section;
     * active section is not set as FULL due to previous write */
    if((TRUE == Fee_ChkFreeSpace()) && (FALSE == Fee_Gv.FeeSectionFull))
    {
      if(FEE_WRITE_JOB == Fee_Gv.FeeJob)
      {
        /* Start block write as space is available in active section */
        FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_INFO);
      }
      /* This is an invalidate job */
      else
      {
        /* Initiate the Block Info write as space is avaliable
         * in the active section */
        FEE_TRIGGER_TRANS(FEE_INVALIDATE);
      }
    }
    /* If space is not available and copy operation is pending,
     * section switching is started without delay */
    else if(TRUE == Fee_Gv.FeeCopyPending)
    {
      /* Start switch copy operation */
      FEE_TRIGGER_TRANS(FEE_SECTION_SWITCHING);
    }
    /* If space is not available and inactive section erase is pending,
     * Erase is started without delay */
    else if(TRUE == Fee_Gv.FeeErasePending)
    {
      /* Start erase of full section */
      FEE_TRIGGER_TRANS(FEE_SS_ERASE_SECTION);
    }
    /* If there is no sufficient space in the active section and
     * inactive section is in erased state */
    else
    {
      /* Write full status to the active section header if it is not written
       * else write active status to the inactive section header */
      if(FALSE == Fee_Gv.FeeSectionFull)
      {
        FEE_TRIGGER_TRANS(FEE_WRITE_SECTION_FULL);
      }
      /* Full status is already written to active section */
      else
      {
        FEE_TRIGGER_TRANS(FEE_WRITE_SECTION_ACTIVE);
      }
    }
  }

  DBG_FEE_PROCESSWRITEORINVALIDATEJOB_EXIT();
}

/*--------------------------[Fee_ProcessInvalidateJob]------------------------*/
STATIC FUNC(void, FEE_CODE) Fee_ProcessInvalidateJob(void)
{

  DBG_FEE_PROCESSINVALIDATEJOB_ENTRY();
  if (FEE_INVALID_BLOCKADDRESS == Fee_Gv.FeeCache[Fee_Gv.FeeIntBlockNumber])
  {
    Fee_Gv.FeeJobResult = MEMIF_JOB_OK;
    Fee_Gv.FeeStatus = MEMIF_IDLE;
    Fee_Gv.FeeJob = FEE_NO_JOB;

    /* Stop the job processing as the block is found to be invalidated in cache.
     * The state machine shall return to IDLE state.
     */
    FEE_TRIGGER_TRANS(FEE_IDLE);

    FEE_NVM_JOB_END_NOTIFICATION();
  }
  else
  {
    Fee_ProcessWriteOrInvalidateJob();
  }

  DBG_FEE_PROCESSINVALIDATEJOB_EXIT();
}

/*--------------------------[Fee_CalculateSpaceforSS]------------------------*/

STATIC FUNC(Fls_LengthType, FEE_CODE) Fee_CalculateSpaceforSS
(
  void
)
{
  Fls_LengthType SpaceRequired = 0U;
  Fls_LengthType LargestBlockSize = 0U;
  uint16_least Index;

  DBG_FEE_CALCULATESPACEFORSS_ENTRY();

  for (Index = Fee_Gv.FeeCopyBlockIndex; Index < FEE_NO_OF_CONFIGURED_BLOCKS; Index++)
  {
    const uint16 FeeBlockAlignedSize = Fee_BlockCfg[Index].FeeBlockAlignedSize;

    SpaceRequired += FeeBlockAlignedSize + (Fls_LengthType)FEE_BLOCKINFO_ALIGNED_SIZE;

    if(LargestBlockSize < FeeBlockAlignedSize)
    {
      LargestBlockSize = FeeBlockAlignedSize;
    }
  }
  SpaceRequired += LargestBlockSize + FEE_BLOCKINFO_ALIGNED_SIZE;


  DBG_FEE_CALCULATESPACEFORSS_EXIT(SpaceRequired);
  return SpaceRequired;
}

/*--------------------------[Fee_SetSectionStatue]---------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SetSectionStatus
(
  const uint8 Section
)
{
  /* Check whether the copied field is updated */

  DBG_FEE_SETSECTIONSTATUS_ENTRY(Section);
  if (FEE_SECTION_STATUS_ERASED != FEE_READ_WORD(Fee_Gv.FeeBuffer, FEE_COPIEDFIELD_INDEX))
  {
    /* Check whether the full field is not erased and active filed is consistent */
    if ((FEE_SECTION_STATUS_ERASED != FEE_READ_WORD(Fee_Gv.FeeBuffer, FEE_FULLFIELD_INDEX)) &&
        (FEE_SECTION_STATUS_ACTIVE == FEE_READ_WORD(Fee_Gv.FeeBuffer, FEE_ACTIVEFIELD_INDEX))
       )
    {
      Fee_Gv.FeeSectionStatus |= FEE_SECTION_COPIED(Section);
    }
    else
    {
      Fee_Gv.FeeSectionStatus |= FEE_SECTION_INCONSISTENT(Section);
    }
  }

  /* Check whether the full field is updated */
  else if (FEE_SECTION_STATUS_ERASED != FEE_READ_WORD(Fee_Gv.FeeBuffer, FEE_FULLFIELD_INDEX))
  {
    /* Check whether the active field is consistent */
    if (FEE_SECTION_STATUS_ACTIVE == FEE_READ_WORD(Fee_Gv.FeeBuffer, FEE_ACTIVEFIELD_INDEX))
    {
      Fee_Gv.FeeSectionStatus |= FEE_SECTION_FULL(Section);
    }
    else
    {
      Fee_Gv.FeeSectionStatus |= FEE_SECTION_INCONSISTENT(Section);
    }
  }

  /* Check whether the active field is updated */
  else if (FEE_SECTION_STATUS_ACTIVE == FEE_READ_WORD(Fee_Gv.FeeBuffer, FEE_ACTIVEFIELD_INDEX))
  {
    Fee_Gv.FeeSectionStatus |= FEE_SECTION_ACTIVE(Section);
  }

  /* Check whether the active field is inconsistent */
  else if (FEE_SECTION_STATUS_ERASED != FEE_READ_WORD(Fee_Gv.FeeBuffer, FEE_ACTIVEFIELD_INDEX))
  {
    Fee_Gv.FeeSectionStatus |= FEE_SECTION_ACTIVEINCONSISTENT(Section);
  }

  else
  {
    Fee_Gv.FeeSectionStatus |= FEE_SECTION_ERASED(Section);
  }

  DBG_FEE_SETSECTIONSTATUS_EXIT(Section);
}

/*--------------------------[Fee_FillCache]----------------------------------*/

STATIC FUNC(boolean, FEE_CODE) Fee_FillCache
(
  void
)
{
  uint16 BufferIndex;
  uint16 AlignedSize;
  Std_ReturnType RetVal;
  boolean Completed = FALSE;

  DBG_FEE_FILLCACHE_ENTRY();

  /* Initialize the buffer index to the end of the buffer */
  BufferIndex = FEE_BUFFER_SIZE;

  /* Check each element in the buffer till all block infos are read */
  while ((BufferIndex >= FEE_BLOCKINFO_ALIGNED_SIZE) && (FALSE == Completed))
  {
    /* Go to the index which contains the next block info written to the section */
    BufferIndex -= FEE_BLOCKINFO_ALIGNED_SIZE;

    /* Check whether all block infos are read */
    if (TRUE == Fee_CheckBlockInfoErased(BufferIndex))
    {
      /* Set the flag to indicate that all block infos in the current section are read */
      Completed = TRUE;
    }
    else
    {

      const uint8 WriteCopy = Fee_Gv.FeeBuffer[BufferIndex];
      uint8 Checksum = Fee_Gv.FeeBuffer[BufferIndex+FEE_CHECKSUM_INDEX];

      /* Validate the checksum field of the block info */
      RetVal = Fee_ValidateChecksum(BufferIndex,Checksum);

      /* if block info is valid */
      if (E_OK == RetVal)
      {
        const uint16 BlockLength =
          FEE_READ_WORD(Fee_Gv.FeeBuffer, BufferIndex+FEE_BLOCKLENGTH_INDEX);
        const uint16 BlockNumber =
          FEE_READ_WORD(Fee_Gv.FeeBuffer, BufferIndex+FEE_BLOCKNUMBER_INDEX);
        const uint16 IntBlockNumber = Fee_SearchConfigTable(BlockNumber);

        /* Check whether the block number has been configured in the BlockCfg Table */
        if (IntBlockNumber < FEE_NO_OF_CONFIGURED_BLOCKS)
        {
          if (FEE_INVALID_SIZE == BlockLength)
          {
            Fee_Gv.FeeCache[IntBlockNumber] = FEE_INVALID_BLOCKADDRESS;
          }
          /* Check whether the block size is not reconfigured */
          else if (Fee_BlockCfg[IntBlockNumber].FeeBlockSize == BlockLength)
          {
            if(TRUE == Fee_Gv.FeeCheckCopyReqd)
            {
              if (FEE_BLOCK_COPIED == WriteCopy)
              {
               /* Check if the last copied block is same as the current block.
                * If the current block is same as last copied block, the old section
                * data address shall not be changed.
                *
                * This is because, this scenario means there were more than one attempt
                * to repair this block before last reset.
                * So the address taken from old section for this block is maintained.
                *
                * If last copied block is not same as the current block,
                * update the old section data address and copy block index.
                *
                * If last copied block is inconsistent,
                * i.e FeeLastCopiedBlockAddress = FEE_INCONSISTENT_ADDRESS,(start of loop
                * iteration or block once copied written again which becomes inconsistent)
                * update the section data address and copy block index. */

                if ((IntBlockNumber != Fee_Gv.FeeCopyBlockIndex) ||
                    (FEE_INCONSISTENT_ADDRESS == Fee_Gv.FeeLastCopiedBlockAddress)
                   )
                {
                  Fee_Gv.FeeCopyBlockIndex = IntBlockNumber;
                  Fee_Gv.FeeLastCopiedBlockAddress = Fee_Gv.FeeCache[IntBlockNumber];
                }
              }
              else
              {
                /* Check if copied block has been written again before last reset.
                 * For ex. an immediate block was written after copying the same block
                 * during last section switching.In this case this block shall not be
                 * copied again. So make FeeLastCopiedBlockAddress as inconsistent. */
                if (IntBlockNumber == Fee_Gv.FeeCopyBlockIndex)
                {
                  Fee_Gv.FeeLastCopiedBlockAddress = FEE_INCONSISTENT_ADDRESS;
                }
              }
            }
            Fee_Gv.FeeCache[IntBlockNumber] = Fee_Gv.FeeDataAddress;
            Fee_Gv.FeeDataAddress += Fee_BlockCfg[IntBlockNumber].FeeBlockAlignedSize;
          }

          /* Block size is reconfigured */
          else
          {
            /* Update the data address by calculating aligned size using the block length
             * read from the block info */
#if (FEE_VIRTUAL_PAGE_SIZE == 1U)
            AlignedSize = BlockLength + FEE_SIZE_BLOCKNUMBER;
#else
            AlignedSize = (uint16)
              (BlockLength + FEE_SIZE_BLOCKNUMBER + (uint16)
                ((FEE_VIRTUAL_PAGE_SIZE - (uint16)
                    ((BlockLength + FEE_SIZE_BLOCKNUMBER)%FEE_VIRTUAL_PAGE_SIZE)
                 )%FEE_VIRTUAL_PAGE_SIZE
                )
              );
#endif
            Fee_Gv.FeeDataAddress += AlignedSize;
          }
        }

        /* The block is not present in the new configuration */
        else
        {
         /* Update the data address by calculating aligned size using the block length
          * read from the block info; Cache cannot be updated as this block is not configured now*/
#if (FEE_VIRTUAL_PAGE_SIZE == 1U)
            AlignedSize = BlockLength + FEE_SIZE_BLOCKNUMBER;
#else
            AlignedSize = (uint16)
            (BlockLength + FEE_SIZE_BLOCKNUMBER + (uint16)
              ((FEE_VIRTUAL_PAGE_SIZE - (uint16)
                  ((BlockLength + FEE_SIZE_BLOCKNUMBER)%FEE_VIRTUAL_PAGE_SIZE)
               )%FEE_VIRTUAL_PAGE_SIZE
              )
            );
#endif
          Fee_Gv.FeeDataAddress += AlignedSize;
        }
      }

      /* Update the block info address */
      Fee_Gv.FeeInfoAddress -= FEE_BLOCKINFO_ALIGNED_SIZE;
      /* If all the block infos in the current section are read */
      if (Fee_Gv.FeeDataAddress > Fee_Gv.FeeInfoAddress)
      {
        /* Set the flag to indicate that all block infos in the current section is read */
        Completed = TRUE;
      }
    }
  }


  DBG_FEE_FILLCACHE_EXIT(Completed);
  return Completed;
}

/*--------------------------[Fee_NextStateOfFillCache]-----------------------*/

STATIC FUNC(void, FEE_CODE) Fee_NextStateOfFillCache
(
  void
)
{
  /* Check whether the last copied block needs to be repaired */

  DBG_FEE_NEXTSTATEOFFILLCACHE_ENTRY();
  if((TRUE == Fee_Gv.FeeCheckCopyReqd) &&
     (FEE_INCONSISTENT_ADDRESS != Fee_Gv.FeeLastCopiedBlockAddress)
    )
  {
    FEE_TRIGGER_TRANS(FEE_INIT_CHK_LASTBLOCK);
  }
  else
  {
    /* First part of initilization is complete; clear startup part1 pending flag */
    Fee_Gv.FeeStartup1Pending = FALSE;
    Fee_Gv.FeeStatus = MEMIF_BUSY;
    /* Initiate a queued read/write/invalidate job */
    Fee_InitiateJob();
  }
  Fee_Gv.FeeCheckCopyReqd = FALSE;

  DBG_FEE_NEXTSTATEOFFILLCACHE_EXIT();
}

/*--------------------------[Fee_ValidateChecksum]---------------------------*/

STATIC FUNC(Std_ReturnType, FEE_CODE) Fee_ValidateChecksum
(
  const uint16 BufferIndex,
  const uint8 Checksum
)
{
  uint8 GetChecksum;
  Std_ReturnType RetVal;

  DBG_FEE_VALIDATECHECKSUM_ENTRY(BufferIndex,Checksum);

  /* Calculate the check sum of the current block info */
  GetChecksum = Fee_CalculateChecksum(&(Fee_Gv.FeeBuffer[BufferIndex]),
    ((Fls_LengthType)FEE_BLOCKINFO_SIZE - (Fls_LengthType)FEE_CHECKSUM_SIZE));

  /* Check whether the checksum calculated is same as that present in the
   * block info */
  if (Checksum == GetChecksum)
  {
    RetVal = E_OK;
  }
  else
  {
    RetVal = E_NOT_OK;
  }


  DBG_FEE_VALIDATECHECKSUM_EXIT(RetVal,BufferIndex,Checksum);
  return RetVal;
}

/*--------------------------[Fee_CheckBlockInfoErased]-----------------------*/

STATIC FUNC (boolean, FEE_CODE) Fee_CheckBlockInfoErased
(
  const uint16 Index
)
{
  uint8_least i;
  boolean Flag = TRUE; /* if an erased byte is present */

  DBG_FEE_CHECKBLOCKINFOERASED_ENTRY(Index);

  for (i = 0U; i < FEE_BLOCKINFO_SIZE; i++)
  {
    /* Check whether the byte is in erased state */
    if (FEE_FLASH_ERASE_VALUE != Fee_Gv.FeeBuffer[Index+i])
    {
      Flag = FALSE;
      break;
    }
  }


  DBG_FEE_CHECKBLOCKINFOERASED_EXIT(Flag,Index);
  return Flag;
}

/*--------------------------[Fee_EraseFlash]---------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_EraseFlash
(
  const Fls_LengthType Length
)
{

  DBG_FEE_ERASEFLASH_ENTRY(Length);
  if (E_OK == Fls_Erase(FEE_SECTION_START_ADDRESS(Fee_Gv.FeeActiveSection), Length))
  {
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }

  /* last flash job was a failure */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_ERASEFLASH_EXIT(Length);
}

/*--------------------------[Fee_InitiateBlockSwitch]------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_InitiateBlockSwitch
(
  void
)
{
  /* if the block is inconsistent, copy only the block info */

  DBG_FEE_INITIATEBLOCKSWITCH_ENTRY();
  if (FEE_INCONSISTENT_ADDRESS == Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex])
  {
    FEE_TRIGGER_TRANS(FEE_SS_COPY_INFO);
  }

  /* Next block to be copied is a valid block; Now the block shall be copied only if the
   * block is consistent in flash. So read the block to check consistency  */
  else
  {
    FEE_TRIGGER_TRANS(FEE_SS_READ_DATA);
  }

  DBG_FEE_INITIATEBLOCKSWITCH_EXIT();
}

/*--------------------------[Fee_ChkFreeSpaceForCopy]------------------------*/

STATIC FUNC(boolean, FEE_CODE) Fee_ChkFreeSpaceForCopy
(
  void
)
{
  Fls_AddressType SpaceRequired;
  boolean RetVal = FALSE;

  const Fls_AddressType SpaceAvailable =
    (Fee_Gv.FeeInfoAddress + (Fls_AddressType)FEE_BLOCKINFO_ALIGNED_SIZE) -
      Fee_Gv.FeeDataAddress;

  DBG_FEE_CHKFREESPACEFORCOPY_ENTRY();

  /* Set space required as block size aligned to virtual page size and block info
   * size aligned to virtual page size */
  SpaceRequired =
    (Fls_AddressType)Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockAlignedSize +
    (Fls_AddressType)FEE_BLOCKINFO_ALIGNED_SIZE;

  /* If available space is enough to hold required space */
  if (SpaceAvailable >= SpaceRequired)
  {
    RetVal = TRUE;
  }


  DBG_FEE_CHKFREESPACEFORCOPY_EXIT(RetVal);
  return RetVal;
}

/*--------------------------[Fee_SwitchSectionFailure]-----------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SwitchSectionFailure
(
  void
)
{

  DBG_FEE_SWITCHSECTIONFAILURE_ENTRY();
  if((TRUE == Fee_ChkFreeSpace()) && (FEE_WRITE_JOB == Fee_Gv.FeeJob))
  {
    Fee_Gv.FeeFlashFailure = TRUE;
   /* Proceed with the immediate write operation */
    FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_INFO);
  }
  else
  {
    /* Go to idle; If a job is pending but there is not enough space,
     * dequeue the job and call nvm error notification */
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SWITCHSECTIONFAILURE_EXIT();
}

/*--------------------------[Fee_InitiateJob]--------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_InitiateJob
(
  void
)
{
  /* Fee_Gv.FeeJob is updated with the corresponding job
   * from the API, when the job is accepted */

  DBG_FEE_INITIATEJOB_ENTRY();
  switch(Fee_Gv.FeeJob)
  {
    case FEE_WRITE_JOB:
      Fee_ProcessWriteOrInvalidateJob();
      break;

    case FEE_READ_JOB:
      Fee_InitiateRead();
      break;

    case FEE_INVALIDATE_JOB:
      Fee_ProcessInvalidateJob();
      break;

    case FEE_NO_JOB:
      FEE_TRIGGER_TRANS(FEE_IDLE);
      Fee_Gv.FeeStatus = MEMIF_IDLE;
      break;

      /* CHECK: NOPARSE */
    default:
      FEE_UNREACHABLE_CODE_ASSERT(FEE_INTERNAL_API_ID);
      break; /* Caution: this 'break' is required by MISRA-C:2004 */
      /* CHECK: PARSE */
  }

  DBG_FEE_INITIATEJOB_EXIT();
}

/*--------------------------[Fee_ChkFreeSpace]-------------------------------*/

STATIC FUNC(boolean, FEE_CODE)Fee_ChkFreeSpace
(
  void
)
{
  Fls_LengthType SpaceAvailable;
  Fls_LengthType SpaceRequired;
  boolean IsEnoughSpace = FALSE;

  DBG_FEE_CHKFREESPACE_ENTRY();

  /* Calculate available space */
  SpaceAvailable =
    (Fls_LengthType)((Fee_Gv.FeeInfoAddress + FEE_BLOCKINFO_ALIGNED_SIZE) -
      Fee_Gv.FeeDataAddress);

  /* If switch operation going on */
  if (TRUE == Fee_Gv.FeeCopyPending)
  {
    /* Set space required as space required for all blocks + largest block size */
    SpaceRequired = Fee_CalculateSpaceforSS();
  }
  else
  {
    if(FEE_WRITE_JOB == Fee_Gv.FeeJob)
    {
      /* Set space required as block size aligned to virtual page size and block info
           size aligned to virtual page size */
      SpaceRequired =
        (Fls_LengthType)Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockAlignedSize +
        (Fls_LengthType)FEE_BLOCKINFO_ALIGNED_SIZE;
    }
    else if(FEE_INVALIDATE_JOB == Fee_Gv.FeeJob)
    {
      /* Set space required as the block info size aligned to virtual page size */
      SpaceRequired = (Fls_LengthType)FEE_BLOCKINFO_ALIGNED_SIZE;
    }
    else
    {
      /* Set space required as 0, as no job is pending */
      SpaceRequired = 0U;
    }
  }
  /* If available space is enough to hold required space */
  if (SpaceAvailable >= SpaceRequired)
  {
    IsEnoughSpace = TRUE;
  }


  DBG_FEE_CHKFREESPACE_EXIT(IsEnoughSpace);
  return IsEnoughSpace;
}

/*--------------------------[Fee_SearchConfigTable]--------------------------*/

STATIC FUNC(uint16, FEE_CODE) Fee_SearchConfigTable
(
  const uint16 BlockNumber
)
{
  uint16_least i;

  DBG_FEE_SEARCHCONFIGTABLE_ENTRY(BlockNumber);

  /* Check whether the passed block number is equal to the block number
   * stored in the configuration block being checked */
  for (i = 0U; i < FEE_NO_OF_CONFIGURED_BLOCKS; ++i)
  {
    if (Fee_BlockCfg[i].FeeBlockNumber == BlockNumber)
    {
      break;
    }
  }


  DBG_FEE_SEARCHCONFIGTABLE_EXIT((uint16)i,BlockNumber);
  return (uint16)i;
}

/*--------------------------[Fee_CalculateChecksum]--------------------------*/

STATIC FUNC(uint8, FEE_CODE) Fee_CalculateChecksum
(
  CONSTP2VAR(uint8, FEE_CONST, FEE_APPL_DATA) Buffer,
  const Fls_LengthType                   DataLength
)
{
  uint8 ChkSum = 0U;
  Fls_LengthType ByteIndex;

  DBG_FEE_CALCULATECHECKSUM_ENTRY(Buffer,DataLength);

  for (ByteIndex = 0U; ByteIndex < DataLength; ByteIndex++)
  {
    ChkSum += Buffer[ByteIndex];
  }

  /* Calculate final checksum from 2s complement */
  ChkSum = (uint8)(0U - ChkSum);


  DBG_FEE_CALCULATECHECKSUM_EXIT(ChkSum,Buffer,DataLength);
  return ChkSum;
}

/*--------------------------[Fee_FillBlockInfo]------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_FillBlockInfo
(
  const uint16 BlockNumber,
  const uint16 BlockSize,
  const uint8  CopyWriteStatus
)
{

  DBG_FEE_FILLBLOCKINFO_ENTRY(BlockNumber,BlockSize,CopyWriteStatus);
  Fee_Gv.FeeBlockInfoBuffer[FEE_WRITE_STATUS_INDEX] = CopyWriteStatus;
  /* Update block length */
  FEE_WRITE_WORD(Fee_Gv.FeeBlockInfoBuffer, FEE_BLOCKLENGTH_INDEX, BlockSize);
  /* Update block number */
  FEE_WRITE_WORD(Fee_Gv.FeeBlockInfoBuffer, FEE_BLOCKNUMBER_INDEX, BlockNumber);
  /* Calculate and update checksum */
  Fee_Gv.FeeBlockInfoBuffer[FEE_CHECKSUM_INDEX]
    = Fee_CalculateChecksum(Fee_Gv.FeeBlockInfoBuffer,
                            ((Fls_LengthType)FEE_BLOCKINFO_SIZE -
                             (Fls_LengthType)FEE_CHECKSUM_SIZE
                            )
                           );
  /* Pad the unused space in block info*/
  TS_MemSet (&(Fee_Gv.FeeBlockInfoBuffer[FEE_BLOCKINFO_SIZE]), FEE_PAD_BYTE,
             FEE_BLOCKINFO_ALIGNED_SIZE - FEE_BLOCKINFO_SIZE);

  DBG_FEE_FILLBLOCKINFO_EXIT(BlockNumber,BlockSize,CopyWriteStatus);
}

/*--------------------------[Fee_WriteSectionHeader]-------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_WriteSectionHeader
(
  const uint8  FieldIndex,
  const uint16 StatusValue,
  const uint8  TargetSection
)
{
  /* Calculate target address  to write */
  const Fls_AddressType TargetAddressInFlash =
    (Fls_AddressType)FEE_HEADER_ADDRESS(TargetSection) + (Fls_AddressType)FieldIndex;

  DBG_FEE_WRITESECTIONHEADER_ENTRY(FieldIndex,StatusValue,TargetSection);
  /* Fill status in fee buffer */
  FEE_WRITE_WORD(Fee_Gv.FeeBuffer, FEE_INDEX_ZERO, StatusValue);
  /* Write section status */
  if (E_OK == Fls_Write(TargetAddressInFlash,
                        Fee_Gv.FeeBuffer,
                        FEE_SECTION_STATUS_ALIGNED_SIZE
                       )
     )
  {
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }
  else
  {
    /* Mark the section as failed */
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_WRITESECTIONHEADER_EXIT(FieldIndex,StatusValue,TargetSection);
}

/*--------------------------[Fee_FlsRead]------------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_FlsRead
(
  const Fls_AddressType SourceAddress,
  const Fls_LengthType Length
)
{

  DBG_FEE_FLSREAD_ENTRY(SourceAddress,Length);
  if (E_OK == Fls_Read(SourceAddress, Fee_Gv.FeeBuffer, Length))
  {
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }
  else
  {
    /* Set the module state, status and job result as failure */
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_FLSREAD_EXIT(SourceAddress,Length);
}

/*--------------------------[Fee_StartInit2]---------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_StartInit2
(
  void
)
{

  DBG_FEE_STARTINIT2_ENTRY();
  switch (Fee_Gv.FeeSectionStatus)
  {
    /* Section 0 headers are inconsitent, section 1 is erased or in full state */
    case FEE_ACTIVEINCONSISTENT_ERASED: /* fall through */
    case FEE_ACTIVEINCONSISTENT_FULL:
      Fee_Gv.FeeActiveSection = FEE_SECTION_0;
      /* Erase section 0 of the flash */
      FEE_TRIGGER_TRANS(FEE_INIT_ERASE);
    break;

    /* Section 0 is full, section 1 active header status is inconsistent */
    case FEE_FULL_ACTIVEINCONSISTENT:
      Fee_Gv.FeeActiveSection = FEE_SECTION_1;
      /* Erase section 1 of the flash */
      FEE_TRIGGER_TRANS(FEE_INIT_ERASE);
    break;

    /* Section 0 is full, section 1 is erased */
    case FEE_FULL_ERASED:
      Fee_Gv.FeeActiveSection = FEE_SECTION_1;
      /* Initiate flash write to write active to section 1 */
      FEE_TRIGGER_TRANS(FEE_WRITE_SECTION_ACTIVE);
    break;

    /* Section 0 is erased, section 1 is erased or full */
    case FEE_ERASED_FULL: /* fall through */
    /* Both the sections are erased */
    case FEE_BOTH_SECTION_ERASED:
      Fee_Gv.FeeActiveSection = FEE_SECTION_0;
      /* Initiate flash write to write active to section 0 */
      FEE_TRIGGER_TRANS(FEE_WRITE_SECTION_ACTIVE);
    break;

    /* In all the other cases(invalid combination of headers) erase the full flash */
    default:
      Fee_Gv.FeeActiveSection = FEE_SECTION_0;
      /* Erase the full flash */
      FEE_TRIGGER_TRANS(FEE_INIT_ERASE);
    break;
  }

  DBG_FEE_STARTINIT2_EXIT();
}

/*--------------------------[Fee_SetStatesOnFailure]-------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SetStatesOnFailure
(
  void
)
{

  DBG_FEE_SETSTATESONFAILURE_ENTRY();
  Fee_Gv.FeeFlashFailure = TRUE;
  FEE_TRIGGER_TRANS(FEE_IDLE);
  Fee_Gv.FeeFlashJobResult = MEMIF_JOB_FAILED;

  /* Check whether a job was pending in FEE */
  if (MEMIF_BUSY == Fee_Gv.FeeStatus)
  {
    Fee_Gv.FeeJobResult = MEMIF_JOB_FAILED;
    Fee_Gv.FeeJob = FEE_NO_JOB;
    /* Call NVM's error notification function */
    FEE_NVM_JOB_ERROR_NOTIFICATION();
  }
  Fee_Gv.FeeStatus = MEMIF_IDLE;

  DBG_FEE_SETSTATESONFAILURE_EXIT();
}

/*--------------------------[Fee_InitiateRead]-------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_InitiateRead
(
  void
)
{

  DBG_FEE_INITIATEREAD_ENTRY();
  if (FEE_INCONSISTENT_ADDRESS == Fee_Gv.FeeCache[Fee_Gv.FeeIntBlockNumber])
  {
    Fee_Gv.FeeJobResult = MEMIF_BLOCK_INCONSISTENT;
    Fee_Gv.FeeStatus = MEMIF_IDLE;
    Fee_Gv.FeeJob = FEE_NO_JOB;

    /* Stop the job processing as the block is found to be inconsistent in cache.
     * The state machine shall return to IDLE state.
     */
    FEE_TRIGGER_TRANS(FEE_IDLE);

    FEE_NVM_JOB_ERROR_NOTIFICATION();
  }

  else if (FEE_INVALID_BLOCKADDRESS == Fee_Gv.FeeCache[Fee_Gv.FeeIntBlockNumber])
  {
    Fee_Gv.FeeJobResult = MEMIF_BLOCK_INVALID;
    Fee_Gv.FeeStatus = MEMIF_IDLE;
    Fee_Gv.FeeJob = FEE_NO_JOB;

    /* Stop the job processing as the block is found to be invalidated in cache.
     * The state machine shall return to IDLE state.
     */
    FEE_TRIGGER_TRANS(FEE_IDLE);

    FEE_NVM_JOB_ERROR_NOTIFICATION();
  }

  else
  {
    FEE_TRIGGER_TRANS(FEE_READ_BLOCKNUMBER);
  }

  DBG_FEE_INITIATEREAD_EXIT();
}

/*--------------------------[Fee_SfIdleState]--------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfIdleState
(
  void
)
{
  /* Get the flash job result */

  DBG_FEE_SFIDLESTATE_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  /* Flash failure need not be handled in idle state;
   * The flash job completed in idle state will always be a cancel job
   * in this case the job result will be already updated as job cancelled */
  if (MEMIF_JOB_PENDING != Fee_Gv.FeeFlashJobResult)
  {
    if (Fee_Gv.FeeStatus == MEMIF_BUSY)
    {
      /* Fee_Gv.FeeJob will be updated when a job request is posted
       * Initiate the requested job */
      Fee_InitiateJob();
    }
    /* No flash failure has happened yet */
    else if (FALSE == Fee_Gv.FeeFlashFailure)
    {
      /* Check whether delay is not completed */
      if (Fee_Gv.FeeDelayTimer > 1U)
      {
        Fee_Gv.FeeDelayTimer--;
      }
      else
      {
        Fee_Gv.FeeDelayTimer = 0U;
        if(TRUE == Fee_Gv.FeeStartup2Pending)
        {
          Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
          /* Do the part 2 of initilization */
          Fee_StartInit2();
        }
        else if (TRUE == Fee_Gv.FeeCopyPending)
        {
          Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
          /* As the delay expired, start the pending switch section */
          FEE_TRIGGER_TRANS(FEE_SECTION_SWITCHING);
        }
        /* Check whether the erase operation is pending */
        else if (TRUE == Fee_Gv.FeeErasePending)
        {
          Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
          /* Erase the inactive section */
          FEE_TRIGGER_TRANS(FEE_SS_ERASE_SECTION);
        }
        else
        {
          /* Do nothing */
        }
      }
    }
    else
    {
      /* Do nothing.
       * No write/invalidate job is posted after the previous flash failure;
       * So part2 of startup/copy/erase cannot be initiated from the idle state */
    }
  }

  DBG_FEE_SFIDLESTATE_EXIT();
}

/*--------------------------[Fee_SfOnEntryInitReadHeader0]-------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitReadHeader0
(
  void
)
{
  /* Call flash read to read the header 0 fields */

  DBG_FEE_SFONENTRYINITREADHEADER0_ENTRY();
  Fee_FlsRead(FEE_HEADER0_ADDRESS, FEE_HEADER_SIZE);

  DBG_FEE_SFONENTRYINITREADHEADER0_EXIT();
}

/*--------------------------[Fee_SfInitReadHeader0]--------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfInitReadHeader0
(
  void
)
{

  DBG_FEE_SFINITREADHEADER0_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* Read of section 0 headers completed successfully */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    Fee_SetSectionStatus(FEE_SECTION_0);
    FEE_TRIGGER_TRANS(FEE_INIT_READ_HEADER1);
  }

  /* previous flash job was failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFINITREADHEADER0_EXIT();
}

/*--------------------------[Fee_SfOnEntryInitreadHeader1]-------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitReadHeader1
(
  void
)
{
  /* Call flash read to read the header fields of section 1 */

  DBG_FEE_SFONENTRYINITREADHEADER1_ENTRY();
  Fee_FlsRead(FEE_HEADER1_ADDRESS, FEE_HEADER_SIZE);

  DBG_FEE_SFONENTRYINITREADHEADER1_EXIT();
}

/*--------------------------[Fee_SfInitReadHeader1]--------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfInitReadHeader1
(
  void
)
{

  DBG_FEE_SFINITREADHEADER1_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* Read of section 1 headers completed successfully */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* Set the section status according to the header fields read from flash.
     * Now the status of both section are updated in FeeSectionStatus */
    Fee_SetSectionStatus(FEE_SECTION_1);

    switch (Fee_Gv.FeeSectionStatus)
    {
      /* Section 0 is active, Section 1 is erased */
      case FEE_ACTIVE_ERASED:
        /* The cache is to be filled from the section 0 */
        Fee_Gv.FeeActiveSection = FEE_SECTION_0;
      break;

      /* Section 0 is active, Section 1 is full */
      case FEE_ACTIVE_FULL:
        /* Copying of blocks as part of section switch is to be done */
        Fee_Gv.FeeCopyPending = TRUE;
        Fee_Gv.FeeDelayTimer = FEE_DELAY;
        /* The cache is to be filled from the section 1 first and then section 0
         * FeeActiveSection is set to section 1; after filling the cache FeeActiveSection
         * will be set as section 0 as FeeFillBoth flag is set */
        Fee_Gv.FeeActiveSection = FEE_SECTION_1;
        Fee_Gv.FeeFillBoth = TRUE;
      break;

      /* Section 0 is active, Section 1 is copied */
      case FEE_ACTIVE_COPIED:
      /* Section 0 is active ; Section 1 headers are inconsistent */
      case FEE_ACTIVE_INCONSISTENT:
      /* Section 0 is active; Active field of section 1 header is inconsistent */
      case FEE_ACTIVE_ACTIVEINCONSISTENT:
        /* Erase of section 1 as part of section switch is to be done */
        Fee_Gv.FeeErasePending = TRUE;
        Fee_Gv.FeeDelayTimer = FEE_DELAY;
        /* The cache is to be filled from the section 0 */
        Fee_Gv.FeeActiveSection = FEE_SECTION_0;
      break;

      /* Section 0 is erased, Section 1 is active */
      case FEE_ERASED_ACTIVE:
        /* The cache is to be filled from the section 1 */
        Fee_Gv.FeeActiveSection = FEE_SECTION_1;
      break;

      /* Section 0 is erased, Section 1 is full */
      case FEE_ERASED_FULL:
      /* Only active field of section 0 is incosistent, Section 1 is full */
      case FEE_ACTIVEINCONSISTENT_FULL:
        /* Repair/write of header is to be done */
        Fee_Gv.FeeStartup2Pending = TRUE;
        Fee_Gv.FeeCopyPending = TRUE;
        Fee_Gv.FeeDelayTimer = FEE_DELAY;
        /* The cache is to be filled from the section 1 */
        Fee_Gv.FeeActiveSection = FEE_SECTION_1;
      break;

      /* Section 0 is full, Section 1 is erased */
      case FEE_FULL_ERASED:
      /* Section 0 is full, Active field of Section 1 header is inconsistent */
      case FEE_FULL_ACTIVEINCONSISTENT:
        /* Repair/write of header is to be done */
        Fee_Gv.FeeStartup2Pending = TRUE;
        Fee_Gv.FeeCopyPending = TRUE;
        Fee_Gv.FeeDelayTimer = FEE_DELAY;
        /* The cache is to be filled from the section 0 */
        Fee_Gv.FeeActiveSection = FEE_SECTION_0;
      break;

      /* Section 0 is full, Section 1 is active */
      case FEE_FULL_ACTIVE:
        /* Copying of blocks as part of section switch is to be done */
        Fee_Gv.FeeCopyPending = TRUE;
        Fee_Gv.FeeDelayTimer = FEE_DELAY;
        /* The cache is to be filled from the section 0 first and then section 1
         * FeeActiveSection is set to section 0; after filling the cache FeeActiveSection
         * will be set as section 1 as FeeFillBoth flag is set */
        Fee_Gv.FeeActiveSection = FEE_SECTION_0;
        Fee_Gv.FeeFillBoth = TRUE;
      break;

      /* Section 0 is copied, Section 1 is active */
      case FEE_COPIED_ACTIVE:
      /* Section 0 headers are inconsistent; Section 1 is active */
      case FEE_INCONSISTENT_ACTIVE:
      /* Active field of section 0 header is inconsistent; Section 1 is active */
      case FEE_ACTIVEINCONSISTENT_ACTIVE:
        /* Erase of section 0 as part of section switch is to be done */
        Fee_Gv.FeeErasePending = TRUE;
        Fee_Gv.FeeDelayTimer = FEE_DELAY;
        /* The cache is to be filled from the section 1 */
        Fee_Gv.FeeActiveSection = FEE_SECTION_1;
      break;

      /* The other cases are
       * - Invalid combination of section status where full flash needs to be erased
       * - The full flash is erased, where section0 needs to be written as Active */
      default:
        Fee_Gv.FeeStartup2Pending = TRUE;
        Fee_Gv.FeeDelayTimer = FEE_DELAY;
        FEE_TRIGGER_TRANS(FEE_IDLE);
      break;
    }

    /* Check whether the cache is to be filled */
    if (Fee_NextState != FEE_IDLE)
    {
      FEE_TRIGGER_TRANS(FEE_INIT_FILL_CACHE);
    }
    else
    {
      /* First part of initilization is complete; clear startup part1 pending flag */
      Fee_Gv.FeeStartup1Pending = FALSE;
      Fee_Gv.FeeStatus = MEMIF_BUSY;
      /* Initiate a queued read/invalidate/write job */
      Fee_InitiateJob();
    }
  }

  /* previous flash job failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFINITREADHEADER1_EXIT();
}

/*--------------------------[Fee_SfOnEntryInitFillCache]---------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitFillCache
(
  void
)
{

  DBG_FEE_SFONENTRYINITFILLCACHE_ENTRY();
  Fee_Gv.FeeDataAddress = FEE_DATA_ADDRESS(Fee_Gv.FeeActiveSection);
  Fee_Gv.FeeInfoAddress = FEE_FIRST_BLOCK_INFO_ADDRESS(Fee_Gv.FeeActiveSection);

  /* Call flash read to read the block infos to buffer from the section to be
   * filled into cache */
  Fee_FlsRead(FEE_READ_INFO_ADDRESS(Fee_Gv.FeeActiveSection),
              FEE_BUFFER_SIZE
             );

  DBG_FEE_SFONENTRYINITFILLCACHE_EXIT();
}

/*--------------------------[Fee_SfInitFillCache]----------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfInitFillCache
(
  void
)
{
  boolean Completed = FALSE; /* if cache filling is completed */

  DBG_FEE_SFINITFILLCACHE_ENTRY();

  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* Fill the Cache using the block infos read from flash to buffer */
    Completed = Fee_FillCache();

    /* If block infos are yet to be read from the current section */
    if (FALSE == Completed)
    {
      /* Read the block infos again from the current section */
      Fee_FlsRead((Fee_Gv.FeeInfoAddress - FEE_BUFFER_SIZE) + FEE_BLOCKINFO_ALIGNED_SIZE,
                  FEE_BUFFER_SIZE
                 );
    }
    else
    {
      /* Check whether the cache is to filled from other section also */
      if (TRUE == Fee_Gv.FeeFillBoth)
      {
        /* no more sections to be read to fill the cache */
        Fee_Gv.FeeFillBoth = FALSE;
        Fee_Gv.FeeCheckCopyReqd = TRUE;

        /* Update the other section as active section */
        Fee_Gv.FeeActiveSection = FEE_OTHER_SECTION(Fee_Gv.FeeActiveSection);

        /* Update the data address and the block info address*/
        Fee_Gv.FeeDataAddress = FEE_DATA_ADDRESS(Fee_Gv.FeeActiveSection);
        Fee_Gv.FeeInfoAddress = FEE_FIRST_BLOCK_INFO_ADDRESS(Fee_Gv.FeeActiveSection);

        /* Initiate the read of block infos from flash */
        Fee_FlsRead(FEE_READ_INFO_ADDRESS(Fee_Gv.FeeActiveSection),
                    FEE_BUFFER_SIZE
                   );
      }
      else
      {
        /* Initiate the next operation */
        Fee_NextStateOfFillCache();
      }
    }
  }

  /* previous flash job failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFINITFILLCACHE_EXIT();
}

/*--------------------------[Fee_SfOnEntryInitErase]-------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitErase
(
  void
)
{

  DBG_FEE_SFONENTRYINITERASE_ENTRY();
  switch (Fee_Gv.FeeSectionStatus)
  {
    /* Section 0 headers are inconsitent, section 1 is erased or in full state */
    case FEE_ACTIVEINCONSISTENT_ERASED:
    case FEE_ACTIVEINCONSISTENT_FULL: /* fall through */
    /* Section 0 is full, section 1 active header status is inconsistent */
    case FEE_FULL_ACTIVEINCONSISTENT:
      /* Erase the inconsistent section */
      Fee_EraseFlash(FEE_SECTION_SIZE(Fee_Gv.FeeActiveSection));
    break;

    /* In all the other cases(invalid combination of headers) erase the full flash */
    default:
      Fee_Gv.FeeActiveSection = FEE_SECTION_0;
      Fee_EraseFlash(FEE_TOTAL_FLASH_SIZE);
    break;
  }

  DBG_FEE_SFONENTRYINITERASE_EXIT();
}

/*--------------------------[Fee_SfInitErase]--------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfInitErase
(
  void
)
{

  DBG_FEE_SFINITERASE_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* Update the section status as erased */
    switch (Fee_Gv.FeeSectionStatus)
    {
      case FEE_ACTIVEINCONSISTENT_ERASED:
      case FEE_ACTIVEINCONSISTENT_FULL:
      case FEE_FULL_ACTIVEINCONSISTENT:
        Fee_Gv.FeeSectionStatus |= FEE_SECTION_ERASED(Fee_Gv.FeeActiveSection);
      break;
      /* In all the other cases(invalid combination of headers) */
      default:
        Fee_Gv.FeeSectionStatus = FEE_BOTH_SECTION_ERASED;
      break;
    }
    FEE_TRIGGER_TRANS(FEE_WRITE_SECTION_ACTIVE);
  }

  /* previous flash job failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFINITERASE_EXIT();
}

/*--------------------------[Fee_SfOnEntryInitChkLastBlock]------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInitChkLastBlock
(
  void
)
{
  /* Read the block number of the last copied block.
   * This is done to check the consistency of the last copied block */

  DBG_FEE_SFONENTRYINITCHKLASTBLOCK_ENTRY();
  Fee_FlsRead(Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex] +
              Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockSize,
              FEE_SIZE_BLOCKNUMBER
             );

  DBG_FEE_SFONENTRYINITCHKLASTBLOCK_EXIT();
}

/*--------------------------[Fee_SfInitChkLastBlock]-------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfInitChkLastBlock
(
  void
)
{

  DBG_FEE_SFINITCHKLASTBLOCK_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* Check whether the last copied block is consistent */
    if (Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockNumber == FEE_READ_WORD(Fee_Gv.FeeBuffer, 0U)
	   )
    {
      /* This block need not be copied again, continue the switch section
       * from the next block */
      Fee_Gv.FeeCopyBlockIndex++;
    }
    else
    {
      /* Update the cache with the address in the old section */
      Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex] = Fee_Gv.FeeLastCopiedBlockAddress;
    }
    /* First part of initilization is complete; clear startup part1 pending flag */
    Fee_Gv.FeeStartup1Pending = FALSE;
    Fee_Gv.FeeStatus = MEMIF_BUSY;
    /* Initiate a queued read/write/invalidate job */
    Fee_InitiateJob();
  }

  /* previous flash job failed */
  else
  {
    Fee_SetStatesOnFailure();
  }


  DBG_FEE_SFINITCHKLASTBLOCK_EXIT();
}

/*--------------------------[Fee_SfOnEntryReadBlockNumber]-------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryReadBlockNumber
(
  void
)
{

  DBG_FEE_SFONENTRYREADBLOCKNUMBER_ENTRY();
  Fee_FlsRead(Fee_Gv.FeeCache[Fee_Gv.FeeIntBlockNumber] +
              Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockSize,
              FEE_SIZE_BLOCKNUMBER
             );
  DBG_FEE_SFONENTRYREADBLOCKNUMBER_EXIT();
}

/*--------------------------[Fee_SfReadBlockNumber]--------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfReadBlockNumber
(
  void
)
{

  DBG_FEE_SFREADBLOCKNUMBER_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* Block number is successfully read */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* Check whether the block is consistent */
    if (FEE_READ_WORD(Fee_Gv.FeeBuffer, 0U) == Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockNumber
       )
    {
      FEE_TRIGGER_TRANS(FEE_READ_BLOCKDATA);
    }

    /* Block is inconsistent in flash */
    else
    {
      Fee_Gv.FeeCache[Fee_Gv.FeeIntBlockNumber] = FEE_INCONSISTENT_ADDRESS;
      FEE_TRIGGER_TRANS(FEE_IDLE);
      Fee_Gv.FeeStatus = MEMIF_IDLE;
      Fee_Gv.FeeJob = FEE_NO_JOB;
      Fee_Gv.FeeJobResult = MEMIF_BLOCK_INCONSISTENT;

      FEE_NVM_JOB_ERROR_NOTIFICATION();
    }
  }

  /* previous flash job failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFREADBLOCKNUMBER_EXIT();
}

/*--------------------------[Fee_SfOnEntryReadBlockData]---------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryReadBlockData
(
  void
)
{
  /* !LINKSTO FEE007,1 */

  DBG_FEE_SFONENTRYREADBLOCKDATA_ENTRY();
  if(E_OK == Fls_Read((Fee_Gv.FeeCache[Fee_Gv.FeeIntBlockNumber]+Fee_Gv.FeeBlockReadOffset),
                      Fee_Gv.FeeBlockDataReadBuffPtr,
                      (Fls_LengthType)Fee_Gv.FeeLength
                     )
    )
  {
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }

  /* flash job failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFONENTRYREADBLOCKDATA_EXIT();
}

/*--------------------------[Fee_SfReadBlockData]----------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfReadBlockData(void)
{

  DBG_FEE_SFREADBLOCKDATA_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* Block data read was successful */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    FEE_TRIGGER_TRANS(FEE_IDLE);
    Fee_Gv.FeeStatus = MEMIF_IDLE;
    Fee_Gv.FeeJobResult = MEMIF_JOB_OK;
    Fee_Gv.FeeJob = FEE_NO_JOB;

    FEE_NVM_JOB_END_NOTIFICATION();
  }

  /* Block data read from flash failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFREADBLOCKDATA_EXIT();
}

/*--------------------------[Fee_SfOnEntrySSReadData]------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSReadData
(
  void
)
{

  DBG_FEE_SFONENTRYSSREADDATA_ENTRY();
  if (E_OK == Fls_Read(Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex],
                       Fee_Gv.FeeBuffer,
                       (Fls_LengthType)Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockAlignedSize
                      )
     )
  {
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }

  /* flash read failed */
  else
  {
    Fee_SwitchSectionFailure();
  }

  DBG_FEE_SFONENTRYSSREADDATA_EXIT();
}

/*--------------------------[Fee_SfSSReadData]-------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfSSReadData
(
  void
)
{

  DBG_FEE_SFSSREADDATA_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
#if (FEE_FLS_CANCEL_API == STD_ON)
    if (TRUE == Fee_Gv.FeeInterruptSwitch)
    {
      /* an immediate job has been requested:
       * - cancel the ongoing flash read
       * - initiate the write job */
      Fls_Cancel();
      FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_INFO);
    }
#endif

    /* do nothing, flash job is not completed */
  }

  /* If block data and id read finished without error */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* If an immediate write is requested, stop the switch operation and initiate the write */
    if (TRUE == Fee_Gv.FeeInterruptSwitch)
    {
      FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_INFO);
    }
    else
    {
      /* Verify block id at the end of data, read from flash to check consistency,
       * if block is inconsistent update cache with inconsistent address */
      if (Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockNumber !=
           FEE_READ_WORD(Fee_Gv.FeeBuffer, Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockSize)
         )
      {
        Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex] = (Fls_AddressType)FEE_INCONSISTENT_ADDRESS;
      }
      FEE_TRIGGER_TRANS(FEE_SS_COPY_INFO);
    }
  }

  /* flash job was a failure */
  else
  {
    Fee_SwitchSectionFailure();
  }


  DBG_FEE_SFSSREADDATA_EXIT();
}

/*--------------------------[Fee_SfOnEntrySSCopyInfo]------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSCopyInfo
(
  void
)
{

  DBG_FEE_SFONENTRYSSCOPYINFO_ENTRY();
  Fee_FillBlockInfo(Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockNumber,
                    Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockSize,
                    FEE_BLOCK_COPIED
                   );


  /* write block info to flash */
  if (E_OK == Fls_Write(Fee_Gv.FeeInfoAddress,
                        Fee_Gv.FeeBlockInfoBuffer,
                        FEE_BLOCKINFO_ALIGNED_SIZE
                       )
     )
  {
    /* If flash write is accepted, update the Info Address */
    Fee_Gv.FeeInfoAddress -= FEE_BLOCKINFO_ALIGNED_SIZE;
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }

  /* flash write was a failure */
  else
  {
    Fee_SwitchSectionFailure();
  }


  DBG_FEE_SFONENTRYSSCOPYINFO_EXIT();
}

/*--------------------------[Fee_SfSSCopyInfo]-------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfSSCopyInfo
(
  void
)
{

  DBG_FEE_SFSSCOPYINFO_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* Copy of block info finished without error */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* For inconsistent block, no need to copy the data block; so check whether the last
     * copied info was for an inconsistent block */
    if (FEE_INCONSISTENT_ADDRESS == Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex])
    {
      Fee_Gv.FeeDataAddress += Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockAlignedSize;
      /* Take next block for copy */
      Fee_Gv.FeeCopyBlockIndex++;
      /* Before copying next block, start the immediate write request if pending */
      if (TRUE == Fee_Gv.FeeInterruptSwitch)
      {
        /* Initiate write of immediate block */
        FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_INFO);
      }
      else
      {
        FEE_TRIGGER_TRANS(FEE_SECTION_SWITCHING);
      }
    }

    /* Block info was copied for a valid block; copy the block data */
    else
    {
      FEE_TRIGGER_TRANS(FEE_SS_COPY_DATA);
    }
  }

  /* Copy of block info failed */
  else
  {
    Fee_SwitchSectionFailure();
  }

  DBG_FEE_SFSSCOPYINFO_EXIT();
}

/*--------------------------[Fee_SfOnEntrySSCopyData]------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSCopyData
(
  void
)
{

  DBG_FEE_SFONENTRYSSCOPYDATA_ENTRY();
  if (E_OK == Fls_Write(Fee_Gv.FeeDataAddress,
                        Fee_Gv.FeeBuffer,
                        (Fls_LengthType)Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockAlignedSize
                       )
     )
  {
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }

  /* flash write failed */
  else
  {
    Fee_SwitchSectionFailure();
  }

  /* Update the data address */
  Fee_Gv.FeeDataAddress += Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockAlignedSize;

  DBG_FEE_SFONENTRYSSCOPYDATA_EXIT();
}

/*--------------------------[Fee_SfSSCopyData]-------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfSSCopyData
(
  void
)
{

  DBG_FEE_SFSSCOPYDATA_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* Copy of block data was successful */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* Update cache of last copied block */
    Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex] =
      Fee_Gv.FeeDataAddress - Fee_BlockCfg[Fee_Gv.FeeCopyBlockIndex].FeeBlockAlignedSize;
    /* Take next block for copy */
    Fee_Gv.FeeCopyBlockIndex++;

    /* Before copying next block, start the immediate write request if pending */
    if (TRUE == Fee_Gv.FeeInterruptSwitch)
    {
      /* Initiate write of immediate block */
      FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_INFO);
    }

    /* Continue the section switching by copying the next block */
    else
    {
      FEE_TRIGGER_TRANS(FEE_SECTION_SWITCHING);
    }
  }

  /* Copy of data block failed */
  else
  {
    Fee_SwitchSectionFailure();
  }

  DBG_FEE_SFSSCOPYDATA_EXIT();
}

/*--------------------------[Fee_SfOnEntrySSWriteCopied]---------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSWriteCopied
(
  void
)
{

  DBG_FEE_SFONENTRYSSWRITECOPIED_ENTRY();
  Fee_WriteSectionHeader(FEE_COPIEDFIELD_INDEX, FEE_SECTION_STATUS_COPIED,
                         FEE_OTHER_SECTION(Fee_Gv.FeeActiveSection)
                        );
  DBG_FEE_SFONENTRYSSWRITECOPIED_EXIT();
}

/*--------------------------[Fee_SfSSWriteCopied]----------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfSSWriteCopied
(
  void
)
{

  DBG_FEE_SFSSWRITECOPIED_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* Section status written as copied successfully */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* Switch section is completed, the inactive section shall be erased */
    Fee_Gv.FeeCopyPending = FALSE;
    Fee_Gv.FeeErasePending = TRUE;

    /* If there is free space to write the immediate block in the active section
     * FeeInterruptSwitch will be set; Process this write job before erasing the inactive
     * section */
    if (TRUE == Fee_Gv.FeeInterruptSwitch)
    {
      FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_INFO);
    }
    else
    {
      /* Erase inactive section */
      FEE_TRIGGER_TRANS(FEE_SS_ERASE_SECTION);
    }
  }

  /* Copy section status write failed */
  else
  {
    Fee_SwitchSectionFailure();
  }

  DBG_FEE_SFSSWRITECOPIED_EXIT();
}

/*--------------------------[Fee_SfOnEntrySSEraseSection]--------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntrySSEraseSection
(
  void
)
{
  const uint8 Section = FEE_OTHER_SECTION(Fee_Gv.FeeActiveSection);

  DBG_FEE_SFONENTRYSSERASESECTION_ENTRY();

  if (E_OK == Fls_Erase((Fls_AddressType)FEE_SECTION_START_ADDRESS(Section),
                        (Fls_LengthType)FEE_SECTION_SIZE(Section)
                       )
     )
  {
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }

  /* erase failed */
  else
  {
    Fee_SwitchSectionFailure();
  }

  DBG_FEE_SFONENTRYSSERASESECTION_EXIT();
}

/*--------------------------[Fee_SfSSEraseSection]---------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfSSEraseSection
(
  void
)
{

  DBG_FEE_SFSSERASESECTION_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
#if (FEE_FLS_CANCEL_API == STD_ON)
    if (TRUE == Fee_Gv.FeeInterruptSwitch)
    {
      /* an immediate write is requested:
       * - stop the ongoing erase
       * - initiate write */
      Fls_Cancel();
      FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_INFO);
    }
#endif
  }

  /* Erase completed successfully */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    /* Clear erase pending flag */
    Fee_Gv.FeeErasePending = FALSE;
    /* Initiate the job in queue; an immediate write request taken during
     * switch operation or an invalidate/write job request which triggered the
     * switch operaton during delay can be in queue */
    Fee_InitiateJob();
  }

  /* Erase failed */
  else
  {
    Fee_SwitchSectionFailure();
  }

  DBG_FEE_SFSSERASESECTION_EXIT();
}

/*--------------------------[Fee_SfSectionSwitching]-------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfSectionSwitching
(
  void
)
{
  boolean ExitFlag = FALSE;

  DBG_FEE_SFSECTIONSWITCHING_ENTRY();

  /* Get the next block that can be copied.
   *  The next block to be copied must be in inactive section and consistent
   *  and there must be enough space to copy the block. */
  while ((Fee_Gv.FeeCopyBlockIndex < FEE_NO_OF_CONFIGURED_BLOCKS) &&
         (FALSE == ExitFlag)
        )
  {
    /* If block is invalid or the block is already present in active section */
    if ((FEE_INVALID_BLOCKADDRESS == Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex]) ||
        ((Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex] >=
          FEE_SECTION_START_ADDRESS(Fee_Gv.FeeActiveSection)
         ) &&
         (Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex] <=
            FEE_SECTION_END_ADDRESS(Fee_Gv.FeeActiveSection)
         )
        )
       )
    {
      /* No need to copy current block. Look for next block which needs to be copied */
      Fee_Gv.FeeCopyBlockIndex++;
    }

    /* If block is consistent and in inactive section */
    else
    {
      /* Check whether space is available to copy current block */
      if (TRUE == Fee_ChkFreeSpaceForCopy())
      {
        /* Copy current block as space is available */
        ExitFlag = TRUE;
      }
      else
      {
        /* Can't copy current block, Mark the block as invalid */
        Fee_Gv.FeeCache[Fee_Gv.FeeCopyBlockIndex] = FEE_INVALID_BLOCKADDRESS;

        /* Look for next block which fits in available space  */
        Fee_Gv.FeeCopyBlockIndex++;
      }
    }
  }

  /* No more blocks remaining to be copied  */
  if (FEE_NO_OF_CONFIGURED_BLOCKS <= Fee_Gv.FeeCopyBlockIndex)
  {
    FEE_TRIGGER_TRANS(FEE_SS_WRITE_COPIED);
  }
  /* More blocks are yet to be copied */
  else
  {
    /* Start switch operation for the next block */
    Fee_InitiateBlockSwitch();
  }

  DBG_FEE_SFSECTIONSWITCHING_EXIT();
}

/*--------------------------[Fee_SfOnEntryWriteBlockInfo]--------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryWriteBlockInfo
(
  void
)
{

  DBG_FEE_SFONENTRYWRITEBLOCKINFO_ENTRY();
  Fee_Gv.FeeInterruptSwitch = FALSE;
  /* !LINKSTO FEE049,1, FEE153,1 */
  Fee_Gv.FeeCache[Fee_Gv.FeeIntBlockNumber] = (Fls_AddressType)FEE_INCONSISTENT_ADDRESS;

  Fee_FillBlockInfo(Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockNumber,
                    Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockSize,
                    FEE_WRITE_BLOCK
                   );

  if (E_OK == Fls_Write(
                        Fee_Gv.FeeInfoAddress ,
                        Fee_Gv.FeeBlockInfoBuffer,
                        FEE_BLOCKINFO_ALIGNED_SIZE
                       )
     )
  {
    /* Update the Block Info Address */
    Fee_Gv.FeeInfoAddress -= FEE_BLOCKINFO_ALIGNED_SIZE;
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }

  /* flash write failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFONENTRYWRITEBLOCKINFO_EXIT();
}

/*--------------------------[Fee_SfWriteBlockInfo]---------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfWriteBlockInfo
(
  void
)
{

  DBG_FEE_SFWRITEBLOCKINFO_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* If last block info write was successful */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    if (TRUE == Fee_Gv.FeeJobCancelled)
    {
      /* Update the block data address since the block info write is successful */
      Fee_Gv.FeeDataAddress += Fee_BlockCfg[Fee_Gv.FeeCancelledBlock].FeeBlockAlignedSize;
      Fee_Gv.FeeJobCancelled = FALSE;

      /* Since the module was internal busy; an immediate job request might be queued.
       * Initiate this job request else go to IDLE */
      Fee_InitiateJob();
    }
    else
    {
      FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_DATA);
    }
  }

  /* The write of block info failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFWRITEBLOCKINFO_EXIT();
}

/*--------------------------[Fee_SfOnEntryWriteBlockData]--------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryWriteBlockData
(
  void
)
{
  const uint16 BlockSize =
    Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockSize;
  const uint16 BlockAlignedSize =
    Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockAlignedSize;
  const uint16 BlockPadOffset = BlockSize + FEE_SIZE_BLOCKNUMBER;

  DBG_FEE_SFONENTRYWRITEBLOCKDATA_ENTRY();

  /* Copy application buffer data to FEE buffer */
  TS_MemCpy(Fee_Gv.FeeBuffer,Fee_Gv.FeeBlockDataWriteBuffPtr, BlockSize);

  /* Update the FEE buffer with block number after the data */
  FEE_WRITE_WORD(Fee_Gv.FeeBuffer, BlockSize,
                 Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockNumber);

  /* Pad the unused space of the last virtual page */
  /* Deviation MISRA-2 */
  TS_MemSet (&(Fee_Gv.FeeBuffer[BlockPadOffset]),
             FEE_PAD_BYTE, BlockAlignedSize - BlockPadOffset);

  if (E_OK == Fls_Write(Fee_Gv.FeeDataAddress,
                        Fee_Gv.FeeBuffer,
                        (Fls_LengthType)BlockAlignedSize
                       )
     )
  {
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }

  /* flash write failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  /* Update the block data address since the block info write is successful */
  Fee_Gv.FeeDataAddress += BlockAlignedSize;

  DBG_FEE_SFONENTRYWRITEBLOCKDATA_EXIT();
}

/*--------------------------[Fee_SfWriteBlockData]---------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfWriteBlockData
(
  void
)
{

  DBG_FEE_SFWRITEBLOCKDATA_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* If block data write was successful */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    boolean IsSpaceAvailable;
    /* !LINKSTO FEE049,1, FEE154,1 */
    Fee_Gv.FeeCache[Fee_Gv.FeeIntBlockNumber]
      = Fee_Gv.FeeDataAddress - Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockAlignedSize;
    Fee_Gv.FeeJobResult = MEMIF_JOB_OK;
    Fee_Gv.FeeStatus = MEMIF_IDLE;
    Fee_Gv.FeeJob = FEE_NO_JOB;

    FEE_NVM_JOB_END_NOTIFICATION();

    /* There can be two cases in which switch is initiated after a write job
     * Case 1: When delay is elapsed and switch operation interrupted due to immediate write
     * - copy of blocks is pending, copy of blocks is started
     * - erase is pending, erase of inactive section is started
     * Case 2: When delay is present and after the write of the block switch operation shall be
     * started as the space in section is less space than the reserved switch section space. */


    /* Get available space status */
    IsSpaceAvailable = Fee_ChkFreeSpace();
    if (SECTION_SWITCHING_INPROGRESS() &&
        (FALSE == Fee_Gv.FeeFlashFailure)
       )
    {
      Fee_Gv.FeeDelayTimer = 0U;
      Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
      if (TRUE == Fee_Gv.FeeCopyPending)
      {
        FEE_TRIGGER_TRANS(FEE_SECTION_SWITCHING);
      }
      else
      {
        FEE_TRIGGER_TRANS(FEE_SS_ERASE_SECTION);
      }
    }
    else if (((Fee_Gv.FeeDelayTimer > 0U) && (TRUE != IsSpaceAvailable))
                 && (FALSE == Fee_Gv.FeeFlashFailure)
               )
    {
       Fee_Gv.FeeDelayTimer = 0U;
       Fee_Gv.FeeStatus = MEMIF_BUSY_INTERNAL;
       FEE_TRIGGER_TRANS(FEE_SECTION_SWITCHING);
    }
    else
    {
      FEE_TRIGGER_TRANS(FEE_IDLE);
    }
  }

  /* data write failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFWRITEBLOCKDATA_EXIT();
}

/*--------------------------[Fee_SfOnEntryWriteSectionActive]----------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryWriteSectionActive
(
  void
)
{
  uint8 Section;

  DBG_FEE_SFONENTRYWRITESECTIONACTIVE_ENTRY();

  if(TRUE == Fee_Gv.FeeStartup2Pending)
  {
    Section = Fee_Gv.FeeActiveSection;
  }
  else
  {
    Section = FEE_OTHER_SECTION(Fee_Gv.FeeActiveSection);
  }

  /* Write the inactive section as active */
  Fee_WriteSectionHeader(FEE_ACTIVEFIELD_INDEX,
                         FEE_SECTION_STATUS_ACTIVE,
                         Section
                        );

  DBG_FEE_SFONENTRYWRITESECTIONACTIVE_EXIT();
}

/*--------------------------[Fee_SfWriteSectionActive]-----------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfWriteSectionActive
(
  void
)
{

  DBG_FEE_SFWRITESECTIONACTIVE_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* Check whether the write section active status is successful */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    Fee_Gv.FeeSectionFull = FALSE;

    /* Check if the header write was due to Startup */
    if (TRUE == Fee_Gv.FeeStartup2Pending)
    {
      /* The Active section is already updated in this case.
         Reset the startup2 pending flag */
      Fee_Gv.FeeStartup2Pending = FALSE;
    }

    /* header write is not during startup */
    else
    {
      /* Initialize the copy block index to start the switch operation */
      Fee_Gv.FeeCopyBlockIndex = 0U;
      /* Switch operation shall be started after the delay */
      Fee_Gv.FeeCopyPending = TRUE;
      Fee_Gv.FeeDelayTimer = FEE_DELAY;
      /* Change the Active Section */
      Fee_Gv.FeeActiveSection = FEE_OTHER_SECTION(Fee_Gv.FeeActiveSection);
    }

    /* Load next data write address  */
    Fee_Gv.FeeDataAddress = FEE_DATA_ADDRESS(Fee_Gv.FeeActiveSection);
    /* Load next info write address */
    Fee_Gv.FeeInfoAddress = FEE_FIRST_BLOCK_INFO_ADDRESS(Fee_Gv.FeeActiveSection);

    if (TRUE == Fee_Gv.FeeJobCancelled)
    {
      Fee_Gv.FeeJobCancelled = FALSE;
      FEE_TRIGGER_TRANS(FEE_IDLE);
      if (MEMIF_BUSY != Fee_Gv.FeeStatus)
      {
        Fee_Gv.FeeStatus = MEMIF_IDLE;
      }
    }

    /* Initiate the queued job, if it was not cancelled */
    else
    {
      if(FEE_WRITE_JOB == Fee_Gv.FeeJob)
      {
        FEE_TRIGGER_TRANS(FEE_WRITE_BLOCK_INFO);
      }
      else if(FEE_INVALIDATE_JOB == Fee_Gv.FeeJob)
      {
        FEE_TRIGGER_TRANS(FEE_INVALIDATE);
      }
      else
      {
        Fee_Gv.FeeStatus = MEMIF_IDLE;
        FEE_TRIGGER_TRANS(FEE_IDLE);
      }
    }
  }

  /* previous flash job was failed */
  else
  {
    if (TRUE == Fee_Gv.FeeStartup2Pending)
    {
      /* Marking the section as activeinconsistent to indicate flash failure */
      Fee_Gv.FeeSectionStatus |= FEE_SECTION_ACTIVEINCONSISTENT(Fee_Gv.FeeActiveSection);
    }

    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFWRITESECTIONACTIVE_EXIT();
}

/*--------------------------[Fee_SfOnEntryWriteSectionFull]------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryWriteSectionFull
(
  void
)
{

  DBG_FEE_SFONENTRYWRITESECTIONFULL_ENTRY();
  Fee_WriteSectionHeader(FEE_FULLFIELD_INDEX,
                         FEE_SECTION_STATUS_FULL,
                         Fee_Gv.FeeActiveSection
                        );
  DBG_FEE_SFONENTRYWRITESECTIONFULL_EXIT();
}

/*--------------------------[Fee_SfWriteSectionFull]-------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfWriteSectionFull
(
  void
)
{

  DBG_FEE_SFWRITESECTIONFULL_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  /* Check whether the write section full status is successful */
  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    Fee_Gv.FeeSectionFull = TRUE;

    /* Job which made the section full was cancelled */
    if (TRUE == Fee_Gv.FeeJobCancelled)
    {
      Fee_Gv.FeeJobCancelled = FALSE;
      FEE_TRIGGER_TRANS(FEE_IDLE);
      Fee_Gv.FeeStatus = MEMIF_IDLE;
    }

    /* If the job is not cancelled, write the active status and make the inactive section active */
    else
    {
      FEE_TRIGGER_TRANS(FEE_WRITE_SECTION_ACTIVE);
    }
  }

  /* previous flash job failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFWRITESECTIONFULL_EXIT();
}

/*--------------------------[Fee_SfOnEntryInvalidate]------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfOnEntryInvalidate
(
  void
)
{

  DBG_FEE_SFONENTRYINVALIDATE_ENTRY();
  Fee_FillBlockInfo(Fee_BlockCfg[Fee_Gv.FeeIntBlockNumber].FeeBlockNumber,
                    FEE_INVALID_SIZE,
                    FEE_WRITE_BLOCK
                   );

  /* Call flash write to initiate the write of block info*/
  if (E_OK == Fls_Write(Fee_Gv.FeeInfoAddress ,
                        Fee_Gv.FeeBlockInfoBuffer,
                        FEE_BLOCKINFO_ALIGNED_SIZE
                       )
     )
  {
    Fee_Gv.FeeInfoAddress = Fee_Gv.FeeInfoAddress - FEE_BLOCKINFO_ALIGNED_SIZE;
    Fee_Gv.FeeFlashJobResult = MEMIF_JOB_PENDING;
  }

  /* flash write failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFONENTRYINVALIDATE_EXIT();
}

/*--------------------------[Fee_SfInvalidate]-------------------------------*/

STATIC FUNC(void, FEE_CODE) Fee_SfInvalidate
(
  void
)
{

  DBG_FEE_SFINVALIDATE_ENTRY();
  FEE_GET_FLASH_JOB_RESULT();

  if (MEMIF_JOB_PENDING == Fee_Gv.FeeFlashJobResult)
  {
    /* Do nothing; flash job is not complete */
  }

  else if (MEMIF_JOB_OK == Fee_Gv.FeeFlashJobResult)
  {
    Fee_Gv.FeeCache[Fee_Gv.FeeIntBlockNumber] = FEE_INVALID_BLOCKADDRESS;
    if (TRUE == Fee_Gv.FeeJobCancelled)
    {
      Fee_Gv.FeeJobCancelled = FALSE;
      /* Since the module was internal busy; an immediate job request might be queued
       * Initiate this job request else go to IDLE */
      Fee_InitiateJob();
    }
    else
    {
      Fee_Gv.FeeJobResult = MEMIF_JOB_OK;
      Fee_Gv.FeeStatus = MEMIF_IDLE;
      FEE_TRIGGER_TRANS(FEE_IDLE);
      /* Unqueue the current job */
      Fee_Gv.FeeJob = FEE_NO_JOB;
      /* Call NVM's notification function */
      FEE_NVM_JOB_END_NOTIFICATION();
    }
  }

  /* flash job failed */
  else
  {
    Fee_SetStatesOnFailure();
  }

  DBG_FEE_SFINVALIDATE_EXIT();
}

#define FEE_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]=================================*/

