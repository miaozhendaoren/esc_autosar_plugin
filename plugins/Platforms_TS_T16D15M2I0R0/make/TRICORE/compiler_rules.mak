# \file
#
# \brief AUTOSAR Platforms
#
# This file contains the implementation of the AUTOSAR
# module Platforms.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# this file just references the compiler_rules.mak makfiles
# in the TOOLCHAIN directory

ifeq ($(Platforms_CORE_PATH), )
include $(SSC_ROOT)\Platforms_$(Platforms_VARIANT)\make\$(CPU)\$(TOOLCHAIN)\compiler$(RULES_FILE_SUFFIX)
else
include $(Platforms_CORE_PATH)\make\$(CPU)\$(TOOLCHAIN)\compiler$(RULES_FILE_SUFFIX)
endif


#####################################################################
# debug : starts BUILD_ALL and in case of success starts the TRACE32
# from Lauterbach to debug the application.
.PHONY : debug

debug: $(TRACE32_PATH) BUILD_ALL
	$(DB), $(DEBUG_OPT_FILE)

start_debug: $(TRACE32_PATH)
	$(DB), $(DEBUG_OPT_FILE)

