# \file
#
# \brief AUTOSAR Platforms
#
# This file contains the implementation of the AUTOSAR
# module Platforms.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# The Application Makefile (Makefile.mak) contains the variable
# COMPILER_MODE. This variable shall have one of the following
# values: GLOBAL_OPTION_FILE, LOCAL_OPTION_MODE and NO_OPTION_MODE.
# If GLOBAL_OPTiON_MODE is set, one global parameter file will be
# created for all source files. If LOCAL_OPTION_FILE is set, an
# parameter file will be created for each source file and if
# NO_OPTION_FILE is defined, no option file will be created.

#################################################################
# Define location of file containing information of a run of
# make
#
ifneq ($(COMPILER_MODE), NO_OPTION_FILE)
MAKE_INC_FILE := $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX)
endif

#################################################################
# The next rule generates an option file with the include paths
# and the compiler flags for each source file.
ifeq ($(COMPILER_MODE), LOCAL_OPTION_FILE)

#################################################################
# Compile all C files *.c -> *.obj
#
$(CC_TO_OBJ_BUILD_LIST) :

	$(shell echo $(GET_CC_BUILD_OPTIONS) > $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ))
	$(foreach i, $(GET_CC_INCLUDE_FILES), $(shell echo $i >> $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ )))
	$(shell echo $(subst \\,/,$(shell type $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ))) > $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ) )
	$(shell echo $(subst \,/,$(shell type $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ))) > $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ) )
ifeq ($(CREATE_PREPROCESSOR_FILE),YES)
	$(CPRE) -E @$(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ) $(getSourceFile) -o $(subst .$(OBJ_FILE_SUFFIX),.$(PRE_FILE_SUFFIX),$@) 
endif	

	$(CC) -c @$(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ) $(getSourceFile) -o $@

else

#################################################################
# The next rule generates a global option file with the include
# pathes and the compiler flags. This file will be used for all
# source files.
ifeq ($(COMPILER_MODE), GLOBAL_OPTION_FILE)


#################################################################
# Create file containing compiler switches
#
$(MAKE_INC_FILE):
	$(PRE_BUILD_BLOCK)
	$(SEPARATOR) Creating parameter file (.$(INC_FILE_SUFFIX)) ...
	$(shell echo $(GET_CC_BUILD_OPTIONS) > $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) )
	$(foreach i,$(GET_CC_INCLUDE_FILES), $(shell echo $i >> $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) ))
	$(shell echo $(subst \\,/,$(shell type $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX))) > $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) )
	$(shell echo $(subst \,/,$(shell type $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX))) > $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) )
	$(POST_BUILD_BLOCK)

$(CC_TO_OBJ_BUILD_LIST) :
	$(PRE_BUILD_BLOCK)
ifeq ($(CREATE_PREPROCESSOR_FILE),YES)
	$(CPRE) -E @$(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) $(getSourceFile) -o $(subst .$(OBJ_FILE_SUFFIX),.$(PRE_FILE_SUFFIX),$@) 
endif	
	$(CC) -c @$(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) $(getSourceFile) -o $@
	$(POST_BUILD_BLOCK)

else

#################################################################
# The last rule to compile C source files does not use an option
# file. If the windows comand line reports an error, the
# GLOBAL_OPTION_FILE or the LOCAL_OPTION_FILE mode shall be used.

$(CC_TO_OBJ_BUILD_LIST) :
ifeq ($(CREATE_PREPROCESSOR_FILE),YES)
	$(CPRE) -E $(GET_CC_BUILD_OPTIONS) $(GET_CC_INCLUDE_FILES) $(getSourceFile) -o $(subst .$(OBJ_FILE_SUFFIX),.$(PRE_FILE_SUFFIX),$@) 
endif
	$(CC) -c $(GET_CC_BUILD_OPTIONS) $(GET_CC_INCLUDE_FILES) $(getSourceFile) -o $@

# endif LOCAL_OPTION_FILE
endif
endif


#################################################################
# Compile all CPP files *.cpp -> *.obj
#
$(if $(strip $(CPP_TO_OBJ_BUILD_LIST)),$(error The plugin Platforms for tricore_gnu does not support cpp/cc/c++ files. -$(CPP_TO_OBJ_BUILD_LIST)- ))
$(CPP_TO_OBJ_BUILD_LIST) :
	$(PRE_BUILD_BLOCK)
	$(SEPARATOR)
	$(SEPARATOR) Compiling cpp-o
	$(SEPARATOR)
	@echo $@ - $(getSourceFile)
	$(POST_BUILD_BLOCK)


#################################################################
# Compile all assembler files *.asm/*.s -> *.obj
#
$(ASM_TO_OBJ_BUILD_LIST) :
	$(CPRE) -E $(GET_ASS_BUILD_OPTIONS) $(GET_ASM_INCLUDE_FILES) -o $(OBJ_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$@))).$(ASM_FILE_SUFFIX_2) $(getSourceFile)
	$(ASM)  $(ASM_OPT) -o $@ $(OBJ_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$@))).$(ASM_FILE_SUFFIX_2)



################################################################
# helper to generate a command that archives a few objects into a lib
# note: the 3rd line (empty) is really needed to serve as a cmd-separator.
#
# args: 1 - cmds to perform
define SC_CMD_AR_HELPER
	$(1)

endef

################################################################
# recursive helper to perform a task on chunks of a list
# note: do not place a space in front of argument 5 of the recursive call,
#       ( $(4),<here>$(wordlist... )). This would result in infinite recursion.
#
# args: 1 - variable-name that contains commands to perform
#       2 - additional arguments to $(1)
#       3 - max. number of elements in a chunk
#       4 - $3+1 (sorry, cannot perform arith in make)
#       5 - list
SC_CMD_LISTPERFORM = $(if $(5),\
	$(call $(1),$(2) $(wordlist 1, $(3), $(5))) \
	$(call SC_CMD_LISTPERFORM, $(1), $(2), $(3), \
		$(4),$(wordlist $(4), $(words $(5)), $(5))) )

################################################################
# Archive object files *.obj -> *.lib
#
$(OBJ_TO_LIB_BUILD_LIST) :
	@$(RM) $@
	$(call SC_CMD_LISTPERFORM,SC_CMD_AR_HELPER,\
		$(LIB) $(AR_OPT) $@ $(GET_LIB_BUILD_OPTIONS),5,6, $+)

################################################################
# helper to echo data to some file
#
# args: 1 - text to echo
#       2 - destination
define SC_CMD_ECHO_HELPER
	@echo $(1) >> $(2)

endef

#################################################################
# generate a linker description file with all the objects to
# link
#
OBJS_TO_LINK := $(filter-out %.a, $(GET_FILES_TO_LINK))
LIBS_TO_LINK := $(filter     %.a, $(GET_FILES_TO_LINK))
$(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript:
	@echo INPUT( > $@
	$(foreach otl, $(OBJS_TO_LINK), $(call SC_CMD_ECHO_HELPER,$(otl),$@))
	@echo ) >> $@
	@echo GROUP( >> $@
	$(foreach ltl, $(LIBS_TO_LINK), $(call SC_CMD_ECHO_HELPER,$(ltl),$@))
	@echo ) >> $@


#################################################################
# Link all objects and libraries *.obj + *.lib -> .out
#
ifeq ($(BUILD_MODE),LIB)
$(BIN_OUTPUT_PATH)\$(PROJECT).elf : $(GET_FILES_TO_LINK) $(GET_LOC_FILE) $(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript
else
$(BIN_OUTPUT_PATH)\$(PROJECT).elf : $(GET_FILES_TO_LINK) $(GET_LOC_FILE) $(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript
	$(PRE_BUILD_BLOCK)
	$(SEPARATOR) create binary
	$(LINK) $(LINK_OPT) -o $@ -T $(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript -T $(GET_LOC_FILE)
	$(POST_BUILD_BLOCK)

#################################################################
# Remove preprocessor files (*.pre )
#
ifeq ($(CREATE_PREPROCESSOR_FILE),YES)
MAKE_CLEAN_LIB_RULES += clean_lib_pre
LIB_PRE_FILES = $(patsubst %.$(OBJ_FILE_SUFFIX),%.$(PRE_FILE_SUFFIX),$(CC_TO_OBJ_BUILD_LIST))

clean_lib_pre:
	$(SEPARATOR) [clean] 	lib preprocessor files
	$(foreach file,$(LIB_PRE_FILES),$(call SC_CMD_AR_HELPER, $(RM) $(file)))
endif
endif

#################################################################
# Delete all compiler specific files (*.obj, *.lib, *.abs...)
#
compiler_clean :
	$(SEPARATOR)
	$(START_BLOCK)
	$(SEPARATOR) DELETING COMPILER FILES
	$(SEPARATOR)
	$(RM) $(BIN_OUTPUT_PATH)\$(PROJECT).elf
	$(RM) $(BIN_OUTPUT_PATH)\$(PROJECT).$(HEX_FILE_SUFFIX)
	$(RM) $(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript
	$(RM) "$(OBJ_OUTPUT_PATH)\*.$(OBJ_FILE_SUFFIX)"
	$(RM) "$(OBJ_OUTPUT_PATH)\*.src"
	$(RM) "$(OBJ_OUTPUT_PATH)\*.$(INC_FILE_SUFFIX)"
	$(RM) "$(LIB_OUTPUT_PATH)\*.$(LIB_FILE_SUFFIX)"
	$(RM) "$(TMP_OUTPUT_PATH)\*.*"
	$(SEPARATOR)
	$(END_BLOCK)

