# \file
#
# \brief AUTOSAR Platforms
#
# This file contains the implementation of the AUTOSAR
# module Platforms.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

##################################################################
# VARIABLES
##################################################################

MISRA_CHECK_OPTS += -Y 32bit

##################################################################
# CHECKS
##################################################################

##################################################################
# CPU dependent options
##################################################################

##################################################################
# TARGETS
##################################################################

###################################################################
# RULES
###################################################################

