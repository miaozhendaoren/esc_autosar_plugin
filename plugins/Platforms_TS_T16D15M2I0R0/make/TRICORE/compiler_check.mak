# \file
#
# \brief AUTOSAR Platforms
#
# This file contains the implementation of the AUTOSAR
# module Platforms.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# this file just references the compiler_check.mak makfiles
# in the TOOLCHAIN directory

ifeq ($(Platforms_CORE_PATH), )
include $(SSC_ROOT)\Platforms_$(Platforms_VARIANT)\make\$(CPU)\$(TOOLCHAIN)\compiler$(CHECK_FILE_SUFFIX)
else
include $(Platforms_CORE_PATH)\make\$(CPU)\$(TOOLCHAIN)\compiler$(CHECK_FILE_SUFFIX)
endif