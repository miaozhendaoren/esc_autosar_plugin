<?xml version='1.0'?>

<!--
 \file

 \brief AUTOSAR Com

 This file contains the implementation of the AUTOSAR
 module Com.

 \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany

 Copyright 2005 - 2013 Elektrobit Automotive GmbH
 All rights exclusively reserved for Elektrobit Automotive GmbH,
 unless expressly agreed to otherwise.
-->

<datamodel version="3.0" 
           xmlns="http://www.tresos.de/_projects/DataModel2/08/root.xsd" 
           xmlns:a="http://www.tresos.de/_projects/DataModel2/08/attribute.xsd" 
           xmlns:v="http://www.tresos.de/_projects/DataModel2/06/schema.xsd" 
           xmlns:d="http://www.tresos.de/_projects/DataModel2/06/data.xsd">

  <d:ctr type="AUTOSAR" factory="autosar"  
         xmlns:ad="http://www.tresos.de/_projects/DataModel2/08/admindata.xsd" 
         xmlns:icc="http://www.tresos.de/_projects/DataModel2/08/implconfigclass.xsd">
    <d:lst type="TOP-LEVEL-PACKAGES">

      <d:ctr name="TS_TxDxM6I3R0" type="AR-PACKAGE">
        <d:lst type="ELEMENTS">

          <d:chc name="ComRecConfigurationSmall" type="AR-ELEMENT" value="MODULE-CONFIGURATION">
            <d:ctr type="MODULE-CONFIGURATION">
              <a:a name="DEF" value="ASPath:/TS_TxDxM6I3R0/Com"/>
              <d:lst name="ComConfig" type="MAP"/>
              <d:ctr name="ComGeneral" type="IDENTIFIABLE">
                <d:var name="ComConfigurationUseDet" type="BOOLEAN" value="false">
                  <a:a name="ENABLE" value="true"/>
                </d:var>
                <d:var name="ComVersionInfoApi" type="BOOLEAN" value="false"/>
                <d:ctr name="VendorSpecific" type="IDENTIFIABLE">
                  <d:var name="ComDataMemSize" type="INTEGER" >
                    <a:a name="ENABLE" value="false"/>
                  </d:var>
                  <d:var name="ComRamSizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComCbkTxTOutArraySizeMax" type="ENUMERATION" 
                         value="INDEX_NONE"/>
                  <d:var name="ComCbkRxTOutArraySizeMax" type="ENUMERATION" 
                         value="INDEX_NONE"/>
                  <d:var name="ComCbkRxAckPtrArraySizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComCbkTxAckPtrArraySizeMax" type="ENUMERATION" 
                         value="INDEX_NONE"/>
                  <d:var name="ComCbkTxErrPtrArraySizeMax" type="ENUMERATION" 
                         value="INDEX_NONE"/>
                  <d:var name="ComCallOutFuncPtrArraySizeMax" 
                         type="ENUMERATION" value="INDEX_NONE"/>
                  <d:var name="ComTriggerTxCallOutEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComSignalArraySizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComSignalInitValueEnable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComRxDataTimeoutAction" type="ENUMERATION" 
                         value="RX_DATA_TIMEOUT_ACTION_NONE"/>
                  <d:var name="ComEndiannessConversion" type="ENUMERATION" 
                         value="ENDIAN_ENABLE"/>
                  <d:var name="ComRxTimeoutFactorStorage" type="ENUMERATION" 
                         value="STORAGE_REFERENCE"/>
                  <d:var name="ComRxTimeoutFactorArraySMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComRxTimeoutFactorSize" type="ENUMERATION" 
                         value="SIZE_0_BIT"/>
                  <d:var name="ComRxFirstTimeoutFactorSize" type="ENUMERATION" 
                         value="SIZE_0_BIT"/>
                  <d:var name="ComTxTimeoutFactorStorage" type="ENUMERATION" 
                         value="STORAGE_REFERENCE"/>
                  <d:var name="ComTxTimeoutFactorArraySizeMax" 
                         type="ENUMERATION" value="INDEX_UINT8"/>
                  <d:var name="ComTxTimeoutFactorSize" type="ENUMERATION" 
                         value="SIZE_0_BIT"/>
                  <d:var name="ComTxModeRepetitionPeriodFactorS" 
                         type="ENUMERATION" value="SIZE_8_BIT"/>
                  <d:var name="ComTxModeTimeOffsetFactorSize" 
                         type="ENUMERATION" value="SIZE_8_BIT"/>
                  <d:var name="ComTxModeTimePeriodFactorSize" 
                         type="ENUMERATION" value="SIZE_8_BIT"/>
                  <d:var name="ComTxIpduMDTFactorArraySizeMax" 
                         type="ENUMERATION" value="INDEX_UINT8"/>
                  <d:var name="ComTxIpduMDTFactorSize" type="ENUMERATION" 
                         value="SIZE_0_BIT"/>
                  <d:var name="ComTxIpduMDTFactorStorage" type="ENUMERATION" 
                         value="STORAGE_REFERENCE"/>
                  <d:var name="ComTxModeArraySizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComIpduArraySizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComSignalGroupArraySizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComGroupSignalArraySizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComSignalByteAlign" type="BOOLEAN" value="false"/>
                  <d:var name="ComIpduLengthMax" type="ENUMERATION" 
                         value="SIZE_8_BYTES"/>
                  <d:var name="ComUpdateBitRxConfig" type="ENUMERATION" 
                         value="UPDATE_BIT_ABSENT_FOR_ALL"/>
                  <d:var name="ComUpdateBitTxConfig" type="ENUMERATION" 
                         value="UPDATE_BIT_ABSENT_FOR_ALL"/>
                  <d:var name="ComTmsEnable" type="BOOLEAN" value="false"/>
                  <d:var name="ComFilterReceiverEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComFilterArraySizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComSignalTypeBooleanEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComSignalTypeUint8Enable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComSignalTypeSint8Enable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComSignalTypeUint16Enable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComSignalTypeSint16Enable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComSignalTypeUint32Enable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComSignalTypeSint32Enable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComSignalTypeUint8NEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComFilterOneEveryNPeriodOffSMax" 
                         type="ENUMERATION" value="SIZE_0_BIT"/>
                  <d:var name="ComFilterOneEveryNOccuranceMax" 
                         type="ENUMERATION" value="SIZE_0_BIT"/>
                  <d:var name="ComTxModeDirectEnable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComTxModeNTimesEnable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComTxModePeriodicEnable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComTxModeMixedDirectEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComTxModeMixedNTimesEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComTxSigConfDeferredEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComTxSigConfImmediateEnable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComRxSigConfDeferredEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComRxSigConfImmediateEnable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComSignalGwEnable" type="BOOLEAN" value="false"/>
                  <d:var name="ComGwSourceArraySizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComGwDestinationArraySizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComCheckValueSizeEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComConstCfgAddressEnable" type="BOOLEAN" 
                         value="false"/>
                  <d:var name="ComConstCfgAddress" type="INTEGER" value="0"/>
                  <d:var name="ComRelocatableCfgEnable" type="BOOLEAN" 
                         value="true"/>
                  <d:var name="ComSignalBufferRefSizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="Com_TxF_MaskNewDiffersMaskOld_En" 
                         type="BOOLEAN" value="true"/>
                  <d:var name="ComTMSperPduMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                  <d:var name="ComIpduGroupMaskRefSizeMax" type="ENUMERATION" 
                         value="INDEX_UINT8"/>
                </d:ctr>
              </d:ctr>
            </d:ctr>
          </d:chc>
        </d:lst>
      </d:ctr>
    </d:lst>
  </d:ctr>

</datamodel>
