/**
 * \file
 *
 * \brief AUTOSAR FiM
 *
 * This file contains the implementation of the AUTOSAR
 * module FiM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

[!AUTOSPACING!]
[!//
/*==================[inclusions]============================================*/
/* !LINKSTO dsn.FiM.IncludeStr,1 */

#include <Std_Types.h>         /* AUTOSAR standard types */

#include <Dem.h>               /* Dem event IDs */

#include <FiM.h>               /* Module public API */
#include <FiM_Int.h>           /* Module internal interface */

/*==================[macros]================================================*/

/* bit masks for the inhibition mask configuration part of the event link
 * configuration */

#if (defined FIM_LAST_FAILED_CFG_MASK)
#error FIM_LAST_FAILED_CFG_MASK already defined
#endif
/** \brief FiM last failed configuration mask */
#define FIM_LAST_FAILED_CFG_MASK       (0U << 14U)

#if (defined FIM_NOT_TESTED_CFG_MASK)
#error FIM_NOT_TESTED_CFG_MASK already defined
#endif
/** \brief FiM not tested configuration mask */
#define FIM_NOT_TESTED_CFG_MASK        (1U << 14U)

#if (defined FIM_TESTED_CFG_MASK)
#error FIM_TESTED_CFG_MASK already defined
#endif
/** \brief FiM tested configuration mask */
#define FIM_TESTED_CFG_MASK            (2U << 14U)

#if (defined FIM_TESTED_AND_FAILED_CFG_MASK)
#error FIM_TESTED_AND_FAILED_CFG_MASK already defined
#endif
/** \brief FiM tested and failed configuration mask */
#define FIM_TESTED_AND_FAILED_CFG_MASK (3U << 14U)

/*------------------[Event configuration sizes]-----------------------------*/

[!INDENT "0"!]
  [!IF "FiMGeneral/FiMEventUpdateTriggeredByDem = 'false'"!]
    /* number of associated events (single and events as part of event summaries)
     [!WS!]* for each FID */
    [!LOOP "node:order(FiMConfigSet/*/FiMFID/*, 'FiMFunctionId')"!]
      [!/* calculate array size */!][!//
      [!/* number of single associated Dem events of FID */!][!//
      [!VAR "size" = "count(../../FiMInhibitionConfiguration/*[(name(as:ref(FiMInhFunctionIdRef)) = name(node:current())) and (FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceDemRef')])"!]
      [!/* loop over summarized events linked to FID */!][!//
      [!LOOP "../../FiMInhibitionConfiguration/*[(name(as:ref(FiMInhFunctionIdRef)) = name(node:current())) and not(FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceDemRef')]"!]
        [!/* add number of elements of summarized event */!][!//
        [!VAR "size" = "$size + count(../../FiMEventSummary/*[FiMOutputSumEventRef = node:current()/FiMInhEventId/FiMInhRefChoice/FiMInhSumRef/*[1]])"!]
      [!ENDLOOP!]

      #if (defined FIM_EVENT_CFG_[!"name(.)"!]_SIZE)
      #error FIM_EVENT_CFG_[!"name(.)"!]_SIZE already defined
      #endif
      /** \brief Number of associated events for FiM [!"name(.)"!] */
      #define FIM_EVENT_CFG_[!"name(.)"!]_SIZE [!"num:i($size)"!]U
    [!ENDLOOP!]
  [!ELSE!][!//
    /* number of associated FIDs for each event (single and event as part
     [!WS!]* of event summaries) */
    [!/* loop over all Dem event parameter to search associated FIDs */!][!//
    [!LOOP "node:order(as:modconf('Dem')[1]/DemConfigSet/*/DemEventParameter/*, 'DemEventId')"!]
      [!VAR "FIDCount"="0"!]
      [!VAR "eventName"="name(.)"!]
      [!LOOP "as:modconf('FiM')[1]/FiMConfigSet/*/FiMInhibitionConfiguration/*"!]
        [!/* single FID associated to Dem event */!][!//
        [!IF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceDemRef' and name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhEventRef)) = $eventName"!]
          [!VAR "FIDCount" = "$FIDCount + 1"!]
        [!ELSEIF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceSumRef'"!][!//
          [!VAR "eventSumName" = "name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhSumRef/*[1]))"!]
          [!VAR "FIDCount" = "$FIDCount + count(../../FiMEventSummary/*[name(as:ref(FiMInputSumEventRef/*[1])) = $eventName and name(as:ref(FiMOutputSumEventRef)) = $eventSumName])"!]
        [!ENDIF!]
      [!ENDLOOP!]
      [!IF "num:i($FIDCount) > 0"!]

        #if (defined FIM_FID_CFG_[!"$eventName"!]_SIZE)
        #error FIM_FID_CFG_[!"$eventName"!]_SIZE already defined
        #endif
        /** \brief Number of associated events for FiM [!"name(.)"!] */
        #define FIM_FID_CFG_[!"$eventName"!]_SIZE [!"num:i($FIDCount)"!]U
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDIF!][!/* endif of FiMEventUpdateTriggeredByDem */!][!//
[!ENDINDENT!]

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[internal constants]====================================*/

#define FIM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>
[!//
[!INDENT "0"!]
  [!IF "FiMGeneral/FiMEventUpdateTriggeredByDem = 'false'"!]
    [!LOOP "node:order(FiMConfigSet/*/FiMFID/*, 'FiMFunctionId')"!]

      /** \brief Array of calibrated inhibition mask and Dem events for the FID with
       [!WS!]** symbolic name "[!"name(.)"!]" */
      STATIC CONST(FiM_EventCfgType, FIM_CONST)
        [!WS "2"!]FiM_EventCfg[!"name(.)"!][FIM_EVENT_CFG_[!"name(.)"!]_SIZE] =
      {
        [!/* loop over all inhibition configurations referring to this FID */!][!//
        [!LOOP "../../FiMInhibitionConfiguration/*[name(as:ref(FiMInhFunctionIdRef)) = name(node:current())]"!]
          [!VAR "InhInhibitionMask" = "FiMInhInhibitionMask"!]
          [!IF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceDemRef'"!]
            [!WS "2"!]/* single Dem event link */
            [!WS "2"!][!"$InhInhibitionMask"!]_CFG_MASK | DemConf_DemEventParameter_[!"name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhEventRef))"!],
          [!ELSE!][!//
            [!WS "2"!]/* Dem event links of the event summary [!"name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhSumRef/*[1]))"!] */
            [!/* loop over all Dem events of the event summary referring to this inhibition config */!][!//
            [!LOOP "../../FiMEventSummary/*[FiMOutputSumEventRef = node:current()/FiMInhEventId/FiMInhRefChoice/FiMInhSumRef/*[1]]"!]
              [!WS "2"!][!"$InhInhibitionMask"!]_CFG_MASK | DemConf_DemEventParameter_[!"name(as:ref(FiMInputSumEventRef/*[1]))"!],
            [!ENDLOOP!]
          [!ENDIF!]
        [!ENDLOOP!]
      };
    [!ENDLOOP!]
  [!ELSE!][!//
    [!VAR "eventName" = "''"!]
    [!VAR "eventSumName" = "''"!]
    [!VAR "EventID" = "0"!]
    [!/* loop over all Dem event parameter to search associated FIDs */!][!//
    [!LOOP "node:order(as:modconf('Dem')[1]/DemConfigSet/*/DemEventParameter/*, 'DemEventId')"!]
      [!VAR "EventID" = "DemEventId"!]
      [!VAR "FIDCount" = "0"!]
      [!VAR "eventName" = "name(.)"!]
      [!LOOP "as:modconf('FiM')[1]/FiMConfigSet/*/FiMInhibitionConfiguration/*"!]
        [!/* single FID associated to Dem event */!][!//
        [!IF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceDemRef' and name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhEventRef)) = $eventName"!]
          [!VAR "FIDCount" = "$FIDCount + 1"!]
        [!ELSEIF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceSumRef'"!][!//
          [!VAR "eventSumName" = "name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhSumRef/*[1]))"!]
          [!VAR "FIDCount" = "$FIDCount + count(../../FiMEventSummary/*[name(as:ref(FiMInputSumEventRef/*[1])) = $eventName and name(as:ref(FiMOutputSumEventRef)) = $eventSumName])"!]
        [!ENDIF!]
      [!ENDLOOP!]
      [!IF "num:i($FIDCount) > 0"!]

        /** \brief Array of calibrated inhibition mask and FID for the Dem event with
         [!WS!]** symbolic name "[!"$eventName"!]" */
        STATIC CONST(FiM_FidCfgType, FIM_CONST)
          [!WS "2"!]FiM_FidCfg[!"$eventName"!][FIM_FID_CFG_[!"$eventName"!]_SIZE] =
        {
          [!LOOP "as:modconf('FiM')[1]/FiMConfigSet/*/FiMInhibitionConfiguration/*"!]
            [!VAR "InhInhibitionMask" = "FiMInhInhibitionMask"!]
            [!VAR "FIDName" = "name(as:ref(FiMInhFunctionIdRef))"!]
            [!IF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceDemRef' and name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhEventRef)) = $eventName"!]
              [!WS "2"!]/* FID-link to Dem Event */
              [!WS "2"!][!"$InhInhibitionMask"!]_CFG_MASK | [!"$FIDName"!],
            [!ELSEIF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceSumRef'"!][!//
              [!VAR "eventSumName" = "name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhSumRef/*[1]))"!]
              [!LOOP "../../FiMEventSummary/*[name(as:ref(FiMInputSumEventRef/*[1])) = $eventName and name(as:ref(FiMOutputSumEventRef)) = $eventSumName]"!]
                [!WS "2"!]/* FID-link via event summary [!"$eventSumName"!] */
                [!WS "2"!][!"$InhInhibitionMask"!]_CFG_MASK | [!"$FIDName"!],
              [!ENDLOOP!]
            [!ENDIF!]
          [!ENDLOOP!]
        };
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDIF!][!/* endif of FiMEventUpdateTriggeredByDem */!][!//
[!ENDINDENT!]

/*==================[external constants]====================================*/

#define FIM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

CONST(FiM_ConfigType, FIM_CONST) [!"name(FiMConfigSet/*[1])"!] = 0U;

[!INDENT "0"!]
  [!IF "FiMGeneral/FiMEventUpdateTriggeredByDem = 'false'"!]
    CONST(FiM_FidConfigType, FIM_CONST) FiM_FidConfig[FIM_FID_NUM] =
    {
      [!INDENT "2"!]
        /* dummy entry for FID 0 (invalid) */
        {
          [!WS "2"!]NULL_PTR,
          [!WS "2"!]0U
        },
        [!LOOP "node:order(FiMConfigSet/*/FiMFID/*, 'FiMFunctionId')"!]
          /* FID: [!"name(.)"!] */
          {
            [!WS "2"!]FiM_EventCfg[!"name(.)"!],
            [!WS "2"!]FIM_EVENT_CFG_[!"name(.)"!]_SIZE
          },
        [!ENDLOOP!]
      [!ENDINDENT!]
    };
  [!ELSE!][!//
    CONST(FiM_EvtConfigType, FIM_CONST) FiM_EvtConfig[FIM_EVENT_NUM] =
    {
      [!/* loop over all Dem event parameter to search associated FIDs */!][!//
      [!LOOP "node:order(as:modconf('Dem')[1]/DemConfigSet/*/DemEventParameter/*, 'DemEventId')"!]
        [!VAR "EventID" = "DemEventId"!]
        [!VAR "FIDCount" = "0"!]
        [!VAR "eventName" = "name(.)"!]
        [!LOOP "as:modconf('FiM')[1]/FiMConfigSet/*/FiMInhibitionConfiguration/*"!]
          [!/* single FID associated to Dem event */!][!//
          [!IF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceDemRef' and name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhEventRef)) = $eventName"!]
            [!VAR "FIDCount" = "$FIDCount + 1"!]
          [!ELSEIF "FiMInhEventId/FiMInhRefChoice = 'FiMInhChoiceSumRef'"!][!//
            [!VAR "eventSumName" = "name(as:ref(FiMInhEventId/FiMInhRefChoice/FiMInhSumRef/*[1]))"!]
            [!VAR "FIDCount" = "$FIDCount + count(../../FiMEventSummary/*[name(as:ref(FiMInputSumEventRef/*[1])) = $eventName and name(as:ref(FiMOutputSumEventRef)) = $eventSumName])"!]
          [!ENDIF!]
        [!ENDLOOP!]
        [!IF "num:i($FIDCount) > 0"!]
          [!INDENT "2"!]
            {
              [!INDENT "4"!]
                /* Event: [!"$eventName"!] */
                DemConf_DemEventParameter_[!"$eventName"!],
                FiM_FidCfg[!"$eventName"!],
                FIM_FID_CFG_[!"$eventName"!]_SIZE
              [!ENDINDENT!]
            },
          [!ENDINDENT!]
        [!ENDIF!]
      [!ENDLOOP!]
    };
  [!ENDIF!][!/* endif of FiMEventUpdateTriggeredByDem */!][!//
[!ENDINDENT!]

#define FIM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
