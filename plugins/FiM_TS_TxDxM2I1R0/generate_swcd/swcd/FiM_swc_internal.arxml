[!AUTOSPACING!]
[!//
<?xml version="1.0" encoding="UTF-8"?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3_STRICT_COMPACT.xsd">
[!/* do not generate this file if RTE is not used */!][!//
[!SKIPFILE "FiMGeneral/FiMRteUsage = 'false'"!][!//
  <AR-PACKAGES>
    <AR-PACKAGE>
      <SHORT-NAME>AUTOSAR_FiM</SHORT-NAME>
      <AR-PACKAGES>
        <AR-PACKAGE>
          <SHORT-NAME>DataConstrs</SHORT-NAME>
          <ELEMENTS>

            <!-- description of internal types -->

            <!-- !LINKSTO FiM.RteTypes_Implicit1,1 -->
            <DATA-CONSTR>
              <SHORT-NAME>CONSTR_FunctionIdType</SHORT-NAME>
              <DATA-CONSTR-RULES>
                <DATA-CONSTR-RULE>
                  <INTERNAL-CONSTRS>
                    <LOWER-LIMIT INTERVAL-TYPE="CLOSED">1</LOWER-LIMIT>
                    <UPPER-LIMIT INTERVAL-TYPE="CLOSED">[!"num:i(count(FiMConfigSet/*/FiMFID/*))"!]</UPPER-LIMIT>
                  </INTERNAL-CONSTRS>
                </DATA-CONSTR-RULE>
              </DATA-CONSTR-RULES>
            </DATA-CONSTR>
          </ELEMENTS>
        </AR-PACKAGE>

        <AR-PACKAGE>
          <SHORT-NAME>ImplementationDataTypes</SHORT-NAME>
          <ELEMENTS>
            <IMPLEMENTATION-DATA-TYPE>
              <SHORT-NAME>FunctionIdType</SHORT-NAME>
              <CATEGORY>VALUE</CATEGORY>
              <SW-DATA-DEF-PROPS>
                <SW-DATA-DEF-PROPS-VARIANTS>
                  <SW-DATA-DEF-PROPS-CONDITIONAL>
[!IF "count(FiMConfigSet/*/FiMFID/*) < 256"!][!//
                    <BASE-TYPE-REF DEST="SW-BASE-TYPE">/AUTOSAR_Platform/BaseTypes/uint8</BASE-TYPE-REF>
[!ELSE!][!//
                    <BASE-TYPE-REF DEST="SW-BASE-TYPE">/AUTOSAR_Platform/BaseTypes/uint16</BASE-TYPE-REF>
[!ENDIF!][!//
                    <DATA-CONSTR-REF DEST="DATA-CONSTR">/AUTOSAR_FiM/DataConstrs/CONSTR_FunctionIdType</DATA-CONSTR-REF>
                  </SW-DATA-DEF-PROPS-CONDITIONAL>
                </SW-DATA-DEF-PROPS-VARIANTS>
              </SW-DATA-DEF-PROPS>
            </IMPLEMENTATION-DATA-TYPE>
          </ELEMENTS>
        </AR-PACKAGE>

        <AR-PACKAGE>
          <SHORT-NAME>SwComponentTypes</SHORT-NAME>
          <ELEMENTS>
            <!-- description of service component -->

            <SERVICE-SW-COMPONENT-TYPE>
              <SHORT-NAME>FiM</SHORT-NAME>
              <PORTS>
[!LOOP "FiMConfigSet/*/FiMFID/*"!]
                <P-PORT-PROTOTYPE>
                  <SHORT-NAME>FI_[!"name(.)"!]</SHORT-NAME>
                  <PROVIDED-COM-SPECS>
                    <SERVER-COM-SPEC>
                      <OPERATION-REF DEST="CLIENT-SERVER-OPERATION">/AUTOSAR_FiM/PortInterfaces/FunctionInhibition/GetFunctionPermission</OPERATION-REF>
                      <QUEUE-LENGTH>1</QUEUE-LENGTH>
                    </SERVER-COM-SPEC>
                  </PROVIDED-COM-SPECS>
                  <PROVIDED-INTERFACE-TREF DEST="CLIENT-SERVER-INTERFACE">/AUTOSAR_FiM/PortInterfaces/FunctionInhibition</PROVIDED-INTERFACE-TREF>
                </P-PORT-PROTOTYPE>
[!ENDLOOP!]
              </PORTS>
              <INTERNAL-BEHAVIORS>
                <SWC-INTERNAL-BEHAVIOR>
                  <SHORT-NAME>FiMIntBeh</SHORT-NAME>
                  <EVENTS>
                   <!-- Events for interface FunctionInhibition -->
[!LOOP "FiMConfigSet/*/FiMFID/*"!]
                   <OPERATION-INVOKED-EVENT>
                     <SHORT-NAME>EV_GetFunctionPermission_[!"name(.)"!]</SHORT-NAME>
                     <START-ON-EVENT-REF DEST="RUNNABLE-ENTITY">/AUTOSAR_FiM/SwComponentTypes/FiM/FiMIntBeh/RE_GetFunctionPermission</START-ON-EVENT-REF>
                     <OPERATION-IREF>
                       <CONTEXT-P-PORT-REF DEST="P-PORT-PROTOTYPE">/AUTOSAR_FiM/SwComponentTypes/FiM/FI_[!"name(.)"!]</CONTEXT-P-PORT-REF>
                       <TARGET-PROVIDED-OPERATION-REF DEST="CLIENT-SERVER-OPERATION">/AUTOSAR_FiM/PortInterfaces/FunctionInhibition/GetFunctionPermission</TARGET-PROVIDED-OPERATION-REF>
                     </OPERATION-IREF>
                   </OPERATION-INVOKED-EVENT>
[!ENDLOOP!]
                  </EVENTS>

                  <HANDLE-TERMINATION-AND-RESTART>NO-SUPPORT</HANDLE-TERMINATION-AND-RESTART>

                  <PORT-API-OPTIONS>
[!LOOP "FiMConfigSet/*/FiMFID/*"!]
                    <!-- *** Port-API options concerning FunctionID '[!"name(.)"!]' *** -->
                    <PORT-API-OPTION>
                      <ENABLE-TAKE-ADDRESS>false</ENABLE-TAKE-ADDRESS>
                      <INDIRECT-API>false</INDIRECT-API>
                      <PORT-ARG-VALUES>
                        <PORT-DEFINED-ARGUMENT-VALUE>
                          <VALUE>
                            <NUMERICAL-VALUE-SPECIFICATION>
                              <VALUE>[!"FiMFunctionId"!]</VALUE>
                            </NUMERICAL-VALUE-SPECIFICATION>
                          </VALUE>
                          <VALUE-TYPE-TREF DEST="IMPLEMENTATION-DATA-TYPE">/AUTOSAR_FiM/ImplementationDataTypes/FunctionIdType</VALUE-TYPE-TREF>
                        </PORT-DEFINED-ARGUMENT-VALUE>
                      </PORT-ARG-VALUES>
                      <PORT-REF DEST="P-PORT-PROTOTYPE">/AUTOSAR_FiM/SwComponentTypes/FiM/FI_[!"name(.)"!]</PORT-REF>
                    </PORT-API-OPTION>
[!ENDLOOP!]
                  </PORT-API-OPTIONS>
                  <RUNNABLES>
                    <!-- Runnables for interface FunctionInhibition -->
                    <RUNNABLE-ENTITY>
                      <SHORT-NAME>RE_GetFunctionPermission</SHORT-NAME>
                      <MINIMUM-START-INTERVAL>0</MINIMUM-START-INTERVAL>
                      <CAN-BE-INVOKED-CONCURRENTLY>true</CAN-BE-INVOKED-CONCURRENTLY>
                      <SYMBOL>FiM_GetFunctionPermission</SYMBOL>
                    </RUNNABLE-ENTITY>
                  </RUNNABLES>
                  <SUPPORTS-MULTIPLE-INSTANTIATION>false</SUPPORTS-MULTIPLE-INSTANTIATION>
                </SWC-INTERNAL-BEHAVIOR>
              </INTERNAL-BEHAVIORS>
            </SERVICE-SW-COMPONENT-TYPE>
          </ELEMENTS>
        </AR-PACKAGE>
      </AR-PACKAGES>
    </AR-PACKAGE>
    <AR-PACKAGE>
      <SHORT-NAME>EB_FiM_TxDxM2I1R0</SHORT-NAME>
      <AR-PACKAGES>
        <AR-PACKAGE>
          <SHORT-NAME>SwcImplementations</SHORT-NAME>
          <ELEMENTS>
            <SWC-IMPLEMENTATION>
              <SHORT-NAME>SwcImplementation_0</SHORT-NAME>
              <CODE-DESCRIPTORS>
                <CODE>
                  <SHORT-NAME>FiMCode</SHORT-NAME>
                  <ARTIFACT-DESCRIPTORS>
                    <AUTOSAR-ENGINEERING-OBJECT>
                      <SHORT-LABEL>EngObject</SHORT-LABEL>
                      <CATEGORY>SWSRC</CATEGORY>
                    </AUTOSAR-ENGINEERING-OBJECT>
                  </ARTIFACT-DESCRIPTORS>
                </CODE>
              </CODE-DESCRIPTORS>
              <PROGRAMMING-LANGUAGE>C</PROGRAMMING-LANGUAGE>
              <RESOURCE-CONSUMPTION>
                <SHORT-NAME>Resources</SHORT-NAME>
              </RESOURCE-CONSUMPTION>
              <SW-VERSION>2.1.7</SW-VERSION>
              <VENDOR-ID>1</VENDOR-ID>
              <BEHAVIOR-REF DEST="SWC-INTERNAL-BEHAVIOR">/AUTOSAR_FiM/SwComponentTypes/FiM/FiMIntBeh</BEHAVIOR-REF>
            </SWC-IMPLEMENTATION>
          </ELEMENTS>
        </AR-PACKAGE>
      </AR-PACKAGES>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
