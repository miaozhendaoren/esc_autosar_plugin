# \file
#
# \brief AUTOSAR Resource
#
# This file contains the implementation of the AUTOSAR
# module Resource.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

##################################################################
# CONFIGURATION
#

Resource_CORE_PATH           := $(SSC_ROOT)\Resource_$(Resource_VARIANT)
Resource_OUTPUT_PATH         := $(AUTOSAR_BASE_OUTPUT_PATH)

#################################################################

CC_INCLUDE_PATH      +=  $(Resource_CORE_PATH)\include
