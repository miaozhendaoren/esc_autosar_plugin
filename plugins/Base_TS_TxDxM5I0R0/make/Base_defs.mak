# \file
#
# \brief AUTOSAR Base
#
# This file contains the implementation of the AUTOSAR
# module Base.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

Base_CORE_PATH         := $(SSC_ROOT)\Base_$(Base_VARIANT)

Base_OUTPUT_PATH       := $(AUTOSAR_BASE_OUTPUT_PATH)

#################################################################
# REGISTRY
SSC_PLUGINS            += Base
CC_INCLUDE_PATH        += \
   $(Base_CORE_PATH)\include \
   $(Base_OUTPUT_PATH)\include
