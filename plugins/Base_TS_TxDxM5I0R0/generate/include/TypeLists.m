[!/**
 * \file
 *
 * \brief AUTOSAR Base
 *
 * This file contains the implementation of the AUTOSAR
 * module Base.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!AUTOSPACING!][!//
[!/* List of all AUTOASR types provided as property */!][!//
[!VAR "AutosarTypeList"="'boolean sint8 sint16 sint32 uint8 uint16 uint32 sint8_least sint16_least sint32_least uint8_least uint16_least uint32_least float32 float64'"!][!//
[!/* List of all C90 types provided as property */!][!//
[!VAR "CTypeList"="'char short int long float double'"!][!//
[!/* List of all complex types provided as property */!][!//
[!VAR "ComplexTypeList"="'struct array'"!][!//
[!/* List of all complex types provided as property */!][!//
[!VAR "DerivedTypeList"="'TS_MaxAlignedType'"!][!//