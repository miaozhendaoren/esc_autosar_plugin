/**
 * \file
 *
 * \brief AUTOSAR Base
 *
 * This file contains the implementation of the AUTOSAR
 * module Base.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined COMSTACK_CFG_H)
#define COMSTACK_CFG_H


[!IF "node:exists(as:modconf('EcuC')[1]/EcucPduCollection/PduIdTypeEnum) and
          (as:modconf('EcuC')[1]/EcucPduCollection/PduIdTypeEnum != 'UINT16')"!]
[!ERROR  "For PduIdTypeEnum only data type UINT16 is supported"!]
[!ENDIF!]

[!IF "node:exists(as:modconf('EcuC')[1]/EcucPduCollection/PduLengthTypeEnum) and
          (as:modconf('EcuC')[1]/EcucPduCollection/PduLengthTypeEnum != 'UINT16')"!]
[!ERROR  "For PduLengthTypeEnum only data type UINT16 is supported"!]
[!ENDIF!]

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/
[!/*
[!// Get PduIdType from EcuC module and convert the enum into the
[!// character representing it
[!// If the EcuC node does not exist UINT16 is chosen as default value
[!// Parameter Unit: Use 'Bit' to express size in bits, any other string for bytes
*/!][!MACRO "GetEcucPduIdBitWidth","Unit"!][!/*
*/!][!IF "node:exists(as:modconf('EcuC')[1]/EcucPduCollection/PduIdTypeEnum) and
          (as:modconf('EcuC')[1]/EcucPduCollection/PduIdTypeEnum = 'UINT8')"!][!/*
*/!][!VAR "Size" = "num:i('8')"!][!/*
*/!][!ELSE!][!/*
*/!][!VAR "Size" = "'16'"!][!/*
*/!][!ENDIF!][!/*
*/!][!IF "$Unit = 'Bit'"!][!/*
*/!][!"num:i($Size)"!][!/*
*/!][!ELSE!][!/*
*/!][!"num:i($Size div 8)"!][!/*
*/!][!ENDIF!][!/*
*/!][!ENDMACRO!][!/*

[!// Get PduLengthType from EcuC module and convert the enum into the
[!// character representing it
[!// If the EcuC node does not exist UINT16 is chosen as default value
[!// Parameter Unit: Use 'Bit' to express size in bits, any other string for bytes
*/!][!MACRO "GetEcucPduLengthBitWidth","Unit"!][!/*
*/!][!IF "node:exists(as:modconf('EcuC')[1]/EcucPduCollection/PduLengthTypeEnum) and
          (as:modconf('EcuC')[1]/EcucPduCollection/PduLengthTypeEnum = 'UINT32')"!][!/*
*/!][!VAR "Size" = "num:i('32')"!][!/*
*/!][!ELSEIF "node:exists(as:modconf('EcuC')[1]/EcucPduCollection/PduLengthTypeEnum) and
              (as:modconf('EcuC')[1]/EcucPduCollection/PduLengthTypeEnum = 'UINT8')"!][!/*
*/!][!VAR "Size" = "num:i('8')"!][!/*
*/!][!ELSE!][!/*
*/!][!VAR "Size" = "num:i('16')"!][!/*
*/!][!ENDIF!][!/*
*/!][!IF "$Unit = 'Bit'"!][!/*
*/!][!"num:i($Size)"!][!/*
*/!][!ELSE!][!/*
*/!][!"num:i($Size div 8)"!][!/*
*/!][!ENDIF!][!/*
*/!][!ENDMACRO!][!/*

*/!]
/*------------------[Size of Autosar Comstack Types]-------------------------*/

#if (defined TS_SIZE_PduIdType)
#error TS_SIZE_PduIdType already defined
#endif
/** \brief Size of AUTOSAR standard type PduIdType */
#define TS_SIZE_PduIdType [!CALL "GetEcucPduIdBitWidth","Unit"="'Byte'"!]U

#if (defined TS_SIZE_PduLengthType)
#error TS_SIZE_PduLengthType already defined
#endif
/** \brief Size of AUTOSAR standard type PduLengthType */
#define TS_SIZE_PduLengthType [!CALL "GetEcucPduLengthBitWidth","Unit"="'Byte'"!]U

/*==================[type definitions]======================================*/

/* this type can be configured:
 * - uint8 or uint16
 * - range: 0 .. PduIdmax
 */
/** \brief Type for a unique identifier for a PDU */
typedef uint[!CALL "GetEcucPduIdBitWidth","Unit"="'Bit'"!] PduIdType;

/* this type can be configured:
 * - uint8/uint16/uint32
 * - range: 0 .. PduLengthmax
 */
/** \brief Type for lengths information of a PDU */
typedef uint[!CALL "GetEcucPduLengthBitWidth","Unit"="'Bit'"!] PduLengthType;

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( COMSTACK_CFG_H ) */
/*==================[end of file]============================================*/
