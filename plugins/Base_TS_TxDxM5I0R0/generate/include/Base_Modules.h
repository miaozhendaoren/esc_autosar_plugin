/**
 * \file
 *
 * \brief AUTOSAR Base
 *
 * This file contains the implementation of the AUTOSAR
 * module Base.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/* !LINKSTO Base.ModuleInfo.HeaderFile,1 */

#if (!defined BASE_MODULES_H)
#define BASE_MODULES_H

[!AUTOSPACING!]
[!// Get all enabled modules
[!VAR "Base_EnabledModules" = "text:join(node:foreach(text:split($enabledModules),'mod','substring-before($mod,&apos;_&apos; )'))"!]

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

[!/* Iterate over all module configurations, sorted by extracted module name */!][!//
[!LOOP "node:order(/AUTOSAR/TOP-LEVEL-PACKAGES/*/ELEMENTS/*[@type='MODULE-CONFIGURATION'], 'text:split(as:path(as:dtos(.)), &apos;/&apos;)[position()=last()]')"!]
[!/* tresos Studio does not support to directly get the name of the module
   * from the schema node, so we have to extract it from the last element of the
   * schema path */!]
[!VAR "moduleName" = "text:split(as:path(as:dtos(.)), '/')[position()=last()]"!]
[!/* check if there is an implementationConfigVariant element. - If not fall back to
   * 'VariantPreCompile'. Note that there are modules that don't have this element
   * (e.g., MemMap) */!]
[!IF "node:exists(./IMPLEMENTATION_CONFIG_VARIANT)"!]
[!VAR "implementationConfigVariant" = "./IMPLEMENTATION_CONFIG_VARIANT"!]
[!ELSE!]
[!VAR "implementationConfigVariant" = "'VariantPreCompile'"!]
[!ENDIF!]
/** \brief Enable status of the module [!"$moduleName"!] ([!"$implementationConfigVariant"!]) */
#define BASE_[!"text:toupper($moduleName)"!]_ENABLED  [!//
[!IF "node:containsValue(text:split($Base_EnabledModules),$moduleName)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!/* select the multiple configuration container of the module if there is any */!][!//
  [!SELECT "*[node:dtos(.)/@type = 'MULTIPLE-CONFIGURATION-CONTAINER']"!]
    [!IF "count(*)>0"!]
#ifndef BASE_[!"text:toupper($moduleName)"!]_CONFIG_PTR
[!/* special handling for non post-build ComM */!][!//
      [!IF "($moduleName = 'ComM') and ($implementationConfigVariant != 'VariantPostBuild')"!][!//
/** \brief empty for [!"$moduleName"!] -> special handling for [!"$moduleName"!] ([!"$implementationConfigVariant"!]) */
#define BASE_[!"text:toupper($moduleName)"!]_CONFIG_PTR
      [!ELSE!][!//
[!/* check if the module shall be initialized by the PbcfgM */!][!//
        [!IF "count(as:modconf('PbcfgM')[1]/PbcfgMBswModules/*[(node:refvalid(PbcfgMBswModuleRef)) and (name(as:ref(PbcfgMBswModuleRef)) = $moduleName)])>0"!][!//
/** \brief NULL_PTR for [!"$moduleName"!] -> initialized by PbcfgM */
#define BASE_[!"text:toupper($moduleName)"!]_CONFIG_PTR NULL_PTR
        [!ELSE!][!//
/** \brief Address of the first multiple configuration container for
 * [!"$moduleName"!] */
#define BASE_[!"text:toupper($moduleName)"!]_CONFIG_PTR (&[!"name(*[1])"!])
        [!ENDIF!][!//
      [!ENDIF!][!//
#endif
    [!ELSE!]
[!/* The multiplicity restriction of multiple config containers for
   * most (?) schema definitions should prevent the code generation
   * in the first place */!][!//
#error No multiple configuration container found for module [!"$moduleName"!].\
 Add a container to [!"node:path(.)"!].
    [!ENDIF!]
  [!ENDSELECT!]
[!/* special handling for PbcfgM regarding name of config (since this one does not have a multiple config container) */!][!//
  [!IF "$moduleName = 'PbcfgM'"!]
#ifndef BASE_PBCFGM_CONFIG_PTR
/** \brief Address of the post build configuration for
 * PbcfgM */
#define BASE_PBCFGM_CONFIG_PTR (&(PbcfgM_Config.PbcfgM_RootConfig))
#endif
  [!ENDIF!]
[!ENDLOOP!]

/*==================[type definitions]======================================*/

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( BASE_MODULES_H ) */
/*==================[end of file]============================================*/
