#if (!defined STD_TYPES_H)
#define STD_TYPES_H
/* STD014 */

/**
 * \file
 *
 * \brief AUTOSAR Base
 *
 * This file contains the implementation of the AUTOSAR
 * module Base.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * MISRA-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 6.3 (required)
 *   'typedefs' that indicate size and signedness should be used in place of
 *   the basic types.
 *
 *   Reason:
 *   the Autosar specification SWS_StdTypes requires the definition
 *   of the StatusType for OSEK compatibility.
 */

/*==================[inclusions]=============================================*/

/* STD001 */
#include <Platform_Types.h> /* Autosar platform specific type declarations */
#include <Compiler.h>       /* Autosar compiler specific declarations */

/*==================[macros]=================================================*/

/* MANDO specific */

#ifndef ON             /* avoid clash with existing definitions */
#define ON       1U
#endif

#ifndef OFF            /* avoid clash with existing definitions */
#define OFF      0U
#endif


/* STD007 */
#if (defined STD_HIGH)
#error STD_HIGH already defined
#endif

/** \brief physical state 5V or 3.3V  */
#define STD_HIGH 1U

#if (defined STD_LOW)
#error STD_LOW already defined
#endif

/** \brief physical state 0V */
#define STD_LOW 0U

/* STD013 */
#if (defined STD_ACTIVE)
#error STD_ACTIVE already defined
#endif

/** \brief Autosar logical state 'active' */
#define STD_ACTIVE 1U

#if (defined STD_IDLE)
#error STD_IDLE already defined
#endif

/** \brief Autosar logical state 'idle' */
#define STD_IDLE 0U

/* STD010 */
#if (defined STD_ON)
#error STD_ON already defined
#endif

/** \brief Autosar definition for 'on' */
#define STD_ON 1U

#if (defined STD_OFF)
#error STD_OFF already defined
#endif

/** \brief Autosar definition for 'off' */
#define STD_OFF 0U

#if (!defined STATUSTYPEDEFINED)
#define STATUSTYPEDEFINED
/* Deviation MISRA-1 */
typedef unsigned char StatusType; /* OSEK compliance */
#endif

/* STD006 */
#if (!defined E_OK)
#define E_OK 0U
#endif

#if (!defined E_NOT_OK)
#define E_NOT_OK 1U
#endif

/*------------------[AUTOSAR vendor identification]-------------------------*/

#if (defined STD_VENDOR_ID)
#error STD_VENDOR_ID is already defined
#endif
/** \brief AUTOSAR vendor identification: Elektrobit Automotive GmbH */
#define STD_VENDOR_ID         1U

/*------------------[AUTOSAR module identification]-------------------------*/

#if (defined STD_MODULE_ID)
#error STD_MODULE_ID already defined
#endif
/** \brief AUTOSAR module identification
 *
 * Left empty as this header does not belong to any module */
#define STD_MODULE_ID         197U

/*------------------[AUTOSAR release version identification]----------------*/

#if (defined STD_AR_RELEASE_MAJOR_VERSION)
#error STD_AR_RELEASE_MAJOR_VERSION already defined
#endif
/** \brief AUTOSAR release major version */
#define STD_AR_RELEASE_MAJOR_VERSION     4U

#if (defined STD_AR_RELEASE_MINOR_VERSION)
#error STD_AR_RELEASE_MINOR_VERSION already defined
#endif
/** \brief AUTOSAR release minor version */
#define STD_AR_RELEASE_MINOR_VERSION     0U

#if (defined STD_AR_RELEASE_REVISION_VERSION)
#error STD_AR_RELEASE_REVISION_VERSION already defined
#endif
/** \brief AUTOSAR release revision version */
#define STD_AR_RELEASE_REVISION_VERSION  3U

/*------------------[AUTOSAR module version identification]------------------*/

#if (defined STD_SW_MAJOR_VERSION)
#error STD_SW_MAJOR_VERSION already defined
#endif
/** \brief AUTOSAR module major version */
#define STD_SW_MAJOR_VERSION     1U

#if (defined STD_SW_MINOR_VERSION)
#error STD_SW_MINOR_VERSION already defined
#endif
/** \brief AUTOSAR module minor version */
#define STD_SW_MINOR_VERSION     3U

#if (defined STD_SW_PATCH_VERSION)
#error STD_SW_PATCH_VERSION already defined
#endif
/** \brief AUTOSAR module patch version */
#define STD_SW_PATCH_VERSION     0U

/*------------------[AUTOSAR module vendor API infix]------------------------*/

#if (defined STD_VENDOR_API_INFIX)
#error STD_VENDOR_API_INFIX already defined
#endif
/** \brief Vendor API infix
 *
 * Left empty as this header does not belong to any module */
#define STD_VENDOR_API_INFIX

/*==================[type definitions]=======================================*/

/* STD005 */
/** \brief Autosar standard API return type */
typedef uint8 Std_ReturnType;

#if (!defined STD_VERSION_INFO_TYPE_DEFINED)
#define STD_VERSION_INFO_TYPE_DEFINED STD_OFF
#endif

/* !LINKSTO Base.CustomStdVersionInfoType.ConfigMacro,1 */
#if (STD_VERSION_INFO_TYPE_DEFINED != STD_ON)
/* STD015 */
/** \brief return type for <Module_name>_GetVersionInfo() calls
 **
 ** This type is used to request the version of BSW module using the
 ** <Module_name>_GetVersionInfo() function. */
typedef struct
{
   uint16 vendorID;
   uint16 moduleID;
   uint8  sw_major_version;
   uint8  sw_minor_version;
   uint8  sw_patch_version;
} Std_VersionInfoType;

#endif /* #if (STD_VERSION_INFO_TYPE_DEFINED != STD_ON) */

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

/*==================[end of file]============================================*/
#endif /* if !defined( STD_TYPES_H ) */
