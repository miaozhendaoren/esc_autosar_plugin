<?xml version='1.0'?>
<datamodel version="3.0" 
           xmlns="http://www.tresos.de/_projects/DataModel2/08/root.xsd" 
           xmlns:a="http://www.tresos.de/_projects/DataModel2/08/attribute.xsd" 
           xmlns:v="http://www.tresos.de/_projects/DataModel2/06/schema.xsd" 
           xmlns:d="http://www.tresos.de/_projects/DataModel2/06/data.xsd">

  <d:ctr type="AUTOSAR" factory="autosar" 
         xmlns:ad="http://www.tresos.de/_projects/DataModel2/08/admindata.xsd" 
         xmlns:icc="http://www.tresos.de/_projects/DataModel2/08/implconfigclass.xsd" 
         xmlns:mt="http://www.tresos.de/_projects/DataModel2/11/multitest.xsd" >
    <d:lst type="TOP-LEVEL-PACKAGES">
      <d:ctr name="TS_TxDxM1I2R0" type="AR-PACKAGE">
        <d:lst type="ELEMENTS">
          <d:chc name="PbcfgM" type="AR-ELEMENT" value="MODULE-DEF">
            <v:ctr type="MODULE-DEF">
              <a:a name="ADMIN-DATA" type="ADMIN-DATA">
                <ad:ADMIN-DATA>
                  <ad:LANGUAGE>EN</ad:LANGUAGE>
                  <ad:DOC-REVISIONS>
                    <ad:DOC-REVISION>
                      <ad:REVISION-LABEL>4.2.0</ad:REVISION-LABEL>
                      <ad:ISSUED-BY>AUTOSAR</ad:ISSUED-BY>
                      <ad:DATE>2011-11-09T11:36:22Z</ad:DATE>
                    </ad:DOC-REVISION>
                    <ad:DOC-REVISION>
                      <ad:REVISION-LABEL>1.2.4</ad:REVISION-LABEL>
                      <ad:ISSUED-BY>Elektrobit Automotive GmbH</ad:ISSUED-BY>
                      <ad:DATE>2013-02-28T23:59:59Z</ad:DATE>
                    </ad:DOC-REVISION>
                  </ad:DOC-REVISIONS>
                </ad:ADMIN-DATA>
              </a:a>
              <a:a name="DESC">
                <a:v>&lt;html&gt;
                    Configuration of the PbcfgM (Post build configuration manager) module.
                  &lt;/html&gt;</a:v>
              </a:a>
              <a:a name="RELEASE" value="asc:4.0"/>
              <a:a name="UPPER-MULTIPLICITY" value="1"/>
              <a:a name="UUID" value="fe05a885-9532-427e-838c-a4fc3fdc1266"/>
              <v:ctr name="CommonPublishedInformation" type="IDENTIFIABLE">
                <a:a name="DESC">
                  <a:v>EN:
                    &lt;html&gt;
                      Common container, aggregated by all modules. It contains published information about vendor and versions.
                  &lt;/html&gt;</a:v>
                </a:a>
                <a:a name="LABEL" value="Common Published Information"/>
                <a:a name="UUID" 
                     value="ECUC:a00be3e0-8783-9123-2d52-1eb616737ca6"/>
                <v:var name="ArMajorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Major version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="AUTOSAR Major Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:948f3f2e-f129-4bc5-b4e1-7a7bdb8599e1"/>
                  <a:da name="DEFAULT" value="0"/>
                </v:var>
                <v:var name="ArMinorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Minor version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="AUTOSAR Minor Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:2893b920-59d5-4ac2-b2c1-e23742e66d70"/>
                  <a:da name="DEFAULT" value="0"/>
                </v:var>
                <v:var name="ArPatchVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Patch level version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="AUTOSAR Patch Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:6428eb9b-8790-488a-b9a3-0fba52d0f59e"/>
                  <a:da name="DEFAULT" value="0"/>
                </v:var>
                <v:var name="SwMajorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Major version number of the vendor specific implementation of the module.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Software Major Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:605b18ae-3f9a-41d4-9225-67c9c5f6fc34"/>
                  <a:da name="DEFAULT" value="1"/>
                </v:var>
                <v:var name="SwMinorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Minor version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Software Minor Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" value="a7fe44dc-ea25-4b8d-8fad-9fbcae86d56f"/>
                  <a:da name="DEFAULT" value="2"/>
                </v:var>
                <v:var name="SwPatchVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Patch level version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Software Patch Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" value="a318d2d9-0e75-49da-ac43-e7e4e682e2f9"/>
                  <a:da name="DEFAULT" value="4"/>
                </v:var>
                <v:var name="ModuleId" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Module ID of this module from Module List
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Numeric Module ID"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:78bc8362-080f-4253-b3da-804ab69a7154"/>
                  <a:da name="DEFAULT" value="32769"/>
                </v:var>
                <v:var name="VendorId" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Vendor ID of the dedicated implementation of this module according to the AUTOSAR vendor list
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Vendor ID"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:01b0f467-6943-4558-b4f2-3fa1fad28449"/>
                  <a:da name="DEFAULT" value="1"/>
                </v:var>
                <v:var name="VendorApiInfix" type="STRING_LABEL">
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Vendor API Infix"/>
                  <a:a name="OPTIONAL" value="true"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:1c68a547-f24e-4a4e-9540-69fbd466ec89"/>
                  <a:a name="VISIBLE" value="false"/>
                  <a:da name="DEFAULT" value=""/>
                  <a:da name="ENABLE" value="false"/>
                </v:var>
                <v:var name="Release" type="STRING_LABEL">
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Release Information"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:1c68a547-f24e-4a4e-9540-69fbd533ec89"/>
                  <a:da name="DEFAULT" value=""/>
                </v:var>
              </v:ctr>
              <v:lst name="PbcfgMBswModules" type="MAP">
                <v:ctr name="PbcfgMBswModules" type="IDENTIFIABLE">
                  <a:a name="DESC">
                    <a:v>&lt;html&gt;
                        Each container references a module configuration and module instance which are merged to one PbcfgM configuration.
                      &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="UUID" value="c506d5b5-0a98-40a0-8059-6a568aacaa35"/>
                  <v:var name="PbcfgMModuleInstanceID" type="INTEGER">
                    <a:a name="DESC" 
                         value="EN: Instance ID of the referenced module"/>
                    <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                         type="IMPLEMENTATIONCONFIGCLASS">
                      <icc:v class="PreCompile">VariantPostBuild</icc:v>
                    </a:a>
                    <a:a name="OPTIONAL" value="true"/>
                    <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                    <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                    <a:a name="UUID" 
                         value="6dcb4e9a-3af1-4335-9837-3592aad70408"/>
                    <a:da name="DEFAULT" value="0"/>
                    <a:da name="ENABLE" value="false"/>
                    <a:da name="INVALID" type="Range">
                      <a:tst expr="&gt;=0"/>
                      <a:tst expr="&lt;=255"/>
                    </a:da>
                  </v:var>
                  <v:ref name="PbcfgMBswModuleRef" type="CHOICE-REFERENCE">
                    <a:a name="DESC">
                      <a:v>&lt;html&gt;
                          This is a reference to one BSW module&apos;s configuration.
                        &lt;/html&gt;</a:v>
                      <a:v>&lt;html&gt;
                          This is a reference to one BSW module&apos;s configuration.
                        &lt;/html&gt;</a:v>
                    </a:a>
                    <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                         type="IMPLEMENTATIONCONFIGCLASS">
                      <icc:v class="PreCompile">VariantPostBuild</icc:v>
                    </a:a>
                    <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                    <a:a name="UUID" 
                         value="0d001eb5-f01f-4a04-b80b-5884aff08453"/>
                    <a:da name="INVALID" type="XPath">
                      <a:tst expr="node:refvalid(.)" 
                             false="PbcfgMBswModuleRef must reference a valid post build module."/>
                      <a:tst 
                             expr="contains(node:ref(.)/IMPLEMENTATION_CONFIG_VARIANT, &apos;PostBuild&apos;)" 
                             false="PbcfgMBswModuleRef must reference a module with an implementation config variant of post build."/>
                      <a:tst 
                             expr="(num:i(count(../../*[PbcfgMBswModuleRef = node:current()]))&gt;1)" 
                             true="The reference to this module must be unique. There is another reference to this module."/>
                    </a:da>
                    <a:da name="REF">
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Adc</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/BswM</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Can</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/CanIf</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/CanNm</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/CanSM</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/CanTp</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/CanTrcv</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Com</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/ComM</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Dbg</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Dcm</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Dem</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Dio</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Dlt</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Eep</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Eth</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/EthIf</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/EthTrcv</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/FiM</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Fls</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Fr</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/FrIf</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/FrNm</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/FrSM</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/FrTp</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Gpt</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Icu</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/IpduM</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/J1939Tp</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Lin</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/LinIf</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/LinSM</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/LinTp</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Mcu</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/PduR</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Port</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Pwm</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Rte</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Sd</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/SoAd</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Spi</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/UdpNm</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Wdg</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/WdgM</a:v>
                      <a:v>ASPathDataOfSchema:/AUTOSAR/EcucDefs/Xcp</a:v>
                    </a:da>
                  </v:ref>
                </v:ctr>
              </v:lst>
              <v:var name="IMPLEMENTATION_CONFIG_VARIANT" type="ENUMERATION">
                <a:a name="LABEL" value="Config Variant"/>
                <a:a name="UUID" value="215bc213-dab6-44cd-9b2d-7d139975abb3"/>
                <a:da name="DEFAULT" value="VariantPostBuild"/>
                <a:da name="EDITABLE" value="false"/>
                <a:da name="RANGE" value="VariantPostBuild"/>
              </v:var>
              <v:ctr name="PbcfgMGeneral" type="IDENTIFIABLE">
                <a:a name="DESC">
                  <a:v>&lt;html&gt;
                        This container specifies the general configuration parameters of the PbcfgM.
                      &lt;/html&gt;</a:v>
                </a:a>
                <a:a name="UUID" value="8b88b773-af4c-4931-97eb-31f0a4e9100f"/>
                <v:var name="PbcfgMDevErrorDetect" type="BOOLEAN">
                  <a:a name="DESC">
                    <a:v>&lt;html&gt;
                          Switches the Development Error Detection and Notification ON or OFF.
                          &lt;p&gt;&lt;strong&gt;Optimization Effect:&lt;/strong&gt;&lt;/p&gt;
                          &lt;ul&gt;
                            &lt;li class=&quot;optimization.code.rom.reduction&quot;&gt;
                              &lt;strong&gt;ROM reduction (code):&lt;/strong&gt;
                              Disabling this parameter reduces the ROM consumption of the module code.
                            &lt;/li&gt;

                            &lt;li class=&quot;optimization.code.exetime.reduction&quot;&gt;
                             &lt;strong&gt;Execution time reduction (code):&lt;/strong&gt;
                             Disabling this parameter reduces the execution time of the module code.
                           &lt;/li&gt;
                          &lt;/ul&gt;
                        &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       type="IMPLEMENTATIONCONFIGCLASS">
                    <icc:v class="PreCompile">VariantPostBuild</icc:v>
                  </a:a>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" value="800c540c-e765-48ad-a228-80de91774c93"/>
                  <a:da name="DEFAULT" value="true"/>
                </v:var>
                <v:var name="PbcfgMRelocatableCfgEnable" type="BOOLEAN">
                  <a:a name="DESC">
                    <a:v>&lt;html&gt;
                        Enables or disables the post-build-time configuration data to be used either by
                        relative offsets to the configuration start address (relocatable) or by absolute
                        pointers (not relocatable).&lt;br/&gt;
                        &lt;ul&gt;
                          &lt;li&gt;&lt;code&gt;TRUE&lt;/code&gt;: Relocateable configuration is in use (switched on).&lt;/li&gt;
                          &lt;li&gt;&lt;code&gt;FALSE&lt;/code&gt;: Relocateable configuration is not in use (switched off).&lt;/li&gt;
                        &lt;/ul&gt;

                        &lt;p&gt;&lt;strong&gt;Optimization Effect:&lt;/strong&gt;&lt;/p&gt;
                        &lt;ul&gt;
                          &lt;li class=&quot;optimization.config.rom.reduction&quot;&gt;
                            &lt;strong&gt;ROM reduction (config):&lt;/strong&gt;
                            Enabling this parameter reduces the ROM consumption of the module configuration.
                          &lt;/li&gt;
                          &lt;li class=&quot;optimization.code.exetime.reduction&quot;&gt;
                            &lt;strong&gt;Execution time reduction (code):&lt;/strong&gt;
                            Disabling this parameter reduces the execution time of the module code.
                          &lt;/li&gt;
                        &lt;/ul&gt;
                      &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       type="IMPLEMENTATIONCONFIGCLASS">
                    <icc:v class="PreCompile">VariantPostBuild</icc:v>
                  </a:a>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" value="800c540c-e765-48ad-a228-80de91774c93"/>
                  <a:da name="DEFAULT" value="true"/>
                </v:var>
                <v:var name="PbcfgMConstCfgAddress" type="INTEGER">
                  <a:a name="DEFAULT_RADIX" value="HEX"/>
                  <a:a name="DESC">
                    <a:v>&lt;html&gt;
                        Defines the fix address where the configuration starts.
                        &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       type="IMPLEMENTATIONCONFIGCLASS">
                    <icc:v class="PostBuild">VariantPostBuild</icc:v>
                  </a:a>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:da name="DEFAULT" value="0"/>
                  <a:da name="INVALID" type="Range">
                    <a:tst expr="&gt;=0"/>
                    <a:tst expr="&lt;4294967295"/>
                  </a:da>
                </v:var>
                <v:var name="PbcfgMBinarySupportEnable" type="BOOLEAN">
                  <a:a name="DESC">
                    <a:v>&lt;html&gt;
                        Enables or disables the support for generation of binary post build configuration (i.e. generation of the Motorola srec file)
                        &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       type="IMPLEMENTATIONCONFIGCLASS">
                    <icc:v class="PostBuild">VariantPostBuild</icc:v>
                  </a:a>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:da name="DEFAULT" value="false"/>
                  <a:da name="INVALID" type="XPath" 
                        expr="(. = &apos;true&apos;) and not(lic:feature(&apos;PB_GEN_BINARY&apos;))" 
                        true="Your EB tresos license does not include support for binary code generation. If you want to generate binary post build configurations, please contact EB."/>
                </v:var>
              </v:ctr>
            </v:ctr>
          </d:chc>
        </d:lst>
      </d:ctr>
    </d:lst>
  </d:ctr>

</datamodel>
