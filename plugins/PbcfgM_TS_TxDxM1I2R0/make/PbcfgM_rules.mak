# \file
#
# \brief AUTOSAR PbcfgM
#
# This file contains the implementation of the AUTOSAR
# module PbcfgM.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

ifndef TS_PBCFGM_COMPILE_WITH_POSTBUILD
TS_PBCFGM_COMPILE_WITH_POSTBUILD := TRUE
endif

ifndef TS_BUILD_POST_BUILD_BINARY
TS_BUILD_POST_BUILD_BINARY := FALSE
endif

#################################################################
# REGISTRY

PbcfgM_src_FILES += $(PbcfgM_CORE_PATH)\src\PbcfgM.c

LIBRARIES_TO_BUILD += PbcfgM_src
LIBRARIES_PBCFG_TO_BUILD += PbcfgM_pbconfig

ifeq ($(TS_PBCFGM_COMPILE_WITH_POSTBUILD),TRUE)
# Compile with post build
PbcfgM_src_FILES += $(PbcfgM_OUTPUT_PATH)/src/PbcfgM_PBcfg.c
endif

# Add generated files to the list of source files
PbcfgM_src_FILES += $(PbcfgM_OUTPUT_PATH)/src/PbcfgM_Lcfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary file shall be built do not compile any files other then the post build files.
PbcfgM_src_FILES :=
endif

# Fill the list with post build configuration files needed to build the post build binary.
PbcfgM_pbconfig_FILES := $(PbcfgM_OUTPUT_PATH)/src/PbcfgM_PBcfg.c

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES
