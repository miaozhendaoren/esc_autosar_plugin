/**
 * \file
 *
 * \brief AUTOSAR ComM
 *
 * This file contains the implementation of the AUTOSAR
 * module ComM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
/* !LINKSTO ComM456,1 */
#if (!defined COMM_CFG_H)
#define COMM_CFG_H

/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 8.8 (required)
 *     An external object or function shall be declared in one
 *     and only one file.
 *
 *     Reason:
 *     The files SchM_ComM.h and ComM_Cfg.h contain declarations of
 *     ComM_MainFunction_x. This is due to the issue reported in
 *     http://www.autosar.org/bugzilla/show_bug.cgi?id=53853
 *     and should be fixed in a future version of the Rte.
 */

/*==================[includes]==============================================*/

/* !LINKSTO ComM466,1 */
#include <Std_Types.h>

[!IF "ComMGeneral/ComMPncSupport = 'true'"!]
#include <Com.h>
[!ENDIF!]


/*==================[macros]================================================*/
/* *** General feature flags *** */

#if (defined COMM_SERVICE_API_NONE)
#error COMM_SERVICE_API_NONE already defined
#endif
/** \brief Definition that no default service API is set **/
#define COMM_SERVICE_API_NONE   0U

#if (defined COMM_SERVICE_API_ASR32)
#error COMM_SERVICE_API_ASR32 already defined
#endif
/** \brief Definition that AUTOSAR 3.2 service API is set as default **/
#define COMM_SERVICE_API_ASR32 32U

#if (defined COMM_SERVICE_API_ASR40)
#error COMM_SERVICE_API_ASR40 already defined
#endif
/** \brief Definition that AUTOSAR 4.0 service API is set as default **/
#define COMM_SERVICE_API_ASR40 40U

#if (defined COMM_ENABLE_ASR32_SERVICE_API)
#error COMM_ENABLE_ASR32_SERVICE_API already defined
#endif
/** \brief Definition whether AUTOSAR 3.2 service API for ComM is enabled **/
#define COMM_ENABLE_ASR32_SERVICE_API                   [!//
[!IF "ComMGeneral/ComMServiceAPI/ComMEnableASR32ServiceAPI = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined COMM_ENABLE_ASR40_SERVICE_API)
#error COMM_ENABLE_ASR40_SERVICE_API already defined
#endif
/** \brief Definition whether AUTOSAR 4.0 service API for ComM is enabled **/
#define COMM_ENABLE_ASR40_SERVICE_API                   [!//
[!IF "ComMGeneral/ComMServiceAPI/ComMEnableASR40ServiceAPI = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined COMM_DEFAULT_ASR_SERVICE_API)
#error COMM_DEFAULT_ASR_SERVICE_API already defined
#endif
/** \brief Definition of default service API **/
#define COMM_DEFAULT_ASR_SERVICE_API                    [!//
[!IF "ComMGeneral/ComMServiceAPI/ComMDefaultASRServiceAPI = 'AUTOSAR_32'"!]COMM_SERVICE_API_ASR32
[!ELSEIF "ComMGeneral/ComMServiceAPI/ComMDefaultASRServiceAPI = 'AUTOSAR_40'"!]COMM_SERVICE_API_ASR40
[!ELSE!]COMM_SERVICE_API_NONE
[!ENDIF!]

[!SELECT "ComMConfigSet/*[1]"!][!//
[!LOOP "ComMChannel/*"!]
#if (defined ComMConf_ComMChannel_[!"name(.)"!])
#error ComMConf_ComMChannel_[!"name(.)"!] already defined
#endif
/** \brief Symbolic name for the ComM channel "[!"name(.)"!]" */
#define ComMConf_ComMChannel_[!"name(.)"!]   [!"ComMChannelId"!]U

#if (!defined COMM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) [!"ComMChannelId"!] */
#define [!"name(.)"!]                     [!"ComMChannelId"!]U

#if (defined ComM_[!"name(.)"!])
#error ComM_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define ComM_[!"name(.)"!]   [!"ComMChannelId"!]U
#endif /* COMM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]


[!LOOP "ComMUser/*"!]
#if (defined ComMConf_ComMUser_[!"name(.)"!])
#error ComMConf_ComMUser_[!"name(.)"!] already defined
#endif

/** \brief Symbolic name for the ComM user "[!"name(.)"!]" */
#define ComMConf_ComMUser_[!"name(.)"!]   [!"ComMUserIdentifier"!]U

#if (!defined COMM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) [!"ComMUserIdentifier"!] */
#define [!"name(.)"!]                     [!"ComMUserIdentifier"!]U

#if (defined ComM_[!"name(.)"!])
#error ComM_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define ComM_[!"name(.)"!]   [!"ComMUserIdentifier"!]U
#endif /* COMM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */

[!ENDLOOP!]
[!ENDSELECT!][!//

/** \brief Enable/disable development error detection */
#define COMM_DEV_ERROR_DETECT             [!//
[!IF "ComMGeneral/ComMDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Enable/disable the version info API */
#define COMM_VERSION_INFO_API             [!//
[!IF "ComMGeneral/ComMVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Minimum time duration in ms, spent in the Full Communication
 * Mode */
#define COMM_T_MIN_FULL_COM_MODE_DURATION [!//
[!"num:i(ComMGeneral/ComMTMinFullComModeDuration * 1000)"!]U

/** \brief Check if Rte is used in ComM */
#define COMM_INCLUDE_RTE                  [!//
[!IF "ComMGeneral/ComMRteUsage = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Check if Dcm usage is enabled in ComM */
#define COMM_DCM_ENABLED                  [!//
[!IF "ComMGeneral/ComMDcmUsage = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Wake up of one channel shall lead to a wake up of all channels if
 * switch is enabled. */
#define COMM_SYNCHRONOUS_WAKE_UP          [!//
[!IF "ComMGeneral/ComMSynchronousWakeUp = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief mode limitation functionality for channel shall lead to limit mode changing if
 * switch is enabled. **/
#define COMM_MODE_LIMITATION_ENABLED      [!//
[!IF "ComMGeneral/ComMModeLimitationEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief mode inhibition affects the ECU or not. **/
#define COMM_ECU_GROUP_CLASSIFICATION     [!//
[!"num:i(ComMGeneral/ComMEcuGroupClassification)"!]U

/** \brief wakeup inhibition functionality for channel shall limit wakeup if
 * switch is enabled. **/
#define COMM_WAKEUP_INHIBITION_ENABLED    [!//
[!IF "ComMGeneral/ComMWakeupInhibitionEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief signal if the port interface ComM_CurrentChannelRequest is used */
#define COMM_CURRENTCHANNELREQUEST_ENABLED  [!//
[!IF "(count(ComMConfigSet/*[1]/ComMChannel/*[ComMFullCommRequestNotificationEnabled = 'true']) > 0) and (ComMGeneral/ComMRteUsage = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!IF "ComMGeneral/ComMWakeupInhibitionEnabled = 'true' or ComMGeneral/ComMModeLimitationEnabled = 'true'"!][!//
/** \brief Number of bytes in Nvm for storing ComM channel status */
#define COMM_NUM_BYTES_NVM                [!//
[!"num:i((((count(ComMConfigSet/*[1]/ComMChannel/*))- 1 )div 8)+1)"!]U
[!ENDIF!][!//

#if (defined COMM_NVM_ENABLED)
#error COMM_NVM_ENABLED already defined
#endif
[!//
[!VAR "NvMEnabled"!][!IF "node:refvalid(ComMGeneral/ComMGlobalNvMBlockDescriptor)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!][!ENDVAR!][!//

/** \brief Enable/Disable NvM support */
#define COMM_NVM_ENABLED                  [!"$NvMEnabled"!]

[!IF "($NvMEnabled = 'STD_ON') and ((ComMGeneral/ComMWakeupInhibitionEnabled = 'true') or (ComMGeneral/ComMModeLimitationEnabled = 'true'))"!][!//
#if (defined COMM_NVM_BLOCK_DESCRIPTOR)
#error COMM_NVM_BLOCK_DESCRIPTOR already defined
#endif

/** \brief NvM block Id for ComM block Id */
#define COMM_NVM_BLOCK_DESCRIPTOR         [!//
[!"num:i(as:ref(ComMGeneral/ComMGlobalNvMBlockDescriptor)/NvMNvramBlockIdentifier)"!]U
[!ENDIF!][!//

[!IF "ComMGeneral/ComMPncSupport = 'true'"!]
[!VAR "SigSize1" = "0"!][!//
[!VAR "SigSize2" = "0"!][!//
[!/* Get the length of the largest Com signal for array-type signals */!]
[!IF "count(node:refs(ComMConfigSet/*[1]/ComMPnc/*/ComMPncComSignal/*/ComMPncComSignalRef)[text:contains(string(ComSignalType), 'UINT8_')]) != 0"!][!//
[!VAR "SigSize1" = "num:max(node:refs(ComMConfigSet/*[1]/ComMPnc/*/ComMPncComSignal/*/ComMPncComSignalRef)[text:contains(string(ComSignalType), 'UINT8_')]/ComSignalLength)"!][!//
[!ENDIF!][!//
[!/* Get the length of the largest Com signal for non array-type signals */!]
[!IF "count(node:refs(ComMConfigSet/*[1]/ComMPnc/*/ComMPncComSignal/*/ComMPncComSignalRef)[not(text:contains(string(ComSignalType), 'UINT8_'))]) != 0"!][!//
[!VAR "SigSize2" = "num:max(node:foreach(node:refs(ComMConfigSet/*[1]/ComMPnc/*/ComMPncComSignal/*/ComMPncComSignalRef)[not(text:contains(string(ComSignalType), 'UINT8_'))]/ComSignalType, 'SigType', 'node:when($SigType = "BOOLEAN", "8", substring-after($SigType, "T"))')) div 8 "!][!//
[!ENDIF!][!//

/** \brief Number of bytes to store the EIRA or ERA */
#define COMM_PN_INFO_LENGTH               [!//
[!IF "$SigSize1 > $SigSize2"!][!//
[!"num:i($SigSize1)"!]U
[!ELSE!][!//
[!"num:i($SigSize2)"!]U
[!ENDIF!][!//
[!ENDIF!][!//

[!SELECT "ComMConfigSet/*[1]"!][!//
/** \brief Number of ComM communication channels */
#define COMM_NUM_CHANNELS                 [!//
[!"num:i(count(ComMChannel/*))"!]U

/** \brief Number of ComM users */
#define COMM_NUM_USERS                    [!//
[!"num:i(count(ComMUser/*))"!]U

/** \brief Flag indicating if Nm access is needed at all.
 *
 * ComM does not need to call Nm if no ComM channel is configured to used
 * either Nm variant PASSIVE or FULL. */
#define COMM_NM_ACCESS_NEEDED             [!//
[!IF "count(ComMChannel/*[(ComMNetworkManagement/ComMNmVariant = 'PASSIVE') or (ComMNetworkManagement/ComMNmVariant = 'FULL')]) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if CanSm access is needed at all. */
#define COMM_CANSM_ACCESS_NEEDED          [!//
[!IF "count(ComMChannel/*[ComMBusType = 'COMM_BUS_TYPE_CAN']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if FrSm access is needed at all. */
#define COMM_FRSM_ACCESS_NEEDED           [!//
[!IF "count(ComMChannel/*[ComMBusType = 'COMM_BUS_TYPE_FR']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if LinSm access is needed at all. */
#define COMM_LINSM_ACCESS_NEEDED          [!//
[!IF "count(ComMChannel/*[ComMBusType = 'COMM_BUS_TYPE_LIN']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if EthSm access is needed at all. */
#define COMM_ETHSM_ACCESS_NEEDED          [!//
[!IF "count(ComMChannel/*[ComMBusType = 'COMM_BUS_TYPE_ETH']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if there is at least one channel of variant
 * "full" */
#define COMM_NM_VARIANT_FULL_NEEDED       [!//
[!IF "count(ComMChannel/*[ComMNetworkManagement/ComMNmVariant = 'FULL']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if there is at least one channel of variant
 * "passive" */
#define COMM_NM_VARIANT_PASSIVE_NEEDED    [!//
[!IF "count(ComMChannel/*[ComMNetworkManagement/ComMNmVariant = 'PASSIVE']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if there is at least one channel of variant
 * "light" */
#define COMM_NM_VARIANT_LIGHT_NEEDED      [!//
[!IF "count(ComMChannel/*[ComMNetworkManagement/ComMNmVariant = 'LIGHT']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if there is at least one channel of variant
 * "none" */
#define COMM_NM_VARIANT_NONE_NEEDED       [!//
[!IF "count(ComMChannel/*[ComMNetworkManagement/ComMNmVariant = 'NONE']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if only channels of variant "full" are
 * configured */
#define COMM_NM_VARIANT_FULL_ONLY         [!//
[!IF "count(ComMChannel/*[ComMNetworkManagement/ComMNmVariant = 'FULL']) = count(ComMChannel/*)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if only channels of variant "light" ore "none" are
 * configured */
#define COMM_NM_VARIANT_LIGHT_NONE_ONLY   [!//
[!IF "count(ComMChannel/*[(ComMNetworkManagement/ComMNmVariant = 'LIGHT') or (ComMNetworkManagement/ComMNmVariant = 'NONE')]) = count(ComMChannel/*)"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Flag indicating if there is at least one channel of bustype
 * "internal" */
#define COMM_BUSTYPE_INTERNAL_NEEDED      [!//
[!IF "count(ComMChannel/*[ComMBusType = 'COMM_BUS_TYPE_INTERNAL']) > 0"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


/** \brief NM channel connected to the ComM channel */
#define COMM_NM_CHANNEL_OF_CHANNEL(idx)   [!//
[!IF "count(ComMChannel/*) = 1"!][!//
[!SELECT "ComMChannel/*[1]/ComMNetworkManagement"!][!//
[!IF "(ComMNmVariant = 'FULL') or (ComMNmVariant = 'PASSIVE')"!][!//
[!"num:i(as:modconf('Nm')[1]/NmChannelConfig/*[as:ref(NmComMChannelRef)/ComMChannelId = 0]/NmChannelId)"!]U [!//
/* Fixed mapping of channel specific attributes to attributes of channel #0 */
[!ELSE!][!//
0xFFU /* no NM channel associated */
[!ENDIF!][!//
[!ENDSELECT!][!//
[!ELSE!][!//
(ComM_NmChannelOfChannel[(idx)])
/* Dynamic mapping of channel specific attributes */
[!ENDIF!][!//

[!IF "(count(ComMChannel/*[ComMBusType = 'COMM_BUS_TYPE_CAN']) = count(ComMChannel/*))
or (count(ComMChannel/*[ComMBusType = 'COMM_BUS_TYPE_FR']) = count(ComMChannel/*))
or (count(ComMChannel/*[ComMBusType = 'COMM_BUS_TYPE_LIN']) = count(ComMChannel/*))
or (count(ComMChannel/*[ComMBusType = 'COMM_BUS_TYPE_ETH']) = count(ComMChannel/*))"!][!//
[!/* this also covers the case count(ComMChannel/*) = 1 */!][!//
/* !LINKSTO HisComM0001_Refine1,1, HisComM0002_Refine1,1, HisComM0003_Refine1,1 */
/** \brief Bus type of ComM channel */
#define COMM_BUSTYPE_OF_CHANNEL(idx)      [!"ComMChannel/*[1]/ComMBusType"!]
#define COMM_BUSTYPE_OF_CHANNEL_DYNAMIC   STD_OFF
[!ELSE!][!//
/** \brief Bus type of ComM channel */
/* !LINKSTO HisComM0001_Refine1,1, HisComM0002_Refine1,1, HisComM0003_Refine1,1 */
#define COMM_BUSTYPE_OF_CHANNEL(idx)      (ComM_BusTypeOfChannel[(idx)])
#define COMM_BUSTYPE_OF_CHANNEL_DYNAMIC   STD_ON
#define COMM_BUSTYPE_OF_CHANNEL_INIT \
{ \
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  [!"ComMBusType"!], /* for channel [!"name(.)"!] */ \
[!ENDLOOP!][!//
}
[!ENDIF!][!//

[!IF "(count(ComMChannel/*[(ComMNetworkManagement/ComMNmVariant = 'FULL')]) = count(ComMChannel/*))
or (count(ComMChannel/*[(ComMNetworkManagement/ComMNmVariant = 'LIGHT')]) = count(ComMChannel/*))
or (count(ComMChannel/*[(ComMNetworkManagement/ComMNmVariant = 'NONE')]) = count(ComMChannel/*))"!][!//
[!/* this also covers the case count(ComMChannel/*) = 1 */!][!//
/** \brief Nm variant of ComM channel */
/* !LINKSTO HisComM0001_Refine1,1, HisComM0002_Refine1,1, HisComM0003_Refine1,1 */
#define COMM_NM_VARIANT_OF_CHANNEL(idx)    [!//
COMM_NM_[!"ComMChannel/*[1]/ComMNetworkManagement/ComMNmVariant"!]_VARIANT
#define COMM_NM_VARIANT_OF_CHANNEL_DYNAMIC STD_OFF
[!ELSE!][!//
#define COMM_NM_VARIANT_OF_CHANNEL(idx)    (ComM_NmVariantOfChannel[(idx)])
#define COMM_NM_VARIANT_OF_CHANNEL_DYNAMIC STD_ON
#define COMM_NM_VARIANT_OF_CHANNEL_INIT \
{ \
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  COMM_NM_[!"ComMNetworkManagement/ComMNmVariant"!]_VARIANT, /* for channel [!"name(.)"!] */ \
[!ENDLOOP!][!//
}
[!ENDIF!][!//

[!LOOP "ComMChannel/*"!]
[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!]
[!VAR "numUserOfCh"="count(ComMUserPerChannel/*/ComMUserChannel)+count(node:difference(node:refs(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMChannelPerPnc/*[(node:ref(.)/ComMChannelId=node:current()/ComMChannelId)]/../../ComMUserPerPnc/*),node:refs(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMChannel/*[./ComMChannelId=node:current()/ComMChannelId]/ComMUserPerChannel/*/ComMUserChannel)))"!][!//
[!ELSE!][!//
[!VAR "numUserOfCh"="num:i(count(ComMUserPerChannel/*/ComMUserChannel))"!]
[!ENDIF!][!//
[!IF "$numUserOfCh > 0"!]
/** \brief Number of users of ComM channel, [!"name(.)"!],
* ID=[!"ComMChannelId"!] */
#define COMM_NUM_USERS_OF_CHANNEL_[!"ComMChannelId"!] [!"num:i($numUserOfCh)"!]U
[!ENDIF!][!//
[!ENDLOOP!][!//

/** \brief Number of users of ComM channel */
#define COMM_NUM_USERS_OF_CHANNEL(idx)    [!//
[!IF "count(ComMChannel/*) = 1"!][!//
COMM_NUM_USERS_OF_CHANNEL_0
[!ELSE!][!//
(ComM_NumUsersOfChannel[(idx)])
[!ENDIF!][!//
[!ENDSELECT!][!//

#define COMM_PNC_RELEASE_CHANNEL    [!//
[!IF "ComMGeneral/ComMPncReleaseChannel = 'COMM_NO_INTERNAL_AND_EXTERNAL_REQUESTS'"!][!//
COMM_NO_INTERNAL_AND_EXTERNAL_REQUESTS
[!ELSE!][!//
COMM_NO_INTERNAL_REQUESTS
[!ENDIF!]

/*------------------[Pnc configuration]-------------------------------------*/
/** \brief General Support for Partial network cluster (Pnc) */
#define COMM_PNC_SUPPORT                  [!//
[!IF "ComMGeneral/ComMPncSupport = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!IF "node:exists(ComMGeneral/ComMPncGatewayEnabled)"!][!//
/** \brief Support for Partial network cluster gateway */
#define COMM_PNC_GW_ENABLED               [!//
[!IF "ComMGeneral/ComMPncGatewayEnabled = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!ELSE!]
#define COMM_PNC_GW_ENABLED    STD_OFF
[!ENDIF!]

/** \brief Number of Partial network cluster (Pnc) */
#define COMM_NUM_PNC                      [!//
[!IF "ComMGeneral/ComMPncSupport = 'true'"!][!"num:i(count(ComMConfigSet/*[1]/ComMPnc/*))"!]U[!ELSE!]0U[!ENDIF!]

[!IF "ComMGeneral/ComMPncSupport = 'true'"!]
[!VAR "LeastMainFnPeriod" = "0"!][!//
[!LOOP "node:order(ComMConfigSet/*[1]/ComMChannel/*, 'ComMMainFunctionPeriod')"!][!//
[!VAR "LeastMainFnPeriod" = "ComMMainFunctionPeriod"!]
[!BREAK!]
[!ENDLOOP!][!//

/** \brief Prepare Sleep timeout time in ms, spent in the
 *  PNC_PREPARE_SLEEP state */
#define COMM_T_PNC_PREPARE_SLEEP [!"num:i((ComMGeneral/ComMPncPrepareSleepTimer * 1000) div ($LeastMainFnPeriod * 1000))"!]U
[!ENDIF!][!//

[!SELECT "ComMConfigSet/*[1]"!][!//
[!LOOP "ComMPnc/*"!]
#if (defined ComMConf_ComMPnc_[!"name(.)"!])
#error ComMConf_ComMPnc_[!"name(.)"!] already defined
#endif
/** \brief Symbolic name for the ComM Pnc "[!"name(.)"!]" */
#define ComMConf_ComMPnc_[!"name(.)"!]   [!"ComMPncId"!]U

#if (!defined COMM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) [!"ComMPncId"!] */
#define [!"name(.)"!]                     [!"ComMPncId"!]U

#if (defined ComM_[!"name(.)"!])
#error ComM_[!"name(.)"!] already defined
#endif

/** \brief Export symbolic name value with module abbreviation as prefix
 ** only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define ComM_[!"name(.)"!]   [!"ComMPncId"!]U
#endif /* COMM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]

[!VAR "PncIdx"="0"!]
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!][!//
/** \brief Number of users of Pnc[[!"num:i($PncIdx)"!]], i.e. [!"name(.)"!],
 *  ID=[!"ComMPncId"!] */
#define COMM_NUM_USERS_OF_PNC_[!"ComMPncId"!] [!//
[!"num:i(count(ComMUserPerPnc/*))"!]U

[!VAR "PncIdx"="num:i($PncIdx)+num:i(1)"!][!//
[!ENDLOOP!][!//

/** \brief Number of unique Rx EIRA Signals */
#define COMM_NUM_RX_EIRA_SIGNALS      [!//
[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!][!"num:i(count(node:refs(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA'][ComMPncComSignalDirection='RX']/ComMPncComSignalRef)))"!]U[!ELSE!]0U[!ENDIF!]

/** \brief Number of unique Tx EIRA Signals */
#define COMM_NUM_TX_EIRA_SIGNALS      [!//
[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!][!"num:i(count(node:refs(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA'][ComMPncComSignalDirection='TX']/ComMPncComSignalRef)))"!]U[!ELSE!]0U[!ENDIF!]

/** \brief Number of bytes required to store the received status
 ** of unique Rx EIRA signal assigned for all PNCs */
#if (COMM_NUM_RX_EIRA_SIGNALS > 0U)
#define COMM_SIGNAL_STATUS_BYTES (((COMM_NUM_RX_EIRA_SIGNALS - 1U ) / 8U) + 1U)
#else
#define COMM_SIGNAL_STATUS_BYTES 0U
#endif

/*--------[ COMM_NUM_USERS_OF_PNC( ) ]---------*/
/** \brief Number of users of Pnc */
#define COMM_NUM_USERS_OF_PNC(idx)    [!//
[!IF "count(ComMPnc/*) = 1"!][!//
[!"num:i(count(ComMPnc/*[1]/ComMUserPerPnc/*))"!]U
[!ELSE!][!//
(ComM_NumUsersOfPnc[(idx)])
[!ENDIF!][!//

/*--------[ COMM_NUM_CHANNEL_OF_PNC_X ]---------*/
[!VAR "PncIdx"="0"!]
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!][!//
/** \brief Number of channels of Pnc[[!"num:i($PncIdx)"!]], i.e [!"name(.)"!]
 *  ID=[!"ComMPncId"!] */
#define COMM_NUM_CHANNELS_OF_PNC_[!"ComMPncId"!] [!//
[!"num:i(count(ComMChannelPerPnc/*))"!]U

[!VAR "PncIdx"="num:i($PncIdx)+num:i(1)"!][!//
[!ENDLOOP!][!//

[!LOOP "ComMUser/*"!][!//
/** \brief Number of Pnc needed by user [!"name(.)"!], ID=[!"ComMUserIdentifier"!] */
#define COMM_NUM_PNC_OF_USER_[!"ComMUserIdentifier"!] [!//
[!"num:i(count(../../ComMPnc/*[node:refs(ComMUserPerPnc/*)[ComMUserIdentifier = node:current()/ComMUserIdentifier]]))"!]U
[!ENDLOOP!][!//
[!ENDSELECT!][!//

[!IF "ComMGeneral/ComMDirectUserMapping = 'true'"!][!//
/** \brief Short Name of ComM users used by BswM when direct user
 *  mapping is enabled */
[!LOOP "ComMConfigSet/*[1]/ComMPnc/*"!][!//
#define  PNCUser_[!"ComMPncId"!]  [!"ComMPncId"!]U
[!ENDLOOP!][!//

/** \brief Short Name of ComM channels used by BswM when direct user
 *  mapping is enabled */
[!LOOP "ComMConfigSet/*[1]/ComMChannel/*"!][!//
#define  ChannelUser_[!"ComMChannelId"!]  [!"ComMChannelId"!]U
[!ENDLOOP!][!//
[!ENDIF!][!//

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/* hide declaration in own header if included by ComM itself and RTE is
 * enabled */
#if ((!defined(COMM_INTERNAL_USAGE)) || (COMM_INCLUDE_RTE == STD_OFF))

#define COMM_START_SEC_CODE
#include <MemMap.h>

/* If Rte usage is enabled in the ComM configuration the declaration of these
 * functions are provided by Rte_ComM.h. */

[!LOOP "ComMConfigSet/*[1]/ComMChannel/*"!][!//
/** \brief Main function for channel [!"ComMChannelId"!] */
/* Deviation MISRA-1 */
extern FUNC(void, COMM_CODE) ComM_MainFunction_[!"ComMChannelId"!](void);
[!ENDLOOP!][!//

#define COMM_STOP_SEC_CODE
#include <MemMap.h>

#endif

#if (COMM_PNC_SUPPORT == STD_ON)

#define COMM_START_SEC_CODE
#include <MemMap.h>

/** \brief COM Callback function indicates reception of an RX-EIRA
 **
 ** This function is called from the COM module in case the PDU including the
 ** eira signal is received.
 **
 ** \note called in interrupt context */
[!LOOP "node:refs(ComMConfigSet/*[1]/ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA'][ComMPncComSignalDirection='RX']/ComMPncComSignalRef)"!]

/** \brief Com callback function for the signal "[!"node:name(.)"!]" */
extern FUNC(void, COMM_CODE) ComM_COMCbk_[!"node:name(.)"!]
(void);

[!ENDLOOP!]

#define COMM_STOP_SEC_CODE
#include <MemMap.h>

#endif

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* if !defined( COMM_CFG_H ) */
