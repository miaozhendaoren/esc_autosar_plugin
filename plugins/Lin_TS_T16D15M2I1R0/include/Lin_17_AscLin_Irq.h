
#ifndef ASCLIN_17_ASCLIN_IRQ_H
#define ASCLIN_17_ASCLIN_IRQ_H

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Std_Types.h"                                                        

#ifdef OS_KERNEL_TYPE                                                           
#include <Os.h>        /* OS interrupt services */                              
#endif


/*******************************************************************************
**                      Public Macro Definitions                              **
*******************************************************************************/

/************************* interrupt CATEGORY *********************************/

#ifdef IRQ_CAT1
#if (IRQ_CAT1 != 1)
#error IRQ_CAT1 already defined with a wrong value
#endif
#else
#define IRQ_CAT1                    (1)
#endif

#ifdef IRQ_CAT23
#if (IRQ_CAT23 != 2)
#error IRQ_CAT23 already defined with a wrong value
#endif
#else
#define IRQ_CAT23                    (2)
#endif



/* The name of the ISRs shall be the same name than the ISR       *
 * functions provided by Infineon                                 */

/*Set default interrupt to AscLin interrupt */
#define IRQ_ASCLIN_USED_MCALLIN  STD_ON

/*************************ASCLIN0TX_ISR*********************************/          

#ifdef ASCLIN0TX_ISR 
#define IRQ_ASCLIN0TX_EXIST        STD_ON
#define IRQ_ASCLIN_CHANNEL0_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN0_TX_PRIO    	   ASCLIN0TX_ISR_ISR_LEVEL
#define IRQ_ASCLIN0_TX_CAT         ASCLIN0TX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN0TX_EXIST        STD_OFF
#endif

/*************************ASCLIN0RX_ISR*********************************/          

#ifdef ASCLIN0RX_ISR
#define IRQ_ASCLIN0RX_EXIST        STD_ON
#define IRQ_ASCLIN_CHANNEL0_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN0_RX_PRIO        ASCLIN0RX_ISR_ISR_LEVEL
#define IRQ_ASCLIN0_RX_CAT         ASCLIN0RX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN0RX_EXIST        STD_OFF
#endif

/*************************ASCLIN0EX_ISR*********************************/          

#ifdef ASCLIN0EX_ISR
#define IRQ_ASCLIN0ERR_EXIST       STD_ON
#define IRQ_ASCLIN_CHANNEL0_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN0_ERR_PRIO       ASCLIN0EX_ISR_ISR_LEVEL
#define IRQ_ASCLIN0_ERR_CAT        ASCLIN0EX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN0ERR_EXIST       STD_OFF
#endif

/*************************ASCLIN1TX_ISR*********************************/          

#ifdef ASCLIN1TX_ISR
#define IRQ_ASCLIN1TX_EXIST        STD_ON
#define IRQ_ASCLIN_CHANNEL1_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN1_TX_PRIO        ASCLIN1TX_ISR_ISR_LEVEL
#define IRQ_ASCLIN1_TX_CAT         ASCLIN1TX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN1TX_EXIST        STD_OFF
#endif

/*************************ASCLIN1RX_ISR*********************************/          

#ifdef ASCLIN1RX_ISR
#define IRQ_ASCLIN1RX_EXIST        STD_ON
#define IRQ_ASCLIN_CHANNEL1_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN1_RX_PRIO        ASCLIN1RX_ISR_ISR_LEVEL
#define IRQ_ASCLIN1_RX_CAT         ASCLIN1RX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN1RX_EXIST        STD_OFF
#endif

/*************************ASCLIN1EX_ISR*********************************/          

#ifdef ASCLIN1EX_ISR
#define IRQ_ASCLIN1ERR_EXIST       STD_ON
#define IRQ_ASCLIN_CHANNEL1_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN1_ERR_PRIO       ASCLIN1EX_ISR_ISR_LEVEL
#define IRQ_ASCLIN1_ERR_CAT        ASCLIN1EX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN1ERR_EXIST       STD_OFF
#endif

/*************************ASCLIN2TX_ISR*********************************/          

#ifdef ASCLIN2TX_ISR
#define IRQ_ASCLIN2TX_EXIST        STD_ON
#define IRQ_ASCLIN_CHANNEL2_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN2_TX_PRIO        ASCLIN2TX_ISR_ISR_LEVEL
#define IRQ_ASCLIN2_TX_CAT         ASCLIN2TX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN2TX_EXIST        STD_OFF
#endif

/*************************ASCLIN2RX_ISR*********************************/          

#ifdef ASCLIN2RX_ISR
#define IRQ_ASCLIN2RX_EXIST        STD_ON
#define IRQ_ASCLIN_CHANNEL2_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN2_RX_PRIO        ASCLIN2RX_ISR_ISR_LEVEL
#define IRQ_ASCLIN2_RX_CAT         ASCLIN2RX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN2RX_EXIST        STD_OFF
#endif

/*************************ASCLIN2EX_ISR*********************************/          

#ifdef ASCLIN2EX_ISR
#define IRQ_ASCLIN2ERR_EXIST       STD_ON
#define IRQ_ASCLIN_CHANNEL2_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN2_ERR_PRIO       ASCLIN2EX_ISR_ISR_LEVEL
#define IRQ_ASCLIN2_ERR_CAT        ASCLIN2EX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN2ERR_EXIST       STD_OFF
#endif

/*************************ASCLIN3TX_ISR*********************************/          

#ifdef ASCLIN3TX_ISR
#define IRQ_ASCLIN3TX_EXIST        STD_ON
#define IRQ_ASCLIN_CHANNEL3_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN3_TX_PRIO        ASCLIN3TX_ISR_ISR_LEVEL
#define IRQ_ASCLIN3_TX_CAT         ASCLIN3TX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN3TX_EXIST        STD_OFF
#endif

/*************************ASCLIN3RX_ISR*********************************/          

#ifdef ASCLIN3RX_ISR
#define IRQ_ASCLIN3RX_EXIST        STD_ON
#define IRQ_ASCLIN_CHANNEL3_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN3_RX_PRIO        ASCLIN3RX_ISR_ISR_LEVEL
#define IRQ_ASCLIN3_RX_CAT         ASCLIN3RX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN3RX_EXIST        STD_OFF
#endif

/*************************ASCLIN3EX_ISR*********************************/          

#ifdef ASCLIN3EX_ISR
#define IRQ_ASCLIN3ERR_EXIST       STD_ON
#define IRQ_ASCLIN_CHANNEL3_USED   IRQ_ASCLIN_USED_MCALLIN
#define IRQ_ASCLIN3_ERR_PRIO       ASCLIN3EX_ISR_ISR_LEVEL
#define IRQ_ASCLIN3_ERR_CAT        ASCLIN3EX_ISR_ISR_CATEGORY
#else
#define IRQ_ASCLIN3ERR_EXIST       STD_OFF
#endif

#endif /* #ifndef ASCLIN_17_ASCLIN_IRQ_H */
