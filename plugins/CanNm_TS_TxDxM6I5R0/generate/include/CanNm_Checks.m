[!/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//

[!/* These checks were originally present as INVALID checks in CanNm.xdm.
   * Since these inter-module checks involve parameters from different
   * configuration classes, it's no more possible to keep them in CanNm.xdm.
   * Refer ASCCANNM-443 for more details.
   */!]

[!/* *** multiple inclusion protection *** */!]
[!IF "not(var:defined('CANNM_CHECKS_M'))"!]
[!VAR "CANNM_CHECKS_M" = "'true'"!]

[!NOCODE!][!//

[!/* === Inter-module checks between CanNm and CanIf, EcuC or PduR === */!]

[!/* === General checks === */!]

[!/* Ensure that the routing path of the EIRA Rx NSdu is configured in PduR */!]
[!/* === This check has been moved from CanNmPnEiraRxNSduRef in CanNm.xdm.m4 === */!]
[!SELECT "CanNmGlobalConfig/*[1]"!]
  [!IF "((count(CanNmChannelConfig/*[CanNmPnEnabled='true']) > 0) and
         (not(node:exists(as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/
          PduRRoutingPath/*/PduRSrcPdu[PduRSrcPduRef =
          node:current()/CanNmPnEiraRxNSduRef]/PduRSourcePduHandleId))))"!]
    [!ERROR!]"The routing path for CanNm EIRA PDU (CanNmPnEiraRxNSduRef) is not configured in the PduR module."
    [!ENDERROR!]
  [!ENDIF!]
[!ENDSELECT!]

[!/* Perform the following checks for each channel configured */!]
[!LOOP "CanNmGlobalConfig/*[1]/CanNmChannelConfig/*"!]

  [!SELECT "CanNmUserDataRxPdu"!]


    [!/* Ensure that the length of User Data Rx Pdu
       * matches with User Data Length parameter
       */!]
    [!/* === This check has been moved from CanNmUserDataRxPdu in CanNm.xdm.m4 === */!]
    /* !LINKSTO CANNM332,1 */
    [!IF "(num:i(node:ref(CanNmRxUserDataPduRef)/PduLength) !=
           num:i(../CanNmUserDataLength))"!]
      [!ERROR!]Length of Rx User Data Pdu (CanNmRxUserDataPduRef) for channel '[!"name(..)"!]' doesn't match with User Data Length parameter (CanNmUserDataLength).
      [!ENDERROR!]
    [!ENDIF!]

    [!/* Ensure that routing path of User Data Rx Pdu is configured in PduR */!]
    /* !LINKSTO CanNm.ComUserDataSupport.PduR_CanNmUserDataRxId,1 */
    [!/* === This check has been moved from CanNmUserDataRxPdu in CanNm.xdm.m4 === */!]
    [!IF "(not(node:exists(as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/
           PduRRoutingPath/*/PduRSrcPdu[PduRSrcPduRef = node:current()/CanNmRxUserDataPduRef]/PduRSourcePduHandleId)))"!]
      [!ERROR!]Routing path for the CanNm UserData Rx PDU (CanNmRxUserDataPduRef) in channel '[!"name(..)"!]' is not configured in PduR module.
      [!ENDERROR!]
    [!ENDIF!]

  [!ENDSELECT!]

  [!SELECT "CanNmUserDataTxPdu"!]


    [!/* Ensure that the length of User Data Tx Pdu
       * matches with User Data Length parameter
       */!]
    [!/* === This check has been moved from CanNmTxUserDataPduRef in CanNm.xdm.m4 === */!]
    /* !LINKSTO CANNM332,1 */
    [!IF "(num:i(node:ref(CanNmTxUserDataPduRef)/PduLength) != num:i(../CanNmUserDataLength))"!]
      [!ERROR!]Length of Tx User Data Pdu (CanNmTxUserDataPduRef) for channel '[!"name(..)"!]' doesn't match with User Data Length parameter (CanNmUserDataLength).
      [!ENDERROR!]
    [!ENDIF!]

    [!/* Ensure that routing path of User Data Tx Pdu is configured in PduR */!]
    [!IF "(not(node:exists(as:modconf('PduR')[1]/PduRRoutingTables/*/PduRRoutingTable/*/
           PduRRoutingPath/*/PduRDestPdu/*[PduRDestPduRef =
           node:current()/CanNmTxUserDataPduRef]/PduRDestPduHandleId)))"!]
      [!ERROR!]Routing path for the CanNm UserData Tx PDU (CanNmTxUserDataPduRef) in channel '[!"name(..)"!]' is not configured in PduR module.
      [!ENDERROR!]
    [!ENDIF!]

  [!ENDSELECT!]

  [!IF "(node:refvalid(CanNmTxPdu/CanNmTxPduRef))"!]

    [!/* Ensure that the length of User Data Tx Pdu and User Data Rx Pdu are equal */!]
    [!/* === This check has been moved from CanNmTxPduRef in CanNm.xdm.m4 === */!]
    [!IF "((node:refvalid(CanNmRxPdu/CanNmRxPduRef)) and
           (num:i(node:ref(CanNmTxPdu/CanNmTxPduRef)/PduLength) !=
            num:i(node:ref(CanNmRxPdu/CanNmRxPduRef)/PduLength)))"!]
      [!ERROR!]The length of Tx Pdu (CanNmTxPduRef) for channel '[!"name(.)"!]' is not equal to the length of the Rx Pdu (CanNmRxPduRef).
      [!ENDERROR!]
    [!ENDIF!]

    [!/* Ensure that CanIf does not have a Tx confirmation function configured
       * when Passive mode is enabled.
       */!]
    [!/* === This check has been moved from CanNmTxPduRef in CanNm.xdm.m4 === */!]
    [!IF "((../../CanNmPassiveModeEnabled = 'true') and
           (node:exists(as:modconf('CanIf')[1]/CanIfInitConfiguration/*/
            CanIfTxPduConfig/*[PduIdRef = node:current()/
            CanNmTxPdu/CanNmTxPduRef]/CanIfUserTxConfirmation)))"!]
      [!ERROR!]'CanNmPassiveModeEnabled' is set to 'true', but the Tx Pdu referenced by 'CanNmTxPduRef' for channel '[!"node:name(.)"!]' is configured to call a Tx confirmation function in CanIf.
      [!ENDERROR!]
    [!ENDIF!]

    [!/* Ensure that CanIf does not have a Tx confirmation function configured
       * when CanNmImmediateTxconfEnabled is true.
       */!]
    [!/* === This check has been moved from CanNmTxPduRef in CanNm.xdm.m4 === */!]
    [!IF "((../../CanNmImmediateTxconfEnabled = 'true') and
           (node:exists(as:modconf('CanIf')[1]/CanIfInitConfiguration/*/
            CanIfTxPduConfig/*[PduIdRef = node:current()/
            CanNmTxPdu/CanNmTxPduRef]/CanIfUserTxConfirmation)))"!]
      [!ERROR!]'CanNmImmediateTxconfEnabled' is set to 'true', but the Tx Pdu referenced by 'CanNmTxPduRef' for channel '[!"node:name(.)"!]' is configured to call a Tx confirmation function in CanIf.
      [!ENDERROR!]
    [!ENDIF!]

  [!ENDIF!]

  [!/* === Checks for Rx Pdu === */!]

    [!/* Ensure that Rx Pdu length is not 0 */!]
    [!IF "(num:i(node:ref(CanNmRxPdu/CanNmRxPduRef)/PduLength) = 0)"!]
      [!ERROR!]The length of Rx Pdu (CanNmRxPduRef) for channel '[!"name(.)"!]' is 0!
      [!ENDERROR!]
    [!ENDIF!]

    [!/* Ensure that Rx Pdu is large enough to hold PN Info */!]
    [!/* === This check has been moved from CanNmRxPduRef in CanNm.xdm.m4 === */!]
    [!IF "((count(../*[CanNmPnEnabled = 'true']) > 0) and
          (node:ref(CanNmRxPdu/CanNmRxPduRef)/PduLength <
           (../../CanNmPnInfo/CanNmPnInfoLength + ../../CanNmPnInfo/CanNmPnInfoOffset)))"!]
        [!ERROR!]Partial networking is enabled for channel '[!"node:name(.)"!]', but length of Rx Pdu (CanNmRxPduRef) is less than (CanNmPnInfoLength + CanNmPnInfoOffset).
      [!ENDERROR!]
    [!ENDIF!]

    [!/* Ensure that Rx Pdu is large enough to hold CBV and Node Id */!]
    [!/* === This check has been moved from CanNmUserDataLength in CanNm.xdm.m4 === */!]
    [!IF "(not(node:refvalid(CanNmTxPdu/CanNmTxPduRef)) and
           (CanNmUserDataLength > (as:ref(CanNmRxPdu/CanNmRxPduRef)/PduLength -
            count((CanNmPduNidPosition|CanNmPduCbvPosition)[. != 'CANNM_PDU_OFF']))))"!]
      [!ERROR!]The size of the user data is greater than the length of the PDU subtracted by length of the Control Bit Vector or the Node Id.
      [!ENDERROR!]
    [!ENDIF!]


[!ENDLOOP!]

[!ENDNOCODE!][!//

[!ENDIF!]

[!/*
*************************************************************************
* MACRO to get the string for the symbolic name value.
* "ShortNameRef" must reference the container holding the short name.
*************************************************************************
*/!]
[!NOCODE!][!//
[!MACRO "GetSymbolName","ShortNameRef"!][!//
  [!VAR "SymbolName" = "asc:getShortName($ShortNameRef)"!][!//
  [!IF "$SymbolName = ''"!][!//
    [!VAR "SymbolName" = "concat('CanNmConf_',name(../..),'_',name(..),'_',name(.))"!][!//
  [!ELSE!][!//
    [!VAR "SymbolName" = "concat('CanNmConf_',name(.),'_',$SymbolName)"!][!//
  [!ENDIF!][!//
[!ENDMACRO!][!//
[!ENDNOCODE!]
