/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#ifndef CANNM_INT_H
#define CANNM_INT_H

/*===============================[includes]=================================*/

#include <Std_Types.h>          /* AUTOSAR standard types */
#include <TSAutosar.h>          /* EB specific standard types */
/* !LINKSTO CANNM309,1 */
#include <NmStack_Types.h>      /* Nm specific types */

#include <CanNm.h>              /* Types and decl. from public API */
#include <CanNm_Cfg.h>          /* Module configuration */
/* !LINKSTO CANNM305,1 */
#include <ComStack_Types.h>     /* Include for PduInfoType */


/*===========================[macro definitions]============================*/
#if (defined CANNM_DET_REPORT_ERROR)
#error CANNM_DET_REPORT_ERROR already defined
#endif

/* define function like macro for development error reporting,
 * if development error detection is enabled */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)

/** \brief Macro for reporting an error to DET
 **
 ** \param[in] InstanceId Instance ID of the API function
 ** \param[in] ServiceId Service ID of the API function
 ** \param[in] ErrorCode Error code reported to Det module */
#define CANNM_DET_REPORT_ERROR(InstanceId, ServiceId, ErrorCode)    \
  ((void)Det_ReportError(CANNM_MODULE_ID, (InstanceId), (ServiceId), (ErrorCode)))
#else
#define CANNM_DET_REPORT_ERROR(InstanceId, ServiceId, ErrorCode)
#endif /* if (COMM_DEV_ERROR_DETECT == STD_ON) */

/** \brief Macro to create a bitmask with single bit set at specified position
 ** (BitPos)
 **
 ** \param[in] BitPos - Bit position */
#define CANNM_BITPOS2MASK(BitPos) ((uint8)(1U << (BitPos)))

/** \brief Define CANNM_BUSLOADREDACTIVE */
#define CANNM_BUSLOADREDACTIVE          0U

/* !LINKSTO CANNM045,1 */
/** \brief Bit position of the Repeat message bit in the TX/RX PDU */
#define CANNM_CBV_REPEATMESSAGEBIT      0U

/* !LINKSTO CANNM045,1 */
/** \brief Bit position of the Active Wake Up Bit in the CBV */
#define CANNM_CBV_ACTIVEWAKEUPBIT       0x10U

/* Internal State */
/* The numbers describe bit positions */
#define CANNM_COM_DISABLED_BIT               0U
#define CANNM_NET_REQUESTED_BIT             1U


/* bits 3 and 4 are used for setting Remote sleep indication status */
#define CANNM_RSI_FALSE                 0x00U
#define CANNM_RSI_TRUE                  0x08U
#define CANNM_RSI_REJECT                0x10U
#define CANNM_RSI_MASK                  0x18U

/* Bits 5 is used for setting Immediate Nm Transmission Status */
#define CANNM_IMMEDIATE_TRANS_BIT    5U

/* !LINKSTO CANNM045,1 */
/** \brief Bit position of the Partial Netork Information Bit in the TX/RX PDU
 */
#define CANNM_CBV_PNINFOBIT                                6U

#define CANNM_CBV_PNINFOBITMASK                  CANNM_BITPOS2MASK(CANNM_CBV_PNINFOBIT)

/* Macro when EIRA Timer usage is not used. It is used for mapping
 * PN Index and EIRA timer array */
#define CANNM_EIRA_TIMER_INVALID    0xFFU

/* Bitmasks for Internal States */
#define CANNM_COM_DISABLED                     \
   CANNM_BITPOS2MASK(CANNM_COM_DISABLED_BIT)
#define CANNM_NETWORK_REQUESTED               \
   CANNM_BITPOS2MASK(CANNM_NET_REQUESTED_BIT)
#define CANNM_BUS_LOAD_RED_ENABLED            \
   CANNM_BITPOS2MASK(CANNM_BUS_LOAD_RED)
#define CANNM_IMMEDIATE_TRANSMISSION_ACTIVE  \
   CANNM_BITPOS2MASK(CANNM_IMMEDIATE_TRANS_BIT)
#define CANNM_CHANNEL_CONFIG_BUSLOADREDACTIVE(a) \
   ((CANNM_CHANNEL_CONFIG(a).CCFlags & CANNM_BITPOS2MASK(CANNM_BUSLOADREDACTIVE)) != 0U)



#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
#define CANNM_CHANNEL_STATUS(a)       CanNm_ChanStatus[(a)]
#define CANNM_CHANNEL_CONFIG(a)       CanNm_ChanConfig[(a)]
#else
#define CANNM_CHANNEL_STATUS(a)       CanNm_ChanStatus[0]
#define CANNM_CHANNEL_CONFIG(a)       CanNm_ChanConfig[0]
#endif


/*==================[type definitions]======================================*/

/** \brief Definition of the CanNm_TimeType */
typedef uint16 CanNm_TimeType;

/** \brief Definition of the CanNm_NodeIdType */
typedef uint8 CanNm_NodeIdType;

/** \brief Type for the channel configuration
 **
 ** This type stores the static data of a CANNM channels */
typedef struct
{
   P2VAR(uint8, TYPEDEF, AUTOSAR_COMSTACKDATA) RxPduPtr;
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
   P2VAR(uint8, TYPEDEF, AUTOSAR_COMSTACKDATA) TxPduPtr;
#endif
   CanNm_TimeType    CanNmTime;
   CanNm_TimeType    RMSTime;
   CanNm_TimeType    WBSTime;
   CanNm_TimeType    RSITime;
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
   CanNm_TimeType    ImmediateNmTransmissions;
   CanNm_TimeType    ImmediateNmCycleTime;
   CanNm_TimeType    MsgCycleTime;
   CanNm_TimeType    MsgCycleOffset;
#if (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF)
   CanNm_TimeType    MsgTimeoutTime;
#endif
#if (CANNM_BUS_LOAD_REDUCTION_ENABLED == STD_ON)
   CanNm_TimeType    ReducedTime;
#endif
#endif /* passive mode not enabled */
   PduLengthType     PduLength;
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
   PduIdType         TxPduId;
   PduIdType         TxConfPduId;
#endif
#if ((CANNM_REPEAT_MSG_IND_ENABLED == STD_ON) &&  \
     (CANNM_NODE_DETECTION_ENABLED == STD_ON))
   PduIdType         RxPduId;
#endif
#if (CANNM_COM_USER_DATA_SUPPORT == STD_ON)
   PduIdType         UserTxPduId;
   PduIdType         UserTxConfPduId;
   PduIdType         UserRxPduId;
#endif
   NetworkHandleType nmChannelId;
#if (CANNM_NODE_ID_ENABLED == STD_ON)
   CanNm_NodeIdType  NodeId;
#endif
   uint8             CCFlags;
   uint8             UserDataLength;
   uint8             NidPos;
   uint8             CbvPos;
   uint8             UDFBPos;
   boolean           AllNmMessagesKeepAwake; /* CANNM068_Conf */
   boolean           PnEnabled; /* CANNM066_Conf */
   boolean           PnHandleMultipleNetworkRequests;
#if (CANNM_COM_USER_DATA_SUPPORT == STD_ON)
   boolean           UserRxPduEnabled;
#endif
   boolean           NmRetryFirstMessageRequest;
} CanNm_ChanConfigType;

/** \brief Type for the channel state
 **
 ** This type stores the dynamic data of a CanNm channel; e.g. timers
 ** and states. */
typedef struct
{
   Nm_StateType      CurState;
   CanNm_TimeType    CanNmTimer;
   CanNm_TimeType    UniversalTimer;
   CanNm_TimeType    RmsTimer;
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
   CanNm_TimeType    ImmediateNmTransmissionCounter;
   CanNm_TimeType    MsgCycleTimer;
#if (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF)
   CanNm_TimeType    TimeoutTimer;
#endif
#endif
   uint8             ChanStatus;
#if (CANNM_PN_ENABLED == STD_ON)
   boolean           PnFilterEnabled;
#endif
#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
   uint8             PnInfoEra[CANNM_PN_INFO_LENGTH];
#endif
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
   boolean           NmTimerExpired;
   boolean           FirstCanIfTransmitOk;
#endif   
} CanNm_ChanStatusType;

#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
typedef struct
{
  CanNm_TimeType    EiraTimer[CANNM_EIRA_TIMER_SIZE];
  uint8             EiraValue[CANNM_PN_INFO_LENGTH];
}CanNm_PnStatusType;

#endif

/*=====================[external function declarations]=====================*/
#define CANNM_START_SEC_CODE
#include <MemMap.h>
#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)

/** \brief handles the EIRA accumulation
 **
 ** This function restarts the timer and sets the EIRA bits of the required PNs
 **
 ** \param[in] MessageBuffer Rx or Tx buffer which contains the NM message.
 */
extern FUNC(void, CANNM_CODE) CanNm_HandlePnEira
(
  CONSTP2CONST(uint8, AUTOMATIC, CANNM_APPL_CONST) MessageBuffer
);

/** \brief checks whether the received message is valid or not
 **
 ** This function checks whether the received message is valid or not.
 **
 ** \param[in] PduId Pdu Id of the received PDU.
 **
 ** \return Received message is valid or not.
 */
extern FUNC(boolean, CANNM_CODE) CanNm_IsValidPnMessage
(
  const PduIdType PduId
);
#endif
#define CANNM_STOP_SEC_CODE
#include <MemMap.h>
/*=====================[internal function declarations]=====================*/
#define CANNM_START_SEC_CODE
#include <MemMap.h>

#if ((CANNM_COM_USER_DATA_SUPPORT == STD_ON) || (CANNM_PN_EIRA_CALC_ENABLED == STD_ON))

extern FUNC(void, CANNM_CODE)CanNm_GetPduUserData
(
  const uint8 instIdx,
  P2VAR(PduInfoType, AUTOMATIC, CANNM_APPL_DATA) pduInfo
);

#endif

#define CANNM_STOP_SEC_CODE
#include <MemMap.h>

/*=====================[external constants declarations]====================*/

#define CANNM_START_SEC_CONST_8
#include <MemMap.h>
#if (CANNM_PN_ENABLED == STD_ON)
/*CANNM062_Conf*/
extern CONST(uint8, CANNM_CONST)CanNm_PnFilterMask[CANNM_PN_INFO_LENGTH];
#endif

#define CANNM_STOP_SEC_CONST_8
#include <MemMap.h>

#define CANNM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>
extern CONST(CanNm_ChanConfigType, CANNM_CONST)
   CanNm_ChanConfig[CANNM_NUMBER_OF_CHANNELS];

extern CONST(NetworkHandleType, CANNM_CONST)
   CanNm_IndexFromNmChannelHandle[CANNM_INDEXFROMNMCHANNELHANDLE_NUM];

#define CANNM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*=====================[internal constants declarations]====================*/

/*=======================[external data declarations]=======================*/

#define CANNM_START_SEC_VAR_FAST_NO_INIT_UNSPECIFIED
#include <MemMap.h>

/** \brief Declaration of the extern variable CanNm_ChanStatus */
extern VAR(CanNm_ChanStatusType, CANNM_VAR_FAST)
   CanNm_ChanStatus[CANNM_NUMBER_OF_CHANNELS];

#define CANNM_STOP_SEC_VAR_FAST_NO_INIT_UNSPECIFIED
#include <MemMap.h>

#endif /* (!defined CANNM_INT_H) */

/*==============================[end of file]===============================*/
