/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#ifndef CANNM_HSMCFG_H
#define CANNM_HSMCFG_H

/* configuration options for the hsm */

/*==================[inclusions]============================================*/

#include <CanNm_Cfg.h> /* number of state machines */

/*==================[macros]================================================*/

/** \brief Enable multiple Hsm models in one module (different statecharts) */
#define CANNM_HSM_HSM_MULTI_ENABLED       STD_OFF

#if (CANNM_NUMBER_OF_CHANNELS > 1U)
/** \brief Enable multiple instances of the same statemachine */
#define CANNM_HSM_INST_MULTI_ENABLED     STD_ON
#else
#define CANNM_HSM_INST_MULTI_ENABLED     STD_OFF
#endif

/** \brief Number of instances of hsm CanNm
 *
 * Definition is only needed if CANNM_HSM_INST_MULTI_ENABLED==STD_ON */
#define CANNM_HSM_CANNM_NUM_INST          CANNM_NUMBER_OF_CHANNELS


/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif
/*==================[end of file]===========================================*/
