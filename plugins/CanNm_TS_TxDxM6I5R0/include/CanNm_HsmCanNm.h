/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#ifndef CANNM_HSMCANNM_H
#define CANNM_HSMCANNM_H

/* Public interface for the CanNm
 * state machine.
 *
 * This file defines the public symbols and functions to use the
 * CanNm state machine.
 */

/*==================[inclusions]============================================*/

#include <Std_Types.h>                            /* AUTOSAR standard types */

/* include state machine driver interface */
#include <CanNm_Hsm.h>

/*==================[macros]================================================*/

/* Events/signal defined in the CanNm state machine. */
#define CANNM_HSM_CANNM_EV_NETWORK_START 0U
#define CANNM_HSM_CANNM_EV_COM_CONTROL 1U
#define CANNM_HSM_CANNM_EV_TX_CONFIRMATION 2U
#define CANNM_HSM_CANNM_EV_TX_TIMEOUT 3U
#define CANNM_HSM_CANNM_EV_MSG_CYCLE_TIMEOUT 4U
#define CANNM_HSM_CANNM_EV_NM_TIMEOUT 5U
#define CANNM_HSM_CANNM_EV_REPEAT_MESSAGE_REASON 6U
#define CANNM_HSM_CANNM_EV_RMS_TIMEOUT 7U
#define CANNM_HSM_CANNM_EV_NET_REQ_STATUS_CHANGED 8U
#define CANNM_HSM_CANNM_EV_RX_INDICATION 9U
#define CANNM_HSM_CANNM_EV_UNI_TIMEOUT 10U

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

#define CANNM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief Initialization data for the statechart statechart CanNm */
extern CONST(CanNm_HsmStatechartType, CANNM_CONST)
 CanNm_HsmScCanNm;

#define CANNM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* defined CANNM_HSMCANNM_H */
/*==================[end of file]===========================================*/
