/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#ifndef CANNM_HSMCANNMINT_H
#define CANNM_HSMCANNMINT_H

/* Internal interface for the CanNm state
 * machine.
 *
 * This header must only be included by
 * CanNm_HsmCanNmData.c and CanNm_HsmCanNmFnct.c.
 */

/* CHECK: RULE 301 OFF (this file is generated, some lines may be longer then
 * 100 characters) */

/*==================[inclusions]============================================*/

#include <Std_Types.h>                            /* AUTOSAR standard types */

/* include state machine driver interface */
#include <CanNm_Hsm.h>

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/* *** State indices and IDs for states *** */
typedef enum
{
  CANNM_HSM_CANNM_SIDX_TOP,
  CANNM_HSM_CANNM_SIDX_BUSSLEEPMODE,
  CANNM_HSM_CANNM_SIDX_NETWORKMODE,
  CANNM_HSM_CANNM_SIDX_READYSLEEPSTATE,
  CANNM_HSM_CANNM_SIDX_READYSLEEPREMOTEACTIVITY,
  CANNM_HSM_CANNM_SIDX_READYSLEEPREMOTESLEEP,
  CANNM_HSM_CANNM_SIDX_SENDINGSUBMODE,
  CANNM_HSM_CANNM_SIDX_NORMALOPERATIONSTATE,
  CANNM_HSM_CANNM_SIDX_NORMALOPERATIONREMOTEACTIVITY,
  CANNM_HSM_CANNM_SIDX_NORMALOPERATIONREMOTESLEEP,
  CANNM_HSM_CANNM_SIDX_REPEATMESSAGESTATE,
  CANNM_HSM_CANNM_SIDX_PREPAREBUSSLEEPMODE,
  CANNM_HSM_CANNM_NO_OF_STATES
} CanNm_HsmCanNmStateIdType;
/* typedefed type only used for debugging */

/* *** IDs for entry/axit and transition actions *** */
typedef enum
{
  CANNM_HSM_CANNM_AIDX_TOP_ENTRY,
  CANNM_HSM_CANNM_AIDX_BUSSLEEPMODE_ENTRY,
  CANNM_HSM_CANNM_AIDX_BUSSLEEPMODE_ACTION_1,
  CANNM_HSM_CANNM_AIDX_NETWORKMODE_ENTRY,
  CANNM_HSM_CANNM_AIDX_NETWORKMODE_EXIT,
  CANNM_HSM_CANNM_AIDX_NETWORKMODE_ACTION_1,
  CANNM_HSM_CANNM_AIDX_NETWORKMODE_ACTION_2,
  CANNM_HSM_CANNM_AIDX_NETWORKMODE_ACTION_3,
  CANNM_HSM_CANNM_AIDX_READYSLEEPSTATE_ENTRY,
  CANNM_HSM_CANNM_AIDX_READYSLEEPREMOTESLEEP_ACTION_2,
  CANNM_HSM_CANNM_AIDX_READYSLEEPREMOTESLEEP_ACTION_3,
  CANNM_HSM_CANNM_AIDX_SENDINGSUBMODE_ENTRY,
  CANNM_HSM_CANNM_AIDX_SENDINGSUBMODE_EXIT,
  CANNM_HSM_CANNM_AIDX_SENDINGSUBMODE_ACTION_1,
  CANNM_HSM_CANNM_AIDX_SENDINGSUBMODE_ACTION_2,
  CANNM_HSM_CANNM_AIDX_SENDINGSUBMODE_ACTION_3,
  CANNM_HSM_CANNM_AIDX_NORMALOPERATIONSTATE_ENTRY,
  CANNM_HSM_CANNM_AIDX_NORMALOPERATIONSTATE_EXIT,
  CANNM_HSM_CANNM_AIDX_NORMALOPERATIONREMOTEACTIVITY_ENTRY,
  CANNM_HSM_CANNM_AIDX_NORMALOPERATIONREMOTEACTIVITY_ACTION_1,
  CANNM_HSM_CANNM_AIDX_NORMALOPERATIONREMOTESLEEP_ENTRY,
  CANNM_HSM_CANNM_AIDX_NORMALOPERATIONREMOTESLEEP_ACTION_2,
  CANNM_HSM_CANNM_AIDX_NORMALOPERATIONREMOTESLEEP_ACTION_3,
  CANNM_HSM_CANNM_AIDX_REPEATMESSAGESTATE_ENTRY,
  CANNM_HSM_CANNM_AIDX_REPEATMESSAGESTATE_EXIT,
  CANNM_HSM_CANNM_AIDX_PREPAREBUSSLEEPMODE_ENTRY,
  CANNM_HSM_CANNM_AIDX_PREPAREBUSSLEEPMODE_ACTION_1,
  CANNM_HSM_CANNM_AIDX_PREPAREBUSSLEEPMODE_ACTION_2,
  CANNM_HSM_CANNM_NO_OF_ACTIONS
} CanNm_HsmCanNmActionIdType;
/* typedefed type only used for debugging */

/* *** IDs for guards *** */
typedef enum
{
  CANNM_HSM_CANNM_GIDX_BUSSLEEPMODE_GUARD_3,
  CANNM_HSM_CANNM_GIDX_NETWORKMODE_GUARD_4,
  CANNM_HSM_CANNM_GIDX_READYSLEEPSTATE_GUARD_1,
  CANNM_HSM_CANNM_GIDX_READYSLEEPREMOTEACTIVITY_GUARD_1,
  CANNM_HSM_CANNM_GIDX_READYSLEEPREMOTESLEEP_GUARD_1,
  CANNM_HSM_CANNM_GIDX_NORMALOPERATIONREMOTEACTIVITY_GUARD_2,
  CANNM_HSM_CANNM_GIDX_NORMALOPERATIONREMOTEACTIVITY_GUARD_4,
  CANNM_HSM_CANNM_GIDX_NORMALOPERATIONREMOTESLEEP_GUARD_1,
  CANNM_HSM_CANNM_GIDX_REPEATMESSAGESTATE_GUARD_1,
  CANNM_HSM_CANNM_GIDX_REPEATMESSAGESTATE_GUARD_2,
  CANNM_HSM_CANNM_GIDX_PREPAREBUSSLEEPMODE_GUARD_1,
  CANNM_HSM_CANNM_NO_OF_GUARDS
} CanNm_HsmCanNmGuardIdType;
/* typedefed type only used for debugging */

/*==================[external function declarations]========================*/

#define CANNM_START_SEC_CODE
#include <MemMap.h>

/* function declarations of state entry, exit, guard, action functions
 * defined in CanNm_fnct.c */

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfTOPEntry(
  CANNM_PDL_SF(const uint8 instIdx));

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfBusSleepModeEntry(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'Nm_NetworkStartIndication(); #ifdef (...) Nm_PduRxIndication; #endif #if(Det == STD_ON);DET_REPORT_ERROR();#endif --> CANNM127 CANNM037 CANNM336 CANNM337' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfBusSleepModeAction1(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements guard 'NetworkRequested==TRUE' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfBusSleepModeGuard3(
  CANNM_PDL_SF(const uint8 instIdx));

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNetworkModeEntry(
  CANNM_PDL_SF(const uint8 instIdx));
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNetworkModeExit(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action '#if (CANNM_COM_CONTROL_ENABLED == STD_ON) HandleComControl()' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNetworkModeAction1(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'HandleRxIndicationCommon()' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNetworkModeAction2(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'HandleTxConfirmation();' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNetworkModeAction3(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements guard 'PnHandleMultipleNetworkRequests==TRUE && NetworkRequested==TRUE' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfNetworkModeGuard4(
  CANNM_PDL_SF(const uint8 instIdx));

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfReadySleepStateEntry(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements guard 'NmTimer == 0' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfReadySleepStateGuard1(
  CANNM_PDL_SF(const uint8 instIdx));

/* implements guard 'PnHandleMultipleNetworkRequests==FALSE && NetworkRequested==TRUE' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfReadySleepRemoteActivityGuard1(
  CANNM_PDL_SF(const uint8 instIdx));

/* implements guard 'PnHandleMultipleNetworkRequests==FALSE && NetworkRequested==TRUE' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfReadySleepRemoteSleepGuard1(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'Nm_RemoteSleepCancellation();' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfReadySleepRemoteSleepAction2(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'Nm_RemoteSleepCancellation(); HandleRxIndicationCommon();' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfReadySleepRemoteSleepAction3(
  CANNM_PDL_SF(const uint8 instIdx));

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfSendingSubModeEntry(
  CANNM_PDL_SF(const uint8 instIdx));
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfSendingSubModeExit(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action '#if ((PASSIVE_MODE == false) && (IMM_TRANCONF_ENBLD == false)) Nm_TxTimeoutException(); #if (CANNM_PN_ENABLED == STD_ON) CanSM_TxTimeoutException();' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfSendingSubModeAction1(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'HandleMsgCycleTimeout() --> CANNM032' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfSendingSubModeAction2(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'if (NmTimeout == 0) {HandleNmTimeout();}' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfSendingSubModeAction3(
  CANNM_PDL_SF(const uint8 instIdx));

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationStateEntry(
  CANNM_PDL_SF(const uint8 instIdx));
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationStateExit(
  CANNM_PDL_SF(const uint8 instIdx));

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationRemoteActivityEntry(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'HandleRxIndication_NOState_RA();' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationRemoteActivityAction1(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements guard 'NetworkRequested==FALSE' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationRemoteActivityGuard2(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements guard '(!(ChanStatus & CANNM_COM_DISABLED)) && (UniTimer == 0)' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationRemoteActivityGuard4(
  CANNM_PDL_SF(const uint8 instIdx));

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationRemoteSleepEntry(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements guard 'NetworkRequested==FALSE' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationRemoteSleepGuard1(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'Nm_RemoteSleepCancellation();' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationRemoteSleepAction2(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action 'HandleRxIndication_NOState() RSI timer is started by entry action /' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfNormalOperationRemoteSleepAction3(
  CANNM_PDL_SF(const uint8 instIdx));

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfRepeatMessageStateEntry(
  CANNM_PDL_SF(const uint8 instIdx));
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfRepeatMessageStateExit(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements guard '(NetworkRequested==FALSE) && (RMS timer == 0))' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfRepeatMessageStateGuard1(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements guard '((NetworkRequested==TRUE) && (RMS timer == 0))' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfRepeatMessageStateGuard2(
  CANNM_PDL_SF(const uint8 instIdx));

extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfPrepareBusSleepModeEntry(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements guard 'NetworkRequested==TRUE' */
extern FUNC(boolean, CANNM_CODE)
 CanNm_HsmCanNmSfPrepareBusSleepModeGuard1(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action '#if (CANNM_IMM._RESTART == true) <Send NM message> #endif' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfPrepareBusSleepModeAction1(
  CANNM_PDL_SF(const uint8 instIdx));
/* implements action '#ifdef (...) Nm_PduRxIndication(); #endif --> CANNM037' */
extern FUNC(void, CANNM_CODE)
 CanNm_HsmCanNmSfPrepareBusSleepModeAction2(
  CANNM_PDL_SF(const uint8 instIdx));


#define CANNM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* defined CANNM_HSMCANNMINT_H */
/*==================[end of file]===========================================*/
