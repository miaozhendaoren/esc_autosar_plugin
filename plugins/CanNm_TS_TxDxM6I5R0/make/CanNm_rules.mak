# \file
#
# \brief AUTOSAR CanNm
#
# This file contains the implementation of the AUTOSAR
# module CanNm.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

#################################################################
# REGISTRY

CanNm_src_FILES      := \
    $(CanNm_CORE_PATH)\src\CanNm.c \
    $(CanNm_CORE_PATH)\src\CanNm_Hsm.c \
    $(CanNm_CORE_PATH)\src\CanNm_HsmCanNmData.c \
    $(CanNm_CORE_PATH)\src\CanNm_HsmCanNmFnct.c \
    $(CanNm_OUTPUT_PATH)\src\CanNm_Cfg.c

LIBRARIES_TO_BUILD   += CanNm_src

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES
