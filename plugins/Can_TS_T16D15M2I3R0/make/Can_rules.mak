# \file
#
# \brief AUTOSAR Can
#
# This file contains the implementation of the AUTOSAR
# module Can.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Can_src

Can_src_FILES       += $(Can_CORE_PATH)\src\Can_17_MCanP.c
Can_src_FILES       += $(Can_CORE_PATH)\src\Can_17_MCanP_Irq.c
Can_src_FILES       += $(Can_OUTPUT_PATH)\src\Can_17_MCanP_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Can_src_FILES :=
endif
