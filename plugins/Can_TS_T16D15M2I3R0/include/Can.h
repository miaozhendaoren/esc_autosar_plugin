/**
 * \file
 *
 * \brief Autosar Can
 *
 * This file contains the implementation of the Autosar
 * module Can.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* EB: Workaround for deviation of CanIf only a single CAN Driver is supported *
 * Tracked in ticket ASCPD-1 */

#if (!defined CAN_H)
#define CAN_H

#include <Can_17_MCanP.h>

/* Published parameters */
#define CAN_VENDOR_ID                       CAN_17_MCANP_VENDOR_ID
#define CAN_MODULE_ID                       CAN_17_MCANP_MODULE_ID
#define CAN_AS_VERSION                      CAN_17_MCANP_AS_VERSION
#define CAN_AR_RELEASE_MAJOR_VERSION        CAN_17_MCANP_AR_RELEASE_MAJOR_VERSION
#define CAN_AR_RELEASE_MINOR_VERSION        CAN_17_MCANP_AR_RELEASE_MINOR_VERSION
#define CAN_AR_RELEASE_REVISION_VERSION     CAN_17_MCANP_AR_RELEASE_REVISION_VERSION
#define CAN_SW_MAJOR_VERSION                CAN_17_MCANP_SW_MAJOR_VERSION
#define CAN_SW_MINOR_VERSION                CAN_17_MCANP_SW_MINOR_VERSION
#define CAN_SW_PATCH_VERSION                CAN_17_MCANP_SW_PATCH_VERSION

/* Global Type Definitions */
typedef Can_17_MCanP_ConfigType                       Can_ConfigType;
typedef Can_17_MCanP_ControllerBaudrateConfigType     Can_ControllerBaudrateConfigType;

/* API Functions */
#define Can_Init                          Can_17_MCanP_Init
#define Can_GetVersionInfo                Can_17_MCanP_GetVersionInfo
#define Can_CheckBaudrate                 Can_17_MCanP_CheckBaudrate
#define Can_ChangeBaudrate                Can_17_MCanP_ChangeBaudrate
#define Can_SetControllerMode             Can_17_MCanP_SetControllerMode
#define Can_DisableControllerInterrupts   Can_17_MCanP_DisableControllerInterrupts
#define Can_EnableControllerInterrupts    Can_17_MCanP_EnableControllerInterrupts
#define Can_CheckWakeup                   Can_17_MCanP_CheckWakeup
#define Can_Write                         Can_17_MCanP_Write
#define Can_MainFunction_Write            Can_17_MCanP_MainFunction_Write
#define Can_MainFunction_Read             Can_17_MCanP_MainFunction_Read
#define Can_MainFunction_BusOff           Can_17_MCanP_MainFunction_BusOff
#define Can_MainFunction_Wakeup           Can_17_MCanP_MainFunction_Wakeup
#define Can_MainFunction_Mode             Can_17_MCanP_MainFunction_Mode
#define Can_IsrBusOffHandler              Can_17_MCanP_IsrBusOffHandler
#define Can_IsrReceiveHandler             Can_17_MCanP_IsrReceiveHandler
#define Can_IsrTransmitHandler            Can_17_MCanP_IsrTransmitHandler

#endif /* #if (!defined CAN_H) */
