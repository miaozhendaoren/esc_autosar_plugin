/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Spi_Irq.c $                                                **
**                                                                           **
**  $CC VERSION : \main\22 $                                                 **
**                                                                           **
**  $DATE       : 2014-08-20 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains                                         **
**                - ISRs of SPI Handler driver.                              **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: Yes                                     **
**                                                                           **
******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Inclusion of Platform_Types.h and Compiler.h */
#include "Std_Types.h"

/* MCAL driver utility functions for setting interrupt priority initialization 
    and interrupt category */
#include "Spi_Irq.h"
/* Global functions like Set/Reset END INIT protection bit, 
  Enable/Disable interrupts, Automic write function */
#include "Mcal.h"
/* Own header file, this includes own configuration file also */
#include "Spi.h"
/*******************************************************************************
**                      Imported Compiler Switch Checks                       **
*******************************************************************************/

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
#define SPI_START_SEC_CODE
#include "MemMap.h"
/******************************************************************************
** Syntax : void OS_ISR_QSPI4ERR_ISR(void)                                     **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for Error interrupt of QSPIx                     **
**                                                                           **
*****************************************************************************/
#if (IRQ_QSPI0ERR_EXIST == STD_ON)
#if ((SPI_QSPI0_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
/* This node is for QSPI0_ESRC */
#if ((IRQ_QSPI0_ERR_PRIO > 0) || (IRQ_QSPI0_ERR_CAT == IRQ_CAT23))
#if ((IRQ_QSPI0_ERR_PRIO > 0) && (IRQ_QSPI0_ERR_CAT == IRQ_CAT1))
void OS_ISR_QSPI0ERR_ISR (void)
#elif IRQ_QSPI0_ERR_CAT == IRQ_CAT23
ISR(QSPI0ERR_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiError(SPI_QSPI0_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI1ERR_EXIST == STD_ON)
#if ((SPI_QSPI1_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
/* This node is for QSPI1_ESRC */
#if ((IRQ_QSPI1_ERR_PRIO > 0) || (IRQ_QSPI1_ERR_CAT == IRQ_CAT23))
#if ((IRQ_QSPI1_ERR_PRIO > 0) && (IRQ_QSPI1_ERR_CAT == IRQ_CAT1))
void OS_ISR_QSPI1ERR_ISR (void)
#elif IRQ_QSPI1_ERR_CAT == IRQ_CAT23
ISR(QSPI1ERR_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPIx Interrupt funtion*/                                              
  Spi_IsrQspiError(SPI_QSPI1_INDEX);  

}
#endif
#endif
#endif

#if (IRQ_QSPI2ERR_EXIST == STD_ON)
#if ((SPI_QSPI2_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
/* This node is for QSPI2_ESRC */
#if ((IRQ_QSPI2_ERR_PRIO > 0) || (IRQ_QSPI2_ERR_CAT == IRQ_CAT23))
#if ((IRQ_QSPI2_ERR_PRIO > 0) && (IRQ_QSPI2_ERR_CAT == IRQ_CAT1))
void OS_ISR_QSPI2ERR_ISR (void)
#elif IRQ_QSPI2_ERR_CAT == IRQ_CAT23
ISR(QSPI2ERR_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPIx Interrupt funtion*/                                              
  Spi_IsrQspiError(SPI_QSPI2_INDEX);  

}
#endif
#endif
#endif

#if (IRQ_QSPI3ERR_EXIST == STD_ON)
#if ((SPI_QSPI3_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
/* This node is for QSPI3_ESRC */
#if ((IRQ_QSPI3_ERR_PRIO > 0) || (IRQ_QSPI3_ERR_CAT == IRQ_CAT23))
#if ((IRQ_QSPI3_ERR_PRIO > 0) && (IRQ_QSPI3_ERR_CAT == IRQ_CAT1))
void OS_ISR_QSPI3ERR_ISR (void)
#elif IRQ_QSPI3_ERR_CAT == IRQ_CAT23
ISR(QSPI3ERR_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPIx Interrupt funtion*/                                              
  Spi_IsrQspiError(SPI_QSPI3_INDEX);  

}
#endif
#endif
#endif

#if (IRQ_QSPI4ERR_EXIST == STD_ON)
#if ((SPI_QSPI4_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
/* This node is for QSPI4_ESRC */
#if ((IRQ_QSPI4_ERR_PRIO > 0) || (IRQ_QSPI4_ERR_CAT == IRQ_CAT23))
#if ((IRQ_QSPI4_ERR_PRIO > 0) && (IRQ_QSPI4_ERR_CAT == IRQ_CAT1))
void OS_ISR_QSPIXERR_ISR (void)
#elif IRQ_QSPI4_ERR_CAT == IRQ_CAT23
ISR(QSPIXERR_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPIx Interrupt funtion*/                                              
  Spi_IsrQspiError(SPI_QSPI4_INDEX);  

}
#endif
#endif
#endif

#if (IRQ_QSPI5ERR_EXIST == STD_ON)
#if ((SPI_QSPI5_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
/* This node is for QSPI5_ESRC */
#if ((IRQ_QSPI5_ERR_PRIO > 0) || (IRQ_QSPI5_ERR_CAT == IRQ_CAT23))
#if ((IRQ_QSPI5_ERR_PRIO > 0) && (IRQ_QSPI5_ERR_CAT == IRQ_CAT1))
void OS_ISR_QSPI5ERR_ISR (void)
#elif IRQ_QSPI5_ERR_CAT == IRQ_CAT23
ISR(QSPI5ERR_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPIx Interrupt funtion*/                                              
  Spi_IsrQspiError(SPI_QSPI5_INDEX);  

}
#endif
#endif
#endif


/******************************************************************************
** Syntax : void OS_ISR_QSPIXPT_ISR(void)                                     **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for Phase transition interrupt (PT2) used by QSPIx    **
**                                                                           **
*****************************************************************************/
#if (IRQ_QSPI0PT_EXIST == STD_ON)
#if ((SPI_QSPI0_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI0_PT_PRIO > 0) || (IRQ_QSPI0_PT_CAT == IRQ_CAT23))
#if ((IRQ_QSPI0_PT_PRIO > 0) && (IRQ_QSPI0_PT_CAT == IRQ_CAT1))
void OS_ISR_QSPI0PT_ISR (void)
#elif IRQ_QSPI0_PT_CAT == IRQ_CAT23
ISR(QSPI0PT_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiPt(SPI_QSPI0_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI1PT_EXIST == STD_ON)
#if ((SPI_QSPI1_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI1_PT_PRIO > 0) || (IRQ_QSPI1_PT_CAT == IRQ_CAT23))
#if ((IRQ_QSPI1_PT_PRIO > 0) && (IRQ_QSPI1_PT_CAT == IRQ_CAT1))
void OS_ISR_QSPI1PT_ISR (void)
#elif IRQ_QSPI1_PT_CAT == IRQ_CAT23
ISR(QSPI1PT_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiPt(SPI_QSPI1_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI2PT_EXIST == STD_ON)
#if ((SPI_QSPI2_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI2_PT_PRIO > 0) || (IRQ_QSPI2_PT_CAT == IRQ_CAT23))
#if ((IRQ_QSPI2_PT_PRIO > 0) && (IRQ_QSPI2_PT_CAT == IRQ_CAT1))
void OS_ISR_QSPI2PT_ISR (void)
#elif IRQ_QSPI2_PT_CAT == IRQ_CAT23
ISR(QSPI2PT_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiPt(SPI_QSPI2_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI3PT_EXIST == STD_ON)
#if ((SPI_QSPI3_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI3_PT_PRIO > 0) || (IRQ_QSPI3_PT_CAT == IRQ_CAT23))
#if ((IRQ_QSPI3_PT_PRIO > 0) && (IRQ_QSPI3_PT_CAT == IRQ_CAT1))
void OS_ISR_QSPI3PT_ISR (void)
#elif IRQ_QSPI3_PT_CAT == IRQ_CAT23
ISR(QSPI3PT_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiPt(SPI_QSPI3_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI4PT_EXIST == STD_ON)
#if ((SPI_QSPI4_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI4_PT_PRIO > 0) || (IRQ_QSPI4_PT_CAT == IRQ_CAT23))
#if ((IRQ_QSPI4_PT_PRIO > 0) && (IRQ_QSPI4_PT_CAT == IRQ_CAT1))
void OS_ISR_QSPI4PT_ISR (void)
#elif IRQ_QSPI4_PT_CAT == IRQ_CAT23
ISR(QSPI4PT_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiPt(SPI_QSPI4_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI5PT_EXIST == STD_ON)
#if ((SPI_QSPI5_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI5_PT_PRIO > 0) || (IRQ_QSPI5_PT_CAT == IRQ_CAT23))
#if ((IRQ_QSPI5_PT_PRIO > 0) && (IRQ_QSPI5_PT_CAT == IRQ_CAT1))
void OS_ISR_QSPI5PT_ISR (void)
#elif IRQ_QSPI5_PT_CAT == IRQ_CAT23
ISR(QSPI5PT_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiPt(SPI_QSPI5_INDEX);  
}
#endif
#endif
#endif

/******************************************************************************
** Syntax : void OS_ISR_QSPIXUD_ISR(void)                                     **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for User interrupt (PT1) used by QSPIx    **
**                                                                           **
*****************************************************************************/
#if (IRQ_QSPI0UD_EXIST == STD_ON)
#if ((SPI_QSPI0_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI0_UD_PRIO > 0) || (IRQ_QSPI0_UD_CAT == IRQ_CAT23))
#if ((IRQ_QSPI0_UD_PRIO > 0) && (IRQ_QSPI0_UD_CAT == IRQ_CAT1))
void OS_ISR_QSPI0UD_ISR (void)
#elif IRQ_QSPI0_UD_CAT == IRQ_CAT23
ISR(QSPI0UD_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiUsr(SPI_QSPI0_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI1UD_EXIST == STD_ON)
#if ((SPI_QSPI1_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI1_UD_PRIO > 0) || (IRQ_QSPI1_UD_CAT == IRQ_CAT23))
#if ((IRQ_QSPI1_UD_PRIO > 0) && (IRQ_QSPI1_UD_CAT == IRQ_CAT1))
void OS_ISR_QSPI1UD_ISR (void)
#elif IRQ_QSPI1_UD_CAT == IRQ_CAT23
ISR(QSPI1UD_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiUsr(SPI_QSPI1_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI2UD_EXIST == STD_ON)
#if ((SPI_QSPI2_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI2_UD_PRIO > 0) || (IRQ_QSPI2_UD_CAT == IRQ_CAT23))
#if ((IRQ_QSPI2_UD_PRIO > 0) && (IRQ_QSPI2_UD_CAT == IRQ_CAT1))
void OS_ISR_QSPI2UD_ISR (void)
#elif IRQ_QSPI2_UD_CAT == IRQ_CAT23
ISR(QSPI2UD_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiUsr(SPI_QSPI2_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI3UD_EXIST == STD_ON)
#if ((SPI_QSPI3_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI3_UD_PRIO > 0) || (IRQ_QSPI3_UD_CAT == IRQ_CAT23))
#if ((IRQ_QSPI3_UD_PRIO > 0) && (IRQ_QSPI3_UD_CAT == IRQ_CAT1))
void OS_ISR_QSPI3UD_ISR (void)
#elif IRQ_QSPI3_UD_CAT == IRQ_CAT23
ISR(QSPI3UD_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiUsr(SPI_QSPI3_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI4UD_EXIST == STD_ON)
#if ((SPI_QSPI4_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI4_UD_PRIO > 0) || (IRQ_QSPI4_UD_CAT == IRQ_CAT23))
#if ((IRQ_QSPI4_UD_PRIO > 0) && (IRQ_QSPI4_UD_CAT == IRQ_CAT1))
void OS_ISR_QSPI4UD_ISR (void)
#elif IRQ_QSPI4_UD_CAT == IRQ_CAT23
ISR(QSPI4UD_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiUsr(SPI_QSPI4_INDEX);  
}
#endif
#endif
#endif

#if (IRQ_QSPI5UD_EXIST == STD_ON)
#if ((SPI_QSPI5_USED == STD_ON) && (SPI_LEVEL_DELIVERED != 0))
#if ((IRQ_QSPI5_UD_PRIO > 0) || (IRQ_QSPI5_UD_CAT == IRQ_CAT23))
#if ((IRQ_QSPI5_UD_PRIO > 0) && (IRQ_QSPI5_UD_CAT == IRQ_CAT1))
void OS_ISR_QSPI5UD_ISR (void)
#elif IRQ_QSPI5_UD_CAT == IRQ_CAT23
ISR(QSPI5UD_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

  /* Call QSPI0 Interrupt funtion*/                                              
  Spi_IsrQspiUsr(SPI_QSPI5_INDEX);  
}
#endif
#endif
#endif

#define SPI_STOP_SEC_CODE
#include "MemMap.h"


