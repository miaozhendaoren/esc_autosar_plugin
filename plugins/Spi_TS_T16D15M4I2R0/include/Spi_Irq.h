
#ifndef QSPI_IRQ_H
#define QSPI_IRQ_H

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Std_Types.h"                                                        

#ifdef OS_KERNEL_TYPE                                                           
#include <Os.h>        /* OS interrupt services */                              
#endif


/*******************************************************************************
**                      Public Macro Definitions                              **
*******************************************************************************/

/************************* interrupt CATEGORY *********************************/

#ifdef IRQ_CAT1
#if (IRQ_CAT1 != 1)
#error IRQ_CAT1 already defined with a wrong value
#endif
#else
#define IRQ_CAT1                    (1)
#endif

#ifdef IRQ_CAT23
#if (IRQ_CAT23 != 2)
#error IRQ_CAT23 already defined with a wrong value
#endif
#else
#define IRQ_CAT23                    (2)
#endif



/* The name of the ISRs shall be the same name than the ISR       *
 * functions provided by Infineon                                 */

/*************************QSPI0ERR_ISR*********************************/          


#ifdef QSPI0ERR_ISR
#define IRQ_QSPI0ERR_EXIST    STD_ON
#define IRQ_QSPI0_ERR_PRIO    QSPI0ERR_ISR_ISR_LEVEL
#define IRQ_QSPI0_ERR_CAT     QSPI0ERR_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI0ERR_EXIST    STD_OFF
#endif

/*************************QSPI0PT_ISR*********************************/          

#ifdef QSPI0PT_ISR
#define IRQ_QSPI0PT_EXIST    STD_ON
#define IRQ_QSPI0_PT_PRIO    QSPI0PT_ISR_ISR_LEVEL
#define IRQ_QSPI0_PT_CAT     QSPI0PT_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI0PT_EXIST    STD_OFF
#endif

/*************************QSPI0UD_ISR*********************************/          

#ifdef QSPI0UD_ISR
#define IRQ_QSPI0UD_EXIST    STD_ON
#define IRQ_QSPI0_UD_PRIO    QSPI0UD_ISR_ISR_LEVEL
#define IRQ_QSPI0_UD_CAT     QSPI0UD_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI0UD_EXIST    STD_OFF
#endif

/*************************QSPI1ERR_ISR*********************************/          

#ifdef QSPI1ERR_ISR
#define IRQ_QSPI1ERR_EXIST    STD_ON
#define IRQ_QSPI1_ERR_PRIO    QSPI1ERR_ISR_ISR_LEVEL
#define IRQ_QSPI1_ERR_CAT     QSPI1ERR_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI1ERR_EXIST    STD_OFF
#endif

/*************************QSPI1UD_ISR*********************************/          

#ifdef QSPI1UD_ISR
#define IRQ_QSPI1UD_EXIST    STD_ON
#define IRQ_QSPI1_UD_PRIO    QSPI1UD_ISR_ISR_LEVEL
#define IRQ_QSPI1_UD_CAT     QSPI1UD_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI1UD_EXIST    STD_OFF
#endif


/*************************QSPI1PT_ISR*********************************/          

#ifdef QSPI1PT_ISR
#define IRQ_QSPI1PT_EXIST    STD_ON
#define IRQ_QSPI1_PT_PRIO    QSPI1PT_ISR_ISR_LEVEL
#define IRQ_QSPI1_PT_CAT     QSPI1PT_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI1PT_EXIST    STD_OFF
#endif

/*************************QSPI2ERR_ISR*********************************/          

#ifdef QSPI2ERR_ISR
#define IRQ_QSPI2ERR_EXIST    STD_ON
#define IRQ_QSPI2_ERR_PRIO    QSPI2ERR_ISR_ISR_LEVEL
#define IRQ_QSPI2_ERR_CAT     QSPI2ERR_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI2ERR_EXIST    STD_OFF
#endif

/*************************QSPI2PT_ISR*********************************/          

#ifdef QSPI2PT_ISR
#define IRQ_QSPI2PT_EXIST    STD_ON
#define IRQ_QSPI2_PT_PRIO    QSPI2PT_ISR_ISR_LEVEL
#define IRQ_QSPI2_PT_CAT     QSPI2PT_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI2PT_EXIST    STD_OFF
#endif

/*************************QSPI2UD_ISR*********************************/          

#ifdef QSPI2UD_ISR
#define IRQ_QSPI2UD_EXIST    STD_ON
#define IRQ_QSPI2_UD_PRIO    QSPI2UD_ISR_ISR_LEVEL
#define IRQ_QSPI2_UD_CAT     QSPI2UD_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI2UD_EXIST    STD_OFF
#endif


/*************************QSPI3ERR_ISR*********************************/          

#ifdef QSPI3ERR_ISR
#define IRQ_QSPI3ERR_EXIST    STD_ON
#define IRQ_QSPI3_ERR_PRIO    QSPI3ERR_ISR_ISR_LEVEL
#define IRQ_QSPI3_ERR_CAT     QSPI3ERR_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI3ERR_EXIST    STD_OFF
#endif

/*************************QSPI3UD_ISR*********************************/          

#ifdef QSPI3UD_ISR
#define IRQ_QSPI3UD_EXIST    STD_ON
#define IRQ_QSPI3_UD_PRIO    QSPI3UD_ISR_ISR_LEVEL
#define IRQ_QSPI3_UD_CAT     QSPI3UD_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI3UD_EXIST    STD_OFF
#endif

/*************************QSPI3PT_ISR*********************************/          

#ifdef QSPI3PT_ISR
#define IRQ_QSPI3PT_EXIST    STD_ON
#define IRQ_QSPI3_PT_PRIO    QSPI3PT_ISR_ISR_LEVEL
#define IRQ_QSPI3_PT_CAT     QSPI3PT_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI3PT_EXIST    STD_OFF
#endif


/*************************QSPIXERR_ISR*********************************/          

#ifdef QSPIXERR_ISR
#define IRQ_QSPI4ERR_EXIST    STD_ON
#define IRQ_QSPI4_ERR_PRIO    QSPIXERR_ISR_ISR_LEVEL
#define IRQ_QSPI4_ERR_CAT     QSPIXERR_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI4ERR_EXIST    STD_OFF
#endif


/*************************QSPI4PT_ISR*********************************/          

#ifdef QSPI4PT_ISR
#define IRQ_QSPI4PT_EXIST    STD_ON
#define IRQ_QSPI4_PT_PRIO    QSPI4PT_ISR_ISR_LEVEL
#define IRQ_QSPI4_PT_CAT     QSPI4PT_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI4PT_EXIST    STD_OFF
#endif

/*************************QSPI4UD_ISR*********************************/          

#ifdef QSPI4UD_ISR
#define IRQ_QSPI4UD_EXIST    STD_ON
#define IRQ_QSPI4_UD_PRIO    QSPI4UD_ISR_ISR_LEVEL
#define IRQ_QSPI4_UD_CAT     QSPI4UD_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI4UD_EXIST    STD_OFF
#endif

/*************************QSPI5ERR_ISR*********************************/          

#ifdef QSPI5ERR_ISR
#define IRQ_QSPI5ERR_EXIST    STD_ON
#define IRQ_QSPI5_ERR_PRIO    QSPI5ERR_ISR_ISR_LEVEL
#define IRQ_QSPI5_ERR_CAT     QSPI5ERR_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI5ERR_EXIST    STD_OFF
#endif

/*************************QSPI5UD_ISR*********************************/          

#ifdef QSPI5UD_ISR
#define IRQ_QSPI5UD_EXIST    STD_ON
#define IRQ_QSPI5_UD_PRIO    QSPI5UD_ISR_ISR_LEVEL
#define IRQ_QSPI5_UD_CAT     QSPI5UD_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI5UD_EXIST    STD_OFF
#endif

/*************************QSPI5PT_ISR*********************************/          

#ifdef QSPI5PT_ISR
#define IRQ_QSPI5PT_EXIST    STD_ON
#define IRQ_QSPI5_PT_PRIO    QSPI5PT_ISR_ISR_LEVEL
#define SPI_QSPI15_USED      QSPI5PT_ISR_ISR_CATEGORY
#else
#define IRQ_QSPI5PT_EXIST    STD_OFF
#endif


#endif /* #ifndef QSPI_IRQ_H */

