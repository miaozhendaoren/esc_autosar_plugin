# \file
#
# \brief AUTOSAR Spi
#
# This file contains the implementation of the AUTOSAR
# module Spi.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Spi_src

Spi_src_FILES       += $(Spi_CORE_PATH)\src\Spi.c
Spi_src_FILES       += $(Spi_CORE_PATH)\src\SpiSlave.c
Spi_src_FILES       += $(Spi_CORE_PATH)\src\Spi_Ver.c
Spi_src_FILES       += $(Spi_CORE_PATH)\src\Spi_Irq.c
Spi_src_FILES       += $(Spi_OUTPUT_PATH)\src\Spi_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Spi_src_FILES :=
endif
